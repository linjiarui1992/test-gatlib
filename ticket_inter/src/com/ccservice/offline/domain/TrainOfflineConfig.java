/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineConfig
 * @description: TODO - 新增配置信息表 - 主要是针对账号的使用的相关的内容的处理
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月23日 下午12:40:56 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineConfig {
    private Integer PKID;//主键ID
    private String keyName;//key
    private String value;//value
    private String remark1;//备注
    private String remark2;//
    public Integer getPKID() {
        return PKID;
    }
    public void setPKID(Integer pKID) {
        PKID = pKID;
    }
    public String getKeyName() {
        return keyName;
    }
    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getRemark1() {
        return remark1;
    }
    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }
    public String getRemark2() {
        return remark2;
    }
    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }
    @Override
    public String toString() {
        return "TrainOfflineConfig [PKID=" + PKID + ", keyName=" + keyName + ", value=" + value + ", remark1=" + remark1
                + ", remark2=" + remark2 + "]";
    }
}
