package com.ccservice.b2b2c.policy;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.axis2.AxisFault;

import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.AvailableFlightWithPriceAndCommisionReply;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.AvailableFlightWithPriceAndCommisionRequest;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommision;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.WsFlightWithPriceAndCommision;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.WsFlightWithPriceAndCommisionItem;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.WsSeatWithPriceAndComisionItem;
import client.GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.WsPolicyData;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.base.service.IAirService;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.test.HttpClient;

public class FiveoneSearch extends SupplyMethod {
    //    static FiveonBookVersion3 book = new FiveonBookVersion3();
    public static void main(String[] args) {
        FiveoneSearch fiveoneSearch = new FiveoneSearch();
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        listFlightInfoAll = fiveoneSearch.getAvailableFlightWithPriceAndCommision("HRB", "SHA", "2016-08-02", "", 1, 0,
                1, 1);

    }

    /**
     * 51book航班查询接口（含票面价、返佣）
     * 
     * @param orgAirportCode 出发机场三字码
     * @param dstAirportCode 抵达城市三字码
     * @param date 起飞日期
     *            格式:“yyyy-MM-dd”
     * @param airlineCode 航空公司三字码
     * @param onlyAvailableSeat 只返回可用舱位
     * @param onlyNormalCommision 是否包括特殊政策
     * @param onlyOnWorkingCommision 是否只返回在工作时间内政策
     * @param onlySelfPNR 是否可更换PNR出票
     * @return
     */
    public List<FlightInfo> getAvailableFlightWithPriceAndCommision(String orgAirportCode, String dstAirportCode,
            String date, String airlineCode, int onlyAvailableSeat, int onlyNormalCommision,
            int onlyOnWorkingCommision, int onlySelfPNR) {
        onlyNormalCommision = 0;
        int tempint = new Random().nextInt(10000);
        Long t1 = System.currentTimeMillis();
        String userwhere = "51book";
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        if (orgAirportCode == null || dstAirportCode == null || date == null) {
            return listFlightInfoAll;
        }
        try {
            //            String agentcode = book.getFiveoneUsernameV3();
            //            String safecode = book.getFiveonePasswordV3();
            String agentcode = PropertyUtil.getValue("51bookV3Account_agentcode", "air.properties");
            String safecode = PropertyUtil.getValue("51bookV3Account_safecode", "air.properties");

            //			String agentcode = "HTHYWL";
            //			String safecode = "f755)e#t";
            GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub stub = new GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub();
            GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE getAvailableFlightWithPriceAndCommisionE = new GetAvailableFlightWithPriceAndCommisionServiceImpl_1_0ServiceStub.GetAvailableFlightWithPriceAndCommisionE();
            GetAvailableFlightWithPriceAndCommision getAvailableFlightWithPriceAndCommision = new GetAvailableFlightWithPriceAndCommision();
            AvailableFlightWithPriceAndCommisionRequest request = new AvailableFlightWithPriceAndCommisionRequest();
            request.setAgencyCode(agentcode);// 公司代码
            request.setSign(HttpClient.MD5(agentcode + dstAirportCode + onlyAvailableSeat + onlyNormalCommision
                    + onlyOnWorkingCommision + onlySelfPNR + orgAirportCode + safecode));// 签名信息 备注
            // MD5加密（agencyCode+dstCity+onlyAvailableSeat+onlyNormalCommision+onlyOnWorkingCommision+onlySelfPNR+orgCity+安全码）。加密为32位小写。
            request.setOrgAirportCode(orgAirportCode);// 出发机场三字码 例：PEK
            // PEK包含NAY SHA包含PVG
            request.setDstAirportCode(dstAirportCode);// 抵达城市三字码 例：SHA
            request.setDate(date);// 起飞日期 格式:“yyyy-MM-dd”
            request.setOnlyAvailableSeat(onlyAvailableSeat);// 只返回可用舱位 0
            // 只返回可用舱位;1
            // 返回完整舱位列表;即只返回舱位数量为1～9或为A的舱位;
            // 仅返回普通政策; 1
            request.setOnlyNormalCommision(onlyNormalCommision);// 是否包括特殊政策 0
            // 允许返回特殊政策;
            request.setOnlyOnWorkingCommision(onlyOnWorkingCommision);// 是否只返回在工作时间内政策
            // 0不做限制
            // 1仅返回当前仍在工作时间的政策;
            request.setOnlySelfPNR(onlySelfPNR);// 是否可更换PNR出票 0可以更换PNR后出票
            // 1只用自己的PNR出票
            request.setAirlineCode(airlineCode);// 航空公司
            getAvailableFlightWithPriceAndCommision.setRequest(request);
            getAvailableFlightWithPriceAndCommisionE
                    .setGetAvailableFlightWithPriceAndCommision(getAvailableFlightWithPriceAndCommision);

            String search_String_51 = tempint + ":" + orgAirportCode + ":" + dstAirportCode + ":" + date + ":"
                    + airlineCode + ":" + onlyAvailableSeat + ":" + onlyNormalCommision + ":" + onlyOnWorkingCommision
                    + ":" + onlySelfPNR + ":" + agentcode + ":" + request.getSign();
            AvailableFlightWithPriceAndCommisionReply reply = stub
                    .getAvailableFlightWithPriceAndCommision(getAvailableFlightWithPriceAndCommisionE)
                    .getGetAvailableFlightWithPriceAndCommisionResponse().get_return();
            try {
                //                System.out.println(JSONObject.toJSONString(reply));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            WsFlightWithPriceAndCommisionItem[] wsFlightWithPriceAndCommisionItemArr = reply.getFlightItems();// 航班信息
            WriteLog.write("SEARCH_51BOOK", search_String_51);
            if (reply.getReturnCode().equals("F") || wsFlightWithPriceAndCommisionItemArr == null) {
                System.out.println(orgAirportCode + ":" + dstAirportCode + ":" + date + ":51book查询航班失败:"
                        + reply.getReturnMessage());
                WriteLog.write("SEARCH_51BOOK", tempint + ":" + reply.getReturnCode() + ":" + reply.getReturnMessage());
                if (airlineCode != null && airlineCode.length() > 0) {
                }
                else {
                    userwhere = "jinri";
                    listFlightInfoAll = JinriMethod.getAvailableFlightWithPriceAndCommision(orgAirportCode,
                            dstAirportCode, date, airlineCode, onlyAvailableSeat, onlyNormalCommision,
                            onlyOnWorkingCommision, onlySelfPNR);
                }
            }
            int len = wsFlightWithPriceAndCommisionItemArr.length;
            if (len > 0) {
                for (int a = 0; a < len; a++) {
                    WsFlightWithPriceAndCommisionItem wsFlightWithPriceAndCommisionItem = wsFlightWithPriceAndCommisionItemArr[a];
                    String flightDat = wsFlightWithPriceAndCommisionItem.getDate();// 出发日期
                    // String orgCity
                    // =wsFlightWithPriceAndCommisionItem.getOrgCity();// 出发城市
                    // String dstCity
                    // =wsFlightWithPriceAndCommisionItem.getDstCity();// 抵达城市
                    WsFlightWithPriceAndCommision[] flights = wsFlightWithPriceAndCommisionItem.getFlights();// 航班详细信息

                    if (flights == null) {
                        System.out.println("航班详细信息为空");
                        continue;
                    }
                    int flen = flights.length;

                    //--------------------------------------------
                    List<Zrate> zrateToDbs = new ArrayList<Zrate>();
                    String zrateurl = getSysconfigString("51bookzrateurl");
                    //					String zratetype51book = SupplyMethod.getSysconfigString("51bookzratetype");
                    String zratesave51book = getSysconfigString("51bookzratesave");
                    //                    String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                    //                    DateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");
                    //--------------------------------------------

                    if (flen > 0) {
                        for (int b = 0; b < flen; b++) {
                            WsFlightWithPriceAndCommision wsFlightWithPriceAndCommision = flights[b];
                            Double audletAirportTax = wsFlightWithPriceAndCommision.getAirportTax();// 成人的机建费
                            Double audletFuelTax = wsFlightWithPriceAndCommision.getFuelTax();// 成人的燃油费
                            Double basePrice = wsFlightWithPriceAndCommisionItem.getBasePrice();// Y舱价格
                            Boolean codeShare = wsFlightWithPriceAndCommision.getCodeShare();// 是否共享航班
                            String shareNum = wsFlightWithPriceAndCommision.getShareNum();// 共享航班号
                            String flightNo = wsFlightWithPriceAndCommision.getFlightNo();// 航班号
                            String orgCity = wsFlightWithPriceAndCommision.getOrgCity();// 出发城市
                            String dstCity = wsFlightWithPriceAndCommision.getDstCity();// 抵达城市
                            String depTime = wsFlightWithPriceAndCommision.getDepTime();// 起飞时间
                            String arriTime = wsFlightWithPriceAndCommision.getArriTime();// 降落时间
                            String planeType = wsFlightWithPriceAndCommision.getPlaneType();// 机型
                            // 除CR2、CRJ、EMB、ERJ、MA65种小机型没有机场建设费，其他机型收取机场建设费
                            Integer stopnum = wsFlightWithPriceAndCommision.getStopnum();// 经停次数
                            String orgJetquay = wsFlightWithPriceAndCommision.getOrgJetquay();// 始发航站楼
                            if ("KY8294".equals(flightNo)) {
                                orgJetquay = "T2";
                            }
                            String dstJetquay = wsFlightWithPriceAndCommision.getDstJetquay();// 到达航站楼
                            WsSeatWithPriceAndComisionItem[] fseatItems = wsFlightWithPriceAndCommision.getSeatItems();// 舱位信息

                            FlightInfo flightInfo = new FlightInfo();
                            flightInfo.setStartAirport(orgCity);// 起飞机场
                            flightInfo.setStartAirportName("");// 起飞起场名称
                            flightInfo.setEndAirport(dstCity);// 到达机场
                            flightInfo.setEndAirportName("");// 到达机场名称
                            flightInfo.setStartAirportCity(orgCity);// 起飞起场城市名称
                            flightInfo.setAirline(flightNo);// 航线
                            String com = flightNo.substring(0, 2);
                            flightInfo.setAirCompany(com);// 航空公司
                            flightInfo.setAirCompanyName(com);// 航空公司名称
                            flightInfo.setAirportFee(audletAirportTax.floatValue());// 机场建设费
                            flightInfo.setFuelFee(audletFuelTax.floatValue());// 燃油费
                            flightInfo.setDistance(wsFlightWithPriceAndCommisionItem.getDistance() + "");// 里程数
                            flightInfo.setMeal(wsFlightWithPriceAndCommision.getMeal().equals("true") ? true : false);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HHmm");
                            Date startDate = dateFormat.parse(flightDat + " " + depTime);
                            Date arriveDate = dateFormat.parse(flightDat + " " + arriTime);
                            if (Integer.valueOf(arriTime) < Integer.valueOf(depTime)) {
                                arriveDate = new Date(arriveDate.getTime() + 24 * 3600 * 1000);
                            }
                            flightInfo.setDepartTime(new Timestamp(startDate.getTime()));// 起飞时间
                            flightInfo.setArriveTime(new Timestamp(arriveDate.getTime()));// 到达时间

                            flightInfo.setYPrice(basePrice.floatValue());// 全价价格
                            if (codeShare != null && codeShare) {
                                flightInfo.setIsShare(1);// 是否是共享航班
                            }
                            else {
                                flightInfo.setIsShare(0);
                            }
                            if (shareNum != null) {
                                flightInfo.setShareFlightNumber(shareNum);// 共享航班号
                            }
                            flightInfo.setAirplaneType(planeType);// 飞机型号
                            flightInfo.setAirplaneTypeDesc("");// 飞机型号描述
                            if (orgJetquay != null) {
                                flightInfo.setOffPointAT(orgJetquay);// 出发航站楼
                            }
                            if (dstJetquay != null) {
                                flightInfo.setBorderPointAT(dstJetquay);// 到达航站楼
                            }
                            if (stopnum > 0) {
                                flightInfo.setStop(true);
                                flightInfo.setIsStopInfo(stopnum + "");
                            }
                            else {
                                flightInfo.setStop(false);
                            }

                            if (fseatItems == null) {
                                System.out.println("舱位信息为空");
                                continue;
                            }
                            int fslen = fseatItems.length;
                            List<CarbinInfo> listCabinAll = new ArrayList<CarbinInfo>();
                            if (fslen > 0) {
                                for (int c = 0; c < fslen; c++) {
                                    WsSeatWithPriceAndComisionItem wsSeatWithPriceAndComisionItem = fseatItems[c];
                                    String fsseatCode = wsSeatWithPriceAndComisionItem.getSeatCode();// 舱位码
                                    String seatStatus = wsSeatWithPriceAndComisionItem.getSeatStatus();// 舱位状态
                                    Double discount = wsSeatWithPriceAndComisionItem.getDiscount();// 折扣
                                    //									System.out.println(flightNo+":"+fsseatCode+":"+discount);
                                    Integer parPrice = wsSeatWithPriceAndComisionItem.getParPrice();// 票面价
                                    String cabinTypeName = wsSeatWithPriceAndComisionItem.getSeatMsg();//舱位名称
                                    CarbinInfo cabin = new CarbinInfo();
                                    cabin.setCabin(fsseatCode);
                                    if (cabinTypeName != null) {
                                        cabin.setCabintypename(cabinTypeName);
                                    }
                                    if (seatStatus != null && seatStatus.equals("A")) {
                                        cabin.setSeatNum("9");
                                    }
                                    else {
                                        cabin.setSeatNum(seatStatus);
                                    }
                                    if (discount >= 0) {

                                        cabin.setDiscount(Float.parseFloat(discount * 100 + ""));
                                    }

                                    if (wsSeatWithPriceAndComisionItem.getSeatType() == 3) {
                                        cabin.setSpecial(true);
                                    }
                                    Float price = parPrice.floatValue();
                                    cabin.setPrice(price);
                                    WsPolicyData wsPolicyData = wsSeatWithPriceAndComisionItem.getPolicyData();
                                    Zrate zrate = getZrateInfo(wsPolicyData, orgCity, dstCity, flightNo, fsseatCode,
                                            com, flightDat, price);//获取到政策信息
                                    cabin.setZrate(zrate);
                                    listCabinAll.add(cabin);
                                }
                            }
                            Collections.sort(listCabinAll, new Comparator<CarbinInfo>() {
                                @Override
                                public int compare(CarbinInfo o1, CarbinInfo o2) {
                                    if (o1.getDiscount() == null || o2.getDiscount() == null) {
                                        return 1;
                                    }
                                    else {
                                        if (o1.getDiscount() > o2.getDiscount()) {
                                            return 1;
                                        }
                                        else if (o1.getDiscount() < o2.getDiscount()) {
                                            return -1;
                                        }
                                    }
                                    return 0;
                                }
                            });
                            flightInfo.setCarbins(listCabinAll);// 加入 仓位信息
                            int cabinIndex = 0;
                            CarbinInfo lowCabinInfo = new CarbinInfo();
                            for (int i = 0; i < listCabinAll.size(); i++) {
                                if (listCabinAll.get(i).getPrice() > 0) {
                                    cabinIndex = i;
                                    break;
                                }
                            }
                            if (listCabinAll.size() > 0 && listCabinAll.size() >= cabinIndex) {
                                CarbinInfo tempCabinInfo = (CarbinInfo) listCabinAll.get(cabinIndex);
                                if (tempCabinInfo.getCabin() != null) {
                                    lowCabinInfo.setCabin(tempCabinInfo.getCabin());
                                }
                                if (tempCabinInfo.getRatevalue() != null) {
                                    lowCabinInfo.setRatevalue(tempCabinInfo.getRatevalue());
                                }
                                if (tempCabinInfo.getCabinRemark() != null) {
                                    lowCabinInfo.setCabinRemark(tempCabinInfo.getCabinRemark());
                                }
                                else {
                                    lowCabinInfo.setCabinRemark("");
                                }
                                if (tempCabinInfo.getCabinRules() != null) {
                                    lowCabinInfo.setCabinRules(tempCabinInfo.getCabinRules());
                                }
                                else {
                                    lowCabinInfo.setCabinRules("");
                                }
                                if (tempCabinInfo.getDiscount() != null) {
                                    lowCabinInfo.setDiscount(tempCabinInfo.getDiscount());
                                }
                                if (tempCabinInfo.getLevel() != null) {
                                    lowCabinInfo.setLevel(tempCabinInfo.getLevel());
                                }
                                else {
                                    lowCabinInfo.setLevel(1);
                                }
                                if (tempCabinInfo.getPrice() != null) {
                                    lowCabinInfo.setPrice(tempCabinInfo.getPrice());
                                }
                                else {
                                    lowCabinInfo.setPrice(0f);
                                }
                                if (tempCabinInfo.getSeatNum() != null) {
                                    lowCabinInfo.setSeatNum(tempCabinInfo.getSeatNum());
                                }
                                else {
                                    lowCabinInfo.setSeatNum("0");
                                }
                                if (tempCabinInfo.getCabintypename() != null) {
                                    lowCabinInfo.setCabintypename(tempCabinInfo.getCabintypename());
                                }
                                else {
                                    lowCabinInfo.setCabintypename("");
                                }
                                if (tempCabinInfo.isSpecial()) {
                                    lowCabinInfo.setSpecial(true);
                                }
                                else {
                                    lowCabinInfo.setSpecial(false);
                                }
                                Zrate tempzrate = new Zrate();
                                try {
                                    if (tempCabinInfo.getZrate() != null && tempCabinInfo.getZrate().getGeneral() == 1) {
                                        tempzrate.setRatevalue(tempCabinInfo.getZrate().getRatevalue());
                                        tempzrate.setId(1L);
                                        tempzrate.setGeneral(tempCabinInfo.getZrate().getGeneral());
                                        tempzrate.setOutid(tempCabinInfo.getZrate().getOutid());
                                        tempzrate.setAgentid(tempCabinInfo.getZrate().getAgentid());
                                        tempzrate.setWorktime(tempCabinInfo.getZrate().getWorktime());
                                        tempzrate.setAfterworktime(tempCabinInfo.getZrate().getAfterworktime());
                                        tempzrate.setOnetofivewastetime(tempCabinInfo.getZrate()
                                                .getOnetofivewastetime());
                                        tempzrate.setSpeed("10");
                                        tempzrate.setRemark("");
                                    }
                                    else {
                                        tempzrate = getTempzrate(tempzrate);

                                    }
                                }
                                catch (Exception e) {
                                    tempzrate = getTempzrate(tempzrate);
                                }
                                lowCabinInfo.setZrate(tempzrate);
                                flightInfo.setLowCarbin(lowCabinInfo);
                            }
                            else {
                                continue;
                            }
                            listFlightInfoAll.add(flightInfo);
                        }
                    }
                }
            }
        }
        catch (AxisFault e) {
        }
        catch (RemoteException e) {
        }
        catch (NoSuchAlgorithmException e) {
        }
        catch (ParseException e) {
        }
        catch (Exception e) {
        }
        WriteLog.write("SEARCH_51BOOK", tempint + ":" + userwhere + ":" + (System.currentTimeMillis() - t1));
        return listFlightInfoAll;
    }

    /**
     * 
     * @param tempzrate
     * @return
     * @time 2016年7月1日 上午10:19:01
     * @author chendong
     */
    private Zrate getTempzrate(Zrate tempzrate) {
        tempzrate.setId(1L);
        tempzrate.setRatevalue(0.0F);
        tempzrate.setGeneral(1L);
        tempzrate.setOutid("1");
        tempzrate.setAgentid(5L);
        tempzrate.setWorktime("08:00");
        tempzrate.setAfterworktime("18:00");
        tempzrate.setOnetofivewastetime("17:30");
        tempzrate.setSpeed("10");
        tempzrate.setRemark("");
        return tempzrate;
    }

    /**
     * 
     * @return
     * @time 2016年7月1日 上午10:12:40
     * @author chendong
     * @param wsPolicyData 
     * @param fsseatCode 
     * @param flightNo 
     * @param dstCity 
     * @param orgCity 
     * @param com 
     * @param flightDat 
     * @param price  票面价
     */
    private Zrate getZrateInfo(WsPolicyData wsPolicyData, String orgCity, String dstCity, String flightNo,
            String fsseatCode, String com, String flightDat, Float price) {
        Zrate zrate = new Zrate();
        try {
            zrate.setAgentid(5L);
            zrate.setDepartureport(orgCity);
            zrate.setArrivalport(dstCity);
            zrate.setFlightnumber(flightNo);
            zrate.setCabincode(fsseatCode);
            zrate.setCreateuser("51bookSearch");
            zrate.setCreatetime(new Timestamp(System.currentTimeMillis()));
            zrate.setIsenable(1);
            zrate.setAircompanycode(com);
            int ttickettype = 2;
            if (wsPolicyData.getPolicyType().indexOf("BSP") >= 0) {
                ttickettype = 1;
            }
            zrate.setTickettype(ttickettype);
            zrate.setOutid(wsPolicyData.getPolicyId() + "");
            zrate.setRemark(wsPolicyData.getComment());
            zrate.setWorktime(wsPolicyData.getWorkTime().split("-")[0]);
            zrate.setAfterworktime(wsPolicyData.getWorkTime().split("-")[1]);
            zrate.setOnetofivewastetime(wsPolicyData.getVtWorkTime());
            zrate.setGeneral(wsPolicyData.getCommisionType() + 1L);
            zrate.setBegindate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(flightDat).getTime()));
            //            zrate.setRatevalueMoney(wsPolicyData.getCommisionMoney());
            Float ratevalue = Float.parseFloat(wsPolicyData.getCommisionPoint());//返点
            Double commisionMoney = wsPolicyData.getCommisionMoney();
            if (commisionMoney > 0) {
                int iRatevalue = (int) (price * ratevalue * 0.01) - 1;
                int totalMoney = (int) (iRatevalue + commisionMoney);
                ratevalue = totalMoney / price * 100;
                DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足1位,会以0补足.
                String p = decimalFormat.format(ratevalue);//format 返回的是字符串
                ratevalue = Float.parseFloat(p);
            }
            zrate.setRatevalue(ratevalue);
        }
        catch (Exception e) {
            //                                      e.printStackTrace();
        }
        return zrate;
    }
}
