package com.ccservice.grabticket.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ccservice.huamin.WriteLog;

public class DateUtils {

	public static String dateFormat(String dateStr, String format){
		
		String result = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		try {
			Date date = sdf.parse(dateStr);
			
			result = sdf.format(date);
			
		} catch (ParseException e) {

			WriteLog.write("线上线下抢票接口对接", "时间格式转换错误" + dateStr);
		}
		return result;
	}

	public static String dateFormat(Date date, String format){
		
		String result = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
			
		result = sdf.format(date);
		return result;
	}
}
