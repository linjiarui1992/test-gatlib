package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.YeeXingMethod;

public class YeeXingZratebyFlightNumThread implements Callable<List<Zrate>> {

	private Orderinfo order;
	private int special;

	public YeeXingZratebyFlightNumThread() {
	}

	public YeeXingZratebyFlightNumThread(Orderinfo order, int special) {
		this.order = order;
		this.special = special;
	}

	
	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> yeeXingZrates = new ArrayList<Zrate>();
		try {
			yeeXingZrates = YeeXingMethod.PnrMatchAirp(order.getPnr());
		} catch (Exception e) {
		}
		return yeeXingZrates;
	}
}
