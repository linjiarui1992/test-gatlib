package com.ccservice.rabbitmq.util;

import javax.jms.JMSException;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.mq.producer.method.MQSendMessageMethod;

public class MQUtil {
    //1是rabbit 0是active
    private static int MQ_TYPE = 1;

    private static final int RABBIT_MQ = 1;

    private static final int ACTIVE_MQ = 0;

    /**
     * 表示是用于支付的
     */
    public static final int PAY_TYPE = 1;
    static {
        try {
            MQ_TYPE = Integer.valueOf(PropertyUtil.getValue("isMeiTuanMQ", "rabbitMQ.properties"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMQ(String url, String QUEUE_NAME, String text) throws JMSException {
        if (RABBIT_MQ == MQ_TYPE) {
            //RabbitMQUtil.sendOnemessagePay(text, QUEUE_NAME);
            MQSendMessageMethod.sendOnemessageUnException(text, QUEUE_NAME, 3);
        }
        else if (ACTIVE_MQ == MQ_TYPE) {
            ActiveMQUtil.sendMessage(url, QUEUE_NAME, text);
        }
    }
}
