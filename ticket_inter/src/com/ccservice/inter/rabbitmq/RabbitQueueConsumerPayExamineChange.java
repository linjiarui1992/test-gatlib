package com.ccservice.inter.rabbitmq; 

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.train.mqlistener.TrainorderChangePayMessageListener;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * 
 * 改签支付审核
 * @author RRRRRR
 * @time 2016年11月18日 下午2:03:33
 */
public class RabbitQueueConsumerPayExamineChange extends QueueConsumer implements Runnable,Consumer{
    
    public RabbitQueueConsumerPayExamineChange(String endPointName) throws IOException {
        super(endPointName);
    }
    
    private TrainorderChangePayMessageListener tcpml=new TrainorderChangePayMessageListener();
    
    private long changeId;

    public void handleDelivery(String consumerTag, Envelope env,BasicProperties props, byte[] body) throws IOException {
        Map<?, ?> map = (HashMap<?, ?>)SerializationUtils.deserialize(body);
        try {
            changeId=Long.valueOf(map.get("message number").toString());
            System.out.println("改签支付审核  接收 ：————————>"+map.get("message number").toString());
            WriteLog.write("Rabbit开始改签支付审核", System.currentTimeMillis() + ":changeNoticeResult:" + changeId);
        }
        catch (Exception e) {
            changeId = 0;
        }
        //支付审核操作
        if (changeId > 0) {
            tcpml.RepOperateV2(changeId);
        }
    }
    
   
    
    
}
