package com.ccservice.train.qunar;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.compareprice.PropertyUtil;

public class QunarBindPhone {

    /**
     * 去哪儿核验手机号方法
     * 
     * @param un 12306用户名
     * @param pw 12306密码
     * @time 2015年8月27日 下午3:20:00
     * @author chendong
     */
    public static String BindPhoneValidate(String un, String pw) {
        String HMAC = "";
        String QunarBindPhone_merchantCode = PropertyUtil.getValue("QunarBindPhone_merchantCode",
                "train.qunar.properties");
        String QunarBindPhone_qunarUrl = PropertyUtil.getValue("QunarBindPhone_qunarUrl", "train.qunar.properties");
        String QunarBindPhone_MerchantKey = PropertyUtil.getValue("QunarBindPhone_MerchantKey",
                "train.qunar.properties");
        WriteLog.write("QunarBindPhone_BindPhoneValidate", "merchantCode:" + QunarBindPhone_merchantCode + "-un:" + un
                + "-pw:" + pw + "-key:" + QunarBindPhone_MerchantKey);
        try {
            HMAC = ElongHotelInterfaceUtil.MD5(QunarBindPhone_MerchantKey + QunarBindPhone_merchantCode + un + pw)
                    .toUpperCase();

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject json = new JSONObject();
        json.put("merchantCode", QunarBindPhone_merchantCode);
        json.put("un", un);
        json.put("pw", pw);
        json.put("HMAC", HMAC);
        //        String paramContent = json.toString();merchantCode
        String paramContent = "un=" + un + "&pw=" + pw + "&merchantCode=" + QunarBindPhone_merchantCode + "&HMAC="
                + HMAC;
        String resultStr = SendPostandGet.submitPost(QunarBindPhone_qunarUrl, paramContent, "UTF-8").toString();

        WriteLog.write("QunarBindPhone_BindPhoneValidate", ":resultStr:" + resultStr + ":paramContent:" + paramContent
                + ":url:" + QunarBindPhone_qunarUrl);
        return resultStr;
    }

    public static void main(String[] args) {
        //        JSONObject json = new JSONObject();
        //        String url = "http://121.40.241.126:9516/cn_interface/QunarBindPhoneServlet";
        //        json.put("un", "cd198992906");
        //        json.put("pw", "asd123456");
        //
        //        String resultStr = SendPostandGet.submitPost(url, json.toString(), "UTF-8").toString();
        //        System.out.println(resultStr);

    }
}
