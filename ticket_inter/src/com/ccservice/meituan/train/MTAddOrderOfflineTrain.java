package com.ccservice.meituan.train;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.List;




import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;



import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;



/**
 * 美团下单接口
 * @author zhangruixuan
 * 2017-08-24
 */

public class MTAddOrderOfflineTrain extends HttpServlet {
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	@Override
	public void init() throws ServletException {
		try {
			Scheduler sched = new StdSchedulerFactory().getScheduler();
			//MeiTuanUpdateExpressJob
			String jobName = "ExpressInfoJob";						//任务名称
			String JOB_GROUP_NAME = "ExpressInfoJobGROUP";			//任务组名称 
			String TRIGGER_GROUP_NAME = "ExpressInfoTRIGGER"; 		//触发器组名称
			JobDetail jobDetail = new JobDetail(jobName, JOB_GROUP_NAME, 
					new MeiTuanUpdateExpressJob().getClass());		//任务名，任务组，任务执行类  
			CronTrigger  trigger =   
		            new CronTrigger(jobName, TRIGGER_GROUP_NAME);	//触发器名,触发器组  
			trigger.setCronExpression("0 0/20 * * * ? ");			//设置任务处理时间(每隔20分钟请求一次)
			sched.scheduleJob(jobDetail, trigger); 			
			if(!sched.isShutdown()){
				sched.start();			
			}	          
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int code = 0;										//返回码
		String msg = "错误的业务参数";							//返回描述
		Boolean success = false ;
		String req_token = "";
		String MTOrderNumber = "";
		BufferedReader bf =null;
		//设置请求参数的编码
				
			try {
				request.setCharacterEncoding("UTF-8");
				ServletInputStream inputStream = request.getInputStream();			
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
				bf = new BufferedReader(inputStreamReader);	//获取请求体中的内容
				StringBuffer sb = new StringBuffer("");  
				String temp;  
				while ((temp = bf.readLine()) != null) {  
				    sb.append(temp);  
				}  
				String jsonStr = sb.toString();			
				WriteLog.write("美团请求下单接口", "解析reader完成。。。。。。。。。。。。。"+jsonStr);
				if (jsonStr!=null && !"".equals(jsonStr)) {
					JSONObject json = JSONObject.parseObject(jsonStr);
					String paper_msg = "";
					//获取参数
					MTOrderNumber = json.getString("orderid");	//美团订单编号
					//String order_type = json.getString("order_type");	//订单类型，paied:先支付后占座，unpaied:先占座后支付
					String train_code = json.getString("train_code");	//车次
					String from_station_code = json.getString("from_station_code");	//出发站简码
					String from_station_name = json.getString("from_station_name");	//出发站名称
					String to_station_code = json.getString("to_station_code");		//到达站简码
					String to_station_name = json.getString("to_station_name");		//到达站名称
					String train_date = json.getString("train_date");				//乘车日期
					req_token = json.getString("req_token");
					String choose_no_seat = json.getString("choose_no_seat");				//是否出无座票  true:不出无座票  false:允许出无座票		
					JSONArray passengers = json.getJSONArray("passengers");					//乘客信息
					String is_paper = json.getString("is_paper");							//是否纸质票
					String paper_type = json.getString("paper_type");								//纸质票类型(0：普通，1：靠窗，2：连座，3：过道，4：下铺，5：中铺，6：上铺，7：连铺包间)
					String paper_low_seat = json.getString("paper_low_seat");						//定制坐席的最少数量		定制服务使用的
					String paper_backup = json.getString("paper_backup");							//当选择的纸质票定制坐席无票时，是否接收其他坐席(1：接收，0：不接收)
					String paper_transport_name = json.getString("paper_transport_name");		//纸质票配送收件人姓名
					String paper_transport_phone = json.getString("paper_transport_phone");		//纸质票配送收件人电话
					String paper_transport_address = json.getString("paper_transport_address");	//纸质票配送收件人地址
					
					if (!MTOrderNumber.isEmpty()&&!train_code.isEmpty()&&!from_station_code.isEmpty()&&!from_station_name.isEmpty()&&
							!to_station_code.isEmpty()&&!to_station_name.isEmpty()&&!train_date.isEmpty()&&!choose_no_seat.isEmpty()&&
							!req_token.isEmpty()&&!paper_type.isEmpty()&&!paper_low_seat.isEmpty()&&!paper_backup.isEmpty()&&!paper_transport_name.isEmpty()
							&&!paper_transport_phone.isEmpty()&&!paper_transport_address.isEmpty()&&!passengers.toString().isEmpty()) {
						
						WriteLog.write("美团请求下单接口", "请求参数json="+jsonStr.toString());
						if ("true".equals(is_paper)) {//纸质票					
							String sql = "SELECT id FROM TrainOrderOffline with(nolock) WHERE OrderNumberOnline='" + MTOrderNumber+ "'";
							List list = getSystemServiceOldDB().findMapResultBySql(sql, null); //根据美团订单号判断重复订单					
							if (list.size()==0) {
								new MyThreadMeiTuanAddOrderOfflin(json).start();				    									    					    	
							    code = 100;
					            msg = "处理或操作成功";
					            success = true;			
							}else {
								code = 108;
								msg	= "错误的业务参数,重复订单";
								success = false;	
							}			
						}else{
							code = 108;
							msg	= "错误的业务参数";
							success = false;				
						}							
					}else{
						WriteLog.write("美团请求下单接口", "请求参数json="+jsonStr.toString());
						code = 108;
						msg	= "错误的业务参数";
						success = false;
					}																																									
				}else{
					code = 101;
					msg	= "传入的json为空对象";
					success = false;
					}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				WriteLog.write("美团请求下单接口", "servlet异常，MTOrderNumber"+MTOrderNumber);
			}finally{
				if (bf != null) {
	                bf.close();
	            }
			}		
		response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        responsejson.put("success", success);
        responsejson.put("req_token", req_token);
        WriteLog.write("美团请求下单接口", "orderNumber="+MTOrderNumber+";responsejson="+responsejson.toString());
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }					
	
	}
	
	public ISystemService getSystemServiceOldDB() {
        String systemdburlString = PropertyUtil.getValue("offlineservice", "train.properties");
        HessianProxyFactory factory = new HessianProxyFactory();
        try {
            return (ISystemService) factory.create(ISystemService.class,
                    systemdburlString + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
	
	
}
