
package com.speed.iesales.service;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rateDel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rateDel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="deletestrategyid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="opdate" type="{http://service.iesales.speed.com/}timestamp" minOccurs="0"/>
 *         &lt;element name="strategyId" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rateDel", propOrder = {
    "deletestrategyid",
    "opdate",
    "strategyId"
})
public class RateDel {

    protected String deletestrategyid;
    protected Timestamp opdate;
    protected BigDecimal strategyId;

    /**
     * Gets the value of the deletestrategyid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeletestrategyid() {
        return deletestrategyid;
    }

    /**
     * Sets the value of the deletestrategyid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeletestrategyid(String value) {
        this.deletestrategyid = value;
    }

    /**
     * Gets the value of the opdate property.
     * 
     * @return
     *     possible object is
     *     {@link Timestamp }
     *     
     */
    public Timestamp getOpdate() {
        return opdate;
    }

    /**
     * Sets the value of the opdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Timestamp }
     *     
     */
    public void setOpdate(Timestamp value) {
        this.opdate = value;
    }

    /**
     * Gets the value of the strategyId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStrategyId() {
        return strategyId;
    }

    /**
     * Sets the value of the strategyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStrategyId(BigDecimal value) {
        this.strategyId = value;
    }

}
