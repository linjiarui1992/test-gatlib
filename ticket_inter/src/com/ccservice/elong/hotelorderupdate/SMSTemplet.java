package com.ccservice.elong.hotelorderupdate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.templet.Templet;
import com.ccservice.inter.server.Server;

/**
 * 短信模板接口 hanxiaoshuang 2012-07-19
 * 
 * @author Administrator
 * 
 */
@SuppressWarnings("unused")
public class SMSTemplet {
  static	Log log = LogFactory.getLog(SMSTemplet.class);

	public static class SMSType {
		public SMSType(int type) {
			this.type = type;
		}

		public static SMSType setSmstype(int type) {
			return new SMSType(type);
		}

		private int type = 0;

		// 注册短信
		public static final SMSType REGISTRATION = setSmstype(1);

		// 机票预定成功短信
		public static final SMSType FLIGHTSUCCESSOF = setSmstype(2);

		// 机票出票短信
		public static final SMSType TICKETOUTOFTHETICKET = setSmstype(3);

		// 酒店订单确认短信
		public static final SMSType HOTELORDERSCONFIRMATION = setSmstype(4);

		// 酒店订单取消短信
		public static final SMSType HOTELCANCELLATIONOFORDERS = setSmstype(5);

		// 酒店订单付款通知短信
		public static final SMSType HOTELORDERSPAYMENTNOTIFICATION = setSmstype(6);

		// 酒店预订成功短信
		public static final SMSType HOTELBOOKINGSUCCESSFUL = setSmstype(7);

		// 机票退费票
		public static final SMSType TICKETREFUNDTICKET = setSmstype(8);

		// 机票出票通知(往返)
		public static final SMSType TICKETROUNDTRIP = setSmstype(9);

		// 机票出票通知(单程)
		public static final SMSType TICKETONEWAY = setSmstype(10);
		
		//采购商开户
		public static final SMSType OPENACCOUNT = setSmstype(11);
		

		public int getType() {
			return type;
		}

		public void setType(int type) {

			this.type = type;
		}
	}


	// 通过方法去调用
	public static String getSMSTemplet(SMSType smstype,
			Dnsmaintenance dnsmaintenance) {
		try {
			Templet templet = Server.getInstance().getMemberService()
					.findTempletbyagentid(smstype.getType(),dnsmaintenance.getAgentid());
			if (templet != null) {
				return templet.getTempletmess();
			}else{
				log.error("dnsmaintenance.getAgentid():"+dnsmaintenance+";smstype:"+smstype);
			}
		} catch (Exception e) {
			log.error("dnsmaintenance.getAgentid():"+dnsmaintenance+";smstype:"+smstype);
			e.printStackTrace();
		}finally{
			
		}
		return "";
	}
}
