package com.ccservice.compareprice.jielv;

import java.util.*;
import java.text.SimpleDateFormat;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class JlComparePriceWithQunarJob implements Job {

    private int catchdays = Integer.parseInt(PropertyUtil.getValue("catchdays").trim());

    private int addPrice = Integer.parseInt(PropertyUtil.getValue("addPrice").trim());//加价

    //private String ReloadQunarGetHotelInfo = PropertyUtil.getValue("ReloadQunarGetHotelInfo");

    private String comparePriceTime = PropertyUtil.getValue("comparePriceTime").trim();

    private String onlyhaveroom = PropertyUtil.getValue("onlyhaveroom").trim();

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    @SuppressWarnings("unchecked")
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String propertyInterval = PropertyUtil.getValue("jlComparePriceUpdateInterval");
        long jlComparePriceUpdateInterval = Long.parseLong(propertyInterval.trim()) * 1000;//更新间隔
        while (true) {
            try {
                //TIME
                Date current = format.parse(format.format(new Date()));
                Date start = format.parse(comparePriceTime.split("-")[0]);
                Date end = format.parse(comparePriceTime.split("-")[1]);
                //WAIT
                if (current.before(start)) {
                    long wait = start.getTime() - current.getTime();
                    System.out.println("比价时间未到，等待" + wait / 1000 + "秒。");
                    Thread.sleep(wait);
                }
                if (current.after(end)) {
                    Date _24 = format.parse("24:00:00");
                    Date _00 = format.parse("00:00:00");
                    long wait = _24.getTime() - current.getTime() + start.getTime() - _00.getTime();
                    System.out.println("比价时间未到，等待" + wait / 1000 + "秒。");
                    Thread.sleep(wait);
                }
            }
            catch (Exception e) {
            }
            long start = System.currentTimeMillis();
            try {
                //删除小于当前日期的、更新最后时间
                String updateSql = "delete from T_HOTELGOODDATA where C_DATENUM < '"
                        + ElongHotelInterfaceUtil.getCurrentDate() + "'";
                Server.getInstance().getSystemService().findMapResultBySql(updateSql, null);
                if (addPrice < 0) {
                    throw new Exception("加价错误。");
                }
                List<City> cityes = Server.getInstance().getHotelService()
                        .findAllCity("where c_jlcode is not null", "order by id", -1, 0);
                for (City city : cityes) {
                    if (city.getId() == 102 || city.getName().contains("上海")) {
                        continue;//上海
                    }
                    System.out.println("城市：" + city.getName());
                    try {
                        getHotelDatas(city.getId());
                    }
                    catch (Exception e) {
                        StackTraceElement ele = e.getStackTrace()[0];
                        System.out.println("城市 [" + city.getName() + " ]比价去哪儿发生异常：" + e.getMessage() + "，类名："
                                + ele.getFileName() + "，方法名：" + ele.getMethodName() + "，行数：" + ele.getLineNumber());
                    }
                }
            }
            catch (Exception e) {
                if ("加价错误。".equals(e.getMessage())) {
                    System.out.println("加价错误，中断操作。");
                    break;
                }
            }
            finally {
                //更新缓存
                //SendPostandGet.submitGet(ReloadQunarGetHotelInfo).toString();
                long end = System.currentTimeMillis();
                long interval = end - start;
                if (interval < jlComparePriceUpdateInterval) {
                    try {
                        long longtime = jlComparePriceUpdateInterval - interval;
                        System.out.println("当前比价去哪儿完毕，休息" + (longtime / 1000) + "秒。");
                        Thread.sleep(longtime);
                    }
                    catch (Exception e) {
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        List<City> cityes = Server.getInstance().getHotelService()
                .findAllCity("where c_jlcode is not null", "order by id", -1, 0);
        for (City city : cityes) {
            if (city.getId() == 102 || city.getName().contains("上海")) {
                continue;//上海
            }
            System.out.println("城市：" + city.getName());
            new JlComparePriceWithQunarJob().getHotelDatas(city.getId());
            //更新缓存
            SendPostandGet.submitGet(PropertyUtil.getValue("ReloadQunarGetHotelInfo")).toString();
        }
        long end = System.currentTimeMillis();
        System.out.println("用时：" + (end - start) / 1000 / 60 + "分钟");
    }

    @SuppressWarnings("unchecked")
    public void getHotelDatas(long cityid) throws Exception {
        String where = new String(PropertyUtil.getValue("jlHotelSql").getBytes("iso8859-1"), "utf-8");
        where += " and c_cityid = " + cityid;
        List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(where.trim(), "", -1, 0);
        if (hotels.size() > 0) {
            for (Hotel h : hotels) {
                try {
                    if (ElongHotelInterfaceUtil.StringIsNull(h.getQunarId())
                            || ElongHotelInterfaceUtil.StringIsNull(h.getHotelcode())) {
                        continue;
                    }
                    long hid = h.getId();
                    //时间
                    String startDate = ElongHotelInterfaceUtil.getCurrentDate();
                    String endDate = ElongHotelInterfaceUtil.getAddDate(startDate, catchdays);
                    //加载捷旅
                    Map<String, List<HotelGoodData>> map = new HashMap<String, List<HotelGoodData>>();
                    try {
                        map = loadJLroomtype(h, startDate, endDate);
                    }
                    catch (Exception e) {
                    }
                    //加载去哪儿
                    List<HotelGoodData> datas = new JlComparePrice().loadQunarData(map, h, startDate, endDate);
                    //判断酒店是否利用时间差通过手工上去哪儿了、概率小
                    h = Server.getInstance().getHotelService().findHotel(h.getId());
                    if (h.getPush() != null && h.getPush().intValue() == 1) {
                        continue;
                    }
                    //优势数据入库
                    if (datas != null && datas.size() > 0) {
                        writeDataDB(datas, hid);
                    }
                    //无优势、删除原有数据、更新酒店push
                    else {
                        String sql = "select top 1 * from t_hotelgooddata where c_hotelid = " + hid;
                        List<HotelGoodData> olds = h.getPush() == null ? Server.getInstance().getHotelService()
                                .findAllHotelGoodDataBySql(sql, -1, 0) : new ArrayList<HotelGoodData>();
                        if (h.getPush() != null || olds.size() > 0) {
                            String delsql = "delete from t_hotelgooddata where c_hotelid = " + hid
                                    + ";update t_hotel set c_push = null , c_goqunarupdatetime = '"
                                    + sdf.format(new Date()) + "' where id = " + hid;
                            Server.getInstance().getSystemService().findMapResultBySql(delsql, null);
                        }
                    }
                }
                catch (Exception e) {
                    StackTraceElement ele = e.getStackTrace()[0];
                    System.out.println("深捷旅酒店 [" + h.getName() + " ]比价去哪儿发生异常：" + e.getMessage() + "，类名："
                            + ele.getFileName() + "，方法名：" + ele.getMethodName() + "，行数：" + ele.getLineNumber());
                }
            }
        }
    }

    //优势数据写入数据库
    @SuppressWarnings("unchecked")
    private void writeDataDB(List<HotelGoodData> datas, long hotelid) throws Exception {
        //查询原有
        Map<String, HotelGoodData> oldMap = new HashMap<String, HotelGoodData>();//key：jlkeyid
        List<HotelGoodData> olds = Server.getInstance().getHotelService()
                .findAllHotelGoodData("where c_hotelid = " + hotelid, "", -1, 0);
        if (olds != null && olds.size() > 0) {
            for (HotelGoodData old : olds) {
                oldMap.put(old.getJlkeyid(), old);
            }
        }
        //更新酒店标示
        int count = 0;
        //保存新数据
        String updatetime = sdf.format(new Date());
        for (HotelGoodData jl : datas) {
            String keyid = jl.getJlkeyid();
            try {
                //原先存在，更新
                if (oldMap.size() > 0 && oldMap.containsKey(keyid)) {
                    HotelGoodData old = oldMap.get(keyid);
                    //深捷旅最后更新时间不同或价格发生变化
                    if (!old.getJltime().equals(jl.getJltime())
                            || old.getShijiprice().longValue() != jl.getShijiprice().longValue()) {
                        jl.setId(old.getId());
                        jl.setRoomflag(old.getRoomflag());
                        jl.setUpdatetime(updatetime);
                        Server.getInstance().getHotelService().updateHotelGoodData(jl);
                        count++;
                    }
                }
                //原先不存在，新增
                else {
                    jl.setUpdatetime(updatetime);
                    Server.getInstance().getHotelService().createHotelGoodData(jl);
                    count++;
                }
            }
            catch (Exception e) {
                StackTraceElement ele = e.getStackTrace()[0];
                System.out.println("深捷旅酒店 [" + jl.getHotelname() + " ]比价去哪儿发生异常：" + e.getMessage() + "，类名："
                        + ele.getFileName() + "，方法名：" + ele.getMethodName() + "，行数：" + ele.getLineNumber());
            }
            finally {
                oldMap.remove(keyid);
            }
        }
        //删除
        if (oldMap.size() > 0) {
            String ids = "";
            for (String key : oldMap.keySet()) {
                HotelGoodData old = oldMap.get(key);
                ids += old.getId() + ",";
            }
            ids = ids.substring(0, ids.length() - 1);
            String delsql = "delete from T_HOTELGOODDATA where ID in (" + ids + ")";
            Server.getInstance().getSystemService().findMapResultBySql(delsql, null);
            count++;
        }
        //PUSH --> 2：程序上去哪儿 、 通过比价更新
        if (count > 0) {
            String sql = "update t_hotel set c_push = 2 , c_goqunarupdatetime = '" + sdf.format(new Date())
                    + "' where id = " + hotelid;
            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        }
    }

    @SuppressWarnings("unchecked")
    private Map<String, List<HotelGoodData>> loadJLroomtype(Hotel hotel, String startDate, String endDate)
            throws Exception {
        System.out.println("加载深捷旅数据：" + hotel.getName());
        Map<String, Boolean> repeatMap = new HashMap<String, Boolean>();//重复天数，有问题数据
        Map<String, String> checkRepeatMap = new HashMap<String, String>();
        Map<String, List<HotelGoodData>> map = new HashMap<String, List<HotelGoodData>>();
        Map<String, Integer> checkAllClose = new HashMap<String, Integer>();
        //本地房型
        String where = "where C_ROOMCODE != '' and C_HOTELID  = " + hotel.getId();
        List<Roomtype> localRoomList = Server.getInstance().getHotelService().findAllRoomtype(where, "", -1, 0);
        if (localRoomList == null || localRoomList.size() == 0) {
            return map;
        }
        //请求深捷旅
        List<JLPriceResult> jlResults = Server.getInstance().getIJLHotelService()
                .getHotelPriceByRoom(hotel.getHotelcode().trim(), "", "", startDate, endDate);
        if (jlResults == null || jlResults.size() == 0) {
            return map;
        }
        Map<String, Roomtype> localRoomMap = new HashMap<String, Roomtype>();
        for (Roomtype r : localRoomList) {
            localRoomMap.put(r.getRoomcode().trim(), r);
        }
        for (JLPriceResult jl : jlResults) {
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getJlroomtype())
                    || "null".equalsIgnoreCase(jl.getJlroomtype().trim())) {
                continue;
            }
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getRatetypeid())
                    || "null".equalsIgnoreCase(jl.getRatetypeid().trim())) {
                continue;
            }
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getStayDate())
                    || "null".equalsIgnoreCase(jl.getStayDate().trim())) {
                continue;
            }
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getPprice()) || "null".equalsIgnoreCase(jl.getPprice().trim())) {
                continue;
            }
            if ("外宾".equals(jl.getRatetype())) {
                continue;
            }
            String jlRoomId = jl.getJlroomtype().trim();
            Roomtype local = localRoomMap.get(jlRoomId);
            if (local == null) {
                continue;
            }
            //日期有重复或同一套餐，多个价格
            String roomAndProd = jlRoomId + "@" + jl.getRatetypeid().trim();
            String roomAndProdAndDate = roomAndProd + "@" + jl.getStayDate().trim();
            String repeatDate = checkRepeatMap.get(roomAndProdAndDate);
            if (!ElongHotelInterfaceUtil.StringIsNull(repeatDate)) {
                repeatMap.put(roomAndProd, true);
                continue;
            }
            else {
                checkRepeatMap.put(roomAndProdAndDate, jl.getStayDate().trim());
            }
            HotelGoodData hd = new HotelGoodData();
            hd.setHotelid(hotel.getId());
            hd.setHotelname(hotel.getName());
            hd.setRoomtypeid(local.getId());
            hd.setRoomtypename(jl.getRoomtype().trim());
            //去哪儿房型 0：大床； 1：双床；2：大/双床；3：三床；5：单人床
            hd.setBedtypeid(getBed(local.getBed()));
            try {
                hd.setBaseprice((long) Double.parseDouble(jl.getPprice()));
            }
            catch (Exception e) {
                hd.setBaseprice(0l);
            }
            if (hd.getBaseprice().longValue() <= 0) {
                continue;
            }
            hd.setProfit(Long.valueOf(addPrice));
            hd.setShijiprice((long) ElongHotelInterfaceUtil.add(hd.getBaseprice().longValue(), addPrice));
            hd.setJlroomtypeid(jlRoomId);
            hd.setJlf(jl.getFangliang());
            hd.setJlft(jl.getRoomstate());
            hd.setJlkeyid(jl.getJlkeyid().trim());
            hd.setJltime(jl.getJltime());
            hd.setAllotmenttype(jl.getAllotmenttype());
            hd.setRatetype(jl.getRatetype());
            hd.setRatetypeid(jl.getRatetypeid().trim());
            hd.setDatenum(jl.getStayDate());
            //最少连住
            try {
                hd.setMinday(Long.parseLong(jl.getMinDay()));
            }
            catch (Exception e) {
                hd.setMinday(1l);
            }
            //提前天数
            try {
                hd.setBeforeday(Long.parseLong(jl.getLeadTime()));
            }
            catch (Exception e) {
                hd.setBeforeday(0l);
            }
            //判断即时确认
            boolean isNowFlag = false;
            //房态 12:Open 13:良好 14:紧张 15:不可超 16:满房
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getAllot()) || "null".equalsIgnoreCase(jl.getAllot().trim())) {
                jl.setAllot("0");//用于判断房量
            }
            String jlRoomStatus = jl.getAllot().trim();
            //Open
            if ("12".equals(jlRoomStatus)) {
                isNowFlag = true;
            }
            if ("16".equals(jlRoomStatus)) {
                hd.setYuliunum(0l);
            }
            else {
                try {
                    hd.setYuliunum(Long.parseLong(jl.getFangliang()));
                }
                catch (Exception e) {
                    hd.setYuliunum(0l);
                }
            }
            if (hd.getYuliunum().longValue() > 0) {
                isNowFlag = true;
            }
            if (!isNowFlag && "1".equals(onlyhaveroom)) {
                continue;
            }
            //0：开房  ；1：关房
            hd.setRoomstatus(isNowFlag ? 0l : 1l);
            if (!isNowFlag) {
                int CloseCount = checkAllClose.containsKey(roomAndProd) ? checkAllClose.get(roomAndProd) : 0;
                CloseCount++;
                checkAllClose.put(roomAndProd, CloseCount);
            }
            //捷旅房态
            hd.setJlft(jlRoomStatus);
            hd.setContractid(hotel.getHotelcode2());
            //深捷旅早餐转去哪儿早餐
            String bf = getBf(jl.getBreakfast());
            hd.setBfcount(Long.parseLong(bf));
            //深捷旅宽带转去哪儿宽带
            hd.setWeb(getWeb(jl.getNetfee()));
            hd.setSorucetype("6");
            hd.setCityid(String.valueOf(hotel.getCityid()));
            //多套餐，用ID区分
            List<HotelGoodData> tempList = map.get(roomAndProd);
            if (tempList == null || tempList.size() == 0) {
                tempList = new ArrayList<HotelGoodData>();
            }
            tempList.add(hd);
            map.put(roomAndProd, tempList);
        }
        if (checkAllClose.size() > 0 && map.size() > 0) {
            for (String key : checkAllClose.keySet()) {
                int CloseCount = checkAllClose.get(key);
                if (CloseCount == catchdays && map.containsKey(key)) {
                    map.remove(key);
                }
            }
        }
        if (repeatMap.size() > 0 && map.size() > 0) {
            for (String key : repeatMap.keySet()) {
                if (map.containsKey(key)) {
                    map.remove(key);
                }
            }
        }
        return map;
    }

    //深捷旅房型转去哪儿房型 0：大床； 1：双床；2：大/双床；5：单人床
    private long getBed(Integer bedid) {
        long bed = 2;//大/双床
        if (bedid == null || bedid == 0) {
            return bed;
        }
        else if (bedid == 1) {
            bed = 5;//单人床
        }
        else if (bedid == 2) {
            bed = 0;//大床
        }
        else if (bedid == 3) {
            bed = 1;//双床
        }
        return bed;
    }

    //深捷旅早餐
    private String getBf(String bf) {
        String ret = "0";//不含早
        if (ElongHotelInterfaceUtil.StringIsNull(bf) || "null".equalsIgnoreCase(bf.trim())) {
            ret = "0";//不含早
        }
        else if (bf.equals("11") || bf.equals("12") || bf.equals("13") || bf.equals("1")) {
            ret = "1";//单早
        }
        else if (bf.equals("21") || bf.equals("22") || bf.equals("23") || bf.equals("2")) {
            ret = "2";//双早
        }
        else if (bf.equals("31") || bf.equals("32") || bf.equals("33") || bf.equals("3")) {
            ret = "3";//三早
        }
        else if (bf.equals("34") || bf.equals("6")) {
            ret = "-1";//含早
        }
        return ret;
    }

    //深捷旅宽带
    private int getWeb(String net) {
        int web = 0;
        if (ElongHotelInterfaceUtil.StringIsNull(net) || "null".equalsIgnoreCase(net.trim())) {
            web = 0;//无
        }
        else if (net.trim().equals("0")) {
            web = 2;//免费
        }
        else if (Integer.parseInt(net.trim()) > 0) {
            web = 3;//收费
        }
        return web;
    }
}
