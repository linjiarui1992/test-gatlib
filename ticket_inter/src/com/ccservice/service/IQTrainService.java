package com.ccservice.service;

public interface IQTrainService {

    /**
     * 作者：邹远超
     * 日期：2014年6月15日
     * 说明：（出票、拒单共用）
     * @param trainorder
     * @return
     */
    public boolean trainIssueOrRefuse(long orderid, int state);

    public boolean trainRefundresult(String qunarordernum, int state, int reason);

    public boolean trainRefundPrice(String qunarordernum, int reason, float refundCash);
}
