package com.ccervice.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.ccservice.qunar.util.ExceptionUtil;

public class FileUtil {
    public List<String> readLog(String filePath) {
        List<String> list = new ArrayList<String>();

        try {
            FileInputStream is = new FileInputStream(filePath);
            InputStreamReader isr = new InputStreamReader(is, "GBK");
            BufferedReader br = new BufferedReader(isr);
            String line;
            int lineInt = 0;
            try {
                while ((line = br.readLine()) != null) {
                    lineInt++;
                    if (line.equals(""))
                        continue;
                    else
                        list.add(line);
                }
            }
            catch (IOException e) {
                ExceptionUtil.writelogByException("FileUtil_IOException", e, "读取一行数据时出错IOException>>>" + filePath
                        + ">>>" + lineInt);
            }
        }
        catch (FileNotFoundException e) {
            ExceptionUtil.writelogByException("FileUtil_FileNotFoundException", e, "文件读取路径错误FileNotFoundException>>>"
                    + filePath);
        }
        catch (UnsupportedEncodingException e) {
            ExceptionUtil.writelogByException("FileUtil_UnsupportedEncodingException", e,
                    "不可识别加密格式UnsupportedEncodingException>>>" + filePath);
        }
        return list;
    }

}
