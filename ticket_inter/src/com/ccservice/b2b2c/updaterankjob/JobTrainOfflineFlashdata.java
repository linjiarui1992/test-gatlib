package com.ccservice.b2b2c.updaterankjob;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;


public class JobTrainOfflineFlashdata implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        WriteLog.write("JobTrainOfflineFlashdata_expr", "begin...");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//        Date as = new Date(new Date().getTime()-24*60*60*1000);
        Date as = new Date();
        String dates=sdf.format(as);
        //所有当天的出票成功的agentid
            String sql1="select distinct AgentId FROM TrainOrderOffline where CreateTime between '"+dates+" 00:00:00' and '"+dates+" 23:59:59' and OrderStatus=2  order by AgentId";
            List listagent = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
            if(listagent.size()>0){
            for(int i=0;i<listagent.size();i++){
                Map map=(Map)listagent.get(i);
                //根据所有的 agentid 查找TrainOrderOfflineRecord 表中的 数据
                String sql2="SELECT distinct FKTrainOrderOfflineId,ResponseTime-DistributionTime as chatime FROM TrainOrderOfflineRecord WHERE ProviderAgentid="+map.get("AgentId").toString()+" AND ResponseTime>=DistributionTime and DistributionTime between '"+dates+" 06:00:00' and  '"+dates+" 23:00:00'";
                List listtime = Server.getInstance().getSystemService().findMapResultBySql(sql2, null);
                int alltime=0;
                String letterSix="";
                String biggerSix="";
                String biggerTen="";
                double successavg=0;
                String allordersyes="";
                String allorders="";
                for(int j=0;j<listtime.size();j++){
                    Map map2=(Map)listtime.get(j);
                    int ti=Integer.parseInt(map2.get("chatime").toString().substring(11, 13))*3600+Integer.parseInt(map2.get("chatime").toString().substring(14, 16))*60+Integer.parseInt(map2.get("chatime").toString().substring(17, 19));
                    alltime+=ti;
                    String sqlavg1="SELECT COUNT(1) as success FROM TrainOrderOfflineRecord WITH (NOLOCK) WHERE ProviderAgentid="+map.get("AgentId").toString()+" and DistributionTime between '"+dates+" 06:00:00' and  '"+dates+" 23:00:00' and (DealResult=1 ) ";
                    List listavg1 = Server.getInstance().getSystemService().findMapResultBySql(sqlavg1, null);
                    Map mapavg1=(Map)listavg1.get(0);
                    String sqlavg2="SELECT COUNT(1) as alls FROM TrainOrderOfflineRecord WHERE ProviderAgentid="+map.get("AgentId").toString()+" and DistributionTime between '"+dates+" 06:00:00' and  '"+dates+" 23:00:00' and (DealResult=1 or DealResult=2) ";
                    List listavg2 = Server.getInstance().getSystemService().findMapResultBySql(sqlavg2, null);
                    Map mapavg2=(Map)listavg2.get(0);
                     allordersyes=mapavg1.get("success").toString();//出票成功
                     allorders=mapavg2.get("alls").toString();
                    float f1=Float.parseFloat(mapavg1.get("success").toString())/Float.parseFloat(mapavg2.get("alls").toString());
                    BigDecimal bd = new BigDecimal(Double.parseDouble(f1+""));  
                     successavg=bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                    if(ti<360){
                        letterSix+=map2.get("FKTrainOrderOfflineId").toString()+",";
                    }else if(ti>360 && ti<600){
                        biggerSix+=map2.get("FKTrainOrderOfflineId").toString()+",";
                    }else if(ti>600){
                        biggerTen+=map2.get("FKTrainOrderOfflineId").toString()+",";
                    }
                     
                }
                //失败订单总时间
                int alltimes=0;
                String sqlalltimes="SELECT distinct FKTrainOrderOfflineId,ProviderAgentid,DistributionTime FROM TrainOrderOfflineRecord WHERE ProviderAgentid="+map.get("AgentId").toString()+" AND DealResult=2 and DistributionTime between '"+dates+" 00:00:00' and  '"+dates+" 23:59:00'";
                List listalltimes = Server.getInstance().getSystemService().findMapResultBySql(sqlalltimes, null);
                for(int j=0;j<listalltimes.size();j++){
                    Map map3=(Map)listalltimes.get(j);
                    String dateStart=map3.get("DistributionTime").toString();
                    String fensql="SELECT DistributionTime from TrainOrderOfflineRecord where DealResult=0 and FKTrainOrderOfflineId="+map3.get("FKTrainOrderOfflineId").toString() +" and ProviderAgentid="+map3.get("ProviderAgentid").toString();
                    List listalltime = Server.getInstance().getSystemService().findMapResultBySql(fensql, null);
                    Map maptim=new HashMap();
                    if(listalltime.size()==0){
                        String fensql1="SELECT DistributionTime from TrainOrderOfflineRecord where DealResult=4 and FKTrainOrderOfflineId="+map3.get("FKTrainOrderOfflineId").toString() +" and ProviderAgentid="+map3.get("ProviderAgentid").toString();
                        List listalltime1 = Server.getInstance().getSystemService().findMapResultBySql(fensql1, null);
                         maptim=(Map)listalltime1.get(0);
                    }else{
                        maptim=(Map)listalltime.get(0);
                    }
                    String dateStop=maptim.get("DistributionTime").toString();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d1 = null;
                    Date d2 = null;
                        try {
                            d1 = format.parse(dateStart);
                            d2 = format.parse(dateStop);
                        }
                        catch (ParseException e) {
                            e.printStackTrace();
                        }
                        
                        //毫秒ms
                        long diff = d1.getTime() - d2.getTime();
                        alltimes+=diff/1000;
                }
                
                int letterSixSum=0;
                int biggerSixSum=0;
                int biggerTenSum=0;
                
                if(letterSix.contains(",")){
                    letterSix=letterSix.substring(0, letterSix.length()-1);
                    String sql3="select count(1) as ordercount from trainorderoffline where id in ("+letterSix+")";
                    List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                    Map mapcount=(Map)listcount.get(0);
                    letterSixSum=Integer.parseInt(mapcount.get("ordercount").toString());
                }else{
                    letterSixSum=0;
                }
                if(biggerSix.contains(",")){
                    biggerSix=biggerSix.substring(0, biggerSix.length()-1);
                    String sql3="select count(1) as ordercount from trainorderoffline where id in ("+biggerSix+")";
                    List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                    Map mapcount=(Map)listcount.get(0);
                    biggerSixSum=Integer.parseInt(mapcount.get("ordercount").toString());
                }else{
                    biggerSixSum=0;
                } 
                if(biggerTen.contains(",")){
                    biggerTen=biggerTen.substring(0, biggerTen.length()-1);
                    String sql3="select count(1) as ordercount from trainorderoffline where id in ("+biggerTen+")";
                    List listcount = Server.getInstance().getSystemService().findMapResultBySql(sql3, null);
                    Map mapcount=(Map)listcount.get(0);
                    biggerTenSum=Integer.parseInt(mapcount.get("ordercount").toString());
                }else{
                    biggerTenSum=0;
                }
                String insuresql="select * from TrainOrderOfflineStatistics where agentId="+map.get("AgentId").toString()+" and staticDate='"+dates+"'";
                List listinsure = Server.getInstance().getSystemService().findMapResultBySql(insuresql, null);
                if(listinsure.size()==0){
                    String sqlInsert="insert into TrainOrderOfflineStatistics(agentId,finishTime,avgAllTime,finishAvg,letterSix,biggerSix,biggerTen,finishSum,allOrderSum,staticDate,ranking) "
                            + " values("+map.get("AgentId").toString()+","+alltime/listtime.size()+","+(alltimes+alltime)/Integer.parseInt(allorders)+","+(successavg*100)+","+letterSixSum+","+biggerSixSum+","+biggerTenSum+","+(letterSixSum+biggerSixSum+biggerTenSum)+","+allorders+",'"+dates+"',0)";
                    WriteLog.write("statistics_1_sqlInsert_"+dates, "sqlInsert="+sqlInsert);
                    Server.getInstance().getSystemService().excuteAdvertisementBySql(sqlInsert);
                }else{
                    String updatesql="update TrainOrderOfflineStatistics set finishTime="+alltime/listtime.size()+",avgAllTime="+(alltimes+alltime)/Integer.parseInt(allorders)+",finishAvg="+(successavg*100)+",letterSix="+letterSixSum+",biggerSix="+biggerSixSum+",biggerTen="+biggerTenSum+",finishSum="+(letterSixSum+biggerSixSum+biggerTenSum)+",allOrderSum="+allorders+" where staticDate='"+dates+"' and agentId="+map.get("AgentId").toString();
                    WriteLog.write("statistics_1_sqlNoInsert_"+dates, "AgentId="+map.get("AgentId").toString()+";avgtime="+alltime/listtime.size()+";allavgs="+(alltimes+alltime)/Integer.parseInt(allorders)+";successavg="+successavg+";letterSixSum="+letterSixSum+";biggerSixSum="+biggerSixSum+";biggerTenSum="+biggerTenSum+";all="+(letterSixSum+biggerSixSum+biggerTenSum)+";allorders="+allorders+";allordersyes="+allordersyes);
                    WriteLog.write("statistics_1_sqlNoInsert_"+dates, "updatesql="+updatesql);
                    Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
                }
//                System.out.println("AgentId="+map.get("AgentId").toString()+";avgtime="+alltime/listtime.size()+";allavgs="+(alltimes+alltime)/Integer.parseInt(allorders)+";successavg="+successavg+";letterSixSum="+letterSixSum+";biggerSixSum="+biggerSixSum+";biggerTenSum="+biggerTenSum+";all="+(letterSixSum+biggerSixSum+biggerTenSum)+";allorders="+allorders+";allordersyes="+allordersyes);
                
                WriteLog.write("statistics_1_"+dates, "AgentId="+map.get("AgentId").toString()+";avgtime="+alltime/listtime.size()+";alltime="+alltime+";alltimes="+(alltimes)+";allavgs="+(alltimes+alltime)/Integer.parseInt(allorders)+";successavg="+successavg+";letterSixSum="+letterSixSum+";biggerSixSum="+biggerSixSum+";biggerTenSum="+biggerTenSum+";all="+(letterSixSum+biggerSixSum+biggerTenSum)+";allorders="+allorders+";allordersyes="+allordersyes);
            }
            } 
        
    }
    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        System.out.println("开始UpdateData_StartJobTrainOfflineFlashData_expr="+expr);
        WriteLog.write("JobTrainOfflineFlashdata_expr", "expr="+expr);
        JobDetail jobDetail = new JobDetail("JobTrainOfflineFlashdata", "JobTrainOfflineFlashdataGroup",
                JobTrainOfflineFlashdata.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobTrainOfflineFlashdata","JobTrainOfflineFlashdataGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

}
