package com.ccservice.service;

import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.axis2.AxisFault;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.ben.Orderchange;

public interface IRateService {

    /**
     * 申请退费票
     * 
     * @param order
     * @return
     */

    public String ChangeOrder(Orderinfo order, Orderchange orderchange);

    /**
     * 创建第三方订单
     * 
     * @param order
     * @param sinfo
     * @param listPassenger
     * @return
     * @time 2014年9月1日 上午10:04:51
     * @author chendong
     */
    public String CreateOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger);

    /**
     * 创建易订行婴儿订单
     * 
     * @param order
     * @return
     */
    public String CreateBabyOrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger);

    /**
     * 创建B2C异地订单
     */
    public Orderinfo CreateB2COrder(Orderinfo order, Segmentinfo sinfo, List<Passenger> list);

    /**
     * 异地自动支付
     * 
     * @param order
     * @return
     */
    public String AutoPay(Orderinfo order);

    public String orderchangenotify(long changeid, String memo);

    /**
     * 
     * 收银台模式
     * 
     * @param order
     * @return
     */
    public String OrderPay(Orderinfo order);

    /**
     * 根据航班号查询政策
     * @param scity	出发三字码
     * @param ecity	到达三字码
     * @param sdate	出发时间
     * @param flightnumber 航班号
     * @param cabin 舱位
     * @param type 查询类型1航班列表2下单页面
     * @return
     */
    public List<Zrate> FindZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String traveltype, String type);

    /**
     * 查找今日政策单条
     * 
     * @return
     */
    public Zrate FindRateOne(String rateno);

    /**
     * @param orderid
     * @param outid
     * @param fromcity
     * @param tocity
     * @param aircode
     * @return
     */
    public Zrate getEightnewZrate(long orderid, String outid, String fromcity, String tocity, String aircode);

    /**
     * 根据PNR查找最优政策。
     * 
     * @param PNR
     * @param special
     *            0：普通政策。1：特殊政策。3：所有政策。
     * @return
     */
    public Zrate getBestZrateByPNR(String PNR, int special);

    /**
     * @param order
     * @param sinfo
     * @param Passenger
     * @param zratecount 返回政策的数量
     * @return Zrate 下单时候匹配最优政策
     */
    public List<Zrate> FindZrateByFlight(Orderinfo order, List<Segmentinfo> listSinfo, List<Passenger> listPassenger,
            int zratecount) throws ParseException, SQLException;

    public Zrate FindZrateByIdTo51Book(String zrateid) throws NoSuchAlgorithmException, AxisFault, RemoteException,
            SQLException, ParseException;

    public String PayOrderUrlById(String orderid);

    public Zrate FindOneZrateTo51Book(String zrateoutid);

    /**
     * 根据订单号和供应商的agentid获取票号
     * @param orderno
     * @param angid
     * @return 高志勇|刘珍珍|张妍|^324-2348850112|324-2348850113|324-2348850114|
     */
    public String FindOutOrderInfoByOrderNo(String orderno, String angid);

    public List SeachFlight(String scity, String ecity, String compname, String depdate, String type);

    public List SeachFiveoneFlight(String scity, String ecity, String compname, String depdate, String type);

    /**
     * 航班查询（含票面价、返佣）
     * 
     * @param orgAirportCode 出发机场三字码
     * @param dstAirportCode 抵达城市三字码
     * @param date 起飞日期
     *            格式:“yyyy-MM-dd”
     * @param airlineCode 航空公司三字码
     * @param onlyAvailableSeat 只返回可用舱位
     * @param onlyNormalCommision 是否包括特殊政策
     * @param onlyOnWorkingCommision 是否只返回在工作时间内政策
     * @param onlySelfPNR 是否可更换PNR出票
     * @return
     */
    public List getAvailableFlightWithPriceAndCommision(String orgAirportCode, String dstAirportCode, String date,
            String airlineCode, int onlyAvailableSeat, int onlyNormalCommision, int onlyOnWorkingCommision,
            int onlySelfPNR);

    public String TuiFeiOrder(Orderinfo orderinfo, List<Passenger> listPassenger, String WhyId, Orderchange orderchange);

    /**
     * 根据PNR的rt内容获取政策信息。注意：此方法是必须根据PNR的rt和pat信息获取！
     * @param order
     * @param sinfo
     * @param listPassenger
     * @return
     */
    public List<Zrate> findAllPlantBestZrate(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger);

    /**
     * 退票退款通知：接口方退票退款成功，通知到易订行。
     * @return
     */
    public String ticketrefundNotify(long orderid);

}
