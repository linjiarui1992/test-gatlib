package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 来订单后在队列里获取到那个火车票订单的id然后下单
 * 
 * @time 2015年1月8日 下午2:10:34
 * @author chendong
 */
public class Create12306orderServerlet extends HttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = 1234687798432167L;

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ用户名

    @Override
    public void init() throws ServletException {
        super.init();
        String wethernotice = this.getInitParameter("wethernotice");
        if (wethernotice != null && wethernotice.equals("1")) {
            System.out.println("同程异步来订单MQ通知：开启");
            mqaddress = this.getInitParameter("mqaddress");
            mqusername = this.getInitParameter("mqusername");
            monitorMQmessage();
        }
    }

    public void monitorMQmessage() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(this.mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new ActiveMQQueue(mqusername);
            MessageConsumer consumer = session.createConsumer(destination);
            conn.start();
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        String orderNoticeResult = ((TextMessage) message).getText();//从MQ中接收到得信息
                        WriteLog.write("异步队列下单订单id", "从MQ中接收的需要下单到12306的订单id:[" + orderNoticeResult + "]");
                        System.out.println("从MQ中接收的需要下单到12306的订单id:[" + orderNoticeResult + "]");
                        Long trainorderid = Long.parseLong(orderNoticeResult);
                        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
                        if (orderNoticeResult != null && !orderNoticeResult.equals("")) {
                            //                            parseOrderNoticeResult();
                            //                            if (orderInfo != null) {
                            //                                afterReceivingMessage();
                            //                                //执行完一次通知后，对orderInfo对象进行置空，如果不进行置空，对象中的保留值会影响其他的订单
                            //                                orderInfo = null;
                            //                                reason = "";//对供应商取消订单的原因进行置空，以免影响下一个订单
                            //                            }
                        }
                    }
                    catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
