package com.ccservice.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.ccservice.b2b2c.policy.Airutil;
import com.ccservice.b2b2c.policy.FiveoneBookutil;
import com.ccservice.b2b2c.policy.FiveoneSearch;
import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.b2b2c.policy.ben.FiveonBook;
import com.ccservice.b2b2c.policy.ben.JinRiBook;
import com.ccservice.inter.job.WriteLog;

public class SearchFlightService implements ISearchFlightService {

	private static Log log = LogFactory.getLog(SearchFlightService.class);

	JinRiBook jinRiBook= new JinRiBook();
	//BaQianYiBook baQianYiBook =BaQianYiBook.getInstance();
	BaQianYiBook baQianYiBook =new BaQianYiBook();
	FiveonBook fiveonBook = new FiveonBook();
	WriteLog writeLog = new WriteLog();
	
	private  String JRURL =jinRiBook.getJRURL();
	private  String JRUSER = jinRiBook.getJRUSER();
	private  String JRUSER_xiaji = jinRiBook.getJRUSER_xiaji();
	public   String JRKey = jinRiBook.getJRKey();
	private Airutil ariutil = new Airutil();
	private FiveoneBookutil book = new FiveoneBookutil();
	private FiveoneSearch fiveoneSearch = new FiveoneSearch();
	
	
	public List	SeachFiveoneFlight(String scity,String ecity,String compname,String depdate,String type){
		
		//List list=fiveoneSearch.SeachFiveoneFlight(scity, ecity, compname, depdate, type);
		List list=book.SeachFlight(scity, ecity, compname, depdate, type);
		return list;
	}


	public static Log getLog() {
		return log;
	}


	public static void setLog(Log log) {
		SearchFlightService.log = log;
	}


	public JinRiBook getJinRiBook() {
		return jinRiBook;
	}


	public void setJinRiBook(JinRiBook jinRiBook) {
		this.jinRiBook = jinRiBook;
	}


	public BaQianYiBook getBaQianYiBook() {
		return baQianYiBook;
	}


	public void setBaQianYiBook(BaQianYiBook baQianYiBook) {
		this.baQianYiBook = baQianYiBook;
	}


	public FiveonBook getFiveonBook() {
		return fiveonBook;
	}


	public void setFiveonBook(FiveonBook fiveonBook) {
		this.fiveonBook = fiveonBook;
	}


	public WriteLog getWriteLog() {
		return writeLog;
	}


	public void setWriteLog(WriteLog writeLog) {
		this.writeLog = writeLog;
	}


	public String getJRURL() {
		return JRURL;
	}


	public void setJRURL(String jrurl) {
		JRURL = jrurl;
	}


	public String getJRUSER() {
		return JRUSER;
	}


	public void setJRUSER(String jruser) {
		JRUSER = jruser;
	}


	public String getJRUSER_xiaji() {
		return JRUSER_xiaji;
	}


	public void setJRUSER_xiaji(String jruser_xiaji) {
		JRUSER_xiaji = jruser_xiaji;
	}


	public String getJRKey() {
		return JRKey;
	}


	public void setJRKey(String key) {
		JRKey = key;
	}


	public Airutil getAriutil() {
		return ariutil;
	}


	public void setAriutil(Airutil ariutil) {
		this.ariutil = ariutil;
	}


	public FiveoneBookutil getBook() {
		return book;
	}


	public void setBook(FiveoneBookutil book) {
		this.book = book;
	}



	
}
