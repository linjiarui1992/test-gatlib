
package com.yeexing.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adultPnr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childPnr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adultPlcId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childPlcId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="adultIbePrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="childIbePrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="out_orderId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="disc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extReward" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="appUserName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "adultPnr",
    "childPnr",
    "adultPlcId",
    "childPlcId",
    "adultIbePrice",
    "childIbePrice",
    "outOrderId",
    "disc",
    "extReward",
    "appUserName",
    "sign"
})
@XmlRootElement(name = "PnrBookWithChild")
public class PnrBookWithChild {

    @XmlElement(required = true, nillable = true)
    protected String userName;
    @XmlElement(required = true, nillable = true)
    protected String adultPnr;
    @XmlElement(required = true, nillable = true)
    protected String childPnr;
    @XmlElement(required = true, nillable = true)
    protected String adultPlcId;
    @XmlElement(required = true, nillable = true)
    protected String childPlcId;
    @XmlElement(required = true, nillable = true)
    protected String adultIbePrice;
    @XmlElement(required = true, nillable = true)
    protected String childIbePrice;
    @XmlElement(name = "out_orderId", required = true, nillable = true)
    protected String outOrderId;
    @XmlElement(required = true, nillable = true)
    protected String disc;
    @XmlElement(required = true, nillable = true)
    protected String extReward;
    @XmlElement(required = true, nillable = true)
    protected String appUserName;
    @XmlElement(required = true, nillable = true)
    protected String sign;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the adultPnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdultPnr() {
        return adultPnr;
    }

    /**
     * Sets the value of the adultPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdultPnr(String value) {
        this.adultPnr = value;
    }

    /**
     * Gets the value of the childPnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildPnr() {
        return childPnr;
    }

    /**
     * Sets the value of the childPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildPnr(String value) {
        this.childPnr = value;
    }

    /**
     * Gets the value of the adultPlcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdultPlcId() {
        return adultPlcId;
    }

    /**
     * Sets the value of the adultPlcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdultPlcId(String value) {
        this.adultPlcId = value;
    }

    /**
     * Gets the value of the childPlcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildPlcId() {
        return childPlcId;
    }

    /**
     * Sets the value of the childPlcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildPlcId(String value) {
        this.childPlcId = value;
    }

    /**
     * Gets the value of the adultIbePrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdultIbePrice() {
        return adultIbePrice;
    }

    /**
     * Sets the value of the adultIbePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdultIbePrice(String value) {
        this.adultIbePrice = value;
    }

    /**
     * Gets the value of the childIbePrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildIbePrice() {
        return childIbePrice;
    }

    /**
     * Sets the value of the childIbePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildIbePrice(String value) {
        this.childIbePrice = value;
    }

    /**
     * Gets the value of the outOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutOrderId() {
        return outOrderId;
    }

    /**
     * Sets the value of the outOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutOrderId(String value) {
        this.outOrderId = value;
    }

    /**
     * Gets the value of the disc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisc() {
        return disc;
    }

    /**
     * Sets the value of the disc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisc(String value) {
        this.disc = value;
    }

    /**
     * Gets the value of the extReward property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtReward() {
        return extReward;
    }

    /**
     * Sets the value of the extReward property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtReward(String value) {
        this.extReward = value;
    }

    /**
     * Gets the value of the appUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppUserName() {
        return appUserName;
    }

    /**
     * Sets the value of the appUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppUserName(String value) {
        this.appUserName = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

}
