package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author 贾建磊
 * 
 */
public class KkkkBook {

    // 以下是kkkk接口需要参数
    private String kkkkUnitID = ""; // kkkk的单位id

    private String kkkkSaftNo = ""; // kkkk的安全码

    private String kkkkStaus = "0"; // kkkk的账号状态 0 禁用 1 启用

    private String kkkkPayUrl = ""; // kkkk的支付页面

    private String kkkkLinkName = ""; // 联系人姓名

    private String kkkkLinkMobile = "";// 联系人手机

    public KkkkBook() {
        List<Eaccount> listEaccountKkkk = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(9);
        if (e != null && e.getName() != null) {
            listEaccountKkkk.add(e);
        }
        if (listEaccountKkkk.size() > 0) {
            kkkkUnitID = listEaccountKkkk.get(0).getKeystr();
            kkkkSaftNo = listEaccountKkkk.get(0).getPassword();
            kkkkStaus = listEaccountKkkk.get(0).getState();
            kkkkPayUrl = listEaccountKkkk.get(0).getPayurl();
            kkkkLinkName = listEaccountKkkk.get(0).getCreateuser();
            kkkkLinkMobile = listEaccountKkkk.get(0).getUrl();
        }
        else {
            kkkkStaus = "0";
            System.out.println("NO-KKKK");
        }
    }

    public String getKkkkUnitID() {
        return kkkkUnitID;
    }

    public void setKkkkUnitID(String kkkkUnitID) {
        this.kkkkUnitID = kkkkUnitID;
    }

    public String getKkkkSaftNo() {
        return kkkkSaftNo;
    }

    public void setKkkkSaftNo(String kkkkSaftNo) {
        this.kkkkSaftNo = kkkkSaftNo;
    }

    public String getKkkkStaus() {
        return kkkkStaus;
    }

    public void setKkkkStaus(String kkkkStaus) {
        this.kkkkStaus = kkkkStaus;
    }

    public String getKkkkPayUrl() {
        return kkkkPayUrl;
    }

    public void setKkkkPayUrl(String kkkkPayUrl) {
        this.kkkkPayUrl = kkkkPayUrl;
    }

    public String getKkkkLinkMobile() {
        return kkkkLinkMobile;
    }

    public void setKkkkLinkMobile(String kkkkLinkMobile) {
        this.kkkkLinkMobile = kkkkLinkMobile;
    }

    public String getKkkkLinkName() {
        return kkkkLinkName;
    }

    public void setKkkkLinkName(String kkkkLinkName) {
        this.kkkkLinkName = kkkkLinkName;
    }
}
