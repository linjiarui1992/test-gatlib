/**
 * 
 */
package com.ccservice.inter.job.train.account;

import java.util.Map;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;

/**
 * 手动跑账号
 * @time 2015年9月30日 下午1:50:14
 * @author chendong
 */
public class Job12306AccountThread extends Thread {
    public static void main(String[] args) throws Exception {
        System.out.println(JobTrainUtil.checkLoginName("zhaofan2uyzwzc1l"));
    }

    Map map;

    int i;

    ISystemService isystemservice;

    public Job12306AccountThread(Map map, ISystemService isystemservice, int i) {
        super();
        this.map = map;
        this.isystemservice = isystemservice;
        this.i = i;
    }

    @Override
    public void run() {
        Customeruser customeruser = new Customeruser();
        String loginname = map.get("C_LOGINNAME").toString();
        String password = map.get("C_LOGPASSWORD").toString();
        Long id = Long.parseLong(map.get("ID").toString());
        customeruser.setId(id);
        customeruser.setLoginname(loginname);
        customeruser.setLogpassword(password);
        String repUrl = "";
        boolean isExist = getisExist(loginname);
        if (isExist) {//12306存在的话去核验
            System.out.println(this.i + ":" + loginname + ":" + id + ":存在:" + isExist);
            WriteLog.write("cunzai", id + "");
            //String cookieString = JobTrainUtil.getCookie(repUrl, loginname, password);
            isystemservice.excuteGiftBySql("update T_CUSTOMERUSER set C_ISENABLE=118 where ID=" + id);//118注册完的新账号，待登录验证 [临时状态]
        }
        else {
            //12306不存在 修改状态    12注册完邮箱账号拿完乘客等待去12306注册信息
            //把这个账号的isenable改为12
            System.out.println(this.i + ":" + loginname + ":" + id + ":不存在:" + isExist);
            isystemservice.excuteGiftBySql("update T_CUSTOMERUSER set C_ISENABLE=16 where ID=" + id);//16登录名不存在
        }
    }

    private boolean getisExist(String loginname) {
        boolean isExist = false;
        for (int i = 0; i < 10; i++) {
            try {
                isExist = JobTrainUtil.checkLoginName(loginname);
                break;
            }
            catch (Exception e) {
                continue;
            }
        }
        return isExist;
    }
}
