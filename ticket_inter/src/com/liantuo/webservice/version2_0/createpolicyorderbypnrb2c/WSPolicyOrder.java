
package com.liantuo.webservice.version2_0.createpolicyorderbypnrb2c;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSPolicyOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSPolicyOrder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="commisionInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flightInfoList" type="{http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com}ArrayOfFlightInfo" minOccurs="0"/>
 *         &lt;element name="gmtCreated" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="gmtTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="increaseSystemCharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="passengerList" type="{http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com}ArrayOfWSPassenger" minOccurs="0"/>
 *         &lt;element name="paymentInfo" type="{http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com}PaymentInfo" minOccurs="0"/>
 *         &lt;element name="pnrNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pnrTxt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="policyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sequenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSPolicyOrder", propOrder = {
    "commisionInfo",
    "flightInfoList",
    "gmtCreated",
    "gmtTime",
    "increaseSystemCharge",
    "param1",
    "param2",
    "param3",
    "passengerList",
    "paymentInfo",
    "pnrNo",
    "pnrTxt",
    "policyId",
    "sequenceNo",
    "status"
})
public class WSPolicyOrder {

    @XmlElementRef(name = "commisionInfo", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> commisionInfo;
    @XmlElementRef(name = "flightInfoList", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<ArrayOfFlightInfo> flightInfoList;
    protected XMLGregorianCalendar gmtCreated;
    protected XMLGregorianCalendar gmtTime;
    @XmlElementRef(name = "increaseSystemCharge", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> increaseSystemCharge;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "param2", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param2;
    @XmlElementRef(name = "param3", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param3;
    @XmlElementRef(name = "passengerList", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<ArrayOfWSPassenger> passengerList;
    @XmlElementRef(name = "paymentInfo", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<PaymentInfo> paymentInfo;
    @XmlElementRef(name = "pnrNo", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> pnrNo;
    @XmlElementRef(name = "pnrTxt", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> pnrTxt;
    protected Integer policyId;
    @XmlElementRef(name = "sequenceNo", namespace = "http://createpolicyorderbypnrb2c.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> sequenceNo;
    protected Integer status;

    /**
     * Gets the value of the commisionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCommisionInfo() {
        return commisionInfo;
    }

    /**
     * Sets the value of the commisionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCommisionInfo(JAXBElement<String> value) {
        this.commisionInfo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the flightInfoList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFlightInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFlightInfo> getFlightInfoList() {
        return flightInfoList;
    }

    /**
     * Sets the value of the flightInfoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFlightInfo }{@code >}
     *     
     */
    public void setFlightInfoList(JAXBElement<ArrayOfFlightInfo> value) {
        this.flightInfoList = ((JAXBElement<ArrayOfFlightInfo> ) value);
    }

    /**
     * Gets the value of the gmtCreated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGmtCreated() {
        return gmtCreated;
    }

    /**
     * Sets the value of the gmtCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGmtCreated(XMLGregorianCalendar value) {
        this.gmtCreated = value;
    }

    /**
     * Gets the value of the gmtTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGmtTime() {
        return gmtTime;
    }

    /**
     * Sets the value of the gmtTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGmtTime(XMLGregorianCalendar value) {
        this.gmtTime = value;
    }

    /**
     * Gets the value of the increaseSystemCharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIncreaseSystemCharge() {
        return increaseSystemCharge;
    }

    /**
     * Sets the value of the increaseSystemCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIncreaseSystemCharge(JAXBElement<String> value) {
        this.increaseSystemCharge = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam2() {
        return param2;
    }

    /**
     * Sets the value of the param2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam2(JAXBElement<String> value) {
        this.param2 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam3() {
        return param3;
    }

    /**
     * Sets the value of the param3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam3(JAXBElement<String> value) {
        this.param3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the passengerList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfWSPassenger }{@code >}
     *     
     */
    public JAXBElement<ArrayOfWSPassenger> getPassengerList() {
        return passengerList;
    }

    /**
     * Sets the value of the passengerList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfWSPassenger }{@code >}
     *     
     */
    public void setPassengerList(JAXBElement<ArrayOfWSPassenger> value) {
        this.passengerList = ((JAXBElement<ArrayOfWSPassenger> ) value);
    }

    /**
     * Gets the value of the paymentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PaymentInfo }{@code >}
     *     
     */
    public JAXBElement<PaymentInfo> getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * Sets the value of the paymentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PaymentInfo }{@code >}
     *     
     */
    public void setPaymentInfo(JAXBElement<PaymentInfo> value) {
        this.paymentInfo = ((JAXBElement<PaymentInfo> ) value);
    }

    /**
     * Gets the value of the pnrNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPnrNo() {
        return pnrNo;
    }

    /**
     * Sets the value of the pnrNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPnrNo(JAXBElement<String> value) {
        this.pnrNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the pnrTxt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPnrTxt() {
        return pnrTxt;
    }

    /**
     * Sets the value of the pnrTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPnrTxt(JAXBElement<String> value) {
        this.pnrTxt = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the policyId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPolicyId(Integer value) {
        this.policyId = value;
    }

    /**
     * Gets the value of the sequenceNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSequenceNo() {
        return sequenceNo;
    }

    /**
     * Sets the value of the sequenceNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSequenceNo(JAXBElement<String> value) {
        this.sequenceNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatus(Integer value) {
        this.status = value;
    }

}
