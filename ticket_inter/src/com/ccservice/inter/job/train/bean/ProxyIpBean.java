/**
 * 
 */
package com.ccservice.inter.job.train.bean;

/**
 * 
 * @time 2015年8月28日 下午4:45:04
 * @author chendong
 */
public class ProxyIpBean {
    String proxyHost;

    String proxyPort;

    int status;

    Long lastUseTime;

    Integer wlfmCount;

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public Long getLastUseTime() {
        return lastUseTime;
    }

    public void setLastUseTime(Long lastUseTime) {
        this.lastUseTime = lastUseTime;
    }

    public Integer getWlfmCount() {
        return wlfmCount;
    }

    public void setWlfmCount(Integer wlfmCount) {
        this.wlfmCount = wlfmCount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
