package com.ccservice.inter.rabbitmq;

import java.io.IOException;
//import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
//import java.util.concurrent.TimeoutException;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.time.TimeUtil;
//import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;
//import com.ccservice.rabbitmq.util.RabbitMQCustomer;
import com.ccservice.train.qunar.TrainorderDeduction;
//import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.IConsumer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.RabbitMQDefaultCustomer;
import com.ccservice.util.mq.util_mq.rabbitmq.customer.Monitoring.RabbitMQCustomerMonitoringUtil;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * 扣款消费者优化     加入短信提醒
 * 
 * @author 之至
 * @time 2016年12月20日 下午1:05:32
 */
public class RabbitQueueConsumerTrainDeductionV3 extends RabbitMQDefaultCustomer implements IConsumer{//extends RabbitMQCustomer {

    private String strTypeString = "扣款消費者";

    private String queueNameString;

    public RabbitQueueConsumerTrainDeductionV3(String queueNameString, int type) throws IOException {
//        super(queueNameString, type);
//        this.queueNameString = queueNameString;
//        this.type = type;
    }
    
    /**
     * 
     * @param name
     * @param pingtaiType
     * @param host
     * @param username
     * @param password
     * @throws IOException
     */
    public RabbitQueueConsumerTrainDeductionV3(String name, String pingtaiType, String host, String username, String password) throws IOException {
        super(name, pingtaiType, host, username, password);
    }

    private long orderid;
    private int type;
    
    public long tempTime;
    public String logName = this.getClass().getSimpleName();
    @Override
    public void run() {
        UUID uuid = UUID.randomUUID(); 
        CustomerName = queueNameString + ":" + uuid.toString();//消费者名字随机生成的
        this.logName = this.getClass().getSimpleName();
        try {
            initCounnection();//初始化链接 【第一步】:获取连接 【第二步】:初始化channel 【第三步】:初始化consumer,并连接到队列
        }
        catch (IOException e1) { 
            e1.printStackTrace();
        }
        while (true) {//循环获取消息
            if(!super.connection.isOpen()){//如果连接已经关闭了就关闭
                willClose = true;
                WriteLog.write(this.logName + "-Exception", "连接关了");
            }
            else {
                this.tempTime = System.currentTimeMillis();
                if (!isCanOrdering(new Date())) {//是否可以下单 
                    try {
                        sleep(1000L);//如果不能下单休息1秒
                    }
                    catch (Exception e) {
                    }
                    continue;
                }
                String msg = "";
                String orderNoticeResult = "";
                try {
                    msg = getMsgNew();//【第四步】:获取消息
                    orderNoticeResult = msg;//消息内容
                    System.out.println(TimeUtil.gettodaydate(5) + ":接收到消息:" + orderNoticeResult);
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + orderNoticeResult + "】");
                    willClose = processTheMessage(orderNoticeResult);//【这一步是进行业务逻辑的方法】去下单的方法挪到了一个方法里了【真正下单的方法返回是否释放该消费者】
                    WriteLog.write(this.logName, this.tempTime + ":" + ":【" + willClose + "】");
                    AckMsg();//【第五步】:确认消息，已经收到
                }
                catch (ShutdownSignalException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (ConsumerCancelledException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    ExceptionUtil.writelogByException(this.logName + "-Exception", e, "run=" + orderNoticeResult);
                    willClose = true;
                }
            }
            if (willClose) {//是否即将关闭，这个地方是用来控制消费者数量的
                try {
                    closeMethod(CustomerName);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    @SuppressWarnings("rawtypes")
    public void runBack() {
        try {
            //手机号组
            String mobilePhoneStrings = "";
            System.out.println("扣款队列  接收 ：————————>开启");
            while (true) {
                System.out.println("新一轮获取消息==");
                //获取消息
                String orderNoticeResult = getNewMessageString(queueNameString);
                DeductionMethod(orderNoticeResult);//具体的业务逻辑
                //确认消息已经收到.必须
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                //同程扣款编号   注意配置文件的名称！！！！！！！！！！！！！！！！！
                String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");
                //平台
                String pingTaiType = PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties");
                List list = Server.getInstance().getSystemService()
                        .findMapResultBySql(" select Status from TrainPingTai with(nolock)  where PingTai ="
                                + pingTaiType + " and type =" + kt110, null);
                if (list.size() > 0) {
                    Map map3 = (Map) list.get(0);
                    String StatusString = map3.containsKey("Status") ? map3.get("Status").toString() : "";
                    if (!"".equals(StatusString) && "0".equals(StatusString)) {
                        //之前的逻辑先注释了 com.ccservice.rabbitmq.util.RabbitMQCustomer继承这个类之后可以放开，要不老报错，这里先保留如果修改后没问题，在删除。chendong 2017年3月10日18:03:43
//                        if (Mapcustomers.get(rabbitMQConnectsBean.getKey()).getKey()
//                                .equals(rabbitMQConnectsBean.getKey())) {
//                            Mapcustomers.remove(rabbitMQConnectsBean.getKey());
//                            for (int i = 0; i < ListcustomerNames.size(); i++) {
//                                if (rabbitMQConnectsBean.getKey().equals(ListcustomerNames.get(i))) {
//                                    ListcustomerNames.remove(i);
//                                }
//                            }
//                        }
//                        channel.close();
//                        connection.close();
//                        System.out.println("扣款队列  接收 ：————————>已关闭");
//                        if (Mapcustomers.size() == 0 || ListcustomerNames.size() == 0) {
//                            try {//發短信
//                                List list1 = Server.getInstance().getSystemService().findMapResultBySql(
//                                        " select ReMark from TrainPingTai with(nolock)  where PingTai =99 and type =1",
//                                        null);
//                                if (list1.size() > 0) {
//                                    Map map1 = (Map) list1.get(0);
//                                    mobilePhoneStrings = map1.containsKey("ReMark") ? map1.get("ReMark").toString()
//                                            : "";
//                                    if (!"".equals(mobilePhoneStrings)) {
//                                        String[] dsts = mobilePhoneStrings.split(",");
//                                        for (int i = 0; i < dsts.length; i++) {
//                                            String dst = dsts[i];
//                                            String msg = URLEncoder.encode(strTypeString + "炸啦！！！！！！！！！！！！！！！", "GBK");
//                                            SendPostandGet.submitGet(PublicConnectionInfos.SMSURL + "?name="
//                                                    + PublicConnectionInfos.SMSNAME + "&pwd="
//                                                    + PublicConnectionInfos.SMSPWD + "&dst=" + dst + "&msg=" + msg
//                                                    + "&txt=ccdx", "GBK");
//                                        }
//                                    }
//                                }
//                            }
//                            catch (Exception e) {
//                                e.printStackTrace();
//                                WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception",
//                                        e.fillInStackTrace().toString());
//                            }
//                        }
//                        break;
                        
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", e.getMessage());
        }
        catch (ShutdownSignalException e) {
            e.printStackTrace();
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", e.getMessage());
        }
        catch (ConsumerCancelledException e) {
            e.printStackTrace();
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", e.getMessage());
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", e.getMessage());
        }
        finally {
            try {
                Thread.sleep(1000);
                new RabbitQueueConsumerTrainDeductionV3(queueNameString, type).start();
            }
            catch (Exception e) {
                e.printStackTrace();
                WriteLog.write("Rabbit_QueueMQ_TrainDeduction_Exception", "死灰復燃失敗。！" + e.getMessage());
            }

        }
    }
    
    /**
     * 处理接受到的消息
     * 
     * @param orderNoticeResult
     * @time 2017年3月10日 下午5:58:47
     * @author chendong
     */
    private void DeductionMethod(String orderNoticeResult) {
        System.out.println("获取消息:" + orderNoticeResult);
        WriteLog.write(this.logName,this.tempTime+":"+ "orderNoticeResult:" + orderNoticeResult);
        JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
        this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
        this.type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
        //处理方法
        new TrainorderDeduction(this.orderid, type).tongchengDeduction();
        WriteLog.write(this.logName,this.tempTime+":"+"orderid:" + this.orderid + "type:" + this.type);
    }
    
    @Override
    public boolean processTheMessage(String msgInfo) {
        boolean willClose = false;//是否即将关闭
        try {
            DeductionMethod(msgInfo);//【具体的实际进行的业务逻辑】
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            consumerStatus = RabbitMQCustomerMonitoringUtil.Mapcustomers.get(CustomerName).isUse();//控制消费者
            if (!consumerStatus) {
                willClose = true;
            }
        }
        return willClose;
    }

}
