package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;

import com.callback.PropertyUtil;
import com.ccservice.b2b2c.atom.component.QunarBestMethod;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.policy.ben.QunarBestBook;

/**
 * 
 * @author WZC
 * qunar优选政策
 *
 */
public class QunarBestPolicy {
    static QunarBestBook qunar = new QunarBestBook();

    /**
     * 提交退票
     * 
     * @param orderid 去哪对应订单id
     * @param orderNo 去哪对应订单号
     * @param site 去哪供应商域名
     * @param whyid 退票原因
     * @return
     * @time 2014年9月14日 下午1:23:09
     * @author wzc
     */
    public static String submitrefundticket(String orderid, String orderNo, String site, String whyid,
            List<Passenger> listPassenger) {
        String result = "-1";
        String url = "";
        if (whyid == null) {
            whyid = "-1";
        }
        try {
            String gqinfo = QunarBestMethod.getTgqInfo(orderid, orderNo, site);
            WriteLog.write("qunar提交退票", gqinfo);
            if (gqinfo != null && !"".equals(gqinfo)) {
                JSONObject obj = JSONObject.fromObject(gqinfo);
                JSONObject tgqViewInfo = obj.getJSONObject("tgqViewInfo");
                String nowChangeTime = "";
                if (tgqViewInfo.containsKey("nowChangeTime")) {
                    nowChangeTime = tgqViewInfo.getString("nowChangeTime");//所处时间点
                }
                else {
                    return result;
                }
                JSONArray tgqReasons = obj.getJSONArray("tgqReasons");
                String changeCause = "";
                for (int i = 0; i < tgqReasons.size(); i++) {
                    JSONObject singwhy = tgqReasons.getJSONObject(i);
                    if (singwhy.getString("code") != null && singwhy.getString("code").equals(whyid)) {
                        changeCause = singwhy.getString("msg");//获取退费原因
                    }
                }
                JSONArray tgqPassengerInfos = obj.getJSONArray("tgqPassengerInfos");
                int allFee = 0;//退票总手续费   来自退票数据 tgqPassengerInfos.refundFee 的  总和
                int allReturnPrice = 0;//总退票费（不 包含快递） 来自退票数据 tgqPassengerInfos.returnRefundFee 的总和
                String pidstr = "";
                int ind = 0;
                for (int i = 0; i < tgqPassengerInfos.size(); i++) {
                    JSONObject tgqpassenger = tgqPassengerInfos.getJSONObject(i);
                    String passengerName = tgqpassenger.getString("passengerName");//姓名
                    String passengerId = tgqpassenger.getString("passengerId");//对应乘客id
                    String refundFee = tgqpassenger.getString("refundFee");//
                    String returnRefundFee = tgqpassenger.getString("returnRefundFee");
                    for (int j = 0; j < listPassenger.size(); j++) {
                        Passenger p = listPassenger.get(j);
                        if (p.getName().equals(passengerName)) {
                            pidstr += "&pid=" + passengerId;
                            allFee += Float.valueOf(refundFee).intValue();
                            allReturnPrice += Float.valueOf(returnRefundFee).intValue();
                            ind++;
                        }
                    }

                }
                if (ind == listPassenger.size()) {
                    String param = "orderId=" + URLEncoder.encode(orderid, "UTF-8") + "&orderNo="
                            + URLEncoder.encode(orderNo, "UTF-8") + "&quserName="
                            + URLEncoder.encode(qunar.getRegistname(), "UTF-8") + "&site="
                            + URLEncoder.encode(site, "UTF-8") + "&vendorID=" + qunar.getVendorID() + pidstr
                            + "&returnCode=" + URLEncoder.encode(whyid, "utf-8") + "&changeCause="
                            + URLEncoder.encode(changeCause, "utf-8") + "&allFee="
                            + URLEncoder.encode(allFee + "", "utf-8") + "&allReturnPrice="
                            + URLEncoder.encode(allReturnPrice + "", "utf-8") + "&time="
                            + URLEncoder.encode(nowChangeTime, "utf-8") + "&haveBx=0";
                    url = qunar.getQunarurl() + "/apply_refund?" + param;
                    WriteLog.write("qunar提交退票", url);
                    String resultt = SendPostandGet.submitGet(url, "UTF-8");
                    WriteLog.write("qunar提交退票", resultt);
                    if (resultt != null && !"".equals(resultt)) {
                        JSONObject tprmsg = JSONObject.fromObject(resultt);
                        String resultflag = tprmsg.getString("ret");
                        if (resultflag != null && "true".equals(resultflag)) {
                            String allReturnP = tprmsg.getString("allReturnPrice");
                            return "1@" + allReturnP;//成功，返回退款金额
                        }
                        else {
                            String errmsg = "";
                            if (tprmsg.containsKey("errmsg")) {
                                errmsg = tprmsg.getString("errmsg");
                                WriteLog.write("qunar提交退票", "errmsg:" + errmsg);
                            }
                            return "-1@" + errmsg;
                        }
                    }
                }
            }
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        Qunarcreateorderlowprice1();
    }

    /**
     * 
     * 
     * @param order
     * @param sinfo
     * @param listPassenger
     * @return 去哪最低价下单
     * @time 2014年11月5日 下午2:20:47
     * @author wzc
     */
    public static String Qunarcreateorderlowprice1() {
        String url = "http://biz.flight.qunar.com/enCreateOrder";
        String param = "cabin=E&deptCity=PEK&arriCity=WUH&airline=CZ6605&price=616&shareairline=&flightCode=CZ&deptTime=0835&tag=TN&clientSite=xtw.trade.qunar.com&deptDate=20141226&quserName=jszjexd1177&contact=%E8%88%AA%E5%A4%A9%E5%8D%8E%E6%9C%89&contactMob=15811073432&contactTel=&sjr=&address=&conetactEmail=&xcd=0&flightType=1&vendorID=CCS&passengers[0].name=%E6%9D%8E%E5%BB%B7%E6%B0%B8&passengers[0].ischild=0&passengers[0].cardNo=510121194712145271&passengers[0].cardType=NI&passengers[0].birthday=&passengers[0].sex=0&passengers[0].passengerPriceTag=TN&passengers[1].name=%E9%82%93%E8%B4%A4%E5%A7%AC&passengers[1].ischild=0&passengers[1].cardNo=350403197503284053&passengers[1].cardType=NI&passengers[1].birthday=&passengers[1].sex=0&passengers[1].passengerPriceTag=TN";
        WriteLog.write("qunarbestlow", "POST:" + url + "?" + param);
        StringBuffer resultTt = SendPostandGet.submitPost(url, param, "UTF-8");
        String resultt = resultTt == null ? "" : resultTt.toString();
        System.out.println(resultt);
        WriteLog.write("qunarbestlow", resultt);
        return "";
    }

    /**
     * 
     * 
     * @param order
     * @param sinfo
     * @param listPassenger
     * @return 去哪最低价下单
     * @time 2014年11月5日 下午2:20:47
     * @author wzc
     */
    public static String Qunarcreateorderlowprice(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdf1 = new SimpleDateFormat("HHmm");
            String url = qunar.getQunarurl() + "/enCreateOrder";
            int luoprice = (int) ((order.getExtorderprice() - order.getTotalfuelfee() - order.getTotalairportfee()) / listPassenger
                    .size());
            String param = "cabin=" + URLEncoder.encode(sinfo.getCabincode(), "utf-8") + "&deptCity="
                    + URLEncoder.encode(sinfo.getStartairport(), "utf-8") + "&arriCity="
                    + URLEncoder.encode(sinfo.getEndairport(), "utf-8") + "&airline=" + sinfo.getFlightnumber()
                    + "&price=" + URLEncoder.encode(luoprice + "", "utf-8") + "&shareairline=" + "" + "&flightCode="
                    + sinfo.getAircomapnycode() + "&deptTime=" + sdf1.format(sinfo.getDeparttime()) + "&tag=TN"
                    + "&clientSite=" + order.getTripnote() + "&deptDate=" + sdf.format(sinfo.getDeparttime())
                    + "&quserName=" + qunar.getRegistname() + "&contact=" + URLEncoder.encode("航天华有", "utf-8")
                    + "&contactMob=" + URLEncoder.encode(qunar.getLinkmobile(), "utf-8")
                    + "&contactTel=&sjr=&address=&conetactEmail=&xcd=0&flightType=1&vendorID=" + qunar.getVendorID();
            if (listPassenger.size() > 0) {
                for (int i = 0; i < listPassenger.size(); i++) {
                    Passenger p = listPassenger.get(i);
                    String ischild = "0";// 0 表示成人 1 表示儿童
                    if (p.getPtype() == 1) {
                        ischild = "0";
                    }
                    else {
                        ischild = "1";
                    }
                    String cardtype = "NI";
                    if (p.getIdtype() == 1) {// 身份证
                        cardtype = "NI";
                    }
                    else if (p.getIdtype() == 3) {// 护照
                        cardtype = "PP";
                    }
                    else {
                        cardtype = "ID";// 其他
                    }
                    param += "&passengers[" + i + "].name=" + URLEncoder.encode(p.getName(), "utf-8") + "&passengers["
                            + i + "].ischild=" + ischild + "&passengers[" + i + "].cardNo=" + p.getIdnumber()
                            + "&passengers[" + i + "].cardType=" + cardtype + "&passengers[" + i + "].birthday="
                            + "&passengers[" + i + "].sex=0" + "&passengers[" + i + "].passengerPriceTag=TN";
                }
            }

            WriteLog.write("qunarbestlow", "POST:" + url + "?" + param);
            StringBuffer resultTt = SendPostandGet.submitPost(url, param, "UTF-8");
            String resultt = resultTt == null ? "" : resultTt.toString();
            System.out.println(resultt);
            WriteLog.write("qunarbestlow", resultt);
            if (!"".equals(resultt)) {
                JSONObject obj = JSONObject.fromObject(resultt);
                String state = obj.getString("status");
                if ("1".equals(state)) {
                    String orderNo = obj.getString("orderNo");//外部订单号
                    String id = "0";
                    if (obj.containsKey("orderId")) {
                        id = obj.getString("orderId");//外部订单号id
                    }
                    String orderStatus = obj.getString("orderStatus");//订单状态
                    result = "1|" + orderNo + "|" + id + "|" + orderStatus;
                }
                else {
                    result = "-1|" + state;
                }
            }
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 去哪创建订单
     * 
     * @param supplyurl
     *            qunar供应商地址
     * @param bookingtab
     *            价格加密字符串
     */
    public static String Qunarcreateorder(Orderinfo order, Segmentinfo sinfo, List<Passenger> listPassenger) {
        String result = "";
        String quserName = qunar.getRegistname();// 在 qunar 注册的用户名
        String clientSite = order.getTripnote();// 代理商域名
        String contact = "航天华有";// 联系人姓名
        String contactMob = qunar.getLinkmobile();// 联系人手机
        String contactTel = qunar.getLinkmobile();// 联系人电话
        String sjr = "";// 收件人
        String address = "";// 收件人地址
        String bxInvoice = "0";// (0 表示不需要 1 表示需要)
        String contactEmail = "";// 联系人 Email
        String xcd = "0";// 是否需要行程单 1 表示需要,0 表示不需要
        String xcdMethod = "";// 行程单主键 传入 booking 返回信息 expres 中的 id
        String flightType = "1";// 航班类型 1 单程 2 往返
        String bookingTag = order.getExtpolicyid();// 价格信息加密串 来着（booking 接口返回的 bookingTag
        // 的值）
        String url = "";
        try {
            String param = "quserName=" + URLEncoder.encode(quserName, "utf-8") + "&clientSite="
                    + URLEncoder.encode(clientSite, "utf-8") + "&contact=" + URLEncoder.encode(contact, "utf-8")
                    + "&contactMob=" + contactMob + "&contactTel=" + URLEncoder.encode(contactTel, "utf-8") + "&sjr="
                    + sjr + "&address=&bxInvoice=" + bxInvoice + "&contactEmail=" + contactEmail + "&xcd=" + xcd
                    + "&xcdMethod=&flightType=" + flightType + "&bookingTag=" + URLEncoder.encode(bookingTag, "utf-8")
                    + "&vendorID=" + URLEncoder.encode(qunar.getVendorID(), "utf-8");
            // 祝明明 360428198810080134 测试乘客数据
            // 左海妮 130225198207282943
            if (listPassenger.size() > 0) {
                for (int i = 0; i < listPassenger.size(); i++) {
                    Passenger p = listPassenger.get(i);
                    String ischild = "0";// 0 表示成人 1 表示儿童
                    if (p.getPtype() == 1) {
                        ischild = "0";
                    }
                    else {
                        ischild = "1";
                    }
                    String cardtype = "NI";
                    if (p.getIdtype() == 1) {// 身份证
                        cardtype = "NI";
                    }
                    else if (p.getIdtype() == 3) {// 护照
                        cardtype = "PP";
                    }
                    else {
                        cardtype = "ID";// 其他
                    }
                    param += "&name=" + URLEncoder.encode(p.getName(), "utf-8") + "&ischild="
                            + URLEncoder.encode(ischild, "utf-8") + "&bx=0&cardno="
                            + URLEncoder.encode(p.getIdnumber(), "utf-8") + "&cardtype="
                            + URLEncoder.encode(cardtype, "utf-8") + "&birthday=&sex=&passengerPriceTag=S";
                }
            }
            //            url = qunar.getQunarurl() + "/createOrder?" + param;
            //            WriteLog.write("qunarbest", url);
            //            String resultt = SendPostandGet.submitGet(url, "UTF-8");

            url = qunar.getQunarurl() + "/createOrder";
            WriteLog.write("qunarbest", "POST:" + url + "?" + param);
            StringBuffer resultTt = SendPostandGet.submitPost(url, param, "UTF-8");
            String resultt = resultTt == null ? "" : resultTt.toString();
            WriteLog.write("qunarbest", resultt);
            if (!"".equals(resultt)) {
                JSONObject obj = JSONObject.fromObject(resultt);
                String state = obj.getString("status");
                if ("0".equals(state)) {
                    String orderNo = obj.getString("orderNo");//外部订单号
                    String id = obj.getString("id");//外部订单号id
                    String orderStatus = obj.getString("orderStatus");//订单状态
                    result = "1|" + orderNo + "|" + id + "|" + orderStatus;
                }
                else {
                    result = "-1|" + state;
                }
            }

        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 支付接口
     * 
     * @throws UnsupportedEncodingException
     */
    public static String payqnorderold(Orderinfo orderinfo) {
        String msg = "0";
        String param = "";
        WriteLog.write("自动代扣", "qunaer:" + orderinfo.getExtorderid());
        if (orderinfo.getExtorderid() != null) {
            try {
                String orderId = orderinfo.getPostcode();
                String orderNo = orderinfo.getExtorderid();
                String amount = orderinfo.getExtorderprice() + "";
                String client = orderinfo.getTripnote();
                WriteLog.write("自动代扣", "qunaer:" + orderId + ":" + orderNo + ":" + amount + ":" + client);
                param = "orderId=" + URLEncoder.encode(orderId, "utf-8") + "&orderNo="
                        + URLEncoder.encode(orderNo, "utf-8") + "&amount=" + URLEncoder.encode(amount, "utf-8")
                        + "&client=" + URLEncoder.encode(client, "utf-8") + "&vendorID="
                        + URLEncoder.encode(qunar.getVendorID(), "utf-8");
                String mdstr = "signKey=" + qunar.getSignKey() + "&orderId=" + orderId + "&orderNo=" + orderNo
                        + "&client=" + client + "&vendorID=" + qunar.getVendorID();
                String validates = URLEncoder.encode(DigestUtils.md5Hex(mdstr), "utf-8");
                param += "&validate=" + validates;
                String url = qunar.getQunarurl();
                if (orderinfo.getPnr() != null && !"123456".equals(orderinfo.getPnr())) {
                    url += "/b2b_flight_pay?" + param;
                }
                else {
                    url += "/flight_pay?" + param;
                }
                WriteLog.write("自动代扣", url);
                String result = SendPostandGet.submitGet(url, "UTF-8");
                WriteLog.write("自动代扣", "支付返回信息：" + result);
                JSONObject obj = JSONObject.fromObject(result);
                String state = obj.getString("status");
                if (state.equals("1")) {//支付成功
                    msg = "S|";
                    try {
                        String status = "";// QunarBestMethod.getQunarorderState(orderNo);
                        msg += status;
                    }
                    catch (Exception e) {
                    }
                }
                else {
                    String msgerror = obj.getString("errMsg");//支付失败原因
                    msg = "0|" + msgerror;
                }
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        else {
            WriteLog.write("自动代扣", orderinfo.getOrdernumber() + "支付失败");
        }
        return msg;
    }

    /**
     * 支付接口
     * 
     * @throws UnsupportedEncodingException
     */
    public static String payqnorder(Orderinfo orderinfo) {
        String url = PropertyUtil.getValue("qunaropen", "qunarOpen.properties");
        //        String url = "http://192.168.0.153:8080/air_qunaropen/GoWhere";
        String param = com.alibaba.fastjson.JSONObject.toJSONString(orderinfo);
        String tag = "flight.national.supply.sl.pay";
        String paramContent = "param=" + param + "&tag=" + tag;
        WriteLog.write("自动代扣", orderinfo.getOrdernumber() + ":去哪儿:" + paramContent);
        String result = SendPostandGet.submitPost(url, paramContent, "utf-8").toString();
        WriteLog.write("自动代扣", orderinfo.getOrdernumber() + ":去哪儿:" + result);
        return result;
    }

}
