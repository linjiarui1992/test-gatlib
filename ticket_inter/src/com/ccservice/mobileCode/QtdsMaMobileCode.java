/**
 * 
 */
package com.ccservice.mobileCode;

import java.io.IOException;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.util.CCSHttpClient;
import com.ccservice.b2b2c.atom.component.util.CCSPostMethod;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.mobileCode.util.MobileCodeServer;

/**
 * 齐天大圣  http://www.7tds.com/login
 * @time 2015年10月12日10:07:44
 * @author chendong
 */
public class QtdsMaMobileCode implements IMobileCode {
    public static void main(String[] args) {
        String pid = "33XKU939DKRF";//12306项目项目编号
        MobileCodeServer.qtdsCookieString = "JSESSIONID=1E175D075C073AE22E480EDB877B3ED9";
        //        13168390626:(JSESSIONID=4EC1C1414093B1DFEDC6FE56AB2023C5):33XKU93BO4HC:hthy7tds-hthy7tds147-<!DOCTYPE html>
        //        String uid = "";//12306
        QtdsMaMobileCode qtdsmamobilecode = getInstance(pid);
        String mobile = qtdsmamobilecode.GetMobilenum("", "", "");
        //        String mobile = "13082984425";
        System.out.println(mobile);
        String mobileValue = MobileCodeServer.mapQtdsMobile2OrderId.get(mobile);
        //        String mobileValue = "33YFD1S1U2AY";
        System.out.println(mobileValue);
        MobileCodeServer.mapQtdsMobile2OrderId.put(mobile, mobileValue);
        String result = "";
        result = qtdsmamobilecode.send(mobile, "999");
        System.out.println(result);
        //        qtdsmamobilecode = getInstance(pid);
        //        System.out.println("mobile:" + mobile);
        //        mobile = qtdsmamobilecode.GetMobilenum("", "", "");
        //        System.out.println("mobile:" + mobile);
        //        13168390626:(JSESSIONID=4EC1C1414093B1DFEDC6FE56AB2023C5):33XKU93BO4HC:hthy7tds-hthy7tds147-<!DOCTYPE html>
        //        String SecurityCode = qtdsmamobilecode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        //        System.out.println("SecurityCode:"+SecurityCode);
        //        System.out.println(qtdsmamobilecode.AddIgnoreList("", mobile, "", ""));

    }

    /**
     * 
     * @param pid 4036(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static QtdsMaMobileCode getInstance(String pid) {
        //        String pid = "";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("qtdsMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("qtdsMaAuthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        QtdsMaMobileCode qtdsMaMobileCode = new QtdsMaMobileCode(pid, uid, author_uid, author_pwd);
        return qtdsMaMobileCode;
    }

    //爱码平台登陆
    final static String LOGININURL = "http://www.7tds.com/api/user/login";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://www.7tds.com/api/order/getphone";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://www.7tds.com/api/order/result";

    //加黑手机号码
    final static String ADDIGNORELISTURL = "http://www.7tds.com/api/order/toblacklist";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.7tds.com/api/order/releaseall";

    //释放
    final static String CANCELSMSRECV = "http://www.7tds.com/api/order/releasenumber";

    String pid;//项目编号

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public QtdsMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        if (MobileCodeServer.qtdsCookieString == null) {
            this.token = LoginIn("", "");
        }
    }

    static String INFO = "http://www.7tds.com/api/user/info";

    /**
     * 
     * @time 2015年10月15日 下午7:36:07
     * @author chendong
     */
    private String getInfo() {

        String result = "-1";
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod(GETMOBILENUMURL);
        post.setFollowRedirects(false);
        post.addRequestHeader("Cookie", MobileCodeServer.qtdsCookieString);
        NameValuePair NameValuePairLoginName = new NameValuePair("numbers", "1");//用户名
        NameValuePair NameValuePairPassword = new NameValuePair("pid", this.pid);// 密码 
        NameValuePair NameValuePaircidvalidcode = new NameValuePair("cid:validcode", "validcode");// 密码 
        NameValuePair[] names = { NameValuePairLoginName, NameValuePairPassword, NameValuePaircidvalidcode };
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            //            System.out.println("statusCode==============>" + statusCode);
            //            System.out.println("==============responseBody==============");
            //            System.out.println("responseBody:" + responseBody);
            WriteLog.write("QtdsMobileCode_GetMobilenum",
                    this.author_uid + "-" + this.author_pwd + "-" + responseBody + ":responseBody:");
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int error = jsonObject.getIntValue("error");
            int status = jsonObject.getIntValue("status");
            if (statusCode == 200) {
                if (error > 0) {//错误
                    result = responseBody;
                }
                else {//正确
                    JSONArray phonesJsonArray = jsonObject.getJSONArray("phones");
                    if (phonesJsonArray.size() > 0) {
                        result = phonesJsonArray.getString(0);
                        String order_id = jsonObject.getString("order_id");
                        MobileCodeServer.mapQtdsMobile2OrderId.put(result, order_id);
                    }
                    else {
                        result = responseBody;
                    }
                }
            }
            else {
                result = status + ":了";
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;

    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 优码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod(LOGININURL);
        post.setFollowRedirects(false);
        NameValuePair NameValuePairLoginName = new NameValuePair("username", this.author_uid);//用户名
        NameValuePair NameValuePairPassword = new NameValuePair("password", this.author_pwd);// 密码 
        NameValuePair[] names = { NameValuePairLoginName, NameValuePairPassword };
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            //            System.out.println("statusCode==============>" + statusCode);
            //            System.out.println("==============responseBody==============");
            //            System.out.println("responseBody:" + responseBody);
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int status = jsonObject.getIntValue("status");
            if (status == 1 || status == 2 || status == 3) {
                this.token = getCookieString(httpClient);
                MobileCodeServer.qtdsCookieString = this.token;
            }
            else {
                System.out.println("MobileCodeServer.qtdsCookieString:" + MobileCodeServer.qtdsCookieString);
                System.out.println("LoginIn_responseBody:" + responseBody);
                WriteLog.write("QtdsMobileCode_LoginIn",
                        this.author_uid + "-" + this.author_pwd + "-" + responseBody + ":responseBody:");
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return this.token;
    }

    /**
     * 
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String pid, String uid, String token) {
        String result = "-1";
        result = getSrcMobile(0);
        return result;
    }

    /**
     * 
     * @time 2015年10月15日 下午8:40:49
     * @author chendong
     * @param j 
     */
    private String getSrcMobile(int j) {
        String result = "获取手机号失败";
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod(GETMOBILENUMURL);
        post.setFollowRedirects(false);
        post.addRequestHeader("Cookie", MobileCodeServer.qtdsCookieString);
        NameValuePair NameValuePairLoginName = new NameValuePair("numbers", "1");//获取多少个手机号
        NameValuePair NameValuePairPassword = new NameValuePair("pid", this.pid);//
        NameValuePair NameValuePaircidvalidcode = new NameValuePair("cid", "sendforcode");
        NameValuePair[] names = { NameValuePairLoginName, NameValuePairPassword, NameValuePaircidvalidcode };
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            //            System.out.println("statusCode==============>" + statusCode);
            //            System.out.println("==============responseBody==============");
            //            System.out.println("responseBody:" + responseBody);
            WriteLog.write("QtdsMobileCode_GetMobilenum",
                    this.author_uid + "-" + this.author_pwd + "-" + responseBody + ":responseBody:");
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int error = jsonObject.getIntValue("error");
            int status = jsonObject.getIntValue("status");
            if (statusCode == 200) {
                if (error > 0) {//错误
                    System.out.println("getSrcMobile:" + responseBody);
                    //                    result = responseBody;
                    if (error == 10001) {
                        this.token = LoginIn("", "");
                        if (j == 0) {
                            result = getSrcMobile(1);
                        }
                    }
                }
                else {//正确
                    JSONArray phonesJsonArray = jsonObject.getJSONArray("phones");
                    if (phonesJsonArray.size() > 0) {
                        //                        result = phonesJsonArray.getString(0);
                        String order_id = jsonObject.getString("order_id");
                        MobileCodeServer.qtdsoid = order_id;
                        for (int i = 0; i < phonesJsonArray.size(); i++) {
                            String mobile = phonesJsonArray.getString(i);
                            MobileCodeServer.mapQtdsMobile2OrderId.put(mobile, order_id);
                            result = mobile;
                        }
                    }
                    else {
                        //                        result = responseBody;
                    }
                }
            }
            else {
                //                result = status + ":了";
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String result = "no_data";
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod(GETVCODEANDRELEASEMOBILEURL);
        post.setFollowRedirects(false);
        post.addRequestHeader("Cookie", MobileCodeServer.qtdsCookieString);
        String oid = MobileCodeServer.mapQtdsMobile2OrderId.get(mobile);
        NameValuePair NameValuePairoid = new NameValuePair("oid", oid);//用户名
        NameValuePair NameValuePairoidshow = new NameValuePair("show", "json");//json

        NameValuePair[] names = { NameValuePairoid, NameValuePairoidshow };
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            WriteLog.write("QtdsMobileCode_getVcodeAndReleaseMobile", mobile + ":(" + MobileCodeServer.qtdsCookieString
                    + "):" + oid + ":" + this.author_uid + "-" + this.author_pwd + "-" + responseBody);
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int error = jsonObject.getIntValue("error");
            int status = jsonObject.getIntValue("status");
            if (statusCode == 200) {
                if (error > 0) {//错误
                    result = responseBody;
                }
                else {//正确
                      //                    {"status":0,"rows":[{"step_id":1,"step_now":1,"step_count":1,"order_detail_id":"33XKU939LU1B",
                      //                    "phone_number":"13473752814","phone_balance":10,"amount":0.15,"amount_rule":null,
                      //                    "time":"2015-10-15 18:12","stauts":"等待中","waitingInput":false,"price":0.15,
                      //                    "discount":10,"send_type":"接收","phone_to":null,"sms":null,"sms_tag":[]}]}
                      //                    int response_status = jsonObject.getIntValue("status");
                    JSONArray rowsJsonArray = jsonObject.getJSONArray("rows");
                    if (rowsJsonArray.size() > 0) {
                        result = getSms(mobile, rowsJsonArray);
                    }
                    else {
                        result = "no_data";
                    }
                }
            }
            else {
                WriteLog.write("QtdsMobileCode_getVcodeAndReleaseMobile_err", mobile + ":" + oid + ":" + this.author_uid
                        + "-" + this.author_pwd + "-" + statusCode + ":statusCode:");
                result = "no_data";
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 
     * @param rowsJsonArray
     * @time 2015年10月15日 下午11:35:55
     * @author chendong
     */
    private static String getSms(String mobile, JSONArray rowsJsonArray) {
        String result = "-1";
        for (int i = 0; i < rowsJsonArray.size(); i++) {
            JSONObject jsonObject1 = rowsJsonArray.getJSONObject(i);
            String sms = jsonObject1.getString("sms");
            if (sms == null || "".equals(sms)) {
                result = "no_data";
                continue;
            }
            else {
                if (mobile.equals(jsonObject1.getString("phone_number"))) {
                    result = sms;
                    break;
                }
                else {
                    result = "no_data";
                    continue;
                }
            }
        }
        return result;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList_old(String uid, String mobiles, String pid, String token) {
        String result = "-1";
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod(ADDIGNORELISTURL);
        post.setFollowRedirects(false);
        post.addRequestHeader("Cookie", MobileCodeServer.qtdsCookieString);
        NameValuePair NameValuePairphones = new NameValuePair("phones", mobiles);
        NameValuePair NameValuePairPassword = new NameValuePair("pid", this.pid);
        NameValuePair[] names = { NameValuePairphones, NameValuePairPassword };
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            System.out.println("responseBody:" + responseBody);
            WriteLog.write("QtdsMobileCode_AddIgnoreList", this.author_uid + "-" + this.author_pwd + "-" + this.pid
                    + "-" + mobiles + "-" + responseBody + ":responseBody:");
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int error = jsonObject.getIntValue("error");
            int status = jsonObject.getIntValue("status");
            if (statusCode == 200) {
                if (error > 0) {//错误
                    result = responseBody;
                }
                else {//正确
                    result = jsonObject.getString("msg");
                    MobileCodeServer.mapQtdsMobile2OrderId.remove(mobiles);
                }
            }
            else {
                result = status + ":了";
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token + "&mobile="
                    + mobile;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_CancelSMSRecv", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    public String getCookieString(HttpClient httpClient) {
        StringBuffer cookieString = new StringBuffer("");
        Cookie[] cookies = httpClient.getState().getCookies();
        for (int i = 0; i < cookies.length; i++) {
            Cookie cookie1 = cookies[i];
            if (i > 0) {
                cookieString.append(" ");
            }
            cookieString.append(cookie1.getName() + "=" + cookie1.getValue());
            if (i < cookies.length - 1) {
                cookieString.append(";");
            }
        }
        return cookieString.toString();
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        String result = "-1";
        CCSPostMethod post = null;
        CCSHttpClient httpClient = new CCSHttpClient(false, 60000L, "*/*");
        post = new CCSPostMethod("http://www.7tds.com/api/order/send");
        post.setFollowRedirects(false);
        post.addRequestHeader("Cookie", MobileCodeServer.qtdsCookieString);
        String did = MobileCodeServer.mapQtdsMobile2OrderId.get(mobile);
        NameValuePair NameValuePairphones = new NameValuePair("did", did);
        String inputs = "";//{"code":"13881340000", "pass":"852147", "s":"11"}
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("code", "12306");
        jsonObject1.put("pass", content);
        jsonObject1.put("s", content);
        inputs = jsonObject1.toJSONString();
        NameValuePair NameValuePairPassword = new NameValuePair("inputs", inputs);
        NameValuePair[] names = { NameValuePairphones, NameValuePairPassword };
        WriteLog.write("QtdsMobileCode_send", mobile + ":" + JSONObject.toJSONString(names));
        post.setRequestBody(names);
        try {
            httpClient.executeMethod(post);
            String responseBody = post.getResponseBodyAsString();
            int statusCode = post.getStatusCode();
            System.out.println("responseBody:" + responseBody);
            WriteLog.write("QtdsMobileCode_send", mobile + ":" + this.author_uid + "-" + this.author_pwd + "-"
                    + this.pid + "-" + mobile + "-" + responseBody + ":responseBody:");
            JSONObject jsonObject = JSONObject.parseObject(responseBody);
            int error = jsonObject.getIntValue("error");
            int status = jsonObject.getIntValue("status");
            if (statusCode == 200) {
                if (error > 0) {//错误
                    result = responseBody;
                }
                else {//正确
                    result = jsonObject.getString("msg");
                }
            }
            else {
                result = status + ":了";
            }
        }
        catch (HttpException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }
}
