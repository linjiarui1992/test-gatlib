
package com.webservice.kkkk;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTgqStipulate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTgqStipulate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TgqStipulate" type="{http://tempuri.org/}TgqStipulate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTgqStipulate", propOrder = {
    "tgqStipulate"
})
public class ArrayOfTgqStipulate {

    @XmlElement(name = "TgqStipulate", nillable = true)
    protected List<TgqStipulate> tgqStipulate;

    /**
     * Gets the value of the tgqStipulate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tgqStipulate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTgqStipulate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TgqStipulate }
     * 
     * 
     */
    public List<TgqStipulate> getTgqStipulate() {
        if (tgqStipulate == null) {
            tgqStipulate = new ArrayList<TgqStipulate>();
        }
        return this.tgqStipulate;
    }

}
