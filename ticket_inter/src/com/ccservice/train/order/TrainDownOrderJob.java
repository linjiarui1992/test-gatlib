package com.ccservice.train.order;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 定时下单
 */
public class TrainDownOrderJob implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //        String flag = getSysconfigStringbydb("downorderflag");
        //        if ("1".equals(flag) && getDownOrderTimeControl()) {
        downOrderFiend();
        //        }
    }

    /**
     * 说明:下单可用时间:早7晚11
     * 
     * @param date
     * @return
     * @time 2015年5月15日 下午3:46:47
     * @author fiend
     */
    public boolean getNowTime(Date date, String timebefore, String timeend) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        try {
            Date dateBefor = df.parse(timebefore);
            Date dateAfter = df.parse(timeend);
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
            WriteLog.write("getNowTime", e + "");
        }
        return false;//现在24小时,以后有需要再改为FALSE
    }

    /**
     * 收单下单时间限制
     * @return
     */
    public boolean getDownOrderTimeControl() {
        boolean flag = true;
        String timestr = PropertyUtil.getValue("shoudantimecontrol", "train.properties");
        System.out.println("当前允许时间：" + timestr + "======" + TimeUtil.gettodaydate(4));
        if (timestr != null && !"".equals(timestr)) {
            if (getNowTime(new Date(), timestr.split("-")[0], timestr.split("-")[1])) {
                return true;
            }
            else {
                return false;
            }
        }
        return flag;
    }

    public static void main(String[] args) {
        String msg = "{\"extra\":\"\",\"description\":\"book-tbtestauto1942\",\"msg_type\":\"1\",\"main_biz_order_id\":193577538925497,\"user_id\":175992312,\"time_stamp\":\"2014-11-25 16:00:53\",\"sub_biz_order_id\":0}";
        try {
            //            msg = SendPostandGet.submitPost("http://localhost:9004/cn_interface/TaobaoTarinMonitor",
            //                    "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
            msg = SendPostandGet.submitPost("http://121.40.125.114:19116/cn_interface/TaobaoTarinMonitor",
                    "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(msg);
    }

    /**
     * 淘宝仍单
     * @param args
     */
    public static void main11(String[] args) {
        String sql = "select top 5 C_MSG,ID from T_TRAINORDERMSG where c_state=1 order by id asc ";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String msg = map.get("C_MSG").toString();
            String id = map.get("ID").toString();
            System.out.println(msg);
            String updatesqlt = "update T_TRAINORDERMSG set C_STATE=2,c_gettime='"
                    + new Timestamp(System.currentTimeMillis()) + "' where id in=" + id;
            Server.getInstance().getSystemService().findMapResultBySql(updatesqlt, null);
            String orderurl = "http://121.41.171.147:30002/cn_interface/TaobaoTarinMonitor";
            WriteLog.write("t4.5收单下单", id + ":" + msg);
            try {
                msg = SendPostandGet.submitPost(orderurl, "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8")
                        .toString();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            WriteLog.write("t4.5收单下单", "返回：" + id + ":" + msg);
            String updatesql = "";
            if (msg != null && msg.length() > 0) {
                try {
                    if ("success".equals(msg)) {
                        updatesql = "update T_TRAINORDERMSG set c_state=3 where id= " + id;
                        Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                    }
                    else {
                        JSONObject obj = JSONObject.parseObject(msg);
                        String ordernumber = obj.getString("orderid");
                        boolean flag = obj.getBooleanValue("success");
                        if (flag) {
                            updatesql = "update T_TRAINORDERMSG set c_state=3,C_ORDERNUMBER='" + ordernumber
                                    + "' where id= " + id;
                            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                updatesql = "update T_TRAINORDERMSG set c_state=5 where id= " + id;
                Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
            }

        }
    }

    public static void main1(String[] args) {
        //        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id<500";
        //        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id>500 and id<1000";
        //        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id>1000 and id<1500";
        //        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id>1500 and id<2000";
        //        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id>2000 and id<2500";
        String sql = "select top 500 C_MSG,ID from T_TRAINORDERMSG where c_state=3 and id>2500";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String msg = map.get("C_MSG").toString();
            String id = map.get("ID").toString();
            JSONObject obj = JSONObject.parseObject(msg);
            String tcorder = obj.getString("orderid");
            String countsql = "select id from t_trainorder with(nolock) where C_QUNARORDERNUMBER='" + tcorder + "'";
            List countlist = Server.getInstance().getSystemService().findMapResultBySql(countsql, null);
            if (countlist != null && countlist.size() > 0) {

            }
            else {
                try {

                    String msgt = msg;
                    try {
                        msg = SendPostandGet.submitPost("http://tctrainorder.hangtian123.net/tcTrain",
                                "jsonStr=" + URLEncoder.encode(msg, "UTF-8"), "UTF-8").toString();
                    }
                    catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    System.out.println(msg);
                    String sqtl = "update T_TRAINORDERMSG set c_state=3 where id=" + id;
                    Server.getInstance().getSystemService().findMapResultBySql(sqtl, null);
                }
                catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * new!!!
     * 
     * @time 2016年9月7日 下午5:16:49
     * @author fiend
     */
    private void downOrderFiend() {
        String downorderthreadcount = getSysconfigStringbydb("synchronizationorderthreadcount");//下单启动的线程数量
        String downorderordercount = getSysconfigStringbydb("synchronizationorderordercount");//下单取订单的数量
        if ("-1".equals(downorderordercount)) {
            downorderordercount = "50";
        }
        if ("-1".equals(downorderthreadcount)) {
            downorderthreadcount = "60";
        }
        String orderurl = getSysconfigString("createorderurl");//访问下单地址  
        ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(downorderthreadcount).intValue());
        String sql = "select top " + downorderordercount
                + " * from T_TRAINORDERMSG with(nolock)  where C_STATE=1  order by id asc ";
        System.out.println("启动的线程数量:" + downorderthreadcount + ",下单取订单的数量:" + downorderordercount);
        WriteLog.write("t火车票接口_4.5收单下单", "启动的线程数量:" + downorderthreadcount + ",下单取订单的数量:" + downorderordercount
                + "\n执行sql:" + sql);
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        if (list != null && list.size() > 0) {
            String idstr = "";
            for (int i = 0; i < list.size(); i++) {
                try {
                    Map map = (Map) list.get(i);
                    long sid = Long.valueOf(map.get("ID").toString());//任务标识
                    if (i == list.size() - 1) {
                        idstr += sid;
                    }
                    else {
                        idstr += sid + ",";
                    }
                }
                catch (Exception e) {
                }
            }
            String updatesql = "update T_TRAINORDERMSG set C_STATE=2,c_gettime='"
                    + new Timestamp(System.currentTimeMillis()) + "' where id in (" + idstr + ")";
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
        }
        for (int i = 0; i < list.size(); i++) {
            try {
                Map map = (Map) list.get(i);
                long sid = Long.valueOf(map.get("ID").toString());//任务标识
                String ordermsg = map.get("C_MSG").toString();//下单信息
                System.out.println("sid:" + sid);
                WriteLog.write("t同程火车票接口_4.5收单下单", ordermsg);
                Thread thr = new MyThreadDownOrder(ordermsg, orderurl, sid);
                pool.execute(thr);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        pool.shutdown();
    }

    /**
     * 下单执行方法
     */
    public void downOrder() {
        String downorderthreadcount = getSysconfigStringbydb("downorderthreadcount");//下单启动的线程数量
        String downorderordercount = getSysconfigStringbydb("downorderordercount");//下单取订单的数量
        if ("-1".equals(downorderordercount)) {
            downorderordercount = "50";
        }
        if ("-1".equals(downorderthreadcount)) {
            downorderthreadcount = "60";
        }
        String orderurl = getSysconfigString("createorderurl");//访问下单地址
        String taobaoorderurl = getSysconfigString("taobaocreateorderurl");//淘宝访问下单地址
        String nodownorderagentidstr = getSysconfigStringbydb("downorderagentidstr");//不去下单的agentID
        ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(downorderthreadcount).intValue());
        String sql = "select top " + downorderordercount + " * from T_TRAINORDERMSG with(nolock)  where C_STATE=1 ";
        if (!"".equals(nodownorderagentidstr) && nodownorderagentidstr.length() > 0) {
            sql += " and C_AGENTID  in (" + nodownorderagentidstr + ") ";
        }
        sql += " order by id asc ";
        WriteLog.write("t火车票接口_4.5收单下单", "启动的线程数量:" + downorderthreadcount + ",下单取订单的数量:" + downorderordercount
                + "\n执行sql:" + sql);
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        String requsturl = "";
        if (list != null && list.size() > 0) {
            String idstr = "";
            for (int i = 0; i < list.size(); i++) {
                try {
                    Map map = (Map) list.get(i);
                    long sid = Long.valueOf(map.get("ID").toString());//任务标识
                    if (i == list.size() - 1) {
                        idstr += sid;
                    }
                    else {
                        idstr += sid + ",";
                    }
                }
                catch (Exception e) {
                }
            }
            String updatesql = "update T_TRAINORDERMSG set C_STATE=2,c_gettime='"
                    + new Timestamp(System.currentTimeMillis()) + "' where id in (" + idstr + ")";
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
            for (int i = 0; i < list.size(); i++) {
                try {
                    Map map = (Map) list.get(i);
                    long sid = Long.valueOf(map.get("ID").toString());//任务标识
                    String ordermsg = map.get("C_MSG").toString();//下单信息
                    long interfactype = map.get("C_INTERFACETYPE") == null ? 0 : Long.valueOf(map
                            .get("C_INTERFACETYPE").toString());
                    if (interfactype == 2) {
                        requsturl = taobaoorderurl;
                    }
                    else {
                        requsturl = orderurl;
                    }
                    WriteLog.write("t同程火车票接口_4.5收单下单", ordermsg);
                    Thread thr = new MyThreadDownOrder(ordermsg, requsturl, sid);
                    pool.execute(thr);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        pool.shutdown();
    }

    /**
     * 根据sysconfig的name获得value
     * 内存
     * @param name
     * @return
     */
    public String getSysconfigString(String name) {
        String result = "-1";
        try {
            if (Server.getInstance().getDateHashMap().get(name) == null) {
                List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                        .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
                if (sysoconfigs.size() > 0) {
                    result = sysoconfigs.get(0).getValue();
                    Server.getInstance().getDateHashMap().put(name, result);
                }
            }
            else {
                result = Server.getInstance().getDateHashMap().get(name);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 根据sysconfig的name获得value
     * 实时 如果是判断的必须调用实时接口
     * @param name
     * @return
     */
    public String getSysconfigStringbydb(String name) {
        String result = "-1";
        List<Sysconfig> sysoconfigs = Server.getInstance().getSystemService()
                .findAllSysconfig("WHERE C_NAME='" + name + "'", "", -1, 0);
        if (sysoconfigs.size() > 0) {
            result = sysoconfigs.get(0).getValue();
        }
        return result;
    }

}
