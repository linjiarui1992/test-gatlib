package com.ccservice.inter.job.train.thread;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 说明：12306登录线程 
 * @time 2014年9月5日 下午12:29:59
 * @author yinshubin
 */
public class MyThreadLogin extends Thread {
    private Customeruser customeruser;

    private String url;

    private String mobile;

    private int r1;

    private RepServerBean rep;

    private Integer login_type;

    public MyThreadLogin(Customeruser customeruser, String url) {
        this.customeruser = customeruser;
        this.url = url;
    }

    public MyThreadLogin(Customeruser customeruser, RepServerBean rep) {
        this.customeruser = customeruser;
        this.url = rep.getUrl();
        this.rep = rep;
    }

    public MyThreadLogin(String mobile, RepServerBean rep) {
        this.mobile = mobile;
        this.rep = rep;
        this.url = rep.getUrl();
        this.login_type = 1;
    }

    @Override
    public void run() {
        this.r1 = new Random().nextInt(10000);
        Long l1 = System.currentTimeMillis();
        if (this.login_type != null && this.login_type == 1) {
            customeruser = getTologinAccountbyMobile(this.mobile);
        }
        WriteLog.write("Job12306index_MyThreadLogin", r1 + ":正在自动登录12306账号:" + customeruser.getLoginname());
        customeruser = verification12306(customeruser);
        WriteLog.write("Job12306index_MyThreadLogin",
                r1 + ":登录结果:" + customeruser.getState() + ":耗时:" + (System.currentTimeMillis() - l1));
    }

    /**
     * 说明：获取满足条件的12306账号进行登录
     * @time 2014年8月30日 上午11:45:17
     * @author yinshubin
     */
    public Customeruser verification12306(Customeruser customeruser) {
        String logname = customeruser.getLoginname();
        String logpassword = customeruser.getLogpassword();
        String mobile = customeruser.getMobile();
        String sql = "-1";
        try {
            customeruser.setCardtype(null);
            customeruser.setState(0);
            String par = "datatypeflag=12&logname=" + logname + "&logpassword=" + logpassword + "&mobile=" + mobile;
            String result = "";
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":开始时间:" + TimeUtil.gettodaydate(5));
            int sleep_time = new Random().nextInt(10000);
            //            System.out.println("睡觉:" + sleep_time);
            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";登录12306账号结果:" + result + ":" + url
                    + "?" + par);
            if (null != result && !"".equals(result) && result.contains("JSESSIONID")) {
                customeruser.setState(1);
                customeruser.setId(customeruser.getId());
                customeruser.setCardnunber(result);
                customeruser.setMemberemail(url);
                customeruser.setEnname("1");
                customeruser.setIsenable(1);
                customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
                customeruser.setBirthday(new Timestamp(System.currentTimeMillis()));
                int updatedb = 0;
                try {
                    updatedb = Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
                }
                catch (Exception e) {
                    e.printStackTrace();
                    sql = "UPDATE T_CUSTOMERUSER SET C_STATE=0 where id=" + customeruser.getId();
                }
                WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号成功:" + logname
                        + ":更新结果:" + updatedb);
            }
            else if (result.contains("登录失败")) {
                sql = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=3 where id=" + customeruser.getId();
                WriteLog.write("Job12306index_MyThreadLogin_login_fail", r1 + ":" + logname + ":result:" + result + ":"
                        + sql);
            }
            else {
                sql = "UPDATE T_CUSTOMERUSER SET C_STATE=0 where id=" + customeruser.getId();
            }
        }
        catch (Exception e) {
            sql = "UPDATE T_CUSTOMERUSER SET C_STATE=0 where id=" + customeruser.getId();
        }
        int icount = 0;
        try {
            icount = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        }
        catch (Exception e) {
        }
        WriteLog.write("Job12306index_MyThreadLogin_login_fail", r1 + ":" + logname + ":result:" + icount);
        return customeruser;
    }

    /**
     * 12306发手机短信验证码登录,根据手机号获取到可以登录的账号
     * 
     * @time 2015年5月8日 下午8:49:18
     * @author chendong
     */
    public Customeruser getTologinAccountbyMobile(String mobile) {
        Customeruser user = new Customeruser();
        List list = Server.getInstance().getSystemService()
                .findMapResultByProcedure("sp_CustomerUser_JobIndexAgain_getcustomerusersBymobile @mobile=" + mobile);
        if (list.size() > 0) {//ID,C_LOGINNAME,C_LOGPASSWORD,C_ISENABLE,C_STATE,C_ENNAME,c_mobile
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            String C_LOGINNAME = objectisnull(map.get("C_LOGINNAME"));
            String C_LOGPASSWORD = objectisnull(map.get("C_LOGPASSWORD"));
            //            String C_ISENABLE = objectisnull(map.get("C_ISENABLE"));
            String C_MOBILE = objectisnull(map.get("C_MOBILE"));
            user.setId(id);
            user.setLoginname(C_LOGINNAME);
            user.setLogpassword(C_LOGPASSWORD);
            //            user.setIsenable(Integer.parseInt(C_ISENABLE));
            user.setMobile(C_MOBILE);
        }
        return user;
    }

    /**
     * 如果为空返回 "0"
     * 
     * @param object
     * @return
     * @time 2015年4月15日 下午1:57:54
     * @author chendong
     */
    public String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

}
