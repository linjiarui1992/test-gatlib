package com.ccservice.offlineExpress.servlet.taobao;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.servlet.taobao.service.TrainTaoBaoOfflineLockRapidSendOvertime;
import com.ccservice.offlineExpress.servlet.taobao.service.TrainTaoBaoOfflineLockSecondRapidSendOvertime;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineLockOrderCallbackServlet
 * @description: TODO - 淘宝线下票闪送单的锁票请求接口 - 闪送订单
 * 
 * cn_home平台 请求该接口
 * 
 * 模拟实现锁单的问题 - 锁单期间不允许取消 - 锁单时长
 * 
 * 一期没有锁单的接口需求
 * 
 * 20171027-新增相关的逻辑和实现
 * 
 * 走途牛的锁单的类似的反馈的结果 - 只提交请求
 * 
 * 闪送单走另外的逻辑
 * 
 * @author: 郑州-技术-郭伟强  E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:57:36
 * @version: v 1.0
 * @since 
 * 
 */
public class TrainTaoBaoOfflineLockTicketRapidSendServletCN extends HttpServlet {
    private static final String LOGNAME = "淘宝线下票闪送单的锁票请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String orderId = request.getParameter("orderId");//系统表的主键的订单表的ID
        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志

        Long orderIdl = Long.valueOf(orderId);
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求信息-orderId-->" + orderIdl + ",userid-->" + useridi);

        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起闪送单的锁票请求");

        JSONObject result = taoBaoLockTicketRapidSendCN(orderIdl, useridi);

        WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求结果-result" + result);

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject taoBaoLockTicketRapidSendCN(Long orderIdl, Integer useridi) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求-进入到方法-taoBaoLockTicketRapidSendCN:orderIdl-->" + orderIdl
                + ",useridi-->" + useridi);

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        //一般不会出现上述原因

        resResult.put("orderid", trainOrderOffline.getOrderNumber());

        /**
         * 第一首先是访问UU跑腿进行下单 - 
         * 
         * //更新锁单状态为锁单中 - 点击一次之后
                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderIdl, 3);//锁单中
                
         * 如果同步反馈失败的话一般是UU跑腿网络异常造成的
         * 
         * 假如同步反馈成功的话，
         * 
         * 设置定时任务，如果在8min中之后还没有反馈的，自动调取消接口，同步反馈锁单失败
         * 
         */

        if (trainOrderOffline.getLockedStatus() != 0) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已发起过锁单请求，请勿重复操作");
            return resResult;
        }

        //一般不会出现该种原因
        if (trainOrderOffline.getOrderStatus() != 1) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单是非等待出票的状态，不允许锁单");
            return resResult;
        }

        //由于出现了 同程的二次进单的代码，因而，在闪送-锁单的操作中，需要 判定，该单是否已经做过二次下单的操作
        Boolean isSecondRapidSend = TrainOrderOfflineUtil.getIsSecondOrderRapidSend(trainOrderOffline.getOrderNumber());//判断闪送单是否是二次订单 - 

        UUptService uuptService = new UUptService();
        JSONObject responseJson = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("orderId", orderIdl);
        try {
            if (isSecondRapidSend) {//二次下单
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起闪送二次下单请求");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "addOrderAgain", data, responseJson);
            }
            else {//一次取消
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "发起闪送下单请求");
                responseJson = uuptService.operate(LOGNAME, "uupt", r1, "addOrder", data, responseJson);
            }
        }
        catch (Exception e) {
            ExceptionTNUtil.handleTNException(e);
        }
        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求反馈信息-responseJson" + responseJson);

        /**
         * 
                responseJson.put("message", "失败");
                responseJson.put("success", false);
            } else {
                responseJson.put("message", "成功");
                responseJson.put("success", true);
         * 
        {"message":"成功","result":{"partnerOrderNumber":"U37908001710310949679202216","message":"订单发布成功","orderNumber":"","success":true},"success":true}
         * 
        {"message":"请求发生异常","result":"","success":false}
         * 
         */
        //Boolean responseB = responseJson.getBooleanValue("success");
        Boolean responseB = false;
        JSONObject responseJsonResult = new JSONObject();
        if (responseJson.getBooleanValue("success")) {
            responseJsonResult = responseJson.getJSONObject("result");
            responseB = responseJsonResult.getBooleanValue("success");
        }

        if (responseB) {
            //锁单请求成功
            //更新锁单状态为锁单中 - 点击一次之后
            trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderIdl, 3);//锁单中

            //下单成功之后，需要做快递单号的修改
            String ExpressNum = responseJson.getJSONObject("result").getString("partnerOrderNumber");
            MailAddressDao mailAddressDao = new MailAddressDao();

            WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求反馈信息-更新快递单号-ExpressNum:" + ExpressNum + ",orderId:" + orderIdl);

            mailAddressDao.updateExpressNumByORDERID(orderIdl.intValue(), ExpressNum);

            /**
             * 如果想模拟成为同步反馈的结果，需要判定是否收到异步的锁单反馈 - 且不允许再次锁单 - 需要加上一个标志位
             * 
             * 途牛的锁票的异步转同步的内部等待时间 - 以s为单位
             */
            String LockRapidSendWait = PropertyUtil.getValue("TrainTongChengOfflineLockRapidSendWait",
                    "Train.GuestAccount.properties");

            //毫秒数
            Long overtime = new Date().getTime() + Integer.valueOf(LockRapidSendWait) * 60 * 1000;

            String lockWaitDateTime = TrainOrderOfflineUtil.getTimestrByTime(overtime);
            //记录日志
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi,
                    "闪送单的锁票请求成功，等待UU跑腿异步反馈结果，目前等待超时的设置时间是:" + LockRapidSendWait + "min，超时后自动访问UU跑腿取消订单的接口，设置该单为闪送接单超时");

            //此处另起JOB，完成5分钟之后的拒单操作

            String year = lockWaitDateTime.substring(0, 4);
            String month = lockWaitDateTime.substring(5, 7);
            String day = lockWaitDateTime.substring(8, 10);
            String hour = lockWaitDateTime.substring(11, 13);
            String minute = lockWaitDateTime.substring(14, 16);
            String second = lockWaitDateTime.substring(17, 19);

            //"40 17 14 25 08 ? 2017"
            String cronExpression = second + " " + minute + " " + hour + " " + day + " " + month + " ? " + year;

            //System.out.println(cronExpression);

            WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求启动定时任务-cronExpression" + cronExpression);

            //实际的锁单请求中 - 可能会出现 8 min 内，原有的锁票的任务 还未执行的情况下，发起了后续的锁单请求的交互结果 - 所以此处的定时任务的判定逻辑需要做相关的修改

            /**
             * 后期涉及的东西
             * 要求在自动发起取消的同时，结束相关位置的定时任务 - 正常流程中排除了下述 jobDetail!=null 的情况的出现
             */
            try {
                JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail(
                        "TrainTaoBaoOfflineLockRapidSendOvertime" + "Job" + trainOrderOffline.getId(),
                        "TrainTaoBaoOfflineLockRapidSendOvertime" + "JobGroup");
                if (jobDetail != null) {
                    WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求启动定时任务-jobDetail:" + jobDetail.getFullName());

                    resResult.put("success", "false");
                    resResult.put("msg", "锁票请求的定时取消任务已存在，处理失败");
                    return resResult;
                }
                else {
                    WriteLog.write(LOGNAME, r1 + ":淘宝线下票闪送单的锁票请求启动定时任务-jobDetail:" + null);

                    //创建一个定时任务，并在指定的时间点进行启动
                    try {
                        if (isSecondRapidSend) {//如果是二次订单的话，需要走二次取消的相关逻辑
                            SchedulerUtil.startLockScheduler(orderIdl, useridi, LockRapidSendWait, cronExpression,
                                    "TrainTaoBaoOfflineLockRapidSendOvertime",
                                    TrainTaoBaoOfflineLockSecondRapidSendOvertime.class);
                        }
                        else {//一次取消
                            SchedulerUtil.startLockScheduler(orderIdl, useridi, LockRapidSendWait, cronExpression,
                                    "TrainTaoBaoOfflineLockRapidSendOvertime",
                                    TrainTaoBaoOfflineLockRapidSendOvertime.class);
                        }
                    }
                    catch (Exception e) {
                        return ExceptionTCUtil.handleTCException(e);
                    }

                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "闪送单的锁票请求成功");

                    resResult.put("success", "true");
                    resResult.put("msg", "闪送单的锁票请求成功");
                    return resResult;
                }
            }
            catch (SchedulerException e1) {
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "锁票失败，原因异常，请联系技术处理");//记录日志

                resResult.put("success", "true");
                resResult.put("msg", "闪送单的锁票请求-定时任务出现异常");
                return resResult;
            }
        }
        else {//锁单请求失败 - 后续可以再次锁单 - 不更改任何内容 - 系统问题
            String errorMsg = responseJsonResult.getString("message");
            if (errorMsg == null || "".equals(errorMsg)) {
                errorMsg = responseJson.getString("message");
            }
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "该订单UU跑腿下单失败失败:" + errorMsg + "-请联系技术处理");

            resResult.put("success", "false");
            resResult.put("msg", "该订单UU跑腿下单失败");
            return resResult;
        }
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        String result = "";

        String orderId = "1289770";
        String agentid = "382";
        //系统表的主键的订单表的ID
        String param = "?orderId=" + orderId + "&userid=" + agentid;

        String tongchengLockTicketRapidSendServlet = "http://localhost:8097/ticket_inter/TrainTongChengOfflineLockTicketRapidSendServletCN";

        //String tongchengLockTicketRapidSendServlet = "http://ws.peisong.51kongtie.com/ticket_inter/TrainTongChengOfflineLockTicketRapidSendServletCN";

        //本地测试地址
        //String url = "http://localhost:8097/ticket_inter/TrainTongChengOfflineOrder";
        //测试环境地址
        //String url = "http://121.40.226.72:9007/ticket_inter/TrainTongChengOfflineOrder";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/TrainTongChengOfflineOrder";

        try {
            System.out.println(new HttpUtil().doGet(tongchengLockTicketRapidSendServlet, param));
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
