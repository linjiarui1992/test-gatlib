package com.ccservice.inter.job.train.train12306;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.flightinfo.FlightSearch;
import com.ccservice.b2b2c.policy.SendPostandGet;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.RepServerUtil;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.qunar.util.ExceptionUtil;

public class TrainorderLishiMethod extends TrainSupplyMethod {
    static int limitNum = 0;

    static long lastUpdateTimitNumTime = 0;

    public static void main(String[] args) {
        //        GZQ:LDQ:2016-02-01:http://221.10.101.119:20052/Reptile/traininit:K6546:硬座
        TrainorderLishiMethod trainorderLishiMethod = new TrainorderLishiMethod();
        //        trainorderLishiMethod
        //                .cheyupiaoByjson(
        //                        "{\"validateMessagesShowId\":\"_validatorMessage\",\"status\":true,\"httpstatus\":200,\"data\":{\"datas\":[{\"train_no\":\"760000K81806\",\"station_train_code\":\"K818\",\"start_station_telecode\":\"CDW\",\"start_station_name\":\"成都\",\"end_station_telecode\":\"BXP\",\"end_station_name\":\"北京西\",\"from_station_telecode\":\"NCW\",\"from_station_name\":\"南充\",\"to_station_telecode\":\"PAW\",\"to_station_name\":\"蓬安\",\"start_time\":\"22:26\",\"arrive_time\":\"22:53\",\"day_difference\":\"0\",\"train_class_name\":\"\",\"lishi\":\"00:27\",\"canWebBuy\":\"Y\",\"lishiValue\":\"27\",\"yp_info\":\"10009532714003850007100095000020014500003002650052\",\"control_train_day\":\"20300331\",\"start_train_date\":\"20160126\",\"seat_feature\":\"W343132333\",\"yp_ex\":\"1040102030\",\"train_seat_feature\":\"3\",\"seat_types\":\"14123\",\"location_code\":\"W2\",\"from_station_no\":\"04\",\"to_station_no\":\"05\",\"control_day\":59,\"sale_time\":\"0830\",\"is_support_card\":\"0\",\"note\":\"\",\"controlled_train_flag\":\"0\",\"controlled_train_message\":\"正常车次，不受控\",\"gg_num\":\"--\",\"gr_num\":\"--\",\"qt_num\":\"--\",\"rw_num\":\"7\",\"rz_num\":\"无\",\"tz_num\":\"--\",\"wz_num\":\"271\",\"yb_num\":\"--\",\"yw_num\":\"52\",\"yz_num\":\"无\",\"ze_num\":\"--\",\"zy_num\":\"--\",\"swz_num\":\"--\"},{\"train_no\":\"76000K161602\",\"station_train_code\":\"K1616\",\"start_station_telecode\":\"CDW\",\"start_station_name\":\"成都\",\"end_station_telecode\":\"YIJ\",\"end_station_name\":\"银川\",\"from_station_telecode\":\"NCW\",\"from_station_name\":\"南充\",\"to_station_telecode\":\"PAW\",\"to_station_name\":\"蓬安\",\"start_time\":\"23:38\",\"arrive_time\":\"00:05\",\"day_difference\":\"1\",\"train_class_name\":\"\",\"lishi\":\"00:27\",\"canWebBuy\":\"Y\",\"lishiValue\":\"27\",\"yp_info\":\"1000953384400385001310009504473002650159\",\"control_train_day\":\"20300331\",\"start_train_date\":\"20160126\",\"seat_feature\":\"W3431333\",\"yp_ex\":\"10401030\",\"train_seat_feature\":\"3\",\"seat_types\":\"1413\",\"location_code\":\"W1\",\"from_station_no\":\"03\",\"to_station_no\":\"04\",\"control_day\":59,\"sale_time\":\"0830\",\"is_support_card\":\"0\",\"note\":\"\",\"controlled_train_flag\":\"0\",\"controlled_train_message\":\"正常车次，不受控\",\"gg_num\":\"--\",\"gr_num\":\"--\",\"qt_num\":\"--\",\"rw_num\":\"13\",\"rz_num\":\"--\",\"tz_num\":\"--\",\"wz_num\":\"384\",\"yb_num\":\"--\",\"yw_num\":\"159\",\"yz_num\":\"447\",\"ze_num\":\"--\",\"zy_num\":\"--\",\"swz_num\":\"--\"}],\"flag\":true,\"searchDate\":\"2016年01月26号&nbsp;&nbsp;周二\"},\"messages\":[],\"validateMessages\":{}}",
        //                        "硬座", "K818", 1);
        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(10L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(trainorderLishiMethod.getLimitNum());
        }
    }

    private final int lishi_max = 4;

    /**
     * 根据出发到达时间获取列车信息 数据是实时返回的 train.getStart_station_name() 始发站名字
     * train.getSfz()始发站三字码 train.getEnd_station_name()终点站名字
     * train.getZdz()终点站名字三字码 train.getRun_time_minute()运行分钟数
     * 
     * @param fromcity
     *            出发三字码
     * @param tocity
     *            到达三字码
     * @param date
     *            时间 格式2014-12-11
     * @param traincode
     *            车次
     * @return
     * @time 2014年12月11日 下午12:28:51
     * @author chendong
     */
    public String getTrainby12306(long orderid, String fromcity, String tocity, String date, String traincode) {
        WriteLog.write("TrainorderLishiMethod_getTrainby12306", orderid + "---" + fromcity + "---" + tocity + "---"
                + date + "---" + traincode);
        String lishi = "-1";
        boolean isgetlishi = true;
        if (!Train12306StationInfoUtil.StringIsNull(fromcity) && !Train12306StationInfoUtil.StringIsNull(tocity)) {
            try {
                fromcity = Train12306StationInfoUtil.getThreeByName(fromcity);
                tocity = Train12306StationInfoUtil.getThreeByName(tocity);
                WriteLog.write("TrainorderLishiMethod_getTrainby12306", orderid + "---" + fromcity + "---" + tocity
                        + "---" + date + "---" + traincode);
            }
            catch (Exception e) {
                WriteLog.write("Error_TrainorderLishiMethod_getThreeByName", "" + orderid);
                ExceptionUtil.writelogByException("Error_TrainorderLishiMethod_getThreeByName", e);
            }
            if (Train12306StationInfoUtil.StringIsNull(fromcity) || Train12306StationInfoUtil.StringIsNull(tocity)) {
                isgetlishi = false;
            }
        }
        else {
            isgetlishi = false;
        }
        if (isgetlishi && !Train12306StationInfoUtil.StringIsNull(date)) {
            date = date.split(" ")[0];
            FlightSearch flightSearch = new FlightSearch();
            flightSearch.setGeneral(2);
            for (int i = 0; i < this.lishi_max; i++) {
                lishi = getYupiaobyCreateOrderRep(orderid, fromcity, tocity, date, flightSearch, traincode);
                WriteLog.write("TrainorderLishiMethod_getLishiby12306", fromcity + ":" + tocity + ":" + date + ":"
                        + traincode + "第" + i + "次,返回:" + lishi);
                if (lishi == null || "".equals(lishi) || "-1".equals(lishi)) {
                    try {
                        Thread.sleep(100L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
                else {
                    break;
                }
            }
        }
        return lishi;
    }

    /**
    * 从下单的rep里获取到余票信息
    * @param fromcity
    * @param tocity
    * @param date
    * @param flightSearch
    * @return
    * @time 2015年8月10日 上午11:52:32
    * @author chendong
    */
    private String getYupiaobyCreateOrderRep(long orderid, String fromcity, String tocity, String date,
            FlightSearch flightSearch, String trainno) {
        //结果
        String lishi = "-1";
        //获取REP
        RepServerBean repServerBean = RepServerUtil.dontNeedLoginRepServer();
        //获取成功
        if (!ElongHotelInterfaceUtil.StringIsNull(repServerBean.getUrl())) {
            //查历时
            lishi = getTrainLishiByrepUrl(orderid, fromcity, tocity, date, repServerBean.getUrl(), trainno);
        }
        //释放REP
        RepServerUtil.freeRepServer(repServerBean);
        //返回结果
        return lishi;
    }

    /**
     * 
     * @time 2015年8月10日 下午12:38:32
     * @author chendong
     */
    private String getTrainLishiByrepUrl(long orderid, String fromcity, String tocity, String date, String repUrl,
            String trainno) {
        String lishi = "-1";
        String resultString = get12306JsonDate(orderid, fromcity, tocity, date, repUrl);
        if (resultString != null && !"".equals(resultString) && !"-1".equals(resultString)) {
            JSONObject jsonobject = JSON.parseObject(resultString);
            jsonobject = (JSONObject) jsonobject.get("data");
            JSONArray datasArray = jsonobject.getJSONArray("datas");
            if (datasArray != null) {
                for (int i = 0; i < datasArray.size(); i++) {
                    JSONObject data_index = (JSONObject) datasArray.get(i);
                    String traincode = data_index.getString("station_train_code");// 车次
                    String costTime = data_index.getString("lishi");// 运行时间
                    if (traincode != null && trainno.equals(traincode)) {
                        lishi = costTime;
                        break;
                    }
                }
            }
        }
        return lishi;
    }

    /**
     * 根据出发到达车站三字码和时间获取12306返回的json余票信息
     * 
     * @param orderid
     * @param fromcity 出发站三字码
     * @param tocity    到达站三字码
     * @param date
     * @param repUrl 使用的rep
     * @param trainno
     * @return
     * @time 2016年1月24日 下午6:21:09
     * @author chen
     */
    private String get12306JsonDate(long orderid, String fromcity, String tocity, String date, String repUrl) {
        String resultString = "";
        String paramContent = "params={\"from_station\":\"" + fromcity + "\",\"to_station\":\"" + tocity
                + "\",\"queryDate\":\"" + date + "\"}";
        WriteLog.write("TrainorderLishiMethod_getTrainLishiByrepUrl", orderid + "----->" + repUrl + "?datatypeflag=103"
                + "&" + paramContent);
        resultString = SendPostandGet
                .submitPostTimeOutFiend(repUrl + "?datatypeflag=103", paramContent, "utf-8", 30000).toString();
        return resultString;
    }

    /**
     * 检查余票是否大于当前设置值如果大于返回true,小于返回false
     * 
     * @param orderid
     * @param fromcity
     * @param tocity
     * @param date
     * @param repUrl
     * @param traincode
     * @return
     * @time 2016年1月24日 下午7:21:59
     * @author chen
     */
    public boolean checkYuPiao(long orderid, String fromcity, String tocity, String date, String repUrl,
            String traincode, String seattype) {
        int limitNum = getLimitNum();//限制剩余张数,
        boolean haveYuPiao = true;
        if (limitNum >= 0) {//限制数量大于等于0才做判断
            String lishiString = get12306JsonDate(orderid, fromcity, tocity, date, repUrl);
            //            WriteLog.write("TrainorderLishiMethod_getTrainLishiByrepUrl", orderid + "-----lishiString>" + lishiString);
            haveYuPiao = cheyupiaoByjson(lishiString, seattype, traincode, limitNum);
            //            WriteLog.write("TrainorderLishiMethod_getTrainLishiByrepUrl", orderid + "-----haveYuPiao>" + haveYuPiao);
        }
        return haveYuPiao;
    }

    /**
     * 
     * @return
     * @time 2016年1月26日 下午6:41:29
     * @author chendong
     * @param lishiString 
     * @param seattype 
     * @param traincode 
     * @param limitNum 
     */
    private boolean cheyupiaoByjson(String lishiString, String seattype, String traincode, int limitNum) {
        boolean haveYuPiao = true;
        JSONObject JSONObjectlishi = JSONObject.parseObject(lishiString);
        if (JSONObjectlishi.getJSONObject("data") != null
                && JSONObjectlishi.getJSONObject("data").getJSONArray("datas") != null) {
            JSONArray datas = JSONObjectlishi.getJSONObject("data").getJSONArray("datas");
            for (int i = 0; i < datas.size(); i++) {
                JSONObject dataJSONObject = datas.getJSONObject(i);
                String station_train_code = dataJSONObject.getString("station_train_code");
                //String qtxb_num = dataJSONObject.getString("qt_num");// 其他席别余票数量
                if (traincode.equals(station_train_code)) {
                    if ("商务座".equals(seattype)) {
                        int i_swzyp = getNum(dataJSONObject.getString("swz_num"));// 商务座余票
                        if (i_swzyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("特等座".equals(seattype)) {
                        int i_tdzyp = getNum(dataJSONObject.getString("tz_num"));// 特等座余票
                        if (i_tdzyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("一等座".equals(seattype)) {
                        String rz1yp = dataJSONObject.getString("zy_num");// 一等软余票
                        int i_rz1yp = getNum(rz1yp);
                        if (i_rz1yp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("二等座".equals(seattype) || seattype.contains("二等座")) {
                        String rz2yp = dataJSONObject.getString("ze_num");// 二等软座余票
                        int i_rz2yp = getNum(rz2yp);
                        String wzyp = dataJSONObject.getString("wz_num");// 无座余票
                        int i_wzyp = getNum(wzyp);// 处理 余票数量是有的问题
                        if (i_rz2yp + i_wzyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("一等软座".equals(seattype)) {
                    }
                    else if ("二等软座".equals(seattype)) {
                    }
                    else if ("高级软卧".equals(seattype)) {
                        String gwyp = dataJSONObject.getString("gr_num");// 高级软卧余票
                        int i_gwyp = getNum(gwyp);
                        if (i_gwyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("软卧".equals(seattype)) {
                        String rwyp = dataJSONObject.getString("rw_num");// 软卧余票
                        int i_rwyp = getNum(rwyp);
                        if (i_rwyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("硬卧".equals(seattype)) {
                        String ywyp = dataJSONObject.getString("yw_num");// 硬卧余票
                        int i_ywyp = getNum(ywyp);
                        if (i_ywyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("软座".equals(seattype)) {
                        String rzyp = dataJSONObject.getString("rz_num");// 软座余票
                        int i_rzyp = getNum(rzyp);
                        if (i_rzyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if ("硬座".equals(seattype) || "硬卧代硬座".equals(seattype)) {
                        String yzyp = dataJSONObject.getString("yz_num");// 硬座余票
                        int i_yzyp = getNum(yzyp);// 处理 余票数量是有的问题
                        String wzyp = dataJSONObject.getString("wz_num");// 无座余票
                        int i_wzyp = getNum(wzyp);// 处理 余票数量是有的问题
                        if (i_yzyp + i_wzyp < limitNum) {
                            haveYuPiao = false;
                            break;
                        }
                    }
                    else if (seattype.contains("高级动卧")) {
                    }
                    else if (seattype.contains("动卧")) {
                    }
                }
            }
        }
        return haveYuPiao;
    }

    /**
     * 
     * @return
     * @time 2016年1月25日 下午4:08:34
     * @author chendong
     */
    private int getLimitNum() {
        try {
            if ((System.currentTimeMillis() - TrainorderLishiMethod.lastUpdateTimitNumTime) > 60000) {//大于一分钟的才更新
                TrainorderLishiMethod.lastUpdateTimitNumTime = System.currentTimeMillis();
                String TrainCreateOrderLimitNum = getSysconfigStringbydb("TrainCreateOrderLimitNum");
                if (Integer.parseInt(TrainCreateOrderLimitNum) > 0) {
                    TrainorderLishiMethod.limitNum = Integer.parseInt(TrainCreateOrderLimitNum);
                }
            }
        }
        catch (Exception e) {
        }
        return TrainorderLishiMethod.limitNum;
    }

    private int getNum(String num) {
        int inum = 0;
        try {
            if ("--".equals(num)) {
            }
            else {
                inum = Integer.parseInt(num);
            }
        }
        catch (Exception e) {
        }
        return inum;
    }
}
