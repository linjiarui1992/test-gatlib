package com.ccservice.mixdata;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Date;
import java.util.List;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccervice.huamin.update.PHUtil;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;

/**
 * 获取去哪儿酒店
 * 
 * 经纬度 http://hotel.qunar.com/detail/detailMapData.jsp?seq=beijing_city_298&type=traffic,canguan,jingdian,ent&callback=XQScript_4
 * 
 * 附近酒店 http://hotel.qunar.com/render/detailRecommend.jsp?hotelSEQ=beijing_city_298
 * 
 * mixKey http://hotel.qunar.com/city/beijing_city/#fromDate=2013-11-15&toDate=2013-11-16&from=hotellist&QHFP=ZSL_A491C891&bs=&bc=北京
 * 
 * 图片 http://hotel.qunar.com/render/hotelDetailAllImage.jsp?hotelseq=beijing_city_298&_=1384505474403
 * 
 * 房型 http://hotel.qunar.com/render/detailV2.jsp?fromDate=2013-11-18&toDate=2013-11-19&cityurl=beijing_city&HotelSEQ=beijing_city_298&mixKey=0f646640f8e1f85f40d1384752531964155W5sulf2yGELwiOZ3xejc&roomId=&lastupdate=1384505404158&basicData=1&v=1384505490067&cn=4&requestID=c0a8e97d-m055s-718t&__jscallback=jQuery18306666195504367352_1384505473715&_=1384505490068
 * 
 * 最低价 http://hotel.qunar.com/price/api3.jsp?output=json1.1&showPrices=0&cityurl=beijing_city&fromDate=2013-11-20&toDate=2013-11-21&ids=beijing_city_298&v=0.2778531233780086&requestor=RT_HSLIST_D
 * 
 * 点评 http://review.qunar.com/api/h/beijing_city_298/detail/rank/v1/page/1?__jscallback=jQuery183038196931104175746_1384763760694&rate=all&_=1384763762022
 * 
 * 最新预订时间、在线人数 http://hotel.qunar.com/render/hotelLastMsg.jsp?hotelseq=beijing_city_298&t=1384763761951
 * 
 * 评分 http://review.qunar.com/api/h/beijing_city_298/detail?__jscallback=jQuery183038196931104175746_1384763760694&_=1384763761933
 * 
 * 详细信息 http://hotel.qunar.com/city/beijing_city/dt-190/?tag=beijing_city#fromDate=2013-11-20&toDate=2013-11-21&q=&from=hotellist&showMap=0&qptype=&QHFP=ZSS_A13A9337&QHPR=1_1_1_0
 * 
 */
@SuppressWarnings("unchecked")
public class FindQunarHotel implements Job {
    //酒店品牌
    private static String findChainInfo = "select ID,C_FULLNAME from T_CHAININFO where C_FULLNAME is not null";

    private static List<Chaininfo> chaininfoList = Server.getInstance().getSystemService()
            .findMapResultBySql(findChainInfo, null);

    //行政区、商业区
    private static String findRegion = "select ID,C_NAME,C_CITYID,C_TYPE from T_REGION where C_NAME is not null and C_CITYID > 0 and C_TYPE in ('1','2')";

    private static List<Region> regionList = Server.getInstance().getSystemService()
            .findMapResultBySql(findRegion, null);

    private static int totalpage = 0;//总共页数

    private static int pageindex = 0;//当前页数

    private static int pagesize = 0;//每页多少家酒店

    private static String mixKey = "0fcef4b4b15ef1a2f0d138492860690312vv0ylLG2uwfiZyIci5";//默认一个MixKey

    private static String previousQunarCity = "";//上一个请求去哪儿城市

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        List<City> citys = Server.getInstance().getHotelService()
                .findAllCity("where C_QUNARCODE is not null", "order by C_SUPERIORCITY,ID", -1, 0);
        for (City c : citys) {
            if (!UpdateElDataUtil.StringIsNull(c.getQunarcode())) {
                updateQunarHotelAll(c);
            }
        }
        System.out.println("结束更新去哪儿酒店...");
    }

    public static void main(String[] args) throws Exception {
        List<City> citys = Server.getInstance().getHotelService()
                .findAllCity("where C_QUNARCODE is not null", "order by C_SUPERIORCITY,ID", -1, 0);
        for (City c : citys) {
            if (!UpdateElDataUtil.StringIsNull(c.getQunarcode())) {
                updateQunarHotelAll(c);
            }
        }
        System.out.println("结束更新去哪儿酒店...");
    }

    @SuppressWarnings("rawtypes")
    public static void updateQunarHotelAll(City city) {
        String json = "";
        try {
            //请求下一城市，重置条件
            if (!previousQunarCity.equals(city.getQunarcode())) {
                totalpage = 0;
                pageindex = 1;
                pagesize = 500;
                previousQunarCity = city.getQunarcode();
            }
            String fromDate = UpdateElDataUtil.getCurrentDate();
            String toDate = UpdateElDataUtil.getAddDate(fromDate, 1);
            long currentTimeMillis = System.currentTimeMillis();
            String strUrl = "http://hotel.qunar.com/render/renderAPIList.jsp?attrs=L0F4L3C1,ZO1FcGJH,J6TkcChI,HCEm2cI6,08F7hM4i,"
                    + "8dksuR_,YRHLp-jc,pl6clDL0,HFn32cI6,vf_x4Gjt,2XkzJryU,vNfnYBK6,TDoolO-H,pk4QaDyF,x0oSHP6u,z4VVfNJo,5_VrVbqO,"
                    + "VAuXapLv,U1ur4rJN,px3FxFdF,xaSZV4wU,ZZY89LZZ,HGYGeXFY,ownT_WG6,0Ie44fNU,yYdMIL83,MMObDrW4,dDjWmcqr,Y0LTFGFh,"
                    + "6X7_yoo3,8F2RFLSO,U3rHP23d&showAllCondition=0&showBrandInfo=0&showNonPrice=1&showFullRoom=1&showPromotion=1&"
                    + "showTopHotel=1&showGroupShop=1&output=json1.1&v=0.7880550532970746&requestTime="
                    + currentTimeMillis
                    + "&mixKey="
                    + mixKey
                    + "&requestor=RT_HSLIST&cityurl="
                    + city.getQunarcode()
                    + "&fromDate="
                    + fromDate
                    + "&toDate="
                    + toDate
                    + "&limit="
                    + (pageindex - 1)
                    * pagesize
                    + "%2C"
                    + pagesize + "&needFP=0&__jscallback=XQScript_7";
            System.out.println("POST请求去哪儿[" + city.getName() + "]列表第[" + pageindex + "]页酒店：" + strUrl);
            json = PHUtil.submitPost(strUrl, "").toString();
            if (!UpdateElDataUtil.StringIsNull(json)) {
                json = json.replace("XQScript_7(", "");
                json = json.substring(0, json.lastIndexOf(")"));
                JSONObject obj = JSONObject.fromObject(json);
                if (obj.containsKey("errmsg")) {
                    //mixKey失效 XQScript_7({"ret":false,"errcode":110,"errmsg":"invalid cookie"})
                    if ("invalid cookie".equals(obj.getString("errmsg"))) {
                        //获取mixKey
                        mixKey = GetQunarMixKey.get(city.getQunarcode());
                        //重新请求
                        updateQunarHotelAll(city);
                    }
                    //访问频繁，需要验证码 XQScript_7({"ret":false,"errcode":110,"errmsg":"invalid vcd cookie"})
                    else if ("invalid vcd cookie".equals(obj.getString("errmsg"))
                            || "suspect user".equals(obj.get("errmsg"))) {
                        System.out.println("访问频繁，等待10秒...");
                        //等10秒
                        Thread.sleep(1000 * 10);
                        //重新请求
                        updateQunarHotelAll(city);
                    }
                    else {
                        throw new Exception("ERROR");
                    }
                }
                //解析去哪儿返回信息
                else {
                    JSONObject info = obj.getJSONObject("info");
                    String qCityUrl = info.getString("cityurl");//去哪儿城市ID
                    String qCityName = info.getString("cityName");//去哪儿城市名称
                    if (!city.getQunarcode().equals(qCityUrl)) {
                        throw new Exception("城市不一致，" + city.getName() + "[" + city.getQunarcode() + "] / " + qCityName
                                + "[" + qCityUrl + "]");
                    }
                    //酒店列表
                    JSONArray hotels = obj.getJSONArray("hotels");
                    for (int i = 0; i < hotels.size(); i++) {
                        //正式酒店
                        Hotelall hotelall = new Hotelall();
                        hotelall.setType(1);//国内
                        hotelall.setLanguage(0);//语言
                        hotelall.setSourcetype(168l);//酒店来源，168：去哪儿，用于标示去哪儿新增酒店
                        hotelall.setCityid(city.getId());
                        hotelall.setProvinceid(city.getProvinceid());
                        hotelall.setCountryid(city.getCountryid());
                        //去哪儿酒店
                        JSONObject hotel = hotels.getJSONObject(i);
                        //去哪儿酒店属性
                        JSONObject attrs = hotel.getJSONObject("attrs");
                        //去哪儿酒店ID
                        String id = hotel.getString("id");
                        //酒店名称
                        String hotelName = attrs.getString("hotelName");
                        if (UpdateElDataUtil.StringIsNull(id) || UpdateElDataUtil.StringIsNull(hotelName)) {
                            continue;
                        }
                        hotelall.setQunarId(id);
                        hotelall.setName(hotelName.trim());
                        System.out.println((i + 1) + "-----当前去哪儿酒店-----" + hotelName + "-----" + id);
                        //最低价格
                        double price = hotel.containsKey("price") ? hotel.getDouble("price") : 0;
                        if (price > 0)
                            hotelall.setStartprice(price);
                        //状态
                        String os = hotel.getString("os");//营业中/0、筹建中/1、暂停营业/2、已停业/3
                        int priceStatus = hotel.getInt("priceStatus"); //价格状态 0：不可用； 1：可用
                        if ("营业中".equals(os) && priceStatus == 1) {
                            hotelall.setState(3);
                            hotelall.setStatedesc("该酒店可用");
                        }
                        else {
                            hotelall.setState(0);
                            if ("营业中".equals(os))
                                os = "该酒店暂不可用";
                            hotelall.setStatedesc(os);
                        }
                        //星级
                        String hotelStars = attrs.getString("hotelStars");
                        int star = 0;
                        if ("5".equals(hotelStars) || "4".equals(hotelStars) || "3".equals(hotelStars)
                                || "2".equals(hotelStars) || "1".equals(hotelStars)) {
                            star = Integer.parseInt(hotelStars);
                        }
                        hotelall.setStar(star);
                        //档次 4：五星/豪华型；3：四星/高档型；2：三星/舒适；1：经济型；0，5：二星/其他
                        String dangci = attrs.getString("dangci");
                        int repair = 4;//经济
                        if ("4".equals(dangci))
                            repair = 1;
                        if ("3".equals(dangci))
                            repair = 2;
                        if ("2".equals(dangci))
                            repair = 3;
                        hotelall.setRepair(repair);
                        //酒店品牌
                        String hotelBrand = attrs.getString("hotelBrand");

                        if ("jiali".equals(hotelBrand) || "shengmao".equals(hotelBrand))
                            hotelBrand = "XiangGeLiLa";//嘉里及盛贸、香格里拉旗下
                        if ("lvjukuaijie".equals(hotelBrand))
                            hotelBrand = "LvJu";//旅居快捷-->旅居
                        if ("heyi".equals(hotelBrand))
                            hotelBrand = "RuJia";//如家和颐、如家旗下
                        if ("juzishuijing".equals(hotelBrand) || "juzijingxuan".equals(hotelBrand))
                            hotelBrand = "juzi";//桔子水晶、桔子精选
                        if ("yibisishangpin".equals(hotelBrand))
                            hotelBrand = "YiBiSi";//宜必思尚品
                        if ("kaiyuanmingdu".equals(hotelBrand) || "kaiyuandujia".equals(hotelBrand)
                                || "kaiyuanmanju".equals(hotelBrand)) {
                            hotelBrand = "kaiyuan";//开元名都、开元度假村、开元曼居
                        }
                        if ("tanglayaxiu".equals(hotelBrand) || "haihangkuaijie".equals(hotelBrand)
                                || "haihangshangwu".equals(hotelBrand)) {
                            hotelBrand = "haihangdajiudian"; //唐拉雅秀，海航旗下
                        }
                        if ("haohuaqianxi".equals(hotelBrand))
                            hotelBrand = "QianXi";//豪华千禧-->千禧
                        if ("weijingguoji".equals(hotelBrand))
                            hotelBrand = "WeiJing";//维景国际-->维景
                        if ("quanji".equals(hotelBrand))
                            hotelBrand = "HanTing";//全季酒店、汉庭旗下
                        if ("jiaridujia".equals(hotelBrand))
                            hotelBrand = "jiari";//假日度假-->假日
                        if ("quhuhuxiaoshi".equals(hotelBrand))
                            hotelBrand = "quhuhu";//去呼呼小时房-->去呼呼
                        if ("gelinlianmeng".equals(hotelBrand))
                            hotelBrand = "GeLinHaoTai";//格林联盟、格林豪泰旗下
                        if ("shangyue".equals(hotelBrand) || "ducheng".equals(hotelBrand))
                            hotelBrand = "jinjiangjiudian";//商悦及锦江都城、锦江旗下
                        if ("junyi".equals(hotelBrand))
                            hotelBrand = "shangke";//骏怡城际、尚客优旗下
                        if ("yongshengkujia".equals(hotelBrand))
                            hotelBrand = "yongshengxiandai";//永生酷家、永生现代旗下
                        if ("haiyijunchuo".equals(hotelBrand))
                            hotelBrand = "HaiYi";//海逸君绰、海逸旗下
                        if ("junlandujia".equals(hotelBrand))
                            hotelBrand = "JunLan";//君澜度假、君澜旗下
                        if ("3hao".equals(hotelBrand))
                            hotelBrand = "WeiYeNa";//维也纳3好、维也纳旗下

                        if (!UpdateElDataUtil.StringIsNull(hotelBrand) && chaininfoList != null
                                && chaininfoList.size() > 0) {
                            for (int j = 0; j < chaininfoList.size(); j++) {
                                Map map = (Map) chaininfoList.get(j);
                                if (hotelBrand.equalsIgnoreCase(map.get("C_FULLNAME").toString())) {
                                    hotelall.setChaininfoid(Long.parseLong(map.get("ID").toString()));
                                    break;
                                }
                            }
                        }
                        //行政区
                        String HotelArea = attrs.getString("HotelArea");
                        if (!UpdateElDataUtil.StringIsNull(HotelArea) && regionList != null && regionList.size() > 0) {
                            for (int j = 0; j < regionList.size(); j++) {
                                Map map = (Map) regionList.get(j);
                                if ((city.getId() == Long.parseLong(map.get("C_CITYID").toString()))
                                        && HotelArea.equals(map.get("C_NAME").toString())
                                        && Long.parseLong(map.get("C_TYPE").toString()) == 2) {
                                    hotelall.setRegionid1(Long.parseLong(map.get("ID").toString()));
                                    break;
                                }
                            }
                        }
                        //商业区
                        String tradingArea = attrs.getString("tradingArea");
                        if (!UpdateElDataUtil.StringIsNull(tradingArea) && regionList != null && regionList.size() > 0) {
                            for (int j = 0; j < regionList.size(); j++) {
                                Map map = (Map) regionList.get(j);
                                if ((city.getId() == Long.parseLong(map.get("C_CITYID").toString()))
                                        && Long.parseLong(map.get("C_TYPE").toString()) == 1) {
                                    if (tradingArea.endsWith("商圈"))
                                        tradingArea = tradingArea.substring(0, tradingArea.length() - 2);
                                    if (tradingArea.endsWith("地区"))
                                        tradingArea = tradingArea.substring(0, tradingArea.length() - 2);
                                    if (tradingArea.endsWith("商业区"))
                                        tradingArea = tradingArea.substring(0, tradingArea.length() - 3);
                                    //本地区域名称
                                    String localName = map.get("C_NAME").toString();
                                    if (localName.endsWith("商圈"))
                                        localName = localName.substring(0, localName.length() - 2);
                                    if (localName.endsWith("地区"))
                                        localName = localName.substring(0, localName.length() - 2);
                                    if (localName.endsWith("商业区"))
                                        localName = localName.substring(0, localName.length() - 3);
                                    //临时标示
                                    boolean tempflag = false;
                                    if (tradingArea.equals(localName))
                                        tempflag = true;
                                    //北京站、建国门地区
                                    if (!tempflag && localName.contains("、")) {
                                        for (int k = 0; k < localName.split("、").length; k++) {
                                            if (tradingArea.equals(localName.split("、")[k])) {
                                                tempflag = true;
                                                break;
                                            }
                                        }
                                    }
                                    //鼓楼/后海/新街口
                                    if (!tempflag && localName.contains("/")) {
                                        for (int k = 0; k < localName.split("/").length; k++) {
                                            if (tradingArea.equals(localName.split("/")[k])) {
                                                tempflag = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (tempflag) {
                                        hotelall.setRegionid2(Long.parseLong(map.get("ID").toString()));
                                        break;
                                    }
                                }
                            }
                        }
                        //酒店地址
                        String hotelAddress = attrs.getString("hotelAddress");
                        hotelall.setAddress(hotelAddress);
                        //一句话描述
                        String oneSentence = attrs.getString("oneSentence");
                        hotelall.setDescription(oneSentence);
                        //百度经纬度
                        String bpoint = attrs.getString("bpoint");
                        if (!UpdateElDataUtil.StringIsNull(bpoint) && bpoint.contains(",")
                                && bpoint.split(",").length == 2) {
                            hotelall.setBLat(Double.valueOf(bpoint.split(",")[0]));//纬度
                            hotelall.setBLng(Double.valueOf(bpoint.split(",")[1]));//经度
                        }
                        //谷歌经纬度
                        String gpoint = attrs.getString("gpoint");//39.88205,116.43451
                        if (!UpdateElDataUtil.StringIsNull(gpoint) && gpoint.contains(",")
                                && gpoint.split(",").length == 2) {
                            hotelall.setLat(Double.valueOf(gpoint.split(",")[0]));//纬度
                            hotelall.setLng(Double.valueOf(gpoint.split(",")[1]));//经度
                        }
                        String facilities = "";
                        String serviceitem = "";
                        //是否有室内泳池 Y/N
                        String HasIndoorPool = attrs.getString("HasIndoorPool");
                        if ("Y".equals(HasIndoorPool.toUpperCase())) {
                            facilities += "9,";
                            serviceitem += "室内游泳池、";
                        }
                        //是否有健身房 Y/N
                        String HasFitnessRoom = attrs.getString("HasFitnessRoom");
                        if ("Y".equals(HasFitnessRoom.toUpperCase())) {
                            facilities += "11,";
                            serviceitem += "健身房、";
                        }
                        //是否有商务中心 Y/N
                        String HasBusinessCenter = attrs.getString("HasBusinessCenter");
                        if ("Y".equals(HasBusinessCenter.toUpperCase())) {
                            facilities += "12,";
                            serviceitem += "商务中心、";
                        }
                        //是否有会议或宴会空间 Y/N
                        String HasMeetingORBanquetSpace = attrs.getString("HasMeetingORBanquetSpace");
                        if ("Y".equals(HasMeetingORBanquetSpace.toUpperCase())) {
                            facilities += "13,";
                            serviceitem += "会议室、";
                        }
                        //是否有餐厅 Y/N
                        String HasRestaurant = attrs.getString("HasRestaurant");
                        if ("Y".equals(HasRestaurant.toUpperCase())) {
                            facilities += "14,";
                            serviceitem += "餐厅、";
                        }
                        //是否有无烟房
                        String HasNonSmokingAvailable = attrs.getString("HasNonSmokingAvailable");
                        if ("Y".equals(HasNonSmokingAvailable.toUpperCase())) {
                            serviceitem += "无烟房、";
                        }
                        if (facilities.endsWith(","))
                            facilities = facilities.substring(0, facilities.length() - 1);
                        hotelall.setFacilities(facilities);
                        if (serviceitem.endsWith("、")) {
                            serviceitem = serviceitem.substring(0, serviceitem.length() - 1);
                            hotelall.setServiceitem(serviceitem);
                        }
                        //列表图片
                        String imageID = attrs.getString("imageID");
                        //匹配本地
                        String sql = "where c_qunarid = '" + id + "'";
                        List<Hotelall> locallist = Server.getInstance().getHotelService()
                                .findAllHotelall(sql, "", -1, 0);
                        boolean detailflag = false;
                        if (locallist == null || locallist.size() == 0) {
                            detailflag = true;
                        }
                        else {
                            Hotelall local = locallist.get(0);
                            if (UpdateElDataUtil.StringIsNull(local.getMarkettell())
                                    || UpdateElDataUtil.StringIsNull(local.getFax1())
                                    || UpdateElDataUtil.StringIsNull(local.getDescription())
                                    || UpdateElDataUtil.StringIsNull(local.getServiceitem())
                                    || UpdateElDataUtil.StringIsNull(local.getRoomAmenities())) {
                                //detailflag = true;
                            }
                        }
                        if (detailflag) {
                            //请求酒店详细信息
                            try {
                                getHotelInfo(hotelall, city);
                            }
                            catch (Exception e) {
                            }
                        }
                        //新增
                        if (locallist == null || locallist.size() == 0) {
                            //最后更新时间
                            hotelall.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            hotelall = Server.getInstance().getHotelService().createHotelall(hotelall);
                            System.out.println("新增酒店-----" + hotelall.getId() + "-----" + hotelall.getName());
                        }
                        //更新
                        else {
                            Hotelall local = locallist.get(0);
                            //赋值
                            boolean updateflag = false;
                            if (hotelall.getStartprice() != null && hotelall.getStartprice().doubleValue() > 0
                                    && (local.getStartprice() == null || local.getStartprice().doubleValue() == 0)) {
                                updateflag = true;
                                local.setStartprice(hotelall.getStartprice());
                            }
                            if (!hotelall.getType().equals(local.getType())) {
                                updateflag = true;
                                local.setType(hotelall.getType());
                            }
                            if (!hotelall.getLanguage().equals(local.getLanguage())) {
                                updateflag = true;
                                local.setLanguage(hotelall.getLanguage());
                            }
                            //状态 通过sourcetype判断是否是通过去哪儿新增的酒店，新增酒店置状态
                            if (local.getSourcetype() != null && local.getSourcetype().longValue() == 168
                                    && !hotelall.getState().equals(local.getState())) {
                                updateflag = true;
                                local.setState(hotelall.getState());
                            }
                            if (!hotelall.getCityid().equals(local.getCityid())) {
                                updateflag = true;
                                local.setCityid(hotelall.getCityid());
                            }
                            if (hotelall.getProvinceid() != null
                                    && !hotelall.getProvinceid().equals(local.getProvinceid())) {
                                updateflag = true;
                                local.setProvinceid(hotelall.getProvinceid());
                            }
                            if (hotelall.getCountryid() != null
                                    && !hotelall.getCountryid().equals(local.getCountryid())) {
                                updateflag = true;
                                local.setCountryid(hotelall.getCountryid());
                            }
                            if (!hotelall.getStar().equals(local.getStar())) {
                                if ((local.getSourcetype() != null && local.getSourcetype().longValue() == 168)
                                        || (local.getStar() == null || local.getStar().intValue() == 0)) {
                                    updateflag = true;
                                    local.setStar(hotelall.getStar());
                                }
                            }
                            if (!hotelall.getRepair().equals(local.getRepair())) {
                                updateflag = true;
                                local.setRepair(hotelall.getRepair());
                            }
                            if (hotelall.getChaininfoid() != null
                                    && (local.getChaininfoid() == null || local.getChaininfoid().longValue() != hotelall
                                            .getChaininfoid().longValue())) {
                                updateflag = true;
                                local.setChaininfoid(hotelall.getChaininfoid());
                            }
                            if (hotelall.getRegionid1() != null && local.getRegionid1() == null) {
                                updateflag = true;
                                local.setRegionid1(hotelall.getRegionid1());
                            }
                            if (hotelall.getRegionid2() != null && local.getRegionid2() == null) {
                                updateflag = true;
                                local.setRegionid2(hotelall.getRegionid2());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getAddress())
                                    && UpdateElDataUtil.StringIsNull(local.getAddress())) {
                                updateflag = true;
                                local.setAddress(hotelall.getAddress());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getMarkettell())
                                    && UpdateElDataUtil.StringIsNull(local.getMarkettell())) {
                                updateflag = true;
                                local.setMarkettell(hotelall.getMarkettell());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getFax1())
                                    && UpdateElDataUtil.StringIsNull(local.getFax1())) {
                                updateflag = true;
                                local.setFax1(hotelall.getFax1());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getDescription())
                                    && UpdateElDataUtil.StringIsNull(local.getDescription())) {
                                updateflag = true;
                                local.setDescription(hotelall.getDescription());
                            }
                            //经、纬度直接用去哪儿
                            if (hotelall.getBLat() != null && hotelall.getBLat().doubleValue() > 0
                                    && !hotelall.getBLat().equals(local.getBLat())) {
                                updateflag = true;
                                local.setBLat(hotelall.getBLat());
                            }
                            if (hotelall.getBLng() != null && hotelall.getBLng().doubleValue() > 0
                                    && !hotelall.getBLng().equals(local.getBLng())) {
                                updateflag = true;
                                local.setBLng(hotelall.getBLng());
                            }
                            if (hotelall.getLat() != null && hotelall.getLat().doubleValue() > 0
                                    && !hotelall.getLat().equals(local.getLat())) {
                                updateflag = true;
                                local.setLat(hotelall.getLat());
                            }
                            if (hotelall.getLng() != null && hotelall.getLng().doubleValue() > 0
                                    && !hotelall.getLng().equals(local.getLng())) {
                                updateflag = true;
                                local.setLng(hotelall.getLng());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getFacilities())
                                    && UpdateElDataUtil.StringIsNull(local.getFacilities())) {
                                updateflag = true;
                                local.setFacilities(hotelall.getFacilities());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getServiceitem())
                                    && UpdateElDataUtil.StringIsNull(local.getServiceitem())) {
                                updateflag = true;
                                local.setServiceitem(hotelall.getServiceitem());
                            }
                            if (!UpdateElDataUtil.StringIsNull(hotelall.getRoomAmenities())
                                    && UpdateElDataUtil.StringIsNull(local.getRoomAmenities())) {
                                updateflag = true;
                                local.setRoomAmenities(hotelall.getRoomAmenities());
                            }
                            //更新
                            if (updateflag) {
                                local.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                                Server.getInstance().getHotelService().updateHotelallIgnoreNull(local);
                                System.out.println("更新酒店-----" + local.getId() + "-----" + local.getName());
                            }
                            else {
                                //System.out.println("匹配酒店-----" + local.getId() + "-----" + local.getName());
                            }
                            hotelall = local;
                        }
                        //存列表页图片
                        if (hotelall.getId() > 0 && !UpdateElDataUtil.StringIsNull(imageID)) {
                            List<Hotelimage> imageList = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllHotelimage(
                                            "where C_ZSHOTELID = " + hotelall.getId() + " and C_SIZETYPE = 3", "", -1,
                                            0);
                            //无列表图片，进行图片存储
                            if (imageList == null || imageList.size() == 0) {
                                Hotelimage hotelImage = new Hotelimage();
                                hotelImage.setPath(imageID);//临时
                                hotelImage.setType(10);
                                hotelImage.setDescription("其他");
                                hotelImage.setHotelid(null); //去哪儿酒店，只存正式酒店ID
                                hotelImage.setLanguage(0);
                                hotelImage.setSizeType(3);//小图，用于列表页
                                hotelImage.setZshotelid(hotelall.getId());
                                hotelImage.setIsNewFlag(2);//图片来源 2：去哪儿
                                //保存图片
                                saveImage(hotelImage, city);
                            }
                        }
                        //存一张大图
                        if (hotelall.getId() > 0 && !UpdateElDataUtil.StringIsNull(imageID)
                                && imageID.endsWith("76.jpg")) {
                            List<Hotelimage> imageList = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllHotelimage(
                                            "where C_ZSHOTELID = " + hotelall.getId() + " and C_SIZETYPE != 3", "", -1,
                                            0);
                            //无大图
                            if (imageList == null || imageList.size() == 0) {
                                Hotelimage hotelImage = new Hotelimage();
                                hotelImage.setPath(imageID.substring(0, imageID.length() - "76.jpg".length())
                                        + "i312.jpg");//临时
                                hotelImage.setType(10);
                                hotelImage.setDescription("其他");
                                hotelImage.setHotelid(null); //去哪儿酒店，只存正式酒店ID
                                hotelImage.setLanguage(0);
                                hotelImage.setSizeType(1);//大图，312*208
                                hotelImage.setZshotelid(hotelall.getId());
                                hotelImage.setIsNewFlag(2);//图片来源 2：去哪儿
                                //保存图片
                                saveImage(hotelImage, city);
                            }
                        }
                    }
                    //酒店合计
                    int count = info.getInt("count");
                    if (count > pagesize && pageindex != totalpage) {
                        totalpage = count / pagesize;
                        if (count % pagesize > 0) {
                            totalpage = totalpage + 1;
                        }
                        if (pageindex < totalpage) {
                            pageindex++;
                        }
                        updateQunarHotelAll(city);
                    }
                }
            }
        }
        catch (Exception e) {
            System.out.println("请求去哪儿酒店列表异常，异常信息为：" + e.getMessage());
            //请求下一页
            if (totalpage > 0 && pageindex < totalpage) {
                pageindex++;
                updateQunarHotelAll(city);
            }
        }
    }

    private static String str(String html, String str) {
        int idx = html.indexOf(str);
        if (idx < 0) {
            return html;
        }
        else {
            return html.substring(idx + str.length());
        }
    }

    private static String end(String html, String end) {
        int idx = html.indexOf(end);
        if (idx < 0) {
            return html;
        }
        else {
            return html.substring(0, idx);
        }
    }

    /**
     * 酒店详细信息
     */
    private static void getHotelInfo(Hotelall hotelall, City city) throws Exception {
        String fromDate = UpdateElDataUtil.getCurrentDate();
        String toDate = UpdateElDataUtil.getAddDate(fromDate, 1);
        String id = hotelall.getQunarId();
        String url = "http://hotel.qunar.com/city/" + city.getQunarcode() + "/dt-"
                + id.substring(id.lastIndexOf("_") + 1, id.length()) + "/?tag=" + city.getQunarcode() + "#fromDate="
                + fromDate + "&toDate=" + toDate
                + "&q=&from=hotellist&showMap=0&qptype=&QHFP=ZSS_A13A9337&QHPR=1_1_1_0";
        System.out.println("POST请求去哪儿酒店[ " + hotelall.getName() + " ]详细信息：" + url);
        String html = PHUtil.submitPost(url, "").toString().toString();
        if (html.contains("<div class=\"hotel-introduce\"  id=\"descContent\">")) {
            html = html.substring(html.indexOf("<!--有酒店概况:star-->"), html.indexOf("<!--有酒店概况:end-->"));
            String str = "<cite>电话";
            String end = "</cite>";
            //电话
            if (html.contains(str)) {
                String phone = str(html, str);
                phone = end(phone, end);
                hotelall.setMarkettell(phone);
            }
            //传真
            str = "<cite>传真";
            if (html.contains(str)) {
                String fax = str(html, str);
                fax = end(fax, end);
                hotelall.setFax1(fax);
            }
            //房间设施
            str = "<dt>房间设施</dt>";
            if (html.contains(str)) {
                String roomAmenities = str(html, str);
                str = "<dd>";
                end = "</dd>";
                roomAmenities = str(roomAmenities, str);
                roomAmenities = end(roomAmenities, end);
                String[] arys = roomAmenities.split("<span class=\"each-facility\">");
                roomAmenities = "";
                for (int i = 0; i < arys.length; i++) {
                    if (arys[i].contains("提供此设施")) {
                        String roomAmenitie = str(arys[i], "</i>");
                        roomAmenitie = end(roomAmenitie, "</span>");
                        roomAmenitie = str(roomAmenitie, "<b class=\"gray\">");
                        roomAmenitie = end(roomAmenitie, "</b>");
                        roomAmenities += roomAmenitie + "、";
                    }
                }
                if (roomAmenities.endsWith("、")) {
                    roomAmenities = roomAmenities.substring(0, roomAmenities.length() - 1);
                    hotelall.setRoomAmenities(roomAmenities);
                }
            }
            //酒店服务、设施
            str = "<dt>酒店服务</dt>";
            String serviceitem = "";
            if (html.contains(str)) {
                String HotelService = str(html, str);
                str = "<dd>";
                end = "</dd>";
                HotelService = str(HotelService, str);
                HotelService = end(HotelService, end);
                String[] arys = HotelService.split("<span class=\"each-facility\">");
                for (int i = 0; i < arys.length; i++) {
                    if (arys[i].contains("提供此服务")) {
                        String service = str(arys[i], "</i>");
                        service = end(service, "</span>");
                        service = end(service, "<b class=");
                        serviceitem += service + "、";
                    }
                }
            }
            str = "<dt>酒店设施</dt>";
            if (html.contains(str)) {
                String HotelFacilities = str(html, str);
                str = "<dd>";
                end = "</dd>";
                HotelFacilities = str(HotelFacilities, str);
                HotelFacilities = end(HotelFacilities, end);
                String[] arys = HotelFacilities.split("<span class=\"each-facility\">");
                for (int i = 0; i < arys.length; i++) {
                    if (arys[i].contains("提供此设施")) {
                        String HotelFacilitie = str(arys[i], "</i>");
                        HotelFacilitie = end(HotelFacilitie, "</span>");
                        HotelFacilitie = str(HotelFacilitie, "<b class=\"gray\">");
                        HotelFacilitie = end(HotelFacilitie, "</b>");
                        serviceitem += HotelFacilitie + "、";
                    }
                }
            }
            if (serviceitem.endsWith("、")) {
                serviceitem = serviceitem.substring(0, serviceitem.length() - 1);
                hotelall.setServiceitem(serviceitem);
            }
            //酒店介绍
            str = "<dt>酒店简介</dt>";
            if (html.contains(str)) {
                String desc = str(html, str);
                str = "<div class=\"introduce-all hide\" id=\"js_hotelInfo_descAll\">";
                if (desc.contains(str)) {
                    desc = str(html, str);
                    desc = str(desc, "<p>");
                    desc = end(desc, "<p/>");
                    desc = end(desc, "<br/>");
                    hotelall.setDescription(desc.trim());
                }
                else {
                    str = "<div class=\"introduce\">";
                    if (desc.contains(str)) {
                        desc = str(html, str);
                        desc = str(desc, "<p>");
                        desc = end(desc, "<p/>");
                        desc = end(desc, "<br/>");
                        hotelall.setDescription(desc.trim());
                    }
                }
            }
        }
    }

    /**
     * 保存酒店列表图片，一个酒店只存一张
     */
    private static void saveImage(Hotelimage img, City city) {
        try {
            long hotelid = img.getZshotelid();
            //本地存储路径
            String tempPath = "";
            if (hotelid < 10) {
                tempPath = "0" + hotelid;
            }
            else {
                String strid = Long.toString(hotelid);
                tempPath = strid.substring(strid.length() - 2, strid.length()); //后两位数字
            }
            String filePath = "D:\\hotelimage\\" + city.getId() + "\\" + tempPath + "\\" + hotelid;
            if (!new File(filePath).exists()) {
                new File(filePath).mkdirs();
            }
            //库中存储图片名称  http://userimg.qunar.com/imgs/201208/05/JhS1_tJQ_lOkvghIJ76.jpg
            String ImgUrl = img.getPath();
            String localImgName = ImgUrl.substring(ImgUrl.lastIndexOf("/") + 1, ImgUrl.lastIndexOf(".")) + ".jpg";
            //本地存储路径
            String localPath = filePath + "\\" + localImgName;
            //判断图片是否存在
            if (!new File(localPath).exists()) {
                //下载去哪儿图片
                download(ImgUrl, localPath);
            }
            //修改数据库记录
            img.setPath("/hotelimage/" + city.getId() + "/" + tempPath + "/" + hotelid + "/" + localImgName);
            img = Server.getInstance().getHotelService().createHotelimage(img);
            System.out.println("新增了一条图片记录----------" + img.getId());
        }
        catch (Exception e) {

        }
    }

    /**
     * 下载图片
     */
    private static void download(String urlString, String filename) throws Exception {
        System.out.println("开始下载图片----------" + filename);
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        InputStream is = con.getInputStream();
        byte[] bs = new byte[1024];
        int len;
        OutputStream os = new FileOutputStream(filename);
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        os.close();
        is.close();
    }
}