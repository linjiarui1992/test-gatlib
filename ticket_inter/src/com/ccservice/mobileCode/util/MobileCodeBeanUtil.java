/**
 * 
 */
package com.ccservice.mobileCode.util;

import com.ccservice.mobileCode.AiMaMobileCode;
import com.ccservice.mobileCode.AiwmaMobileCode;
import com.ccservice.mobileCode.BaoMaMobileCode;
import com.ccservice.mobileCode.FeiMaMobileCode;
import com.ccservice.mobileCode.HangTianMobileCode;
import com.ccservice.mobileCode.IMobileCode;
import com.ccservice.mobileCode.KaShang1MobileCode;
import com.ccservice.mobileCode.TaobaoMobileCode;
import com.ccservice.mobileCode.TongChengMobileCode;
import com.ccservice.mobileCode.TongHangMobileCode;
import com.ccservice.mobileCode.YMa0MobileCode;
import com.ccservice.mobileCode.YaoMaMobileCode;
import com.ccservice.mobileCode.YiMaMobileCode;
import com.ccservice.mobileCode.YouMaMobileCode;
import com.ccservice.mobileCode.ZhuanMaMobileCode;
import com.ccservice.mobileCode.ZhuoMaMobileCode;

/**
 * 
 * @time 2015年9月10日 上午11:02:07
 * @author chendong
 */
public class MobileCodeBeanUtil {

    /**
     * //# 1:优码,2:淘码,3:y码,4:壹码,5:爱码,6:卓码,7:要码 初始化手机验证码 平台
     * 
     * @param mobileCodeType2
     * @time 2015年8月3日 下午1:57:09
     * @author chendong
     */
    public static IMobileCode initMobileCodeType(String mobileCodeType) {
        IMobileCode mobilecode = null;
        if ("1".equals(mobileCodeType)) {
            mobilecode = MobileCodeBeanUtil.getYouMaMobileCode();
        }
        else if ("2".equals(mobileCodeType)) {// 2:淘码
            // mobilecode = MobileCodeBeanUtil.getTaoMaMobileCode();
        }
        else if ("3".equals(mobileCodeType)) {// 3:Y码
            mobilecode = YMa0MobileCode.getInstance("9185");
        }
        else if ("4".equals(mobileCodeType)) {// 4:壹码
            mobilecode = YiMaMobileCode.getInstance("5995");
        }
        else if ("5".equals(mobileCodeType)) {// 5:爱码
            mobilecode = AiMaMobileCode.getInstance("4036");
        }
        else if ("6".equals(mobileCodeType)) {// 6:卓码
            mobilecode = ZhuoMaMobileCode.getInstance("8403");
        }
        else if ("7".equals(mobileCodeType)) {// 7:要码
            mobilecode = YaoMaMobileCode.getInstance("469");
        }
        else if ("8".equals(mobileCodeType)) {// 8:同航码
            mobilecode = TongHangMobileCode.getInstance("");
        }
        else if ("9".equals(mobileCodeType)) {// 9:赚码
            mobilecode = ZhuanMaMobileCode.getInstance("3067");
        }
        else if ("10".equals(mobileCodeType)) {// 10:飞码
            mobilecode = FeiMaMobileCode.getInstance("1553");
        }
        else if ("11".equals(mobileCodeType)) {// 11:微码
            mobilecode = AiwmaMobileCode.getInstance("3305");
        }
        else if ("12".equals(mobileCodeType)) {// 12:宝码
            mobilecode = BaoMaMobileCode.getInstance("6");
        }
        else if ("13".equals(mobileCodeType)) {// 13:齐天大圣
            // mobilecode = QtdsMaMobileCode.getInstance("33XKU939DKRF");
        }
        else if ("21".equals(mobileCodeType)) {// 21:卡商1
            mobilecode = KaShang1MobileCode.getInstance("");
        }
        else if ("22".equals(mobileCodeType)) {// 22:淘宝
            mobilecode = TaobaoMobileCode.getInstance("");
        }
        else if ("31".equals(mobileCodeType)) {// 31:自己的平台
            mobilecode = HangTianMobileCode.getInstance("");
        }
        else if ("32".equals(mobileCodeType) || "33".equals(mobileCodeType)) {// 32:同程接口[注册],33:同程接口[手机号核验]
            mobilecode = TongChengMobileCode.getInstance("");
        }
        else {
            mobilecode = getYouMaMobileCode();// 默认获取优码
        }
        return mobilecode;
    }

    /**
     * 获取一个优码的实例
     * 
     * @time 2015年7月29日 下午6:48:00
     * @author chendong
     */
    public static YouMaMobileCode getYouMaMobileCode() {
        String pid = "6168";// 12306项目id
        String uid = "120865";// 12306
        String author_uid = "cd1989929";// 开发者用户名
        String author_pwd = "cd1989929";// 开发者用户名
        YouMaMobileCode youmamobilecode = new YouMaMobileCode(pid, uid, author_uid, author_pwd);
        String token = youmamobilecode.LoginIn(author_uid, author_pwd);
        token = youmamobilecode.gettoken(token, "Token");
        youmamobilecode.setToken(token);
        return youmamobilecode;
    }

    /**
     * 壹码 http://www.yzm1.com/message.php?sTime=2015-09-10+00%3A00%3A00
     * 
     * @return
     * @time 2015年9月10日 上午10:58:14
     * @author chendong
     */
    public static YiMaMobileCode getYiMaMobileCode() {
        String pid = "5995";// 12306项目id
        String uid = "";//
        String author_uid = "";// 开发者用户名
        String author_pwd = "";// 开发者用户名
        String token = "";
        YiMaMobileCode yimamobilecode = new YiMaMobileCode(pid, uid, author_uid, author_pwd);
        return yimamobilecode;
    }

    /**
     * 爱码
     * 
     * @return
     * @time 2015年9月10日 上午10:58:14
     * @author chendong
     */
    public static AiMaMobileCode getAiMaMobileCode() {
        String pid = "4036";// 12306项目项目编号
        String uid = "";// 12306
        String author_uid = "cd1989929";// 开发者用户名
        String author_pwd = "cd1989929";// 开发者用户名
        AiMaMobileCode aimamobilecode = new AiMaMobileCode(pid, uid, author_uid, author_pwd);
        return aimamobilecode;
    }
}
