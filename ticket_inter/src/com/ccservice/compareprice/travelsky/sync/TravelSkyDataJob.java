package com.ccservice.compareprice.travelsky.sync;

import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXQueryHotelRequest;

/**
 * 同步中航信酒店至本地、暂用于中航信酒店比价上去哪儿
 * @author WH
 */

public class TravelSkyDataJob implements Job {

    private static String onlineCity = PropertyUtil.getValue("onlineCity").trim();//上线城市

    public void execute(JobExecutionContext context) throws JobExecutionException {
        start();
    }

    public static void main(String[] args) {
        new TravelSkyDataJob().start();
    }

    @SuppressWarnings("unchecked")
    private void start() {
        System.out.println("开始航信酒店同步=====" + ElongHotelInterfaceUtil.getCurrentTime());
        long start = System.currentTimeMillis();

        String citysql = "where C_TRAVELSKYCODE != '' and C_TYPE = 1";
        if (!ElongHotelInterfaceUtil.StringIsNull(onlineCity)) {
            citysql += " and ID in (" + onlineCity + ")";
        }
        List<City> citys = Server.getInstance().getHotelService().findAllCity(citysql, "order by Id desc", -1, 0);
        for (City city : citys) {
            try {
                System.out.println("当前同步中航信酒店城市=====" + city.getName());
                //NEW
                HXQueryHotelRequest req = new HXQueryHotelRequest();
                req.setPageNum(1);
                req.setLocalCity(city);
                req.setCityCode(city.getTravelSkyCode());
                req.setCheckInDate(ElongHotelInterfaceUtil.getCurrentDate());
                req.setCheckOutDate(ElongHotelInterfaceUtil.getAddDate(req.getCheckInDate(), 1));
                //REQ
                Server.getInstance().getTravelskyHotelService().travelskyMultiHotels(req);
            }
            catch (Exception e) {
                System.out.println(ElongHotelInterfaceUtil.errormsg(e));
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("结束航信酒店同步，耗时：" + (end - start) / 1000 + "秒。");
    }

}
