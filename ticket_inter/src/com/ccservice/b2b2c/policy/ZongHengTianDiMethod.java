package com.ccservice.b2b2c.policy;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.ZongHengTianDiBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.webservice.zhtd.ItourCrsServiceStub;
import com.webservice.zhtd.THPolicyWebService;

/**
 * 
 * @author 贾建磊
 * 
 */
public class ZongHengTianDiMethod extends SupplyMethod {

    private static final ZongHengTianDiBook zhtdbook = new ZongHengTianDiBook();// 纵横天地账号相关信息

    /**
     * 根据航班号获取政策（对应文档中的：国内散客政策查询 单程政策查询(V2)）
     * 
     * @param departureDate
     *            乘机日期 yyyy-MM-dd
     * @param scity
     *            出发城市三字码
     * @param ecity
     *            到达城市三字码
     * @param carrierCode
     *            航空公司两字码 可以为空
     * @param cabinCode
     *            舱位码
     * @param fightnumber
     *            航班号
     * @return
     */
    public static List<Zrate> GetSinglePolicyListV2(String departureDate, String scity, String ecity,
            String carrierCode, String cabinCode, String fightnumber) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                String loginXml = "<LoginInfoBase>";
                loginXml += "<ClientID>" + zhtdbook.getZhtdClientid() + "</ClientID>";
                loginXml += "<UserName>" + zhtdbook.getZhtdUsername() + "</UserName>";
                loginXml += "<Password>" + zhtdbook.getZhtdPassword() + "</Password>";
                loginXml += "</LoginInfoBase>";
                String paramsXml = "<ParamsXml>";
                paramsXml += "<Params>";
                paramsXml += "<DepartureDate>" + departureDate + "</DepartureDate>";
                paramsXml += "<Airline>" + scity + ecity + "</Airline>";
                paramsXml += "<CarrierCode>" + carrierCode + "</CarrierCode>";
                paramsXml += "<Class>" + cabinCode + "</Class>";
                paramsXml += "<FlightNo>" + fightnumber + "</FlightNo>";
                paramsXml += "</Params>";
                paramsXml += "</ParamsXml>";

                WriteLog.write("ZHTD_Zrate", "根据航班号获取政策传入接口的数据[loginXml：" + loginXml + "---------paramsXml:"
                        + paramsXml + "]");
                ItourCrsServiceStub.GetSinglePolicyListV2 getsinglepolicylistv2 = new ItourCrsServiceStub.GetSinglePolicyListV2();
                getsinglepolicylistv2.setLoginXml(loginXml);
                getsinglepolicylistv2.setParamsXml(paramsXml);

                ItourCrsServiceStub stub = new ItourCrsServiceStub();
                ItourCrsServiceStub.GetSinglePolicyListV2Response response = stub
                        .getSinglePolicyListV2(getsinglepolicylistv2);
                String result = response.getGetSinglePolicyListV2Result();
                //				WriteLog.write("根据航班号获取政策（纵横天地）", "根据航班号获取政策接口返回的数据[" + result + "]");
                result = result.replaceAll("&lt;", "<");
                result = result.replaceAll("&gt;", ">");
                WriteLog.write("ZHTD_Zrate", "根据航班号获取政策接口返回的数据[" + result + "]");
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root.elementText("ErrCode") != null && root.elementText("ErrCode").equals("S0000")) {
                    Element resultEle = root.element("Result");
                    Element newDataSetEle = resultEle.element("NewDataSet");
                    List policyInfoList = newDataSetEle.elements("PolicyInfo");
                    Iterator it = policyInfoList.iterator();
                    while (it.hasNext()) {
                        Zrate zrate = new Zrate();
                        Element policyInfoEle = (Element) it.next();
                        zrate.setAgentid(8L);
                        zrate.setCreateuser("纵横天地");
                        zrate.setModifyuser("纵横天地");
                        zrate.setIsenable(1);
                        if (policyInfoEle.elementText("TicketType") != null) {
                            if (policyInfoEle.elementText("TicketType").equals("B2B")) {
                                zrate.setTickettype(2);
                            }
                            else {
                                zrate.setTickettype(1);
                            }
                        }
                        if (policyInfoEle.elementText("PolicyID") != null) {
                            zrate.setOutid(policyInfoEle.elementText("PolicyID"));
                        }
                        if (policyInfoEle.elementText("ClientID") != null) {
                            zrate.setAgentcode(policyInfoEle.elementText("ClientID"));
                            String suppierWorkTime = getWorkTimeByClientID(policyInfoEle.elementText("ClientID"));
                            if (suppierWorkTime != null && !suppierWorkTime.equals("")
                                    && !suppierWorkTime.equals("FAIL")) {
                                String[] str = suppierWorkTime.split("--");
                                if (str[0].indexOf('：') >= 0) {
                                    zrate.setWorktime(str[0].trim().split("：")[1]);
                                }
                                else {
                                    zrate.setWorktime(str[0].trim());
                                }
                                zrate.setAfterworktime(str[1].trim());
                                zrate.setOnetofivewastetime(str[1].trim());
                                zrate.setWeekendwastetime(str[1].trim());
                            }
                        }
                        if (policyInfoEle.elementText("Carrier") != null) {
                            zrate.setAircompanycode(policyInfoEle.elementText("Carrier"));
                        }
                        if (policyInfoEle.elementText("FlightNo") != null) {
                            zrate.setFlightnumber(policyInfoEle.elementText("FlightNo"));
                        }
                        if (policyInfoEle.elementText("Class") != null) {
                            zrate.setCabincode(policyInfoEle.elementText("Class"));
                        }
                        if (policyInfoEle.elementText("PolicyType") != null) {
                            if (policyInfoEle.elementText("PolicyType").equals("普通政策")) {
                                zrate.setGeneral(1L);
                            }
                            else {
                                zrate.setGeneral(2L);
                            }
                        }
                        if (policyInfoEle.elementText("CommissionRate") != null) {// 返点=积分比率+促销返点
                            BigDecimal b1 = new BigDecimal(policyInfoEle.elementText("CommissionRate"));
                            zrate.setRatevalue(b1.floatValue());
                        }
                        if (policyInfoEle.elementText("Remark") != null) {
                            zrate.setRemark(policyInfoEle.elementText("Remark"));
                        }
                        if (zrate.getRatevalue() > 0) {
                            zrateList.add(zrate);
                        }
                    }
                }
                else {
                    System.out.println("ErrorInfo：" + root.elementText("Error"));
                    return zrateList;
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return zrateList;
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return zrateList;
    }

    /**
     * 根据pnr获取政策
     * 
     * @param pnr
     * @return
     */
    public static List<Zrate> getZrateByPnr(String pnr) {
        List<Zrate> zrateList = new ArrayList<Zrate>();
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                String loginXml = "<LoginInfoBase>";
                loginXml += "<ClientID>" + zhtdbook.getZhtdClientid() + "</ClientID>";
                loginXml += "<UserName>" + zhtdbook.getZhtdUsername() + "</UserName>";
                loginXml += "<Password>" + zhtdbook.getZhtdPassword() + "</Password>";
                loginXml += "</LoginInfoBase>";
                String paramsXml = "<ParamsXml>";
                paramsXml += "<Params>";
                paramsXml += "<PNR>" + pnr + "</PNR>";
                paramsXml += "<BigClientID></BigClientID>";
                paramsXml += "</Params>";
                paramsXml += "</ParamsXml>";

                WriteLog.write("getzratebypnr_根据pnr获取政策（纵横天地）", "根据pnr获取政策传入接口的数据:[loginXml：" + loginXml
                        + "------------paramsXml:" + paramsXml + "]");
                ItourCrsServiceStub.PatPolicyByPnr patpolicybypnr = new ItourCrsServiceStub.PatPolicyByPnr();
                patpolicybypnr.setLoginXml(loginXml);
                patpolicybypnr.setParamsXml(paramsXml);
                ItourCrsServiceStub stub = new ItourCrsServiceStub();
                ItourCrsServiceStub.PatPolicyByPnrResponse response = stub.patPolicyByPnr(patpolicybypnr);
                String result = response.getPatPolicyByPnrResult();
                result = result.replaceAll("&lt;", "<");
                result = result.replaceAll("&gt;", ">");
                WriteLog.write("getzratebypnr_根据pnr获取政策（纵横天地）", "根据pnr获取政策接口返回的数据:[" + result + "]");
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root.elementText("ErrCode") != null && root.elementText("ErrCode").equals("S0000")) {
                    Element resultEle = root.element("Result");
                    Element newDataSetEle = resultEle.element("NewDataSet");
                    Element flightInfoEle = newDataSetEle.element("FlightInfo");
                    Element policyInfoEle = newDataSetEle.element("PolicyInfo");
                    Zrate zrate = new Zrate();// 纵横天地根据pnr获取政策，只能获取到一条政策，不能获取到多条政策
                    zrate.setAgentid(8L);
                    zrate.setCreateuser("纵横天地");
                    zrate.setModifyuser("纵横天地");
                    zrate.setIsenable(1);
                    if (flightInfoEle.elementText("Carrier") != null) {
                        zrate.setAircompanycode(flightInfoEle.elementText("Carrier"));
                    }
                    if (flightInfoEle.elementText("Flight") != null) {
                        zrate.setFlightnumber(flightInfoEle.elementText("Flight"));
                    }
                    if (flightInfoEle.elementText("Class") != null) {
                        zrate.setCabincode(flightInfoEle.elementText("Class"));
                    }
                    if (policyInfoEle.elementText("PolicyID") != null) {
                        zrate.setOutid(policyInfoEle.elementText("PolicyID"));
                    }
                    if (policyInfoEle.elementText("FDCTimeDesc") != null) {
                        if (policyInfoEle.elementText("FDCTimeDesc").contains("24小时")) {
                            zrate.setWorktime("08:00");
                            zrate.setAfterworktime("19:00");
                            zrate.setOnetofivewastetime("19:00");
                            zrate.setWeekendworktime("19:00");
                        }
                        else {
                            String[] str = policyInfoEle.elementText("FDCTimeDesc").split("-");
                            zrate.setWorktime(str[0]);
                            zrate.setAfterworktime(str[1]);
                            zrate.setOnetofivewastetime(str[1]);
                            zrate.setWeekendworktime(str[1]);
                        }
                    }
                    if (policyInfoEle.elementText("PromotionCommission") != null) {
                        zrate.setRatevalue(Float.valueOf(policyInfoEle.elementText("PromotionCommission")));
                    }
                    if (policyInfoEle.elementText("PolicyTypeID") != null) {
                        if (policyInfoEle.elementText("PolicyTypeID").equals("1")) {
                            zrate.setGeneral(1L);
                        }
                        else if (policyInfoEle.elementText("PolicyTypeID").equals("2")) {
                            zrate.setGeneral(2L);
                        }
                    }
                    if (policyInfoEle.elementText("Remark") != null) {
                        zrate.setRemark(policyInfoEle.elementText("Remark"));
                    }
                    if (zrate.getRatevalue() > 0) {
                        zrateList.add(zrate);
                    }
                }
                else {
                    return zrateList;
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return zrateList;
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return zrateList;
    }

    /**
     * PNR导入生成订单
     * 
     * @param order
     * @param AccountPay
     *            实际支付金额
     * @param policyID
     *            政策ID
     * @return 如果未创建成功返回-1 成功：返回 "S|" + orderNO + "|" + payurl + "|" + officeid +
     *         "|"+ payfee;
     */
    public String ReserveByPnr(Orderinfo order, String accountPay, String policyID) {
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                String loginXml = "<LoginInfoBase>";
                loginXml += "<ClientID>" + zhtdbook.getZhtdClientid() + "</ClientID>";
                loginXml += "<UserName>" + zhtdbook.getZhtdUsername() + "</UserName>";
                loginXml += "<Password>" + zhtdbook.getZhtdPassword() + "</Password>";
                loginXml += "</LoginInfoBase>";
                String paramsXml = "<ParamsXml>";
                paramsXml += "<Params>";
                paramsXml += "<PNR>" + order.getPnr() + "</PNR>";
                paramsXml += "<AccountPay>" + accountPay + "</AccountPay>";
                paramsXml += "<OutOrderNo></OutOrderNo>";
                paramsXml += "<ClientID></ClientID>";
                paramsXml += "<PolicyID>" + policyID + "</PolicyID>";
                paramsXml += "<Linkman>" + zhtdbook.getZhtdLinkName() + "</Linkman>";
                paramsXml += "<Phone>" + zhtdbook.getZhtdLinkMobile() + "</Phone>";
                paramsXml += "</Params>";
                paramsXml += "</ParamsXml>";

                WriteLog.write("CreateOrderZHTD", "PNR导入生成订单传入接口的数据:[loginXml：" + loginXml + "------------paramsXml:"
                        + paramsXml + "]");
                ItourCrsServiceStub.ReserveByPnr reservebypnr = new ItourCrsServiceStub.ReserveByPnr();
                reservebypnr.setLoginXml(loginXml);
                reservebypnr.setParamsXml(paramsXml);
                ItourCrsServiceStub stub = new ItourCrsServiceStub();
                ItourCrsServiceStub.ReserveByPnrResponse response = stub.reserveByPnr(reservebypnr);
                String result = response.getReserveByPnrResult();
                result = result.replaceAll("&lt;", "<");
                result = result.replaceAll("&gt;", ">");
                WriteLog.write("CreateOrderZHTD", "PNR导入生成订单接口返回的数据:[" + result + "]");
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root.elementText("ErrCode") != null && root.elementText("ErrCode").equals("S0000")) {
                    Element resultEle = root.element("Result");
                    Element newDataSetEle = resultEle.element("NewDataSet");
                    Element orderInfoEle = newDataSetEle.element("OrderInfo");
                    String orderID = orderInfoEle.elementText("OrderID");// 订单id
                    String officeid = orderInfoEle.elementText("supplierOfficeNo");// office号
                    String payfee = orderInfoEle.elementText("Gathering");// 实际付款金额
                    String payurl = getOrderPayUrl(orderID, "1");// 支付链接
                    String resultInfo = "-1";
                    if (!payurl.equals("FAIL")) {
                        resultInfo = "S|" + orderID + "|" + payurl + "|" + officeid + "|" + payfee;
                    }
                    return resultInfo;
                }
                else {
                    return "-1";
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return "-1";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    /**
     * 获取订单支付链接
     * 
     * @param orderID
     *            外部订单号
     * @param payType
     *            支付类型：0-分销平台支付地址,1-支付宝 平台地址
     * @return
     */
    public String getOrderPayUrl(String orderID, String payType) {
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                String loginXml = "<LoginInfoBase>";
                loginXml += "<ClientID>" + zhtdbook.getZhtdClientid() + "</ClientID>";
                loginXml += "<UserName>" + zhtdbook.getZhtdUsername() + "</UserName>";
                loginXml += "<Password>" + zhtdbook.getZhtdPassword() + "</Password>";
                loginXml += "</LoginInfoBase>";
                String paramsXml = "<ParamsXml>";
                paramsXml += "<Params>";
                paramsXml += "<OrderID>" + orderID + "</OrderID>";
                paramsXml += "<PayType>" + payType + "</PayType>";
                paramsXml += "<OperatorName>" + zhtdbook.getZhtdLinkName() + "</OperatorName>";
                paramsXml += "</Params>";
                paramsXml += "</ParamsXml>";

                WriteLog.write("获取订单支付链接（纵横天地）", "获取订单支付链接传入接口的数据:[loginXml：" + loginXml + "------------paramsXml:"
                        + paramsXml + "]");
                ItourCrsServiceStub.GetOrderPayUrl getorderpayurl = new ItourCrsServiceStub.GetOrderPayUrl();
                getorderpayurl.setLoginXml(loginXml);
                getorderpayurl.setParamsXml(paramsXml);
                ItourCrsServiceStub stub = new ItourCrsServiceStub();
                ItourCrsServiceStub.GetOrderPayUrlResponse response = stub.getOrderPayUrl(getorderpayurl);
                String result = response.getGetOrderPayUrlResult();
                result = result.replaceAll("&lt;", "<");
                result = result.replaceAll("&gt;", ">");
                WriteLog.write("获取订单支付链接（纵横天地）", "获取订单支付链接接口返回的数据:[" + result + "]");
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root.elementText("ErrCode") != null && root.elementText("ErrCode").equals("S0000")) {
                    if (root.elementText("Result") != null) {
                        String payurl = root.elementText("Result");
                        return payurl;
                    }
                    else {
                        return "FAIL";
                    }
                }
                else {
                    return "FAIL";
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return "FAIL";
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return "FAIL";
    }

    /**
     * 支付宝代扣
     * 
     * @param waibuorderid
     *            外部订单id
     * @param nativeorderid
     *            本地订单id
     * @return
     */
    public String autoPayOrder(String waibuorderid, String nativeorderid) {
        String resultStr = "-1";
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                String loginXml = "<LoginInfoBase>";
                loginXml += "<ClientID>" + zhtdbook.getZhtdClientid() + "</ClientID>";
                loginXml += "<UserName>" + zhtdbook.getZhtdUsername() + "</UserName>";
                loginXml += "<Password>" + zhtdbook.getZhtdPassword() + "</Password>";
                loginXml += "</LoginInfoBase>";
                String paramsXml = "<ParamsXml>";
                paramsXml += "<Params>";
                paramsXml += "<orderidlist>" + waibuorderid + "</orderidlist>";
                paramsXml += "<opid>" + zhtdbook.getOperatorID() + "</opid>";
                paramsXml += "<pidlist>1</pidlist>";
                paramsXml += "<code></code>";
                paramsXml += "<account_out>" + zhtdbook.getZhtdAccountOut() + "</account_out>";
                paramsXml += "<payType>1</payType>";
                paramsXml += "</Params>";
                paramsXml += "</ParamsXml>";

                WriteLog.write("自动代扣（纵横天地）", "纵横天地接口日志支付宝代扣传入接口的数据:[loginXml：" + loginXml + "------------paramsXml:"
                        + paramsXml + "]");
                ItourCrsServiceStub.AliPay alipay = new ItourCrsServiceStub.AliPay();
                alipay.setLoginXml(loginXml);
                alipay.setParamsXml(paramsXml);
                ItourCrsServiceStub stub = new ItourCrsServiceStub();
                ItourCrsServiceStub.AliPayResponse response = stub.aliPay(alipay);
                String result = response.getAliPayResult();
                result = result.replaceAll("&lt;", "<");
                result = result.replaceAll("&gt;", ">");
                WriteLog.write("自动代扣（纵横天地）", "纵横天地接口日志支付宝代扣接口返回的数据:[" + result + "]");
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();

                if (root.elementText("Error").indexOf("成功") >= 0) {
                    resultStr = "S";
                    //					String sql = "UPDATE T_ORDERINFO SET "
                    //							+ Orderinfo.COL_extorderstatus + "='2' WHERE ID="
                    //							+ nativeorderid;
                    //					Server.getInstance().getSystemService().findMapResultBySql(
                    //							sql, null);
                    return resultStr;
                }
                else {
                    return resultStr;
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return resultStr;
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return resultStr;
    }

    /**
     * 根据政策供应商ID查询供应商工作时间
     * 
     * @param clientID
     *            政策供应商ID
     * @return 返回的成功的数据格式，例如：工作时间：08:00--19:00
     */
    public static String getWorkTimeByClientID(String clientID) {
        try {
            if (zhtdbook.getZhtdStaus().equals("1")) {
                THPolicyWebService thpolicywebservice = new THPolicyWebService();
                String result = thpolicywebservice.getTHPolicyWebServiceSoap().getSuppierWorkTime(clientID,
                        zhtdbook.getZhtdGetPolicyUsername(), zhtdbook.getZhtdGetPolicyPassword(), "XML");
                WriteLog.write("ZHTD", result);
                Document document = DocumentHelper.parseText(result);
                Element root = document.getRootElement();
                if (root.elementText("ErrCode") != null && root.elementText("ErrCode").equals("S0000")) {
                    if (root.elementText("Result") != null && !root.elementText("Result").equals("")) {
                        if (root.elementText("Result").contains("24小时")) {
                            return "工作时间：08:00--19:00";
                        }
                        else {
                            return root.elementText("Result");
                        }
                    }
                }
            }
            else {
                System.out.println("纵横天地接口：被禁用");
                return "FAIL";
            }
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        return "FAIL";
    }

    public static ZongHengTianDiBook getZhtdbook() {
        return zhtdbook;
    }
}
