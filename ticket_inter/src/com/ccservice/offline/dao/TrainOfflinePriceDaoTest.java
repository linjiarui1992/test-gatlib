/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccservice.offline.domain.TrainOfflinePrice;

/**
 * @className: com.ccservice.tuniu.train.dao.T_CUSTOMERAGENTDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-杨威   E-mail:653395455@qq.com
 * @createTime: 2017年9月04日 下午5:20:11 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflinePriceDaoTest {
    private TrainOfflinePriceDao trainOfflinePriceDao = new TrainOfflinePriceDao();

    public void testAddTrainOfflineExpRec() throws Exception {
        //订单信息入库
    	TrainOfflinePrice trainOfflinePrice = new TrainOfflinePrice();
    	trainOfflinePrice.setAgentId(50);
    	trainOfflinePrice.setExpress1(11);
    	trainOfflinePrice.setExpress2(11);
    	trainOfflinePrice.setShouxuPrice(11);
        System.out.println(trainOfflinePriceDao.addTrainOfflinePrice(trainOfflinePrice));
    }
    public void testDelTrainOfflinePriceById() throws Exception {
        System.out.println(trainOfflinePriceDao.delTrainOfflinePriceById(24));
    }
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOfflinePriceDaoTest trainOfflinePriceDaoTest = new TrainOfflinePriceDaoTest();
        //trainOfflinePriceDaoTest.testAddTrainOfflineExpRec();
        //trainOfflinePriceDaoTest.testDelTrainOfflinePriceById();
        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
