package com.ccservice.newjltour.updatejob;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.newjltour.hoteldb.JlHotelDB;

/**
 * 整体更新深捷旅价格
 * @author WH
 */

public class NewJlPriceJob implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        JlHotelDB.updateJlprice();
    }

}