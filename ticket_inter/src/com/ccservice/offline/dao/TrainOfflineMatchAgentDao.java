/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.util.List;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.offline.domain.TrainOfflineMatchAgent;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:08:02 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineMatchAgentDao {
    //获取随机分配的代售点的信息
    public List<TrainOfflineMatchAgent> findTrainOfflineMatchAgentListDetailByRandom() throws Exception {
        String sql = "OFFLINE_findTrainOfflineMatchAgentListDetailByRandom";
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return (List<TrainOfflineMatchAgent>) new BeanListHanlder(TrainOfflineMatchAgent.class).handle(dataTable);
    }
}
