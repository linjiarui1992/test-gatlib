package com.ccservice.b2b2c.yilong;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.axis2.AxisFault;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alipay.util.Md5Encrypt;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallback;
import com.ccservice.b2b2c.yilong.JaxRpcCallbackServiceStub.CtripCallbackResponse;
import com.yeexing.webservice.service.MD5Util;

public class HthyLockOrder {
	
	public String autoLockOrder(String xml){
		String result="";
		try {
			Document document = DocumentHelper.parseText(xml);
			Element orderResponse = document.getRootElement();
			String orderNumber=orderResponse.elementText("OrderNumber");//接口服务名
			String status=orderResponse.elementText("Status");//接口服务名
			if("SUCCESS".equals(status)){
				result =lockOrder(orderNumber);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return result;
	}
	public static void main(String[] args) {
		String result=lockOrder("elong_20160119253298052");
		System.out.println("result="+result);
	}
	public static String lockOrder(String orderid){
		String orderstatus="";
		String result="";
		try {
			JaxRpcCallbackServiceStub Stub=new JaxRpcCallbackServiceStub();
			CtripCallback ctripCallback = new CtripCallback();
			org.apache.axis2.databinding.types.soapencoding.String param = new org.apache.axis2.databinding.types.soapencoding.String();
			String xmlparam=getXml(orderid);
			param.setString(xmlparam);
			ctripCallback.setXml(param);
			CtripCallbackResponse ctripCallbackResponse = Stub.ctripCallback(ctripCallback);
			org.apache.axis2.databinding.types.soapencoding.String ss=ctripCallbackResponse.getCtripCallbackReturn();
			WriteLog.write("Elong请求艺龙锁单操作返回信息", "orderid:"+orderid+",response="+ss);
			result=ss+"";
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		//锁单返回信息解析
		try {
			Document document = DocumentHelper.parseText(result);
			Element orderResponse = document.getRootElement();
			String serviceName=orderResponse.elementText("ServiceName");//接口服务名
			String operationDateTime=orderResponse.elementText("OperationDateTime");//操作日期
			String orderNumber=orderResponse.elementText("OrderNumber");//订单号
			String status=orderResponse.elementText("Status");//状态
			orderstatus=status;
			Element errorResponse=orderResponse.element("ErrorResponse");
			String errorMessage=errorResponse.elementText("ErrorMessage");//信息
			if("FAIL".equals(status)){
				String errorCode=errorResponse.elementText("ErrorCode");//ErrorCode
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return orderstatus;
	}
	
		public static String getXml(String orderid) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//下单请求接口调用Response
			Document document=DocumentHelper.createDocument();
			Element orderProcessRequest=document.addElement("OrderProcessRequest");
			orderProcessRequest.addAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance");
			Element authentication=orderProcessRequest.addElement("Authentication");
			Element timeStamp=authentication.addElement("TimeStamp");
			String times=sdf.format(new Date());
			timeStamp.setText(times);
			Element serviceName=authentication.addElement("ServiceName");
			serviceName.setText("web.order.lockOrder");
			Element messageIdentity=authentication.addElement("MessageIdentity");
			String message=getMessage(times,"web.order.lockOrder","hangtian111").toUpperCase();
			messageIdentity.setText(message);
			Element partnerName=authentication.addElement("PartnerName");
			partnerName.setText("hangtian111");
			Element trainOrderService=orderProcessRequest.addElement("TrainOrderService");
			Element orderInfo=trainOrderService.addElement("OrderInfo");
			Element orderNumber=orderInfo.addElement("OrderNumber");
			orderNumber.setText(orderid);
			return document.asXML();
		}
		public static String getMessage(String times,String serviceName,String partName){
			String result=MD5Util.MD5(times+serviceName+partName);
			System.out.println(times+serviceName+partName+"<-->"+result);
			return result;
		}
}
