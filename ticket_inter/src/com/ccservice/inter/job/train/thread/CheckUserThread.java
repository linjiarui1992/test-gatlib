package com.ccservice.inter.job.train.thread;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;
import com.ccservice.inter.server.Server;

public class CheckUserThread extends Thread {

    //    private JobCheckUser jobCheckUser;
    //
    //    public CheckUserThread(JobCheckUser jobCheckUser) {
    //        this.jobCheckUser = jobCheckUser;
    //    }

    @Override
    public void run() {
        int threadcount = 1;
        List customerusers = Server.getInstance().getSystemService()
                .findMapResultByProcedure("sp_CustomerUser_Job12306CheckIsenable");
        String sql11 = null;
        if (customerusers.size() > 0) {
            Customeruser customeruser = new Customeruser();
            Map map = (Map) customerusers.get(0);
            String loginname = map.get("C_LOGINNAME").toString();
            String password = map.get("C_LOGPASSWORD").toString();
            Long id = Long.parseLong(map.get("ID").toString());
            //            String repUrl = getrepurl(2).getUrl();

            String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
            String cardnunber = null;
            try {
                cardnunber = rep12Method(loginname, password, repUrl);//登录获取cookie;
                if (cardnunber == null || cardnunber.equals("")) {
                    return;
                }

            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
            WriteLog.write("Job12306CheckIsenable", "id:" + id + ":loginname:" + loginname + ":cardnunber:"
                    + cardnunber + ":repUrl:" + repUrl);
            if (!"".equals(cardnunber) && !cardnunber.contains("失败")) {
                customeruser.setId(id);
                customeruser.setLoginname(loginname);
                customeruser.setCardnunber(cardnunber);
                //{"shoujihaoma":"13169820344","shoujihaoma_heyanstatus":"已通过","heyanzhuangtai":"已通过"}
                JSONObject json = JobTrainUtil.initQueryUserInfo(cardnunber);
                if (json.containsKey("loginStatus")) {
                    System.out.println("登录未成功");
                    return;
                }
                if (json != null && json.size() > 0) {
                    String shoujihaoma = json.getString("shoujihaoma");
                    String shoujihaoma_heyanstatus = json.getString("shoujihaoma_heyanstatus");
                    String heyanzhuangtai = json.getString("heyanzhuangtai");
                    if ("已通过".contains(heyanzhuangtai)) {
                        if ("已通过".contains(shoujihaoma_heyanstatus)) {
                            //1 手机已通过|身份已通过
                            System.out.println("1:手机已通过|身份已通过:" + id);
                            sql11 = "update T_CUSTOMERUSER set C_ISENABLE='1' where id=" + id;
                        }
                        else if ("未通过".contains(shoujihaoma_heyanstatus)) {
                            //38 手机未通过|身份已通过
                            System.out.println("38:手机未通过|身份已通过:" + id);
                            sql11 = "update T_CUSTOMERUSER set C_ISENABLE='38' where id=" + id;
                        }
                    }
                    else if ("未通过".contains(heyanzhuangtai)) {
                        if ("已通过".contains(shoujihaoma_heyanstatus)) {
                            //37 身份未通过【手机已通过】  
                            System.out.println("37:身份未通过|手机已通过:" + id);
                            sql11 = "update T_CUSTOMERUSER set C_ISENABLE=37 where id=" + id;
                        }
                        else {
                            //40 身份未通过【手机未通过】 
                            System.out.println("40:身份未通过|手机未通过:" + id);
                            sql11 = "update T_CUSTOMERUSER set C_ISENABLE=40 where id=" + id;
                        }
                    }
                }
            }
            else if (cardnunber.contains("登录名不存在")) {
                sql11 = "update T_CUSTOMERUSER set C_ISENABLE='16' where id=" + id;
                System.out.println("16:登录名不存在" + id);
            }
            else if (cardnunber.contains("您的用户信息被他人冒用")) {
                sql11 = "update T_CUSTOMERUSER set C_ISENABLE='43' where id=" + id;
                System.out.println("43:您的用户信息被他人冒用");
            }
            //else if ("失败".equals(cardnunber)) {
            //   sql11 = "update T_CUSTOMERUSER set C_ISENABLE='0' where id=" + id;
            //   System.out.println("失败" + id);
            // }
            if (sql11 != null) {
                int count = Server.getInstance().getSystemService().excuteEaccountBySql(sql11);
                System.out.println("SQL执行结果：" + count);
                WriteLog.write("Job12306CheckIsenable", "id:" + id + ":count:" + count + ":sql11:" + sql11);
            }
        }
        else {
            System.out.println("Job12306CheckIsenable_没人了");
        }

    }

    public static RepServerBean getrepurl() {
        //        String serviceurl = PropertyUtil.getValue("reg_serviceurl_getrepurl", "train.properties");
        //        String serviceurl = "http://121.40.174.4:39101/cn_service/service/";
        String serviceurl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
        RepServerBean rep = new RepServerBean();
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        List list = Server.getInstance().getSystemService().findMapResultByProcedure("sp_RepServer_GetOneRep");
        //        List list = isystemservice.findMapResultByProcedure("sp_RepServer_GetOneRep");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            //REP名称
            String name = objectisnull(map.get("C_NAME"));
            //REP地址
            String url = objectisnull(map.get("C_URL"));
            //最大订单数
            int maxNumber = Integer.parseInt(objectisnull(map.get("C_MAXNUMBER")));
            //在下订单数
            //int useNumber = Integer.parseInt(map.get("C_USENUMBER").toString());
            //最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            //SET
            rep.setId(id);
            rep.setName(name);
            rep.setUrl(url);
            rep.setMaxNumber(maxNumber);
            rep.setUseNumber(0);
            rep.setLastTime(lastTime);
        }
        return rep;
    }

    /**
     * 登录帐号获取cookie
     */
    public String rep12Method(String logname, String logpassword, String repUrl) {
        String damarule = "5,5,5,5,5";
        String paramContent = "";
        paramContent = "logname=" + logname + "&logpassword=" + logpassword + "&damarule=" + damarule
                + "&datatypeflag=12";
        String resultString = "";
        System.out.println(repUrl + paramContent);
        //        int count = 1;
        //        do {
        try {
            resultString = com.ccservice.b2b2c.policy.SendPostandGet.submitPost(repUrl, paramContent, "utf-8")
                    .toString();
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("JobTrainUtil_getCookie_err", e);
        }

        //        System.out.println("第" + count++ + "次循环取Cookie!");
        //        if (resultString.equals("")) {
        //            try {
        //                Thread.sleep(3000L);
        //            }
        //            catch (InterruptedException e) {
        //                e.printStackTrace();
        //            }
        //        }
        //        }
        //        while (resultString == null || resultString.equals(""));
        //        count = 1;
        System.out.println("Cookie：===========>" + resultString);
        return resultString;
    }

    /**
     * 
     * @param object
     * @return
     * @time 2016年1月5日 下午9:09:12
     * @author ZhiHong
     */
    public static String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("CheckUserThread", "CheckUserThreadGroup", CheckUserThread.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("Job12306Registration", "Job12306RegistrationGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

    public static void main(String[] args) throws InterruptedException {
        //        while (true) {
        //            new CheckUserThread(new JobCheckUser()).start();
        //            Thread.sleep(5000L);
        //        }
    }
}
