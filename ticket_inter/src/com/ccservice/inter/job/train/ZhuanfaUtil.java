package com.ccservice.inter.job.train;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import com.ccservice.b2b2c.atom.component.WriteLog;

public class ZhuanfaUtil {
    /**
     * https post方式请求服务器
     * @param url
     * @param content
     * @param charset
     * @param cookiestring
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:58:26
     * @author yinshubin
     */
    public static String zhuanfa(String url, String content, String charset, String cookiestring, String method)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        String urlstr = URLEncoder.encode(url, "utf-8");
        String contentstr = URLEncoder.encode(content, "utf-8");
        String cookiestr = URLEncoder.encode(cookiestring, "utf-8");
        String geturl = "http://12306.hangtian123.com/?url=" + urlstr + "&param=" + contentstr + "&method=" + method
                + "&cookie=" + cookiestr;
        String str = httpget(geturl, "utf-8");
        WriteLog.write("URL转发", geturl);
        if (str.contains("@@@@@")) {
            return str.split("@@@@@")[0];
        }
        else {
            return "";
        }
    }

    public static byte[] httpget(String url) {
        try {
            URL Url = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) Url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream in = conn.getInputStream();
            byte[] buf = new byte[2046];
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            int len = 0;
            int size = 0;
            while ((len = in.read(buf)) > 0) {
                bout.write(buf, 0, len);
                size += len;
            }
            in.close();
            conn.disconnect();
            return bout.toByteArray();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String httpget(String url, String encode) {

        try {
            return new String(httpget(url), encode);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
