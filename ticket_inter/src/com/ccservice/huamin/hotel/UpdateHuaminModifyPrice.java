package com.ccservice.huamin.hotel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccervice.huamin.update.HMRequestUitl;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.huamin.Util;
import com.ccservice.huamin.WriteLog;

public class UpdateHuaminModifyPrice implements Job {
	public static void main(String[] args) {
		 updatePrice();
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		updatePrice();
	}

	public static void updatePrice() {
		File files = new File("D:\\酒店价格");
		if (files.exists()) {
			File[] file = files.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					String filename = pathname.getName();
					if (filename.indexOf("HMC_ALLOTUPDATE") == 0
							|| filename.indexOf("HMC_HC") == 0
							|| filename.indexOf("Copy") == 0) {
						return true;
					}
					return false;
				}

			});
			for (File f : file) {
				SAXBuilder sb = new SAXBuilder();
				try {
					Document doc = sb.build(f);
					Element root = doc.getRootElement();
					if (f.getName().indexOf("HMC_HC") == 0
							|| f.getName().indexOf("CopyHMC_HC") == 0) {
						System.out.println("更新价格……");
						List<Element> contracts = root.getChildren("CONTRACTS");
						for (Element element : contracts) {
							String contract = element.getChildText("CONTRACT");
							String hotelid = element.getChildText("HOTEL");
							List<Hotel> hotels = Server.getInstance()
									.getHotelService().findAllHotel(
											"where C_HOTELCODE='" + hotelid
													+ "' and c_paytype=2", "",
											-1, 0);
							if (hotels.size() == 1) {
								getNewRate(hotels.get(0).getId(), new Date(),
										CatchNext31Day(new Date()));
							} else if (hotels.size() > 1) {
								WriteLog.write("酒店价格更细问题", "找到多个酒店Id："
										+ hotelid);
							} else {
								WriteLog.write("酒店价格更细问题", "未找到对应的酒店Id："
										+ hotelid);
							}
						}
						System.out.println("删除一个文件:" + f.getName());
						f.delete();
					} else if (f.getName().indexOf("HMC_ALLOTUPDATE") == 0
							|| f.getName().indexOf("CopyHMC_ALLOTUPDATE") == 0) {
						System.out.println("更新房态……");
						Element CONTRACTS = root.getChild("CONTRACTS");
						List<Element> contracts = CONTRACTS
								.getChildren("CONTRACT_LIST");
						for (Element element : contracts) {
							String contract = element.getChildText("CONTRACT");
							String hotelid = element.getChildText("HT_CODE");
							List<Hotel> hotels = Server.getInstance()
									.getHotelService().findAllHotel(
											"where C_HOTELCODE='" + hotelid
													+ "' and c_paytype=2", "",
											-1, 0);
							if (hotels.size() == 1) {
								getNewRate(hotels.get(0).getId(), new Date(),
										CatchNext31Day(new Date()));
							} else if (hotels.size() > 1) {
								WriteLog.write("酒店价格更细问题", "找到多个酒店Id："
										+ hotelid);
							} else {
								WriteLog.write("酒店价格更细问题", "未找到对应的酒店Id："
										+ hotelid);
							}
						}
						System.out.println("删除一个文件:" + f.getName());
						f.delete();
					}

				} catch (Exception e) {
					File temp = new File("d:\\酒店价格\\Copy" + f.getName());
					System.out.println(f.getName());
					try {
						if (!temp.exists()) {
							temp.createNewFile();
						}
						copyFile(f, temp);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	public static void copyFile(File sourceFile, File targetFile)
			throws IOException {
		BufferedInputStream inBuff = null;
		BufferedOutputStream outBuff = null;
		try {
			// 新建文件输入流并对它进行缓冲
			inBuff = new BufferedInputStream(new FileInputStream(sourceFile));

			// 新建文件输出流并对它进行缓冲
			outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));

			// 缓冲数组
			byte[] b = new byte[1024 * 5];
			int len;
			while ((len = inBuff.read(b)) != -1) {
				outBuff.write(b, 0, len);
			}
			// 刷新此缓冲的输出流
			outBuff.flush();
		} finally {
			// 关闭流
			if (inBuff != null)
				inBuff.close();
			if (outBuff != null)
				outBuff.close();
		}
	}

	// 录入酒店价格
	public static void getNewRate(long hotelid, Date checkin, Date checkout) {
		SimpleDateFormat sdf = new SimpleDateFormat("d-MMM-yy", Locale.US);
		String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qrate&p_lang=SIM&p_checkin="
				+ sdf.format(checkin)
				+ "&p_checkout="
				+ sdf.format(checkout)
				+ "&p_hotel="
				+ Server.getInstance().getHotelService().findHotel(hotelid)
						.getHotelcode().trim();
		System.out.println("新访问获取价格路径:" + totalurl);
		String xmlstr = Util.getStr(totalurl);
		if (getXmlCount(xmlstr) > 2) {
			parseNewxml(hotelid, xmlstr);
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(checkin);
			cal.add(Calendar.DAY_OF_MONTH, 3);
			String totalurlm = HMRequestUitl.getHMRequestUrlHeader() + "&api=qrate&p_lang=SIM&p_checkin="
					+ sdf.format(cal.getTime())
					+ "&p_checkout="
					+ sdf.format(checkout)
					+ "&p_hotel="
					+ Server.getInstance().getHotelService().findHotel(hotelid)
							.getHotelcode().trim();
			System.out.println("新访问获取价格路径:" + totalurlm);
			String xmlstrm = Util.getStr(totalurlm);
			parseNewxml(hotelid, xmlstrm);
		}
	}

	public static int getXmlCount(String xmlstr) {
		SAXBuilder sb = new SAXBuilder();
		Document doc;
		int count = 0;
		try {
			doc = sb.build(new StringReader(xmlstr));
			Element root = doc.getRootElement();
			Element result = root.getChild("XML_RESULT");
			count = result.getChildren().size();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	// 更新后获取价格代码
	public static void parseNewxml(long hotelid, String xmlStr) {
		Server.getInstance().getSystemService().findMapResultBySql(
				"delete from t_hmhotelprice where c_hotelid=" + hotelid, null);
		System.out.println("删除成功……");
		SAXBuilder sb = new SAXBuilder();
		try {
			Document doc = sb.build(new StringReader(xmlStr));
			Element root = doc.getRootElement();
			Element result = root.getChild("XML_RESULT");
			Element contracts = result.getChild("CONTRACTS");
			if (contracts != null) {
				String hmcontract = contracts.getChildText("CONTRACT");
				System.out.println("酒店合同代码:" + hmcontract);
				String hmcontractver = contracts.getChildText("VER");
				// String hmhotelid = contracts.getChildText("HOTEL");
				// String hmhotelname = contracts.getChildText("HOTELNAME");
				String cur = contracts.getChildText("CUR");
				Element product = contracts.getChild("PRODUCT");
				String prod = product.getChildText("PROD");
				// String nation = product.getChildText("NATION");
				// System.out.println("国家代码:" + nation);
				String nationname = product.getChildText("NATIONNAME");
				String min = product.getChildText("MIN");
				String max = product.getChildText("MAX");
				String advance = product.getChildText("ADVANCE");
				String ticket = product.getChildText("TICKET");
				List<Element> rooms = product.getChildren("ROOM");
				for (Element room : rooms) {
					String hmroomtype = room.getChildText("CAT");
					System.out.println("房型:" + hmroomtype);
					// 床型
					String type = room.getChildText("TYPE");
					System.out.println("TYPE:" + type);
					// 服务包
					String serv = room.getChildText("SERV");
					System.out.println("服务包:" + serv);
					// 以包括早餐数量
					String bf = room.getChildText("BF");
					List<Element> stays = room.getChildren("STAY");
					for (Element stay : stays) {
						Hmhotelprice hotelprice = new Hmhotelprice();
						// 床型
						List<Bedtype> bedtypefromtable = Server.getInstance()
								.getHotelService().findAllBedtype(
										" WHERE " + Bedtype.COL_type + "='"
												+ type.trim() + "'", "", -1, 0);
						if (bedtypefromtable.size() > 0) {
							hotelprice.setType(bedtypefromtable.get(0).getId()
									+ "");
						}
						hotelprice.setCountryname(nationname);
						// 房型代码
						List<Roomtype> roomtypefromtable = Server.getInstance()
								.getHotelService().findAllRoomtype(
										" WHERE " + Roomtype.COL_roomcode
												+ "='" + hmroomtype + "' AND "
												+ Roomtype.COL_hotelid + "='"
												+ hotelid + "' AND "
												+ Roomtype.COL_bed + "='"
												+ hotelprice.getType().trim()
												+ "'", "", -1, 0);
						if (roomtypefromtable.size() > 0) {
							hotelprice.setRoomtypeid(roomtypefromtable.get(0)
									.getId());
						}
						// hotelprice.setRoomtypeid(roomtyid);
						// 服务包
						hotelprice.setServ(serv);
						// 已包含早餐数量
						hotelprice.setBf(Long.parseLong(bf));
						hotelprice.setContractid(hmcontract);
						// 酒店合同版本号
						hotelprice.setContractver(hmcontractver);
						// 酒店代码
						hotelprice.setHotelid(hotelid);
						// 货币
						hotelprice.setCur(cur);
						// 提醒代码
						hotelprice.setProd(prod);
						// 国籍代码
						// hotelprice.setCountryid(168l);
						// // 国家名称
						// if (nation.equals("ALL")) {
						// hotelprice.setCountryname("中国");
						// }
						// if (nation.equals("PRC")) {
						// hotelprice.setCountryname("中国大陆市场");
						// }
						// if (nation.equals("NPRC")) {
						// hotelprice.setCountryname("非中国大陆市场");
						// }
						// 最少停留晚数
						// System.out.println("最少停留晚数:" + min);
						hotelprice.setMinday(Long.parseLong(min.trim()));
						// 最多停留晚数
						// System.out.println("最多停留晚数:" + max);
						hotelprice.setMaxday(Long.parseLong(max.trim()));
						// 提前预定天数
						hotelprice.setAdvancedday(Long.parseLong(advance));
						// 有无机票 1有机票 0 无机票
						hotelprice.setTicket(ticket);
						// if (Long.parseLong(ticket.trim()) == 0) {
						// System.out.println("不需要机票号码");
						// }
						// if (Long.parseLong(ticket.trim()) == 1) {
						// System.out.println("需要机票号码");
						// }

						SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd");
						// System.out.println(sd.format(LastDay()));
						String deadline = room.getChildText("DEADLINE");
						// 最后期限
						hotelprice.setDeadline(sdf.format(sd.parse(deadline)));
						String statedate = stay.getChildText("STAYDATE");
						// 日期
						hotelprice
								.setStatedate(sdf.format(sd.parse(statedate)));
						System.out.println("日期:"
								+ sdf.format(sd.parse(statedate)));
						List<Hmhotelprice> hmhotelpricefromtable = Server
								.getInstance().getHotelService()
								.findAllHmhotelprice(
										" WHERE " + Hmhotelprice.COL_hotelid
												+ "='"
												+ hotelprice.getHotelid()
												+ "' AND "
												+ Hmhotelprice.COL_roomtypeid
												+ "='"
												+ hotelprice.getRoomtypeid()
												+ "' AND "
												+ Hmhotelprice.COL_statedate
												+ "='"
												+ hotelprice.getStatedate()
												+ "' AND "
												+ Hmhotelprice.COL_serv + "='"
												+ hotelprice.getServ()
												+ "' AND "
												+ Hmhotelprice.COL_type + "='"
												+ hotelprice.getType() + "'",
										"", -1, 0);
						String price = stay.getChildText("PRICE");
						try {
							double chaprice = 0;
							if (hmhotelpricefromtable.size() > 0) {
								chaprice = hmhotelpricefromtable.get(0)
										.getPriceoffer()
										- hmhotelpricefromtable.get(0)
												.getPrice();
							}
							if (chaprice <= 0) {
								chaprice = (int) (Double.parseDouble(price) * 0.1);
							}
							System.out.println("差价：" + chaprice);
							// 价格
							hotelprice.setPrice(Double.parseDouble(price));
							// 可维护的酒店价格
							hotelprice.setPriceoffer(Double.parseDouble(price)
									+ chaprice);
							// 去哪的价格
							hotelprice.setQunarprice(Double.parseDouble(price));
						} catch (Exception e) {
							WriteLog.write("录入酒店价格出错", "酒店id:"
									+ hotelprice.getHotelid());
						}
						String allot = stay.getChildText("ALLOT");
						// 获得分配代码
						// System.out.println("allot:"+allot);
						hotelprice.setAllot(allot);
						// Y-房型已獲得分配, 能夠即時確認 N-房型未獲得分配, 等待回覆 C-酒店關閉
						String isallot = stay.getChildText("IS_ALLOT");
						// System.out.println("isallot:" + isallot);
						// if (isallot.equals("Y")) {
						// hotelprice.setIsallot("房型已获得分配,能够及时确认");
						// }
						// if (isallot.equals("N")) {
						// hotelprice.setIsallot("房型未获得分配,等待回复");
						// }
						hotelprice.setIsallot(isallot);
						// if (isallot.equals("C")) {
						// Hotel hotel2 =
						// Server.getInstance().getHotelService().findHotel(hotelprice.getHotelid());
						// System.out.println("已关闭酒店的名字:" +
						// hotel2.getName());
						// hotel2.setState(0);
						// hotel2.setStatedesc("华闽数据中这家酒店已经关闭");
						// Server.getInstance().getHotelService().updateHotel(hotel2);
						// Roomtype roomtype = Server.getInstance()
						// .getHotelService().findRoomtype(
						// hotelprice.getRoomtypeid());
						// roomtype.setState(0);
						// Server.getInstance().getHotelService()
						// .updateRoomtypeIgnoreNull(roomtype);
						// } else {
						// System.out.println(" WHERE " +
						// Hmhotelprice.COL_hotelid + "='" +
						// hotelprice.getHotelid() + "' AND "
						// + Hmhotelprice.COL_roomtypeid + "='" +
						// hotelprice.getRoomtypeid() + "' AND " +
						// Hmhotelprice.COL_statedate + "='"
						// + hotelprice.getStatedate() + "' AND " +
						// Hmhotelprice.COL_serv + "='" +
						// hotelprice.getServ() + "'");
						if (hmhotelpricefromtable.size() > 0) {
							hotelprice.setId(hmhotelpricefromtable.get(0)
									.getId());
							Server.getInstance().getHotelService()
									.updateHmhotelpriceIgnoreNull(hotelprice.getId(),hotelprice.getHotelid(),"",hotelprice);
							System.out.println("更新一条价格~~~~");
						} else {
							Server.getInstance().getHotelService()
									.createHmhotelprice(hotelprice);
							System.out.println("插入一条价格~~~~");
							// }
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取当前日期31天后的日期
	 * 
	 * @return
	 */
	public static Date CatchNext31Day(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.roll(Calendar.MONTH, 1);
		cal.roll(Calendar.DATE, -1);
		Date newdate = cal.getTime();
		return newdate;
	}
}
