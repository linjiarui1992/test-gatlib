package com.ccservice.offlineExpress.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.ems.EmsService;
import com.ccservice.offlineExpress.sf.SfService;
import com.ccservice.offlineExpress.uupt.UUptService;

public class RouteCallBackUtil {

    /**
     * 根据订单号更新物流状态
     * 
     * @param orderId
     * @return
     * @time 2017年11月8日 下午3:26:30
     * @author liujun
     * @throws Exception 
     */
    public static void flushExpressRoute(String logName, long random, String expressNum) {
        if (expressNum != null && !"".equals(expressNum)) {
            String sql = "SELECT ORDERID, ExpressAgent FROM mailaddress WITH (NOLOCK) WHERE ExpressNum = '" + expressNum
                    + "'";
            DataTable dataTable = new DataTable();
            try {
                dataTable = DBHelperOffline.GetDataTable(sql, null);
            }
            catch (Exception e) {
                // 记录异常日志
                ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
            }
            if (dataTable != null & dataTable.GetRow().size() > 0) {
                String orderId = String.valueOf(dataTable.GetRow().get(0).GetColumn("ORDERID").GetValue());//订单ID
                String expressAgent = String.valueOf(dataTable.GetRow().get(0).GetColumn("ExpressAgent").GetValue());//快递种类
                WriteLog.write(logName,
                        random + "-->订单ID：" + orderId + "---快递种类：" + expressAgent + "---快递单号：" + expressNum);
                if (!"".equals(orderId) && !"".equals(expressAgent)) {
                    JSONObject reqResultJson = new JSONObject();
                    String method = "getExpressRoute";
                    JSONObject data = new JSONObject();
                    data.put("expressNum", expressNum);
                    try {
                        if ("0".equals(expressAgent)) {
                            reqResultJson = new SfService().operate(logName, "sf", random, method, data, reqResultJson);
                        }
                        else if ("1".equals(expressAgent)) {
                            reqResultJson = new EmsService().operate(logName, "ems", random, method, data,
                                    reqResultJson);
                        }
                        else if ("10".equals(expressAgent)) {
                            reqResultJson = new UUptService().operate(logName, "uupt", random, method, data,
                                    reqResultJson);
                        }
                    }
                    catch (Exception e) {
                        // 记录异常日志
                        ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
                    }
                    WriteLog.write(logName, random + "-->订单ID：" + orderId + "-->查询路由信息返回" + reqResultJson);
                    String sb1 = "";
                    int status = 1;
                    String takeTime = "";
                    if (reqResultJson.getBoolean("success")) {
                        JSONArray jsonArray = JSONObject.parseArray(reqResultJson.getString("result"));
                        if (jsonArray != null && jsonArray.size() > 0) {
                            for (int i = 0; i < jsonArray.size(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                sb1 += jsonObject.getString("acceptTime") + "DDD" + jsonObject.getString("remark")
                                        + "BBB";
                            }
                        }
                        if (!"".equals(sb1)) {
                            if ("0".equals(expressAgent)) {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                if (sb1.contains("已签收")) {
                                    status = 3;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                                else if (sb1.contains("已收取快件")) {
                                    status = 2;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                            }
                            else if ("1".equals(expressAgent)) {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                if (sb1.contains("投递员") || sb1.contains("签收人")) {
                                    status = 3;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                                else if (sb1.contains("已收件")) {
                                    status = 2;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                            }
                            else if ("10".equals(expressAgent)) {
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                if (sb1.contains("完成收货")) {
                                    status = 3;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                                else if (sb1.contains("已取件")) {
                                    status = 2;
                                    takeTime = jsonObject.getString("acceptTime");
                                }
                            }
                        }
                        insertOrUpdate(takeTime, status, sb1, orderId, random, logName);
                    }
                }
            }
        }
    }

    /**
     * 更新和插入快递时效
     * 
     * @time 2017年11月8日 下午4:11:59
     * @author liujun
     */
    private static void insertOrUpdate(String takeTime, int expressStatus, String routeMsg, String orderid, long random,
            String logName) {
        //更新影响行数
        int updateRow = 0;
        //当前时间
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        //查询订单是否有物流
        String sql = "SELECT Id FROM TrainExpressOffline WITH (NOLOCK) WHERE OrderId = " + orderid;
        //更新sql
        String updateSql = "";
        DataTable dataTable = new DataTable();
        //异常
        try {
            //查询订单是否有物流
            dataTable = DBHelperOffline.GetDataTable(sql, null);
            //有-->更新物流  无-->插入
            if (dataTable != null && dataTable.GetRow().size() > 0) {
                updateSql = "UPDATE TrainExpressOffline SET TakeTime = '" + takeTime + "', RefreshTime = '" + date
                        + "', ExpressStatus = '" + expressStatus + "', RouteMsg = '" + routeMsg + "' WHERE OrderId ="
                        + orderid;
                //执行sql
                updateRow = DBHelperOffline.UpdateData(updateSql);
                //判断更新结果
                if (updateRow == 0) {
                    //记录日志
                    WriteLog.write(logName + "_物流信息更新失败", random + "-->OrderId:" + orderid);
                }
            }
            else {
                updateSql = "INSERT INTO TrainExpressOffline(OrderId,TakeTime,RefreshTime,ExpressStatus,RouteMsg) VALUES("
                        + orderid + ",'" + takeTime + "','" + date + "'," + expressStatus + ",'" + routeMsg + "')";
                //执行sql
                updateRow = DBHelperOffline.insertSql(updateSql);
                //判断更新结果
                if (updateRow == 0) {
                    //记录日志
                    WriteLog.write(logName + "_物流信息插入失败", random + "-->OrderId:" + orderid);
                }
            }
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName, e, String.valueOf(random));
        }
    }
}
