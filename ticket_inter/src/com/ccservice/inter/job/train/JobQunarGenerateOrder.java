package com.ccservice.inter.job.train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.JMSException;

import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccervice.util.ActiveMQUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThread;
import com.ccservice.inter.job.train.thread.MyThreadQunar;
import com.ccservice.inter.server.Server;

public class JobQunarGenerateOrder extends TrainSupplyMethod implements Job {
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
    }

    public void exThreadQunarJSP(long orderid) {
        if (orderid > 0) {
            //qunar回调地址
            String QunarCallBack = getSysconfigString("QunarCallBack");
            //qunar配置环境
            String merchantCode = "hangt";
            //配置循环下单次数
            Integer ordermax = Integer.valueOf(getSysconfigString("ordermax"));
            if (ordermax < 0) {
                ordermax = 4;
            }
            //配置线程值大小
            Integer orderingpoolsize = Integer.valueOf(getSysconfigString("orderingpoolsize"));
            if (orderingpoolsize < 0) {
                orderingpoolsize = 50;
            }
            // 创建一个可重用固定线程数的线程池
            ExecutorService pool = Executors.newFixedThreadPool(orderingpoolsize);
            // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
            Thread t1 = null;
            //1'获取待出票订单
            String sql = "";
            sql = "UPDATE T_TRAINORDER SET C_STATE12306=2 WHERE  C_ISQUESTIONORDER=0 AND ID =" + orderid;
            int ordersum = Server.getInstance().getSystemService().excuteGiftBySql(sql);
            WriteLog.write("QUNAR自动占座是否正常", orderid + ":" + ordersum);
            t1 = new MyThreadQunar(orderid, ordermax, QunarCallBack, merchantCode);
            pool.execute(t1);
            pool.shutdown();
        }
    }
    /**
     * 
     * @Title:       sendQunarOrderIdToMsg 
     * @Description: TODO(将下单订单放入MQ) 
     * @param：		 @param orderid	
     * @return：              void    返回类型 
     * @author       wangwei
     * @throws
     */
    public void sendQunarOrderIdToMsg(long orderid){
    	if (orderid > 0) {
            //qunar回调地址
            String QunarCallBack = getSysconfigString("QunarCallBack");
            //qunar配置环境
            String merchantCode = "hangt";
            //配置循环下单次数
            Integer ordermax = Integer.valueOf(getSysconfigString("ordermax"));
            if (ordermax < 0) {
                ordermax = 4;
            }
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("ordermax", ordermax);
            jsoseng.put("QunarCallBack", QunarCallBack);
            jsoseng.put("merchantCode", merchantCode);
            
            try {
				ActiveMQUtil.sendMessage(getSysconfigString("activeMQ_url"), "Qunar_Train_Order", jsoseng.toString());
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    /**
     * 说明:接口可调用时间:早7晚11
     * @param date
     * @return
     * @time 2014年8月30日 下午4:23:20
     * @author yinshubin
     */
    public boolean getNowTime() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("07:01:00");
            Date dateAfter = df.parse("23:00:00");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
        }
        return false;//现在24小时,以后有需要再改为FALSE
    }
}
