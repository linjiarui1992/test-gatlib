
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderSource.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrderSource">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PlatformOrder"/>
 *     &lt;enumeration value="InputOrder"/>
 *     &lt;enumeration value="OuterInerface"/>
 *     &lt;enumeration value="B2B2GInterface"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrderSource")
@XmlEnum
public enum OrderSource {

    @XmlEnumValue("PlatformOrder")
    PLATFORM_ORDER("PlatformOrder"),
    @XmlEnumValue("InputOrder")
    INPUT_ORDER("InputOrder"),
    @XmlEnumValue("OuterInerface")
    OUTER_INERFACE("OuterInerface"),
    @XmlEnumValue("B2B2GInterface")
    B_2_B_2_G_INTERFACE("B2B2GInterface");
    private final String value;

    OrderSource(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrderSource fromValue(String v) {
        for (OrderSource c: OrderSource.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
