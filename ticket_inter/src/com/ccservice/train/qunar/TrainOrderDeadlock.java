package com.ccservice.train.qunar;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.train.mqlistener.TrainActiveMQ;

public class TrainOrderDeadlock {
    private Trainorder trainorder;

    private long orderid;

    public TrainOrderDeadlock(long orderid) {
        this.orderid = orderid;
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
    }

    public void payTrue() {
        this.trainorder.setState12306(Trainorder.ORDEREDPAYED);
        this.trainorder.setSupplypayway(14);
        this.trainorder.setSupplytradeno("qunarpaytrue");
        float differenceprice = getdirrerenceprice(trainorder);
        writeRc(this.orderid, Trainticket.ISSUED, "支付成功", "qunar支付接口接口", 1);
        trainorder.setOrderstatus(Trainorder.ISSUED);
        try {
            Server.getInstance().getTrainService().refusequnarTrain(trainorder, Trainticket.ISSUED);
            writeRc(this.orderid, Trainticket.ISSUED, "出票完成。退还差价:" + differenceprice + "元", "qunar出票接口", 1);
            trainorder.setIsquestionorder(Trainorder.NOQUESTION);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
        catch (Exception e) {
            WriteLog.write("代付死锁", "代付成功:" + this.trainorder.getOrdernumber());
            writeRc(this.orderid, Trainticket.ISSUED, "出票完成。退还差价:" + differenceprice + "元--->死锁", "qunar出票接口", 1);
            try {
                new TrainActiveMQ(orderid).sendDeadlockMQ();
                WriteLog.write("代付死锁_转队列", "代付成功转队列成功:" + this.trainorder.getOrdernumber());
            }
            catch (Exception e1) {
                WriteLog.write("代付死锁_转队列", "代付成功转队列失败:" + this.trainorder.getOrdernumber());
            }
        }
    }

    /**
    * 说明:得到订单差价 
    * @param trainorder
    * @return
    * @time 2014年9月1日 下午3:29:14
    * @author yinshubin
    */
    public float getdirrerenceprice(Trainorder trainorder) {
        float differenceprice = 0f;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                differenceprice += trainticket.getPayprice() - trainticket.getPrice();
            }
        }
        return differenceprice;
    }

    /**
     * 书写操作记录 
     * @param orderid
     * @param status
     * @param content
     * @param createuser
     * @param ywtype
     * @time 2015年1月22日 下午4:45:07
     * @author fiend
     */
    public void writeRc(long orderid, int status, String content, String createuser, int ywtype) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(orderid);
        rc.setStatus(status);
        rc.setContent(content);
        rc.setCreateuser(createuser);
        rc.setYwtype(ywtype);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

}
