package com.ccservice.mixdata;

import java.util.*;
import java.text.SimpleDateFormat;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.b2b2c.base.roomtypeofficial.RoomTypeOfficial;

/**
 * 删除冗余正式酒店
 * @author WH
 */

public class DeleteRedundancyHotelAll {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println("Start...");
		//正式酒店
		String sql = "where c_qunarid in (select c_qunarid from T_HOTELALL where c_qunarid !='' group by c_qunarid having COUNT(*)>1)";
		List<Hotelall> list = Server.getInstance().getHotelService().findAllHotelall(sql, "order by c_qunarid , id", -1, 0);
		//临时酒店
		String ids = "";
		for(Hotelall h:list){
			ids += h.getId() + ",";
		}
		if(ids.contains(",")){
			ids = ids.substring(0, ids.length()-1);
			List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel("where C_ZSHOTELID in ("+ids+")", "", -1, 0);
			//临时酒店
			Map<Long,List<Hotel>> lsmap = new HashMap<Long, List<Hotel>>();
			for(Hotel h:hotels){
				List<Hotel> temp = lsmap.get(h.getZshotelid());
				if(temp==null) temp = new ArrayList<Hotel>();
				temp.add(h);
				lsmap.put(h.getZshotelid(), temp);
			}
			//正式酒店
			Map<String,List<Hotelall>> zsmap = new HashMap<String, List<Hotelall>>();
			for(Hotelall h:list){
				String key = h.getQunarId().trim();
				List<Hotelall> temp = zsmap.get(key);
				if(temp==null) temp = new ArrayList<Hotelall>();
				temp.add(h);
				zsmap.put(key, temp);
			}
			int total = zsmap.size();
			for(String key:zsmap.keySet()){
				List<Hotelall> temp = zsmap.get(key);
				if(temp.size()>1){
					//ID小到大排序
					Collections.sort(temp, new Comparator<Hotelall>(){
						public int compare(Hotelall a, Hotelall b) {
							if(a.getId()>b.getId()){
								return 1;
							}else{
								return -1;
							}
						}
					});
					//判断是否关联了临时酒店
					int flagcount = 0;
					for(Hotelall t:temp){
						if(lsmap.get(t.getId())!=null){
							flagcount++;
						}
					}
					//无关联
					if(flagcount==0){
						//留一个ID小的
						for(int i = 0 ; i < temp.size() ; i++){
							if(i>0) delZsHotel(temp.get(i));
						}
					}
					//有关联
					else{
						//留关联的
						for(int i = temp.size()-1 ; i >=0 ; i--){
							Hotelall t = temp.get(i);
							if(lsmap.get(t.getId())==null){
								delZsHotel(t);
								temp.remove(t);
							}
						}
						//多个关联、留一个ID小的
						if(flagcount>1){
							//ID小的
							Hotelall first = temp.get(0);
							//其他要删除的
							temp.remove(first);
							for(int i = 0 ; i < temp.size() ; i++){
								Hotelall t = temp.get(i);
								upZsHotel(first,lsmap.get(t.getId()));
								delZsHotel(t);
							}
						}
					}
				}
				total--;
				System.out.println("剩余：" + total);
			}
		}
		System.out.println("End...");
		long end = System.currentTimeMillis();
		System.out.println("耗时：" + (end-start)/1000 + "秒");
	}
	
	@SuppressWarnings("unchecked")
	private static void delZsHotel(Hotelall t){
		System.out.println("Delete：" + t.getId() + "/" + t.getName());
		long zsid = t.getId();
		//正式房型
		List<RoomTypeOfficial> zsrooms = Server.getInstance().getHotelService().findAllRoomTypeOfficial("where C_ZSHOTELID = " + zsid, "", -1, 0);
		String ids = "";
		for(RoomTypeOfficial r:zsrooms){
			ids += r.getId() + ",";
		}
		String updatesql = "";
		String updatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		if(ids.endsWith(",")){
			ids = ids.substring(0, ids.length()-1);
			//删除正式房型
			String deletesql = "delete from T_ROOMTYPEOFFICIAL where C_ZSHOTELID = " + zsid;
			Server.getInstance().getSystemService().findMapResultBySql(deletesql, null);
			//匹配的供应房型清空正式房型ID
			if(ids.contains(",")){
				updatesql = "update T_ROOMTYPE set C_ZSROOMTYPEID = null , C_MODIFYTIME = '"+updatetime+"' where C_ZSROOMTYPEID in (" + ids + ")";
			}else{
				updatesql = "update T_ROOMTYPE set C_ZSROOMTYPEID = null , C_MODIFYTIME = '"+updatetime+"' where C_ZSROOMTYPEID = " + ids;
			}
			Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
		}
		//价格更新正式酒店ID
		updatesql = "update T_HMHOTELPRICE set C_ZSHOTELID = null , C_ZSROOMTYPEID = null , C_UPDATETIME = '"+updatetime+"' where C_ZSHOTELID = " + zsid;
		Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
		//临时酒店
		updatesql = "update T_HOTEL set C_ZSHOTELID = null , C_LASTUPDATETIME = '"+updatetime+"' where C_ZSHOTELID = " + zsid;
		Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
		//删除正式酒店
		Server.getInstance().getHotelService().deleteHotelall(zsid);
	}
	
	@SuppressWarnings("unchecked")
	private static void upZsHotel(Hotelall hotelall,List<Hotel> hotels){
		System.out.println("Update：" + hotelall.getId() + "/" + hotelall.getName());
		long hotelAllId = hotelall.getId();
		for(Hotel hotel:hotels){
			long hotelId = hotel.getId();
			//Hotel
			hotel.setZshotelid(hotelAllId);
			Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
			//通过关联的Hotel的支付方式来修改Hotelall的支付方式
			List<Hotel> temphotels = Server.getInstance().getHotelService().findAllHotel("where c_paytype in (1,2) and c_zshotelid = " + hotelAllId, "", -1, 0);
			if(temphotels==null || temphotels.size()==0){
				hotelall.setPaytype(null);
			}else{
				List<Long> paytypelist = new ArrayList<Long>();
				for(Hotel temph:temphotels){
					Long temppaytype = temph.getPaytype();
					if(temppaytype!=null && !paytypelist.contains(temppaytype)){
						paytypelist.add(temppaytype);
					}
				}
				if(paytypelist.size()==0){
					hotelall.setPaytype(null);
				}else if(paytypelist.size()==1){
					hotelall.setPaytype(paytypelist.get(0));
				}else{
					hotelall.setPaytype(3l);
				}
			}
			boolean flag = false;
			Long oldHotelAllPayType = hotelall.getPaytype();
			if(oldHotelAllPayType==null){
				if(hotelall.getPaytype()!=null){
					flag = true;
				}
			}else{
				if(hotelall.getPaytype()==null){
					flag = true;
				}else if(!hotelall.getPaytype().equals(oldHotelAllPayType)){
					flag = true;
				}
			}
			if(flag){
				Server.getInstance().getHotelService().updateHotelallIgnoreNull(hotelall);//更新正式酒店信息
			}
			//正式房型更新正式酒店ID
			String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
			String tempsql = "where c_hotelid = " + hotelId;
			String sql = "update T_ROOMTYPEOFFICIAL set C_ZSHOTELID = " + hotelAllId + ",C_MODIFYTIME = '" + time + "'" + tempsql;
			Server.getInstance().getSystemService().findMapResultBySql(sql, null);
		    //价格更新正式酒店ID
		    sql = "update t_hmhotelprice set c_zshotelid = "+hotelAllId + ",C_UPDATETIME = '" + time + "'" + tempsql;
		    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
		}
	}
}
