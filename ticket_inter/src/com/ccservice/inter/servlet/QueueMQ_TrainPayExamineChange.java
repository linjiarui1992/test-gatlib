package com.ccservice.inter.servlet;

import javax.jms.Session;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.ConnectionFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.ActiveMQConnectionFactory;
import com.ccservice.train.mqlistener.TrainorderChangePayMessageListener;

/**
 * 改签审核
 * @author 彩娜
 * @time 2016年1月15日 下午4:59:34
 * @version 1.0
 */

@SuppressWarnings("serial")
public class QueueMQ_TrainPayExamineChange extends HttpServlet {

    private String isstart = "";//是否开启

    private String mqaddress = "";//MQ地址

    private String mqusername = "";//MQ用户名

    private int checkchangeorderpaynum = 0;

    public void init() throws ServletException {
        super.init();
        isstart = getInitParameter("isstart");
        mqaddress = getInitParameter("mqaddress");
        mqusername = getInitParameter("mqusername");
        checkchangeorderpaynum = Integer.parseInt(getInitParameter("checkchangeorderpaynum"));
        //审核开启
        if ("1".equals(this.isstart)) {
            System.out.println("改签支付审核队列-----开启");
            payExamine();
        }
    }

    private void payExamine() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < this.checkchangeorderpaynum; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainorderChangePayMessageListener());
            }
            conn.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}