package com.ccservice.jielv.hotel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.compareprice.HotelData;
import com.ccservice.compareprice.HttpClient;


public class QunarRoomType implements Job  {

	private QunarRoomType proc = null;// 比价工具对象
	public final static String YDX_ID = "wiotatts037";// 易定行id
	public static void main(String[] args) {
		QunarRoomType dp=new QunarRoomType();
		dp.test();
	}



	/**
	 * 构造器，实例化比价对象
	 */
	public QunarRoomType() {
		proc = new QunarRoomType();
	}


	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
		QunarRoomType dp=new QunarRoomType();
		dp.test();
	}
	public void test() {
		List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel("where C_qunarId is not null and C_qunarId!='' and c_sourcetype=6  and id in (select distinct c_hotelid from t_hotelgooddata) and id in (select c_hotelid from t_roomtype where id in (select distinct c_roomtypeid from t_hotelgooddata))",
				"ORDER BY ID ASC", -1, 0);
		Set<String> hotelnames = new HashSet<String>();
		for (Hotel hotel : hotels) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 0); // 明天
			Date date = calendar.getTime();
			List<HotelData> hoteDatas = loadQunarData(hotel, date);
			for (HotelData hotelData : hoteDatas) {
				Roomtype roomtype = Server.getInstance().getHotelService()
				.findRoomtype(hotelData.getRoomtypeid());
				System.out.println(roomtype.getName());
				if (hotelData.getRoomname() != null&& !"".equals(hotelData.getRoomname())) {
					hotelnames.add(hotel.getId() + ",");
					System.out.print("酒店名称："+hotel.getName());
					System.out.println(roomtype.getName() + ":"+ hotelData.getRoomname());
					roomtype.setQunarname(hotelData.getRoomname());
					Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(roomtype);
				}
			}
		}
		System.out.println(hotelnames);
		
	}
	// 加载去哪酒店数据
	public List<HotelData> loadQunarData(Hotel hotel, Date date) {
		List<HotelData> listHotelData = new ArrayList<HotelData>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String citycode = "";
		if (hotel.getQunarId() != null && !"".equals(hotel.getQunarId())) {
			citycode = hotel.getQunarId().substring(0,
					hotel.getQunarId().lastIndexOf("_"));
			String url = "http://hotel.qunar.com/price/detail.jsp?fromDate="
					+ sdf.format(date) + "&toDate=" + sdf.format(cal.getTime())
					+ "&cityurl=" + citycode + "&HotelSEQ="
					+ hotel.getQunarId().trim();
			String str = HttpClient.httpget(url, "utf-8");
			if (str != null && !str.equals("")) {
				String key = str.trim();
				if (!key.equals("")) {
					String strstr = key.substring(key.indexOf("(") + 1, key
							.trim().indexOf("/*"));
					JSONObject datas = JSONObject.fromObject(strstr + "}");
					JSONObject result = datas.getJSONObject("result");
					Iterator<String> strs = result.keys();
					HotelData hoteldata;
					while (strs.hasNext()) {
						hoteldata = new HotelData();
						String keys = strs.next();
						JSONArray arra = JSONArray.fromObject(result.get(keys));
						if (arra.get(0) != null) {
							hoteldata.setSealprice(Integer.parseInt(arra.get(0)
									.toString()));
						}
						hoteldata.setAgentNo(keys.substring(0, keys.trim()
								.indexOf("|")));
						hoteldata.setHotelid(hotel.getId() + "");
						hoteldata.setHotelname(hotel.getName());
						if (arra.get(2) != null) {
							String strtemp = arra.getString(2);
							hoteldata.setRoomnameBF(getString(strtemp));
						}
						// 开关房
						if (arra.get(9) != null) {
							String strtemp = arra.getString(9);
							hoteldata.setRoomstatus(Integer.parseInt(strtemp));
						}
						// 现预付
						if (arra.get(14) != null) {
							String strtemp = arra.getString(14);
							hoteldata.setType(Integer.parseInt(strtemp));
						}
						if (arra.get(4) != null) {
							String strtemp = arra.getString(4);
							if (YDX_ID.equals(hoteldata.getAgentNo())) {
								String roomtypeidtemp = strtemp.substring(
										strtemp.indexOf("roomId=") + 7,
										strtemp.indexOf("&cpcRoomType")).trim();
								String roomtypeid=roomtypeidtemp.substring(roomtypeidtemp.lastIndexOf("_")+1);
								hoteldata.setRoomtypeid(Long.parseLong(roomtypeid));
							}
						}
						if (arra.get(3) != null) {
							if (YDX_ID.equals(hoteldata.getAgentNo())) {
								hoteldata.setRoomname(arra.getString(3));
								listHotelData.add(hoteldata);
							}

						}
					}
				}
			}
		}
		return listHotelData;
	}

	// 处理以上字符串，获取汉字内容
	public String getString(String str) {
		String[] st = str.split("<(\\S*?)[^>]*>.*?");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < st.length; i++) {
			String temp = st[i].trim();
			if (!temp.equals("")) {
				sb.append(temp);
			}
		}
		return sb.toString();
	}
}
