package com.ccservice.t12306;

import java.util.Comparator;

public class Ticket12306 {

    private String traindate;

    private float price;

    private String seat;

    private String traincode;

    private String secretstr;

    private String train_no;

    Ticket12306(float price, String seat, String traindate, String secretstr, String train_no, String traincode) {
        this.price = price;
        this.seat = seat;
        this.traindate = traindate;
        this.secretstr = secretstr;
        this.train_no = train_no;
        this.traincode = traincode;
    }

    public Ticket12306() {
    }

    static class ticket12306Comparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Ticket12306 s1 = (Ticket12306) o1;
            Ticket12306 s2 = (Ticket12306) o2;
            int result = s1.price > s2.price ? 1 : -1;
            // 注意：此处在对比num相同时，再按照name的首字母比较。
            return result;
        }
    }

    public String getTraindate() {
        return traindate;
    }

    public void setTraindate(String traindate) {
        this.traindate = traindate;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getTraincode() {
        return traincode;
    }

    public void setTraincode(String traincode) {
        this.traincode = traincode;
    }

    public String getSecretstr() {
        return secretstr;
    }

    public void setSecretstr(String secretstr) {
        this.secretstr = secretstr;
    }

    public String getTrain_no() {
        return train_no;
    }

    public void setTrain_no(String train_no) {
        this.train_no = train_no;
    }

    //    "gg_num": "--",
    //    "gr_num": "--",
    //    "qt_num": "--",
    //    "rw_num": "11",
    //    "rz_num": "--",
    //    "tz_num": "--",
    //    "wz_num": "有",
    //    "yb_num": "--",
    //    "yw_num": "有",
    //    "yz_num": "无",
    //    "ze_num": "--",
    //    "zy_num": "--",
    //    "swz_num": "--"

}
