package com.ccservice.mixdata;

import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import java.text.SimpleDateFormat;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.b2b2c.atom.qunar.PHUtil;
import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

public class QunarHotelLowestPrice {
	public static void main(String[] args) throws Exception{
		QunarHotelLowestPrice.start();
	}
	
	@SuppressWarnings("unchecked")
	public static void start() throws Exception{
		//城市
		List<City> citys = Server.getInstance().getHotelService().findAllCity("where c_qunarcode is not null", "order by c_superiorcity,id", -1, 0);
		for(City c:citys){
			System.out.println("当前城市---" + c.getName());
			long start = System.currentTimeMillis();
			if(!UpdateElDataUtil.StringIsNull(c.getQunarcode())){
				//酒店
				String sql = "where (c_startprice is null or c_startprice = 0) and c_qunarid is not null and c_cityid = " + c.getId();
				List<Hotelall> hotels = Server.getInstance().getHotelService().findAllHotelall(sql, "order by c_state desc", -1, 0);
				if(hotels!=null && hotels.size()>0){
					int total = 15;//一次请求15个去哪儿酒店
					int remainder = hotels.size()%total;//不足total个的酒店数
					int count = 0;
					String HotelIds = "";//去哪儿酒店ID
					Map<String,Hotelall> HotelMap = new HashMap<String, Hotelall>();//<去哪儿酒店ID,本地酒店>
					for (int i = 0; i < hotels.size(); i++) {
						//本地酒店
						Hotelall hotel = hotels.get(i);
						//去哪儿酒店ID
						String qunarHotelId = hotel.getQunarId();
						//赋值
						HotelIds += qunarHotelId + ",";
						HotelMap.put(qunarHotelId, hotel);
						//最多15个
						count++;
						if(count==total || (remainder>0 && i==hotels.size()-1)){
							HotelIds = HotelIds.substring(0,HotelIds.length()-1);//去掉最后的逗号
							//请求去哪儿
							String fromDate = ElongHotelInterfaceUtil.getCurrentDate();
							String toDate = ElongHotelInterfaceUtil.getAddDate(fromDate, 1);
							String url = "http://hotel.qunar.com/price/api3.jsp?output=json1.1&showPrices=0&cityurl="+c.getQunarcode()+
										 "&fromDate="+fromDate+"&toDate="+toDate+"&ids="+HotelIds+"&v=0.2778531233780086&requestor=RT_HSLIST_D";
							System.out.println("请求URL：" + url);
							String json = PHUtil.submitPost(url, "").toString();
							//解析JSON
							if(!ElongHotelInterfaceUtil.StringIsNull(json) && json.contains("price")){
								JSONObject obj = JSONObject.fromObject(json);
								JSONArray qunarhotels = obj.getJSONArray("hotels");
								if(qunarhotels!=null && qunarhotels.size()>0){
									for(int j = 0 ; j < qunarhotels.size() ; j++){
										JSONObject h = qunarhotels.getJSONObject(j);
										String id = h.getString("id");
										double price = h.getDouble("price");
										if(!ElongHotelInterfaceUtil.StringIsNull(id) && price>0 && HotelMap.containsKey(id) && HotelMap.get(id).getId()>0){
											Hotelall current = new Hotelall();
											current.setId(HotelMap.get(id).getId());
											current.setStartprice(price);
											current.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
											Server.getInstance().getHotelService().updateHotelallIgnoreNull(current);
											System.out.println("更新[" + HotelMap.get(id).getName() + "]最低价格---" + price);
										}
									}
								}
							}
							//置空
							count = 0;
							HotelIds = "";
							HotelMap = new HashMap<String, Hotelall>();
						}
					}
				}
			}
			long end = System.currentTimeMillis();
			System.out.println("更新"+c.getName()+"酒店最低价格和状态完成-----耗时-----" + DateSwitch.showTime(end - start));
		}
		System.out.println("结束所有更新.");
	}
}
