package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.RepServerBean;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.Job12306ReleaseCustomeruserReLogintoCheckHeyanMyThread;
import com.ccservice.inter.job.train.thread.MyThreadLogin;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

/**
 * 一天只能运行一次执行初始化账号
 * 早上7点
 * 定时将12306账号中乘客数量少于90的账号登录
 * 
 * 
 * 同时给同程发通知是否 4.15. 是否可以正常下单通知
 * @time 2014年8月30日 上午11:45:08
 * @author yinshubin
 */
public class Job12306index extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //        Server.getServer().setCustomeruser12306account(new ArrayList<Customeruser>());
        //        exThread();
    }

    public void exThread() {
        if (GoAccountSystem()) {
            return;
        }
        //把前一天的账号都修改为未登录的状态          
        //（C_STATE=0未登录,,C_TYPE=4:12306账号,可用的12306账号）
        //C_ENNAME=1可使用;
        //3:封闭期(指该账号处于封印期当该账号里的订单过了发车时间再解封,解封前暂时不可用) 
        //4:封闭期仅限当天不可用
        /*已经放到sqlserver的job里
         * String sql = "UPDATE T_CUSTOMERUSER SET C_STATE=0,C_ENNAME=1 WHERE C_TYPE=4 "
                + "AND (C_ISENABLE=1 OR C_ISENABLE=4) AND (C_STATE=1 or C_STATE=4)";
        int initcount = Server.getInstance().getMemberService().excuteSysconfigBySql(sql);
        WriteLog.write("Job12306index", "initcount:" + initcount);*/
        // 创建一个可重用固定线程数的线程池
        ExecutorService pool = Executors.newFixedThreadPool(200);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
        Thread t1 = null;
        //1'获取待出票订单
        int indexsum = Integer.valueOf(getSysconfigString("indexsum"));
        WriteLog.write("Job12306index", "当天设置自动登录的账号总数:" + indexsum);
        String where = " WHERE C_LOGINNUM>0 AND C_LOGINNUM<90  AND C_TYPE=4 AND C_ISENABLE=1 AND C_STATE=0 ";
        List<Customeruser> customerusers = Server.getInstance().getMemberService()
                .findAllCustomeruser(where, " ORDER BY C_LOGINNUM ASC ", indexsum * 2, 0);
        WriteLog.write("Job12306index", "符合自动登录的账号总数:" + customerusers.size());
        for (Customeruser customeruser : customerusers) {
            try {
                Thread.sleep(100L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            //            String url = randomIp();
            //            RepServerBean rep = getrepurl(1);
            // 将线程放入池中进行执行
            //            t1 = new MyThreadLogin(customeruser, url);
            //            t1 = new MyThreadLogin(customeruser, rep);
            pool.execute(t1);
        }
        // 关闭线程池
        pool.shutdown();
    }

    /**
     * 4.15. 是否可以正常下单通知
     *   说明：为了提供更稳定可靠的服务，供应商应于每天早晨 7：00 自测一个订单（自 测单大约耗时不超过3 分钟），将测试结果通知给同程，
     *   同程根据通知的结果正常与 否再决定是否开放自己的下单接口，即供应商通知接口正常，同程则可放开自己的下单接口， 正常运行；
     *   若通知不正常，同程则不需要放开接口。
     *   意义：不正常的几率很小，做这项工作的目的就在于避免这些微小的非正常几率带来更 多更大的负面影响。
     *   做法：同程提供接受通知的 URL，供应商通过 POST 方式请求此 URL，将是否可以开 放下单接口通知给同程。
     * @time 2014年12月26日 下午1:26:46
     * @author chendong
     */
    private void sendmsgTotongchengenableCreateOrder() {
        int r1 = new Random().nextInt(10000000);
        String tongchengurl = "";

        String key = "x3z5nj8mnvl14nirtwlvhvuialo0akyt";//正式的key
        String partnerid = "tongcheng_train";//正式的partnerid
        //        reqtime 请求时间，格式：yyyyMMddHHmmss（非空）例：20140101093518
        String reqtime = getreqtime();
        //        sign    数字签名=md5(partnerid+reqtime+md5(key)) 其中 partnerid 为同程登录开放平台的账户，key 是之前开放平台 分配给同程使用的 key。
        //md5 算法得到的字符串全部为小写
        String sign = getsign(partnerid, reqtime, key);
        //        toconfirmticket 是否可以正常下单出票：YES 表示接口正常，可以下单出票，NO 表        示不正常，不能下单
        String toconfirmticket = "YES";
        String paramContent = "{\"reqtime\":\"" + reqtime + "\",\"sign\":\"" + sign + "\",\"toconfirmticket\":\""
                + toconfirmticket + "\"}";
        WriteLog.write("t同程火车票接口_4.15是否可以正常下单通知", r1 + ":tongchengurl:" + tongchengurl);
        WriteLog.write("t同程火车票接口_4.15是否可以正常下单通知", r1 + ":paramContent:" + paramContent);
        String responseString = SendPostandGet.submitPost(tongchengurl, paramContent, "utf-8").toString();
        WriteLog.write("t同程火车票接口_4.15是否可以正常下单通知", r1 + ":responseString:" + responseString);
    }

    /**
     * md5(partnerid+method+reqtime+md5(key))，
     * 
     * @time 2014年12月26日13:20:38
     * @author chendong
     */
    private String getsign(String partnerid, String reqtime, String key) {
        return MD5Util.MD5Encode(partnerid + reqtime + MD5Util.MD5Encode(key, "UTF-8"), "UTF-8");
    }

    private String getreqtime() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static void main(String[] args) {
        new Job12306index().exThread();
    }
}
