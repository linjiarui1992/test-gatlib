package com.ccservice.b2b2c.policy;

import java.rmi.RemoteException;
import java.util.Iterator;

import net._8000yi.websvr.newply.webinterface.orderservice_asmx.OrderServiceStub;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;

import sun.util.logging.resources.logging;

import com.ccservice.b2b2c.policy.ben.BaQianYiBook;
import com.ccservice.inter.job.WriteLog;

public class AirutilNew40 {
    BaQianYiBook baQianYiBook = new BaQianYiBook();

    private String name = baQianYiBook.getUsername8000();

    private String pwd = baQianYiBook.getPassword8000();

    /**
     * 8000yi4.0文档,根据RT信息下订单
     * 
     * @param PNR
     *            pnr编码
     * @param pid
     *            政策匹配好的政策编号
     * @param userRtInfo
     *            PNR的RT信息
     * @return orderId|OfficeNo
     * @throws RemoteException
     */
    @SuppressWarnings("static-access")
    public String createEeightOrderNew40(String PNR, String pid, String userRtInfo) throws RemoteException {
        PNR = PNR.trim();
        WriteLog.write("CreateOrder8000yi", "createOrder:pid:" + pid + "--PNR=" + PNR + ":" + name + ":" + pwd);
        String result = "-1";
        try {
            String orderId = "";
            String OfficeNo = "";
            String CWZongJia = "";
            OrderServiceStub stub = new OrderServiceStub();
            OrderServiceStub.CreatOrderNew_ByPNRNote createOrder = new OrderServiceStub.CreatOrderNew_ByPNRNote();
            createOrder.setName(name);
            createOrder.setPwd(pwd);
            createOrder.setPid(pid);
            createOrder.setPnr(PNR);
            createOrder.setUserRtInfo(" " + userRtInfo);
            WriteLog.write("CreateOrder8000yi", PNR);
            OrderServiceStub.CreatOrderNew_ByPNRNoteResponse response = stub.creatOrderNew_ByPNRNote(createOrder);
            WriteLog.write("CreateOrder8000yi", response.getCreatOrderNew_ByPNRNoteResult().toString());
            OMElement element = response.getCreatOrderNew_ByPNRNoteResult().getExtraElement();
            Iterator<OMElement> oneiterator = element.getChildElements();
            while (oneiterator.hasNext()) {
                OMElement twoome = oneiterator.next();
                Iterator<OMElement> twoiterator = twoome.getChildElements();
                while (twoiterator.hasNext()) {
                    OMElement threeome = twoiterator.next();
                    Iterator<OMElement> threeiterator = threeome.getChildElements();
                    while (threeiterator.hasNext()) {
                        OMElement rt = threeiterator.next();
                        String name = rt.getLocalName().toUpperCase();
                        String value = rt.getText();
                        if (name.equals("ERRINFO")) {
                            System.out.println(rt.getText());
                            WriteLog.write("CreateOrder8000yi", "根据PNR创建订单失败" + PNR + ":" + rt.getText());
                            return "-1";
                        }
                        if ("ORDERID".equals(name)) {
                            orderId = value;
                        }
                        if ("OFFICENO".equals(name)) {
                            OfficeNo = value;
                        }
                        if ("CWZONGJIA".equals(name)) {
                            CWZongJia = value;
                        }
                    }
                }
            }
            //		if (!CWZongJia.isEmpty()) {
            //			try {
            //				String where = "UPDATE T_ORDERINFO SET "
            //						+ Orderinfo.COL_extorderprice + "=" + CWZongJia
            //						+ " WHERE " + Orderinfo.COL_pnr + "='" + PNR + "'";
            //				writeLog.write("z支付供应", "8000yiCreateOrder:" + where);
            //				Server.getInstance().getSystemService().findMapResultBySql(
            //						where, null);
            //			} catch (Exception e) {
            //				e.printStackTrace();
            //			}
            //		}
            if (OfficeNo != null) {
                if (!CWZongJia.isEmpty()) {
                    // 返回格式：S|订单号|支付url|Office号|支付供应价格
                    result = "S|" + orderId + "|" + getPayUrl(orderId) + "|" + OfficeNo + "|" + CWZongJia;
                }
                else {
                    // 返回格式：S|订单号|支付url|Office号
                    result = "S|" + orderId + "|" + getPayUrl(orderId) + "|" + OfficeNo;
                }
            }
            else {
                result = "S|" + orderId + "|" + getPayUrl(orderId) + "||" + CWZongJia;
            }
        }
        catch (Exception e) {
            WriteLog.write("CreateOrder8000yi", "Exception:" + e.fillInStackTrace());
        }
        WriteLog.write("CreateOrder8000yi", "根据PNR创建订单结果:" + result);
        return result;
    }

    public String getPayUrl(String orderId) {
        String payurl = "";
        try {
            OrderServiceStub stub = new OrderServiceStub();
            OrderServiceStub.PayURL payURL = new OrderServiceStub.PayURL();
            payURL.setName(name);
            payURL.setPwd(pwd);
            payURL.setOrderGuid(orderId);
            OrderServiceStub.PayURLResponse response = stub.payURL(payURL);
            payurl = response.getPayURLResult();
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return payurl;
    }

    /**
     * 8000yi自动代扣
     */
    public String AutomatismPay(String extorderid) {
        String result = "-1";
        try {
            OrderServiceStub stub = new OrderServiceStub();
            OrderServiceStub.AutomatismPay automatismPay = new OrderServiceStub.AutomatismPay();
            automatismPay.setName(name);
            automatismPay.setOrderguid(extorderid);
            automatismPay.setPayAccount(baQianYiBook.getPayAccount());
            automatismPay.setPwd(pwd);
            WriteLog.write("自动代扣", "8000yi:0:name:" + name + ",extorderid:" + extorderid + ",payAccount:"
                    + baQianYiBook.getPayAccount());
            OrderServiceStub.AutomatismPayResponse res = stub.automatismPay(automatismPay);
            OMElement element = res.getAutomatismPayResult().getExtraElement();
            WriteLog.write("自动代扣", "8000yi:1:" + element.getText());
            Iterator<OMElement> oneiterator = element.getChildElements();
            String tempResult = "";
            while (oneiterator.hasNext()) {
                OMElement twoome = oneiterator.next();
                WriteLog.write("自动代扣", "8000yi:1.1:" + twoome.getLocalName() + ":" + twoome.getText());
                Iterator<OMElement> twoiterator = twoome.getChildElements();
                String name = twoome.getLocalName().toUpperCase();
                String value = twoome.getText();
                if ("OUT_ORDER_NO".equals(name)) {
                    tempResult += ",OUT_ORDER_NO:" + value;
                }
                if ("PAY_DATE".equals(name)) {
                    tempResult += ",PAY_DATE:" + value;
                }
                if ("STATUS".equals(name)) {
                    tempResult += ",STATUS:" + value;
                    if ("TRADE_SUCCESS".equals(value)) {
                        result = "S";
                    }
                }
                if ("TRADE_NO".equals(name)) {
                    tempResult += ",TRADE_NO:" + value;
                }
                if ("IS_SUCCESS".equals(name)) {
                    tempResult += ",IS_SUCCESS:" + value;

                }
                if ("ERROR".equals(name)) {
                    tempResult += ",ERROR:" + value;
                }
            }
            WriteLog.write("自动代扣", "8000yi:2:" + tempResult);
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("OUT_ORDER_NO".equals("out_order_no"));
    }
}
