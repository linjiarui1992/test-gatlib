package com.ccservice.ctripoffsts.crack;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.alibaba.fastjson.JSONArray;
import com.ccservice.crack.ctrippffsts.TrainCtripOfflineUtil;
import com.ccservice.offlineExpress.util.CommonUtil;

/**
 * @className: com.ccservice.component.BusSXLWSP.BusSXLWSPUtil
 * @description: TODO - 自测用户登录程序
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年6月19日 下午3:01:15 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainCtripOfflineFetchOrderList {
    private TrainCtripOfflineUtil trainCtripOfflineUtil = new TrainCtripOfflineUtil();

    private static final int random = CommonUtil.randomNum();

    private static final String logName = "线下火车票_携程点击获取订单_ticket_inter";

    public JSONArray fetchOrderList() throws Exception {
        CloseableHttpClient defaultClient = HttpClients.createDefault();
        return trainCtripOfflineUtil.fetchOrderList(logName, random, defaultClient, "TJZ01");
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        System.out.println(new TrainCtripOfflineFetchOrderList().fetchOrderList());

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
