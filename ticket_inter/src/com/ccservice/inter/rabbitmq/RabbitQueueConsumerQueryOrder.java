package com.ccservice.inter.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;




import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.JobQueryMyOrder;
import com.ccservice.rabbitmq.util.QueueConsumer;
import com.ccservice.train.mqlistener.MQMethod;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * 
 * @author zlx
 * @time 2016年11月18日 上午9:50:13
 * @Description TODO
 * 美团云审核队列消费者
 */
public class RabbitQueueConsumerQueryOrder extends QueueConsumer implements Runnable,Consumer{

    private long orderid;

    private long successtime;

    private int queryno;

    public RabbitQueueConsumerQueryOrder(String endPointName) throws IOException {
        super(endPointName);
    }

    public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body) throws IOException {
        try {
            Map<?, ?> map = (HashMap<?, ?>) SerializationUtils.deserialize(body);
            String orderNoticeResult = map.get("message number").toString();
            System.out.println("审核队列  接收 ：————————>"+map.get("message number").toString());
            WriteLog.write("Rabbit_QueueMQ_QueryOrder", "orderNoticeResult:" + orderNoticeResult);
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
            long intotime = jsonobject.containsKey("intotime") ? jsonobject.getLongValue("intotime") : 0;
            this.successtime = jsonobject.containsKey("successtime") ? jsonobject.getLongValue("successtime") : 0;
            this.queryno = jsonobject.containsKey("queryno") ? jsonobject.getIntValue("queryno") : 0;
            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
            WriteLog.write("Rabbit_QueueMQ_QueryOrder", "orderid:" + orderid + "type:" + type);
            if (MQMethod.QUERY == type) {
                //系统配置的审核间隔时间
                new JobQueryMyOrder().gotoQuery(this.orderid, this.successtime, this.queryno);
            }
        }
        catch (Exception e) {
        }
    }

}
