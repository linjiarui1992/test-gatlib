
package com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlightInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlightInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arrCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arrDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flightNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planeModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seatClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seatDiscount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightInfo", propOrder = {
    "arrCode",
    "arrDate",
    "depCode",
    "depDate",
    "flightNo",
    "param1",
    "planeModel",
    "seatClass",
    "seatDiscount"
})
public class FlightInfo {

    @XmlElementRef(name = "arrCode", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> arrCode;
    @XmlElementRef(name = "arrDate", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> arrDate;
    @XmlElementRef(name = "depCode", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> depCode;
    @XmlElementRef(name = "depDate", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> depDate;
    @XmlElementRef(name = "flightNo", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> flightNo;
    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "planeModel", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> planeModel;
    @XmlElementRef(name = "seatClass", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> seatClass;
    protected Double seatDiscount;

    /**
     * Gets the value of the arrCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrCode() {
        return arrCode;
    }

    /**
     * Sets the value of the arrCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrCode(JAXBElement<String> value) {
        this.arrCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the arrDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArrDate() {
        return arrDate;
    }

    /**
     * Sets the value of the arrDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArrDate(JAXBElement<String> value) {
        this.arrDate = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the depCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepCode() {
        return depCode;
    }

    /**
     * Sets the value of the depCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepCode(JAXBElement<String> value) {
        this.depCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the depDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepDate() {
        return depDate;
    }

    /**
     * Sets the value of the depDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepDate(JAXBElement<String> value) {
        this.depDate = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the flightNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFlightNo() {
        return flightNo;
    }

    /**
     * Sets the value of the flightNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFlightNo(JAXBElement<String> value) {
        this.flightNo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the planeModel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlaneModel() {
        return planeModel;
    }

    /**
     * Sets the value of the planeModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlaneModel(JAXBElement<String> value) {
        this.planeModel = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the seatClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeatClass() {
        return seatClass;
    }

    /**
     * Sets the value of the seatClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeatClass(JAXBElement<String> value) {
        this.seatClass = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the seatDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSeatDiscount() {
        return seatDiscount;
    }

    /**
     * Sets the value of the seatDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSeatDiscount(Double value) {
        this.seatDiscount = value;
    }

}
