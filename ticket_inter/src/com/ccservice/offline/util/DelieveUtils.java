package com.ccservice.offline.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataRow;
import com.ccervice.util.db.DataTable;
import com.ccservice.Util.time.TimeUtil;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

public class DelieveUtils {

    //直接反馈到达日期
    public static String getDelieveStr2(String agengId, String address) {
        String results = "";
        String fromcode = "010";
        //        WriteLog.write("线下火车票_获取快递时效", "地址" + address);
        if (address.contains("襄樊市")) {
            address = address.replace("襄樊市", "襄阳市");
        }
        JSONObject codeJson = getExpressCodes(address);
        // 地址信息在顺丰code表找不到
        if (codeJson.getInteger("flg") == 4) {
            //results = "该地址信息错误，请联系客户.";
            results = "0";
            return results;
        }
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        // 根据采购商ID获取代购点CODE和发快递时间
        String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes with(nolock) where agentId=" + agengId;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            fromcode = map.get("fromcode").toString();
            time1 = map.get("time1").toString();
            time2 = map.get("time2").toString();
        }
        // 获取真实的发快递时间
        String realTime = getRealTimes(dates, time1, time2);
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + codeJson.getString("code");
        WriteLog.write("线下火车票_获取快递时效",
                "agengId=" + agengId + "------->" + "address=" + address + "---------->" + urlString + "?" + param);
        // 根据寄件地址，发快递时间，收件地址，获取真实的到达时间。
        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
        try {
            // 返回数据XML化
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if (result.contains("business_type_desc")) {
                Element deliverTmResponse = body.element("DeliverTmResponse");
                Element deliverTm = deliverTmResponse.element("DeliverTm");

                //String business_type_desc = deliverTm.attributeValue("business_type_desc");
                // 真实时间区间
                String deliver_time = deliverTm.attributeValue("deliver_time");
                //WriteLog.write("线下火车票_获取快递时效", "deliver_time1：" + deliver_time);

                //String business_type = deliverTm.attributeValue("business_type");
                // 地址街道/乡镇信息不明确
                if (codeJson.getInteger("flg") == 3) {
                    results = deliver_time;
                    // 地址街道/乡镇信息不明确
                }
                else if (codeJson.getInteger("flg") == 2) {
                    //results = "警示：该地址街道/乡镇信息不明确，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                    results = deliver_time;
                    // 信息明确，
                }
                else if (codeJson.getInteger("flg") == 1) {
                    // 是否全境配送
                    String isCanMail = codeJson.getString("isCanMail");
                    // 加时
                    String isAddTime = codeJson.getString("isAddTime");
                    // 是否是代理点
                    String isAgent = codeJson.getString("isAgent");
                    // 全境配送
                    if ("1".equals(isCanMail)) {
                        // 收件地区有加时的时候，再返回的送达时间基础上加上加时的时间
                        if (!"null".equals(isAddTime)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], isAddTime);
                            deliver_time += "," + deliver_time;
                        }
                        // 收件地区是代理点的时候，在送达时间上加上0.5天到1天
                        if ("1".equals(isAgent)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], "0.5");
                            deliver_time += "," + getdatebyfront(deliver_time.split(",")[0], "0.5");
                        }
                        //WriteLog.write("线下火车票_获取快递时效", "deliver_time2：" + deliver_time);
                        results = deliver_time;
                        // 不能送达的时候， 直接警示，不显示快递时效
                    }
                    else if ("3".equals(isCanMail)) {
                        //results = "警示：该地区不能送达，请选择其他快递。";
                        results = "1";
                        //return results;
                    }
                    else {
                        //results = "警示：该地区非全境配送，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                        results = deliver_time;
                    }
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                }
                else {
                    results = deliver_time;
                }

                /*results += "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                        + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                WriteLog.write("线下火车票_获取快递时效", "地址" + results);*/
            }
            // 获取快递时间失败
            else {
                //results = "获取快递时间失败！请上官网核验快递送达时间。";
                results = "2";
            }
        }
        catch (DocumentException e) {
            //results = "获取快递时间失败！请上官网核验快递送达时间。";
            results = "2";
            ExceptionTCUtil.handleTCException(e);
        }

        return results;
    }

    //直接反馈到达日期
    public static String getDelieveStrDistribution(String agengId, String address) {
        String results = "";
        String fromcode = "010";
        //        WriteLog.write("线下火车票_获取快递时效", "地址" + address);
        if (address.contains("襄樊市")) {
            address = address.replace("襄樊市", "襄阳市");
        }
        JSONObject codeJson = getExpressCodes(address);
        // 地址信息在顺丰code表找不到
        if (codeJson.getInteger("flg") == 4) {
            //results = "该地址信息错误，请联系客户.";
            results = "0";
            return results;
        }
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        // 根据采购商ID获取代购点CODE和发快递时间
        String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes with(nolock) where agentId=" + agengId;
        //        List list = DBHelpUtil.getSystemServiceOldDB().findMapResultBySql(sql1, null);
        DataTable list = new DataTable();
        try {
            list = DBHelperOffline.GetDataTable(sql1, null);
        }
        catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        if (list.GetRow().size() > 0) {
            //            Map map = (Map) list.get(0);
            DataRow map = list.GetRow().get(0);
            fromcode = map.GetColumnString("fromcode").toString();
            time1 = map.GetColumnString("time1").toString();
            time2 = map.GetColumnString("time2").toString();
        }
        // 获取真实的发快递时间
        String realTime = getRealTimes(dates, time1, time2);
        String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + codeJson.getString("code");
        WriteLog.write("线下火车票_获取快递时效",
                "agengId=" + agengId + "------->" + "address=" + address + "---------->" + urlString + "?" + param);
        // 根据寄件地址，发快递时间，收件地址，获取真实的到达时间。
        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
        try {
            // 返回数据XML化
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if (result.contains("business_type_desc")) {
                Element deliverTmResponse = body.element("DeliverTmResponse");
                Element deliverTm = deliverTmResponse.element("DeliverTm");

                //String business_type_desc = deliverTm.attributeValue("business_type_desc");
                // 真实时间区间
                String deliver_time = deliverTm.attributeValue("deliver_time");
                //WriteLog.write("线下火车票_获取快递时效", "deliver_time1：" + deliver_time);

                //String business_type = deliverTm.attributeValue("business_type");
                // 地址街道/乡镇信息不明确
                if (codeJson.getInteger("flg") == 3) {
                    results = deliver_time;
                    // 地址街道/乡镇信息不明确
                }
                else if (codeJson.getInteger("flg") == 2) {
                    //results = "警示：该地址街道/乡镇信息不明确，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                    results = deliver_time;
                    // 信息明确，
                }
                else if (codeJson.getInteger("flg") == 1) {
                    // 是否全境配送
                    String isCanMail = codeJson.getString("isCanMail");
                    // 加时
                    String isAddTime = codeJson.getString("isAddTime");
                    // 是否是代理点
                    String isAgent = codeJson.getString("isAgent");
                    // 全境配送
                    if ("1".equals(isCanMail)) {
                        // 收件地区有加时的时候，再返回的送达时间基础上加上加时的时间
                        if (!"null".equals(isAddTime)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], isAddTime);
                            deliver_time += "," + deliver_time;
                        }
                        // 收件地区是代理点的时候，在送达时间上加上0.5天到1天
                        if ("1".equals(isAgent)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], "0.5");
                            deliver_time += "," + getdatebyfront(deliver_time.split(",")[0], "0.5");
                        }
                        //WriteLog.write("线下火车票_获取快递时效", "deliver_time2：" + deliver_time);
                        results = deliver_time;
                        // 不能送达的时候， 直接警示，不显示快递时效
                    }
                    else if ("3".equals(isCanMail)) {
                        //results = "警示：该地区不能送达，请选择其他快递。";
                        results = "0";
                        //return results;
                    }
                    else {
                        //results = "警示：该地区非全境配送，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                        results = deliver_time;
                    }
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                }
                else {
                    results = deliver_time;
                }

                /*results += "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                        + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                WriteLog.write("线下火车票_获取快递时效", "地址" + results);*/
            }
            // 获取快递时间失败
            else {
                //results = "获取快递时间失败！请上官网核验快递送达时间。";
                results = "1";
            }
        }
        catch (DocumentException e) {
            //results = "获取快递时间失败！请上官网核验快递送达时间。";
            results = "2";
        }

        return results;
    }

    public static JSONObject getDelieveStr2GT(String agengId, String address) {
        String strResult = "暂无快递信息!";

        String results = "";
        String fromcode = "010";
        //        WriteLog.write("线下火车票_获取快递时效", "地址" + address);
        if (address.contains("襄樊市")) {
            address = address.replace("襄樊市", "襄阳市");
        }
        JSONObject codeJson = getExpressCodes(address);
        // 地址信息在顺丰code表找不到
        if (codeJson.getInteger("flg") == 4) {
            //results = "该地址信息错误，请联系客户.";
            results = "1";
        }
        String time1 = "10:00:00";
        String time2 = "18:00:00";

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String dates = sdf.format(new Date());
        // 根据采购商ID获取代购点CODE和发快递时间
        String sql1 = "SELECT fromcode,time1,time2 from TrainOrderAgentTimes with(nolock) where agentId=" + agengId;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            fromcode = map.get("fromcode").toString();
            time1 = map.get("time1").toString();
            time2 = map.get("time2").toString();
        }

        // 获取真实的发快递时间
        String realTime = getRealTimes(dates, time1, time2);

        String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
        String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + codeJson.getString("code");
        WriteLog.write("线下火车票_获取快递时效",
                "agengId=" + agengId + "------->" + "address=" + address + "---------->" + urlString + "?" + param);
        // 根据寄件地址，发快递时间，收件地址，获取真实的到达时间。
        String result = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();

        String totime = "";

        try {
            // 返回数据XML化
            Document document = DocumentHelper.parseText(result);
            Element root = document.getRootElement();
            Element head = root.element("Head");
            Element body = root.element("Body");
            if (result.contains("business_type_desc")) {
                Element deliverTmResponse = body.element("DeliverTmResponse");
                Element deliverTm = deliverTmResponse.element("DeliverTm");

                //String business_type_desc = deliverTm.attributeValue("business_type_desc");
                // 真实时间区间
                String deliver_time = deliverTm.attributeValue("deliver_time");

                deliver_time = deliver_time.substring(0, deliver_time.indexOf(","));//2017-09-07 18:00:00

                //WriteLog.write("线下火车票_获取快递时效", "deliver_time1：" + deliver_time);

                //String business_type = deliverTm.attributeValue("business_type");
                // 地址街道/乡镇信息不明确
                if (codeJson.getInteger("flg") == 3) {

                    //results = "配送费用￥20," + realTime + "前完成支付," + deliver_time.split(",")[0] + "前送到！";
                    strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";

                    results = deliver_time;
                    // 地址街道/乡镇信息不明确
                }
                else if (codeJson.getInteger("flg") == 2) {
                    //results = "警示：该地址街道/乡镇信息不明确，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                    strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";

                    results = deliver_time;
                    // 信息明确，
                }
                else if (codeJson.getInteger("flg") == 1) {
                    // 是否全境配送
                    String isCanMail = codeJson.getString("isCanMail");
                    // 加时
                    String isAddTime = codeJson.getString("isAddTime");
                    // 是否是代理点
                    String isAgent = codeJson.getString("isAgent");
                    // 全境配送
                    if ("1".equals(isCanMail)) {
                        // 收件地区有加时的时候，再返回的送达时间基础上加上加时的时间
                        if (!"null".equals(isAddTime)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], isAddTime);
                            deliver_time += "," + deliver_time;
                        }
                        // 收件地区是代理点的时候，在送达时间上加上0.5天到1天
                        if ("1".equals(isAgent)) {
                            deliver_time = getdatebyfront(deliver_time.split(",")[0], "0.5");
                            deliver_time += "," + getdatebyfront(deliver_time.split(",")[0], "0.5");
                        }
                        //WriteLog.write("线下火车票_获取快递时效", "deliver_time2：" + deliver_time);
                        strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";

                        results = deliver_time;
                        // 不能送达的时候， 直接警示，不显示快递时效
                    }
                    else if ("3".equals(isCanMail)) {
                        //results = "警示：该地区不能送达，请选择其他快递。";
                        results = "0";
                        //return results;
                    }
                    else {
                        //results = "警示：该地区非全境配送，有未能送到的情况， 请上顺丰官网核验快递送达时间。下面提示仅供参考。<br/>";
                        strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";

                        results = deliver_time;
                    }
                    //WriteLog.write("线下火车票_获取快递时效", "地址" + results);
                }
                else {
                    strResult = "配送费用￥20," + realTime + "前完成支付," + deliver_time + "前送到！";

                    results = deliver_time;
                }

                /*results += "如果" + realTime + "正常发件。快递类型为:" + business_type_desc + "。快递预计到达时间:" + deliver_time
                        + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
                WriteLog.write("线下火车票_获取快递时效", "地址" + results);*/
            }
            // 获取快递时间失败
            else {
                //results = "获取快递时间失败！请上官网核验快递送达时间。";
                results = "1";
            }
        }
        catch (DocumentException e) {
            //results = "获取快递时间失败！请上官网核验快递送达时间。";
            results = "2";
            ExceptionTCUtil.handleTCException(e);
        }

        totime = results;

        JSONObject jbb = new JSONObject();

        if (strResult.equals("暂无快递信息!")) {
            strResult = strResult + "-" + realTime;
        }

        jbb.put("results", strResult);
        jbb.put("realTime", realTime);
        jbb.put("totime", totime);

        //记录请求信息日志
        WriteLog.write("线下票获取快递时效", "getDelieveStr2GT--->results" + jbb.toJSONString());

        return jbb;
    }

    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address
     * @return
     */
    private static JSONObject getExpressCodes(String address) {
        JSONObject jsonObject = new JSONObject();
        // 根据邮寄地址，返回对应的信息。
        String procedure = "sp_TrainOfflineExpress_getCode2 @address='" + address + "'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        WriteLog.write("线下火车票_获取快递时效", "获取地址件数" + list.size());
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                // 乡镇
                String town = map.get("town").toString();
                WriteLog.write("线下火车票_获取快递时效", "顺丰数据库乡镇街道：" + town);
                // 邮寄地址中有乡镇信息， 可以查到的时候，直接返回信息，设置对应的code，isCanMail，isAddTime，isAgent，flg。
                if (!"".equals(town) && address.contains(town)) {
                    jsonObject.put("code", String.valueOf(map.get("code")));
                    jsonObject.put("isCanMail", String.valueOf(map.get("isCanMail")));
                    jsonObject.put("isAddTime", String.valueOf(map.get("isAddTime")));
                    jsonObject.put("isAgent", String.valueOf(map.get("isAgent")));
                    jsonObject.put("flg", 1);
                    break;
                }
            }
            WriteLog.write("线下火车票_获取快递时效", "findFlg：" + jsonObject.getString("flg"));
            // 没有乡镇信息的时候，
            if (!"1".equals(jsonObject.getString("flg"))) {
                for (int i = 0; i < list.size(); i++) {
                    Map map = (Map) list.get(i);
                    WriteLog.write("线下火车票_获取快递时效", "" + map);
                    String isCanMail = String.valueOf(map.get("isCanMail"));
                    String isAddTime = String.valueOf(map.get("isAddTime"));
                    String isAgent = String.valueOf(map.get("isAgent"));
                    WriteLog.write("线下火车票_获取快递时效",
                            "isCanMail：" + isCanMail + " isAddTime：" + isAddTime + " isAgent：" + isAgent);
                    // 如果该地址的为全区配送， 没有加时， 没有代理点。则按正常的逻辑处理。 没有警示信息。
                    if ("1".equals(isCanMail) && "null".equals(isAddTime) && "2".equals(isAgent)) {
                        jsonObject.put("code", map.get("code").toString());
                        jsonObject.put("flg", 0);
                        continue;
                        // 以外的时候，添加警示信息。
                    }
                    else {
                        jsonObject.put("code", map.get("code").toString());
                        jsonObject.put("flg", 2);
                        break;
                    }
                }
            }
        }
        else {
            // 根据邮寄地址，二次查询市code
            String procedure1 = "sp_TrainOfflineExpress_getCode3 @address='" + address + "'";
            List list1 = Server.getInstance().getSystemService().findMapResultByProcedure(procedure1);
            WriteLog.write("线下火车票_获取快递时效", "获取地址件数" + list.size());
            if (list1.size() > 0) {
                Map map = (Map) list1.get(0);
                jsonObject.put("code", map.get("code").toString());
                jsonObject.put("flg", 3);
            }
            else {
                jsonObject.put("flg", 4);
            }
        }
        WriteLog.write("线下火车票_获取快递时效", "返回数据" + jsonObject);
        return jsonObject;
    }

    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    private static String getRealTimes(String dates, String time1, String time2) {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates = sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if (date0.before(date1)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date1));
            }
            else if (date0.after(date1) && date0.before(date2)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date2));
            }
            else if (date0.after(date2)) {
                Date ds = getDate(new Date());
                String nextd = sdf1.format(ds);
                result = (nextd.substring(0, 10) + " " + sdf.format(date1));
            }
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException("线下火车票_获取快递时效_平台进单_异常", e);
        }
        return result;
    }

    private static String getdatebyfront(String date, String addTime) {

        if (addTime.contains(".5")) {
            int day = Double.valueOf(addTime).intValue() + 1;
            date = getdatebyfront(date, day);
            if ("18".equals(date.substring(11, 13))) {
                date = date.replace(date.substring(11, 13), "12");
            }
            else {
                date = date.replace(date.substring(11, 13), "18");
            }

        }
        else {
            int day = Double.valueOf(addTime).intValue();
            date = getdatebyfront(date, day);
        }

        return date;
    }

    private static String getdatebyfront(String date, int addTime) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date nowDate = null;
        try {
            nowDate = simpleDateFormat.parse(date);
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException("线下火车票_获取快递时效_平台进单_异常", e);
        }
        Calendar now = Calendar.getInstance();
        now.setTime(nowDate);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + addTime);
        nowDate = now.getTime();
        return simpleDateFormat.format(nowDate);
    }

    /**
     * 获取顺丰快递时效。
     * 
     * @param agengId 代售点ID
     * @param address 邮寄地址
     * @return
     * @time 2017年11月10日 下午4:08:29
     * @author liujun
     */
    public static String getDelieveStr(String agengId, String address) {
        //随机
        int random = new Random().nextInt(9000000) + 1000000;
        String delieveStr = "";
        String logName = "线下火车票_顺丰获取快递时效_平台进单";
        JSONObject jsonObject = getDelieveStr("0", logName, random, agengId, address);
        int resultCode = jsonObject.getInteger("resultCode"); // 顺丰地址Code类型
        String deliverTime = deliverTimeFormat(logName, random, jsonObject.getString("deliverTime"));// 顺丰时效
        // 0：乡镇不明，全区无加时，无代理/1：正常/2：街道/乡镇信息不明确/3：市辖区信息不明确/4：地址错误/5：顺丰不能送达/6：顺丰非全境
        if (resultCode == 2) {
            delieveStr = "警示：该地址街道/乡镇信息不明确，有未能送到的情况， 请上顺丰官网查询快递送达时间。下面提示仅供参考。<br/>";
        }
        else if (resultCode == 3) {
            delieveStr = "警示：该地址市辖区信息不明确，请上顺丰官网查询快递送达时间。下面提示为到达该市时间，仅供参考。<br/>";
        }
        else if (resultCode == 4) {
            delieveStr = "该地址信息错误，请联系客户.";
        }
        else if (resultCode == 5) {
            delieveStr = "警示：该地区不能送达，请选择其他快递。";
        }
        else if (resultCode == 6) {
            delieveStr = "警示：该街道/乡镇非全境配送，有未能送到的情况， 请上顺丰官网查询快递配送范围。下面提示仅供参考。<br/>";
        }
        if (resultCode == 0 || resultCode == 1 || resultCode == 2 || resultCode == 3 || resultCode == 6) {
            String realTime = jsonObject.getString("realTime");
            String businessType = jsonObject.getString("businessType");
            delieveStr += "【顺丰快递】<br/>如果" + realTime + "正常发件。快递类型为:" + businessType + "。快递预计到达时间:" + deliverTime
                    + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
        }
        if (deliverTime != null && !"".equals(deliverTime)) {
            deliverTime = deliverTime.split(",")[0];
        }
        return delieveStr;
    }

    /**
     * 获取顺丰Ems快递时效
     * 
     * @param agengId 代售点ID
     * @param address 邮寄地址
     * @return
     * @time 2017年11月10日 上午11:30:09
     * @author liujun
     */
    public static String getEmsDelieveStr(String agengId, String address) {
        String delieveStr = "";
        String logName = "线下火车票_EMS获取快递时效_平台进单";
        //随机
        int random = new Random().nextInt(9000000) + 1000000;
        JSONObject jsonObject = getDelieveStr("1", logName, random, agengId, address);
        int resultCode = jsonObject.getInteger("resultCode"); // 顺丰地址Code类型
        String deliverTime = jsonObject.getString("deliverTime");// 顺丰时效
        // 4：地址错误
        if (resultCode != 4) {
            int type = jsonObject.getInteger("type");//判断地址区间---同城：1/同省：2/跨省：3/偏远跨省：4
            // 同城
            if (type == 1) {
                deliverTime = addDay(logName, random, deliverTime, "0.5");
                WriteLog.write(logName, random + "EMS同城加时0.5天：" + deliverTime);
            }
            // 同省，或者北京，天津，上海
            if (type == 2) {
                deliverTime = addDay(logName, random, deliverTime, "1");
                WriteLog.write(logName, random + "EMS同省加时1天：" + deliverTime);
            }
            // 跨省+1.5天
            if (type == 3) {
                deliverTime = addDay(logName, random, deliverTime, "1.5");
                WriteLog.write(logName, random + "EMS跨省加时1.5天：" + deliverTime);
            }
            // 偏远省跨省+2天
            if (type == 4) {
                deliverTime = addDay(logName, random, deliverTime, "3");
                WriteLog.write(logName, random + "EMS偏远跨省加时3天：" + deliverTime);
            }
            deliverTime = deliverTimeFormat(logName, random, deliverTime);
            String realTime = jsonObject.getString("realTime");
            String businessType = jsonObject.getString("businessType");
            delieveStr += "【EMS快递】<br/>如果" + realTime + "正常发件。快递类型为:" + businessType + "。快递预计到达时间:" + deliverTime
                    + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
        }
        else {
            delieveStr = "该地址信息错误，请联系客户.";
        }
        /*if (deliverTime != null && !"".equals(deliverTime)) {
            deliverTime = deliverTime.split(",")[0];
        }*/
        return delieveStr;
    }

    public static String getEmsDelieveStr2(String agengId, String address) {
        String deliverTime = "";
        String logName = "线下火车票_EMS获取快递时效_平台进单";
        //随机
        int random = new Random().nextInt(9000000) + 1000000;
        JSONObject jsonObject = getDelieveStr("1", logName, random, agengId, address);
        int resultCode = jsonObject.getInteger("resultCode"); // 顺丰地址Code类型
        deliverTime = jsonObject.getString("deliverTime");// 顺丰时效
        // 4：地址错误
        if (resultCode != 4) {
            int type = jsonObject.getInteger("type");//判断地址区间---同城：1/同省：2/跨省：3/偏远跨省：4
            // 同城
            if (type == 1) {
                deliverTime = addDay(logName, random, deliverTime, "0.5");
                WriteLog.write(logName, random + "EMS同城加时0.5天：" + deliverTime);
            }
            // 同省，或者北京，天津，上海
            if (type == 2) {
                deliverTime = addDay(logName, random, deliverTime, "1");
                WriteLog.write(logName, random + "EMS同省加时1天：" + deliverTime);
            }
            // 跨省+1.5天
            if (type == 3) {
                deliverTime = addDay(logName, random, deliverTime, "1.5");
                WriteLog.write(logName, random + "EMS跨省加时1.5天：" + deliverTime);
            }
            // 偏远省跨省+2天
            if (type == 4) {
                deliverTime = addDay(logName, random, deliverTime, "2");
                WriteLog.write(logName, random + "EMS偏远跨省加时2天：" + deliverTime);
            }
            deliverTime = deliverTimeFormat(logName, random, deliverTime);
            /*String realTime = jsonObject.getString("realTime");
            String businessType = jsonObject.getString("businessType");
            delieveStr += "如果" + realTime + "正常发件。快递类型为:" + businessType + "。快递预计到达时间:" + deliverTime
                    + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";*/
        }
        else {
            //delieveStr = "该地址信息错误，请联系客户.";
            deliverTime = "0";
        }

        return deliverTime;
    }

    /**
     * 获取UU跑腿快递时效
     * 
     * @param agengId 代售点ID
     * @param address 邮寄地址
     * @return
     * @time 2017年11月10日 上午11:31:06
     * @author liujun
     */
    public static String getUUptDelieveStr(String agentId, String address) {
        String delieveStr = "";
        String deliverTime = "";
        String logName = "线下火车票_UU跑腿获取快递时效_平台进单";
        //随机
        int random = new Random().nextInt(9000000) + 1000000;
        // 当前日期
        String isOnlineDate = TimeUtil.gettodaydate(1);
        String sql = "SELECT isOnline FROM T_AGENTISONLINETIME with(nolock) WHERE agentId = " + agentId
                + " AND isOnlineDate = '" + isOnlineDate + "'";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        WriteLog.write(logName, "代售点上班信息：" + list);
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            String isOnline = String.valueOf(map.get("isOnline"));
            String realTime = TimeUtil.gettodaydate(4);
            if (isOnline != null && !"".equals(isOnline)) {
                // 代售点为上班状态时，反馈的预计到达时间为当前下单时间+3小时
                if ("1".equals(isOnline)) {
                    deliverTime = addHour(logName, random, realTime + "," + realTime, 3);
                }
                // 当代售点为非上班状态时，反馈的预计到达时间为当前下单时间的第二天8点+3小时
                else {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd");
                    Calendar date = Calendar.getInstance();
                    date.set(Calendar.DATE, date.get(Calendar.DATE) + 1);
                    realTime = sdf.format(date.getTime()) + " 08:00:00";
                    deliverTime = sdf.format(date.getTime()) + " 11:00:00";
                    deliverTime += "," + deliverTime;
                }
            }
            WriteLog.write(logName, "deliverTime：" + deliverTime + "realTime：" + realTime);
            delieveStr += "【UU跑腿】<br/>如果" + realTime + "正常发件。快递预计到达时间:" + deliverTime
                    + "。以上时效为预计到达时间，仅供参考，精准时效以运单追踪结果中的“预计到达时间”为准。";
        }
        else {
            delieveStr = "获取快递时效失败";
        }
        /*if (deliverTime != null && !"".equals(deliverTime)) {
            deliverTime = deliverTime.split(",")[0];
        }*/
        return delieveStr;
    }

    /**
     * 获取快递时效
     * 
     * @param agengId 代售点ID、
     * @param address 邮寄地址
     * @return resultCode 0：获取时效失败/1：正常/2：街道/乡镇信息不明确/3：市辖区信息不明确/4：地址错误/5：顺丰不能送达/6：顺丰非全境
     * @time 2017年10月10日 下午5:02:00
     * @author liujun
     */
    private static JSONObject getDelieveStr(String expressAgent, String logName, int random, String agengId,
            String address) {
        JSONObject result = new JSONObject();
        WriteLog.write(logName, random + "代售点ID：" + agengId + "--地址：" + address);
        JSONObject codeJson = getExpressCode(logName, random, address);
        int resultCode = codeJson.getInteger("flg");// 顺丰Code区分
        int type = 0; // 发件收件地址区间区分
        String deliverTime = ""; //时效时间区间
        String realTime = ""; // 获取真实的发快递时间
        String businessType = ""; // 快递类型
        String fromcode = "010";
        // 地址信息在顺丰code表可以查到
        if (codeJson.getInteger("flg") != 4) {
            String time1 = "";
            String time2 = "";
            // 根据采购商ID获取代购点CODE和发快递时间
            String sql1 = "SELECT fromcode,time1,time2,time3,time4 from TrainOrderAgentTimes with(nolock) where agentId="
                    + agengId;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql1, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                fromcode = map.get("fromcode").toString();
                if ("0".equals(expressAgent)) {
                    time1 = map.get("time1").toString();
                    time2 = map.get("time2").toString();
                }
                else if ("1".equals(expressAgent)) {
                    time1 = map.get("time3").toString();
                    time2 = map.get("time4").toString();
                }
            }
            else {
                if ("0".equals(expressAgent)) {
                    time1 = "10:00:00";
                    time2 = "18:00:00";
                }
                else if ("1".equals(expressAgent)) {
                    time1 = "11:00:00";
                    time2 = "17:00:00";
                }
            }
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String dates = sdf.format(new Date());
            // 获取真实的发快递时间
            realTime = getRealTimes(logName, random, dates, time1, time2);
            String urlString = PropertyUtil.getValue("expressDeliverUrl", "train.properties");
            String tocode = codeJson.getString("code");
            String param = "times=" + realTime + "&fromcode=" + fromcode + "&tocode=" + tocode;
            WriteLog.write(logName, random + urlString + "?" + param);
            // 根据寄件地址，发快递时间，收件地址，获取真实的到达时间。
            String reqResult = SendPostandGet.submitPost(urlString, param, "UTF-8").toString();
            try {
                // 返回数据XML化
                Document document = DocumentHelper.parseText(reqResult);
                Element root = document.getRootElement();
                Element head = root.element("Head");
                Element body = root.element("Body");
                if (reqResult.contains("business_type_desc")) {
                    Element deliverTmResponse = body.element("DeliverTmResponse");
                    Element deliverTm = deliverTmResponse.element("DeliverTm");
                    businessType = deliverTm.attributeValue("business_type_desc");
                    // 真实时间区间
                    deliverTime = deliverTm.attributeValue("deliver_time");
                    WriteLog.write(logName, random + "顺丰API获取时间：" + deliverTime);
                    // 同城：1/同省：2/跨省：3/偏远跨省：4
                    // 根据城市code获取城市信息
                    String sqlfromCity = "SELECT top 1 province,city from SFExpressCityCode with(nolock) WHERE code ='"
                            + fromcode + "'";
                    List listfromCity = Server.getInstance().getSystemService().findMapResultBySql(sqlfromCity, null);
                    // 根据城市code获取城市信息
                    String sqltoCity = "SELECT top 1 province,city from SFExpressCityCode with(nolock) WHERE code ='"
                            + tocode + "'";
                    List listtoCity = Server.getInstance().getSystemService().findMapResultBySql(sqltoCity, null);
                    if (listfromCity.size() > 0 && listtoCity.size() > 0) {
                        Map formCityMap = (Map) listfromCity.get(0);
                        Map toCityMap = (Map) listtoCity.get(0);
                        String fromProvince = String.valueOf(formCityMap.get("province"));// 发件省份
                        String toProvince = String.valueOf(toCityMap.get("province"));//收件省份
                        String fromCity = String.valueOf(formCityMap.get("city"));// 发件城市
                        String toCity = String.valueOf(toCityMap.get("city"));//收件城市
                        WriteLog.write(logName, random + "发件省份：" + fromProvince + "--发件城市：" + fromCity + "--收件省份："
                                + toProvince + "--收件城市：" + toCity);
                        // 如果不同城
                        if (fromCity.equals(toCity)) {
                            type = 1;
                        }
                        else {
                            // 同省，或者北京，天津，上海
                            if (fromProvince.equals(toProvince) || "北京市".equals(toCity) || "天津市".equals(toCity)
                                    || "上海市".equals(toCity)) {
                                type = 2;
                            }
                            else {
                                // 跨省
                                String trans_provincial = PropertyUtil.getValue("trans_provincial", "train.properties");
                                if (trans_provincial.contains(toProvince)) {
                                    type = 3;
                                }
                                // 偏远省跨省
                                String remote_trans_provincial = PropertyUtil.getValue("remote_trans_provincial",
                                        "train.properties");
                                if (remote_trans_provincial.contains(toProvince)) {
                                    type = 4;
                                }
                            }

                        }
                    }
                    // 信息明确
                    if (codeJson.getInteger("flg") == 1) {
                        // 是否全境配送
                        String isCanMail = codeJson.getString("isCanMail");
                        // 加时
                        String isAddTime = codeJson.getString("isAddTime");
                        // 是否是代理点
                        String isAgent = codeJson.getString("isAgent");
                        // 全境配送
                        if ("1".equals(isCanMail)) {
                            // 收件地区有加时的时候，再返回的送达时间基础上加上加时的时间
                            if (isAddTime != null && !"null".equals(isAddTime)) {
                                deliverTime = addDay(logName, random, deliverTime, isAddTime);
                            }
                            // 收件地区是代理点的时候，在送达时间上加上1天
                            if ("1".equals(isAgent)) {
                                deliverTime = addDay(logName, random, deliverTime, "1");
                            }
                            WriteLog.write(logName, "顺丰数据库加时时间：" + deliverTime);
                            // 不能送达的时候， 直接警示，不显示快递时效
                        }
                        else if ("3".equals(isCanMail)) {
                            resultCode = 5;
                        }
                        else {
                            resultCode = 6;
                        }
                    }
                    // 乡镇信息不明，进行加时。
                    else if (codeJson.getInteger("flg") == 2 || codeJson.getInteger("flg") == 3) {
                        // 同城加0.5天
                        if (type == 1) {
                            deliverTime = addDay(logName, random, deliverTime, "0.5");
                            WriteLog.write(logName, random + "同城加时0.5天：" + deliverTime);
                        }
                    }
                }
            }
            catch (DocumentException e) {
                ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
            }
        }
        result.put("type", type);
        result.put("deliverTime", deliverTime);
        result.put("realTime", realTime);
        result.put("businessType", businessType);
        result.put("resultCode", resultCode);
        return result;
    }

    /**
     * 通过存储过程获取乘客地址的citycode
     * @param address 详细地址
     * @return
     */
    public static JSONObject getExpressCode(String logName, int random, String address) {
        JSONObject jsonObject = new JSONObject();
        // 根据邮寄地址，返回对应的信息。
        String procedure = "sp_TrainOfflineExpress_getCode2 @address='" + address + "'";
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(procedure);
        WriteLog.write(logName, random + "第一次获取地址件数" + list.size());
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                // 乡镇
                String town = map.get("town").toString();
                // 邮寄地址中有乡镇信息， 可以查到的时候，直接返回信息，设置对应的code，isCanMail，isAddTime，isAgent，flg。
                if (!"".equals(town) && address.contains(town)) {
                    WriteLog.write(logName, random + "匹配成功乡镇街道：" + town);
                    jsonObject.put("code", String.valueOf(map.get("code")));
                    jsonObject.put("isCanMail", String.valueOf(map.get("isCanMail")));
                    jsonObject.put("isAddTime", String.valueOf(map.get("isAddTime")));
                    jsonObject.put("isAgent", String.valueOf(map.get("isAgent")));
                    jsonObject.put("flg", 1);
                    break;
                }
            }
            WriteLog.write(logName, random + "findFlg：" + jsonObject.getString("flg"));
            // 没有乡镇信息的时候，
            if (!"1".equals(jsonObject.getString("flg"))) {
                for (int i = 0; i < list.size(); i++) {
                    Map map = (Map) list.get(i);
                    String isCanMail = String.valueOf(map.get("isCanMail"));
                    String isAddTime = String.valueOf(map.get("isAddTime"));
                    String isAgent = String.valueOf(map.get("isAgent"));
                    String town = String.valueOf(map.get("town"));
                    WriteLog.write(logName, random + "town：" + town + "isCanMail：" + isCanMail + " isAddTime："
                            + isAddTime + " isAgent：" + isAgent);
                    // 如果该地址的为全区配送， 没有加时， 没有代理点。则按正常的逻辑处理。 没有警示信息。
                    if ("1".equals(isCanMail) && "null".equals(isAddTime) && "2".equals(isAgent)) {
                        jsonObject.put("code", map.get("code").toString());
                        jsonObject.put("flg", 0);
                        continue;
                        // 以外的时候，添加警示信息。
                    }
                    else {
                        jsonObject.put("code", map.get("code").toString());
                        jsonObject.put("flg", 2);
                        break;
                    }
                }
            }
        }
        else {
            // 根据邮寄地址，二次查询市code
            String procedure1 = "sp_TrainOfflineExpress_getCode3 @address='" + address + "'";
            List list1 = Server.getInstance().getSystemService().findMapResultByProcedure(procedure1);
            WriteLog.write(logName, random + "第二次获取地址件数" + list.size());
            if (list1.size() > 0) {
                Map map = (Map) list1.get(0);
                jsonObject.put("code", map.get("code").toString());
                jsonObject.put("flg", 3);
            }
            else {
                jsonObject.put("flg", 4);
            }
        }
        WriteLog.write(logName, random + "获取顺丰Code返回数据" + jsonObject);
        return jsonObject;
    }

    /**
     * 获取取快递时间
     * @param dates
     * @param time1
     * @param time2
     * @return
     */
    private static String getRealTimes(String logName, int random, String dates, String time1, String time2) {
        String result = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String realDates = sdf1.format(new Date());
        try {
            Date date0 = sdf.parse(dates);
            Date date1 = sdf.parse(time1);
            Date date2 = sdf.parse(time2);
            if (date0.before(date1)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date1));
            }
            else if (date0.after(date1) && date0.before(date2)) {
                result = (realDates.substring(0, 10) + " " + sdf.format(date2));
            }
            else if (date0.after(date2)) {
                Date ds = getDate(new Date());
                String nextd = sdf1.format(ds);
                result = (nextd.substring(0, 10) + " " + sdf.format(date1));
            }
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        return result;
    }

    private static Date getDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date date1 = new Date(calendar.getTimeInMillis());
        return date1;
    }

    /**
     * 添加天数
     * 
     * @param date
     * @param addDays 添加天数
     * @return
     * @time 2017年11月10日 下午1:44:35
     * @author liujun
     */
    private static String addDay(String logName, int random, String deliverTime, String days) {
        String result = "";
        String dataStr = deliverTime.split(",")[0];
        // 获取加时小时数
        int hour = new BigDecimal(days).multiply(new BigDecimal(24)).intValue();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dataStr);
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        if (date == null) {
            WriteLog.write(logName, random + "加时失败" + dataStr);
            return deliverTime;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);// 24小时制   
        date = cal.getTime();
        // 更新后的日期 
        result = format.format(date);
        result += "," + result;
        return result;
    }

    /**
     * 添加小时
     * 
     * @param date
     * @param addDays 添加天数
     * @return
     * @time 2017年11月10日 下午1:44:35
     * @author liujun
     */
    private static String addHour(String logName, int random, String deliverTime, int hours) {
        String result = "";
        String dataStr = deliverTime.split(",")[0];
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dataStr);
        }
        catch (ParseException e) {
            ExceptionUtil.writelogByException(logName + "_异常", e, String.valueOf(random));
        }
        if (date == null) {
            WriteLog.write(logName, random + "加时失败" + dataStr);
            return deliverTime;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);// 24小时制   
        date = cal.getTime();
        // 更新后的日期 
        result = format.format(date);
        result += "," + result;
        return result;
    }

    /**
     * 省外快递公式，用于快递时效接口没有查到目的地
     * @param totalAddress
     * @param agentid
     * @param LOGNAME
     * @param random
     * @return
     */
    public static String ungetDelieveTime(String totalAddress, String agentid, String LOGNAME, long random) {
        /**
         * 0 - 警示：该地区不能送达，请选择其他快递。
         * 1 - 接口访问异常 - 获取快递时间失败！请上官网核验快递送达时间。
         * 2 - 反馈的结果的XML解析失败 - 获取快递时间失败！请上官网核验快递送达时间。
         * 
         * 以上三种情况需要进行人为判定选择别的快递，还是留待拒单操作
         * 
         * 
         * 目前新增逻辑判断
         * 
         * 顺丰不能送达 - 尝试走EMS【后期出单需人工判定】 - 
         * 
         * 按照48小时进行判定【系统判定】 - 二期 - 尝试走EMS快递时效
         * 
         * 
         * 接口访问异常 - 走公式逻辑
         * 
         * 代售点匹配之后，匹配省内还是省外 - 省内算两天，省外算四天
         * 
         **/
        String arriveTime = "";

        /**
         * 1;//当天上午件
         * 2;//当天下午件
         * 3;//第二天的上午件
         */
        Integer isMailPMOrNextAM = DelieveUtils.getIsMailPMOrNextAM();//是否包含在11点-18点的下午寄件的时间段内，否则就是当天11点或者第二天11点的寄件

        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //arriveTime = "1900-01-01 00:00:00";
        int doubleNum = 4;//省外 + 4天

        if (isMailPMOrNextAM == 1) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 2) {
            c.set(Calendar.HOUR_OF_DAY, 18 + 24 * doubleNum);//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        else if (isMailPMOrNextAM == 3) {
            c.set(Calendar.HOUR_OF_DAY, 11 + 24 * (doubleNum + 1));//
            c.set(Calendar.MINUTE, 0);//
            c.set(Calendar.SECOND, 0);//
            arriveTime = df2.format(new Date(c.getTimeInMillis()));
        }
        return arriveTime;
    }

    public static Integer getIsMailPMOrNextAM() {
        //以11点和18点为寄件时间进行区分 - 这个只以11点为准进行比较
        Integer isMailPMOrNextAM = 3;//第二天的上午件

        Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (hour < 11) {
            isMailPMOrNextAM = 1;//当天上午件
        }
        else if (hour >= 11 && hour < 18) {
            isMailPMOrNextAM = 2;//当天下午件
        }
        /*else {
            isMailPMOrNextAM = 3;//第二天的上午件
        }*/
        return isMailPMOrNextAM;
    }

    /**
     * 格式化时间
     * 
     * @return
     * @time 2017年11月11日 上午9:38:13
     * @author liujun
     */
    private static String deliverTimeFormat(String logName, int random, String deliverTime) {
        if (deliverTime.contains("00:00:00")) {
            deliverTime = addHour(logName, random, deliverTime, -6);
        }
        return deliverTime;
    }
}
