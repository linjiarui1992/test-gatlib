
package com.webservice.kkkk;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Id" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BPnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EBPnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EPnr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://tempuri.org/}OrderStatus"/>
 *         &lt;element name="Purchaser" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PurchaserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Provider" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProviderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Type" type="{http://tempuri.org/}OrderType"/>
 *         &lt;element name="RangeType" type="{http://tempuri.org/}RangeType"/>
 *         &lt;element name="OfficeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="EtdzMode" type="{http://tempuri.org/}EtdzMode"/>
 *         &lt;element name="TicketType" type="{http://tempuri.org/}TicketType"/>
 *         &lt;element name="PolicyType" type="{http://tempuri.org/}OrderPolicyType"/>
 *         &lt;element name="ReationType" type="{http://tempuri.org/}PolicyReationType"/>
 *         &lt;element name="IsHighRebate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PolicyRemark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://tempuri.org/}OrderSource"/>
 *         &lt;element name="AssociatedOrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FoundationOrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BaseOrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Fare" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="AirportFee" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BAF" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TotalValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OriginalRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SellCommission" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ActualRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BuyCommission" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProviderAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SettleAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayInterface" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayTradeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="ContactInfo" type="{http://tempuri.org/}ContactInfo" minOccurs="0"/>
 *         &lt;element name="ProduceTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Operator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperatorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCoordination" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsValidPnr" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Passengers" type="{http://tempuri.org/}ArrayOfPassengerView" minOccurs="0"/>
 *         &lt;element name="Voyages" type="{http://tempuri.org/}ArrayOfVoyageView" minOccurs="0"/>
 *         &lt;element name="PurchaserInfo" type="{http://tempuri.org/}PurchaserInfo" minOccurs="0"/>
 *         &lt;element name="IsAgainRefund" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SpecialTreatment" type="{http://tempuri.org/}SpecialTreatement"/>
 *         &lt;element name="PayBalanceType" type="{http://tempuri.org/}PayBalanceType"/>
 *         &lt;element name="TicketCategory" type="{http://tempuri.org/}TicketCategory"/>
 *         &lt;element name="InsuranceOrderId" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProfitMode" type="{http://tempuri.org/}ProfitMode"/>
 *         &lt;element name="LazyRebate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="AttachedInfo" type="{http://tempuri.org/}OrderAttachedInfo" minOccurs="0"/>
 *         &lt;element name="AttachedFlag" type="{http://tempuri.org/}AttachedFlag"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderInfo", propOrder = {
    "id",
    "bPnr",
    "pnr",
    "ebPnr",
    "ePnr",
    "status",
    "purchaser",
    "purchaserName",
    "provider",
    "providerName",
    "type",
    "rangeType",
    "officeNo",
    "policyId",
    "etdzMode",
    "ticketType",
    "policyType",
    "reationType",
    "isHighRebate",
    "policyRemark",
    "source",
    "associatedOrderId",
    "foundationOrderId",
    "baseOrderId",
    "fare",
    "airportFee",
    "baf",
    "totalValue",
    "originalRebate",
    "sellCommission",
    "actualRebate",
    "buyCommission",
    "providerAmount",
    "settleAmount",
    "payInterface",
    "payTradeNo",
    "payTime",
    "contactInfo",
    "produceTime",
    "operator",
    "operatorName",
    "isCoordination",
    "isValidPnr",
    "passengers",
    "voyages",
    "purchaserInfo",
    "isAgainRefund",
    "specialTreatment",
    "payBalanceType",
    "ticketCategory",
    "insuranceOrderId",
    "profitMode",
    "lazyRebate",
    "attachedInfo",
    "attachedFlag"
})
@XmlSeeAlso({
    OrderDetailView.class
})
public class OrderInfo {

    @XmlElement(name = "Id", required = true)
    protected BigDecimal id;
    @XmlElement(name = "BPnr")
    protected String bPnr;
    @XmlElement(name = "Pnr")
    protected String pnr;
    @XmlElement(name = "EBPnr")
    protected String ebPnr;
    @XmlElement(name = "EPnr")
    protected String ePnr;
    @XmlElement(name = "Status", required = true)
    protected OrderStatus status;
    @XmlElement(name = "Purchaser", required = true)
    protected BigDecimal purchaser;
    @XmlElement(name = "PurchaserName")
    protected String purchaserName;
    @XmlElement(name = "Provider", required = true)
    protected BigDecimal provider;
    @XmlElement(name = "ProviderName")
    protected String providerName;
    @XmlElement(name = "Type", required = true)
    protected OrderType type;
    @XmlElement(name = "RangeType", required = true)
    protected RangeType rangeType;
    @XmlElement(name = "OfficeNo")
    protected String officeNo;
    @XmlElement(name = "PolicyId", required = true)
    protected BigDecimal policyId;
    @XmlElement(name = "EtdzMode", required = true)
    protected EtdzMode etdzMode;
    @XmlElement(name = "TicketType", required = true)
    protected TicketType ticketType;
    @XmlElement(name = "PolicyType", required = true)
    protected OrderPolicyType policyType;
    @XmlElement(name = "ReationType", required = true)
    protected PolicyReationType reationType;
    @XmlElement(name = "IsHighRebate")
    protected boolean isHighRebate;
    @XmlElement(name = "PolicyRemark")
    protected String policyRemark;
    @XmlElement(name = "Source", required = true)
    protected OrderSource source;
    @XmlElement(name = "AssociatedOrderId", required = true, nillable = true)
    protected BigDecimal associatedOrderId;
    @XmlElement(name = "FoundationOrderId", required = true)
    protected BigDecimal foundationOrderId;
    @XmlElement(name = "BaseOrderId", required = true)
    protected BigDecimal baseOrderId;
    @XmlElement(name = "Fare", required = true)
    protected BigDecimal fare;
    @XmlElement(name = "AirportFee", required = true)
    protected BigDecimal airportFee;
    @XmlElement(name = "BAF", required = true)
    protected BigDecimal baf;
    @XmlElement(name = "TotalValue", required = true)
    protected BigDecimal totalValue;
    @XmlElement(name = "OriginalRebate", required = true)
    protected BigDecimal originalRebate;
    @XmlElement(name = "SellCommission", required = true)
    protected BigDecimal sellCommission;
    @XmlElement(name = "ActualRebate", required = true)
    protected BigDecimal actualRebate;
    @XmlElement(name = "BuyCommission", required = true)
    protected BigDecimal buyCommission;
    @XmlElement(name = "ProviderAmount", required = true)
    protected BigDecimal providerAmount;
    @XmlElement(name = "SettleAmount", required = true)
    protected BigDecimal settleAmount;
    @XmlElement(name = "PayInterface")
    protected String payInterface;
    @XmlElement(name = "PayTradeNo")
    protected String payTradeNo;
    @XmlElement(name = "PayTime", required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar payTime;
    @XmlElement(name = "ContactInfo")
    protected ContactInfo contactInfo;
    @XmlElement(name = "ProduceTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar produceTime;
    @XmlElement(name = "Operator")
    protected String operator;
    @XmlElement(name = "OperatorName")
    protected String operatorName;
    @XmlElement(name = "IsCoordination")
    protected boolean isCoordination;
    @XmlElement(name = "IsValidPnr")
    protected boolean isValidPnr;
    @XmlElement(name = "Passengers")
    protected ArrayOfPassengerView passengers;
    @XmlElement(name = "Voyages")
    protected ArrayOfVoyageView voyages;
    @XmlElement(name = "PurchaserInfo")
    protected PurchaserInfo purchaserInfo;
    @XmlElement(name = "IsAgainRefund")
    protected boolean isAgainRefund;
    @XmlElement(name = "SpecialTreatment", required = true)
    protected SpecialTreatement specialTreatment;
    @XmlElement(name = "PayBalanceType", required = true)
    protected PayBalanceType payBalanceType;
    @XmlElement(name = "TicketCategory", required = true)
    protected TicketCategory ticketCategory;
    @XmlElement(name = "InsuranceOrderId", required = true)
    protected BigDecimal insuranceOrderId;
    @XmlElement(name = "ProfitMode", required = true)
    protected ProfitMode profitMode;
    @XmlElement(name = "LazyRebate", required = true)
    protected BigDecimal lazyRebate;
    @XmlElement(name = "AttachedInfo")
    protected OrderAttachedInfo attachedInfo;
    @XmlList
    @XmlElement(name = "AttachedFlag", required = true)
    protected List<String> attachedFlag;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Gets the value of the bPnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBPnr() {
        return bPnr;
    }

    /**
     * Sets the value of the bPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBPnr(String value) {
        this.bPnr = value;
    }

    /**
     * Gets the value of the pnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnr() {
        return pnr;
    }

    /**
     * Sets the value of the pnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnr(String value) {
        this.pnr = value;
    }

    /**
     * Gets the value of the ebPnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEBPnr() {
        return ebPnr;
    }

    /**
     * Sets the value of the ebPnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEBPnr(String value) {
        this.ebPnr = value;
    }

    /**
     * Gets the value of the ePnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEPnr() {
        return ePnr;
    }

    /**
     * Sets the value of the ePnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEPnr(String value) {
        this.ePnr = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatus }
     *     
     */
    public OrderStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatus }
     *     
     */
    public void setStatus(OrderStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the purchaser property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaser() {
        return purchaser;
    }

    /**
     * Sets the value of the purchaser property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaser(BigDecimal value) {
        this.purchaser = value;
    }

    /**
     * Gets the value of the purchaserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaserName() {
        return purchaserName;
    }

    /**
     * Sets the value of the purchaserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaserName(String value) {
        this.purchaserName = value;
    }

    /**
     * Gets the value of the provider property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProvider() {
        return provider;
    }

    /**
     * Sets the value of the provider property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProvider(BigDecimal value) {
        this.provider = value;
    }

    /**
     * Gets the value of the providerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderName() {
        return providerName;
    }

    /**
     * Sets the value of the providerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderName(String value) {
        this.providerName = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link OrderType }
     *     
     */
    public OrderType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderType }
     *     
     */
    public void setType(OrderType value) {
        this.type = value;
    }

    /**
     * Gets the value of the rangeType property.
     * 
     * @return
     *     possible object is
     *     {@link RangeType }
     *     
     */
    public RangeType getRangeType() {
        return rangeType;
    }

    /**
     * Sets the value of the rangeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RangeType }
     *     
     */
    public void setRangeType(RangeType value) {
        this.rangeType = value;
    }

    /**
     * Gets the value of the officeNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficeNo() {
        return officeNo;
    }

    /**
     * Sets the value of the officeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficeNo(String value) {
        this.officeNo = value;
    }

    /**
     * Gets the value of the policyId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyId() {
        return policyId;
    }

    /**
     * Sets the value of the policyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyId(BigDecimal value) {
        this.policyId = value;
    }

    /**
     * Gets the value of the etdzMode property.
     * 
     * @return
     *     possible object is
     *     {@link EtdzMode }
     *     
     */
    public EtdzMode getEtdzMode() {
        return etdzMode;
    }

    /**
     * Sets the value of the etdzMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtdzMode }
     *     
     */
    public void setEtdzMode(EtdzMode value) {
        this.etdzMode = value;
    }

    /**
     * Gets the value of the ticketType property.
     * 
     * @return
     *     possible object is
     *     {@link TicketType }
     *     
     */
    public TicketType getTicketType() {
        return ticketType;
    }

    /**
     * Sets the value of the ticketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketType }
     *     
     */
    public void setTicketType(TicketType value) {
        this.ticketType = value;
    }

    /**
     * Gets the value of the policyType property.
     * 
     * @return
     *     possible object is
     *     {@link OrderPolicyType }
     *     
     */
    public OrderPolicyType getPolicyType() {
        return policyType;
    }

    /**
     * Sets the value of the policyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderPolicyType }
     *     
     */
    public void setPolicyType(OrderPolicyType value) {
        this.policyType = value;
    }

    /**
     * Gets the value of the reationType property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyReationType }
     *     
     */
    public PolicyReationType getReationType() {
        return reationType;
    }

    /**
     * Sets the value of the reationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyReationType }
     *     
     */
    public void setReationType(PolicyReationType value) {
        this.reationType = value;
    }

    /**
     * Gets the value of the isHighRebate property.
     * 
     */
    public boolean isIsHighRebate() {
        return isHighRebate;
    }

    /**
     * Sets the value of the isHighRebate property.
     * 
     */
    public void setIsHighRebate(boolean value) {
        this.isHighRebate = value;
    }

    /**
     * Gets the value of the policyRemark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyRemark() {
        return policyRemark;
    }

    /**
     * Sets the value of the policyRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyRemark(String value) {
        this.policyRemark = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link OrderSource }
     *     
     */
    public OrderSource getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderSource }
     *     
     */
    public void setSource(OrderSource value) {
        this.source = value;
    }

    /**
     * Gets the value of the associatedOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAssociatedOrderId() {
        return associatedOrderId;
    }

    /**
     * Sets the value of the associatedOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAssociatedOrderId(BigDecimal value) {
        this.associatedOrderId = value;
    }

    /**
     * Gets the value of the foundationOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFoundationOrderId() {
        return foundationOrderId;
    }

    /**
     * Sets the value of the foundationOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFoundationOrderId(BigDecimal value) {
        this.foundationOrderId = value;
    }

    /**
     * Gets the value of the baseOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBaseOrderId() {
        return baseOrderId;
    }

    /**
     * Sets the value of the baseOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBaseOrderId(BigDecimal value) {
        this.baseOrderId = value;
    }

    /**
     * Gets the value of the fare property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFare() {
        return fare;
    }

    /**
     * Sets the value of the fare property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFare(BigDecimal value) {
        this.fare = value;
    }

    /**
     * Gets the value of the airportFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAirportFee() {
        return airportFee;
    }

    /**
     * Sets the value of the airportFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAirportFee(BigDecimal value) {
        this.airportFee = value;
    }

    /**
     * Gets the value of the baf property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBAF() {
        return baf;
    }

    /**
     * Sets the value of the baf property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBAF(BigDecimal value) {
        this.baf = value;
    }

    /**
     * Gets the value of the totalValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalValue() {
        return totalValue;
    }

    /**
     * Sets the value of the totalValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalValue(BigDecimal value) {
        this.totalValue = value;
    }

    /**
     * Gets the value of the originalRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOriginalRebate() {
        return originalRebate;
    }

    /**
     * Sets the value of the originalRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOriginalRebate(BigDecimal value) {
        this.originalRebate = value;
    }

    /**
     * Gets the value of the sellCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSellCommission() {
        return sellCommission;
    }

    /**
     * Sets the value of the sellCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSellCommission(BigDecimal value) {
        this.sellCommission = value;
    }

    /**
     * Gets the value of the actualRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getActualRebate() {
        return actualRebate;
    }

    /**
     * Sets the value of the actualRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setActualRebate(BigDecimal value) {
        this.actualRebate = value;
    }

    /**
     * Gets the value of the buyCommission property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBuyCommission() {
        return buyCommission;
    }

    /**
     * Sets the value of the buyCommission property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBuyCommission(BigDecimal value) {
        this.buyCommission = value;
    }

    /**
     * Gets the value of the providerAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProviderAmount() {
        return providerAmount;
    }

    /**
     * Sets the value of the providerAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProviderAmount(BigDecimal value) {
        this.providerAmount = value;
    }

    /**
     * Gets the value of the settleAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSettleAmount() {
        return settleAmount;
    }

    /**
     * Sets the value of the settleAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSettleAmount(BigDecimal value) {
        this.settleAmount = value;
    }

    /**
     * Gets the value of the payInterface property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayInterface() {
        return payInterface;
    }

    /**
     * Sets the value of the payInterface property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayInterface(String value) {
        this.payInterface = value;
    }

    /**
     * Gets the value of the payTradeNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayTradeNo() {
        return payTradeNo;
    }

    /**
     * Sets the value of the payTradeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayTradeNo(String value) {
        this.payTradeNo = value;
    }

    /**
     * Gets the value of the payTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPayTime() {
        return payTime;
    }

    /**
     * Sets the value of the payTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPayTime(XMLGregorianCalendar value) {
        this.payTime = value;
    }

    /**
     * Gets the value of the contactInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ContactInfo }
     *     
     */
    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    /**
     * Sets the value of the contactInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactInfo }
     *     
     */
    public void setContactInfo(ContactInfo value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the produceTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProduceTime() {
        return produceTime;
    }

    /**
     * Sets the value of the produceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProduceTime(XMLGregorianCalendar value) {
        this.produceTime = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Gets the value of the operatorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Sets the value of the operatorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorName(String value) {
        this.operatorName = value;
    }

    /**
     * Gets the value of the isCoordination property.
     * 
     */
    public boolean isIsCoordination() {
        return isCoordination;
    }

    /**
     * Sets the value of the isCoordination property.
     * 
     */
    public void setIsCoordination(boolean value) {
        this.isCoordination = value;
    }

    /**
     * Gets the value of the isValidPnr property.
     * 
     */
    public boolean isIsValidPnr() {
        return isValidPnr;
    }

    /**
     * Sets the value of the isValidPnr property.
     * 
     */
    public void setIsValidPnr(boolean value) {
        this.isValidPnr = value;
    }

    /**
     * Gets the value of the passengers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPassengerView }
     *     
     */
    public ArrayOfPassengerView getPassengers() {
        return passengers;
    }

    /**
     * Sets the value of the passengers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPassengerView }
     *     
     */
    public void setPassengers(ArrayOfPassengerView value) {
        this.passengers = value;
    }

    /**
     * Gets the value of the voyages property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVoyageView }
     *     
     */
    public ArrayOfVoyageView getVoyages() {
        return voyages;
    }

    /**
     * Sets the value of the voyages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVoyageView }
     *     
     */
    public void setVoyages(ArrayOfVoyageView value) {
        this.voyages = value;
    }

    /**
     * Gets the value of the purchaserInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaserInfo }
     *     
     */
    public PurchaserInfo getPurchaserInfo() {
        return purchaserInfo;
    }

    /**
     * Sets the value of the purchaserInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaserInfo }
     *     
     */
    public void setPurchaserInfo(PurchaserInfo value) {
        this.purchaserInfo = value;
    }

    /**
     * Gets the value of the isAgainRefund property.
     * 
     */
    public boolean isIsAgainRefund() {
        return isAgainRefund;
    }

    /**
     * Sets the value of the isAgainRefund property.
     * 
     */
    public void setIsAgainRefund(boolean value) {
        this.isAgainRefund = value;
    }

    /**
     * Gets the value of the specialTreatment property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialTreatement }
     *     
     */
    public SpecialTreatement getSpecialTreatment() {
        return specialTreatment;
    }

    /**
     * Sets the value of the specialTreatment property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialTreatement }
     *     
     */
    public void setSpecialTreatment(SpecialTreatement value) {
        this.specialTreatment = value;
    }

    /**
     * Gets the value of the payBalanceType property.
     * 
     * @return
     *     possible object is
     *     {@link PayBalanceType }
     *     
     */
    public PayBalanceType getPayBalanceType() {
        return payBalanceType;
    }

    /**
     * Sets the value of the payBalanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayBalanceType }
     *     
     */
    public void setPayBalanceType(PayBalanceType value) {
        this.payBalanceType = value;
    }

    /**
     * Gets the value of the ticketCategory property.
     * 
     * @return
     *     possible object is
     *     {@link TicketCategory }
     *     
     */
    public TicketCategory getTicketCategory() {
        return ticketCategory;
    }

    /**
     * Sets the value of the ticketCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketCategory }
     *     
     */
    public void setTicketCategory(TicketCategory value) {
        this.ticketCategory = value;
    }

    /**
     * Gets the value of the insuranceOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInsuranceOrderId() {
        return insuranceOrderId;
    }

    /**
     * Sets the value of the insuranceOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInsuranceOrderId(BigDecimal value) {
        this.insuranceOrderId = value;
    }

    /**
     * Gets the value of the profitMode property.
     * 
     * @return
     *     possible object is
     *     {@link ProfitMode }
     *     
     */
    public ProfitMode getProfitMode() {
        return profitMode;
    }

    /**
     * Sets the value of the profitMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfitMode }
     *     
     */
    public void setProfitMode(ProfitMode value) {
        this.profitMode = value;
    }

    /**
     * Gets the value of the lazyRebate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLazyRebate() {
        return lazyRebate;
    }

    /**
     * Sets the value of the lazyRebate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLazyRebate(BigDecimal value) {
        this.lazyRebate = value;
    }

    /**
     * Gets the value of the attachedInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderAttachedInfo }
     *     
     */
    public OrderAttachedInfo getAttachedInfo() {
        return attachedInfo;
    }

    /**
     * Sets the value of the attachedInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderAttachedInfo }
     *     
     */
    public void setAttachedInfo(OrderAttachedInfo value) {
        this.attachedInfo = value;
    }

    /**
     * Gets the value of the attachedFlag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachedFlag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachedFlag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAttachedFlag() {
        if (attachedFlag == null) {
            attachedFlag = new ArrayList<String>();
        }
        return this.attachedFlag;
    }

}
