
package com.speed.iesales.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.speed.iesales.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSyncRateDelResponse_QNAME = new QName("http://service.iesales.speed.com/", "GetSyncRateDelResponse");
    private final static QName _GetSyncRateResponse_QNAME = new QName("http://service.iesales.speed.com/", "GetSyncRateResponse");
    private final static QName _GetSyncRateDel_QNAME = new QName("http://service.iesales.speed.com/", "GetSyncRateDel");
    private final static QName _GetSyncRate_QNAME = new QName("http://service.iesales.speed.com/", "GetSyncRate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.speed.iesales.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSyncRate }
     * 
     */
    public GetSyncRate createGetSyncRate() {
        return new GetSyncRate();
    }

    /**
     * Create an instance of {@link GetSyncRateDelResponse }
     * 
     */
    public GetSyncRateDelResponse createGetSyncRateDelResponse() {
        return new GetSyncRateDelResponse();
    }

    /**
     * Create an instance of {@link GetSyncRateDel }
     * 
     */
    public GetSyncRateDel createGetSyncRateDel() {
        return new GetSyncRateDel();
    }

    /**
     * Create an instance of {@link RateDel }
     * 
     */
    public RateDel createRateDel() {
        return new RateDel();
    }

    /**
     * Create an instance of {@link SyncRateResponse }
     * 
     */
    public SyncRateResponse createSyncRateResponse() {
        return new SyncRateResponse();
    }

    /**
     * Create an instance of {@link Timestamp }
     * 
     */
    public Timestamp createTimestamp() {
        return new Timestamp();
    }

    /**
     * Create an instance of {@link SyncRateDelResponse }
     * 
     */
    public SyncRateDelResponse createSyncRateDelResponse() {
        return new SyncRateDelResponse();
    }

    /**
     * Create an instance of {@link GetSyncRateResponse }
     * 
     */
    public GetSyncRateResponse createGetSyncRateResponse() {
        return new GetSyncRateResponse();
    }

    /**
     * Create an instance of {@link Rate }
     * 
     */
    public Rate createRate() {
        return new Rate();
    }

    /**
     * Create an instance of {@link StatusResponse }
     * 
     */
    public StatusResponse createStatusResponse() {
        return new StatusResponse();
    }

    /**
     * Create an instance of {@link SyncRateDelRequest }
     * 
     */
    public SyncRateDelRequest createSyncRateDelRequest() {
        return new SyncRateDelRequest();
    }

    /**
     * Create an instance of {@link SyncRateRequest }
     * 
     */
    public SyncRateRequest createSyncRateRequest() {
        return new SyncRateRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSyncRateDelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.iesales.speed.com/", name = "GetSyncRateDelResponse")
    public JAXBElement<GetSyncRateDelResponse> createGetSyncRateDelResponse(GetSyncRateDelResponse value) {
        return new JAXBElement<GetSyncRateDelResponse>(_GetSyncRateDelResponse_QNAME, GetSyncRateDelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSyncRateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.iesales.speed.com/", name = "GetSyncRateResponse")
    public JAXBElement<GetSyncRateResponse> createGetSyncRateResponse(GetSyncRateResponse value) {
        return new JAXBElement<GetSyncRateResponse>(_GetSyncRateResponse_QNAME, GetSyncRateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSyncRateDel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.iesales.speed.com/", name = "GetSyncRateDel")
    public JAXBElement<GetSyncRateDel> createGetSyncRateDel(GetSyncRateDel value) {
        return new JAXBElement<GetSyncRateDel>(_GetSyncRateDel_QNAME, GetSyncRateDel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSyncRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.iesales.speed.com/", name = "GetSyncRate")
    public JAXBElement<GetSyncRate> createGetSyncRate(GetSyncRate value) {
        return new JAXBElement<GetSyncRate>(_GetSyncRate_QNAME, GetSyncRate.class, null, value);
    }

}
