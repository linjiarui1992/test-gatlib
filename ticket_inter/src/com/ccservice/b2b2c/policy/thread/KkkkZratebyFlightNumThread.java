package com.ccservice.b2b2c.policy.thread;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.KkkkMethod;

/**
 * 
 * @author 贾建磊
 * 
 */
public class KkkkZratebyFlightNumThread implements Callable<List<Zrate>> {

	String scity;
	String ecity;
	String sdate;
	String flightnumber;
	String cabin;
	String passengerType;

	public KkkkZratebyFlightNumThread() {
	}

	public KkkkZratebyFlightNumThread(String scity, String ecity, String sdate,
			String flightnumber, String cabin,String passengerType) {
		this.scity = scity;
		this.ecity = ecity;
		this.sdate = sdate;
		this.flightnumber = flightnumber;
		this.cabin = cabin;
		this.passengerType = passengerType;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> kkkkZrates = new ArrayList<Zrate>();
		try {
			List<Segmentinfo> segmentInfos = new ArrayList<Segmentinfo>();
			Segmentinfo segmentinfo = new Segmentinfo();
			segmentinfo.setAircomapnycode(flightnumber.substring(0, 2));
			segmentinfo.setFlightnumber(flightnumber);
			segmentinfo.setDepartureCtiy(scity);
			segmentinfo.setArriveCity(ecity);
			segmentinfo.setDeparttime(new Timestamp(0, 0, 0, 0, 0, 0, 0));
			segmentinfo.setArrivaltime(new Timestamp(0, 0, 0, 0, 0, 0, 0));
			segmentinfo.setDepartureDate(sdate);
			segmentinfo.setCabincode(cabin);
			segmentInfos.add(segmentinfo);
			kkkkZrates = KkkkMethod.getPolicysRebateList(segmentInfos, passengerType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kkkkZrates;
	}
}
