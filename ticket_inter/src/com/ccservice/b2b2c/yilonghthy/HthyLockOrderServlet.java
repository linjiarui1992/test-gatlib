package com.ccservice.b2b2c.yilonghthy;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.yilong.HthyLockOrder;

public class HthyLockOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HthyLockOrderServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderid=request.getParameter("orderid");
		System.out.println("锁单请求orderID："+orderid);
		WriteLog.write("cn_home请求锁单入口", "orderid="+orderid);
		String result= HthyLockOrder.lockOrder(orderid);
		PrintWriter out = response.getWriter();
		try {
			out.print(result);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
