package com.ccservice.newelong.hoteldb;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import org.quartz.Job;
import org.dom4j.Element;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.dom4j.DocumentHelper;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.elong.inter.DownFile;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.newelong.util.UpdateElDataUtil;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;

/**
 * 酒店
 * 艺龙2.0接口
 * @author WH
 */
@SuppressWarnings("unchecked")
public class NewHotelDB implements Job {
    //SQL
    private static String findChainInfo = "select ID,C_BRANDID from T_CHAININFO where C_BRANDID is not null";

    private static String findRegion = "select ID,C_REGIONID,C_CITYID,C_TYPE from T_REGION where C_REGIONID is not null and C_CITYID > 0 and C_TYPE in ('1','2')";

    private static String findCity = "select ID,C_ELONGCITYID,C_PROVINCEID,C_COUNTRYID from T_CITY where C_TYPE = 1 and C_ELONGCITYID is not null";

    //城市
    private static List<City> cityList = Server.getInstance().getSystemService().findMapResultBySql(findCity, null);

    //行政区、商业区
    private static List<Region> regionList = Server.getInstance().getSystemService()
            .findMapResultBySql(findRegion, null);

    //品牌
    private static List<Chaininfo> chaininfoList = Server.getInstance().getSystemService()
            .findMapResultBySql(findChainInfo, null);

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        System.out.println("Start Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
        //下载文件
        DownFile.download("http://api.elong.com/xml/v2.0/hotel/hotellist.xml", "D:\\酒店数据\\hotellist.xml");
        System.out.println("Start Analy HotelList.xml");
        //解析
        read("D:\\酒店数据\\hotellist.xml");
        System.out.println("End Analy HotelList.xml");
        long end = System.currentTimeMillis();
        System.out.println("End Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
        System.out.println("耗时:" + DateSwitch.showTime(end - start));
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            long start = System.currentTimeMillis();
            System.out.println("Start Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
            //下载文件
            DownFile.download("http://api.elong.com/xml/v2.0/hotel/hotellist.xml", "D:\\酒店数据\\hotellist.xml");
            System.out.println("Start Analy HotelList.xml");
            //解析
            read("D:\\酒店数据\\hotellist.xml");
            System.out.println("End Analy HotelList.xml");
            long end = System.currentTimeMillis();
            System.out.println("End Time：" + UpdateElDataUtil.getFullStringTime(new Date()));
            System.out.println("耗时:" + DateSwitch.showTime(end - start));
        }
        catch (Exception e) {

        }
    }

    @SuppressWarnings("unchecked")
    private static void read(String filename) throws Exception {
        //获取XML
        SAXReader reader = new SAXReader();
        Document document = reader.read(new File(filename));
        String xml = document.asXML();
        //解析XML
        Document doc = DocumentHelper.parseText(xml.toString());
        Element root = doc.getRootElement().element("Hotels");
        List<Element> Hotels = root.elements("Hotel");//按照更新时间逆序排列
        //<Hotel HotelId="11602010" UpdatedTime="2013-10-25 01:14:28" Products="0" Status="1" />
        //<Hotel HotelId="11801036" UpdatedTime="2013-10-25 01:14:28" Products="0,2" Status="0" />
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Element Hotel : Hotels) {
            try {
                String HotelId = Hotel.attributeValue("HotelId");
                String UpdatedTime = Hotel.attributeValue("UpdatedTime"); //更新时间
                String Status = Hotel.attributeValue("Status"); //0--可用，1--不可用
                if (UpdateElDataUtil.StringIsNull(HotelId)) {
                    continue;
                }
                Hotel hotel = new Hotel();
                hotel.setHotelcode(HotelId);
                hotel.setSourcetype(1l);//酒店来源 1:艺龙
                hotel.setPaytype(1l);//现付
                hotel.setType(1);//国内
                hotel.setLanguage(0);//语言
                hotel.setElModifytime(UpdatedTime);
                if ("0".equals(Status)) {
                    hotel.setState(3);//酒店状态 是否可用 列名 表示当前酒店可用与否 0--表示暂时不可用 2--表示艺龙那边没有提供给我们房型 3--表示可用
                    hotel.setStatedesc("该酒店可用");
                }
                else {
                    hotel.setState(0);
                    hotel.setStatedesc("该酒店暂不可用");
                }
                //查询本地酒店
                String sql = "where C_SOURCETYPE = 1 and C_HOTELCODE = '" + HotelId + "'";
                List<Hotel> hotelList = Server.getInstance().getHotelService().findAllHotel(sql, "", -1, 0);
                //本地艺龙更新时间
                String localModifytime = "";
                //本地酒店
                Hotel localHotel = new Hotel();
                if (hotelList != null && hotelList.size() > 0) {
                    localHotel = hotelList.get(0);
                    //艺龙可用，本地不可用  --> 状态由状态、最低价格程序更新
                    if (hotel.getState() == 3 && localHotel.getState() != null && localHotel.getState() != 3) {
                        hotel.setState(localHotel.getState());
                    }
                    localModifytime = UpdateElDataUtil.StringIsNull(localHotel.getElModifytime()) ? "" : localHotel
                            .getElModifytime().trim();
                }
                //时间不相等
                if (!localModifytime.equals(UpdatedTime)) {
                    try {
                        //读取酒店详细，更新酒店
                        updatehotel(hotel, localHotel);
                    }
                    catch (Exception e) {
                        //未找到艺龙酒店详情XML，禁用酒店
                        if (localHotel.getId() > 0) {
                            //http://api.elong.com/xml/v2.0/hotel/cn/23/21502023.xml Nested exception: http://api.elong.com/xml/v2.0/hotel/cn/23/21502023.xml
                            String detailurl = UpdateElDataUtil.getELHotelDetailUrl(HotelId);
                            if ((detailurl + " Nested exception: " + detailurl).equals(e.getMessage())) {
                                hotel.setId(localHotel.getId());
                                hotel.setState(0);//暂时不可用
                                hotel.setStatedesc("该酒店暂不可用");
                                hotel.setLastupdatetime(sdf.format(new Date()));
                                Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
                                System.out.println("更新酒店**********" + localHotel.getId() + "**********"
                                        + localHotel.getName() + "**********" + HotelId);
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
            }
        }
    }

    /**
     * hotel 艺龙读取信息
     * localHotel 本地酒店信息
     */
    @SuppressWarnings("unchecked")
    private static void updatehotel(Hotel hotel, Hotel localHotel) throws Exception {
        //艺龙酒店详情XML
        String detailurl = UpdateElDataUtil.getELHotelDetailUrl(hotel.getHotelcode());
        //解析XML
        SAXReader reader = new SAXReader();
        Document document = reader.read(detailurl);
        String xml = document.asXML();
        Document doc = DocumentHelper.parseText(xml.toString());
        Element root = doc.getRootElement();
        Element Detail = root.element("Detail");
        String Id = root.attributeValue("Id");
        if (hotel.getHotelcode().equals(Id)) {
            //名称
            hotel.setName(Detail.elementText("Name").trim());
            //地址
            hotel.setAddress(UpdateElDataUtil.StringIsNull(Detail.elementText("Address")) ? "" : Detail
                    .elementText("Address"));
            //酒店所在地邮编
            hotel.setPostcode(UpdateElDataUtil.StringIsNull(Detail.elementText("PostalCode")) ? "" : Detail
                    .elementText("PostalCode"));
            //挂牌星级
            hotel.setStar(Integer.valueOf(UpdateElDataUtil.StringIsNull(Detail.elementText("StarRate")) ? "0" : Detail
                    .elementText("StarRate")));
            //装修级别 五星级/豪华  四星级/高档  三星级/舒适  二星级/经济
            String Category = Detail.elementText("Category");//-1，0：代表经济型酒店 1：一星级酒店 2：二星级酒店 3：三星级酒店 4：四星级酒店 5：五星级酒店
            if ("5".equals(Category)) {
                hotel.setRepair(1);//1：豪华
            }
            else if ("4".equals(Category)) {
                hotel.setRepair(2);//2：高档
            }
            else if ("3".equals(Category)) {
                hotel.setRepair(3);//3：舒适
            }
            else {
                hotel.setRepair(4);//4：经济
            }
            //电话
            hotel.setMarkettell(UpdateElDataUtil.StringIsNull(Detail.elementText("Phone")) ? "" : Detail
                    .elementText("Phone"));
            //传真
            hotel.setFax1(UpdateElDataUtil.StringIsNull(Detail.elementText("Fax")) ? "" : Detail.elementText("Fax"));
            //酒店开业日期
            String EstablishmentDate = Detail.elementText("EstablishmentDate");
            if (!UpdateElDataUtil.StringIsNull(EstablishmentDate)) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    if (EstablishmentDate.length() == 7) {//1996-09
                        EstablishmentDate = EstablishmentDate + "-01";
                    }
                    if (EstablishmentDate.length() == 10) {//1996-09-01
                        java.sql.Date date = new java.sql.Date(
                                new Timestamp(sdf.parse(EstablishmentDate).getTime()).getTime());
                        hotel.setOpendate(date);
                    }
                }
                catch (Exception e) {

                }
            }
            //酒店装修日期
            hotel.setRepaildate(UpdateElDataUtil.StringIsNull(Detail.elementText("RenovationDate")) ? "" : Detail
                    .elementText("RenovationDate"));
            //酒店所属连锁品牌ID
            String BrandId = Detail.elementText("BrandId");
            if (!UpdateElDataUtil.StringIsNull(BrandId) && chaininfoList != null && chaininfoList.size() > 0) {
                for (int i = 0; i < chaininfoList.size(); i++) {
                    Map map = (Map) chaininfoList.get(i);
                    if (BrandId.equals(map.get("C_BRANDID").toString())) {
                        hotel.setChaininfoid(Long.parseLong(map.get("ID").toString()));
                        break;
                    }
                }
            }
            //酒店所在位置的纬度
            String GoogleLat = Detail.elementText("GoogleLat");
            if (!UpdateElDataUtil.StringIsNull(GoogleLat)) {
                hotel.setLat(Double.valueOf(GoogleLat));
            }
            //酒店所在位置的经度
            String GoogleLon = Detail.elementText("GoogleLon");
            if (!UpdateElDataUtil.StringIsNull(GoogleLon)) {
                hotel.setLng(Double.valueOf(GoogleLon));
            }
            //酒店所在国家、省份、城市
            String CityId = Detail.elementText("CityId");
            if (!UpdateElDataUtil.StringIsNull(CityId) && cityList != null && cityList.size() > 0) {
                for (int i = 0; i < cityList.size(); i++) {
                    Map map = (Map) cityList.get(i);
                    if (Long.parseLong(CityId) == Long.parseLong(map.get("C_ELONGCITYID").toString())) {
                        //国家
                        hotel.setCountryid(Long.parseLong(map.get("C_COUNTRYID").toString()));
                        //省份
                        hotel.setProvinceid(Long.parseLong(map.get("C_PROVINCEID").toString()));
                        //城市
                        hotel.setCityid(Long.parseLong(map.get("ID").toString()));
                        break;
                    }
                }
            }

            if (hotel.getCityid() != null && hotel.getCityid() > 0) {
                //行政区
                String District = Detail.elementText("District");
                if (!UpdateElDataUtil.StringIsNull(District) && regionList != null && regionList.size() > 0) {
                    for (int i = 0; i < regionList.size(); i++) {
                        Map map = (Map) regionList.get(i);
                        if ((hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString()))
                                && Long.parseLong(District) == Long.parseLong(map.get("C_REGIONID").toString())
                                && Long.parseLong(map.get("C_TYPE").toString()) == 2) {
                            hotel.setRegionid1(Long.parseLong(map.get("ID").toString()));
                            break;
                        }
                    }
                }
                //商业区
                String BusinessZone = Detail.elementText("BusinessZone");
                if (!UpdateElDataUtil.StringIsNull(BusinessZone) && regionList != null && regionList.size() > 0) {
                    for (int i = 0; i < regionList.size(); i++) {
                        Map map = (Map) regionList.get(i);
                        if ((hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString()))
                                && Long.parseLong(BusinessZone) == Long.parseLong(map.get("C_REGIONID").toString())
                                && Long.parseLong(map.get("C_TYPE").toString()) == 1) {
                            hotel.setRegionid2(Long.parseLong(map.get("ID").toString()));
                            break;
                        }
                    }
                }
            }
            else {
                //城市ID为空，冗余数据，不新增或更新
                throw new Exception(detailurl + " Nested exception: " + detailurl); //当本地酒店存在时，抛此异常，禁用本地酒店
            }
            //可支持的信用卡
            hotel.setCarttype(UpdateElDataUtil.StringIsNull(Detail.elementText("CreditCards")) ? "" : Detail
                    .elementText("CreditCards"));
            //酒店介绍
            hotel.setDescription(UpdateElDataUtil.StringIsNull(Detail.elementText("IntroEditor")) ? "" : Detail
                    .elementText("IntroEditor"));
            //服务设施
            hotel.setServiceitem(UpdateElDataUtil.StringIsNull(Detail.elementText("GeneralAmenities")) ? "" : Detail
                    .elementText("GeneralAmenities"));
            //房间服务设施
            hotel.setRoomAmenities(UpdateElDataUtil.StringIsNull(Detail.elementText("RoomAmenities")) ? "" : Detail
                    .elementText("RoomAmenities"));
            //休闲服务设施概述
            hotel.setPlayitem(UpdateElDataUtil.StringIsNull(Detail.elementText("RecreationAmenities")) ? "" : Detail
                    .elementText("RecreationAmenities"));
            //会议服务设施信息
            hotel.setMeetingitem(UpdateElDataUtil.StringIsNull(Detail.elementText("ConferenceAmenities")) ? "" : Detail
                    .elementText("ConferenceAmenities"));
            //餐饮服务设施信息
            hotel.setFootitem(UpdateElDataUtil.StringIsNull(Detail.elementText("DiningAmenities")) ? "" : Detail
                    .elementText("DiningAmenities"));
            //周边交通信息概述
            hotel.setTrafficinfo(UpdateElDataUtil.StringIsNull(Detail.elementText("Traffic")) ? "" : Detail
                    .elementText("Traffic"));
            //设施列表
            hotel.setFacilities(UpdateElDataUtil.StringIsNull(Detail.elementText("Facilities")) ? "" : Detail
                    .elementText("Facilities"));
            //接机服务
            String AirportPickUpService = Detail.elementText("AirportPickUpService");
            if (UpdateElDataUtil.StringIsNull(AirportPickUpService) || !AirportPickUpService.contains(",")) {
                hotel.setAirportservice("");
            }
            else {
                //格式：开始日期,结束日期, 开始时间,结束时间
                String airtimes[] = AirportPickUpService.split(",");
                if (airtimes.length == 4) {
                    hotel.setAirportservice("接机开始日期:" + airtimes[0] + "<br/>" + "接机结束日期:" + airtimes[1] + "<br/>"
                            + "接机开始时间:" + airtimes[2] + "<br/>" + "接机结束时间:" + airtimes[3]);
                }
                else {
                    hotel.setAirportservice("");
                }
            }
            //卖点
            hotel.setSellpoint(UpdateElDataUtil.StringIsNull(Detail.elementText("Features")) ? "" : Detail
                    .elementText("Features"));
            //最后更新时间
            hotel.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            //更新
            long zshotelid = 0;//正式酒店ID
            if (localHotel.getId() > 0) {
                hotel.setId(localHotel.getId());
                Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
                System.out.println("更新酒店**********" + hotel.getId() + "**********" + hotel.getName() + "**********"
                        + hotel.getHotelcode());
                //正式酒店更新设施列表
                if (localHotel.getZshotelid() != null && localHotel.getZshotelid() > 0) {
                    Hotelall hotelall = Server.getInstance().getHotelService().findHotelall(localHotel.getZshotelid());
                    if (hotelall != null && hotelall.getId() > 0) {
                        zshotelid = localHotel.getZshotelid();
                        boolean upflag = false;
                        //百度经、纬度
                        String BaiduLat = Detail.elementText("BaiduLat");
                        String BaiduLon = Detail.elementText("BaiduLon");
                        if (!UpdateElDataUtil.StringIsNull(BaiduLat)
                                && !UpdateElDataUtil.StringIsNull(BaiduLon)
                                && (hotelall.getBLat() == null || hotelall.getBLng() == null
                                        || hotelall.getBLat().doubleValue() == 0 || hotelall.getBLng().doubleValue() == 0)) {
                            hotelall.setBLat(Double.parseDouble(BaiduLat));
                            hotelall.setBLng(Double.parseDouble(BaiduLon));
                            upflag = true;
                        }
                        if (!hotel.getFacilities().equals(hotelall.getFacilities())) {
                            hotelall.setFacilities(hotel.getFacilities());
                            upflag = true;
                        }
                        if (upflag) {
                            hotelall.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                            Server.getInstance().getHotelService().updateHotelallIgnoreNull(hotelall);
                        }
                    }
                }
            }
            //新增
            else {
                hotel = Server.getInstance().getHotelService().createHotel(hotel);
                System.out.println("新增酒店**********" + hotel.getId() + "**********" + hotel.getName() + "**********"
                        + hotel.getHotelcode());
            }
            /***********房型***********/
            Map<String, String> RoomNameMap = new HashMap<String, String>();//<房型ID,房型名称>，用于房型图片取描述
            Element Rooms = root.element("Rooms");
            List<Element> reles = Rooms.elements("Room");
            if (reles != null && reles.size() > 0) {
                //查询该酒店下所有房型
                List<Roomtype> roomTypeList = Server.getInstance().getHotelService()
                        .findAllRoomtype("where C_HOTELID = '" + hotel.getId() + "'", "", -1, 0);
                //EL与本地匹配上房型
                List<Roomtype> matchList = new ArrayList<Roomtype>();
                for (Element Room : reles) {
                    String RoomId = Room.attributeValue("Id");
                    String RoomName = Room.attributeValue("Name");
                    if (!UpdateElDataUtil.StringIsNull(RoomId) && !UpdateElDataUtil.StringIsNull(RoomName)) {
                        RoomNameMap.put(RoomId, RoomName);
                        Roomtype roomtype = new Roomtype();
                        roomtype.setRoomcode(RoomId);
                        roomtype.setName(RoomName.trim());
                        roomtype.setAreadesc(UpdateElDataUtil.StringIsNull(Room.attributeValue("Area")) ? "" : Room
                                .attributeValue("Area"));
                        roomtype.setLayer(UpdateElDataUtil.StringIsNull(Room.attributeValue("Floor")) ? "" : Room
                                .attributeValue("Floor"));
                        String BroadnetAccess = Room.attributeValue("BroadnetAccess");//0表示无宽带，1 表示有宽带
                        String BroadnetFee = Room.attributeValue("BroadnetFee");//0表示免费，1 表示收费
                        //宽带 0表示无宽带，1 表示免费 2表示收费
                        if ("1".equals(BroadnetAccess)) {
                            if ("0".equals(BroadnetFee)) {
                                roomtype.setWideband(1);
                            }
                            else if ("1".equals(BroadnetFee)) {
                                roomtype.setWideband(2);
                            }
                            else {
                                roomtype.setWideband(0);
                            }
                        }
                        else {
                            roomtype.setWideband(0);
                        }
                        // 床型描述信息 1 单人床 2 大床 3 双床 4 大或双 5 其他
                        String BedType = Room.attributeValue("BedType");
                        if (UpdateElDataUtil.StringIsNull(BedType)) {
                            BedType = "";
                        }
                        if (BedType.contains("单人床")) {
                            roomtype.setBed(1);
                        }
                        else if (BedType.contains("大床")) {
                            roomtype.setBed(2);
                        }
                        else if (BedType.contains("双床")) {
                            roomtype.setBed(3);
                        }
                        else if (BedType.contains("大或双")) {
                            roomtype.setBed(4);
                        }
                        else {
                            roomtype.setBed(5);
                        }
                        roomtype.setRoomdesc(UpdateElDataUtil.StringIsNull(Room.attributeValue("Description")) ? ""
                                : Room.attributeValue("Description"));
                        roomtype.setNote(UpdateElDataUtil.StringIsNull(Room.attributeValue("Comments")) ? "" : Room
                                .attributeValue("Comments"));
                        roomtype.setLanguage(0);
                        roomtype.setState(1);//可用
                        roomtype.setHotelid(hotel.getId());
                        //匹配本地房型
                        Roomtype localRoom = null;
                        for (Roomtype room : roomTypeList) {
                            if (roomtype.getRoomcode().equals(room.getRoomcode())) {
                                roomtype.setId(room.getId());
                                matchList.add(room);
                                localRoom = room;
                                break;
                            }
                        }
                        if (localRoom != null) {
                            if (!roomtype.getName().equals(localRoom.getName())
                                    || !roomtype.getAreadesc().equals(localRoom.getAreadesc())
                                    || !roomtype.getLayer().equals(localRoom.getLayer())
                                    || !roomtype.getWideband().equals(localRoom.getWideband())
                                    || !roomtype.getBed().equals(localRoom.getBed())
                                    || !roomtype.getRoomdesc().equals(localRoom.getRoomdesc())
                                    || !roomtype.getNote().equals(localRoom.getNote())
                                    || !roomtype.getLanguage().equals(localRoom.getLanguage())) {
                                Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(roomtype);
                                System.out.println("更新房型----------" + roomtype.getId() + "----------"
                                        + roomtype.getName() + "----------" + roomtype.getRoomcode());
                            }
                        }
                        else {
                            roomtype = Server.getInstance().getHotelService().createRoomtype(roomtype);
                            System.out.println("新增房型----------" + roomtype.getId() + "----------" + roomtype.getName()
                                    + "----------" + roomtype.getRoomcode());
                        }
                    }
                }
                //未匹配上房型房型状态置为不可用
                roomTypeList.removeAll(matchList);
                if (roomTypeList != null && roomTypeList.size() > 0) {
                    for (Roomtype room : roomTypeList) {
                        //房型状态 列名 0不可用 1 可用
                        room.setState(0);
                        Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(room);
                        System.out.println("房型未匹配上----------" + room.getId() + "----------" + room.getName()
                                + "----------" + room.getRoomcode());
                    }
                }
            }
            else {
                hotel.setState(2);
                hotel.setStatedesc("酒店可用,但是艺龙没有提供给我们房型!");
                hotel.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
                System.out.println("更新酒店**********" + hotel.getId() + "**********" + hotel.getName() + "**********"
                        + hotel.getHotelcode());
            }
            /***********图片***********/
            Element Images = root.element("Images");
            List<Element> imgs = Images.elements("Image");
            //正式酒店图片
            List<Hotelimage> zsImageList = new ArrayList<Hotelimage>();
            if (zshotelid > 0) {
                zsImageList = Server.getInstance().getHotelService()
                        .findAllHotelimage("where C_ZSHOTELID = " + zshotelid + " and C_SIZETYPE != 3", "", -1, 0);
            }
            if (imgs != null && imgs.size() > 0 && (zsImageList == null || zsImageList.size() == 0)) {
                //酒店列表页面图片，取一张
                List<Hotelimage> hotelListImages = new ArrayList<Hotelimage>();
                //本地酒店图片
                List<Hotelimage> imageList = Server.getInstance().getHotelService()
                        .findAllHotelimage("where C_HOTELID = " + hotel.getId(), "", -1, 0);
                for (Element img : imgs) {
                    /**
                     * 0-展示图；1-餐厅；2-休闲室；3-会议室 ；4-服务；5-酒店外观 ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
                     * 列表页面的图片使用类型5，如果没有依次考虑类型0和9
                     */
                    String Type = img.attributeValue("Type");
                    Element LocationsEle = img.element("Locations");
                    if (UpdateElDataUtil.StringIsNull(Type)) {
                        Type = "10";//其他
                    }
                    //图片对应房型ID，可为空
                    String ImgRoomId = img.elementText("RoomId");
                    //图片数组
                    List<Element> Locations = LocationsEle.elements("Location");
                    if (Locations != null && Locations.size() > 0) {
                        for (Element Location : Locations) {
                            /**
                             * 是否有水印
                             * 0-N
                             * 1-Y
                             */
                            String WaterMark = Location.attributeValue("WaterMark");
                            /**
                             * 1：jpg图片，固定长边350，固定长边350缩放(用于详情页图片展示)
                             * 2：jpg图片，尺寸70x70(用于详情页图片列表的缩微图)
                             * 3：jpg图片，尺寸120x120(用于列表页)
                             * 5：png图片，尺寸70x70
                             * 6：png图片，尺寸120x120
                             * 7：png图片，固定长边640放缩
                             */
                            String Size = Location.attributeValue("Size");
                            /**
                             * EL图片地址
                             */
                            String elImgUrl = Location.getText();
                            if (!"0".equals(WaterMark) || UpdateElDataUtil.StringIsNull(elImgUrl)) {
                                continue;
                            }
                            //设置类型为房型的图片描述
                            String description = "";
                            if ("2".equals(Type)) {
                                description = "休闲";
                            }
                            if ("3".equals(Type)) {
                                description = "会议室";
                            }
                            if ("5".equals(Type)) {
                                description = "酒店外观";
                            }
                            if ("6".equals(Type)) {
                                description = "大堂/接待台";
                            }
                            if ("8".equals(Type)) {
                                if (!UpdateElDataUtil.StringIsNull(ImgRoomId)
                                        && !UpdateElDataUtil.StringIsNull(RoomNameMap.get(ImgRoomId))) {
                                    description = RoomNameMap.get(ImgRoomId);
                                }
                                else {
                                    description = "客房";
                                }
                            }
                            if ("1".equals(Size)) {
                                //赋值
                                Hotelimage hotelImage = new Hotelimage();
                                hotelImage.setSizeType(1);//固定长边350
                                hotelImage.setType(Integer.parseInt(Type));
                                hotelImage.setDescription(description);
                                hotelImage.setHotelid(hotel.getId());
                                hotelImage.setLanguage(0);
                                hotelImage.setIsNewFlag(1);
                                if (zshotelid > 0)
                                    hotelImage.setZshotelid(zshotelid);
                                //保存图片
                                saveImage(hotelImage, hotel, elImgUrl, imageList);
                            }
                            //列表页图片，小图
                            if ("2".equals(Size) || "3".equals(Size) || "5".equals(Size) || "6".equals(Size)) {
                                Hotelimage hotelListImage = new Hotelimage();
                                hotelListImage.setSizeType(Integer.parseInt(Size));
                                hotelListImage.setType(Integer.parseInt(Type));
                                hotelListImage.setDescription(description);
                                hotelListImage.setHotelid(hotel.getId());
                                hotelListImage.setLanguage(0);
                                hotelListImage.setPath(elImgUrl);//临时值
                                hotelListImage.setIsNewFlag(1);
                                if (zshotelid > 0)
                                    hotelListImage.setZshotelid(zshotelid);
                                hotelListImages.add(hotelListImage);
                            }
                        }
                    }
                }
                //取一张酒店列表页小图
                if (hotelListImages.size() > 0) {
                    Hotelimage temp = null;
                    //酒店外观 120*120
                    for (Hotelimage img : hotelListImages) {
                        if (img.getType().intValue() == 5
                                && (img.getSizeType().intValue() == 3 || img.getSizeType().intValue() == 6)) {
                            temp = img;
                            break;
                        }
                    }
                    //酒店外观 70*70
                    if (temp == null) {
                        for (Hotelimage img : hotelListImages) {
                            if (img.getType().intValue() == 5
                                    && (img.getSizeType().intValue() == 2 || img.getSizeType().intValue() == 5)) {
                                temp = img;
                                break;
                            }
                        }
                    }
                    //展示图、背景图 120*120
                    if (temp == null) {
                        for (Hotelimage img : hotelListImages) {
                            if ((img.getType().intValue() == 0 || img.getType().intValue() == 9)
                                    && (img.getSizeType().intValue() == 3 || img.getSizeType().intValue() == 6)) {
                                temp = img;
                                break;
                            }
                        }
                    }
                    //展示图、背景图 70*70
                    if (temp == null) {
                        for (Hotelimage img : hotelListImages) {
                            if ((img.getType().intValue() == 0 || img.getType().intValue() == 9)
                                    && (img.getSizeType().intValue() == 2 || img.getSizeType().intValue() == 5)) {
                                temp = img;
                                break;
                            }
                        }
                    }
                    //120*120
                    if (temp == null) {
                        for (Hotelimage img : hotelListImages) {
                            if (img.getSizeType().intValue() == 3 || img.getSizeType().intValue() == 6) {
                                temp = img;
                                break;
                            }
                        }
                    }
                    //70*70
                    if (temp == null) {
                        for (Hotelimage img : hotelListImages) {
                            if (img.getSizeType().intValue() == 2 || img.getSizeType().intValue() == 5) {
                                temp = img;
                                break;
                            }
                        }
                    }
                    if (temp != null) {
                        //保存图片
                        temp.setSizeType(3);//列表页图片
                        saveImage(temp, hotel, temp.getPath(), imageList);
                    }
                }
            }
        }
    }

    /**
     * 保存图片
     */
    private static void saveImage(Hotelimage hotelImage, Hotel hotel, String elImgUrl, List<Hotelimage> imageList) {
        try {
            //本地存储路径
            String tempPath = "";
            if (hotel.getId() < 10) {
                tempPath = "0" + hotel.getId();
            }
            else {
                String strid = Long.toString(hotel.getId());
                tempPath = strid.substring(strid.length() - 2, strid.length()); //后两位数字
            }
            String filePath = "D:\\hotelimage\\" + hotel.getCityid() + "\\" + tempPath + "\\" + hotel.getId();
            if (!new File(filePath).exists()) {
                new File(filePath).mkdirs();
            }
            //库中存储图片名称
            String localImgName = elImgUrl.substring(elImgUrl.lastIndexOf("/") + 1, elImgUrl.lastIndexOf(".")) + ".jpg";
            //本地存储路径
            String localPath = filePath + "\\" + localImgName;
            //判断图片是否存在
            if (!new File(localPath).exists()) {
                //下载EL图片
                download(elImgUrl, localPath);
            }
            //修改数据库记录
            hotelImage.setPath("/hotelimage/" + hotel.getCityid() + "/" + tempPath + "/" + hotel.getId() + "/"
                    + localImgName);
            Hotelimage localImage = null;
            for (Hotelimage i : imageList) {
                //路径一样或酒店列表图片
                if (hotelImage.getPath().equals(i.getPath())
                        || (hotelImage.getSizeType().intValue() == 3 && hotelImage.getSizeType()
                                .equals(i.getSizeType()))) {
                    hotelImage.setId(i.getId());
                    localImage = i;
                    break;
                }
            }
            if (localImage != null) {
                if (!hotelImage.getSizeType().equals(localImage.getSizeType())
                        || !hotelImage.getType().equals(localImage.getType())
                        || !hotelImage.getDescription().equals(localImage.getDescription())
                        || !hotelImage.getLanguage().equals(localImage.getLanguage())
                        || (hotelImage.getZshotelid() != null && !hotelImage.getZshotelid().equals(
                                localImage.getZshotelid()))
                        || !hotelImage.getIsNewFlag().equals(localImage.getIsNewFlag())) {
                    Server.getInstance().getHotelService().updateHotelimageIgnoreNull(hotelImage);
                    System.out.println("更新了一条图片记录----------" + hotelImage.getId());
                }
            }
            else {
                hotelImage = Server.getInstance().getHotelService().createHotelimage(hotelImage);
                System.out.println("新增了一条图片记录----------" + hotelImage.getId());
            }
        }
        catch (Exception e) {
            System.out.println("抓取艺龙图片异常----------" + e.getMessage());
        }
    }

    /**
     * 下载图片
     */
    private static void download(String urlString, String filename) throws Exception {
        System.out.println("开始下载图片----------" + filename);
        URL url = new URL(urlString);
        URLConnection con = url.openConnection();
        InputStream is = con.getInputStream();
        byte[] bs = new byte[1024];
        int len;
        OutputStream os = new FileOutputStream(filename);
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        os.close();
        is.close();
    }
}