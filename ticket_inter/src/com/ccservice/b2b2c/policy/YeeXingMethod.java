package com.ccservice.b2b2c.policy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.dom4j.DocumentException;

import client.ServiceStub;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ben.YeeXingBook;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.yeexing.webservice.service.IBEService;
import com.yeexing.webservice.service.MD5Util;

public class YeeXingMethod {

	/**
	 * 3.3.3. 最后政策同步时间
	 * 
	 * @return
	 */
	public String getLastUpdateTime() {
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername();
		String sign = "";
		IBEService serv = new IBEService();
		try {
			sign = MD5Util.MD5(URLEncoder.encode(userName + yee.getKey(),
					"UTF-8").toUpperCase());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String result = serv.getIBEServiceHttpPort().airpSyncLastDate(userName,
				sign);
		// TODO 写日志
		WriteLog.write("易行天下", "lastTime:" + result);
		System.out.println(result);
		// String result = "<?xml version=\"1.0\"
		// encoding=\"GB2312\"?><result><lastModifyTime>2012-06-27
		// 14:03:41</lastModifyTime><is_success>T</is_success></result>";
		String time = "";
		try {
			org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(result);
			org.dom4j.Element root = doc.getRootElement();
			String is_success = root.elementText("is_success");
			if (is_success.equals("T")) {
				time = root.elementText("lastModifyTime");
			} else {
				time = "-1";
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		System.out.println(time);
		return time;
	}

	/**
	 * 3.1.1 PNR匹配政策 合作伙伴已经通过黑屏预订pnr成功，通过该接口可以通过pnr实时查询匹配的政策
	 * 
	 * @author 周俊波 2013-6-19 16:03:55
	 * @param pnr
	 * @return
	 */
	public static List<Zrate> PnrMatchAirp(String pnr) {
		List<Zrate> zrates = new ArrayList<Zrate>();
		try {
			YeeXingBook yee = new YeeXingBook();
			String userName = yee.getUsername();
			String temp = pnr + userName + yee.getKey();
			String sign = MD5Util.MD5(URLEncoder.encode(temp, "UTF-8")
					.toUpperCase());

			ServiceStub stub = new ServiceStub();
			ServiceStub.PnrMatchAirp pnrMatchAirp1 = new ServiceStub.PnrMatchAirp();
			pnrMatchAirp1.setPnr(pnr);
			pnrMatchAirp1.setUserName(userName);
			pnrMatchAirp1.setSign(sign);
			ServiceStub.PnrMatchAirpResponse response = stub
					.pnrMatchAirp(pnrMatchAirp1);
			String result = response.getPnrMatchAirpResult();
			WriteLog.write("易行天下", "获取最优政策:0:" + userName + ":" + pnr + ":"
					+ sign);
			WriteLog.write("易行天下", "获取最优政策:1:" + result);
			org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(result);
			org.dom4j.Element root = doc.getRootElement();
			String is_success = root.elementText("is_success");
			if (is_success.equals("T")) {

				// 航班限制(排除航班号)
				// String weeknum = lineinfo.attributeValue("");

				org.dom4j.Element priceinfos = root.element("priceinfos");
				List priceinfoslist = priceinfos.elements("priceinfo");
				Iterator it = priceinfoslist.iterator();
				while (it.hasNext()) {
					Zrate zrate = new Zrate();
					zrate.setAgentid(4L);
					zrate.setCreateuser("job");
					zrate.setCreatetime(new Timestamp(new Date().getTime()));
					zrate.setModifytime(new Timestamp(new Date().getTime()));
					zrate.setModifyuser("job");
					zrate.setIsenable(1);
					zrate.setUsertype("1");
					org.dom4j.Element lineinfos = root.element("lineinfos");
					String traveltype = lineinfos.attributeValue("airSeg");
					zrate.setTraveltype(Integer.parseInt(traveltype));
					// 航程类型 返回1表示单程，返回2表示往返，返回3表示单程往返
					String voyagetype = lineinfos.attributeValue("airSeg");
					zrate.setVoyagetype(voyagetype);
					org.dom4j.Element lineinfo = lineinfos.element("lineinfo");
					String departureport = lineinfo.attributeValue("orgCity");
					zrate.setDepartureport(departureport);
					// 到达机场
					String arrivalport = lineinfo.attributeValue("dstCity");
					zrate.setArrivalport(arrivalport);
					// 航空公司两字代码
					String aircompanycode = lineinfo.attributeValue("airComp");
					zrate.setAircompanycode(aircompanycode);
					// 使用航班号 如果此字段里面有值要判断所查询的航班是否匹配这个航班号
					String flightnumber = lineinfo.attributeValue("flight");
					zrate.setFlightnumber(flightnumber);
					// 舱位码
					String cabincode = lineinfo.attributeValue("cabin");
					zrate.setCabincode(cabincode);
					// 政策开始时间
					Timestamp begindate = new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd HH:mm").parse(
							lineinfo.attributeValue("startTime")).getTime());
					zrate.setBegindate(begindate);
					// 政策结束时间
					Timestamp enddate = new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd HH:mm").parse(
							lineinfo.attributeValue("endTime")).getTime());
					zrate.setEnddate(enddate);
					System.out.println(departureport + "----" + arrivalport
							+ "----" + aircompanycode + "----" + flightnumber
							+ "----" + cabincode + "----" + begindate + "----"
							+ enddate);

					org.dom4j.Element elmt = (org.dom4j.Element) it.next();
					String tickettype = elmt.attributeValue("tickType");
					zrate.setTickettype(Integer.parseInt(tickettype));
					String ischange = elmt.attributeValue("changePnr");
					zrate.setIschange(Long.parseLong(ischange));
					String isSphigh = elmt.attributeValue("isSphigh");
					zrate.setGeneral(Long.parseLong(isSphigh) + 1);
					zrate.setZtype(zrate.getGeneral() + "");
					zrate.setIstype(Long.parseLong(isSphigh));
					String outid = elmt.attributeValue("plcid");
					zrate.setOutid(outid);
					String disc = elmt.attributeValue("disc");
					zrate.setRatevalue(Float.parseFloat(disc));
					String extReward = elmt.attributeValue("extReward");
					zrate.setAddratevalue(Float.parseFloat(extReward));
					/**
					 * String tickPrice =
					 * elmt.attributeValue("tickPrice");//单张票价 String profits =
					 * elmt.attributeValue("profits");//单张利润
					 */
					String workTime = elmt.attributeValue("workTime");
					zrate.setWorktime(workTime.split("[-]")[0]);
					zrate.setAfterworktime(workTime.split("[-]")[1]);
					String remark = elmt.attributeValue("memo");// 备注信息
					zrate.setRemark(remark);
					if (zrate.getOutid() != null
							&& zrate.getRatevalue() != null) {
						zrates.add(zrate);
					} else {
						continue;
					}
				}
			}

		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return zrates;
	}

	/**
	 * 3.3.4.PNR匹配政策 合作伙伴已经通过黑屏预订pnr成功，通过该接口可以通过pnr实时查询匹配的政策。
	 * 
	 * @author 陈栋 2012-6-26 16:11:44
	 * @param pnr
	 * @return
	 */
	public List<Zrate> FindZrateByPNR(String pnr) {
		List<Zrate> zrates = new ArrayList<Zrate>();
		IBEService serv = new IBEService();
		try {
			YeeXingBook yee = new YeeXingBook();
			String userName = yee.getUsername();
			String temp = pnr + userName + yee.getKey();
			String sign = MD5Util.MD5(URLEncoder.encode(temp, "UTF-8")
					.toUpperCase());

			String result = serv.getIBEServiceHttpPort().pnrMatchAirp(userName,
					pnr, sign);
			System.out.println(result);

			WriteLog.write("易行天下的政策", "获取最优政策的信息" + userName + ":" + pnr + ":"
					+ sign);
			WriteLog.write("易行天下的政策", "获取最优政策" + result);
			org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(result);
			org.dom4j.Element root = doc.getRootElement();
			String is_success = root.elementText("is_success");
			if (is_success.equals("T")) {

				// 航班限制(排除航班号)
				// String weeknum = lineinfo.attributeValue("");

				org.dom4j.Element priceinfos = root.element("priceinfos");
				List priceinfoslist = priceinfos.elements("priceinfo");
				Iterator it = priceinfoslist.iterator();
				while (it.hasNext()) {
					Zrate zrate = new Zrate();
					zrate.setAgentid(4L);
					zrate.setCreateuser("job");
					zrate.setCreatetime(new Timestamp(new Date().getTime()));
					zrate.setModifytime(new Timestamp(new Date().getTime()));
					zrate.setModifyuser("job");
					zrate.setIsenable(1);
					zrate.setUsertype("1");
					org.dom4j.Element lineinfos = root.element("lineinfos");
					String traveltype = lineinfos.attributeValue("airSeg");
					zrate.setTraveltype(Integer.parseInt(traveltype));
					// 航程类型 返回1表示单程，返回2表示往返，返回3表示单程往返
					String voyagetype = lineinfos.attributeValue("airSeg");
					zrate.setVoyagetype(voyagetype);
					org.dom4j.Element lineinfo = lineinfos.element("lineinfo");
					String departureport = lineinfo.attributeValue("orgCity");
					zrate.setDepartureport(departureport);
					// 到达机场
					String arrivalport = lineinfo.attributeValue("dstCity");
					zrate.setArrivalport(arrivalport);
					// 航空公司两字代码
					String aircompanycode = lineinfo.attributeValue("airComp");
					zrate.setAircompanycode(aircompanycode);
					// 使用航班号 如果此字段里面有值要判断所查询的航班是否匹配这个航班号
					String flightnumber = lineinfo.attributeValue("flight");
					zrate.setFlightnumber(flightnumber);
					// 舱位码
					String cabincode = lineinfo.attributeValue("cabin");
					zrate.setCabincode(cabincode);
					// 政策开始时间
					Timestamp begindate = new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd HH:mm").parse(
							lineinfo.attributeValue("startTime")).getTime());
					zrate.setBegindate(begindate);
					// 政策结束时间
					Timestamp enddate = new Timestamp(new SimpleDateFormat(
							"yyyy-MM-dd HH:mm").parse(
							lineinfo.attributeValue("endTime")).getTime());
					zrate.setEnddate(enddate);
					System.out.println(departureport + "----" + arrivalport
							+ "----" + aircompanycode + "----" + flightnumber
							+ "----" + cabincode + "----" + begindate + "----"
							+ enddate);

					org.dom4j.Element elmt = (org.dom4j.Element) it.next();
					String tickettype = elmt.attributeValue("tickType");
					zrate.setTickettype(Integer.parseInt(tickettype));
					String ischange = elmt.attributeValue("changePnr");
					zrate.setIschange(Long.parseLong(ischange));
					String isSphigh = elmt.attributeValue("isSphigh");
					zrate.setGeneral(Long.parseLong(isSphigh) + 1);
					zrate.setZtype(zrate.getGeneral() + "");
					zrate.setIstype(Long.parseLong(isSphigh));
					String outid = elmt.attributeValue("plcid");
					zrate.setOutid(outid);
					String disc = elmt.attributeValue("disc");
					zrate.setRatevalue(Float.parseFloat(disc));
					String extReward = elmt.attributeValue("extReward");
					zrate.setAddratevalue(Float.parseFloat(extReward));
					/**
					 * String tickPrice =
					 * elmt.attributeValue("tickPrice");//单张票价 String profits =
					 * elmt.attributeValue("profits");//单张利润
					 */
					String workTime = elmt.attributeValue("workTime");
					zrate.setWorktime(workTime.split("[-]")[0]);
					zrate.setAfterworktime(workTime.split("[-]")[1]);
					String remark = elmt.attributeValue("memo");// 备注信息
					zrate.setRemark(remark);
					if (zrate.getOutid() != null
							&& zrate.getRatevalue() != null) {
						zrates.add(zrate);
					} else {
						continue;
					}
				}
			} else {
				WriteLog.write("易行天下根据PNR获取政策ERROR", pnr + ":"
						+ root.elementText("error"));
				return zrates;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return zrates;
	}

	/**
	 * 3.2.1. PNR生成订单 合作伙伴已经调用pnr匹配政策接口或者政策全取和同步接口获得政策id号，就通过该接口实时生成订单
	 * 
	 * @author 周俊波 2013-6-21 14:07:41
	 * 
	 */
	public String orderGenerationByPNR(String pnr, String plcid,
			String ibePrice, String outOrderid, String disc, String extReward) {
		extReward = "0";
		String sign = "";
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername();
		String result = "";
		outOrderid = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		try {
			ServiceStub stub = new ServiceStub();
			String signstr = disc + extReward + ibePrice + outOrderid + plcid
					+ pnr + userName + yee.getKey();
			sign = MD5Util.MD5(URLEncoder.encode(signstr, "UTF-8")
					.toUpperCase());
			WriteLog.write("CreateOrder易行天下", "下单的XML:0:" + signstr + ":sign:"
					+ sign);
			ServiceStub.PnrBook pnrBook = new ServiceStub.PnrBook();
			pnrBook.setDisc(disc);
			pnrBook.setExtReward(extReward);
			pnrBook.setIbePrice(ibePrice);
			pnrBook.setOut_orderid(outOrderid);
			pnrBook.setPlcid(plcid);
			pnrBook.setPnr(pnr);
			pnrBook.setSign(sign);
			pnrBook.setUserName(userName);
			ServiceStub.PnrBookResponse pnrBookResponse = stub.pnrBook(pnrBook);
			String xml = pnrBookResponse.getPnrBookResult();
			WriteLog.write("CreateOrder易行天下", "返回的XML:0.5:" + xml);
			org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(xml);
			org.dom4j.Element root = doc.getRootElement();
			String is_success = root.elementText("is_success");// T:成功F:失败
			String orderid = "";
			String totalPrice = "";
			if (is_success.equals("T")) {
				org.dom4j.Element orderInfo = root.element("orderInfo");
				orderid = orderInfo.attributeValue("orderid");
				org.dom4j.Element price = root.element("price");
				totalPrice = price.attributeValue("totalPrice");

				result += "S|" + orderid + "|";

				String payurl = getPayUrl(orderid, totalPrice, "1"); // 1是支付宝
				if (payurl != null && !payurl.equals("")
						&& !payurl.equals("FAIL")) {
					result += payurl; // 2是汇付天下
				} else {
					result += getPayUrl(orderid, totalPrice, "2"); // 2是汇付天下
				}

				result += "||" + totalPrice;
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		WriteLog.write("CreateOrder易行天下", "返回的XML:1:" + result);
		return result;
	}

	/**
	 * 3.3.5. PNR生成订单 合作伙伴已经调用pnr匹配政策接口或者政策全取和同步接口获得政策id号，就通过该接口实时生成订单
	 * 
	 * @author 陈栋
	 * @param pnr
	 * @param plcid
	 *            政策id号 Y
	 * @param ibePrice
	 *            票面价 Y
	 * @param outOrderid
	 *            外部订单号 Y 合作伙伴订单号（确保在合作伙伴系统中唯一）长度小于20位
	 * @param disc
	 *            政策返点 Y 3.0表示返3个点
	 * @param extReward
	 *            红包 Y 5.0表示返点之外再减5元
	 */
	public String createOrderbyPNR(String pnr, String plcid, String ibePrice,
			String outOrderid, String disc, String extReward) {
		String sign = "";
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername();
		String result = "";
		IBEService serv = new IBEService();
		outOrderid = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		try {
			String signstr = disc + extReward + ibePrice + outOrderid + plcid
					+ pnr + userName + yee.getKey();
			sign = com.tenpay.util.MD5Util.MD5Encode(signstr, "UTF-8")
					.toUpperCase();
			WriteLog
					.write("易行天下", "下单的XML:string:" + signstr + ":sign:" + sign);

			String xml = serv.getIBEServiceHttpPort().pnrBook(userName, pnr,
					plcid, ibePrice, outOrderid, disc, extReward, sign);

			WriteLog.write("易行天下", "返回的XML" + xml);
			org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(xml);
			org.dom4j.Element root = doc.getRootElement();
			String is_success = root.elementText("is_success");// T:成功F:失败
			String orderid = "";
			String totalPrice = "";
			if (is_success.equals("T")) {
				org.dom4j.Element orderInfo = root.element("orderInfo");
				orderid = orderInfo.attributeValue("orderid");
				org.dom4j.Element price = root.element("price");
				totalPrice = price.attributeValue("totalPrice");

				String payurl = "";

				try {
					payurl = getPayUrl(orderid, totalPrice, "1"); // 1是支付宝
					if (payurl != null && !payurl.equals("")
							&& !payurl.equals("FAIL")) {

					} else {
						payurl = getPayUrl(orderid, totalPrice, "2"); // 2是汇付天下
					}
				} catch (Exception e) {
				}
				result += "S|" + orderid + "|" + payurl + "|";
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		System.out.println(result);
		return result;
	}

	/**
	 * 自动扣款
	 * 
	 * @param nativeorderid
	 *            本地订单号
	 * @param waibuorderid
	 *            外部订单号
	 * @param totalPrice
	 *            订单总支付价格
	 * @param payType
	 *            支付方式:1—支付宝 2—汇付 6—IPS 7—德付通
	 * @return
	 */
	public static String autoPay(String orderid, String totalPrice,
			String payType) {
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername(); // 合作伙伴用户名
		String payPlatform = payType; // 支付方式:1—支付宝 2—汇付 6—IPS 7—德付通
		String payNotifyUrl = yee.getPay_notify_url(); // 扣款成功通知地址
		String outNotifyUrl = yee.getOut_notify_url(); // 出票成功通知地址
		String sign = ""; // 签名
		try {
			Map<String, String> params = new HashMap<String, String>();
			WriteLog.write("自动代扣", "易行天下自动扣款传入接口的参数[外部订单号：" + orderid + "---支付金额："
					+ totalPrice + "---支付方式：" + payPlatform + "---签名：" + sign
					+ "]");
			params.put("userName", userName);
			params.put("orderid", orderid);
			params.put("totalPrice", totalPrice);
			params.put("payPlatform", payPlatform);
			params.put("pay_notify_url", payNotifyUrl);
			params.put("out_notify_url", outNotifyUrl);
			sign = YeeXingMethod.sign(params, yee.getKey());
			ServiceStub.PayOut payout = new ServiceStub.PayOut();
			payout.setUserName(userName);
			payout.setOrderid(orderid);
			payout.setTotalPrice(totalPrice);
			payout.setPayPlatform(payPlatform);
			payout.setPaynotifyurl(payNotifyUrl);
			payout.setOutnotifyurl(outNotifyUrl);
			payout.setSign(sign);

			ServiceStub stub = new ServiceStub();
			ServiceStub.PayOutResponse response = stub.payOut(payout);

			String strXML = response.getPayOutResult();
			WriteLog.write("自动代扣", "易行天下自动扣款返回的XML[" + strXML + "]");
			org.dom4j.Document document = org.dom4j.DocumentHelper
					.parseText(strXML);
			org.dom4j.Element root = document.getRootElement();
			if (root.elementText("is_success") != null
					&& root.elementText("is_success").equals("T")) {
				if (root.elementText("pay_status") != null
						&& root.elementText("pay_status").equals("T")) {
					return "S";
				} else {
					System.out.println("error===" + root.elementText("error"));
					return "-1";
				}
			} else {
				System.out.println("error===" + root.elementText("error"));
				return "-1";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "-1";
		}
	}

	/**
	 * 收银台支付 直接把收银台支付地址发给合作伙伴，由客人直接支付给易行。
	 * 
	 * @param orderid
	 *            订单号
	 * @param totalPrice
	 *            订单总支付价格
	 * @param payType
	 *            支付方式:1—支付宝 2—汇付 6—IPS 7—德付通
	 * @return
	 */
	public String getPayUrl(String orderid, String totalPrice, String payType) {
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername(); // 合作伙伴用户名
		String payPlatform = payType; // 支付方式:1—支付宝 2—汇付 6—IPS 7—德付通
		String payNotifyUrl = yee.getPay_notify_url(); // 扣款成功通知地址
		String outNotifyUrl = yee.getOut_notify_url(); // 出票成功通知地址
		String sign = ""; // 签名
		String result = "";
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("userName", userName);
			params.put("orderid", orderid);
			params.put("totalPrice", totalPrice);
			params.put("payPlatform", payPlatform);
			params.put("pay_notify_url", payNotifyUrl);
			params.put("out_notify_url", outNotifyUrl);
			sign = YeeXingMethod.sign(params, yee.getKey());
			WriteLog.write("易行天下", "获取支付链接传入接口的参数[订单号：" + orderid + "---支付金额："
					+ totalPrice + "---支付方式：" + payPlatform + "---签名：" + sign
					+ "]");

			ServiceStub.Pay pay = new ServiceStub.Pay();
			pay.setOrderid(orderid);
			pay.setOut_notify_url(outNotifyUrl);
			pay.setPay_notify_url(payNotifyUrl);
			pay.setPayPlatform(payPlatform);
			pay.setTotalPrice(totalPrice);
			pay.setUserName(userName);
			pay.setSign(sign);

			ServiceStub stub = new ServiceStub();
			ServiceStub.PayResponse response = stub.pay(pay);

			result = response.getPayResult();
			WriteLog.write("易行天下", "获取支付链接:" + result);
			System.out.println(result);
			org.dom4j.Document document = org.dom4j.DocumentHelper
					.parseText(result);
			org.dom4j.Element root = document.getRootElement();
			if (root.elementText("is_success") != null
					&& root.elementText("is_success").equals("T")) {
				result = root.elementText("payurl");
			} else {
				System.out.println("error==" + root.elementText("error"));
				return "FAIL";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "FAIL";
		}
		return result;
	}

	public void cacelOrder() {

	}

	public void createCHDorder(String adultPnr, String childPnr) {
		YeeXingBook yee = new YeeXingBook();
		String userName = yee.getUsername();
		String sign = "";
		try {
			// sign = MD5Util.MD5(URLEncoder.encode(
			// extReward + disc + ibePrice + outOrderid + plcid + pnr
			// + pnrText + userName + yee.getKey(), "UTF-8")
			// .toUpperCase());
			// String result =
			// serv.getIBEServiceHttpPort().pnrBookWithChild(userName, adultPnr,
			// childPnr, adultPlcId, childPlcId, adultIbePrice, childIbePrice,
			// outOrderId, disc, extReward, appUserName, sign)
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// /**
	// * 3.3.13. PNR信息生成订单 合作伙伴已经调用pnr信息匹配政策接口或者政策全取和同步接口获得政策id号，就通过该接口实时生成订单。
	// *
	// * @author 陈栋 2012-6-26 16:11:53
	// * @param pnr
	// * @param pnrText
	// * @param plcid
	// * 政策id号 Y
	// * @param ibePrice
	// * 票面价 Y
	// * @param outOrderid
	// * 外部订单号 Y 合作伙伴订单号（确保在合作伙伴系统中唯一）长度小于20位
	// * @param disc
	// * 政策返点 Y 3.0表示返3个点
	// * @param extReward
	// * 红包 Y 5.0表示返点之外再减5元
	// * @return
	// */
	// public String createPolicyOrderByPNRText(String pnr, String pnrText,
	// String plcid, String ibePrice, String outOrderid, String disc,
	// String extReward) {
	// IBEService serv = new IBEService();
	// String userName = yee.getUsername();
	// String sign = "";
	// try {
	// sign = MD5Util.MD5(URLEncoder.encode(
	// extReward + disc + ibePrice + outOrderid + plcid + pnr
	// + pnrText + userName + yee.getKey(), "UTF-8")
	// .toUpperCase());
	// } catch (UnsupportedEncodingException e) {
	// e.printStackTrace();
	// }
	// String result = serv.getIBEServiceHttpPort().parsePnrBook(userName,
	// pnr, pnrText, plcid, ibePrice, outOrderid, disc, extReward,
	// sign);
	// // String result = "";
	//		
	//		
	// // String XML = "<?xml version=\"1.0\"
	// // encoding=\"GB2312\"?><result><orderInfo orderid=\"T20120630111\"
	// // out_orderid=\"ts001\" /><is_success>T</is_success></result>";
	// try {
	// org.dom4j.Document doc = org.dom4j.DocumentHelper.parseText(result);
	// org.dom4j.Element root = doc.getRootElement();
	// String is_success = root.elementText("is_success");// T:成功F:失败
	// if (is_success.equals("T")) {
	// org.dom4j.Element orderInfo = root.element("orderInfo");
	// String orderid = orderInfo.attributeValue("orderid");
	// String out_orderid = orderInfo.attributeValue("out_orderid");
	// System.out.println(orderid + ":" + out_orderid);
	// result += "S|" + orderid + "|";
	// } else {
	// return "-1";
	// }
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// return result;
	// }
	//
	// public void getOrderInfo(String outOrderid, String orderid) {
	// IBEService serv = new IBEService();
	// // 用户名
	// String userName = yee.getUsername();
	// String sign = "";
	// try {
	// sign = MD5Util.MD5(URLEncoder.encode(
	// outOrderid + orderid + userName + yee.getKey(), "UTF-8")
	// .toUpperCase());
	// } catch (UnsupportedEncodingException e) {
	// e.printStackTrace();
	// }
	//
	// String result = serv.getIBEServiceHttpPort().orderQuery(userName,
	// outOrderid, orderid, sign);
	// System.out.println(result);
	// }

	/**
	 * 合作伙伴已经通过黑屏预订pnr成功，通过该接口可以通过pnr信息（含pat：a信息）实时查询匹配的政策
	 * 
	 * @author 陈栋 2012-6-26 16:11:44
	 * @param pnr
	 * @param pnrPata
	 * @return
	 */
	public void FindZrateByFlight(String pnr, String pnrPata) {
		try {
			YeeXingBook yee = new YeeXingBook();
			String userName = yee.getUsername();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// pnr导入
	private String pnrMatch() {
		IBEService serv = new IBEService();
		YeeXingBook yee = new YeeXingBook();
		// 用户名
		String userName = yee.getUsername();
		String pnr = "JRGJQE";// JQBYHH
		String sign = "";
		try {
			sign = MD5Util.MD5(URLEncoder.encode(pnr + userName + "1234567890",
					"UTF-8").toUpperCase());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String result = serv.getIBEServiceHttpPort().pnrMatchAirp(userName,
				pnr, sign);
		return result;
	}

	public void seachFl() {

	}

	// 订单状态查询
	private String orderQueryStatus() {
		IBEService serv = new IBEService();
		String userName = "hyccservice";
		String orderid = "T2012070614142";
		String outOrderid = "A10001";
		String sign = "";
		try {
			sign = MD5Util.MD5(URLEncoder.encode(
					orderid + outOrderid + userName + "123456", "UTF-8")
					.toUpperCase());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return serv.getIBEServiceHttpPort().orderQueryStatus(userName, orderid,
				outOrderid, sign);
	}

	/**
	 * 生成签名
	 * 
	 * @param params
	 * @param privateKey
	 * @return
	 */
	public static String sign(Map params, String privateKey) {
		List keys = new ArrayList(params.keySet());
		Collections.sort(keys);

		String prestr = "";
		for (int i = 0; i < keys.size(); ++i) {
			String key = (String) keys.get(i);
			String value = (String) params.get(key);
			prestr = prestr + value;
		}
		String resultstr = prestr + privateKey;
		String s = "";
		try {
			s = URLEncoder.encode(resultstr, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return MD5Util.MD5(s.toUpperCase());
	}
}
