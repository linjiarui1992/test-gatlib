/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.offline.domain.TrainOfflineRefundRecord
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年9月12日 上午9:50:07 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineRefundRecord {
    private Integer Id;//
    private Integer orderId;//
    private String operateName;//
    private String refundSum;//
    private String operateTime;//
    private Integer refundType;//
    private String seqId;//
    public Integer getId() {
        return Id;
    }
    public void setId(Integer id) {
        Id = id;
    }
    public Integer getOrderId() {
        return orderId;
    }
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
    public String getOperateName() {
        return operateName;
    }
    public void setOperateName(String operateName) {
        this.operateName = operateName;
    }
    public String getRefundSum() {
        return refundSum;
    }
    public void setRefundSum(String refundSum) {
        this.refundSum = refundSum;
    }
    public String getOperateTime() {
        return operateTime;
    }
    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }
    public Integer getRefundType() {
        return refundType;
    }
    public void setRefundType(Integer refundType) {
        this.refundType = refundType;
    }
    public String getSeqId() {
        return seqId;
    }
    public void setSeqId(String seqId) {
        this.seqId = seqId;
    }
    @Override
    public String toString() {
        return "TrainOfflineRefundRecord [Id=" + Id + ", orderId=" + orderId + ", operateName=" + operateName
                + ", refundSum=" + refundSum + ", operateTime=" + operateTime + ", refundType=" + refundType
                + ", seqId=" + seqId + "]";
    }
    
}
