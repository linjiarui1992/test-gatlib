package com.ccservice.b2b2c.bus;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ccservice.inter.job.WriteLog;

public class Ctriputil {

    public static String unicodeDecode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len;) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            value = (value << 4) + aChar - '0';
                            break;
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                            value = (value << 4) + 10 + aChar - 'a';
                            break;
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                            value = (value << 4) + 10 + aChar - 'A';
                            break;
                        default:
                            throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                }
                else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            }
            else
                outBuffer.append(aChar);
        }

        return outBuffer.toString();
    }

    public static String Geturl(String strurl) throws Exception {
        URL url = new URL(strurl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        InputStreamReader input = new InputStreamReader(httpConn.getInputStream(), "utf-8");
        BufferedReader bufReader = new BufferedReader(input);
        String line = "";
        StringBuilder contentBuf = new StringBuilder();
        while ((line = bufReader.readLine()) != null) {
            contentBuf.append(line);
        }
        String buf = contentBuf.toString();
        return buf = Ctriputil.unicodeDecode(buf);
    }

    private static class TrustAnyTrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    public static String postXieCheng(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            try {
                String cookiestring = s.toString();
                WriteLog.write("train12306_order", "访问gateway.do获取的Set-Cookie:" + cookiestring);
                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseMessage.toString();
    }

    public static Map<String, String> SpellingXieChengMap(boolean accept, String contentlength, boolean contenttype,
            String referer, String cookie) {
        Map<String, String> header = new HashMap<String, String>();
        if (accept) {
            header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        }
        else {
            header.put("Accept", "*/*");
        }
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        header.put("Connection", "keep-alive");
        if (contentlength != null && !contentlength.trim().equals("")) {
            header.put("Content-Length", contentlength);
        }
        if (contenttype) {
            header.put("Content-Type", "text/html; charset=gb2312");
        }
        if (cookie != null && !cookie.trim().equals("")) {
            header.put("Cookie", cookie);
        }
        header.put("Host", "my.ctrip.com");
        if (referer != null && !referer.trim().equals("")) {
            header.put("Referer", referer);
        }
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0");
        return header;
    }
}
