package com.ccservice.inter.job.train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadQunarOrder;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

public class JobQunarOrder extends TrainSupplyMethod implements Job {
    static Log log = LogFactory.getLog(JobQunarOrder.class);

    private final String order_url = "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=hcpzs&type=WAIT_TICKET&HMAC=3CE388BAA296019BA346766AF0F255F6";

    static String arrStationValue;

    static String dptStationValue;

    static String extSeatValue;

    static String extSeat_Key;

    static String seatValue_Key = null;

    static String orderDateValue;

    static String orderNoValue;

    static String certNoValue;

    static String certTypeValue = "";

    static String nameValue = null;

    static String seatValue;

    static String arrStationValue1;

    static String dptStationValue1;

    static String extSeatValue1;

    static String trainEndTimeValue;

    static String trainNoValue;

    static String trainStartTimeValue;

    static String arrStationValue_lc;

    static String dptStationValue_lc;

    static String extSeatValue_lc;

    static String seatValue_lc;

    static String trainEndTimeValue_lc;

    static String trainNoValue_lc;

    static String trainStartTimeValue_lc;

    static String birthday;

    static float ticketPayValue;

    static float seatValue_Value = 0.0F;

    static float extSeat_Value;

    static float ticketPayValue_lc = 0.0F;

    static int orderTypeValue;

    static int ticketTypeValue;

    static int seqValue;

    static int isjointtrip = 0;

    private long qcyg_agentid;

    private String qunarPayurl;

    WriteLog writeLog = new WriteLog();

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        String isqunaracquisition = null;
        long syscfid = 0L;
        if (getNowTime(new Date())) {
            try {
                isqunaracquisition = getSysconfigStringbydb("isqunaracquisition");
                syscfid = getSystemConfigId("isqunaracquisition");
                WriteLog.write("JobQunarOrder", "当前状态：" + isqunaracquisition);
            }
            catch (Exception localException1) {
            }
            if ((isqunaracquisition == null) || ("0".equals(isqunaracquisition))) {
                int i = (int) (Math.random() * 1000000.0D + 1.0D);
                try {
                    changeSystemCofig(syscfid, "1");
                    WriteLog.write("JobQunarOrder", "正在获取订单:" + i);
                }
                catch (Exception e) {
                    WriteLog.write("JobQunarOrder", "修改获取订单出错:" + i);
                }
                try {
                    WriteLog.write(
                            "JobQunarOrder",
                            "地址"
                                    + "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=hcpzs&type=WAIT_TICKET&HMAC=3CE388BAA296019BA346766AF0F255F6");
                    String url = HttpClient
                            .httpget(
                                    "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode=hcpzs&type=WAIT_TICKET&HMAC=3CE388BAA296019BA346766AF0F255F6",
                                    "UTF-8");
                    WriteLog.write("JobQunarOrder", "josn:" + url);
                    Classification(JSONObject.parseObject(url));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    changeSystemCofig(syscfid, "0");
                    WriteLog.write("JobQunarOrder", "获取订单结束:" + i);
                }
                catch (Exception e) {
                    WriteLog.write("JobQunarOrder", "修改获取订单状态出错:" + i);
                }
            }
        }
    }

    public static long getSystemConfigId(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig(" where c_name='" + name + "'", "", -1, 0);
        if ((configs != null) && (configs.size() == 1)) {
            Sysconfig config = (Sysconfig) configs.get(0);
            return config.getId();
        }
        return 0L;
    }

    public void changeSystemCofig(long scid, String scvalue) {
        if (scid > 0L) {
            Sysconfig sysconfig = Server.getInstance().getSystemService().findSysconfig(scid);
            sysconfig.setId(sysconfig.getId());
            sysconfig.setValue(scvalue);
            Server.getInstance().getSystemService().updateSysconfig(sysconfig);
        }
    }

    public static boolean getNowTime(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("06:05:00");
            Date dateAfter = df.parse("23:00:00");
            Date time = df.parse(df.format(date));
            if ((time.after(dateBefor)) && (time.before(dateAfter))) {
                return true;
            }
        }
        catch (ParseException e) {
            WriteLog.write("JobQunarOrder", "" + e);
        }
        return false;
    }

    public void Classification(JSONObject jsonObject) {
        getCommon();
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrder", jsonObject.toString());
            String orderNos = allOrderNos(data);
            WriteLog.write("JobQunarOrder", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
                            isoldorder = true;
                            exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setQunarordernumber(orderNo);
                        qunarordermethod.setId(0L);
                        qunarordermethod.setOrderstatus(2);
                        qunarordermethod.setCreatetime("");
                        qunarordermethod.setIsquestionorder(0);
                        qunarordermethod.setState12306(1);
                        qunarordermethod.setOrderjson(info);
                        qunarordermethod.setInterfaceType(2);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }

    public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }

    public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT C_QUNARORDERNUMBER,ID,C_ORDERSTATUS,C_CREATETIME,C_ISQUESTIONORDER,C_STATE12306,C_INTERFACETYPE FROM T_TRAINORDER  WHERE C_QUNARORDERNUMBER in ("
                +

                orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        WriteLog.write("JobQunarOrder", "sql_trainorder:" + sql_trainorder);
        WriteLog.write("JobQunarOrder", "list.size:" + list.size());
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("C_QUNARORDERNUMBER").toString());
            qunarordermethod.setId(Long.valueOf(map.get("ID").toString()).longValue());
            qunarordermethod.setOrderstatus(Integer.valueOf(map.get("C_ORDERSTATUS").toString()).intValue());
            qunarordermethod.setCreatetime(map.get("C_CREATETIME").toString());
            qunarordermethod.setIsquestionorder(Integer.valueOf(map.get("C_ISQUESTIONORDER").toString()).intValue());
            try {
                qunarordermethod.setState12306(Integer.valueOf(map.get("C_STATE12306").toString()).intValue());
            }
            catch (NumberFormatException e) {
                qunarordermethod.setState12306(0);
            }
            qunarordermethod.setOrderjson(new JSONObject());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }

    public void getCommon() {
        this.qcyg_agentid = Long.valueOf(getSysconfigString("qcyg_agentid")).longValue();
        this.qunarPayurl = getSysconfigString("qunarPayurl");
    }

    public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;
        WriteLog.write(
                "JobQunarOrder",
                "qunar订单号:"
                        + qunarordermethod.getQunarordernumber()
                        + "===当前订单ID:"
                        + (qunarordermethod.getId() == 0L ? "===不存在" : new StringBuilder(String
                                .valueOf(qunarordermethod.getId())).append("===当前订单状态:")
                                .append(qunarordermethod.getOrderstatus()).append("===当前订单问题订单状态:")
                                .append(qunarordermethod.getIsquestionorder()).append("===当前订单12306状态:")
                                .append(qunarordermethod.getState12306()).append("===当前订单创建时间")
                                .append(qunarordermethod.getCreatetime()).toString()));

        t1 = new MyThreadQunarOrder(qunarordermethod, this.qcyg_agentid, this.qunarPayurl);
        pool.execute(t1);

        pool.shutdown();
        WriteLog.write("JobQunarOrder", "qunar订单号:" + qunarordermethod.getQunarordernumber() + "线程关闭");
    }

    public static void changecustomeruser(String name, String id_no, String isverified) {
        String sql = "select * from T_CUSTOMERUSER b join T_CUSTOMERPASSENGER a on b.ID=a.C_CUSTOMERUSERID join T_CUSTOMERCREDIT c on a.ID=c.C_REFID where a.C_USERNAME = '"
                + name + "' and c.C_CREDITNUMBER='" + id_no + "'" + " and a.C_TYPE = 4 AND a.C_STATE=1";
        WriteLog.write("train12306lock", "changecustomeruser:12306账号锁:" + name + ":" + id_no + ":" + isverified);
        List<Customeruser> customeruserList = Server.getInstance().getMemberService()
                .findAllCustomeruserBySql(sql, -1, 0);
        if (customeruserList.size() > 0) {
            Customeruser customeruser = (Customeruser) customeruserList.get(0);
            customeruser.setId(customeruser.getId());
            customeruser.setEnname(isverified);
            Server.getInstance().getMemberService().updateCustomeruser(customeruser);
        }
    }
}
