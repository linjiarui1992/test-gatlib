package com.ccservice.inter.job.zhifubao;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;

public class PayLogToDatabase implements Job {
    //业务流水号
    static String trandnum = "";

    //支付名称列名
    static String payname = "";

    //金额列名
    static float amount;

    //订单时间
    static String ordertime;

    //key列名
    static String key = "";

    //业务类型名称
    static long busstype;

    //余额列名
    static float balance;

    // 支付订单号
    static String wnumber = "";

    //支付财务流水号
    static String liushuihao = "";

    public static String zhifubaolog_path = null;

    public static String encoding = "utf-8";

    public static String default_zhifubaolog_path = "";

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //定时log入库的方法
        exelogToDatabase();

    }

    //动态的获取路径然后调用入库的方法
    public static void exelogToDatabase() {
        String new_path = PropertyUtil.getValue("zhifubaologToDatabasepath", "train.properties");
        System.out.println(":::::::::::" + new_path);
        String nowdata = TimeUtil.gettodaydate(1);
        WriteLog.write("交易记录", "开始任务,当前路径：" + new_path);
        CopyTask(new_path);
        String filepathnew = new_path + "\\task\\" + nowdata + "\\";
        WriteLog.write("交易记录", "copy完毕：" + filepathnew);
        logToDatabase(filepathnew);
        WriteLog.write("交易记录", "任务完毕");
    }

    /**
     * copy 任务列表
     */
    public static void CopyTask(String path) {
        File filebase = new File(path);
        File[] filelist = filebase.listFiles();
        for (int i = 0; i < filelist.length; i++) {
            File file = filelist[i];
            if (file.isDirectory() && !"task".equals(file.getName()) && !"copyfile".equals(file.getName())) {
                File[] filetxt = file.listFiles();
                for (int j = 0; j < filetxt.length; j++) {
                    File f = filetxt[j];
                    String nowdata = TimeUtil.gettodaydate(1);
                    String timestr = getYestoday();
                    String filepath = path + "\\task\\" + nowdata + "\\" + file.getName();
                    File filepathfile = new File(filepath);
                    if (!filepathfile.exists()) {
                        filepathfile.mkdirs();
                    }
                    if (f.getName().contains(timestr)) {
                        try {
                            copyFile(f, new File(filepath + "\\" + f.getName()));
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        for (int i = 0; i < filelist.length; i++) {
            File file = filelist[i];
            if (file.isDirectory() && !"task".equals(file.getName()) && !"copyfile".equals(file.getName())) {
                File[] filetxt = file.listFiles();
                for (int j = 0; j < filetxt.length; j++) {
                    File f = filetxt[j];
                    String nowdata = TimeUtil.gettodaydate(1);
                    String timestr = getYestoday();
                    String filepath = path + "\\copyfile\\" + nowdata + "\\" + file.getName();
                    File filepathfile = new File(filepath);
                    if (!filepathfile.exists()) {
                        filepathfile.mkdirs();
                    }
                    if (f.getName().contains(timestr)) {
                        try {
                            copyFile(f, new File(filepath + "\\" + f.getName()));
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    /**
     * 获取昨天的时间
     * 
     * @param args
     * @time 2015年5月14日 下午5:15:33
     */
    public static String getYestoday() {
        Date dNow = new Date();
        Date dBefore = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dNow);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        dBefore = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String yestoday1 = sdf.format(dBefore);
        String yestoday = yestoday1.replace("-", ".");
        return yestoday;
    }

    /**
     * 
     * 查找数据进行插入数据
     * @param path
     * @time 2015年5月14日 下午8:34:32
     */

    public static void logToDatabase(String path) {
        String[] newStr = null;
        String lineTxt = null;
        //遍历账户文件夹
        File[] list = new File(path).listFiles();
        for (int i = 0; i < list.length; i++) {
            File sunfile = list[i];
            //账户名称
            String payname = sunfile.getName();
            if (sunfile.isDirectory() && sunfile.exists()) {
                File[] txtfile = sunfile.listFiles();
                for (int j = 0; j < txtfile.length; j++) {
                    if (txtfile[j].exists() && !txtfile[j].isDirectory()) {
                        //获取前天时间
                        try {
                            InputStreamReader read = new InputStreamReader(new FileInputStream(txtfile[j]), encoding);
                            BufferedReader bufferedReader = new BufferedReader(read);
                            lineTxt = bufferedReader.readLine();
                            List<String> str = new ArrayList<String>();
                            while (!"".equals(lineTxt) && lineTxt != null) {
                                lineTxt = bufferedReader.readLine();
                                str.add(lineTxt);
                            }
                            read.close();
                            StringBuffer sb = new StringBuffer();
                            for (int n = 4; n < str.size() - 5; n++) {
                                String sql = null;
                                if (str.size() > 4) {
                                    newStr = str.get(n).split(",");
                                    //将有用户的数据赋值给trade属性
                                    liushuihao = newStr[0].trim();
                                    trandnum = newStr[1].trim();
                                    wnumber = newStr[2].trim();
                                    ordertime = newStr[4];
                                    String shou = newStr[6].trim();
                                    String chu = newStr[7].trim();
                                    String typestr = newStr[10].trim();
                                    if ("".equals(shou)) {
                                        shou = "0.00";
                                    }
                                    if ("".equals(chu)) {
                                        chu = "0.00";
                                    }
                                    Float shouval = Float.parseFloat(shou);
                                    Float chuval = Float.parseFloat(chu);
                                    if (shouval.floatValue() != 0) {
                                        amount = shouval.floatValue();
                                    }
                                    if (chuval.floatValue() != 0) {
                                        amount = chuval.floatValue();
                                    }

                                    if (amount > 0) {
                                        busstype = 2l;
                                    }
                                    if (amount < 0) {
                                        busstype = 1l;
                                    }
                                    if (typestr != null && typestr.contains("转账")) {
                                        busstype = 3l;
                                    }
                                    balance = Float.parseFloat(newStr[8].trim());
                                    key = ElongHotelInterfaceUtil.MD5(trandnum + ordertime + balance + amount);
                                    sql = "insert into T_UNIONTRADE(C_TRANDNUM,C_PAYNAME,C_AMOUNT,C_ORDERTIME,C_KEY,C_BUSSTYPE,C_BALANCE,C_WNUMBER,C_LIUSHUIHAO,C_CREATETIME) select '"
                                            + trandnum
                                            + "','"
                                            + payname
                                            + "','"
                                            + amount
                                            + "','"
                                            + ordertime
                                            + "','"
                                            + key
                                            + "','"
                                            + busstype
                                            + "','"
                                            + balance
                                            + "','"
                                            + wnumber
                                            + "','"
                                            + liushuihao
                                            + "','"
                                            + TimeUtil.gettodaydate(4)
                                            + "' from T_UNIONTRADE where C_KEY='" + key + "' having count(1)<=0;";
                                    sb.append(sql);
                                }
                            }
                            if (addTrade(trandnum, payname, amount, ordertime, key, busstype, balance, wnumber,
                                    liushuihao, sb.toString())) {
                                WriteLog.write("交易记录插入成功", payname + ":" + txtfile[j].getName());
                            }
                            else {
                                WriteLog.write("交易记录插入失败", payname + ":" + txtfile[j].getName() + ":" + sb.toString());
                            }

                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        txtfile[j].delete();
                    }
                    else {
                        break;
                    }
                }

            }

        }

    }

    /**
     * 
     * 执行sql语句
     * @param args
     * @time 2015年5月14日 下午5:24:59
     * @author baiyushan
     */
    public static boolean addTrade(String trandnum, String payname, float amount, String ordertime, String key,
            long busstype, float balance, String wnumber, String liushuihao, String sqlstr) {
        int bo = Server.getInstance().getSystemService().excuteUniontradeBySql(sqlstr);
        if (bo > 0) {
            return true;
        }
        return false;
    }

    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            // 新建文件输入流并对它进行缓冲
            inBuff = new BufferedInputStream(new FileInputStream(sourceFile));
            // 新建文件输出流并对它进行缓冲
            outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));
            // 缓冲数组
            byte[] b = new byte[1024 * 5];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            // 刷新此缓冲的输出流
            outBuff.flush();
        }
        finally {
            // 关闭流
            if (inBuff != null)
                inBuff.close();
            if (outBuff != null)
                outBuff.close();
        }
    }

    public static void main(String[] args) {
        String path = PropertyUtil.getValue("zhifubaologToDatabasepath", "train.properties");
        System.out.println(path);
        //        CopyTask(path);
        //        logToDatabase(path);
        exelogToDatabase();
    }

}
