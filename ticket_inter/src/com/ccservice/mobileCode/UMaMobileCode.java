/**
 * 
 */
package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;

/**
 * 爱码
 * http://www.f02.cn/web/api.html
 * @time 2015年9月14日 下午1:58:11
 * @author chendong
 */
public class UMaMobileCode implements IMobileCode {
    //爱码平台登陆
    final static String LOGININURL = "http://api.f02.cn/http.do?action=loginIn";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://api.f02.cn/http.do?action=getMobilenum";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://api.f02.cn/http.do?action=getVcodeAndReleaseMobile";

    //加黑手机号码
    final static String ADDIGNORELISTURL = "http://api.f02.cn/http.do?action=addIgnoreList";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.6yzm.com/DevApi/getRecvingInfo";

    //    public static readonly String LoginIn = "http://www.6yzm.com/DevApi/loginIn";//登陆
    //    public static readonly String GetMobilenum = "http://www.6yzm.com/DevApi/getMobilenum";//获取号码
    //    public static readonly String GetVcodeAndHoldMobilenum = "http://www.6yzm.com/DevApi/getVcodeAndHoldMobilenum";//获取短信并继续使用该号码
    //    public static readonly String GetVcodeAndReleaseMobile = "http://www.6yzm.com/DevApi/getVcodeAndReleaseMobile";//获取短信并释放该号码
    //    public static readonly String AddIgnoreList = "http://www.6yzm.com/DevApi/addIgnoreList";//添加黑名单
    //    public static readonly String GetRecvingInfo = "http://www.6yzm.com/DevApi/getRecvingInfo";//获取该用户所使用的号码和项目
    //    public static readonly String CancelSMSRecvAll = "http://www.6yzm.com/DevApi/cancelSMSRecvAll";//释放所有号码
    //    public static readonly String CancelSMSRecv = "http://www.6yzm.com/DevApi/cancelSMSRecv";//释放单个号码
    //    final static String PID = "6168";//12306项目id
    //
    //    final static String UID = "120865";//12306
    //
    //    final static String AUTHOR_UID = "cd1989929";//开发者用户名

    String pid;//项目编号

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public UMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = "fe5e21d25d524ff8bad5f30d272f2158";
    }

    public static void main(String[] args) {
        String pid = "4036";//12306项目项目编号
        String uid = "";//12306
        String author_uid = "cd1989929";//开发者用户名
        String author_pwd = "cd1989929";//开发者用户名
        UMaMobileCode aimamobilecode = new UMaMobileCode(pid, uid, author_uid, author_pwd);
        //        String result = aimamobilecode.LoginIn(aimamobilecode.getAuthor_uid(), aimamobilecode.getAuthor_pwd());
        //        System.out.println("result:" + result);
        String mobile = aimamobilecode.GetMobilenum(pid, author_uid, "");
        System.out.println("mobile:" + mobile);
        //String SecurityCode = aimamobilecode.getVcodeAndReleaseMobile(author_uid, mobile, pid, mobile, author_uid);
        //System.out.println("SecurityCode:"+SecurityCode);
        System.out.println(aimamobilecode.AddIgnoreList(author_uid, mobile, pid, ""));

        //        token = youmamobilecode.gettoken(result, "Token");
        //        youmamobilecode.setToken(token);
        //        String Uid = gettoken(result, "Uid");
        //String mobile = "18718516480";
        //            mobile = youmamobilecode.GetMobilenum(youmamobilecode.getUid(), youmamobilecode.getPid(),
        //                    youmamobilecode.getToken());
        //            System.out.println(result);
        //        result = youmamobilecode.getVcodeAndReleaseMobile(youmamobilecode.getUid(), mobile, youmamobilecode.getPid(),
        //                youmamobilecode.getToken(), youmamobilecode.getAuthor_uid());
        //添加黑名单
        //        result = youmamobilecode.AddIgnoreList(youmamobilecode.getUid(), mobile, youmamobilecode.getPid(),
        //                youmamobilecode.getToken());
        //获取当前用户正在使用的号码列表
        //        result = youmamobilecode.GetRecvingInfo(youmamobilecode.getUid(), youmamobilecode.getPid(),
        //                youmamobilecode.getToken());
        //        System.out.println(result);
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年7月29日 下午3:35:09
     * @author chendong
     */
    public String gettoken(String result, String key) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = JSONObject.parseObject(result);
        }
        catch (Exception e) {
            System.out.println("gettoken_err:result:" + result);
        }
        String Token = "-1";
        try {
            Token = jsonObject.getString(key);
        }
        catch (Exception e) {
        }
        return Token;
    }

    /**
     * /// <summary>
        /// 优码平台登陆
        /// </summary>
        /// <param name="uid">用户名</param>
        /// <param name="pwd">密码</param>
        /// <returns></returns>
     */
    public String LoginIn(String uid, String pwd) {
        if (uid == null || pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + uid + "&pwd=" + pwd;
            String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            String[] results = result.split("[|]");
            if (results.length == 2) {
                result = results[1];
            }
            return result;
        }
    }

    /**
     * 
     * 获取手机号码
     * @param uid 登陆返回的用户ID（数值型的UID，不是用户名，如2684）
     * @param pid 项目ID
     * @param token 令牌
     * @return
     * @time 2015年7月29日 下午3:46:44
     * @author chendong
     */
    public String GetMobilenum(String pid, String uid, String token) {
        if (this.author_uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token
                    + "&mobile=&size=1";
            String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
            String[] results = result.split("[|]");
            if (results.length == 2) {
                result = results[0];
            }
            return result;
        }
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /**
     * 获取验证码并释放
     /// <param name="uid">用户ID （同获取号码）</param>
        /// <param name="mobile">手机号码</param>
        /// <param name="pid">项目ID</param>
        /// <param name="token">令牌</param>
        /// <param name="author_uid">开发者用户名</param>
     * 
     * @time 2015年7月29日 下午3:18:57
     * @author chendong
     */
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = "mobile=" + mobile + "&token=" + this.token + "&uid=" + this.author_uid
                    + "&author_uid=" + this.author_uid;
            String result = "-1";
            result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
            WriteLog.write("AiMaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":getVcodeAndReleaseMobile:" + result);
            if (result.contains("not_receive")) {
                result = "no_data";
            }
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles="
                    + mobiles;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("AiMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
