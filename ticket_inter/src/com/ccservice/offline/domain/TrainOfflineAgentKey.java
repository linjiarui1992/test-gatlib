/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainOfflineMatchAgent
 * @description: TODO - 相关的配置信息表
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月17日 上午9:29:33 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineAgentKey {
    private Integer PkId;//
    private String partnerName;//名称
    private String keys;//值
    private String notes;//
    private String notes1;//
    public Integer getPkId() {
        return PkId;
    }
    public void setPkId(Integer pkId) {
        PkId = pkId;
    }
    public String getPartnerName() {
        return partnerName;
    }
    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }
    public String getKeys() {
        return keys;
    }
    public void setKeys(String keys) {
        this.keys = keys;
    }
    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getNotes1() {
        return notes1;
    }
    public void setNotes1(String notes1) {
        this.notes1 = notes1;
    }
    @Override
    public String toString() {
        return "TrainOfflineAgentKey [PkId=" + PkId + ", partnerName=" + partnerName + ", keys=" + keys + ", notes="
                + notes + ", notes1=" + notes1 + "]";
    }
}
