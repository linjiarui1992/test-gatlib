package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.train.mqlistener.TrainOrderCancelGetMQMSGListener;

/**
 * Servlet implementation class TrainorderCancelgetMQMSG
 */
public class TrainorderCancelgetMQMSG extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int cancelmqnum = 1;

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        try {
            this.cancelmqnum = Integer.parseInt(this.getInitParameter("cancelmqnum"));
        }
        catch (Exception e) {
        }
        if ("1".equals(isstart)) {
            System.out.println("下单取消订单队列:开启");
            trainOrderCancel(this.mqaddress, this.mqusername);
        }
    }
    
    
    public void trainOrderCancel(String mqaddress, String mqusername) {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            for (int i = 0; i < cancelmqnum; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                Destination destination = new ActiveMQQueue(mqusername);
                MessageConsumer consumer = session.createConsumer(destination);
                consumer.setMessageListener(new TrainOrderCancelGetMQMSGListener());
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }

}
