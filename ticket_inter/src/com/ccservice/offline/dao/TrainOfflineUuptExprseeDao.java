/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.server.Server;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOfflineExpressCodeDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月28日 下午4:48:32 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineUuptExprseeDao {

    /**
     * 获取UU跑腿快递价钱
     * 
     * @param orderInfo 订单详细
     * @return
     * @throws Exception
     * @time 2017年10月22日 上午9:43:38
     * @author liujun
     */
    public int getOrderPrice(JSONObject jsonObject) throws Exception {
    	int result = 0;
        String orderId = jsonObject.getString("orderId"); // 订单ID
        String distance = jsonObject.getString("distance"); // 配送距离
        String price_token = jsonObject.getString("priceToken"); // 金额令牌
        Double total_money = jsonObject.getDouble("totalMoney"); // 订单总金额（优惠前）
        Double freight_money = jsonObject.getDouble("freightMoney"); // 跑腿费
        Double need_paymoney = jsonObject.getDouble("payMoney"); // 要支付金额
        Double goods_insurancemoney = jsonObject.getDouble("goodsInsuranceMoney"); // 商品保价金额
        Double addfee = jsonObject.getDouble("addFee"); // 加价金额
        Double coupon_amount = jsonObject.getDouble("couponAmount"); // 优惠券金额
        String couponid = jsonObject.getString("couponId"); // 优惠券ID
        String total_priceoff = jsonObject.getString("totalPriceOff"); // 总优惠金额
        String sql = "INSERT INTO uupt_orderprice (orderId, price_token, total_money, need_paymoney, distance, freight_money, couponid, coupon_amount, addfee, goods_insurancemoney, total_priceoff) VALUES "
                +"('"+orderId+"', '"+distance+"', '"+price_token+"', "+total_money+", "+freight_money+", "+need_paymoney+" , "+goods_insurancemoney+", "+addfee+", "+coupon_amount+", '"+couponid+"', '"+total_priceoff+"')";
        result = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
        return result;
    }

}
