
package com.liantuo.webservice.version2_0.applypolicyorderrefund;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="out" type="{http://applypolicyorderrefund.version2_0.webservice.liantuo.com}ApplyPolicyOrderRefundReply"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "out"
})
@XmlRootElement(name = "applyPolicyOrderRefundResponse")
public class ApplyPolicyOrderRefundResponse {

    @XmlElement(required = true, nillable = true)
    protected ApplyPolicyOrderRefundReply out;

    /**
     * Gets the value of the out property.
     * 
     * @return
     *     possible object is
     *     {@link ApplyPolicyOrderRefundReply }
     *     
     */
    public ApplyPolicyOrderRefundReply getOut() {
        return out;
    }

    /**
     * Sets the value of the out property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyPolicyOrderRefundReply }
     *     
     */
    public void setOut(ApplyPolicyOrderRefundReply value) {
        this.out = value;
    }

}
