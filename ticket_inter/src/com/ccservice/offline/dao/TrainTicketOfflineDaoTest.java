package com.ccservice.offline.dao;

import java.util.List;

import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;

public class TrainTicketOfflineDaoTest {
    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    public void testAddTrainTicketOffline() throws Exception {
        TrainTicketOfflineDao dao = new TrainTicketOfflineDao();

        TrainTicketOffline model = new TrainTicketOffline();
        model.setTrainPid(1L);
        model.setOrderId(10L);
        model.setDepartTime("2016-06-28 18:10:00.000");
        model.setDeparture("aaaa");
        model.setArrival("bbbb");
        model.setTrainNo("G590/K123/D126");
        model.setTicketType(1);
        model.setTicketNo("cccc");
        model.setSeatType("dddd");
        model.setSeatNo("eeee");
        model.setCoach("ffff");
        model.setPrice(0.0);
        model.setCostTime("26:34");
        model.setStartTime("18:10");
        model.setArrivalTime("18:10");

        model.setSealPrice(0.0);
        //      model.setTicketNo(null);
        //      model.setRealSeat(null);
        model.setSubOrderId("0"); //

        //      String reString="";
        //      try {
        //          reString =dao.addTrainTicketOffline(model);
        //      } catch (Exception e) {
        //          // TODO Auto-generated catch block
        //          e.printStackTrace();
        //          System.out.println(e.getMessage());
        //      }
        //      System.out.println("Id="+reString);

        //      try {
        //          model = dao.findTrainTicketOfflineById(1539L);
        //          System.out.println(model.getDeparture());
        //      } catch (Exception e) {
        //          // TODO Auto-generated catch block
        //          e.printStackTrace();
        //      }
    }

    public void testFindTrainTicketOfflineListByOrderId() throws Exception {
        /*String orderId = "test17081987854216";
        TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        System.out.println(trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(trainOrderOffline.getId()));*/
        Long orderId = 1290447L;
        //System.out.println(trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(orderId));
        System.out.println(trainTicketOfflineDao.findDepartTimeByOrderId(orderId));
    }

    public void testFindTrainTicketOfflineByTrainPid() throws Exception {
        String orderId = "test17081987854216";
        TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        List<TrainPassengerOffline> trainPassengerOfflineList = trainPassengerOfflineDao
                .findTrainPassengerOfflineListByOrderId(trainOrderOffline.getId());
        System.out.println(trainPassengerOfflineList);
        for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
            TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
            System.out.println(trainTicketOfflineDao.findTrainTicketOfflineByTrainPid(trainPassengerOffline.getId()));
        }
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainTicketOfflineDaoTest trainTicketOfflineDaoTest = new TrainTicketOfflineDaoTest();

        trainTicketOfflineDaoTest.testFindTrainTicketOfflineListByOrderId();
        //trainTicketOfflineDaoTest.testFindTrainTicketOfflineByTrainPid();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
