
package com.liantuo.webservice.version2_0.createpolicyorderbyspecialpnr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSegmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSegmentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SegmentInfo" type="{http://createpolicyorderbyspecialpnr.version2_0.webservice.liantuo.com}SegmentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSegmentInfo", propOrder = {
    "segmentInfo"
})
public class ArrayOfSegmentInfo {

    @XmlElement(name = "SegmentInfo", nillable = true)
    protected List<SegmentInfo> segmentInfo;

    /**
     * Gets the value of the segmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SegmentInfo }
     * 
     * 
     */
    public List<SegmentInfo> getSegmentInfo() {
        if (segmentInfo == null) {
            segmentInfo = new ArrayList<SegmentInfo>();
        }
        return this.segmentInfo;
    }

}
