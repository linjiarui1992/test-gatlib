package com.ccservice.inter.job.train;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
//1.正在下单12306超过40个短信提示。（殷树彬）
//2.下单所有服务器超过3分钟未返回结果的自动排除，短信通知。（陈栋）
//3.支付问题订单超过30个短信提示。（王战朝）
//4.申请退票超过40个短信通知。（王宏）
 * 监测等待支付订单 并警报
 * @time 2014年11月18日 上午9:25:46
 * @author yinshubin
 */
public class JobMonitoringOfPayments extends TrainSupplyMethod implements Job {

    public static void main(String[] args) {
        JobMonitoringOfPayments jmop = new JobMonitoringOfPayments();
        jmop.execute();
        //        String TodayRegCount = jmop.getRegCount();
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        execute();
    }

    private void execute() {
        StringBuffer mailString = new StringBuffer();
        mailString.append("通知时间:" + TimeUtil.gettodaydate(3));
        String TodayRegCount = getRegCount();
        mailString.append("\n\r今天当前时刻注册成功数:" + TodayRegCount);
        String CheckMobileCount = getCheckMobileCount();
        mailString.append("\n\r当前通过的手机核验数:" + CheckMobileCount);
        System.out.println(mailString.toString());
        sendWarnEmail("账号注册通知", mailString.toString());
    }

    /**
     * 获取一共核验了多少个手机号
     * @time 2015年9月11日 下午4:48:18
     * @author chendong
     */
    private ISystemService getISystemService() {
        String serviceurl = "";//同程(这个连接地址已经放到了配置文件)  http://121.40.62.200:40000/cn_service/service/
        String JobIndexAgain_LogintocheckHeyan_serviceurl = PropertyUtil.getValue(
                "JobIndexAgain_LogintocheckHeyan_serviceurl", "train.properties");
        serviceurl = JobIndexAgain_LogintocheckHeyan_serviceurl;
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return isystemservice;
    }

    /**
     * 
     * @time 2015年9月11日 下午4:46:30
     * @author chendong
     */
    private String getCheckMobileCount() {
        String sqlString = "select COUNT(1) AS count from T_CUSTOMERUSER with(nolock) where C_TYPE=4 and C_ISENABLE=21";
        ISystemService isystemservice = getISystemService();
        List datas = isystemservice.findMapResultBySql(sqlString, null);
        String count = "0";
        if (datas.size() > 0) {
            Map map = (Map) datas.get(0);
            count = map.get("count").toString();
        }
        return count;
    }

    /**
     * 获取今天注册多少个账号
     * @time 2015年9月11日 下午4:46:30
     * @author chendong
     */
    private String getRegCount() {
        String sqlString = "select COUNT(1) AS count from T_CUSTOMERUSER with(nolock) "
                + "where C_TYPE=4 and C_ISENABLE in(17,18,1,21,20,3,4) "
                + "and C_CREATETIME>convert(varchar(10),getdate(),120)";
        ISystemService isystemservice = getISystemService();
        List datas = isystemservice.findMapResultBySql(sqlString, null);
        String count = "0";
        if (datas.size() > 0) {
            Map map = (Map) datas.get(0);
            count = map.get("count").toString();
        }
        return count;
    }

    /**
     * 监控火车票订单数
     * 
     * @time 2015年1月27日 下午12:00:15
     * @author chendong
     * @param waitissue_payingquestionmax  等待出票_支付问题 警报数
     * @param waitissuemax  等待出票警报数
     * @param waitpaynummax  等待支付警报数
     */
    private void monitorTrainorderMethod(int waitpaynummax, int waitissuemax, int waitissue_payingquestionmax,
            int waitpay_orderingmax) {
        String jintian = TimeUtil.gettodaydate(1);
        //        String sql_WAITPAY = "SELECT COUNT(ID) FROM T_TRAINORDER WHERE C_ORDERSTATUS=1";//等待支付
        String sql_WAITPAY_WAITORDER = "SELECT COUNT(ID) FROM T_TRAINORDER WITH (NOLOCK) WHERE C_CREATETIME>'"
                + jintian + "' AND C_STATE12306=1";//等待支付_12306等待下单
        String sql_WAITPAY_ORDERING = "SELECT COUNT(ID) FROM T_TRAINORDER WITH (NOLOCK) WHERE C_CREATETIME>'" + jintian
                + "' AND C_STATE12306=2";//等待支付_12306正在下单
        String sql_WAITISSUE = "SELECT COUNT(ID) FROM T_TRAINORDER WITH (NOLOCK) WHERE C_CREATETIME>'" + jintian
                + "' AND C_ORDERSTATUS=2";//等待出票
        //        String sql_WAITISSUE_ORDERINGQUESTION = "SELECT COUNT(ID) FROM T_TRAINORDER WHERE C_ORDERSTATUS=2 and C_ISQUESTIONORDER=1";//等待出票_下单问题
        String sql_WAITISSUE_PAYINGQUESTION = "SELECT COUNT(ID) FROM T_TRAINORDER WITH (NOLOCK) WHERE C_CREATETIME>'"
                + jintian + "' AND C_ORDERSTATUS=2 and C_ISQUESTIONORDER=2";//等待出票_支付问题

        String sql_tuipiao_sql = "SELECT COUNT(ID) FROM T_TRAINTICKET WITH (NOLOCK) WHERE  C_ISAPPLYTICKET = 1 AND C_STATUS = 5";

        ISystemService ISystemService = Server.getInstance().getSystemService();
        try {
            // ISystemService = (ISystemService) new HessianProxyFactory().create(ISystemService.class,
            // "http://121.199.25.199:49001/cn_service/service/" + ISystemService.class.getSimpleName());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        long l1 = System.currentTimeMillis();
        //        int int_WAITPAY = ISystemService.countGiftBySql(sql_WAITPAY);//等待支付
        int int_WAITPAY = -1;//等待支付
        int int_WAITPAY_WAITORDER = ISystemService.countGiftBySql(sql_WAITPAY_WAITORDER);//等待支付_12306等待下单
        int int_WAITPAY_ORDERING = ISystemService.countGiftBySql(sql_WAITPAY_ORDERING);//等待支付_12306正在下单
        //        int int_WAITPAY_ORDERING = -1;
        int int_WAITISSUE = ISystemService.countGiftBySql(sql_WAITISSUE);//等待出票
        //        int int_WAITISSUE_ORDERINGQUESTION = ISystemService.countGiftBySql(sql_WAITISSUE_ORDERINGQUESTION);//等待出票_下单问题
        //        int int_WAITISSUE_ORDERINGQUESTION = -1;
        int int_WAITISSUE_PAYINGQUESTION = ISystemService.countGiftBySql(sql_WAITISSUE_PAYINGQUESTION);//等待出票_支付问题
        int INT_SQL_TUIPIAO_SQL = ISystemService.countGiftBySql(sql_tuipiao_sql);//等待出票_支付问题
        //        String infodata = "预警时间:" + TimeUtil.gettodaydate(4) + ":" + "等待支付:" + int_WAITPAY + ";等待支付_12306等待下单:"
        //                + int_WAITPAY_WAITORDER + ";等待支付_正在下单:" + int_WAITPAY_ORDERING + ";等待出票:" + int_WAITISSUE
        //                + ";等待出票_下单问题:" + int_WAITISSUE_ORDERINGQUESTION + ";等待出票_支付问题:" + int_WAITISSUE_PAYINGQUESTION;
        String infodata = "等支_等下单:" + int_WAITPAY_WAITORDER + ";等支_正下单:" + int_WAITPAY_ORDERING + ";等出票:"
                + int_WAITISSUE + ";等出票_支问题:" + int_WAITISSUE_PAYINGQUESTION + "|"
                + TimeUtil.gettodaydate(4).replace(" ", "");
        int issendWARN = issendWARN(waitpaynummax, int_WAITPAY, waitissuemax, int_WAITISSUE,
                waitissue_payingquestionmax, int_WAITISSUE_PAYINGQUESTION, waitpay_orderingmax, int_WAITPAY_ORDERING,
                INT_SQL_TUIPIAO_SQL);
        WriteLog.write("JobTrainorderMonitor", l1 + ":waitpaynummax=" + waitpaynummax + ":waitissuemax=" + waitissuemax
                + ":waitissue_payingquestionmax=" + waitissue_payingquestionmax + ":waitpay_orderingmax="
                + waitpay_orderingmax);
        WriteLog.write("JobTrainorderMonitor", l1 + ":issendWARN(0不发送 1发送邮件2发送短信3发退票4发短信和退票):" + issendWARN);
        WriteLog.write("JobTrainorderMonitor", l1 + ":infodata=" + infodata);
        String lasttongzhitime = getSysconfigStringbydb("lasttongzhitime");
        if (issendWARN == 2 || issendWARN == 1 || issendWARN > 4) {
            String yujingjiange = getSysconfigString("yujingjiange");
            Long L_yujingjiange = Long.parseLong(yujingjiange);
            Long L_lasttongzhitime = Long.parseLong(lasttongzhitime);
            if (l1 > L_lasttongzhitime + L_yujingjiange) {
                changeSystemCofigbyname("lasttongzhitime", System.currentTimeMillis() + "");
                sendWarnEmail("火车票订单预警通知", infodata);
            }
            sendWarnSms(infodata, 1);
        }

        if (issendWARN == 3) {
            l1 = System.currentTimeMillis();
            Long L_yujingjiange = 600000L;
            Long L_lasttongzhitime = Long.parseLong(lasttongzhitime);
            if (l1 > L_lasttongzhitime + L_yujingjiange) {
                changeSystemCofigbyname("lasttongzhitime", System.currentTimeMillis() + "");
                sendWarnEmail("火车票订单预警通知", infodata);
                String infodatas = "申请退票数:" + INT_SQL_TUIPIAO_SQL;
                sendWarnSms(infodatas, 2);
            }
        }

        l1 = l1 / 1000;
    }

    /**
     * 修改config的value
     * 
     * @param name
     * @param value
     * @time 2014年12月11日 下午10:18:43
     * @author chendong
     */
    public void changeSystemCofigbyname(String name, String value) {
        Server.getInstance().getSystemService()
                .findMapResultBySql("update T_SYSCONFIG set C_VALUE='" + value + "' where C_NAME='" + name + "'", null);

    }

    /**
     * 是否发送警报
     * 
     * @param type
     * @param waitpaynummax
     * @param waitpaynum //等待支付
     * @param waitissuemax
     * @param waitissue //等待出票
     * @param waitissue_payingquestionmax
     * @param waitissue_payingquestion 等待出票_支付问题
     * @return 0不发送 1发送邮件2发送短信
     * @time 2015年1月27日 下午6:59:59
     * @author chendong
     * @param int_WAITPAY_ORDERING 
     * @param waitpay_orderingmax 
     * @param iNT_SQL_TUIPIAO_SQL  现有退票
     */
    public int issendWARN(int waitpaynummax, int waitpaynum, int waitissuemax, int waitissue,
            int waitissue_payingquestionmax, int waitissue_payingquestion, int waitpay_orderingmax,
            int int_WAITPAY_ORDERING, int iNT_SQL_TUIPIAO_SQL) {
        int issend = 0;
        WriteLog.write("JobTrainorderMonitor", "waitpaynummax:" + waitpaynummax + "<=" + waitpaynum);
        WriteLog.write("JobTrainorderMonitor", "waitissuemax:" + waitissuemax + "<=" + waitissue);
        WriteLog.write("JobTrainorderMonitor", "waitissue_payingquestionmax:" + waitissue_payingquestionmax + "<="
                + waitissue_payingquestion);
        WriteLog.write("JobTrainorderMonitor", "waitpay_orderingmax:" + waitpay_orderingmax + "<="
                + int_WAITPAY_ORDERING);
        if (waitpaynummax <= waitpaynum || waitissuemax <= waitissue
                || waitissue_payingquestionmax <= waitissue_payingquestion
                || waitpay_orderingmax <= int_WAITPAY_ORDERING) {
            issend = 1;
        }
        float baifenbi = 0.4f;
        //        if (addnumbybaifenbi(waitpaynummax, baifenbi) <= waitpaynum
        //                || addnumbybaifenbi(waitissuemax, baifenbi) <= waitissue
        //                || addnumbybaifenbi(waitissue_payingquestionmax, baifenbi) <= waitissue_payingquestion
        //                || addnumbybaifenbi(waitpay_orderingmax, baifenbi) <= int_WAITPAY_ORDERING) {
        //            issend = 2;
        //        }
        if (iNT_SQL_TUIPIAO_SQL > 50) {
            if (issend == 0) {
                issend = 3;
            }
            else {
                issend = 3 + issend;
            }
        }
        return issend;
    }

    /**
     * 把传进来的数,传进来
     * 
     * @param oldnum
     * @param baifenbi
     * @return
     * @time 2015年1月27日 下午7:26:27
     * @author chendong
     */
    public int addnumbybaifenbi(int oldnum, float baifenbi) {
        oldnum = (int) (oldnum * (1 + baifenbi));
        return oldnum;
    }

    /**
     * 监控12306账号使用情况
     * 
     * @time 2015年1月27日 下午12:01:52
     * @author chendong
     */
    private void monitor12306AccountMethod() {

    }

    /**
     * 监测等待支付订单 
     * @time 2014年11月18日 上午9:25:29
     * @author yinshubin
     */
    public void monitoringOfPayments(int max) {
        System.currentTimeMillis();
        String sql_trainorder = "SELECT count(*) FROM T_TRAINORDER WHERE C_ORDERSTATUS ='" + Trainorder.WAITPAY + "'";
        int count = Server.getInstance().getSystemService().countGiftBySql(sql_trainorder);
    }

    /**
     * 等待支付订单警报 
     * @time 2014年11月18日 上午9:24:47
     * @author yinshubin
     */
    public void alarmOfWaitPay() {
        JobGenerateOrder jgo = new JobGenerateOrder();
        long cusid = Long.valueOf(getSysconfigString("alarmcusid"));
        Customeruser customeruser = Server.getInstance().getMemberService().findCustomeruser(cusid);
        String[] mobiles = { customeruser.getMobile() };
        Dnsmaintenance dns = jgo.getSmsDnsByAgentid(customeruser.getAgentid());
        boolean isalarm = Server.getInstance().getAtomService2()
                .sendSms(mobiles, "警报", 0, customeruser.getAgentid(), dns, 3);
        System.out.println(isalarm);
    }

    /**
     * 发送警告邮件
     * 
     * @param title
     * @param body
     * @return
     * @time 2015年1月24日 下午5:11:28
     * @author chendong
     */
    private int sendWarnEmail(String title, String body) {
        String mails_sysconfig = "249016428@qq.com";
        String[] mails_sysconfigs = mails_sysconfig.split(",");
        int resultemail = 0;
        String[] mails = new String[mails_sysconfigs.length];
        if (mails_sysconfigs.length > 0) {
            for (int i = 0; i < mails_sysconfigs.length; i++) {
                mails[i] = mails_sysconfigs[i];
            }
            String emailinfodata = body;
            resultemail = Server.getInstance().getAtomService2().sendSimpleMails(mails, title, emailinfodata);
        }
        WriteLog.write("JobTrainorderMonitor_sendWarnEmail", "发送结果:" + resultemail + ":邮箱地址:" + Arrays.toString(mails));
        return resultemail;
    }

    /**
     * 发送警告短信
     * 
     * @time 2015年1月24日 下午5:11:49
     * @author chendong
     * @param j 1 订单预警 2退票预警
     */
    private void sendWarnSms(String content, int j) {
        if (j == 1) {
            String mobiles_sysconfig = getSysconfigString("WarnSmsaddress");
            //        "13041234677,15811073432"
            mobiles_sysconfig = getSysconfigStringbydb("dingdantixingshouji");
            String[] mobiles_sysconfigs = mobiles_sysconfig.split(",");
            for (int i = 0; i < mobiles_sysconfigs.length; i++) {
                String mobile = mobiles_sysconfigs[i];
                boolean issuccess = sendSmspublic(content, mobile, 1);
                WriteLog.write("JobTrainorderMonitor_sendWarnSms", "发送结果:" + issuccess + ":手机号:" + mobile + ":"
                        + content);
            }
        }
        else if (j == 2) {
            String mobile = "13011162001";
            boolean issuccess = sendSmspublic(content, mobile, 1);
            WriteLog.write("JobTrainorderMonitor_sendWarnSms", "发送结果:" + issuccess + ":手机号:" + mobile + ":" + content);
        }
    }

    private Integer parseIntfromString(String data) {
        Integer resultint = 0;
        try {
            resultint = Integer.parseInt(data);
        }
        catch (Exception e) {
        }
        return resultint;
    }
}
