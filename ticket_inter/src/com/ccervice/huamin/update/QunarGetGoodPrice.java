package com.ccervice.huamin.update;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.b2b2c.atom.qunar.HotelData;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelall.Hotelall;
import com.ccservice.huamin.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.mixdata.GetQunarMixKey;

/**
 * 
 * @author wzc
 * 根据去哪酒店id获取去哪最优房型数据
 *
 */
public class QunarGetGoodPrice {
	
	private List<String> roomnames;
	
	// 返回去哪对应的数据集合
	private Map<String, List<HotelData>> roomDatas;
	
	

	
	public String getString(String str) {
		StringBuilder sb = new StringBuilder();
		if(str.indexOf("<span class='enc1' style='display: block;")>0){
			String res=str.substring(str.indexOf("<span class='enc1' style='display: block;"),str.lastIndexOf("</span>"));
			String[] st = res.split("<(\\S*?)[^>]*>.*?");
			for (int i = 0; i < st.length; i++) {
				String temp = st[i].trim();
				if (!temp.equals("")) {
					sb.append(temp);
				}
			}
		}
		return sb.toString();
	}
	
	//程序入口
	public static void main(String[] args) throws Exception {
		QunarGetGoodPrice p=new QunarGetGoodPrice();
		Map<Long, Map<String, List<HotelData>>> qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
		List<Hotelall> hotels = Server.getInstance().getHotelService().findAllHotelall("where c_qunarid is not null and c_qunarid!='' and c_cityid=101 and c_star>4","order by c_cityid asc", -1, 0);
		int k=hotels.size();
		for (Hotelall hotel : hotels) {
			System.out.println("酒店名称："+hotel.getName()+",剩余酒店数量："+k--);
			qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
			Map<Long, Map<String, List<HotelData>>> qunardata=p.findGetGoodPrice(hotel);
			Map<String, List<HotelData>> roomdata=qunardata.get(hotel.getId());
			if(roomdata!=null&&roomdata.size()>0){
				Set<String> roomname=roomdata.keySet();
				Iterator<String> name=roomname.iterator();
				while(name.hasNext()){
					String n=name.next();
					List<HotelData> data=roomdata.get(n);
					System.out.println(n);
					for (HotelData hotelData : data) {
						System.out.println(hotelData);
					}
				}
			}
		}
	}
	
	/**
	 * 抓取数据
	 * @throws Exception 
	 */
	public Map<Long, Map<String, List<HotelData>>> findGetGoodPrice(Hotelall hotel) throws Exception {
		String hotelid="";
		Map<Long, Map<String, List<HotelData>>> qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
		hotelid = hotel.getQunarId() + "";
		qunarDatas = new HashMap<Long, Map<String, List<HotelData>>>();
		qunarDatas=loadQunarData(hotel, Calendar.getInstance().getTime());
		return qunarDatas;
	}
	
	public Map<Long, Map<String, List<HotelData>>> loadQunarData(Hotelall hotel, Date date) throws Exception {
		// 返回酒店列表qunar数据集合
		Map<Long, Map<String, List<HotelData>>> qunarDatas=new HashMap<Long, Map<String,List<HotelData>>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		String citycode = "";
		citycode = hotel.getQunarId().substring(0,
				hotel.getQunarId().lastIndexOf("_"));
		String url = "http://hotel.qunar.com/render/detailV2.jsp?fromDate="
				+ sdf.format(date) + "&toDate=" + sdf.format(cal.getTime())
				+ "&cityurl=" + citycode + "&HotelSEQ="
				+ hotel.getQunarId().trim()+"&mixKey="+GetQunarMixKey.get(citycode);
		System.out.println(url);
		String str = HttpClient.httpget(url, "utf-8");
		roomnames = new ArrayList<String>();
		if (str != null && !str.equals("")) {
			String key = str.trim();
			if (!key.equals("")) {
				String strstr = key.substring(key.indexOf("(") + 1,key.lastIndexOf(")"));
				JSONObject datas = JSONObject.fromObject(strstr);
				JSONObject result = datas.getJSONObject("result");
				Iterator<String> strs = result.keys();
				HotelData hoteldata;
				roomDatas = new HashMap<String, List<HotelData>>();
				while (strs.hasNext()) {
					hoteldata = new HotelData();
					String keys = strs.next();
					JSONArray arra = JSONArray.fromObject(result.get(keys));
					if (arra.get(0) != null) {//去哪卖价 返佣之后的价格
						hoteldata.setSealprice(Integer.parseInt(arra.get(0)
								.toString()));
					}
					hoteldata.setAgentNo(keys.substring(0, keys.trim().indexOf("|")));
					hoteldata.setHotelid(hotel.getId() + "");
					hoteldata.setHotelname(hotel.getName());
					//qunar预订链接
					if(arra.get(4)!=null&&!"".equals(arra.get(4).toString())){
						hoteldata.setUrl(arra.getString(4).toString());
					}
					//去哪加盟商名称
					if(arra.get(5)!=null&&!"".equals(arra.get(5).toString())){
						hoteldata.setAgentName(arra.getString(5).toString());
					}
					//返佣
					if(arra.get(10)!=null&&!"".equals(arra.get(10).toString())){
						hoteldata.setFanyong(Double.valueOf(arra.get(10).toString()));
					}else{
						hoteldata.setFanyong(0.0d);
					}
					//去哪供应商提供的房型，css紊乱
					if (arra.get(2) != null) {
						String strtemp = arra.getString(2);
						hoteldata.setRoomnameBF(getString(strtemp));
					}
					// 开关房
					if (arra.get(9) != null) {
						String strtemp = arra.getString(9);
						hoteldata.setRoomstatus(Integer.parseInt(strtemp));
					}
					// 现预付
					if (arra.get(14) != null) {
						String strtemp = arra.getString(14);
						hoteldata.setType(Integer.parseInt(strtemp));
					}
					//担保
					if (arra.get(18) != null) {
						String strtemp = arra.getString(18);
						try {
							hoteldata.setDanbaoflag(Integer.valueOf(strtemp));
						} catch (RuntimeException e) {
							hoteldata.setDanbaoflag(0);
						}
					}
					if (arra.get(3) != null) {//去哪正式房型名称
						hoteldata.setRoomname(arra.getString(3));
						if (!roomnames.contains(arra.getString(3))) {
							roomnames.add(arra.getString(3));
						}
						if (roomDatas.containsKey(arra.getString(3))) {
							List<HotelData> datastemp = roomDatas.get(arra
									.getString(3));
							datastemp.add(hoteldata);
							Collections.sort(datastemp);
							roomDatas.put(arra.getString(3), datastemp);
						} else {
							List<HotelData> hoteldatas = new ArrayList<HotelData>();
							hoteldatas.add(hoteldata);
							Collections.sort(hoteldatas);
							roomDatas.put(arra.getString(3), hoteldatas);
						}
					}
				}
				qunarDatas.put(hotel.getId(), roomDatas);
			}
		}
		return qunarDatas;
	}
	
	
}
