package com.ccservice.huamin.hotel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Calendar;

public class WriteLog {

	/**
	 * 写日志<br>
	 * 
	 * 写logString字符串到./log目录下的文件中
	 * 
	 * @param logString
	 *            日志字符串
	 * 
	 * @author tower
	 * 
	 */

	public static void write(String fileNameHead, String logString) {

		try {

			String logFilePathName = null;

			Calendar cd = Calendar.getInstance();// 日志文件时间

			int year = cd.get(Calendar.YEAR);

			String month = addZero(cd.get(Calendar.MONTH) + 1);

			String day = addZero(cd.get(Calendar.DAY_OF_MONTH));

			String hour = addZero(cd.get(Calendar.HOUR_OF_DAY));

			String min = addZero(cd.get(Calendar.MINUTE));

			String sec = addZero(cd.get(Calendar.SECOND));

			File fileParentDir = new File("D:/qunar");// 判断log目录是否存在

			if (!fileParentDir.exists()) {

				fileParentDir.mkdir();

			}

			if (fileNameHead == null || fileNameHead.equals("")) {

				logFilePathName = "D:/qunar/" + year + month + day + ".log";// 日志文件名

			} else {

				logFilePathName = "D:/qunar/" + fileNameHead + year + month
						+ day + ".log";// 日志文件名

			}

			PrintWriter printWriter = new PrintWriter(new FileOutputStream(
					logFilePathName, true));// 紧接文件尾写入日志字符串
			printWriter.println(logString);

			printWriter.flush();

		} catch (FileNotFoundException e) {

			// TODO Auto-generated catch block

			e.getMessage();

		}

	}

	/**
	 * 整数i小于10则前面补0
	 * 
	 * @param i
	 * 
	 * @return
	 * 
	 * @author tower
	 * 
	 */

	public static String addZero(int i) {

		if (i < 10) {

			String tmpString = "0" + i;

			return tmpString;

		}

		else {

			return String.valueOf(i);

		}

	}

	public static void main(String[] args) {

		// 前面是文件名字,后面是内容

		write("121212", "4444");
		write("121212", "5555");
		write("ok", "111");

	}

}
