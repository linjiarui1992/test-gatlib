package com.ccservice.train.mqlistener;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

public class TrainorderBudanRemoveIdMessageListener extends TrainSupplyMethod implements MessageListener {
    private String orderNoticeResult;

    private String orderid;

    @Override
    public void onMessage(Message message) {
        try {
            orderNoticeResult = ((TextMessage) message).getText();
            JSONObject jsonobject = JSONObject.parseObject(orderNoticeResult);
            this.orderid = jsonobject.containsKey("orderid") ? jsonobject.getString("orderid") : "-1";
            try {
                List<String> list = Server.getInstance().getBudanids();
                if (list.size() > 0 && list.remove(orderid)) {
                    WriteLog.write("id_清除缓存中订单ID", "清除成功--->" + this.orderid);
                    Server.getInstance().setBudanids(list);
                }
                else {
                    WriteLog.write("id_清除缓存中订单ID", "清除失败--->" + this.orderid);
                }
            }
            catch (Exception e) {
                WriteLog.write("id_清除缓存中订单ID", "清除失败--->" + this.orderid);
            }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
