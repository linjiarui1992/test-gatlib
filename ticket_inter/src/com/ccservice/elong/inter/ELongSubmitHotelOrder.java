package com.ccservice.elong.inter;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;

import com.ccservice.elong.base.NorthBoundAPIServiceStub;

/**
 * 提交订单接口
 * @author 师卫林
 *
 */
public class ELongSubmitHotelOrder {
	public static void main(String[] args) throws RemoteException {
		NorthBoundAPIServiceStub stub=new NorthBoundAPIServiceStub();
		NorthBoundAPIServiceStub.RoomForSubmitHotelOrder roomForSubmitHotelOrder=new NorthBoundAPIServiceStub.RoomForSubmitHotelOrder();
		NorthBoundAPIServiceStub.ContacterForSubmitHotelOrder contacterForSubmitHotelOrder=new NorthBoundAPIServiceStub.ContacterForSubmitHotelOrder();
		NorthBoundAPIServiceStub.PhoneForSubmitHotelOrder phoneForSubmitHotelOrder=new NorthBoundAPIServiceStub.PhoneForSubmitHotelOrder();
		NorthBoundAPIServiceStub.FaxForSubmitHotelOrder faxForSubmitHotelOrder=new NorthBoundAPIServiceStub.FaxForSubmitHotelOrder();
		NorthBoundAPIServiceStub.ArrayOfContacterForSubmitHotelOrder arrayOfContacterForSubmitHotelOrder=new NorthBoundAPIServiceStub.ArrayOfContacterForSubmitHotelOrder();
		NorthBoundAPIServiceStub.CreditCardForSubmitHotelOrder cardForSubmitHotelOrder=new NorthBoundAPIServiceStub.CreditCardForSubmitHotelOrder();
		NorthBoundAPIServiceStub.ArrayOfGuestForSubmitHotelOrder arrayOfGuestForSubmitHotelOrder=new NorthBoundAPIServiceStub.ArrayOfGuestForSubmitHotelOrder();
		NorthBoundAPIServiceStub.GuestForSubmitHotelOrder guestForSubmitHotelOrder=new NorthBoundAPIServiceStub.GuestForSubmitHotelOrder();
		NorthBoundAPIServiceStub.SubmitHotelOrderResponseE responseE=new NorthBoundAPIServiceStub.SubmitHotelOrderResponseE();
		NorthBoundAPIServiceStub.SubmitHotelOrderRequest request=new NorthBoundAPIServiceStub.SubmitHotelOrderRequest();
		NorthBoundAPIServiceStub.HotelOrderForSubmitHotelOrder hotelOrderForSubmitHotelOrder=new NorthBoundAPIServiceStub.HotelOrderForSubmitHotelOrder();
		NorthBoundAPIServiceStub.ArrayOfRoomForSubmitHotelOrder arrayOfRoomForSubmitHotelOrder=new NorthBoundAPIServiceStub.ArrayOfRoomForSubmitHotelOrder();
		NorthBoundAPIServiceStub.SubmitHotelOrder submitHotelOrder=new NorthBoundAPIServiceStub.SubmitHotelOrder();
		
		// 酒店ID
		String HotelId = "00101480";
		// 房型ID
		String RoomTypeID = "0003";
		// RatePlanID
		int RatePlanID = 117553;
		String checkindate = "2012-5-28 00:00:00";
		String checkoutdate = "2012-5-31 00:00:00";
		//RatePlan名称
		String RatePlan="";
		// 入住时间

		Calendar CheckInDate =DateSwitch.SwitchCalendar(checkindate);
		// 离店时间
		Calendar CheckOutDate =DateSwitch.SwitchCalendar(checkoutdate);
		//ELONG卡号
		String ElongCardNo="";
		//宾客类型代码
		String GuestTypeCode="1";
		//房间数量
		int RoomAmount=1;
		//入住人数
		int GuestAmount=1;
		//支付方式
		String PaymentTypeCode="0";
		//最早到达时间
		String arrivalearlytime = "2012-5-28 00:00:00";

		Calendar ArrivalEarlyTime =DateSwitch.SwitchCalendar(arrivalearlytime);
		//最晚入住时间
		String arrivalatetime="2012-5-29 05:00:00";
		Calendar ArrivalLateTime = DateSwitch.SwitchCalendar(arrivalatetime);
		//货币代码
		String CurrencyCode="RMB";
		//总价 （离店日期-入住日期）*房间数*RP的会员价
		BigDecimal TotalPrice=new BigDecimal(1);
		//订单确认方式
		String ConfirmTypeCode="noneed";
		//订单确认语言
		String ConfirmLanguageCode="CN";
		//给酒店备注
		String NoteToHotel="";
		//给艺龙备注
		String NoteToElong="";
		
		//姓名
		String Name="师卫林";
		//性别代码
		String GenderCode="0";
		//Email
		String Email="";
		//手机
		String Mobile="";
		//证件类别代码
		String IdTypeCode="";
		//证件号码
		String IdNumber="";
		//国际区号
		int InterCode=0;
		//国内区号
		int AreaCode=0;
		//电话号码
		int Number=0;
		//分机号
		int Ext=0;
		
		phoneForSubmitHotelOrder.setAreaCode(AreaCode);
		phoneForSubmitHotelOrder.setExt(Ext);
		phoneForSubmitHotelOrder.setInterCode(InterCode);
		phoneForSubmitHotelOrder.setNmber(Number);
		
		faxForSubmitHotelOrder.setAreaCode(AreaCode);
		faxForSubmitHotelOrder.setExt(Ext);
		faxForSubmitHotelOrder.setInterCode(InterCode);
		faxForSubmitHotelOrder.setNmber(Number);
		
		contacterForSubmitHotelOrder.setName(Name);
		contacterForSubmitHotelOrder.setPhone(phoneForSubmitHotelOrder);
		contacterForSubmitHotelOrder.setEmail(Email);
		contacterForSubmitHotelOrder.setFax(faxForSubmitHotelOrder);
		contacterForSubmitHotelOrder.setGenderCode(GenderCode);
		contacterForSubmitHotelOrder.setIdNumber(IdNumber);
		contacterForSubmitHotelOrder.setIdTypeCode(IdTypeCode);
		contacterForSubmitHotelOrder.setMobile(Mobile);
		
		NorthBoundAPIServiceStub.ContacterForSubmitHotelOrder[] arr={contacterForSubmitHotelOrder};
		arrayOfContacterForSubmitHotelOrder.setContacter(arr);
		
		//信用卡号
		String CNumber="";
		//校验码
		String VeryfyCode="";
		//有效期年
		int ValidYear=0;
		//有效期月
		int ValidMonth=0;
		//持卡人姓名
		String CardHolderName="张三";
		
		
		cardForSubmitHotelOrder.setCardHolderName(CardHolderName);
		cardForSubmitHotelOrder.setIdNumber(IdNumber);
		cardForSubmitHotelOrder.setIdTypeCode(IdTypeCode);
		cardForSubmitHotelOrder.setNumber(CNumber);
		cardForSubmitHotelOrder.setValidMonth(ValidMonth);
		cardForSubmitHotelOrder.setValidYear(ValidYear);
		cardForSubmitHotelOrder.setVeryfyCode(VeryfyCode);
		
		//国籍
		String Nationality="";
		
		guestForSubmitHotelOrder.setEmail(Email);
		guestForSubmitHotelOrder.setFax(faxForSubmitHotelOrder);
		guestForSubmitHotelOrder.setGenderCode(GenderCode);
		guestForSubmitHotelOrder.setIdNumber(IdNumber);
		guestForSubmitHotelOrder.setIdTypeCode(IdTypeCode);
		guestForSubmitHotelOrder.setMobile(Mobile);
		guestForSubmitHotelOrder.setName(Name);
		guestForSubmitHotelOrder.setNationality(Nationality);
		guestForSubmitHotelOrder.setPhone(phoneForSubmitHotelOrder);
		
		
		NorthBoundAPIServiceStub.GuestForSubmitHotelOrder[] arry={guestForSubmitHotelOrder};
		arrayOfGuestForSubmitHotelOrder.setGuest(arry);
		
		
		roomForSubmitHotelOrder.setArrivalEarlyTime(ArrivalEarlyTime);
		roomForSubmitHotelOrder.setArrivalLateTime(ArrivalLateTime);
		roomForSubmitHotelOrder.setCheckInDate(CheckInDate);
		roomForSubmitHotelOrder.setCheckOutDate(CheckOutDate);
		roomForSubmitHotelOrder.setConfirmLanguageCode(ConfirmLanguageCode);
		roomForSubmitHotelOrder.setConfirmTypeCode(ConfirmTypeCode);
		roomForSubmitHotelOrder.setContacters(arrayOfContacterForSubmitHotelOrder);
		roomForSubmitHotelOrder.setCreditCard(cardForSubmitHotelOrder);
		roomForSubmitHotelOrder.setCurrencyCode(CurrencyCode);
		roomForSubmitHotelOrder.setElongCardNo(ElongCardNo);
		roomForSubmitHotelOrder.setGuestAmount(GuestAmount);
		roomForSubmitHotelOrder.setGuests(arrayOfGuestForSubmitHotelOrder);
		roomForSubmitHotelOrder.setGuestTypeCode(GuestTypeCode);
		roomForSubmitHotelOrder.setHotelId(HotelId);
		roomForSubmitHotelOrder.setNoteToElong(NoteToElong);
		roomForSubmitHotelOrder.setNoteToHotel(NoteToHotel);
		roomForSubmitHotelOrder.setPaymentTypeCode(PaymentTypeCode);
		roomForSubmitHotelOrder.setRatePlanCode(RatePlan);
		roomForSubmitHotelOrder.setRatePlanID(RatePlanID);
		roomForSubmitHotelOrder.setRoomAmount(RoomAmount);
		roomForSubmitHotelOrder.setRoomTypeId(RoomTypeID);
		roomForSubmitHotelOrder.setTotalPrice(TotalPrice);
		
		NorthBoundAPIServiceStub.RoomForSubmitHotelOrder[] rooms={roomForSubmitHotelOrder};
		arrayOfRoomForSubmitHotelOrder.setRoom(rooms);
		hotelOrderForSubmitHotelOrder.setRoomGroups(arrayOfRoomForSubmitHotelOrder);
		
		request.setRequestHead(ElongRequestHead.getRequestHead(""));
		request.setHotelOrder(hotelOrderForSubmitHotelOrder);
		
		submitHotelOrder.setSubmitHotelOrderRequest(request);
		responseE=stub.submitHotelOrder(submitHotelOrder);
		
		System.out.println(responseE.getSubmitHotelOrderResult().getResponseHead().getResultCode());
		System.out.println(responseE.getSubmitHotelOrderResult().getResponseHead().getResultMessage());
		NorthBoundAPIServiceStub.SubmitHotelOrderResult submitHotelOrderResult=responseE.getSubmitHotelOrderResult().getSubmitHotelOrderResult();
		//订单ID
		System.out.println(submitHotelOrderResult.getHotelOrderID());
		//最晚取消时间
		System.out.println("最晚取消时间:"+DateSwitch.SwitchString(submitHotelOrderResult.getCancelDeadline()));
		//担保金额
		System.out.println(submitHotelOrderResult.getGuaranteeMoney());
		
	}
}
