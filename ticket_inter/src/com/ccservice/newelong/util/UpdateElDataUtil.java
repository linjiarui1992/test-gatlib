package com.ccservice.newelong.util;

import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class UpdateElDataUtil {
	/**字符串是否为空*/
	public static boolean StringIsNull(String str){
		if(str == null || "".equals(str.trim()) || "null".equalsIgnoreCase(str.trim())){
			return true;
		}
		return false;
	}
	/**yyyy-MM-dd HH:mm:ss*/
	public static String getFullStringTime(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
	/**获取当前日期，格式为yyyy-MM-dd*/
	public static String getCurrentDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	
	/**指定日期yyyy-MM-dd加N天*/
	public static String getAddDate(String date,int days) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(date));
		cal.add(Calendar.DATE, days);
		return sdf.format(cal.getTime());
	}
	
	/**艺龙酒店详情访问地址*/
	public static String getELHotelDetailUrl(String hotelId){
		return "http://api.elong.com/xml/v2.0/hotel/cn/"+hotelId.substring(hotelId.length()-2, hotelId.length())+"/"+hotelId+".xml";
	}
}
