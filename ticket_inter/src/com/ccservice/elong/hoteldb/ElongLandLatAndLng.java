package com.ccservice.elong.hoteldb;

import java.net.URLEncoder;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.compareprice.HttpClient;

/**
 * 艺龙地标经纬度 
 * @author WH
 */
public class ElongLandLatAndLng {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		//地标
		List<Landmark> marks = Server.getInstance().getHotelService().findAllLandmark("where (C_LNG is null or C_LAT is null) and C_CITYID is not null and C_NAME is not null", "order by C_CITYID", -1, 0);
		for(Landmark m:marks){
			boolean flag = false;
			String link = "";
			//城市
			City city = Server.getInstance().getHotelService().findCity(m.getCityid());
			String elCityId = city.getElongcityid().length()==3 ? "0" + city.getElongcityid() : city.getElongcityid();
			if(elCityId!=null&&!"".equals(elCityId.trim())){
				try {
					String current = "jsonp" + System.currentTimeMillis();
					link = "http://hotel.elong.com/keywordssuggest.html?isNewB=false&callback="+current+"&q="+URLEncoder.encode(m.getName(),"utf-8")+"&limit=10&cityid="+elCityId+"&language=cn";
					String strReturn = HttpClient.httpget(link, "utf-8");
					if(strReturn==null || (current+"(null)").equals(strReturn) || strReturn.contains("您访问的页面不存在或暂时无法访问")){
						continue;
					}
					strReturn = strReturn.replace(current+"(", "{\""+current+"\":").replace(")", "}");
					/*
					 * {
					 * "Accept": false,
					 * "Lat": "39.96088889",
					 * "Lng": "116.45654596",
					 * "Name": "三元桥",
					 * "PropertiesId": "5605974",
					 * "Type": 999  //999为地标、1为地铁、9为酒店
					 * }, 
					 */
					JSONObject datas = JSONObject.fromObject(strReturn);
					JSONArray arys = datas.getJSONArray(current);
					for(int i = 0 ; i < arys.size() ; i++){
						JSONObject obj = (JSONObject) arys.get(i);
						//String type = obj.getString("Type");
						String name = obj.getString("Name");
						String lat = obj.getString("Lat");
						String lng = obj.getString("Lng");
						if(name!=null && (name.equals(m.getName()))){
							if(lat!=null && !"".equals(lat.trim()) && lng!=null && !"".equals(lng.trim()) && Double.parseDouble(lat)>0 && Double.parseDouble(lng)>0){
								System.out.println("更新："+elCityId+"---"+name);
								m.setLat(Double.parseDouble(lat));
								m.setLng(Double.parseDouble(lng));
								Server.getInstance().getHotelService().updateLandmarkIgnoreNull(m);
							}
							flag = true;
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("更新："+elCityId+"---"+m.getName()+"---出现异常，异常信息为："+e.getMessage());
				} finally{
					if(!flag){
						System.out.println("更新："+elCityId+"---"+m.getName()+"---失败---请求地址："+link);
					}
				}
			}
		}
	}
}
