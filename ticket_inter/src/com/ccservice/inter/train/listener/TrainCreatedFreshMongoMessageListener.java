package com.ccservice.inter.train.listener;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import net.sf.json.JSONObject;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.train.TrainCreatedFreshMongo;
import com.ccservice.qunar.util.ExceptionUtil;

public class TrainCreatedFreshMongoMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        try {
            String orderNoticeResult = ((TextMessage) message).getText();
            WriteLog.write("下单后的价格刷新mongo_MessageListener", System.currentTimeMillis() + ":orderNoticeResult:"
                    + orderNoticeResult);
            JSONObject jsonObject = JSONObject.fromObject(orderNoticeResult);
            new TrainCreatedFreshMongo().freshMongoByMq(jsonObject);
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("ERROR_下单后的价格刷新mongo_MessageListener", e);
        }
    }
}
