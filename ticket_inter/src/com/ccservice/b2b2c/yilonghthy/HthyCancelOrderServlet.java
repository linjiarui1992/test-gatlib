package com.ccservice.b2b2c.yilonghthy;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccservice.b2b2c.atom.component.WriteLog;


public class HthyCancelOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HthyCancelOrderServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderid=request.getParameter("orderid");
		System.out.println("对艺龙进行取消订单操作:orderid="+orderid);
		WriteLog.write("ticket_inter请求艺龙取消订单操作", "orderid="+orderid);
		String result = new HthyCancelOrder().cancelOrder(orderid);
		PrintWriter out = response.getWriter();
		try {
			out.print(result);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
