package com.ccservice.offlineExpress.util;

import java.util.Random;

public class CommonUtil {

    /**
     * 字符串是否为空
     */
    public static boolean StringIsEmpty(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * 随机数
     */
    public static int randomNum() {
        return new Random().nextInt(9000000) + 1000000;
    }

}