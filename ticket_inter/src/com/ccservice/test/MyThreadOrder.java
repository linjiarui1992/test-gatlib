package com.ccservice.test;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.util.ActiveMQUtil;

public class MyThreadOrder extends Thread {

    private String orderid;

    public MyThreadOrder(String orderid) {
        this.orderid = orderid;
    }

    @Override
    public void run() {
        JSONObject jsoseng = new JSONObject();
        jsoseng.put("orderid", orderid);
        System.out.println(jsoseng.toJSONString());
        ActiveMQUtil.sendMessage("tcp://120.26.100.206:61616", "QueueMQ_trainorder_waitorder_orderid", orderid);
    }
}
