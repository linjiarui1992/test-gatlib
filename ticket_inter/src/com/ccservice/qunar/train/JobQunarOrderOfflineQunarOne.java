package com.ccservice.qunar.train;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.interticket.HttpClient;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;
import com.tenpay.util.MD5Util;

public class JobQunarOrderOfflineQunarOne implements Job{
	
	private long qcyg_agentid;
	
	private String qunarPayurl;

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {

	        String sql_trainofflinequnaragents = "select createUCode,hmac from TrainOfflineQunarAgents WHERE Name ='qcltone'";
	        List list=Server.getInstance().getSystemService().findMapResultBySql(sql_trainofflinequnaragents, null);
	        String merchantCode="";
	        String HMAC="";
	        if(list.size()>0){
	        	Map map=(Map)list.get(0); 
	        	merchantCode=map.get("createUCode").toString();
	        	HMAC=map.get("hmac").toString();
	        	HMAC=MD5Util.MD5Encode(HMAC+merchantCode+"WAIT_TICKET", "UTF-8").toUpperCase();
	        } 
	        try {
	            String url = HttpClient
	                    .httpget(
	                            "http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="+merchantCode+"&type=WAIT_TICKET&HMAC="+HMAC,
	                            "UTF-8");
	            WriteLog.write("JobQunarOrderOfflineQunarOne_json拉单json", "请求地址:"+"http://api.pub.train.qunar.com/api/pub/QueryOrders.do?&merchantCode="+merchantCode+"&type=WAIT_TICKET&HMAC="+HMAC+"-->josn:" + url);
	            Classification(JSONObject.parseObject(url));
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
	}
	
	public void Classification(JSONObject jsonObject) {
        //        getCommon();
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            WriteLog.write("JobQunarOrderOfflineQunarOne", jsonObject.toString());
            String orderNos = allOrderNos(data);
            WriteLog.write("JobQunarOrderOfflineQunarOne", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
                                                        isoldorder = true;
                            break;
//                                                        exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setOrderjson(info);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }
	
	// 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
    	System.out.println("Startqclt1QunarLadan");
        JobDetail jobDetail = new JobDetail("JobQunarOrderOfflineQunarOne", "JobQunarOrderOfflineQunarOneGroup",
        		JobQunarOrderOfflineQunarOne.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("JobQunarOrderOfflineQunarOne","JobQunarOrderOfflineQunarOneGroup",
                expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }
	
	public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;

        t1 = new MyThreadQunarOrderOfflineQunarOne(qunarordermethod, this.qcyg_agentid, this.qunarPayurl);
        pool.execute(t1);

        pool.shutdown();
    }
	
	public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
	public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }

}
