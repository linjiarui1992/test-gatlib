package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.train.mqlistener.TrainorderDeductionListenerGq;

/**
 * 
 * @time 2015年2月10日 下午11:05:57
 * @author fiend
 */
/**
 * 扣款队列
 * 
 * @time 2015年4月28日 下午4:18:00
 * @author chendong
 */

@SuppressWarnings("serial")
public class QueueMQ_GQTrainDeduction extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    @Override
    public void init() throws ServletException {
        super.init();
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        if ("1".equals(isstart)) {
            System.out.println("改签扣款mq:开启");
            orderNotice();
        }
        else {
            System.out.println("改签扣款mq:关闭");
        }
    }

    public void orderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(new TrainorderDeductionListenerGq());
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
