package com.ccservice.inter.job.Util;

import java.io.InputStream;
import java.io.PrintStream;

import org.apache.commons.net.telnet.TelnetClient;

public class NetTelnet {
    public static void main(String[] args) {
        Long l1 = System.currentTimeMillis();
        try {
            String mail = "qwe10000002";
            String mailpassword = "123456";
            String ip = "123.56.102.188";
            int port = 4555;
            String user = "root";//whzf011843adm
            String password = "root";//5n0wbIrdsMe3
            NetTelnet telnet = new NetTelnet(ip, port, user, password);
            String cmdcode = "adduser wangaipqar 123456";
            String r4 = telnet.sendCommand(cmdcode, "added", "Error adding user " + mail);
            System.out.println(r4);
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private TelnetClient telnet = new TelnetClient();

    private InputStream in;

    private PrintStream out;

    // 普通用户结束  
    public NetTelnet(String ip, int port, String user, String password) {
        try {
            telnet.connect(ip, port);
            in = telnet.getInputStream();
            out = new PrintStream(telnet.getOutputStream());
            // 根据root用户设置结束符  
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** * 登录 * * @param user * @param password */
    public boolean login(String user, String password) {
        String str = "";
        str = readUntil("Login id:", "ddddddddjjjj");
        write(user);
        str = readUntil("Password:", "ddddddddjjjj");
        write(password);
        str = readUntil("commands", "Login id:");
        if (str.equals("false")) {
            return false;
        }
        return true;
    }

    /** * 读取分析结果 * * @param pattern * @return */
    public String readUntil(String pattern, String pattern2) {
        try {
            char lastChar = pattern.charAt(pattern.length() - 1);
            StringBuffer sb = new StringBuffer();
            char ch = (char) in.read();
            int i = 0;
            while (i < 100) {
                sb.append(ch);
                if (ch == lastChar) {
                    if (sb.toString().endsWith(pattern)) {
                        return sb.toString();
                    }
                }

                if (sb.toString().endsWith(pattern2)) {
                    return sb.toString().trim();
                }
                ch = (char) in.read();
                i++;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /** * 写操作 * * @param value */
    public void write(String value) {
        try {
            out.println(value);
            out.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** * 向目标发送命令字符串 * * @param command * @return */
    public String sendCommand(String command, String endstr, String endstr2) {
        try {
            write(command);
            return readUntil(endstr, endstr2);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /** * 关闭连接 */
    public void disconnect() {
        try {
            telnet.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}