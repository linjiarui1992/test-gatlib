package com.ccservice.inter.job.train;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.policy.ben.RepServerBean;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;

public class JobIndexAgain_RepUtil {

    private JobIndexAgain_RepUtil() {
    }

    private static final JobIndexAgain_RepUtil single = new JobIndexAgain_RepUtil();

    //单例
    public static JobIndexAgain_RepUtil getInstance() {
        return single;
    }

    public static void main(String[] args) {
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
        System.out.println(JobIndexAgain_RepUtil.getInstance().getRepUrl(false));
    }

    /**
     * 获取REP地址，用于登录等
     * @param isDama 是否是打码
     */
    public synchronized String getRepUrl(boolean isDama) {
        RepServerBean rep = GetRep(isDama);
        return rep.getUrl();
    }

    /**
     * 获取REP，用于下单等
     * @param isDama 是否是打码
     */
    public synchronized JSONObject getRepServer(boolean isDama) {
        //REP
        RepServerBean rep = GetRep(isDama);
        //JSON
        JSONObject result = new JSONObject();
        result.put("id", rep.getId());
        result.put("name", rep.getName());
        result.put("url", rep.getUrl());
        result.put("free", "1".equals(PropertyUtil.getValue("needFreeRep")) ? true : false);
        return result;
    }

    /**字符串是否为空*/
    public static boolean StringIsNull(String str) {
        if (str == null || "".equals(str.trim())) {
            return true;
        }
        return false;
    }

    /**
     * 释放REP
     * @param isDama
     */
    public synchronized void FreeRep(boolean isDama, String repUrl) {
        if (StringIsNull(repUrl)) {
            return;
        }
        if (!"1".equals(PropertyUtil.getValue("needFreeRep"))) {
            return;
        }
        List<RepServerBean> RepServers = RepServerList(false);
        Iterator<RepServerBean> iterator = RepServers.iterator();
        while (iterator.hasNext()) {
            RepServerBean rep = iterator.next();
            if (rep != null && repUrl.equals(rep.getUrl())) {
                //当前使用
                int use = isDama ? rep.getDamaUse() : rep.getTrainUse();
                use = use > 1 ? use - 1 : 0;
                //更新使用
                if (isDama) {
                    rep.setDamaUse(use);
                }
                else {
                    rep.setTrainUse(use);
                }
                break;
            }
        }
    }

    private RepServerBean GetRep(boolean isDama) {
        //原REP序列号
        long oldIdx = 0l;
        //打码REP
        if (isDama) {
            oldIdx = Server.getInstance().getDamaRepIdx();
            Server.getInstance().setDamaRepIdx(oldIdx + 1);
        }
        else {
            oldIdx = Server.getInstance().getTrainRepIdx();
            Server.getInstance().setTrainRepIdx(oldIdx + 1);

        }
        //REP服务器
        List<RepServerBean> RepServers = RepServerList(false);
        //REP服务器长度
        int size = RepServers.size();
        //无REP服务器
        if (size == 0) {
            return new RepServerBean();
        }
        int current = (int) (oldIdx % size);
        return CurrentRep(current, RepServers, isDama);
    }

    /**
     * 获取REP服务器
     * @param current 当前REP
     * @param RepServers REP服务器
     */
    private RepServerBean CurrentRep(int current, List<RepServerBean> RepServers, boolean isDama) {
        RepServerBean result = RepServers.get(current);
        //下单REP使用+1
        if (isDama) {
            result.setDamaUse(result.getDamaUse() + 1);
        }
        else {
            result.setTrainUse(result.getTrainUse() + 1);
        }
        result.setLastTime(new Timestamp(System.currentTimeMillis()));
        //返回REP结果
        return result;
    }

    /**
     * 下单REP服务器
     * @param isInit 是否是初始化REP
     */
    @SuppressWarnings("rawtypes")
    public List<RepServerBean> RepServerList(boolean isInit) {
        List<RepServerBean> RepServers = Server.getInstance().getRepServers();
        // 不存在REP或初始化
        boolean NoRep = false;
        if (RepServers == null || RepServers.size() == 0 || isInit) {
            NoRep = true;
            RepServers = new ArrayList<RepServerBean>();
        }
        String repIpString = PropertyUtil.getValue("repIpString", "train.properties");
        String[] repUrls = repIpString.split("[|]");
        List<RepServerBean> newRepList = new ArrayList<RepServerBean>();
        for (int i = 0; i < repUrls.length; i++) {
            // 最后交互时间
            Timestamp lastTime = new Timestamp(System.currentTimeMillis());
            // NEW
            RepServerBean rep = new RepServerBean();
            // SET
            rep.setId(i);
            rep.setUrl(repUrls[i]);
            rep.setName("L" + i);
            rep.setLastTime(lastTime);
            // ADD
            newRepList.add(rep);
            // UPDATE
            RepServers = newRepList;
            Server.getInstance().setRepServers(newRepList);
        }
        // 重新查询数据库
        return RepServers;
    }
}