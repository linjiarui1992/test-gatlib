package com.ccservice.mobileCode;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.policy.SendPostandGet;
import java.io.PrintStream;

public class HangTianMobileCode implements IMobileCode {
    static final String URL = "http://hangTianMobileCode.hangtian123.net/";

    static final String GETMOBILENUMURL = "http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt";

    static final String ADDIGNORELISTURL = "http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt?method=SIM_CARD_NUMBER";

    public static void main(String[] args) {
        String result = "";
        String mobile = "";
        HangTianMobileCode hangTianMobileCode = getInstance("");

        mobile = "13630231374";
        System.out.println(mobile);

        main_getVcodeAndReleaseMobile(hangTianMobileCode, mobile);

        System.out.println(result);
    }

    private static void main_getVcodeAndReleaseMobile(HangTianMobileCode hangTianMobileCode, String mobile) {
        for (int i = 0; i < 200; ++i) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception localException) {
            }
            String vocde_mobile = hangTianMobileCode.getVcodeAndReleaseMobile("", mobile, "", "", "");
            System.out.println(i + ":" + mobile + ":" + vocde_mobile);
            if (!("no_data".equals(vocde_mobile)))
                return;
        }
    }

    public static HangTianMobileCode getInstance(String pid) {
        String uid = "";
        HangTianMobileCode tonghangmobilecode = new HangTianMobileCode();
        return tonghangmobilecode;
    }

    public String LoginIn(String uid, String pwd) {
        return null;
    }

    public String GetMobilenum(String uid, String pid, String token) {
        String result = "";
        for (int i = 0; i < 200; ++i) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("method", "SIM_CARD_NUMBER");
            String paramContent = "jsonStr=" + jsonObject.toJSONString();
            result = SendPostandGet.submitPost("http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt",
                    paramContent, "utf-8").toString();

            WriteLog.write("HangTianMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
            JSONObject jsonObject2 = JSONObject.parseObject(result);
            if (("100".equals(jsonObject2.getString("code")))
                    && (!("no_data".equals(jsonObject2.getString("phonenumber"))))) {
                String phonenumber = jsonObject2.getString("phonenumber");
                if ((phonenumber == null) || (phonenumber.trim().length() != 11))
                    break;
                if (phonenumber.trim().substring(0, 3).equals("170")
                        || phonenumber.trim().substring(0, 3).equals("147")) {
                    WriteLog.write("HangTianMobileCode:170或147开头的手机号", ":" + phonenumber);
                    phonenumber = "";
                    continue;
                }
                else if (phonenumber != null && !"".equals(phonenumber)) {
                    result = phonenumber.trim();
                    break;
                }
            }
            if ("101".equals(jsonObject2.getString("code"))) {
                System.out.println(i + ":停2秒:" + result);
                try {
                    Thread.sleep(2000L);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        return null;
    }

    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", "SMS_CONTENT");
        jsonObject.put("phonenumber", mobile);
        String paramContent = "jsonStr=" + jsonObject.toJSONString();
        String result = SendPostandGet.submitPost("http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt",
                paramContent, "utf-8").toString();
        WriteLog.write("HangTianMobileCode_getVcodeAndReleaseMobile", paramContent + ":GetMobilenum:" + result);
        if ((result != null) && (result.length() > 0)) {
            JSONObject jsonObject2 = JSONObject.parseObject(result);
            if ("100".equals(jsonObject2.getString("code"))) {
                String content = jsonObject2.getString("content");
                if (content != null)
                    result = content.trim();
            }
            else {
                result = "no_data";
            }
        }
        else {
            result = "no_data";
        }
        return result;
    }

    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", "UNAVAILABLE_SIM_CARD");
        jsonObject.put("phonenumber", mobiles);
        String paramContent = "jsonStr=" + jsonObject.toJSONString();
        String result = SendPostandGet.submitPost("http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt",
                paramContent, "utf-8").toString();
        WriteLog.write("HangTianMobileCode_AddIgnoreList", paramContent + ":GetMobilenum:" + result);
        JSONObject jsonObject2 = JSONObject.parseObject(result);
        if ("100".equals(jsonObject2.getString("code"))) {
            String content = jsonObject2.getString("content");
            if (content != null)
                result = content.trim();
        }
        else {
            result = "no_data";
        }
        return result;
    }

    public String GetRecvingInfo(String uid, String pid, String token) {
        return null;
    }

    public String CancelSMSRecvAll(String uid, String token) {
        return null;
    }

    public String CancelSMSRecv(String uid, String mobile, String token) {
        return null;
    }

    public String send(String mobile, String content) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", "SMS_SEND");
        jsonObject.put("phonenumber", mobile);
        jsonObject.put("content", content);
        String paramContent = "jsonStr=" + jsonObject.toJSONString();
        String result = SendPostandGet.submitPost("http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt",
                paramContent, "utf-8").toString();
        WriteLog.write("HangTianMobileCode_send", paramContent + ":GetMobilenum:" + result);
        JSONObject jsonObject2 = JSONObject.parseObject(result);
        if ("100".equals(jsonObject2.getString("code"))) {
            boolean content1 = jsonObject2.getBooleanValue("success");
            if (content1) {
                try {
                    Thread.sleep(10000L);
                }
                catch (Exception localException) {
                }
                result = "true";
            }
            else {
                result = "false";
            }
        }
        else {
            result = "false";
        }
        return result;
    }

    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("method", "FREE_SIM_CARD");
        jsonObject.put("phonenumber", mobiles);
        String paramContent = "jsonStr=" + jsonObject.toJSONString();
        String result = SendPostandGet.submitPost("http://hangTianMobileCode.hangtian123.net/HTHYPhoneSimServelt",
                paramContent, "utf-8").toString();
        WriteLog.write("HangTianMobileCode_ReleaseMobile", paramContent + ":GetMobilenum:" + result);
        JSONObject jsonObject2 = JSONObject.parseObject(result);
        if ("100".equals(jsonObject2.getString("code"))) {
            boolean content1 = jsonObject2.getBooleanValue("success");
            if (content1) {
                result = "true";
            }
            else
                result = "false";
        }
        else {
            result = "false";
        }
        return result;
    }
}