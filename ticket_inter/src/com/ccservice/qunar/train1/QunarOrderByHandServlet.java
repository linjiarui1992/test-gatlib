package com.ccservice.qunar.train1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

public class QunarOrderByHandServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        PrintWriter out = null;
        String param = "";
        JSONObject result = new JSONObject();
        try {
        	out = response.getWriter();
        	param = request.getParameter("jsonStr");
        	if (param == null || "".equals(param)) {
				result.put("success", false);
				result.put("msg", "参数为空");
			} else {
				JSONObject json = JSONObject.parseObject(param);
				
			}
        	
		} catch (Exception e) {
			
		} finally {
			if (out != null) {
                out.print(result);
                out.flush();
                out.close();
            }
		}
	}
	
	public void Classification(JSONObject jsonObject) {
        //        getCommon();
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
//            WriteLog.write("JobQunarOrderOffline", jsonObject.toString());
            String orderNos = allOrderNos(data);
//            WriteLog.write("JobQunarOrderOffline", "orderNos:" + orderNos);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                            qunarordermethod.setOrderjson(info);
//                                                        isoldorder = true;
                            break;
//                                                        exThread(qunarordermethod);
                        }
                    }
                    if (!isoldorder) {
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        //                        qunarordermethod.setQunarordernumber(orderNo);
                        //                        qunarordermethod.setId(0L);
                        //                        qunarordermethod.setOrderstatus(2);
                        //                        qunarordermethod.setCreatetime("");
                        //                        qunarordermethod.setIsquestionorder(0);
                        //                        qunarordermethod.setState12306(1);
                        qunarordermethod.setOrderjson(info);
                        //                        qunarordermethod.setInterfaceType(2);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }
	
    public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
    
    public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        //        WriteLog.write("JobQunarOrder", "sql_trainorder:" + sql_trainorder);
        //        WriteLog.write("JobQunarOrder", "list.size:" + list.size());
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            //            qunarordermethod.setOrderstatus(Integer.valueOf(map.get("C_ORDERSTATUS").toString()).intValue());
            //            qunarordermethod.setCreatetime(map.get("C_CREATETIME").toString());
            //            qunarordermethod.setIsquestionorder(Integer.valueOf(map.get("C_ISQUESTIONORDER").toString()).intValue());
            //            try {
            //                qunarordermethod.setState12306(Integer.valueOf(map.get("C_STATE12306").toString()).intValue());
            //            }
            //            catch (NumberFormatException e) {
            //                qunarordermethod.setState12306(0);
            //            }
            //            qunarordermethod.setOrderjson(new JSONObject());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }
    
    public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;
        //        WriteLog.write(
        //                "JobQunarOrder",
        //                "qunar订单号:"
        //                        + qunarordermethod.getQunarordernumber()
        //                        + "===当前订单ID:"
        //                        + (qunarordermethod.getId() == 0L ? "===不存在" : new StringBuilder(String
        //                                .valueOf(qunarordermethod.getId())).append("===当前订单状态:")
        //                                .append(qunarordermethod.getOrderstatus()).append("===当前订单问题订单状态:")
        //                                .append(qunarordermethod.getIsquestionorder()).append("===当前订单12306状态:")
        //                                .append(qunarordermethod.getState12306()).append("===当前订单创建时间")
        //                                .append(qunarordermethod.getCreatetime()).toString()));

        t1 = new MyThreadQunarOrderOfflineByHand(qunarordermethod, 54);
        pool.execute(t1);

        pool.shutdown();
//        WriteLog.write("JobQunarOrderOffline", "qunar订单号:" + qunarordermethod.getQunarordernumber() + "线程关闭");
    }

}
