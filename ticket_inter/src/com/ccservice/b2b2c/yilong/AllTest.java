package com.ccservice.b2b2c.yilong;


import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

public class AllTest {
	private static DBoperationUtil dt = new DBoperationUtil();

	public static void main(String[] args) {
		String str="<?xml version=\"1.0\" encoding=\"UTF-8\" ?><OrderRequest><Authentication><ServiceName>order.addOrder</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2015-07-22 15:29:22</TimeStamp><MessageIdentity>4cad761817839dda5d9ec5f691563662</MessageIdentity></Authentication><TrainOrderService><OrderNumber>2015072215292295733</OrderNumber><ChannelName>360_search</ChannelName><OrderTicketType>0</OrderTicketType><Order><OrderTime>2015-07-22 15:29:22</OrderTime><OrderMedia>pc</OrderMedia><TicketOffsetFee>0</TicketOffsetFee><Insurance>N</Insurance><Invoice>N</Invoice><PrivateCustomization>1</PrivateCustomization><TicketInfo><TicketItem><FromStationName>上海</FromStationName><ToStationName>汉口</ToStationName><TicketTime>2015-07-27 22:15</TicketTime><TrainNumber>K1152</TrainNumber><TicketPrice>122</TicketPrice><TicketCount>2</TicketCount><AuditTicketCount>2</AuditTicketCount><ChildTicketCount>0</ChildTicketCount><SeatName>硬座</SeatName><AcceptSeat>必须连号，否则无票，出错会造成赔款!</AcceptSeat><passport>张志梦,身份证,42128119871020656X,成人票,1987-10-20,0|卢柳灿,身份证,352229198206214533,成人票,1982-06-21,0</passport><OrderPrice>274</OrderPrice></TicketItem></TicketInfo></Order><User><UserID>54224353</UserID><UserName>张志梦</UserName><UserTel>--</UserTel><userLoginName>13564818486</userLoginName><UserMobile>13564818486</UserMobile></User><DeliverInfo><AreaID>14</AreaID><address>鹤望路365弄65号501室</address><OrderDeliverTypeID>440</OrderDeliverTypeID><DeliverPrice>20</DeliverPrice><WeekendDeliver>N</WeekendDeliver></DeliverInfo></TrainOrderService></OrderRequest>";
			try {
			Document document = DocumentHelper.parseText(str);
			Element root = document.getRootElement();
			//Authentication
			Element Authentication=root.element("Authentication");
			String serviceName=Authentication.elementText("ServiceName");//接口服务名
			String partnerName=Authentication.elementText("PartnerName");//合作方名称
			String timeStamp=Authentication.elementText("TimeStamp");//时间
			String messageIdentity=Authentication.elementText("MessageIdentity");//验证信息
			//TrainOrderService
			Element trainOrderService=root.element("TrainOrderService");
			String orderNumber=trainOrderService.elementText("OrderNumber");//订单号
			String channelName=trainOrderService.elementText("ChannelName");//渠道
			String orderTicketType=trainOrderService.elementText("OrderTicketType");//车票类型 0普通票，1往返票
			//Order
			Element Order=trainOrderService.element("Order");
			String orderTime=Order.elementText("OrderTime");//下单时间
			String orderMedia=Order.elementText("OrderMedia");//下单类型  pc , wap 默认 pc
			String ticketOffsetFee=Order.elementText("TicketOffsetFee");//优惠券金额
			String insurance=Order.elementText("Insurance");//是否是保险
			String invoice=Order.elementText("Invoice");//是否需要发票
			String privateCustomization=Order.elementText("PrivateCustomization");//0不是私人定制，1是私人定制
			//TicketInfo
			Element TicketInfo=Order.element("TicketInfo");
			//TicketItem
			Element ticketItem=TicketInfo.element("TicketItem");
			String fromStationName=ticketItem.elementText("FromStationName");//出发站
			String toStationName=ticketItem.elementText("ToStationName");//到达站
			String ticketTime=ticketItem.elementText("TicketTime");//车票时间,发车时间
			String trainNumber=ticketItem.elementText("TrainNumber");//车次
			String ticketPrice=ticketItem.elementText("TicketPrice");//票面价格
			String ticketCount=ticketItem.elementText("TicketCount");//数量
			String auditTicketCount=ticketItem.elementText("AuditTicketCount");//成人数量
			String childTicketCount=ticketItem.elementText("ChildTicketCount");//儿童数量
			String seatName=ticketItem.elementText("SeatName");//首选坐席
			String acceptSeat=ticketItem.elementText("AcceptSeat");//可接受席位和私人定制话术共用此节点 例子:普通票可接受席位(无座、硬卧上) 私人定制话术:优先下铺，可接受中上铺
			String passport=ticketItem.elementText("passport");//证据信息(张志梦,身份证,42128119871020656X,成人票,1987-10-20,0|)
			String orderPrice=ticketItem.elementText("OrderPrice");//订单总价格 Float
			//User
			Element User=trainOrderService.element("User");
			String userID=User.elementText("UserID");//用户id
			String userName=User.elementText("UserName");//收件人姓名
			String userTel=User.elementText("UserTel");//固定电话
			String userLoginName=User.elementText("userLoginName");//用户登录帐号
			String userMobile=User.elementText("UserMobile");//用户手机号码
			//DeliverInfo
			Element DeliverInfo=trainOrderService.element("DeliverInfo");
			String areaID=DeliverInfo.elementText("AreaID");//地区代码
			String address=DeliverInfo.elementText("address");//配送地址
			String orderDeliverTypeID=DeliverInfo.elementText("OrderDeliverTypeID");//规则ID
			String deliverPrice=DeliverInfo.elementText("DeliverPrice");//物流价格
			String weekendDeliver=DeliverInfo.elementText("WeekendDeliver");//周末配送标志 Y/N Y代表周末进行配送
			System.out.println(areaID+"--"+address+"--"+orderDeliverTypeID+"--"+deliverPrice+"--"+weekendDeliver);
			
			
			
	            Trainorder trainorder = new Trainorder();// 创建订单
	            trainorder.setTaobaosendid("");//备选坐席
	                trainorder.setOrdertype(2);// 订单类型（1、线上 2、线下）默认为1
	            trainorder.setQunarOrdernumber(orderNumber);// 订单号
	            List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
	            String[] passengers=passport.split("[|]");
	            for (int i = 0; i < passengers.length; i++) {
	                List<Trainticket> traintickets = new ArrayList<Trainticket>();
	                String[] passenger=passengers[i].split(",");
	                String certNoValue = passenger[2];// 乘车人证件号码
	                // 1：身份证；C：港澳通行证；G：台湾通行证；B：护照
	                String certTypeValue = passenger[1];// 乘车人证件种类
	                if ("身份证".equals(certTypeValue)) {
	                    certTypeValue = "1";
	                }else if ("B".equals(certTypeValue)) {
	                    certTypeValue = "3";
	                }
	                else if ("C".equals(certTypeValue)) {
	                    certTypeValue = "4";
	                }
	                else if ("G".equals(certTypeValue)) {
	                    certTypeValue = "5";
	                }
	                String birthday="";
	                if (certNoValue.length() == 18) {
	                    String year = certNoValue.substring(6, 10);
	                    String month = certNoValue.substring(10, 12);
	                    String day = certNoValue.substring(12, 14);
	                    birthday = year + "-" + month + "-" + day;
	                }
	                String nameValue = passenger[0];// 乘车人姓名
	                int ticketTypeValue =1;
	                if("儿童票".equals(passenger[3])){
	                	ticketTypeValue=0;
	                }else if("成人票".equals(passenger[3])){
	                	ticketTypeValue=1;
	                	
	                }else if("学生票".equals(passenger[3])){
	                	ticketTypeValue=2;
	                	
	                }
	               
	                // TODO 普通订单存入数据库
	                Trainticket trainticket = new Trainticket();
	                Trainpassenger trainpassenger = new Trainpassenger();
	                trainticket.setArrival(toStationName);// 存入终点站
	                trainticket.setDeparture(fromStationName);// 存入始发站
	                trainticket.setStatus(Trainticket.WAITISSUE);
	                // 无法存入剩余票
	                trainticket.setSeq(0);// 存入订单类型
	                trainticket.setTickettype(ticketTypeValue);// 存入票种类型
	                trainticket.setPayprice(Float.parseFloat(ticketPrice));//票价
	                trainticket.setPrice(Float.parseFloat(ticketPrice));// 应付火车票价格
	                trainticket.setArrivaltime("");// 火车到达时间
	                trainticket.setTrainno(trainNumber); // 火车编号
	                trainticket.setDeparttime(ticketTime);// 火车出发时间(2015-07-27 22:15)
	                trainticket.setIsapplyticket(1);
	                trainticket.setRefundfailreason(0);
	                trainticket.setCosttime("");// 历时
	                trainticket.setSeattype(seatName);// 车票坐席
	                int isInsurance;//是否是保险(1需要，0不需要)
	                if("N".equals(insurance)){
	                	isInsurance=0;
	                }else{
	                	isInsurance=1;
	                }
	                trainticket.setInsurenum(1);
	                trainticket.setInsurprice(0f);// 采购支付价
	                trainticket.setInsurorigprice(0f);// 保险原价
	                traintickets.add(trainticket);
	                trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
	                trainpassenger.setIdtype(Integer.parseInt(certTypeValue));// 存入乘车人证件种类
	                trainpassenger.setName(nameValue);// 存入乘车人姓名
	                trainpassenger.setBirthday(birthday);
	                trainpassenger.setChangeid(0);
	                trainpassenger.setAduitstatus(1);
	                trainpassenger.setTraintickets(traintickets);
	                trainplist.add(trainpassenger);
	            }
	            //纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
	            trainorder.setPaymethod(Integer.parseInt("0"));
	            //当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
	            trainorder.setSupplypayway(Integer.parseInt("1"));
	            //至少接受下铺/靠窗/连坐数量
	            trainorder.setRefuseaffirm(Integer.parseInt("0"));
	            trainorder.setContactuser(userName);//联系人
	            trainorder.setContacttel(userMobile);//联系电话
	            trainorder.setTicketcount(trainplist.size());
	            trainorder.setCreateuid(53l);//52去哪，53艺龙
	            trainorder.setInsureadreess(address);//邮寄地址

	            trainorder.setOrderprice(Float.parseFloat(orderPrice));//订单总价
	            trainorder.setIsjointtrip(0);
	            trainorder.setAgentid(378);
	            trainorder.setState12306(Trainorder.WAITORDER);
	            trainorder.setPassengers(trainplist);
	            trainorder.setOrderstatus(Trainorder.WAITISSUE);
	            trainorder.setAgentprofit(0f);
	            trainorder.setCommission(0f);
	            trainorder.setTcprocedure(0f);
	            trainorder.setInterfacetype(2);
	            trainorder.setCreateuser("艺龙");
	                    trainorderofflineadd(trainorder);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}
	/**
     * 添加火车票线下订单
     */
    public static void trainorderofflineadd(Trainorder trainorder) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        //        String addresstemp = "河北省,石家庄市,长安区,就业服务大厦";
        String addresstemp = trainorder.getInsureadreess();
        if (addresstemp.contains("省") && addresstemp.contains("市")) {
            String province = addresstemp.substring(0, addresstemp.indexOf("省"));
            String city = addresstemp.substring(addresstemp.indexOf("省") + 1, addresstemp.indexOf("市"));
            String region = addresstemp.substring(addresstemp.indexOf("市") + 1);
            addresstemp = province + "," + city + "," + region;
        }
        // 通过订单邮寄地址匹配出票点
        BaiDuMapApi bd = new BaiDuMapApi();
        //        String agentidtemp = bd.getAgentidByInsuraddress(getAllListByAdd(addresstemp, "-1"), addresstemp);
//        String agentidtemp = distribution1(addresstemp);
        String agentidtemp = "378";
        WriteLog.write("线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());

        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        // String insureaddress=trainorder.getInsureadreess();
        //        String insureaddress = "郑宇凤^^^河北^^^石家庄市^^^长安区^^^河北省石家庄市长安区裕华东路99号就业服务大厦^^^050000^^^18819411570^^^";
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        //        address.setProvinceName(splitadd[0]);
        //        address.setCityName(splitadd[1]);
        //        address.setRegionName(splitadd[2]);
        //        address.setTownName("桥东区政府");
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime() + ":00"));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime());
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }
    public static String distribution1(String address1) {
		String resultp="";
		String agentp="378";
		int num=Integer.parseInt(PropertyUtil.getValue("allAgentNum", "train.properties"));
		boolean flag=false;
		for (int i = 1; i <= num; i++) {
			String agents=PropertyUtil.getValue("AgentId"+i, "train.properties");
			String provinces=PropertyUtil.getValue("MailProvince"+i, "train.properties");
			String resultNames="";
			try {
				resultNames = new String(provinces.getBytes("ISO-8859-1"),"utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String[] add=resultNames.split(",");
			for (int j = 0; j < add.length; j++) {
				if (address1.startsWith(add[j])) {
					resultp=add[j];
					agentp=agents;
					flag=true;
				}
				if(flag){
					break;
				}
			}
			if(flag){
				break;
			}
		}
		return agentp;
    }
	public static void main2(String[] args) {
		//下单请求接口调用Response
		Document document=DocumentHelper.createDocument();
		Element orderResponse=document.addElement("OrderResponse");
		Element serviceName=orderResponse.addElement("ServiceName");
		serviceName.setText("order.addOrder");
		Element status=orderResponse.addElement("Status");
		status.setText("SUCCESS");
		Element channel=orderResponse.addElement("Channel");
		channel.setText("Jingyan");
		Element orderNumber=orderResponse.addElement("OrderNumber");
		orderNumber.setText("JingyanT20150825100014");
		Element discription=orderResponse.addElement("Discription");
		discription.setText("新增订单成功.... ");
		System.out.println(document.asXML());
	}

}
