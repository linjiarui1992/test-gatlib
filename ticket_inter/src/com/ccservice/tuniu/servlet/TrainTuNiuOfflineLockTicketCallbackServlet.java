package com.ccservice.tuniu.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.SignUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offline.util.TuNiuDesUtil;
import com.ccservice.tuniu.service.TrainTuNiuOfflineCancelLockTicket;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineLockOrderCallbackServlet
 * @description: TODO - 途牛线下票锁票回调接口
 * @author: 郑州-技术-陈亚峰  E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:57:36 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTuNiuOfflineLockTicketCallbackServlet extends HttpServlet {
    private static final String LOGNAME = "途牛线下票锁票回调接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));  
        String line = null;  
        StringBuilder sb = new StringBuilder();  
        while((line = br.readLine())!=null){  
            sb.append(line);  
        }  
   
        // 将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调信息-reqBody-->"+reqBody);

        //结果的返回
        JSONObject resResult = new JSONObject();
        Boolean resFlag = false;
        Integer errorCode = 231099;//success true用231000，false用231099
        
        PrintWriter out = response.getWriter();

        String partnerid = PropertyUtil.getValue("tuniu.account", "Train.GuestAccount.properties");
        String key = PropertyUtil.getValue("tuniu.desKey", "Train.GuestAccount.properties");

        JSONObject requestBody = JSONObject.parseObject(reqBody);
        /**
         * 传入类似空铁接口的JSON串格式的数据，对其直接进行解析
         */
        /*String account = request.getParameter("account");
        String sign = request.getParameter("sign");
        
        String timestamp = request.getParameter("timestamp");
        
        //与当期系统时间进行比对，比对时间戳？？？ - 时间可能有差值，不用比对
        
        String data = request.getParameter("data");*/
        
        String account = requestBody.getString("account");
        String sign = requestBody.getString("sign");
        
        String timestamp = requestBody.getString("timestamp");
        
        String data = requestBody.getString("data");

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调信息-account-->"+account+"-sign-->"+sign+"-timestamp-->"+timestamp+"-data:" + data);

        //身份校验 - 数据解密，签名校验
        //传的是什么，就用什么解密
        String checkSign = SignUtil.generateSign(account, data, timestamp, key);

        String dataStr = TuNiuDesUtil.decrypt(data);
        //后续再做转换处理
        JSONObject reqData = JSONObject.parseObject(dataStr);

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调-信息解密与校验-checkSign-->"+checkSign+"-reqData-->"+reqData);

        if (!account.equals(partnerid)) {//校验不通过的话，返回失败
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "不合法的请求-签名或者账户名错误");
        } else {
            if (sign.equals(checkSign)) {
                //校验通过的话，可以存储数据
                resResult = lockTuNiuOfflineCallback(resResult, reqData);
            } else {//校验不通过的话，返回失败
                resResult.put("success", resFlag);
                resResult.put("errorCode", errorCode);
                resResult.put("msg", "不合法的请求-签名或者账户名错误");
            }
        }

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调结果-resResult" + resResult);

        out.print(resResult.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject lockTuNiuOfflineCallback(JSONObject resResult, JSONObject reqData) {
        Boolean resFlag = false;
        Integer errorCode = 231099;//success true用231000，false用231099
        
        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调信息请求-进入到方法-lockTuNiuOfflineCallback");
        
        String orderId = reqData.getString("orderId");//途牛订单号
        Boolean isLock = reqData.getBoolean("isLock");//是否锁票回调
        String lockDateTime = reqData.getString("lockDateTime");//{"lockDateTime":"2017-08-21 14:49:00","orderId":"test17082161634407","isLock":true}
        //锁票回调截止时间 格式2017-07-06 20:20:00 (同意锁票回调的时间加上10分钟为锁票回调截止日期)

        JSONObject dataResult = new JSONObject();
        dataResult.put("orderId", orderId);
        resResult.put("data", dataResult);
        
        if (orderId == null || orderId.equals("")) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单不存在，请联系查询传递的订单号");
            return resResult;
        }
        
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }

        WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调信息请求-trainOrderOffline" + trainOrderOffline);

        if (trainOrderOffline == null) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单不存在，请联系查询传递的订单号");
            return resResult;
        }
        
        if (isLock == null) {
            //异常处理
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "未传输是否锁票的字段-isLock:"+isLock);
            return resResult;
        }

        Long id = trainOrderOffline.getId();
        //直接修改相关的锁的状态
        trainOrderOfflineDao.updateTrainOrderOfflineisLockCallbackById(id);//0表示未被锁定，1表示被锁定
        
        try {
            Date lockDateOverTime = TrainOrderOfflineUtil.getDateByTimeStr(lockDateTime);
        }
        catch (Exception e1) {
            return ExceptionTNUtil.handleTNException(e1);
        }
        
        //锁票的回调的状态的健壮性判定
        Integer OrderStatus = trainOrderOffline.getOrderStatus();//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】

        Integer lockedStatus = trainOrderOffline.getLockedStatus();//锁单状态 - 0表示未被锁定，1表示被锁定 - 2-锁票中 3-锁票超时

        //可以重复取消订单吗？ - 可以，回调成功 - 不用修改状态了
        if (OrderStatus != 1) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "订单状态不对，无法完成锁票动作");
            return resResult;
        }

        if (lockedStatus == 1) {
            resFlag = true;
            errorCode = 231000;//success true用231000，false用231099
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "订单已经是锁定状态");
            return resResult;
        }
        
        if (isLock) {//锁
            int res = 0;
            try {
                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(id, 1);//0表示未被锁定，1表示被锁定

                //更新锁单成功或者失败的时间 - 
                try {
                    trainOrderOfflineDao.updateTrainOrderOfflineLockTimeById(id);
                }
                catch (Exception e2) {
                    return ExceptionTNUtil.handleTNException(e2);
                }
                
            }
            catch (Exception e) {
                return ExceptionTNUtil.handleTNException(e);
            }
            
            //如果同意锁票，超时自动解锁，之后提示拒单操作 - 给出【最晚出票时间】 - 此处另开线程处理锁票超时的逻辑 - "lockDateTime":"2017-08-21 14:49:00"

            //String lockDateTime = "2017-08-21 14:49:00";
            
            //更新数据库存储的 - 订单超时时间，并进行显示
            try {
                trainOrderOfflineDao.updateOrderTimeoutById(id, lockDateTime);//2017-08-21 14:49:00
            }
            catch (Exception e1) {
                return ExceptionTNUtil.handleTNException(e1);
            }
            
            String year = lockDateTime.substring(0,4);
            String month = lockDateTime.substring(5,7);
            String day = lockDateTime.substring(8,10);
            String hour = lockDateTime.substring(11,13);
            String minute = lockDateTime.substring(14,16);
            String second = lockDateTime.substring(17,19);
            
            //"40 17 14 25 08 ? 2017"
            String cronExpression = second+" "+minute+" "+hour+" "+day+" "+month+" ? "+year;
            
            //System.out.println(cronExpression);

            WriteLog.write(LOGNAME, r1 + ":途牛线下票锁票回调启动定时任务-cronExpression" + cronExpression);

            //创建一个定时任务，并在指定的时间点进行启动
            try {
                SchedulerUtil.startLockScheduler(id, cronExpression, "TuNiuOfflineOvertimeCancle", TrainTuNiuOfflineCancelLockTicket.class);
            }
            catch (Exception e) {
                return ExceptionTNUtil.handleTNException(e);
            }
            
        } else {//不锁票
            try {
                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(id, 2);//0表示未被锁定，1表示被锁定 2-锁单失败

                //更新锁单成功或者失败的时间 - 
                try {
                    trainOrderOfflineDao.updateTrainOrderOfflineLockTimeById(id);
                }
                catch (Exception e2) {
                    return ExceptionTNUtil.handleTNException(e2);
                }
                
            }
            catch (Exception e) {
                return ExceptionTNUtil.handleTNException(e);
            }
        }
        
        //请求已接收
        resFlag = true;
        errorCode = 231000;//success true用231000，false用231099
        resResult.put("success", resFlag);
        resResult.put("errorCode", errorCode);
        resResult.put("msg", "请求已经接收");
        
        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        String data = "{\"lockDateTime\":\"2017-09-04 11:42:00\",\"isLock\":true,\"orderId\":\"test17082940751090\"}";
        //String timestamp = "2017-08-1810:34:29";
        String timestamp = TrainOrderOfflineUtil.getTuNiuTimestamp();
        
        //String url = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineLockTicketCallback";
        String url = "http://121.40.226.72:9007/ticket_inter/TrainTuNiuOfflineLockTicketCallback";
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTuNiuOfflineLockTicketCallback";
        
        System.out.println(httpPostJsonUtil.getTuNiuRes(data, timestamp, url));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
    
}
