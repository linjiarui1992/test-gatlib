/**
 * 
 */
package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 叮叮卡码
 * http://www.dingdkj.com
 * @time 2015年9月14日 下午1:58:11
 * @author chendong
 */
public class DDKaMaMobileCode implements IMobileCode {
    public static void main(String[] args) {
        String pid = "1162"; //12306项目项目编号
        String uid = ""; //12306
        DDKaMaMobileCode ddkamamobilecode = getInstance(pid);
        //        String token = ddkamamobilecode.LoginIn("", "");
        //        System.out.println(token);
        String mobile = ddkamamobilecode.GetMobilenum("", "", "");
        System.out.println(mobile);
        //        String results = ddkamamobilecode.getMsgQueue(token);
        //        System.out.println(results);

    }

    /**
     * 
     * 
     * @param pid 4036(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static DDKaMaMobileCode getInstance(String pid) {
        //        String pid = "1151";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("DDKaMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("DDKaMaAuthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        DDKaMaMobileCode dDKaMaMobileCode = new DDKaMaMobileCode(pid, uid, author_uid, author_pwd);
        return dDKaMaMobileCode;
    }

    //叮叮卡码平台登陆
    final String LOGININURL = "http://www.dingdkj.com:19876/Url/userLogin";

    //获取手机号码
    final String USERGETPHONE = "http://www.dingdkj.com:19876/Url/userGetPhone";

    //获取验证码并释放
    final String GETMSGQUEUE = "http://www.dingdkj.com:19876//Url/getMsgQueue";

    //加黑手机号码
    final String ADDIGNORELISTURL = "http://www.dingdkj.com:19876";

    //获取该用户所使用的号码和项目
    final String GETRECVINGINFO = "http://www.dingdkj.com:19876";

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    public DDKaMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = LoginIn("", "");
    }

    public String LoginIn(String uid, String pwd) {
        if (this.author_uid == null || this.author_pwd == null) {
            return "用户名或密码不能为空";
        }
        else {
            String paramContent = this.LOGININURL + "?uName=" + this.author_uid + "&pWord=" + this.author_pwd;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            WriteLog.write("DDKaMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
            String[] results = result.split("[&]");
            if (results.length >= 5) {
                result = results[0];
            }
            return result;
        }
    }

    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        String paramContent = this.USERGETPHONE + "?ItemId=" + this.pid + "&token=" + this.token;
        String result = SendPostandGet.submitGet(paramContent, "GB2312");
        WriteLog.write("DDKaMaMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        if (result.contains("false")) {
        }
        else if (result.length() >= 11) {
            result = result.substring(0, 11);
        }
        return result;
    }

    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        if (mobile == null) {
            return "mobile不能为空DDKaMa";
        }
        else {
            String paramContent = this.GETMSGQUEUE + "?token=" + this.token;
            String result = SendPostandGet.submitGet(paramContent, "GB2312");
            WriteLog.write("DDKaMaMobileCode_getVcodeAndReleaseMobile",
                    paramContent + ":getVcodeAndReleaseMobile:" + result);
            return result;
        }
    }

    /**
     * 获取验证码并保留
     * 
     * @param uid
     * @param mobile
     * @param next_pid
     * @param token
     * @return
     * @time 2015年7月29日 下午3:59:31
     * @author chendong
     */
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 添加黑名单
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="mobiles">以,号分隔的手机号列表</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        if (this.uid == null || this.pid == null || this.token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + this.author_uid + "&pid=" + this.pid + "&token=" + this.token + "&mobiles="
                    + mobiles;
            String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("DDKaMaMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
            return result;
        }
    }

    /// <summary>
    /// 获取当前用户正在使用的号码列表
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="pid">项目ID</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String GetRecvingInfo(String uid, String pid, String token) {
        if (uid == null || pid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String paramContent = "uid=" + uid + "&pid=" + pid + "&token=" + token;
            //            System.out.println(GETRECVINGINFO);
            //            System.out.println(paramContent);
            String result = SendPostandGet.submitPost(GETRECVINGINFO, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
            WriteLog.write("DDKaMaMobileCode_GetRecvingInfo", paramContent + ":GetRecvingInfo:" + result);
            return result;
        }
    }

    /// <summary>
    /// 取消所有短信接收，可立即解锁所有被锁定的金额
    /// </summary>
    /// <param name="uid">用户ID （同获取号码）</param>
    /// <param name="token">令牌</param>
    /// <returns></returns>
    public String CancelSMSRecvAll(String uid, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    /// <summary>
    /// 取消一个短信接收，可立即解锁被锁定的金额
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="mobile"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    public String CancelSMSRecv(String uid, String mobile, String token) {
        if (uid == null || token == null) {
            return "参数传入有误";
        }
        else {
            String result = "";
            return result;
        }
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthor_uid() {
        return author_uid;
    }

    public void setAuthor_uid(String author_uid) {
        this.author_uid = author_uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthor_pwd() {
        return author_pwd;
    }

    public void setAuthor_pwd(String author_pwd) {
        this.author_pwd = author_pwd;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
