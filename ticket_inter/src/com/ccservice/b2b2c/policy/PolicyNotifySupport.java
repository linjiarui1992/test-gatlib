package com.ccservice.b2b2c.policy;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServlet;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.orderinforc.Orderinforc;
import com.ccservice.b2b2c.base.passenger.Passenger;
import com.ccservice.b2b2c.base.segmentinfo.Segmentinfo;
import com.ccservice.inter.server.Server;

public abstract class PolicyNotifySupport extends HttpServlet {
	/**
	 * 出票
	 * 
	 * @param outid
	 *            外部订单ID
	 * @throws Exception
	 */
	public static void outTciket(Orderinfo orderinfo) throws Exception {

		if (orderinfo.getOrderstatus() < 3) {
			String sql = "UPDATE T_ORDERINFO SET C_ORDERSTATUS=3,C_PRINTTIME='"
					+ new Timestamp(System.currentTimeMillis()) + "' WHERE ID="
					+ orderinfo.getId();
			Server.getInstance().getSystemService().findMapResultBySql(sql,
					null);
			List<Passenger> listpassenger = Server.getInstance()
					.getAirService().findAllPassenger(
							"WHERE C_ORDERID=" + orderinfo.getId() + " and "
									+ Passenger.COL_state + "<>12",
							"ORDER BY ID", -1, 0);
			List<Segmentinfo> listsegmentinfo = Server.getInstance()
					.getAirService().findAllSegmentinfo(
							"WHERE C_ORDERID=" + orderinfo.getId(), "", -1, 0);
			// 出票成功 返佣
			// new B2borderinfoAction().sharingRebate(orderinfo, listpassenger
			// .size());
			log(orderinfo.getId(), 3, "供应出票成功，系统自动出票。");
			// 发送联系人短信

		}

	}

	public static void log(long orderid, Integer state, String memo)
			throws SQLException {
		Orderinforc orderinforc = new Orderinforc();
		orderinforc.setOrderinfoid(orderid);
		orderinforc.setCustomeruserid(null);
		orderinforc.setCreatetime(new Timestamp(System.currentTimeMillis()));
		orderinforc.setContent(memo);
		orderinforc.setState(state);
		Server.getInstance().getAirService().createOrderinforc(orderinforc);
	}

	public void log(long orderid, String memo) throws SQLException {
		Orderinforc orderinforc = new Orderinforc();
		orderinforc.setOrderinfoid(orderid);
		orderinforc.setCustomeruserid(null);
		orderinforc.setCreatetime(new Timestamp(System.currentTimeMillis()));
		orderinforc.setContent(memo);
		orderinforc.setState(null);
		Server.getInstance().getAirService().createOrderinforc(orderinforc);
	}

}
