package com.ccservice.test;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderchange;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.util.TimeUtil;
import com.ccservice.inter.server.Server;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * 程序猿日常代码
 * 
 * @author 之至
 * @time 2016年12月12日 下午3:26:28
 */
public class YunWeiRabbitMQ {
    //空铁   目标地址
    private static final String HOSTKT = "43.241.233.67";
    //空铁   用户名
    private static final String USERNAMEKT = "admin";
    //空铁   密码
    private static final String PASSWORDKT = "auUaaWcF";
    //同程      目标地址
    private static final String HOSTTC = "43.241.234.100";
    //同程      用户名
    private static final String USERNAMETC = "admin";
    //同程      密码
    private static final String PASSWORDTC = "NWsfTYiD";
    
    public static void main(String[] args) throws TimeoutException {
        

    }
    
    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午5:27:17
     * @Description 同程扣款
     * @param orderids 订单id
     * @throws TimeoutException 
     */
    private static void gotoordertc(String orderids) throws TimeoutException{
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            sendmessage(ss[i], HOSTTC, USERNAMETC, PASSWORDTC, "RabbitWaitOrder");
        }
    }
    
    /**
     * 
     * @author 之至
     * @time 2016年12月21日 下午5:27:36
     * @Description 空铁发扣款
     * @param orderids  订单id
     * @throws TimeoutException 
     */
    private static void gotodeKongtie(String orderids) throws TimeoutException {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("type", 1);
            sendmessage(jsoseng.toJSONString(), HOSTKT, USERNAMEKT, PASSWORDKT, "Rabbit_QueueMQ_Deduction");
        }

    }

    //空铁发扣款
    private static void gotodeTC(String orderids) throws TimeoutException {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("type", 1);
            sendmessage(jsoseng.toJSONString(), HOSTTC, USERNAMETC, PASSWORDTC, "Rabbit_QueueMQ_Deduction");
        }

    }
    /**
     *  TONGCHENG 重新发送下单
     * 
     * @param orderids
     * @time 2015年9月16日 下午4:37:11
     * @author fiend
     * @throws TimeoutException 
     */
    private static void gotoorderTongcheng(String orderids) throws TimeoutException {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            System.out.println(jsoseng.toJSONString());
            sendmessage(ss[i], HOSTTC, USERNAMETC, PASSWORDTC, "RabbitWaitOrder");
        }
    }
    /**
     *  Kongtie 重新发送下单
     * 
     * @param orderids
     * @time 2015年9月16日 下午4:37:11
     * @author fiend
     * @throws TimeoutException 
     */
    private static void gotoorderKongtie(String orderids) throws TimeoutException {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            System.out.println(jsoseng.toJSONString());
            sendmessage(ss[i], HOSTKT, USERNAMEKT, PASSWORDKT, "RabbitWaitOrder");
        }
    }
    /**
     *  空铁重新发送改签下单（接口申请改签）
     *  ID查询SQL：select ID from T_TRAINORDERCHANGE with(nolock) where C_TCCREATETIME > CONVERT(varchar(100), GETDATE(), 23) and C_TCSTATUS = 1
     * @throws TimeoutException 
     */
    private static void gotoChangeOrderKongTie(String changeIds) throws TimeoutException {
        String[] array = changeIds.split(",");
        for (int i = 0; i < array.length; i++) {
            String changeId = array[i];
            System.out.println("空铁改签下单（申请改签）-----" + changeId);
            sendmessage(changeId, HOSTKT, USERNAMEKT, PASSWORDKT, "changeWaitOrder");
        }
    }
    /**
     *  同程重新发送改签下单（接口申请改签）
     *  ID查询SQL：select ID from T_TRAINORDERCHANGE with(nolock) where C_TCCREATETIME > CONVERT(varchar(100), GETDATE(), 23) and C_TCSTATUS = 1
     * @throws TimeoutException 
     */
    private static void gotoChangeOrderTongCheng(String changeIds) throws TimeoutException {
        String[] array = changeIds.split(",");
        for (int i = 0; i < array.length; i++) {
            String changeId = array[i];
            System.out.println("同程改签下单（申请改签）-----" + changeId);
            sendmessage(changeId, HOSTTC, USERNAMETC, PASSWORDTC, "changeWaitOrder");
        }
    }
    /**
     *  空铁重新发送改签支付（接口确认改签）
     *  ID查询SQL：select ID from T_TRAINORDERCHANGE with(nolock) where C_TCCREATETIME > CONVERT(varchar(100), GETDATE(), 23) and C_TCSTATUS = 7
     * @throws TimeoutException 
     */
    private static void gotoChangePayKongTie(String changeIds) throws TimeoutException {
        String[] array = changeIds.split(",");
        for (int i = 0; i < array.length; i++) {
            String changeId = array[i];
            System.out.println("空铁改签支付（确认改签）-----" + changeId);
            sendmessage(changeId, HOSTKT, USERNAMEKT, PASSWORDKT, "changeConfirmOrder");
        }
    }
    /**
     *  同程重新发送改签支付（接口确认改签）
     *  ID查询SQL：select ID from T_TRAINORDERCHANGE with(nolock) where C_TCCREATETIME > CONVERT(varchar(100), GETDATE(), 23) and C_TCSTATUS = 7
     * @throws TimeoutException 
     */
    private static void gotoChangePayTongCheng(String changeIds) throws TimeoutException {
        String[] array = changeIds.split(",");
        for (int i = 0; i < array.length; i++) {
            String changeId = array[i];
            System.out.println("同程改签支付（确认改签）-----" + changeId);
            sendmessage(changeId, HOSTTC, USERNAMETC, PASSWORDTC, "changeConfirmOrder");
        }
    }
    /**
     *  Tongcheng 去扣款
     * 
     * @time 2015年4月15日 下午6:38:18
     * @author fiend
     * @throws TimeoutException 
     */
    private static void gotodeTongcheng(String orderids) throws TimeoutException {
        String[] ss = orderids.split(",");
        for (int i = 0; i < ss.length; i++) {
            Long orderid = Long.parseLong(ss[i]);
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("type", 1);
            System.out.println(jsoseng.toJSONString());
            sendmessage(jsoseng.toJSONString(), HOSTTC, USERNAMETC, PASSWORDTC, "Rabbit_QueueMQ_Deduction");
        }

    }
    /**
     * 
     * @author 之至
     * @time 2016年12月15日 下午4:50:33
     * @Description 艺龙退票退款回调
     * @param merchantCode   代理商code
     * @param orderId  订单号
     * @param orderItemId  乘客id
     * @param result
     * @param key  固定  72d97c2acb5923234b2f80a7dbe8d85c
     * @param amount   退款金额 
     * @param returnmsg   备注 
     * @param returntype
     * @param ticket_no   票号
     * @return
     */
    
    public static String refunCallBack(String merchantCode, String orderId, String orderItemId, boolean result,
            String key, String amount, String returnmsg, String returntype, String ticket_no) {
        String comment = "";
        String singmd = "";
        String ret = "";
        String sign = "";
        //0：表示线下退票退款  2：线下改签退款
        WriteLog.write("t同程火车票接口_4.10退票回调通知_yilong", "|key：" + key + ":returntype:" + returntype + "|returnmsg"
                + returnmsg);
        try {
            if (result || result == true) {
                comment = "成功";
                comment = URLEncoder.encode(comment, "UTF-8");
            }
            else {
                comment = returnmsg;
            }
            String[] parameters = { "merchantCode=" + merchantCode, "orderId=" + orderId, "orderItemId=" + orderItemId,
                    "amount=" + amount, "comment=" + URLDecoder.decode(comment, "UTF-8"), "tradeNo=" + ticket_no };
            sign = sort(parameters);
            WriteLog.write("t同程火车票接口_4.10退票回调通知_yilong", orderId + ":sign:" + sign + "←看下排序后的结果" + "result：" + result
                    + "←看下result进来的什么结果" + ":key:" + key);
            singmd = ElongHotelInterfaceUtil.MD5(sign + key).toUpperCase();
            String parm = "merchantCode=" + merchantCode + "&orderId=" + orderId + "&orderItemId=" + orderItemId
                    + "&comment=" + comment + "&amount=" + amount + "&tradeNo=" + ticket_no + "&sign=" + singmd;
            //请求艺龙
            String payCallbackUrl_temp = "http://trainapi.elong.com/open_api/process_refund";//PropertyUtil.getValue("httpYiLong", "Train.properties");
            WriteLog.write("t同程火车票接口_4.10退票回调通知_yilong", orderId + ":parm:" + parm + ":payCallbackUrl_temp:"
                    + payCallbackUrl_temp);
            ret = SendPostandGet.submitPost(payCallbackUrl_temp, parm, "utf-8").toString();
            WriteLog.write("t同程火车票接口_4.10退票回调通知_yilong", orderId + ":ret:" + ret + ":singmd:" + singmd + "看看编译后的字段");
            JSONObject jsonObjectrest = JSONObject.parseObject(ret);
            Integer retcode = (Integer) jsonObjectrest.get("retcode");
            String retdesc = (String) jsonObjectrest.get("retdesc");
            if (jsonObjectrest.getBooleanValue("成功") || jsonObjectrest.getBooleanValue("200")
                    || jsonObjectrest.getBooleanValue("success") || retcode.equals(200) || retdesc.contains("成功")
                    || retcode == 200) {
                ret = "success";
            }
            else {
                ret = "false";
            }
            WriteLog.write("t同程火车票接口_4.10退票回调通知_yilong", orderId + ":orderId:" + orderItemId + ":orderItemId:"
                    + ":yl艺龙返回:" + ret);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
    
    /**
     * 
     * @author 之至
     * @time 2016年12月12日 下午3:26:59
     * @Description 发送消息公共方法
     * @param message  发送消息
     * @param host     发送的目标地址
     * @param username  用户名
     * @param password   密码
     * @param endPointName  队列名称
     * @throws TimeoutException 
     */
    private static void sendmessage(String messagesString, String host, String username, String password, String endPointName) throws TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(username);
        factory.setPassword(password);
        Connection connection;
        try {
            connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(endPointName, true, false, false, null);
            HashMap<String, String> message = new HashMap<String, String>();
            message.put("message number", messagesString);
            channel.basicPublish("", endPointName, null, SerializationUtils.serialize(message));
            System.out.println(message);
            channel.close();
            connection.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.out.println(e+e.getMessage());
        }
    }
    
    private static String sort(String[] str) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length; i++) {
            String maxStr = str[i];
            int index = i;
            //  System.out.println(str.length + ":第" + i + "次循环");
            for (int j = i + 1; j < str.length; j++) {
                if (maxStr.compareTo(str[j]) >= 0) {
                    maxStr = str[j];
                    index = j;
                }
            }
            str[index] = str[i];
            str[i] = maxStr;
            // System.out.println(i + ":" + maxStr);
            sb.append(maxStr + "&");
        }
        String sign = sb.toString();
        if (sign.endsWith("&")) {
            sign = sign.substring(0, sb.toString().length() - 1);
        }
        return sign;
    }
    
    /**
     * 
     * @author 之至
     * @time 2016年12月27日 下午1:55:48
     * @Description 同程确认改签回调失败
     * @param changeId
     * @param id
     */
    private  void  toTcConfirmChangeCallBack(long changeId,long id){
        Trainorderchange trainOrderChange = Server.getInstance().getTrainService().findTrainOrderChangeById(changeId);
        Trainorder order = Server.getInstance().getTrainService().findTrainorder(id);
        JSONObject jsonObject=new JSONObject();
        JSONArray jsonArray=new JSONArray();
        String reptimeString=TimeUtil.dateToString(new Date(), TimeUtil.yyyyMMddHHmmss);
        //订单乘客
        List<Trainpassenger> passengers = order.getPassengers();
        //循环乘客
        for (Trainpassenger passenger : passengers) {
            //车票
            List<Trainticket> traintickets = passenger.getTraintickets();
            //循环
            for (Trainticket changeTicket : traintickets) {
                //改签车票
                if (changeTicket.getChangeid() == changeId) {
                    //同程返回
                    JSONObject ticket = new JSONObject();
                    JSONObject newticketcxins=new JSONObject();
                    newticketcxins.put("old_ticket_no", changeTicket.getTicketno());
                    newticketcxins.put("new_ticket_no", changeTicket.getTcticketno());
                    //乘客ID
                    if (!ElongHotelInterfaceUtil.StringIsNull(changeTicket.getTcPassengerId())) {
                        newticketcxins.put("passengerid", changeTicket.getTcPassengerId());
                    }
                    newticketcxins.put("cxin", changeTicket.getTccoach() + "车厢," + changeTicket.getTcseatno());
                    //车票信息
                    jsonArray.add(ticket);
                    //车票ID
                    //中断当前
                    break;
                }
            }
        }
        String signString="";
        try {
            signString = ElongHotelInterfaceUtil.MD5("tongcheng_train" + reptimeString + ElongHotelInterfaceUtil.MD5("x3z5nj8mnvl14nirtwlvhvuialo0akyt"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        jsonObject.put("reqtoken", trainOrderChange.getConfirmReqtoken());
        jsonObject.put("ticketpricediffchangeserial", "");
        jsonObject.put("reqtime", reptimeString);
        jsonObject.put("code", "999");
        jsonObject.put("newticketchangeserial", "");
        jsonObject.put("msg", "确认改签失败");
        jsonObject.put("sign", signString);
        jsonObject.put("partnerid", "tongcheng_train");
        jsonObject.put("oldticketchangeserial", "");
        jsonObject.put("newticketcxins", jsonArray.toString());
        jsonObject.put("method", "train_confirm_change");
        jsonObject.put("orderid", order.getQunarOrdernumber());
        jsonObject.put("success", false);
        String resultString=SendPostandGet.submitPost("http://train.17usoft.com/trainOrder/services/confirmTicChangeNotify", "backjson=" + jsonObject.toString(), "UTF-8")
                .toString();
        System.out.println(id+"---"+changeId+"---"+resultString);
    }
    
    /**
     * Kongtie 重新审核
     * 
     * @param strs
     * @time 2015年9月16日 下午4:38:36
     * @author fiend
     */
    private static void querybyStrsKongtie(String strs) {
        String[] strss = strs.split(",");
        for (int i = 0; i < strss.length; i++) {
            Long orderid = Long.valueOf(strss[i]);
            String url = "http://120.26.100.206:49410/ticket_inter/QueryTrainorder.jsp?id=" + orderid;
            System.out.println(orderid);
            SendPostandGet.submitGet(url, "UTF-8");
        }
    }
}
