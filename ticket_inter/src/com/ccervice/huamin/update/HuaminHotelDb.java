package com.ccervice.huamin.update;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.country.Country;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelbene.hotelbene;
import com.ccservice.b2b2c.base.hotelcancel.hotelcancel;
import com.ccservice.b2b2c.base.hotelproduct.hotelproduct;
import com.ccservice.b2b2c.base.hotelserv.hotelserv;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.elong.hoteldb.HanziToPinyin;
import com.ccservice.inter.server.Server;

/**
 * 录入华闽酒店数据
 * 
 */
public class HuaminHotelDb {

    public static void main(String[] args) {
        new HuaminHotelDb().updateHotelID();
    }

    /**
     * 录入并更新酒店id
     * @param args
     */
    @SuppressWarnings("unchecked")
    public void updateHotelID() {
        getHotel("CHN", "", "SIM");
        getHotel("HKG", "", "SIM");
        getHotel("MFM", "", "SIM");
        getHotel("TWN", "", "SIM");
        new HuaminQhotelcattypeDb().getQhotelcattype();
    }

    /**
     * 更新合同附属信息
     */
    @SuppressWarnings("unchecked")
    public void updateContractInfo() {
        List<Hotel> hotelfromtable = Server
                .getInstance()
                .getHotelService()
                .findAllHotel(
                        "where C_SOURCETYPE=3  AND C_TYPE=1 and c_cityid is not null and C_HOTELCODE is not null and ((C_PUSH is null) or C_PUSH!=2) ",
                        "order by c_cityid asc ", -1, 0);
        int sum = hotelfromtable.size();
        for (Hotel hotel : hotelfromtable) {
            System.out.println("酒店名字:" + hotel.getName() + ",剩余：" + sum--);
            try {
                // 和房型表进行关联
                getcontract(hotel.getId(), hotel.getHotelcode());
            }
            catch (Exception e) {
                WriteLog.write("房型错误", "更新到" + hotel.getId() + ",错误信息:" + hotel.getName() + "错误了!");
            }
        }
    }

    /**
     * 更新一个月的价格
     */
    @SuppressWarnings("unchecked")
    public void updateOneMonthPrice() {
        //更新华闽多少个月的数据
        String huaminCatchPriceMonth = PropertyUtil.getValue("huaminCatchPriceMonth");
        if (huaminCatchPriceMonth == null || "".equals(huaminCatchPriceMonth.trim())) {
            huaminCatchPriceMonth = "1";
        }
        long t1 = System.currentTimeMillis();
        List<Hotel> hotelfromtable = Server
                .getInstance()
                .getHotelService()
                .findAllHotel(
                        "where  C_SOURCETYPE=3 and C_TYPE=1 and C_CITYID is not null and C_HOTELCODE is not null and (C_PUSH is null or C_PUSH!=2) ",
                        " order by c_cityid asc ", -1, 0);
        int i = hotelfromtable.size();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "delete from t_hmhotelprice where  C_STATEDATE<'" + timeformat.format(cal.getTime()) + "'";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        System.out.println("删除当天以前的数据：" + sql);
        for (Hotel hotel : hotelfromtable) {
            System.out.println("酒店名字:" + hotel.getName() + ",剩余：" + i--);
            try {
                Date currentStartDate = new Date();
                for (int m = 0; m < Integer.parseInt(huaminCatchPriceMonth); m++) {
                    Date currentEndDate = CatchNext31Day(currentStartDate);
                    getNewRate(hotel.getId(), currentStartDate, currentEndDate, hotel.getCityid().toString());
                    currentStartDate = currentEndDate;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                WriteLog.write("更新" + huaminCatchPriceMonth + "个月的价格",
                        "更新到" + hotel.getId() + ",错误信息:" + hotel.getName() + "错误了!");
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("更新用时：" + (t2 - t1) / 1000 / 60 + "分钟");
        System.out.println("更新" + huaminCatchPriceMonth + "个月的价格结束：" + timeformat.format(new Date()));
    }

    @SuppressWarnings("unchecked")
    public void updateInternationalOneMonthPrice() {
        //更新华闽多少个月的数据
        String huaminCatchPriceMonth = PropertyUtil.getValue("huaminCatchPriceMonth");
        if (huaminCatchPriceMonth == null || "".equals(huaminCatchPriceMonth.trim())) {
            huaminCatchPriceMonth = "1";
        }
        long t1 = System.currentTimeMillis();
        List<Hotel> hotelfromtable = Server
                .getInstance()
                .getHotelService()
                .findAllHotel(
                        "where C_SOURCETYPE=3 AND C_TYPE=2 and C_CITYID is not null and C_HOTELCODE is not null and (C_PUSH != 2 or C_PUSH is null)",
                        "order by c_cityid asc ", -1, 0);
        int i = hotelfromtable.size();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "delete from t_hmhotelprice where  C_STATEDATE<'" + timeformat.format(cal.getTime()) + "'";
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        System.out.println("删除当天以前的数据：" + sql);
        for (Hotel hotel : hotelfromtable) {
            System.out.println("酒店名字:" + hotel.getName() + ",剩余：" + i--);
            try {
                Date currentStartDate = new Date();
                for (int m = 0; m < Integer.parseInt(huaminCatchPriceMonth); m++) {
                    Date currentEndDate = CatchNext31Day(currentStartDate);
                    getNewRate(hotel.getId(), currentStartDate, currentEndDate, hotel.getCityid().toString());
                    currentStartDate = currentEndDate;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                WriteLog.write("更新" + huaminCatchPriceMonth + "个月的价格",
                        "更新到" + hotel.getId() + ",错误信息:" + hotel.getName() + "错误了!");
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("更新用时：" + (t2 - t1) / 1000 / 60 + "分钟");
        System.out.println("更新" + huaminCatchPriceMonth + "个月的价格结束：" + timeformat.format(new Date()));
    }

    /**
     * 更新第31天的价格
     * @param args
     */
    @SuppressWarnings("unchecked")
    public void updatelastdayprice() {
        //更新华闽多少个月的数据
        String huaminCatchPriceMonth = PropertyUtil.getValue("huaminCatchPriceMonth");
        if (huaminCatchPriceMonth == null || "".equals(huaminCatchPriceMonth.trim())) {
            huaminCatchPriceMonth = "1";
        }
        List<Hotel> hotelfromtable = Server.getInstance().getHotelService()
                .findAllHotel(" WHERE C_SOURCETYPE=3 ", "order by id desc", -1, 0);
        for (Hotel hotel : hotelfromtable) {
            System.out.println("酒店名字:" + hotel.getName());
            try {
                Date currentStartDate = new Date();
                for (int m = 0; m < Integer.parseInt(huaminCatchPriceMonth); m++) {
                    currentStartDate = CatchNext31Day(currentStartDate);
                }
                getNewRate(hotel.getId(), CatchNext31Day(currentStartDate),
                        CatchNext31Day(Tomorowday(currentStartDate)), hotel.getCityid().toString());
            }
            catch (Exception e) {
                WriteLog.write("更新最后一天的价格", "更新到" + hotel.getId() + ",错误信息:" + hotel.getName() + "错误了!");
            }
        }
    }

    // 酒店
    private static void getHotel(String countrycode, String hotelcode, String languagetype) {
        if (countrycode != null && countrycode.equals("CHN")) {
            List<Province> pros = Server.getInstance().getHotelService()
                    .findAllProvince("where C_HUAMINCODE is not null", "order by id asc ", -1, 0);
            for (int i = 0; i < pros.size(); i++) {
                String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotel&p_lang=" + languagetype
                        + "&p_country=" + countrycode + "&p_hotel=" + hotelcode + "&p_area="
                        + pros.get(i).getHuamincode();
                System.out.println("URL地址:" + totalurl);
                String str = Util.getStr(totalurl);
                parsexml_hotel(str, languagetype);
            }
        }
        else {
            String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qhotel&p_lang=" + languagetype
                    + "&p_country=" + countrycode + "&p_hotel=" + hotelcode;
            System.out.println("URL地址:" + totalurl);
            String str = Util.getStr(totalurl);
            parsexml_hotel(str, languagetype);
        }
    }

    // 录入酒店
    @SuppressWarnings("unchecked")
    public static void parsexml_hotel(String xmlstr, String languagetype) {
        if (xmlstr != null && xmlstr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")) {
            xmlstr = xmlstr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        }
        SAXBuilder build = new SAXBuilder();
        Document document;
        try {
            document = build.build(new StringReader(xmlstr));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            List<Element> hotels = result.getChildren("HOTELS");
            if (hotels != null && hotels.size() > 0) {
                for (Element hotel : hotels) {
                    String countrycode = hotel.getChildText("COUNTRY");
                    System.out.println("国家代码:" + countrycode);
                    String countryname = hotel.getChildText("COUNTRYNAME");
                    if (countrycode == null || "".equals(countrycode.trim()) || countryname == null
                            || countryname.trim().equals("") || countryname.trim().equals("其它")) {
                        continue;
                    }
                    List<Country> countryfromtable = Server
                            .getInstance()
                            .getInterHotelService()
                            .findAllCountry(
                                    " WHERE " + Country.COL_HUAMINCODE + "='" + countrycode.trim() + "' or "
                                            + Country.COL_zhname + "='" + countryname + "'", "", -1, 0);
                    Country tempc = new Country();
                    tempc.setZhname(countryname);
                    tempc.setName(HanziToPinyin.getPinYin(countryname));
                    tempc.setHuamincode(countrycode.trim());
                    if (countryfromtable == null || countryfromtable.size() == 0) {
                        tempc = Server.getInstance().getInterHotelService().createCountry(tempc);
                    }
                    else if (countryfromtable.size() == 1) {
                        tempc.setId(countryfromtable.get(0).getId());
                        if (!countrycode.trim().equals(countryfromtable.get(0).getHuamincode())) {
                            Server.getInstance().getInterHotelService().updateCountryIgnoreNull(tempc);
                        }
                    }
                    else {
                        for (Country c : countryfromtable) {
                            if (countrycode.trim().equals(c.getHuamincode())) {
                                tempc = c;
                                break;
                            }
                        }
                        //编码未匹配上取名称
                        if (tempc.getId() == 0) {
                            tempc.setId(countryfromtable.get(0).getId());
                            Server.getInstance().getInterHotelService().updateCountryIgnoreNull(tempc);
                        }
                    }
                    Hotel hoteltemp = new Hotel();
                    // 1--表示来自于艺龙,3--表示来自于华闽
                    hoteltemp.setSourcetype(3l);
                    hoteltemp.setCountryid(tempc.getId());
                    // 0--表示暂时不可用
                    // 2--表示艺龙那边没有提供给我们房型
                    // 3--表示可用
                    hoteltemp.setState(3);
                    // 1--表示国内,2--表示国外
                    hoteltemp.setStatedesc("华闽预付酒店");
                    if ("CHN".equals(countrycode)) {
                        hoteltemp.setType(1);
                    }
                    else {
                        hoteltemp.setType(2);
                    }
                    // 预付-2 现付-1
                    hoteltemp.setPaytype(2l);
                    String huamincode = hotel.getChildText("HOTEL");
                    System.out.println("华闽酒店标识：" + huamincode);
                    hoteltemp.setHotelcode(huamincode.trim());
                    String hotelname = hotel.getChildText("HOTELNAME");
                    System.out.println("开始导入：" + hotelname);
                    hoteltemp.setName(hotelname);
                    if (hotelname == null || "".equals(hotelname.trim()) || ",".equals(hotelname.trim())
                            || ".".equals(hotelname.trim()) || "nil".equals(hotelname.trim().toLowerCase())) {
                        hoteltemp.setState(0);//表示暂时不可用
                    }
                    String grade = hotel.getChildText("GRADE");
                    try {
                        if (grade.equals("N") || grade.equals("B")) {
                            hoteltemp.setStar(1);
                        }
                        else {
                            hoteltemp.setStar(Integer.parseInt(grade.trim()));
                        }
                    }
                    catch (Exception e) {
                        WriteLog.write("星级转换出错了", "错误信息:" + hotelname + "," + e.getMessage());
                    }
                    String address = hotel.getChildText("ADDRESS");
                    hoteltemp.setAddress(address);
                    //省份
                    String areacode = hotel.getChildText("AREA");
                    if ("CHN".equals(countrycode) || "HKG".equals(countrycode) || "MFM".equals(countrycode)
                            || "TWN".equals(countrycode)) {//内地、港、澳、台
                        List<Province> provincefromtable = Server
                                .getInstance()
                                .getHotelService()
                                .findAllProvince(
                                        " WHERE " + Province.COL_type + "=1 AND " + Province.COL_HUAMINCODE + "='"
                                                + areacode + "'", "", -1, 0);
                        if (provincefromtable != null && provincefromtable.size() > 0) {
                            hoteltemp.setProvinceid(provincefromtable.get(0).getId());
                        }
                    }
                    //城市
                    String cityid = hotel.getChildText("CITY");
                    String cityname = hotel.getChildText("CITYNAME");
                    if (cityid == null || "".equals(cityid.trim()) || cityname == null || "".equals(cityname.trim())) {
                        continue;
                    }
                    City city = new City();
                    city.setHuamincode(cityid.trim());
                    if ("CHN".equals(countrycode) || "HKG".equals(countrycode) || "MFM".equals(countrycode)
                            || "TWN".equals(countrycode)) {//内地、港、澳、台
                        city.setType(1l);
                    }
                    else {
                        city.setType(4l);//4 -- 国际城市（除内地、港、澳、台城市）
                    }
                    city.setLanguage(0);
                    city.setProvinceid(hoteltemp.getProvinceid());
                    city.setCountryid(hoteltemp.getCountryid());
                    List<City> cityfromtable = Server
                            .getInstance()
                            .getHotelService()
                            .findAllCity(
                                    " WHERE " + City.COL_type + "=" + city.getType() + " AND (" + City.COL_HUAMINCODE
                                            + "='" + cityid.trim() + "' or " + City.COL_name + "='" + cityname + "')",
                                    "", -1, 0);
                    if (cityfromtable == null || cityfromtable.size() == 0) {
                        city.setName(cityname);
                        city.setEnname(HanziToPinyin.getPinYin(city.getName()));
                        city.setSname(city.getEnname());
                        city = Server.getInstance().getHotelService().createCity(city);
                        city.setSort(Integer.parseInt(city.getId() + ""));
                        Server.getInstance().getHotelService().updateCityIgnoreNull(city);
                    }
                    else {
                        City tempCity = new City();
                        if (cityfromtable.size() == 1) {
                            tempCity = cityfromtable.get(0);
                        }
                        else {
                            for (City c : cityfromtable) {
                                if (cityid.trim().equals(c.getHuamincode())) {
                                    tempCity = c;
                                    break;
                                }
                            }
                            if (tempCity.getId() == 0)
                                tempCity = cityfromtable.get(0);
                        }
                        city.setId(tempCity.getId());
                        Server.getInstance().getHotelService().updateCityIgnoreNull(city);
                    }
                    hoteltemp.setCityid(city.getId());
                    //区域
                    String regioncode = hotel.getChildText("DIST");
                    String regionname = hotel.getChildText("DISTNAME");
                    if (regioncode != null && !"".equals(regioncode.trim()) && regionname != null
                            && !"".equals(regionname.trim()) && !"其它".equals(regionname.trim())) {
                        List<Region> regionfromtable = Server
                                .getInstance()
                                .getHotelService()
                                .findAllRegion(
                                        " WHERE " + Region.COL_HUAMINCODE + "='" + regioncode.trim()
                                                + "' and C_CITYID=" + hoteltemp.getCityid(), "", -1, 0);
                        if (regionfromtable != null && regionfromtable.size() > 0) {
                            hoteltemp.setRegionid1(regionfromtable.get(0).getId());
                        }
                        else {
                            Region region = new Region();
                            region.setName(regionname);
                            region.setCityid(hoteltemp.getCityid());
                            region.setType("1");//商业区
                            region.setLanguage(0);
                            region.setHuamincode(regioncode.trim());
                            region.setCountryid(hoteltemp.getCountryid());
                            //region = Server.getInstance().getHotelService().createRegion(region);
                            region.setUcode(region.getId());
                            //Server.getInstance().getHotelService().updateRegionIgnoreNull(region);
                            //hoteltemp.setRegionid1(region.getId());
                        }
                    }
                    //经、纬度
                    String lat = hotel.getChildText("LATITUDE");
                    System.out.println("lat:" + lat);
                    String lon = hotel.getChildText("LONGITUDE");
                    System.out.println("lon:" + lon);
                    try {
                        if (lat != null && !"".equals(lat)) {
                            hoteltemp.setLat(Double.parseDouble(lat.trim()));
                        }
                        if (lon != null && !"".equals(lon)) {
                            hoteltemp.setLng(Double.parseDouble(lon.trim()));
                        }
                    }
                    catch (Exception ex) {
                        WriteLog.write("酒店新增数据", "经纬度错误信息:" + hotelname + "," + lat + "," + lon + ":" + ex.getMessage());
                    }
                    String tel = hotel.getChildText("TEL");
                    hoteltemp.setTortell(tel);
                    hoteltemp.setMarkettell(tel);
                    String fax = hotel.getChildText("FAX");
                    hoteltemp.setFax1(fax);
                    String hoteldesc = hotel.getChildText("HOTELDESC");
                    hoteltemp.setDescription(hoteldesc);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    hoteltemp.setLastupdatetime(sdf.format(new Date(System.currentTimeMillis())));

                    List<Hotel> hotelfromtable = Server
                            .getInstance()
                            .getHotelService()
                            .findAllHotel(
                                    " WHERE " + Hotel.COL_hotelcode + "='" + hoteltemp.getHotelcode()
                                            + "' AND C_SOURCETYPE = 3", "", -1, 0);
                    if (hotelfromtable != null && hotelfromtable.size() > 0) {
                        hoteltemp.setId(hotelfromtable.get(0).getId());
                        Server.getInstance().getHotelService().updateHotelIgnoreNull(hoteltemp);
                        System.out.println("酒店更新成功!~~~~~~~~~~~~~~~~");
                    }
                    else {
                        hoteltemp = Server.getInstance().getHotelService().createHotel(hoteltemp);
                        System.out.println("酒店录入成功!~~~~~~~~~~~~~~~~");
                    }
                    if ("SIM".equals(languagetype)
                            && (hotelname == null || "".equals(hotelname.trim()) || ",".equals(hotelname.trim())
                                    || ".".equals(hotelname.trim()) || "nil".equals(hotelname.trim().toLowerCase()))) {
                        getHotel(countrycode, huamincode, "ENG");
                    }
                    //					try {
                    //						 //和房型表进行关联
                    //						getcontract(hoteltemp.getId(), hoteltemp.getHotelcode());
                    //					} catch (Exception e) {
                    //						WriteLog.write("房型错误", "错误信息:" + hoteltemp.getName()
                    //								+ "错误了!");
                    //					}
                }
            }
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            String errormessage = result.getChildText("ERROR_MESSAGE");
            if (errormessage != null || !"".equals(errormessage)) {
                WriteLog.write("酒店新增数据", "错误信息:" + resultco + "," + errormessage);
            }
        }
        catch (Exception e) {
        }
    }

    // 录入酒店价格
    public static void getNewRate(long hotelid, Date checkin, Date checkout, String cityid) {
        SimpleDateFormat sdf = new SimpleDateFormat("d-MMM-yy", Locale.US);
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qrate&p_lang=SIM&p_checkin="
                + sdf.format(checkin) + "&p_checkout=" + sdf.format(checkout) + "&p_hotel="
                + Server.getInstance().getHotelService().findHotel(hotelid).getHotelcode().trim();
        System.out.println("新访问获取价格路径:" + totalurl);
        String xmlstr = Util.getStr(totalurl);
        if (getXmlCount(xmlstr) > 2) {
            parseNewxml(hotelid, xmlstr, cityid, checkin, checkout);
        }
        else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(checkin);
            cal.add(Calendar.DAY_OF_MONTH, 3);
            String totalurlm = HMRequestUitl.getHMRequestUrlHeader() + "&api=qrate&p_lang=SIM&p_checkin="
                    + sdf.format(cal.getTime()) + "&p_checkout=" + sdf.format(checkout) + "&p_hotel="
                    + Server.getInstance().getHotelService().findHotel(hotelid).getHotelcode().trim();
            System.out.println("新访问获取价格路径:" + totalurlm);
            String xmlstrm = Util.getStr(totalurlm);
            parseNewxml(hotelid, xmlstrm, cityid, checkin, checkout);
        }
    }

    public static int getXmlCount(String xmlstr) {
        if (xmlstr != null && xmlstr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")) {
            xmlstr = xmlstr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        }
        SAXBuilder sb = new SAXBuilder();
        Document doc;
        int count = 0;
        try {
            doc = sb.build(new StringReader(xmlstr));
            Element root = doc.getRootElement();
            Element result = root.getChild("XML_RESULT");
            count = result.getChildren().size();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    // 更新后获取价格代码
    @SuppressWarnings("unchecked")
    public static void parseNewxml(long hotelid, String xmlStr, String cityid, Date checkin, Date checkout) {
        if (xmlStr != null && xmlStr.contains("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>")) {
            xmlStr = xmlStr.replace("?<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Server.getInstance()
                    .getSystemService()
                    .findMapResultBySql(
                            " delete from t_hmhotelprice where C_SOURCETYPE=3  and c_hotelid=" + hotelid
                                    + " and C_STATEDATE>='" + sdf.format(checkin) + "' and C_STATEDATE<'"
                                    + sdf.format(checkout) + "'", null);
            System.out.println("删除成功……");
            SAXBuilder sb = new SAXBuilder();
            List<Hmhotelprice> listprice = new ArrayList<Hmhotelprice>();
            Document doc = sb.build(new StringReader(xmlStr));
            Element root = doc.getRootElement();
            Element result = root.getChild("XML_RESULT");
            List<Element> contractslist = result.getChildren("CONTRACTS");
            label: for (Element contracts : contractslist) {
                if (contracts != null) {
                    String hmcontract = contracts.getChildText("CONTRACT");
                    String hmcontractver = contracts.getChildText("VER");
                    String hmhotelid = contracts.getChildText("HOTEL");
                    // String hmhotelname = contracts.getChildText("HOTELNAME");
                    String cur = contracts.getChildText("CUR");
                    if (!"RMB".equals(cur)) {
                        System.out.println("不适用货币……");
                        continue label;
                    }
                    // Element product = contracts.getChild("PRODUCT");
                    List<Element> products = contracts.getChildren("PRODUCT");
                    String block = contracts.getChildText("BLOCK");
                    if (!block.equals("Y") || 1 == 1) {
                        for (Element product : products) {
                            String prod = product.getChildText("PROD");
                            String nation = product.getChildText("NATION");
                            if (nation.equals("NPRC") || nation.equals("HKID") || nation.equals("NC")) {
                                System.out.println("不适用区域……");
                                continue;
                            }
                            String rarate = product.getChildText("RORATE");
                            String nationname = product.getChildText("NATIONNAME");
                            String min = product.getChildText("MIN");
                            String max = product.getChildText("MAX");
                            String advance = product.getChildText("ADVANCE");
                            String ticket = product.getChildText("TICKET");
                            //取消规则
                            String canceldesc = "";//入库的取消规则
                            String cancel = product.getChildText("CANCEL");
                            List<Element> rooms = product.getChildren("ROOM");
                            if (rooms != null && rooms.size() > 0 && cancel != null && !"".equals(cancel.trim())) {
                                String cancelurl = HMRequestUitl.getHMRequestUrlHeader()
                                        + "&api=qcancel&p_lang=SIM&p_contract=" + hmcontract + "&p_cancel=" + cancel;
                                String cancelxml = Util.getStr(cancelurl);
                                SAXBuilder cancelBuilder = new SAXBuilder();
                                Document cancelDoc = cancelBuilder.build(new StringReader(cancelxml));
                                Element cancelRoot = cancelDoc.getRootElement();
                                Element cancelResult = cancelRoot.getChild("XML_RESULT");
                                List<Element> cancelContracts = cancelResult.getChildren("CONTRACTS");
                                if (cancelContracts != null && cancelContracts.size() > 0) {
                                    for (Element cancelele : cancelContracts) {
                                        List<Element> CANCELLATION = cancelele.getChildren("CANCELLATION");
                                        if (CANCELLATION != null && CANCELLATION.size() > 0) {
                                            Element cancelAtionEle = CANCELLATION.get(0);
                                            String cancelnum = cancelAtionEle.getChildText("CANCEL");
                                            String cancelstr = cancelAtionEle.getChildText("CANCELDESC");
                                            if (cancel.equals(cancelnum) && cancelstr != null
                                                    && !"".equals(cancelstr.trim())) {
                                                canceldesc = cancelstr;
                                            }
                                        }
                                    }
                                }
                            }
                            for (Element room : rooms) {
                                // 床型
                                String type = room.getChildText("TYPE");
                                // 价格关联床型
                                String c_type = "";
                                List<Bedtype> bedtypefromtable = Server
                                        .getInstance()
                                        .getHotelService()
                                        .findAllBedtype(" WHERE " + Bedtype.COL_type + "='" + type.trim() + "'", "",
                                                -1, 0);

                                if (bedtypefromtable != null && bedtypefromtable.size() > 0) {
                                    c_type = bedtypefromtable.get(0).getId() + "";
                                }
                                else {
                                    Qtype.getType(type.trim());
                                    List<Bedtype> t = Server
                                            .getInstance()
                                            .getHotelService()
                                            .findAllBedtype(" WHERE " + Bedtype.COL_type + "='" + type.trim() + "'",
                                                    "", -1, 0);
                                    if (t != null && t.size() > 0) {
                                        c_type = t.get(0).getId() + "";
                                    }
                                }
                                // 房型
                                String hmroomtype = room.getChildText("CAT");
                                // 价格关联房型
                                long c_roomtypeid = 0l;
                                List<Roomtype> roomtypefromtable = Server
                                        .getInstance()
                                        .getHotelService()
                                        .findAllRoomtype(
                                                " WHERE " + Roomtype.COL_roomcode + "='" + hmroomtype + "' AND "
                                                        + Roomtype.COL_hotelid + "='" + hotelid + "' AND "
                                                        + Roomtype.COL_bed + "='" + c_type.trim() + "'", "", -1, 0);
                                if (roomtypefromtable != null && roomtypefromtable.size() > 0) {
                                    c_roomtypeid = roomtypefromtable.get(0).getId();
                                }
                                else {
                                    new HuaminQhotelcattypeDb().getQhotelcattypetemp(hotelid);
                                    // 房型代码
                                    List<Roomtype> roomtypefromtable1 = Server
                                            .getInstance()
                                            .getHotelService()
                                            .findAllRoomtype(
                                                    " WHERE " + Roomtype.COL_roomcode + "='" + hmroomtype + "' AND "
                                                            + Roomtype.COL_hotelid + "='" + hotelid + "' AND "
                                                            + Roomtype.COL_bed + "='" + c_type.trim() + "'", "", -1, 0);
                                    if (roomtypefromtable1 != null && roomtypefromtable1.size() > 0) {
                                        c_roomtypeid = roomtypefromtable1.get(0).getId();
                                    }
                                }
                                // 服务包
                                String serv = room.getChildText("SERV");
                                // 以包括早餐数量
                                String bf = room.getChildText("BF");
                                List<Element> stays = room.getChildren("STAY");
                                for (Element stay : stays) {
                                    Hmhotelprice hotelprice = new Hmhotelprice();
                                    hotelprice.setCountryid(nation);
                                    hotelprice.setCountryname(nationname);
                                    // 床型代码
                                    hotelprice.setType(c_type);
                                    // 房型代码
                                    hotelprice.setRoomtypeid(c_roomtypeid);
                                    // 服务包
                                    hotelprice.setServ(serv);
                                    if (rarate != null && !"".equals(rarate)) {
                                        hotelprice.setRatetype(rarate);
                                    }
                                    else {
                                        hotelprice.setRatetype("");
                                    }
                                    // 已包含早餐数量
                                    hotelprice.setBf(Long.parseLong(bf));
                                    hotelprice.setContractid(hmcontract);
                                    // 酒店合同版本号
                                    hotelprice.setContractver(hmcontractver);
                                    // 酒店代码
                                    hotelprice.setCityid(cityid);
                                    hotelprice.setHotelid(hotelid);
                                    // 货币
                                    hotelprice.setCur(cur);
                                    // 提醒代码
                                    hotelprice.setProd(prod);
                                    hotelprice.setMinday(Long.parseLong(min.trim()));
                                    // 最多停留晚数
                                    hotelprice.setMaxday(Long.parseLong(max.trim()));
                                    // 提前预定天数
                                    hotelprice.setAdvancedday(Long.parseLong(advance));
                                    // 有无机票 1有机票 0 无机票
                                    hotelprice.setTicket(ticket);
                                    SimpleDateFormat sd = new SimpleDateFormat("dd-M-yy");
                                    String deadline = room.getChildText("DEADLINE");
                                    // 最后期限
                                    hotelprice.setDeadline(sdf.format(sd.parse(deadline)));
                                    String statedate = stay.getChildText("STAYDATE");
                                    // 日期
                                    hotelprice.setStatedate(sdf.format(sd.parse(statedate)));
                                    String price = stay.getChildText("PRICE");
                                    try {
                                        // 价格
                                        hotelprice.setPrice(Double.parseDouble(price));
                                        // 可维护的酒店价格
                                        hotelprice.setPriceoffer(Double.parseDouble(price));
                                        // 去哪的价格
                                        hotelprice.setQunarprice(Double.parseDouble(price));
                                    }
                                    catch (Exception e) {
                                        WriteLog.write("录入酒店价格出错", "酒店id:" + hotelprice.getHotelid());
                                    }
                                    String allot = stay.getChildText("ALLOT");
                                    hotelprice.setAllot(allot);
                                    // Y-房型已獲得分配, 能夠即時確認 N-房型未獲得分配, 等待回覆 C-酒店關閉
                                    String isallot = stay.getChildText("IS_ALLOT");
                                    if ("Y".equals(isallot)) {
                                        hotelprice.setRoomstatus(0l);
                                        hotelprice.setYuliuNum(0l);
                                    }
                                    else if ("N".equals(isallot)) {
                                        hotelprice.setRoomstatus(0l);
                                        hotelprice.setYuliuNum(0l);
                                    }
                                    else if ("C".equals(isallot)) {
                                        hotelprice.setRoomstatus(1l);
                                        hotelprice.setYuliuNum(0l);
                                    }
                                    hotelprice.setUpdatetime(timeformat.format(new Date(System.currentTimeMillis())));
                                    hotelprice.setIsallot(isallot);
                                    hotelprice.setAbleornot(0);
                                    hotelprice.setSourcetype("3");
                                    hotelprice.setCanceldesc(canceldesc);
                                    listprice.add(hotelprice);
                                }
                            }
                        }
                    }
                    else {
                        System.out.println(hmhotelid + ":不可预订");
                        WriteLog.write("不可预订", hmhotelid + ":不可预订");
                        Server.getInstance().getSystemService()
                                .findMapResultBySql("delete from t_hmhotelprice where c_hotelid=" + hotelid, null);
                        System.out.println("删除成功……");
                    }
                }
            }
            if (listprice.size() > 0) {
                for (int i = 0; i < listprice.size(); i++) {
                    Hmhotelprice price = listprice.get(i);
                    System.out.println(price);
                    String str = "[dbo].[sp_inserthmhotelprice]" + "@contractid = N'" + price.getContractid() + "',"
                            + "@ver = N'" + price.getContractver() + "'," + "@hotelid = " + price.getHotelid() + ","
                            + "@cur = N'" + price.getCur() + "'," + "@prod = N'" + price.getProd() + "',"
                            + "@country = N'" + price.getCountryid() + "'," + "@countryname = N'"
                            + price.getCountryname() + "'," + "@maxday = " + price.getMaxday() + "," + "@advanceday = "
                            + price.getAdvancedday() + "," + "@ticket = N'" + price.getTicket() + "',"
                            + "@roomtypeid = " + price.getRoomtypeid() + "," + "@type = N'" + price.getType() + "',"
                            + "@serv = N'" + price.getServ() + "'," + "@bf = " + price.getBf() + "," + "@deadline = N'"
                            + price.getDeadline() + "'," + "@statedate = N'" + price.getStatedate() + "',"
                            + "@price = " + price.getPrice() + "," + "@allot = N'" + price.getAllot() + "',"
                            + "@isallot = N'" + price.getIsallot() + "'," + "@minday = " + price.getMinday() + ","
                            + "@priceoffer = " + price.getPriceoffer() + "," + "@qunarprice = " + price.getQunarprice()
                            + "," + "@ableornot = " + price.getAbleornot() + "," + "@updatetime = N'"
                            + price.getUpdatetime() + "'," + "@yuliunum = N'" + price.getYuliuNum() + "',"
                            + "@roomstatus = N'" + price.getRoomstatus() + "'," + "@cityid = N'" + price.getCityid()
                            + "'," + "@sourcetype = N'" + price.getSourcetype() + "'," + "@jlkeyid = N'"
                            + price.getJlkeyid() + "'," + "@jlratename = N'" + price.getRatetype() + "',"
                            + "@jltime = N'" + price.getJltime() + "'," + "@jlallment = N'" + price.getAllotmenttype()
                            + "'," + "@canceldesc = N'" + price.getCanceldesc() + "'";
                    synchronized (price) {
                        List resultt = Server.getInstance().getSystemService().findMapResultByProcedure(str);
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 录入酒店价格
     * 
     * @param hotelid
     * @param xmlstr
     *            getQrate(hotelid,roomtype.getId(),roomtype.getBed(), new
     *            Date(), CatchNext31Day(new Date()));
     */

    /**
     * 将hotel表和roomtype表进行关联
     */
    public static void getcontract(long hotelid, String huamincode) {
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qcontract&p_lang=SIM&p_hotel="
                + huamincode.trim() + "&p_country=&p_city=";
        System.out.println(totalurl);
        String str = Util.getStr(totalurl);
        parsexml_contract(hotelid, str);
    }

    /**
     * 录入
     * 
     * @param hotelid
     * @param xmlstr
     */
    @SuppressWarnings("unchecked")
    public static void parsexml_contract(long hotelid, String xmlstr) {
        SAXBuilder sb = new SAXBuilder();
        try {
            Document doc = sb.build(new StringReader(xmlstr));
            Element root = doc.getRootElement();
            Element result = root.getChild("XML_RESULT");
            if (result.getChildren().size() > 2) {
                List<Element> contractlist = result.getChildren("CONTRACTS");
                for (Element contracts : contractlist) {
                    String contractid = contracts.getChildText("CONTRACT");
                    //parsexml_qserv(hotelid, contractid);
                    //getBenefit(hotelid, contractid);
                    //getCancel(hotelid, contractid);
                    getProd(hotelid, contractid);
                }
            }
            else {
                WriteLog.write("更新酒店合同", "酒店id：" + hotelid + "没有房型数据");

                List<Hotel> list = Server.getInstance().getHotelService()
                        .findAllHotel(" WHERE " + Hotel.COL_id + "=" + hotelid, "", -1, 0);
                if (list != null && list.size() == 1) {
                    Hotel hotel = list.get(0);
                    hotel.setState(0);//暂不可用
                    hotel.setStatedesc("华闽预付酒店，无可用房型，酒店暂不可用");
                    Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
                }
            }
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            String errormessage = result.getChildText("ERROR_MESSAGE");
            if (errormessage != null || !"".equals(errormessage)) {
                WriteLog.write("合同新增数据", "错误信息:" + resultco + "," + errormessage);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取房型名称
     * 
     * @param xml
     */
    public static String parse_cat(String xml) {
        SAXBuilder sb = new SAXBuilder();
        Document document;
        String catname = "";
        try {
            document = sb.build(new StringReader(xml));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            Element errormessage = result.getChild("ERROR_MESSAGE");
            List<Element> categories = result.getChildren("CATEGORIES");
            if (categories != null && categories.size() > 0) {
                for (Element category : categories) {
                    String catcode = category.getChildText("CAT");
                    catname = category.getChildText("CATNAME");
                }
            }
        }
        catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return catname;
    }

    /**
     * 将hotel表和hotelcancel表进行关联
     * 
     * @param hotelid
     * @param contractid
     */
    private static void getCancel(long hotelid, String contractid) {
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qcancel&p_lang=SIM&p_contract=" + contractid
                + "&p_cancel=&p_country=&p_city=";
        System.out.println(totalurl);
        String str = Util.getStr(totalurl);
        parsexml_cancel(hotelid, contractid, str);
    }

    /**
     * 取消规则
     * 
     * @param hotelid
     * @param contractid
     * @param str
     */
    @SuppressWarnings("unchecked")
    private static void parsexml_cancel(long hotelid, String contractid, String xmlstr) {
        SAXBuilder build = new SAXBuilder();
        Document document;
        try {
            document = build.build(new StringReader(xmlstr));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            if (result.getChildren().size() > 2) {
                Element contracts = result.getChild("CONTRACTS");
                String hmcontractid = contracts.getChildText("CONTRACT");
                String hmcontractver = contracts.getChildText("VER");
                List<Element> cancellation = contracts.getChildren("CANCELLATION");
                for (Element cancel : cancellation) {
                    hotelcancel hotelcancel = new hotelcancel();
                    String calcelcode = cancel.getChildText("CANCEL");
                    String canceldesc = cancel.getChildText("CANCELDESC");
                    hotelcancel.setConractid(hmcontractid);
                    hotelcancel.setConractver(hmcontractver);
                    hotelcancel.setHotelid(hotelid);
                    hotelcancel.setCancelid(calcelcode);
                    hotelcancel.setCanceldesc(canceldesc);
                    List<hotelcancel> hotelcancelfromtable = Server
                            .getInstance()
                            .getHotelService()
                            .findAllhotelcancel(
                                    "WHERE " + hotelcancel.COL_hotelid + "='" + hotelcancel.getHotelid() + "' AND "
                                            + hotelcancel.COL_cancelid + "='" + hotelcancel.getCancelid() + "'",
                                    "order by id desc", -1, 0);
                    if (hotelcancelfromtable.size() > 0) {
                        for (int i = 0; i < hotelcancelfromtable.size(); i++) {
                            if (i == 0) {
                                hotelcancel.setId(hotelcancelfromtable.get(i).getId());
                                Server.getInstance().getHotelService().updatehotelcancelIgnoreNull(hotelcancel);
                                System.out.println("更新取消规则……");
                            }
                            else {
                                Server.getInstance().getHotelService()
                                        .deletehotelcancel(hotelcancelfromtable.get(i).getId());
                                System.out.println("删除取消规则……");
                            }
                        }
                    }
                    else {
                        Server.getInstance().getHotelService().createhotelcancel(hotelcancel);
                        System.out.println("新增取消规则……");
                    }
                }
            }
            else {
                WriteLog.write("酒店取消陈述数据", "暂无信息:合同id" + contractid);
            }
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            String errormessage = result.getChildText("ERROR_MESSAGE");
            if (errormessage != null || !"".equals(errormessage)) {
                WriteLog.write("酒店取消陈述数据", "错误信息:" + resultco + "," + errormessage);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 录入与酒店关联的服务
     * 
     * 
     * @param hotelid
     * @param contractid
     * @param xml
     */
    @SuppressWarnings({ "unchecked", "static-access" })
    public static void parsexml_qserv(long hotelid, String contractid) {
        // 服务
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qserv&p_lang=SIM&p_contract="
                + contractid.trim() + "&p_serv=&p_country=&p_city=";
        System.out.println("服务访问地址:" + totalurl);
        String serv = Util.getStr(totalurl);
        SAXBuilder build = new SAXBuilder();
        Document document;
        try {
            document = build.build(new StringReader(serv));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            if (result.getChildren().size() > 2) {
                Element contracts = result.getChild("CONTRACTS");
                String hmcontractid = contracts.getChildText("CONTRACT");
                String hmcontractver = contracts.getChildText("VER");
                String cur = contracts.getChildText("CUR");
                List<Element> services = contracts.getChildren("SERVICE");
                for (Element service : services) {
                    String serverid = service.getChildText("SERV");
                    List<Element> items = service.getChildren("ITEM");
                    Label: for (Element item : items) {
                        String servercode = item.getChildText("SERVCODE");
                        String servername = item.getChildText("SERVNAME");
                        String price = item.getChildText("PRICE");
                        try {
                            Double p = Double.valueOf(price);
                            if (p > 0) {

                            }
                            else {
                                continue Label;
                            }
                        }
                        catch (RuntimeException e1) {
                            continue Label;
                        }
                        String min = item.getChildText("MIN");
                        String max = item.getChildText("MAX");
                        hotelserv hotelserv = new hotelserv();
                        hotelserv.setHotelid(hotelid);
                        hotelserv.setContractid(contractid);
                        hotelserv.setCur(cur);
                        hotelserv.setServ(serverid);
                        hotelserv.setServcode(servercode);
                        hotelserv.setServname(servername);
                        hotelserv.setPrice(price);
                        hotelserv.setContractver(hmcontractver);
                        try {
                            Long maxcount = Long.parseLong(max.trim());
                            Long mincount = Long.parseLong(min.trim());
                            hotelserv.setMaxcount(maxcount);
                            hotelserv.setMincount(mincount);
                        }
                        catch (Exception e) {
                            WriteLog.write("酒店服务包新增数据", "最大数量或最小数量转换异常：酒店合同id：" + contractid + "：max" + max + ":min"
                                    + min);
                        }
                        List<hotelserv> hotelservfromtable = Server
                                .getInstance()
                                .getHotelService()
                                .findAllhotelserv(
                                        "WHERE " + hotelserv.COL_hotelid + "='" + hotelserv.getHotelid() + "' AND "
                                                + hotelserv.COL_serv + "='" + hotelserv.getServ() + "' AND "
                                                + hotelserv.COL_servcode + "='" + hotelserv.getServcode() + "'",
                                        "order by id desc ", -1, 0);
                        if (hotelservfromtable.size() > 0) {
                            for (int i = 0; i < hotelservfromtable.size(); i++) {
                                if (i == 0) {
                                    hotelserv.setId(hotelservfromtable.get(i).getId());
                                    Server.getInstance().getHotelService().updatehotelservIgnoreNull(hotelserv);
                                    System.out.println("更新服务:" + servername);
                                }
                                else {
                                    Server.getInstance().getHotelService()
                                            .deletehotelserv(hotelservfromtable.get(i).getId());
                                    System.out.println("删除服务:" + servername);
                                }
                            }
                        }
                        else {
                            Server.getInstance().getHotelService().createhotelserv(hotelserv);
                            System.out.println("添加服务:" + servername);
                        }
                    }
                }
            }
            else {
                WriteLog.write("酒店服务包新增数据", "酒店合同id：" + contractid + "：酒店产品没数据");
            }
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            String errormessage = result.getChildText("ERROR_MESSAGE");
            if (errormessage != null || !"".equals(errormessage)) {
                WriteLog.write("酒店服务包新增数据", "错误信息:" + resultco + "," + errormessage);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 优点资料
    public static void getBenefit(long hotelid, String contractid) {
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qbene&p_lang=SIM&p_contract=" + contractid
                + "&p_bene=&p_country=&p_city=";
        System.out.println(totalurl);
        String str = Util.getStr(totalurl);
        parsexml_benefit(hotelid, contractid, str);
    }

    // 录入优点资料
    @SuppressWarnings("unchecked")
    private static void parsexml_benefit(long hotelid, String contractid, String xml) {
        SAXBuilder build = new SAXBuilder();
        Document document;
        try {
            document = build.build(new StringReader(xml));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            if (result.getChildren().size() > 2) {
                Element contracts = result.getChild("CONTRACTS");
                String hmcontractver = contracts.getChildText("VER");
                List<Element> benefit = contracts.getChildren("BENEFIT");
                for (Element bft : benefit) {
                    String benefitcode = bft.getChildText("BENE");
                    String benefitdesc = bft.getChildText("BENEDESC");
                    hotelbene hotelbene = new hotelbene();
                    hotelbene.setContracid(contractid);
                    hotelbene.setContracver(hmcontractver);
                    hotelbene.setHotelid(hotelid);
                    hotelbene.setBene(benefitcode);
                    hotelbene.setBenedesc(benefitdesc);
                    List<hotelbene> hotelbenefromtable = Server
                            .getInstance()
                            .getHotelService()
                            .findAllhotelbene(
                                    "WHERE " + hotelbene.COL_hotelid + "='" + hotelbene.getHotelid() + "' " + " AND "
                                            + hotelbene.COL_bene + "='" + hotelbene.getBene() + "'",
                                    " order by id desc ", -1, 0);
                    if (hotelbenefromtable.size() > 0) {
                        for (int i = 0; i < hotelbenefromtable.size(); i++) {
                            if (i == 0) {
                                hotelbene.setId(hotelbenefromtable.get(i).getId());
                                Server.getInstance().getHotelService().updatehotelbeneIgnoreNull(hotelbene);
                                System.out.println("更新酒店优点……" + hotelbene.getBenedesc());
                            }
                            else {
                                Server.getInstance().getHotelService()
                                        .deletehotelbene(hotelbenefromtable.get(i).getId());
                                System.out.println("删除酒店优点……");
                            }
                        }
                    }
                    else {
                        Server.getInstance().getHotelService().createhotelbene(hotelbene);
                        System.out.println("创建酒店优点……" + hotelbene.getBenedesc());
                    }
                }
            }
            else {
                WriteLog.write("酒店优点资料新增数据", "该酒店合同优点没数据" + ",合同id：" + contractid);
            }
            Element resultcode = result.getChild("RETURN_CODE");
            String resultco = resultcode.getText();
            String errormessage = result.getChildText("ERROR_MESSAGE");
            if (errormessage != null || !"".equals(errormessage)) {
                WriteLog.write("酒店优点资料新增数据", "错误信息:" + resultco + "," + errormessage);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 产品代码
    public static void getProd(long hotelid, String contractid) {
        String totalurl = HMRequestUitl.getHMRequestUrlHeader() + "&api=qprod&p_lang=SIM&p_contract=" + contractid
                + "&p_prod=&p_country=&p_city=";
        System.out.println(totalurl);
        String str = Util.getStr(totalurl);
        System.out.println(hotelid + "-------------");
        parsexml_prod(hotelid, contractid, str);
    }

    // 进行关联
    @SuppressWarnings("unchecked")
    public static void parsexml_prod(long hotelid, String contractid, String xml) {
        SAXBuilder build = new SAXBuilder();
        Document document;
        try {
            document = build.build(new StringReader(xml));
            Element root = document.getRootElement();
            Element result = root.getChild("XML_RESULT");
            if (result.getChildren().size() > 2) {
                Element contracts = result.getChild("CONTRACTS");
                // String contid = contracts.getChildText("CONTRACT");
                String ver = contracts.getChildText("VER");
                // String hmhotelid = contracts.getChildText("HOTEL");
                // String hotelname = contracts.getChildText("HOTELNAME");
                List<Element> products = contracts.getChildren("PRODUCT");
                for (Element product : products) {
                    String prod = product.getChildText("PROD");
                    String ratetype = product.getChildText("RATETYPE");
                    String nation = product.getChildText("NATION");
                    String nationname = product.getChildText("NATIONNAME");
                    String min = product.getChildText("MIN");
                    String max = product.getChildText("MAX");
                    String advance = product.getChildText("ADVANCE");
                    String ticket = product.getChildText("TICKET");
                    String prombuy = product.getChildText("PROMBUY");
                    String promget = product.getChildText("PROMGET");
                    String promdisc = product.getChildText("PROMDISC");
                    String promqty = product.getChildText("PROMQTY");
                    String benefit = product.getChildText("BENEFIT");
                    String benefitdesc = product.getChildText("BENEFITDESC");
                    String cancel = product.getChildText("CANCEL");
                    String calceldesc = product.getChildText("CANCELDESC");

                    hotelproduct hotelproduct = new hotelproduct();
                    hotelproduct.setHmcontractid(contractid);
                    hotelproduct.setHmcontractver(ver);
                    hotelproduct.setHotelid(hotelid);
                    hotelproduct.setProd(prod);
                    hotelproduct.setRatetype(ratetype);
                    // hotelproduct.setNationid();// 暂时先入库国内数据
                    hotelproduct.setNationname(nationname);
                    try {
                        hotelproduct.setMinday(Long.parseLong(min.trim()));
                        hotelproduct.setMaxday(Long.parseLong(max.trim()));
                        hotelproduct.setAdvance(Long.parseLong(advance.trim()));
                        hotelproduct.setPrombuy(Long.parseLong(prombuy.trim()));
                        hotelproduct.setBenefitid(benefit.trim());
                        hotelproduct.setPromqty(Long.parseLong(promqty.trim()));
                        hotelproduct.setTicket(Long.parseLong(ticket.trim()));
                        hotelproduct.setPromget(Long.parseLong(promget.trim()));
                    }
                    catch (Exception e) {
                        WriteLog.write("酒店产品数据", "转型失败：min" + min + "：合同id：" + contractid + ":max：" + max + ":advance"
                                + advance + ":prombuy：" + prombuy + ":benefit：" + benefit + ":promqty" + promqty
                                + ":ticket" + ticket + ":" + promget);
                    }
                    hotelproduct.setPromdisc(promdisc);
                    hotelproduct.setBenifitdesc(benefitdesc);
                    hotelproduct.setCancel(cancel.trim());
                    hotelproduct.setCanceldesc(calceldesc);
                    hotelproduct.setPaytype(2l);
                    List<hotelproduct> hotelproductfromtable = Server
                            .getInstance()
                            .getHotelService()
                            .findAllhotelproduct(
                                    "WHERE " + hotelproduct.COL_hotelid + "=" + hotelproduct.getHotelid() + " AND "
                                            + hotelproduct.COL_prod + "='" + hotelproduct.getProd() + "'",
                                    "order by id desc", -1, 0);
                    if (hotelproductfromtable.size() > 0) {
                        for (int i = 0; i < hotelproductfromtable.size(); i++) {
                            if (i == 0) {
                                hotelproduct.setId(hotelproductfromtable.get(0).getId());
                                Server.getInstance().getHotelService().updatehotelproductIgnoreNull(hotelproduct);
                                System.out.println("更新prod信息……");
                            }
                            else {
                                Server.getInstance().getHotelService()
                                        .deletehotelproduct(hotelproductfromtable.get(0).getId());
                                System.out.println("删除prod信息……");
                            }
                        }
                    }
                    else {
                        Server.getInstance().getHotelService().createhotelproduct(hotelproduct);
                        System.out.println("创建prod信息……");
                    }
                }
            }
            else {
                WriteLog.write("酒店产品prod新增数据", "酒店合同id：" + contractid + "：酒店产品没数据");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前日期31天后的日期
     * 
     * @return
     */
    public static Date CatchNext31Day(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        Date newdate = cal.getTime();
        return newdate;
    }

    public static Date Tomorowday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Date newdate = cal.getTime();
        return newdate;
    }

    public static Date Today(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 0);
        Date newdate = cal.getTime();
        return newdate;
    }

    /**
     * 获取指定日期的前一天
     * 
     * @return
     */
    public static Date CatchYesterday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.roll(Calendar.DATE, -1);
        Date newdate = cal.getTime();
        return newdate;
    }

}
