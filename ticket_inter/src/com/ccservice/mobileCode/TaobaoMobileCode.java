/**
 * 
 */
package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;

/**
 * 淘宝提供给航天验证码
 * 
 * @time 2015年9月26日 下午12:54:13
 * @author chendong
 */
public class TaobaoMobileCode implements IMobileCode {
    public static void main(String[] args) {
        TaobaoMobileCode taobaoMobileCode = TaobaoMobileCode.getInstance("");
        //        String mobile = taobaoMobileCode.GetMobilenum("", "", "");

        String mobile = "13097816084";
        System.out.println(mobile);
        String result = taobaoMobileCode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        System.out.println(result);
        //       String result =  taobaoMobileCode.AddIgnoreList("", mobile, "", "");
        //       System.out.println(result);
        //        String result = taobaoMobileCode.send(mobile, "");
        //        System.out.println(result);
        //        taobaoMobileCode.send(mobile, "999");

    }

    //    final static String URL = "http://localhost:9028/12306SearchMonitor/";

    final static String URL = "http://tc12306smsmsg.hangtian123.com/";

    // 获取手机号码
    final static String GETMOBILENUMURL = URL + "SelectCellPhoneInfo?action=getMobilenum";

    // 获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = URL + "SelectCellPhoneInfo?action=getVcodeAndReleaseMobile";

    // 加黑手机号码
    final static String ADDIGNORELISTURL = URL + "SelectCellPhoneInfo?action=addIgnoreList";

    final static String SEND = URL + "SelectCellPhoneInfo";

    /**
     * 
     * 
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static TaobaoMobileCode getInstance(String pid) {
        String uid = "";// 12306
        TaobaoMobileCode TaobaoMobileCode = new TaobaoMobileCode();
        return TaobaoMobileCode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String,
     * java.lang.String)
     */
    @Override
    public String LoginIn(String uid, String pwd) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        // String paramContent = "";
        String paramContent = "mobileType=2";// [CellPhoneNumberSMS_TongchengReplace] // 新库的手机号
        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();// .Post(API.LoginIn,
        WriteLog.write("TaobaoMobileCode_GetMobilenum", paramContent + ":GetMobilenum:" + result);
        String[] results = result.split("[|]");
        if (results.length == 2) {
            result = results[0];
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang
     * .String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ccservice.mobileCode.IMobileCode#getVcodeAndReleaseMobile(java.lang
     * .String, java.lang.String, java.lang.String, java.lang.String,
     * java.lang.String)
     */
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String paramContent = "mobile=" + mobile + "&mobileType=2";
        String result = "-1";
        result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
        WriteLog.write("TaobaoMobileCode_getVcodeAndReleaseMobile", paramContent + ":getVcodeAndReleaseMobile:"
                + result);
        if (result.contains("not_receive")) {
            result = "no_data";
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        // String paramContent = "&mobile=" + mobiles;
        String paramContent = "&mobileType=2&mobile=" + mobiles;
        String result = SendPostandGet.submitPost(ADDIGNORELISTURL, paramContent, "utf-8").toString();// .Post(API.LoginIn,
                                                                                                      // String.Format("uid={0}&pwd={1}",
                                                                                                      // uid, pwd));
        WriteLog.write("TongHangMobileCode_AddIgnoreList", paramContent + ":AddIgnoreList:" + result);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String,
     * java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String,
     * java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        //#TODO mark#chendong
        //使用 SendPostandGet.submitPost 调用SEND地址(上面已定义)的发送短信的方法
        //action=sendsms&mobileType=2&mobile=mobile&content=content四个参数

        String paramString = "action=sendsms&mobileType=2&mobile=" + mobile + "&content=" + content;
        String result = SendPostandGet.submitPost(SEND, paramString, "utf-8").toString();
        if (result != null || !"".equals(result)) {
            result = "true";
        }
        else {
            result = "false";
        }
        return result;
        //发送成功返回字符串"true";发送失败返回字符串"false";
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
