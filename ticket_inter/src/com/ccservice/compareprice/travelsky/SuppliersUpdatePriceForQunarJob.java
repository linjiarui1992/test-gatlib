package com.ccservice.compareprice.travelsky;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.text.SimpleDateFormat;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceDetail;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceResponse;

public class SuppliersUpdatePriceForQunarJob implements Job {

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String updateDataTime = PropertyUtil.getValue("updateDataTime").trim();

    private int catchdays = Integer.parseInt(PropertyUtil.getValue("catchdays").trim());

    public void execute(JobExecutionContext context) throws JobExecutionException {
        updateHotel();
    }

    public static void main(String[] args) {
        new SuppliersUpdatePriceForQunarJob().updateHotel();
    }

    //更新时间判断
    private void timeWait() throws Exception {
        try {
            //TIME
            Date current = format.parse(format.format(new Date()));
            Date start = format.parse(updateDataTime.split("-")[0]);
            Date end = format.parse(updateDataTime.split("-")[1]);
            if (current.before(start)) {
                long wait = start.getTime() - current.getTime();
                System.out.println("更新时间未到，等待" + wait / 1000 + "秒。");
                Thread.sleep(wait);
            }
            if (current.after(end)) {
                Date _24 = format.parse("24:00:00");
                Date _00 = format.parse("00:00:00");
                long wait = _24.getTime() - current.getTime() + start.getTime() - _00.getTime();
                System.out.println("更新时间未到，等待" + wait / 1000 + "秒。");
                Thread.sleep(wait);
            }
        }
        catch (Exception e) {
        }
    }

    @SuppressWarnings("unchecked")
    private void updateHotel() {
        while (true) {
            try {
                String sql = "h where h.C_PUSH = 2 and h.C_HOTELCODE != '' and exists (select 'YES' from T_HOTELGOODDATA where C_HOTELID = h.ID)";
                List<Hotel> list = Server.getInstance().getHotelService().findAllHotel(sql, "order by C_CITYID", -1, 0);
                for (Hotel hotel : list) {
                    timeWait();//判断更新时间
                    try {
                        long hid = hotel.getId();
                        //本地房型
                        String where = "where C_ROOMCODE != '' and C_HOTELID  = " + hid;
                        List<Roomtype> localRoomList = Server.getInstance().getHotelService()
                                .findAllRoomtype(where, "", -1, 0);
                        if (localRoomList == null || localRoomList.size() == 0 || hotel.getState() == null
                                || hotel.getState().intValue() != 3) {
                            delete(hid, hotel.getName());
                            continue;
                        }
                        Map<String, Roomtype> localRoomMap = new HashMap<String, Roomtype>();
                        for (Roomtype r : localRoomList) {
                            localRoomMap.put(r.getRoomcode().trim(), r);
                        }
                        //本地数据
                        Map<String, HotelGoodData> localDataMap = new HashMap<String, HotelGoodData>();
                        List<HotelGoodData> localDataList = Server.getInstance().getHotelService()
                                .findAllHotelGoodData("where C_HOTELID = " + hid, "", -1, 0);
                        for (HotelGoodData d : localDataList) {
                            localDataMap.put(d.getJlkeyid(), d);
                        }
                        //时间
                        String startDate = ElongHotelInterfaceUtil.getCurrentDate();
                        String endDate = ElongHotelInterfaceUtil.getAddDate(startDate, catchdays);
                        //中航信
                        if (hotel.getSourcetype() == 12) {
                            Map<String, HXHotelPriceResponse> hxMap = RequestSuppliersPrice.loadHX(hotel, startDate,
                                    endDate);
                            if (hxMap == null || hxMap.size() == 0) {
                                delete(hid, hotel.getName());
                                continue;
                            }
                            hxUpdate(hotel, hxMap, localDataMap);
                        }
                    }
                    catch (Exception e) {
                        System.out.println("酒店 [" + hotel.getName().trim() + " ]上去哪儿，更新数据，"
                                + ElongHotelInterfaceUtil.errormsg(e));
                    }
                }
            }
            catch (Exception e) {
                System.out.println("上去哪儿，更新数据，" + ElongHotelInterfaceUtil.errormsg(e));
            }
        }
    }

    //中航信更新
    private void hxUpdate(Hotel hotel, Map<String, HXHotelPriceResponse> hxMap, Map<String, HotelGoodData> localDataMap) {
        int count = 0;
        for (String hxKey : hxMap.keySet()) {
            try {
                //航信房型
                HXHotelPriceResponse hxData = hxMap.get(hxKey);
                //价格明细
                Map<String, HXHotelPriceDetail> hxRates = hxData.getRates();
                for (String date : hxRates.keySet()) {
                    HXHotelPriceDetail detail = hxRates.get(date);
                    HotelGoodData data = RequestSuppliersPrice.hxDataToLocal(hxData, detail, date);
                    String keyid = data.getJlkeyid();
                    //更新
                    HotelGoodData old = localDataMap.get(keyid);
                    if (old != null) {
                        //无变化
                        if (!RequestSuppliersPrice.NeedUpdate(old, data, -12)) {//-12：中航信更新
                            localDataMap.remove(keyid);
                            continue;
                        }
                        else {
                            System.out.println("数据变化，需更新~~~" + hotel.getName() + "~~~" + old.getRoomtypename() + "~~~"
                                    + old.getDatenum());
                            //变价，无法获取比价后的卖价，关房处理
                            if (old.getBaseprice().doubleValue() != data.getBaseprice().doubleValue()) {
                                data.setRoomstatus(1l);//0：开房  ；1：关房
                            }
                            data.setId(old.getId());
                            data.setUpdatetime(sdf.format(new Date()));
                            Server.getInstance().getHotelService().updateHotelGoodDataIgnoreNull(data);
                            localDataMap.remove(keyid);
                            count++;
                        }
                    }
                }
            }
            catch (Exception e) {
                System.out.println(ElongHotelInterfaceUtil.errormsg(e));
            }
        }
        if (localDataMap.size() > 0) {
            String ids = "";
            for (String keyid : localDataMap.keySet()) {
                HotelGoodData old = localDataMap.get(keyid);
                if (old.getRoomstatus() == null || old.getRoomstatus().longValue() != 1) {
                    ids += old.getId() + ",";
                    System.out.println(hotel.getName() + "===关房===" + old.getRoomtypename() + "===" + old.getDatenum());
                }
            }
            if (!ElongHotelInterfaceUtil.StringIsNull(ids)) {
                ids = ids.substring(0, ids.length() - 1);
                //关房
                String closesql = "update T_HOTELGOODDATA set C_ROOMSTATUS = 1 , C_UPDATETIME = '"
                        + sdf.format(new Date()) + "' where ID in (" + ids + ")";
                Server.getInstance().getSystemService().findMapResultBySql(closesql, null);
                count++;
            }
        }
        if (count > 0) {
            String updatesql = "update t_hotel set c_goqunarupdatetime = '" + sdf.format(new Date()) + "' where id = "
                    + hotel.getId();
            Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
        }
    }

    //删除数据
    private void delete(long hotelid, String hotelname) {
        String delSql = "delete from t_hotelgooddata where c_hotelid = " + hotelid
                + ";update t_hotel set c_push = null , c_goqunarupdatetime = '" + sdf.format(new Date())
                + "' where id = " + hotelid;
        Server.getInstance().getSystemService().findMapResultBySql(delSql, null);
        System.out.println(hotelname + "===优势过时，删除旧优势数据===");
    }

}
