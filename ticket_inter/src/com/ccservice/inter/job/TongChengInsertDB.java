package com.ccservice.inter.job;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.mobileCode.DBHelperTongchengMobile;
import com.ccservice.mobileCode.TongChengSmsMethod;

public class TongChengInsertDB implements Job {
    TongChengSmsMethod tongChengSmsMethod;

    public static void main(String[] args) {
        try {
            startScheduler("0/1 0/1 0-23 * * ?");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        TongChengInsertDB tongChengInsertDB = new TongChengInsertDB();
        tongChengInsertDB.InsertDb();
    }

    //获取手机号并插入
    public void InsertDb() {
        String result = tongChengSmsMethod.get();
        System.out.println(result);
        boolean isSuccess = false;
        if (result != null && result.length() == 11) {
            long l1 = System.currentTimeMillis();
            String sql = " exec sp_TongChengPhoneSim_Insert @PhoneNumber = N'" + result + "'";
            try {
                for (int i = 0; i <= 10; i++) {
                    isSuccess = DBHelperTongchengMobile.executeSql(sql);
                    if (isSuccess == true) {
                        break;
                    }
                }
                WriteLog.write("TongChengMobileCode_GetMobilenum", l1 + ":" + isSuccess + ":fromDB:" + sql);
            }
            catch (Exception e) {
                e.printStackTrace();
                WriteLog.write("TongChengMobileCode_GetMobilenum_insert_err", l1 + ":" + e.getMessage() + ":fromDB:"
                        + sql);
            }
        }
    }

    // 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
    public static void startScheduler(String expr) throws Exception {
        JobDetail jobDetail = new JobDetail("TongChengInsertDB", "TongChengInsertDB", TongChengInsertDB.class);// 任务名，任务组名，任务执行类
        CronTrigger trigger = new CronTrigger("TongChengInsertDB", "TongChengInsertDBGroup", expr);// 触发器名，触发器组名
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
    }

    public TongChengInsertDB() {
        super();
        String channel = "hs";
        String password = "Btdvg0rRtBQBc1Q7";
        tongChengSmsMethod = new TongChengSmsMethod(channel, password);
    }

}