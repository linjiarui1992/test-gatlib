package com.ccservice.jielv.hotelupdate;

import java.util.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.ccservice.huamin.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.atom.service.IJLHotelService;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * 捷旅更新程序
 */
public class JLUpdateJob {

    private static int month = Integer.valueOf(PropertyUtil.getValue("jlCatchPriceMonth"));

    public static void main(String[] args) throws Exception, SQLException {
        updateJlPrice();
    }

    /**
     * 第一次更新捷旅基础数据job
     */
    public static void updateDataJob() {
        updateHotel();//第一次更新酒店
        updateRoomtype();//第一次更新房型
    }

    /**
     * 整体更新捷旅价格
     */
    public static void updateJlPrice() {
        System.out.println("初始化更新月数：" + month);
        try {
            updateHmHotelprice(month);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateJllastDayPrice() {
        System.out.println("初始化更新月数：" + month);
        try {
            updateJLLastDayprice(month);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 持续更新酒店房型
     */
    public static void updatejlhrJob() {
        updateBaseData();
    }

    /**
     * 持续更新价格
     */
    public static void updateJlpriceJob() {
        try {
            System.out.println("持续更新价格-初始化更新月数：" + month);
            updateChangePrice(month);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新捷旅城市
     */
    public static void updateCity() {
        Server.getInstance().getIJLHotelService().getCity();
    }

    /**
     * 更新捷旅行政区，商业区
     */
    public static void updateZone() {
        Server.getInstance().getIJLHotelService().getBizZone();
        Server.getInstance().getIJLHotelService().getZone();
    }

    /**
     * 更新捷旅酒店
     */
    public static void updateHotel() {
        Server.getInstance().getIJLHotelService().getHotels();
    }

    /**
     * 更新捷旅房型
     */
    @SuppressWarnings("unchecked")
    public static void updateRoomtype() {
        IJLHotelService ser = Server.getInstance().getIJLHotelService();
        List<Hotel> hotels = Server.getInstance().getHotelService()
                .findAllHotel("where c_sourcetype=6 and c_hotelcode is not null", "order by id asc", -1, 0);
        for (Hotel hotelsa : hotels) {
            ser.getRoomType(hotelsa.getId(), hotelsa.getHotelcode());
        }

    }

    /**
     * 更新酒店房型基础数据
     */
    public static void updateBaseData() {
        long time = Long.parseLong(PropertyUtil.getValue("jlbasedatatime"));// 更新间隔时间（秒）
        label: while (true) {
            try {
                long t1 = System.currentTimeMillis();
                //更新酒店
                Server.getInstance().getIJLHotelService().getUpHotel(time);
                //更新房型
                Server.getInstance().getIJLHotelService().getUpRoomType(time);
                long t2 = System.currentTimeMillis();
                long t3 = (t2 - t1) / 1000 + 1;
                System.out.println("更新一次基础酒店房型信息，用时" + t3 + "s");
                time = t3 + 600;
                if (time < 60) {
                    System.out.println("房型更新休息10分钟");
                    Thread.sleep(600000);
                    time = 600;
                }
            }
            catch (Exception e) {
                time = 600 + time;
                WriteLog.write("捷旅基础房型更新异常", e.getMessage());
                System.out.println("报异常，继续循环……");
                continue label;
            }
        }
    }

    /**
     * 每天更新一个月最后一天的价格
     */
    @SuppressWarnings("unchecked")
    public static void updateJLLastDayprice(int month) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, month);
        String starttime = sd.format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 1);
        String endtime = sd.format(cal.getTime());
        Server.getInstance()
                .getSystemService()
                .findMapResultBySql("delete from t_hmhotelprice where C_STATEDATE<'" + sd.format(new Date()) + "'",
                        null);
        List<Hotel> hotels = Server
                .getInstance()
                .getHotelService()
                .findAllHotel("where c_sourcetype=6 and c_cityid>0 and c_hotelcode is not null", "order by c_cityid",
                        -1, 0);
        long t1 = System.currentTimeMillis();
        int k = hotels.size();
        for (int i = 0; i < hotels.size(); i++) {
            System.out.println("剩余酒店：" + k--);
            Map<String, Long> roomtypeids = new HashMap<String, Long>();
            Hotel hotel = hotels.get(i);
            List<JLPriceResult> results = Server.getInstance().getIJLHotelService()
                    .getHotelPrice(hotel.getHotelcode(), starttime, endtime);
            for (int j = 0; j < results.size(); j++) {
                JLPriceResult result = results.get(j);
                String jlkeyid = result.getJlkeyid();
                String jltime = result.getJltime();
                String where = "where C_JLKEYID='" + jlkeyid + "'";
                List<Hmhotelprice> hd = Server.getInstance().getHotelService().findAllHmhotelprice(where, "", 1, 0);
                if (hd.size() > 0) {
                    if (!hd.get(0).getJltime().equals(jltime)) {//最后更新时间点不同，则更新
                        Hmhotelprice price = hd.get(0);
                        long hotelid = hotel.getId();
                        price.setJlkeyid(result.getJlkeyid());
                        int supplyid = -1;
                        try {
                            supplyid = Integer.valueOf(result.getSupplierid());
                        }
                        catch (NumberFormatException e) {
                        }
                        price.setSupplyid(supplyid);
                        price.setJltime(result.getJltime());
                        price.setContractid(result.getHotelcd());
                        price.setHotelid(hotelid);
                        price.setContractver("");
                        price.setCur(result.getCurrency());
                        if (!"RMB".equals(price.getCur())) {
                            continue;
                        }
                        price.setProd(result.getRatetypeid());
                        price.setMaxday(99l);
                        //最少连住
                        try {
                            price.setMinday(Long.valueOf(result.getMinDay()));
                        }
                        catch (Exception e) {
                            price.setMinday(1l);
                        }
                        //提前天数
                        try {
                            price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                        }
                        catch (Exception e) {
                            price.setAdvancedday(0l);
                        }
                        price.setTicket("0");
                        long roomtypeid = 0;
                        if (roomtypeids.containsKey(hotelid + "-" + result.getJlroomtype())) {
                            roomtypeid = roomtypeids.get(hotelid + "-" + result.getJlroomtype());
                        }
                        else {
                            List<Roomtype> rooms = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllRoomtype(
                                            "where c_hotelid=" + hotelid + " and C_ROOMCODE='" + result.getJlroomtype()
                                                    + "'", "", 1, 0);
                            if (rooms.size() == 1) {
                                roomtypeid = rooms.get(0).getId();
                                roomtypeids.put(hotelid + "-" + result.getJlroomtype(), rooms.get(0).getId());
                            }
                        }
                        if (roomtypeid == 0) {
                            continue;
                        }
                        price.setRoomtypeid(roomtypeid);
                        price.setType("");
                        price.setServ("");
                        String bf = result.getBreakfast();
                        if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                            bf = "";
                        }
                        price.setBreakfasttype(bf);
                        if (bf.equals("10")) {
                            price.setBf(0l);
                        }
                        else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                            price.setBf(1l);
                        }
                        else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                            price.setBf(2l);
                        }
                        else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                            price.setBf(3l);
                        }
                        else if (bf.equals("34")) {
                            price.setBf(6l);
                        }
                        else {
                            price.setBf(0l);
                        }
                        price.setStatedate(result.getStayDate());
                        String pricet = result.getPprice();
                        double saveprice = 0d;
                        try {
                            saveprice = Double.parseDouble(pricet);
                        }
                        catch (Exception e) {
                            saveprice = 0d;
                        }
                        if (saveprice <= 0) {
                            continue;
                        }
                        price.setPrice(saveprice);
                        price.setPriceoffer(saveprice);
                        price.setQunarprice(saveprice);
                        price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                        price.setSourcetype("6");
                        price.setCityid(hotel.getCityid().longValue() + "");
                        price.setRatetype(result.getRatetype());
                        String roomstatus = result.getAllot();
                        if ("12".equals(roomstatus)) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(99l);
                        }
                        else if ("16".equals(roomstatus)) {
                            price.setIsallot("C");
                            price.setRoomstatus(1l);
                            price.setYuliuNum(0l);
                        }
                        else {
                            try {
                                price.setYuliuNum(Long.parseLong(result.getFangliang()));
                            }
                            catch (Exception e) {
                                price.setYuliuNum(0l);
                            }
                            if (price.getYuliuNum() > 0) {
                                price.setIsallot("Y");
                                price.setRoomstatus(0l);
                            }
                            else {
                                price.setIsallot("N");
                                price.setRoomstatus(0l);
                                price.setYuliuNum(0l);
                            }
                        }
                        price.setAllotmenttype(result.getAllotmenttype());
                        //取消描述
                        price.setCanceldesc(result.getCanceldesc());
                        //类型：即订即保、提前多少天等
                        if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                                && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                            price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                        }
                        //提前天数
                        if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                                && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                            price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                        }
                        //提前时间  多少小时
                        if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                                && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                            price.setCanceltime(result.getTimeselect());
                        }
                        //不可修改、不可取消、两者
                        if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                                && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                            price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                        }
                        //不可修改内容
                        if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                                && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                            price.setNoedittype(result.getNoedit().trim());
                        }
                        //担保金额类型
                        if (result.getGuaranteeamounttype() != null
                                && !"".equals(result.getGuaranteeamounttype().trim())
                                && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                            price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                        }
                        if (roomtypeid != 0) {
                            String sql = "update T_HMHOTELPRICE set C_HOTELID=" + price.getHotelid() + ",C_CUR='"
                                    + price.getCur() + "',C_ADVANCEDDAY=" + price.getAdvancedday() + ",C_ROOMTYPEID="
                                    + price.getRoomtypeid().longValue() + ",C_BF=" + price.getBf().longValue() + ","
                                    + "C_STATEDATE='" + price.getStatedate() + "',C_PRICE=" + price.getPrice()
                                    + ",C_ISALLOT='" + price.getIsallot() + "',C_MINDAY=" + price.getMinday()
                                    + ",C_PRICEOFFER=" + price.getPriceoffer() + ", C_QUNARPRICE="
                                    + price.getQunarprice() + ",C_UPDATETIME='" + price.getUpdatetime() + "',"
                                    + "C_YULIUNUM='" + price.getYuliuNum() + "',C_ROOMSTATUS='" + price.getRoomstatus()
                                    + "',C_CITYID=" + price.getCityid() + ",C_JLTIME='" + price.getJltime()
                                    + "',C_JLALLMENT='" + price.getAllotmenttype() + "',C_JLRATENAME='"
                                    + price.getRatetype() + "'" + "where C_JLKEYID='" + jlkeyid + "';";
                            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        }
                        System.out.println("最后更新时间点不相同，更新价格……");
                    }
                    else {
                        System.out.println("最后更新时间点相同，不更新……");
                    }
                }
                else {
                    Hmhotelprice price = new Hmhotelprice();
                    price.setJlkeyid(result.getJlkeyid());
                    int supplyid = -1;
                    try {
                        supplyid = Integer.valueOf(result.getSupplierid());
                    }
                    catch (NumberFormatException e) {
                    }
                    price.setSupplyid(supplyid);
                    price.setJltime(result.getJltime());
                    price.setContractid(result.getHotelcd());
                    long hotelid = hotel.getId();
                    price.setHotelid(hotelid);
                    price.setContractver("");
                    price.setCur(result.getCurrency());
                    if (!"RMB".equals(price.getCur())) {
                        continue;
                    }
                    price.setProd(result.getRatetypeid());
                    price.setMaxday(99l);
                    //最少连住
                    try {
                        price.setMinday(Long.valueOf(result.getMinDay()));
                    }
                    catch (Exception e) {
                        price.setMinday(1l);
                    }
                    //提前天数
                    try {
                        price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                    }
                    catch (Exception e) {
                        price.setAdvancedday(0l);
                    }
                    price.setTicket("0");
                    long roomtypeid = 0;
                    if (roomtypeids.containsKey(hotelid + "-" + result.getJlroomtype())) {
                        roomtypeid = roomtypeids.get(hotelid + "-" + result.getJlroomtype());
                        System.out.println("不查数据库--房型——————");
                    }
                    else {
                        List<Roomtype> rooms = Server
                                .getInstance()
                                .getHotelService()
                                .findAllRoomtype(
                                        "where c_hotelid=" + hotelid + " and C_ROOMCODE='" + result.getJlroomtype()
                                                + "'", "", 1, 0);
                        if (rooms.size() == 1) {
                            roomtypeid = rooms.get(0).getId();
                            roomtypeids.put(hotelid + "-" + result.getJlroomtype(), rooms.get(0).getId());
                        }
                    }
                    if (roomtypeid == 0) {
                        continue;
                    }
                    price.setRoomtypeid(roomtypeid);
                    price.setType("");
                    price.setServ("");
                    String bf = result.getBreakfast();
                    if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                        bf = "";
                    }
                    price.setBreakfasttype(bf);
                    if (bf.equals("10")) {
                        price.setBf(0l);
                    }
                    else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                        price.setBf(1l);
                    }
                    else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                        price.setBf(2l);
                    }
                    else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                        price.setBf(3l);
                    }
                    else {
                        price.setBf(0l);
                    }
                    price.setStatedate(result.getStayDate());
                    String pricet = result.getPprice();
                    double saveprice = 0d;
                    try {
                        saveprice = Double.parseDouble(pricet);
                    }
                    catch (Exception e) {
                        saveprice = 0d;
                    }
                    if (saveprice <= 0) {
                        continue;
                    }
                    price.setPrice(saveprice);
                    price.setPriceoffer(saveprice);
                    price.setQunarprice(saveprice);
                    price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                    price.setSourcetype("6");
                    price.setCityid(hotel.getCityid().longValue() + "");
                    price.setRatetype(result.getRatetype());
                    String roomstatus = result.getAllot();
                    if ("12".equals(roomstatus)) {
                        price.setIsallot("Y");
                        price.setRoomstatus(0l);
                        price.setYuliuNum(99l);
                    }
                    else if ("16".equals(roomstatus)) {
                        price.setIsallot("C");
                        price.setRoomstatus(1l);
                        price.setYuliuNum(0l);
                    }
                    else {
                        try {
                            price.setYuliuNum(Long.parseLong(result.getFangliang()));
                        }
                        catch (Exception e) {
                            price.setYuliuNum(0l);
                        }
                        if (price.getYuliuNum() > 0) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                        }
                        else {
                            price.setIsallot("N");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(0l);
                        }
                    }
                    price.setAllotmenttype(result.getAllotmenttype());
                    //取消描述
                    price.setCanceldesc(result.getCanceldesc());
                    //类型：即订即保、提前多少天等
                    if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                            && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                        price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                    }
                    //提前天数
                    if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                            && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                        price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                    }
                    //提前时间  多少小时
                    if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                            && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                        price.setCanceltime(result.getTimeselect());
                    }
                    //不可修改、不可取消、两者
                    if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                            && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                        price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                    }
                    //不可修改内容
                    if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                            && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                        price.setNoedittype(result.getNoedit().trim());
                    }
                    //担保金额类型
                    if (result.getGuaranteeamounttype() != null && !"".equals(result.getGuaranteeamounttype().trim())
                            && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                        price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                    }
                    Server.getInstance().getHotelService().createHmhotelprice(price);
                    System.out.println("更新最后一天的价格，新增：" + result.getStayDate() + ":" + result.getHotelName());
                }
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("更新一次用时" + (t2 - t1) / 1000 + "s");
        WriteLog.write("价格更新用时", (t2 - t1) / 1000 + "s");
    }

    /**
     * 更新变价价格
     */
    @SuppressWarnings("unchecked")
    public static void updateChangePrice(int month) {
        Calendar start = GregorianCalendar.getInstance();
        Calendar end = GregorianCalendar.getInstance();
        end.add(Calendar.MONTH, month);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String started = sdf.format(start.getTime());
        String ended = sdf.format(end.getTime());
        long time = Long.parseLong(PropertyUtil.getValue("jlchanggeprice"));// 更新间隔时间（秒）
        long t1 = System.currentTimeMillis();//更新开始时间
        long t2 = System.currentTimeMillis();//更新结束时间
        long t3 = 0l;//当前时间
        System.out.println("初始更新时间：" + time + "s");
        label: while (true) {
            try {
                t1 = System.currentTimeMillis();
                //删除表
                List<JLPriceResult> jldel = Server.getInstance().getIJLHotelService().getDelHotel(time);
                del(jldel);
                String where = "where C_JLCODE is not null and c_type=1";
                List<City> citys = Server.getInstance().getHotelService().findAllCity(where, "order by id asc", -1, 0);
                int cnum = citys.size();
                long t4 = 0l;//每次更新所查询的时间段
                for (City city : citys) {
                    System.out.println("城市数量：" + cnum--);
                    t3 = System.currentTimeMillis();
                    t4 = time + ((t3 - t2) / 1000) + 600;
                    WriteLog.write("捷旅价格持续更新", city.getName() + ":上次更新用时：" + time + "s-----追加时间："
                            + (((t3 - t2) / 1000) + 600) + "-----更新时间段：" + t4);
                    System.out.println("当前更新所需的时间段：" + t4 + "s");
                    // 更新
                    update(started, ended, city.getId(), city.getJlcode(), t4);
                }
                t2 = System.currentTimeMillis();
                time = ((t2 - t1) / 1000) + 600;
                WriteLog.write("捷旅价格持续更新", "价格更新用时：" + time + "秒");
                if (time <= 60) {
                    System.out.println("停止2分钟……");
                    Thread.sleep(120000);
                    time = 600;
                }
            }
            catch (Exception e) {
                long t5 = System.currentTimeMillis();
                time = ((t5 - t1) / 1000) + 600;
                WriteLog.write("捷旅价格更新异常", e.getMessage());
                WriteLog.write("捷旅价格持续更新", "时间追加：" + time + "秒");
                System.out.println("报异常-价格更新，继续循环……");
                continue label;
            }
        }
    }

    /**
     * 更新捷旅价格数据
     */
    @SuppressWarnings("unchecked")
    public static void update(String started, String ended, Long cityid, String citycode, long time)
            throws SQLException {
        Map<String, Long> hotelids = new HashMap<String, Long>();
        Map<String, Long> roomtypeids = new HashMap<String, Long>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<JLPriceResult> results = Server.getInstance().getIJLHotelService()
                .getUpdateHotelPrice("0", started, ended, time, citycode);
        for (int j = 0; j < results.size(); j++) {
            JLPriceResult result = results.get(j);
            String jlkeyid = result.getJlkeyid();
            String jltime = result.getJltime();
            String where = " where C_JLKEYID='" + jlkeyid + "'";
            List<Hmhotelprice> hd = Server.getInstance().getHotelService().findAllHmhotelprice(where, "", 1, 0);
            if (hd.size() > 0) {
                if (!hd.get(0).getJltime().equals(jltime)) {//最后更新时间点不同，则更新
                    Hmhotelprice price = hd.get(0);
                    String jlcode = result.getHotelid();
                    long hotelid = 0l;
                    if (hotelids.containsKey(jlcode)) {
                        hotelid = hotelids.get(jlcode);
                        System.out.println("不查数据库--酒店——————");
                    }
                    else {
                        List<Hotel> hotels = Server.getInstance().getHotelService()
                                .findAllHotel("where c_sourcetype=6 and c_hotelcode='" + jlcode + "'", "", 1, 0);
                        if (hotels.size() > 0) {
                            Hotel hotelt = hotels.get(0);
                            hotelid = hotelt.getId();
                            hotelids.put(jlcode, hotelt.getId());
                        }
                        else {
                            //添加酒店操作
                            Server.getInstance().getIJLHotelService().getHotelSingle(jlcode);
                            List<Hotel> tes = Server.getInstance().getHotelService()
                                    .findAllHotel("where c_sourcetype=6 and c_hotelcode='" + jlcode + "'", "", 1, 0);
                            if (tes.size() > 0) {
                                Hotel hotelt = tes.get(0);
                                hotelid = hotelt.getId();
                                hotelids.put(jlcode, hotelt.getId());
                            }
                        }
                    }
                    if (hotelid > 0) {
                        price.setJlkeyid(result.getJlkeyid());
                        int supplyid = -1;
                        try {
                            supplyid = Integer.valueOf(result.getSupplierid());
                        }
                        catch (NumberFormatException e) {
                        }
                        price.setSupplyid(supplyid);
                        price.setJltime(result.getJltime());
                        price.setContractid(result.getHotelcd());
                        price.setHotelid(hotelid);
                        price.setContractver("");
                        price.setCur(result.getCurrency());
                        if (!"RMB".equals(price.getCur())) {
                            continue;
                        }
                        price.setProd(result.getRatetypeid());
                        price.setMaxday(99l);
                        //最少连住
                        try {
                            price.setMinday(Long.valueOf(result.getMinDay()));
                        }
                        catch (Exception e) {
                            price.setMinday(1l);
                        }
                        //提前天数
                        try {
                            price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                        }
                        catch (Exception e) {
                            price.setAdvancedday(0l);
                        }
                        price.setTicket("0");
                        long roomtypeid = 0;
                        if (roomtypeids.containsKey(hotelid + "-" + result.getJlroomtype())) {
                            roomtypeid = roomtypeids.get(hotelid + "-" + result.getJlroomtype());
                            System.out.println("不查数据库--房型——————");
                        }
                        else {
                            List<Roomtype> rooms = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllRoomtype(
                                            "where c_hotelid=" + hotelid + " and C_ROOMCODE='" + result.getJlroomtype()
                                                    + "'", "", 1, 0);
                            if (rooms.size() == 1) {
                                roomtypeid = rooms.get(0).getId();
                                roomtypeids.put(hotelid + "-" + result.getJlroomtype(), rooms.get(0).getId());
                            }
                            else {
                                Server.getInstance().getIJLHotelService().getRoomType(price.getHotelid(), jlcode);
                                List<Roomtype> resroom = Server
                                        .getInstance()
                                        .getHotelService()
                                        .findAllRoomtype(
                                                "where c_hotelid=" + hotelid + " and C_ROOMCODE='"
                                                        + result.getJlroomtype() + "'", "", 1, 0);
                                if (resroom.size() == 1) {
                                    roomtypeid = resroom.get(0).getId();
                                    roomtypeids.put(hotelid + "-" + result.getJlroomtype(), resroom.get(0).getId());
                                }
                            }
                        }
                        if (roomtypeid == 0) {
                            continue;
                        }
                        price.setRoomtypeid(roomtypeid);
                        price.setType("");
                        price.setServ("");
                        String bf = result.getBreakfast();
                        if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                            bf = "";
                        }
                        price.setBreakfasttype(bf);
                        if (bf.equals("10")) {
                            price.setBf(0l);
                        }
                        else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                            price.setBf(1l);
                        }
                        else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                            price.setBf(2l);
                        }
                        else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                            price.setBf(3l);
                        }
                        else if (bf.equals("34")) {
                            price.setBf(6l);
                        }
                        else {
                            price.setBf(0l);
                        }
                        price.setStatedate(result.getStayDate());
                        String pricet = result.getPprice();
                        double saveprice = 0d;
                        try {
                            saveprice = Double.parseDouble(pricet);
                        }
                        catch (Exception e) {
                            saveprice = 0d;
                        }
                        if (saveprice <= 0) {
                            continue;
                        }
                        price.setPrice(saveprice);
                        price.setPriceoffer(saveprice);
                        price.setQunarprice(saveprice);
                        price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                        price.setSourcetype("6");
                        price.setCityid(cityid.longValue() + "");
                        price.setRatetype(result.getRatetype());
                        String roomstatus = result.getAllot();
                        if ("12".equals(roomstatus)) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(99l);
                        }
                        else if ("16".equals(roomstatus)) {
                            price.setIsallot("C");
                            price.setRoomstatus(1l);
                            price.setYuliuNum(0l);
                        }
                        else {
                            try {
                                price.setYuliuNum(Long.parseLong(result.getFangliang()));
                            }
                            catch (Exception e) {
                                price.setYuliuNum(0l);
                            }
                            if (price.getYuliuNum() > 0) {
                                price.setIsallot("Y");
                                price.setRoomstatus(0l);
                            }
                            else {
                                price.setIsallot("N");
                                price.setRoomstatus(0l);
                                price.setYuliuNum(0l);
                            }
                        }
                        price.setAllotmenttype(result.getAllotmenttype());
                        //取消描述
                        price.setCanceldesc(result.getCanceldesc());
                        //类型：即订即保、提前多少天等
                        if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                                && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                            price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                        }
                        //提前天数
                        if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                                && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                            price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                        }
                        //提前时间  多少小时
                        if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                                && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                            price.setCanceltime(result.getTimeselect());
                        }
                        //不可修改、不可取消、两者
                        if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                                && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                            price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                        }
                        //不可修改内容
                        if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                                && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                            price.setNoedittype(result.getNoedit().trim());
                        }
                        //担保金额类型
                        if (result.getGuaranteeamounttype() != null
                                && !"".equals(result.getGuaranteeamounttype().trim())
                                && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                            price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                        }
                        String appendstr = "";
                        if (price.getCanceldesc() != null) {
                            appendstr += ",C_CANCELDESC='" + price.getCanceldesc() + "'";
                        }
                        if (price.getCanceltype() != null) {
                            appendstr += ",C_CANCELTYPE=" + price.getCanceltype();
                        }
                        if (price.getCanceladvance() != null) {
                            appendstr += ",C_CANCELADVANCE=" + price.getCanceladvance();
                        }
                        if (price.getCanceltime() != null) {
                            appendstr += ",C_CANCELTIME='" + price.getCanceltime() + "'";
                        }
                        if (price.getNoeditcancel() != null) {
                            appendstr += ",C_NOEDITCANCEL=" + price.getNoeditcancel();
                        }
                        if (price.getNoedittype() != null) {
                            appendstr += ",C_NOEDITTYPE='" + price.getNoedittype() + "'";
                        }
                        if (price.getGuamoneytype() != null) {
                            appendstr += ",C_GUAMONEYTYPE=" + price.getGuamoneytype();
                        }
                        if (roomtypeid != 0) {
                            String sql = "update T_HMHOTELPRICE set C_HOTELID=" + price.getHotelid() + ",C_CUR='"
                                    + price.getCur() + "',C_ADVANCEDDAY=" + price.getAdvancedday() + ",C_ROOMTYPEID="
                                    + price.getRoomtypeid().longValue() + ",C_BF=" + price.getBf().longValue() + ","
                                    + "C_STATEDATE='" + price.getStatedate() + "',C_PRICE=" + price.getPrice()
                                    + ",C_ISALLOT='" + price.getIsallot() + "',C_MINDAY=" + price.getMinday()
                                    + ",C_PRICEOFFER=" + price.getPriceoffer() + ", C_QUNARPRICE="
                                    + price.getQunarprice() + ",C_UPDATETIME='" + price.getUpdatetime() + "',"
                                    + "C_YULIUNUM='" + price.getYuliuNum() + "',C_ROOMSTATUS='" + price.getRoomstatus()
                                    + "',C_CITYID=" + price.getCityid() + ",C_JLTIME='" + price.getJltime()
                                    + "',C_JLALLMENT='" + price.getAllotmenttype() + "',C_JLRATENAME='"
                                    + price.getRatetype() + "'";
                            if (!"".equals(appendstr)) {
                                sql += appendstr;
                            }
                            sql += " where C_JLKEYID='" + jlkeyid + "';";
                            System.out.println(sql);
                            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        }
                        System.out.println("最后更新时间点不相同，更新价格……");
                    }
                }
                else {
                    System.out.println("最后更新时间点相同，不更新……");
                }
            }
            else {
                Hmhotelprice price = new Hmhotelprice();
                price.setJlkeyid(result.getJlkeyid());
                price.setJltime(result.getJltime());
                price.setContractid(result.getHotelcd());
                String jlcode = result.getHotelid();
                int supplyid = -1;
                try {
                    supplyid = Integer.valueOf(result.getSupplierid());
                }
                catch (NumberFormatException e) {
                }
                price.setSupplyid(supplyid);
                long hotelid = 0l;
                if (hotelids.containsKey(jlcode)) {
                    hotelid = hotelids.get(jlcode);
                    System.out.println("不查数据库--酒店——————");
                }
                else {
                    List<Hotel> hotels = Server.getInstance().getHotelService()
                            .findAllHotel("where c_sourcetype=6 and c_hotelcode='" + jlcode + "'", "", 1, 0);
                    if (hotels.size() > 0) {
                        Hotel hotelt = hotels.get(0);
                        hotelid = hotelt.getId();
                        hotelids.put(jlcode, hotelt.getId());
                    }
                    else {
                        //添加酒店操作
                        Server.getInstance().getIJLHotelService().getHotelSingle(jlcode);
                        List<Hotel> tes = Server.getInstance().getHotelService()
                                .findAllHotel("where c_sourcetype=6 and c_hotelcode='" + jlcode + "'", "", 1, 0);
                        if (tes.size() > 0) {
                            Hotel hotelt = tes.get(0);
                            hotelid = hotelt.getId();
                            hotelids.put(jlcode, hotelt.getId());
                        }
                    }
                }
                if (hotelid > 0) {
                    price.setHotelid(hotelid);
                    price.setContractver("");
                    price.setCur(result.getCurrency());
                    if (!"RMB".equals(price.getCur())) {
                        continue;
                    }
                    price.setProd(result.getRatetypeid());
                    price.setMaxday(99l);
                    //最少连住
                    try {
                        price.setMinday(Long.valueOf(result.getMinDay()));
                    }
                    catch (Exception e) {
                        price.setMinday(1l);
                    }
                    //提前天数
                    try {
                        price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                    }
                    catch (Exception e) {
                        price.setAdvancedday(0l);
                    }
                    price.setTicket("0");
                    long roomtypeid = 0;
                    if (roomtypeids.containsKey(hotelid + "-" + result.getJlroomtype())) {
                        roomtypeid = roomtypeids.get(hotelid + "-" + result.getJlroomtype());
                        System.out.println("不查数据库--房型——————");
                    }
                    else {
                        List<Roomtype> rooms = Server
                                .getInstance()
                                .getHotelService()
                                .findAllRoomtype(
                                        "where c_hotelid=" + hotelid + " and C_ROOMCODE='" + result.getJlroomtype()
                                                + "'", "", 1, 0);
                        if (rooms.size() == 1) {
                            roomtypeid = rooms.get(0).getId();
                            roomtypeids.put(hotelid + "-" + result.getJlroomtype(), rooms.get(0).getId());
                        }
                        else {
                            Server.getInstance().getIJLHotelService().getRoomType(price.getHotelid(), jlcode);
                            List<Roomtype> resroom = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllRoomtype(
                                            "where c_hotelid=" + hotelid + " and C_ROOMCODE='" + result.getJlroomtype()
                                                    + "'", "", 1, 0);
                            if (resroom.size() == 1) {
                                roomtypeid = resroom.get(0).getId();
                                roomtypeids.put(hotelid + "-" + result.getJlroomtype(), resroom.get(0).getId());
                            }
                        }
                    }
                    if (roomtypeid == 0) {
                        continue;
                    }
                    price.setRoomtypeid(roomtypeid);
                    price.setType("");
                    price.setServ("");
                    String bf = result.getBreakfast();
                    price.setBreakfasttype(bf);
                    if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                        bf = "";
                    }
                    if (bf.equals("10")) {
                        price.setBf(0l);
                    }
                    else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                        price.setBf(1l);
                    }
                    else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                        price.setBf(2l);
                    }
                    else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                        price.setBf(3l);
                    }
                    else if (bf.equals("34")) {
                        price.setBf(6l);
                    }
                    else {
                        price.setBf(0l);
                    }
                    price.setStatedate(result.getStayDate());
                    String pricet = result.getPprice();
                    double saveprice = 0d;
                    try {
                        saveprice = Double.parseDouble(pricet);
                    }
                    catch (Exception e) {
                        saveprice = 0d;
                    }
                    if (saveprice <= 0) {
                        continue;
                    }
                    price.setPrice(saveprice);
                    price.setPriceoffer(saveprice);
                    price.setQunarprice(saveprice);
                    price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                    price.setSourcetype("6");
                    price.setCityid(cityid.longValue() + "");
                    price.setRatetype(result.getRatetype());
                    String roomstatus = result.getAllot();
                    if ("12".equals(roomstatus)) {
                        price.setIsallot("Y");
                        price.setRoomstatus(0l);
                        price.setYuliuNum(99l);
                    }
                    else if ("16".equals(roomstatus)) {
                        price.setIsallot("C");
                        price.setRoomstatus(1l);
                        price.setYuliuNum(0l);
                    }
                    else {
                        try {
                            price.setYuliuNum(Long.parseLong(result.getFangliang()));
                        }
                        catch (Exception e) {
                            price.setYuliuNum(0l);
                        }
                        if (price.getYuliuNum() > 0) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                        }
                        else {
                            price.setIsallot("N");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(0l);
                        }
                    }
                    price.setAllotmenttype(result.getAllotmenttype());
                    //取消描述
                    price.setCanceldesc(result.getCanceldesc());
                    //类型：即订即保、提前多少天等
                    if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                            && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                        price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                    }
                    //提前天数
                    if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                            && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                        price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                    }
                    //提前时间  多少小时
                    if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                            && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                        price.setCanceltime(result.getTimeselect());
                    }
                    //不可修改、不可取消、两者
                    if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                            && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                        price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                    }
                    //不可修改内容
                    if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                            && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                        price.setNoedittype(result.getNoedit().trim());
                    }
                    //担保金额类型
                    if (result.getGuaranteeamounttype() != null && !"".equals(result.getGuaranteeamounttype().trim())
                            && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                        price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                    }
                    Server.getInstance().getHotelService().createHmhotelprice(price);
                }
            }
        }
    }

    /**
     * 删除表删除数据
     */
    @SuppressWarnings("unchecked")
    public static void del(List<JLPriceResult> jldel) {
        for (int i = 0; i < jldel.size(); i++) {
            JLPriceResult jprs = jldel.get(i);
            String jlkeyid = jprs.getJlkeyid();
            String where = " where C_JLKEYID='" + jlkeyid + "'";
            List<Hmhotelprice> hd = Server.getInstance().getHotelService().findAllHmhotelprice(where, "", 1, 0);
            if (hd.size() > 0) {
                Server.getInstance().getSystemService()
                        .findMapResultBySql("delete from T_HMHOTELPRICE where c_jlkeyid='" + jlkeyid + "'", null);
                System.out.println("删除………………");
            }
            else {
                System.out.println("没查到：" + jlkeyid);
            }
        }
    }

    /**
     * 酒店价格整体更新
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void updateHmHotelprice(int month) throws ParseException, SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        long t1 = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, month);
        Server.getInstance()
                .getSystemService()
                .findMapResultBySql("delete from t_hmhotelprice where C_STATEDATE<'" + sd.format(new Date()) + "'",
                        null);
        List<Hotel> hotels = Server
                .getInstance()
                .getHotelService()
                .findAllHotel("where  c_sourcetype = 6 and c_cityid > 0 and c_hotelcode is not null",
                        "order by c_cityid", -1, 0);
        int k = hotels.size();
        for (int i = 0; i < hotels.size(); i++) {
            System.out.println("剩余酒店：" + k--);
            Map<String, Long> roomtypeids = new HashMap<String, Long>();
            Hotel hotel = hotels.get(i);
            Map<String, String> infos = new HashMap<String, String>();
            List list = Server
                    .getInstance()
                    .getSystemService()
                    .findMapResultBySql(
                            "select C_JLKEYID,C_JLTIME from T_HMHOTELPRICE where c_hotelid=" + hotel.getId(), null);
            for (int j = 0; j < list.size(); j++) {
                Map map = (Map) list.get(j);
                String keyid = map.get("C_JLKEYID").toString();
                String jltime = map.get("C_JLTIME").toString();
                infos.put(keyid, jltime);
            }
            List<JLPriceResult> results = Server.getInstance().getIJLHotelService()
                    .getHotelPrice(hotel.getHotelcode(), sd.format(new Date()), sd.format(cal.getTime()));
            for (int j = 0; j < results.size(); j++) {
                JLPriceResult result = results.get(j);
                Hmhotelprice price = new Hmhotelprice();
                String jlkeyid = result.getJlkeyid();
                String time = result.getJltime();
                if (infos.containsKey(jlkeyid)) {
                    if (time.equals(infos.get(jlkeyid))) {
                    }
                    else {
                        if (hotel.getZshotelid() != null && hotel.getZshotelid() > 0) {
                            price.setZshotelid(hotel.getZshotelid());
                        }
                        else {
                            price.setZshotelid(0l);
                        }
                        price.setJlkeyid(result.getJlkeyid());
                        price.setContractid(result.getHotelcd());
                        price.setHotelid(hotel.getId());
                        price.setContractver("");
                        price.setCur(result.getCurrency());
                        if (!"RMB".equals(price.getCur())) {
                            continue;
                        }
                        price.setProd(result.getRatetypeid());
                        price.setMaxday(99l);
                        //最少连住
                        try {
                            price.setMinday(Long.valueOf(result.getMinDay()));
                        }
                        catch (Exception e) {
                            price.setMinday(1l);
                        }
                        //提前天数
                        try {
                            price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                        }
                        catch (Exception e) {
                            price.setAdvancedday(0l);
                        }
                        price.setTicket("0");
                        long roomtypeid = 0;
                        if (roomtypeids.containsKey(result.getJlroomtype())) {
                            roomtypeid = roomtypeids.get(result.getJlroomtype());
                        }
                        else {
                            List<Roomtype> rooms = Server
                                    .getInstance()
                                    .getHotelService()
                                    .findAllRoomtype(
                                            "where c_hotelid=" + hotel.getId() + " and C_ROOMCODE='"
                                                    + result.getJlroomtype() + "'", "", 1, 0);
                            if (rooms.size() == 1) {
                                roomtypeid = rooms.get(0).getId();
                                roomtypeids.put(result.getJlroomtype(), rooms.get(0).getId());
                            }
                        }
                        if (roomtypeid == 0) {
                            continue;
                        }
                        price.setRoomtypeid(roomtypeid);
                        price.setType("");
                        price.setServ("");
                        String bf = result.getBreakfast();
                        if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                            bf = "";
                        }
                        price.setBreakfasttype(bf);
                        if (bf.equals("10")) {
                            price.setBf(0l);
                        }
                        else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                            price.setBf(1l);
                        }
                        else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                            price.setBf(2l);
                        }
                        else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                            price.setBf(3l);
                        }
                        else if (bf.equals("34")) {
                            price.setBf(6l);
                        }
                        else {
                            price.setBf(0l);
                        }
                        price.setStatedate(result.getStayDate());
                        String pricet = result.getPprice();
                        double saveprice = 0d;
                        try {
                            saveprice = Double.parseDouble(pricet);
                        }
                        catch (Exception e) {
                            saveprice = 0d;
                        }
                        if (saveprice <= 0) {
                            continue;
                        }
                        price.setPrice(saveprice);
                        price.setPriceoffer(saveprice);
                        price.setQunarprice(saveprice);
                        price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                        price.setSourcetype("6");
                        price.setCityid(hotel.getCityid().toString());
                        price.setJltime(result.getJltime());
                        price.setRatetype(result.getRatetype());
                        int supplyid = -1;
                        try {
                            supplyid = Integer.valueOf(result.getSupplierid());
                        }
                        catch (NumberFormatException e) {
                        }
                        price.setSupplyid(supplyid);
                        String roomstatus = result.getAllot();
                        if ("12".equals(roomstatus)) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(99l);
                        }
                        else if ("16".equals(roomstatus)) {
                            price.setIsallot("C");
                            price.setRoomstatus(1l);
                            price.setYuliuNum(0l);
                        }
                        else {
                            try {
                                price.setYuliuNum(Long.parseLong(result.getFangliang()));
                            }
                            catch (Exception e) {
                                price.setYuliuNum(0l);
                            }
                            if (price.getYuliuNum() > 0) {
                                price.setIsallot("Y");
                                price.setRoomstatus(0l);
                            }
                            else {
                                price.setIsallot("N");
                                price.setRoomstatus(0l);
                                price.setYuliuNum(0l);
                            }
                        }
                        price.setAllotmenttype(result.getAllotmenttype());
                        //取消描述
                        price.setCanceldesc(result.getCanceldesc());
                        //类型：即订即保、提前多少天等
                        if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                                && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                            price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                        }
                        //提前天数
                        if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                                && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                            price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                        }
                        //提前时间  多少小时
                        if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                                && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                            price.setCanceltime(result.getTimeselect());
                        }
                        //不可修改、不可取消、两者
                        if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                                && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                            price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                        }
                        //不可修改内容
                        if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                                && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                            price.setNoedittype(result.getNoedit().trim());
                        }
                        //担保金额类型
                        if (result.getGuaranteeamounttype() != null
                                && !"".equals(result.getGuaranteeamounttype().trim())
                                && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                            price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                        }
                        String appendstr = "";
                        if (price.getCanceldesc() != null) {
                            appendstr += ",C_CANCELDESC='" + price.getCanceldesc() + "'";
                        }
                        if (price.getCanceltype() != null) {
                            appendstr += ",C_CANCELTYPE=" + price.getCanceltype();
                        }
                        if (price.getCanceladvance() != null) {
                            appendstr += ",C_CANCELADVANCE=" + price.getCanceladvance();
                        }
                        if (price.getCanceltime() != null) {
                            appendstr += ",C_CANCELTIME='" + price.getCanceltime() + "'";
                        }
                        if (price.getNoeditcancel() != null) {
                            appendstr += ",C_NOEDITCANCEL=" + price.getNoeditcancel();
                        }
                        if (price.getNoedittype() != null) {
                            appendstr += ",C_NOEDITTYPE='" + price.getNoedittype() + "'";
                        }
                        if (price.getGuamoneytype() != null) {
                            appendstr += ",C_GUAMONEYTYPE=" + price.getGuamoneytype();
                        }
                        if (roomtypeid != 0) {
                            String sql = "update T_HMHOTELPRICE set C_ZSHOTELID=" + price.getZshotelid()
                                    + ",C_HOTELID=" + price.getHotelid() + ",C_CUR='" + price.getCur()
                                    + "',C_ADVANCEDDAY=" + price.getAdvancedday() + ",C_ROOMTYPEID="
                                    + price.getRoomtypeid().longValue() + ",C_BF=" + price.getBf().longValue() + ","
                                    + "C_STATEDATE='" + price.getStatedate() + "',C_PRICE=" + price.getPrice()
                                    + ",C_ISALLOT='" + price.getIsallot() + "',C_MINDAY=" + price.getMinday()
                                    + ",C_PRICEOFFER=" + price.getPriceoffer() + ", C_QUNARPRICE="
                                    + price.getQunarprice() + ",C_UPDATETIME='" + price.getUpdatetime() + "',"
                                    + "C_YULIUNUM='" + price.getYuliuNum() + "',C_ROOMSTATUS='" + price.getRoomstatus()
                                    + "',C_CITYID=" + price.getCityid() + ",C_JLTIME='" + price.getJltime()
                                    + "',C_JLALLMENT='" + price.getAllotmenttype() + "',C_JLRATENAME='"
                                    + price.getRatetype() + "'";
                            if (!"".equals(appendstr)) {
                                sql += appendstr;
                            }
                            sql += " where C_JLKEYID='" + jlkeyid + "';";
                            Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        }
                    }
                    infos.remove(jlkeyid);
                }
                else {
                    if (hotel.getZshotelid() != null && hotel.getZshotelid() > 0) {
                        price.setZshotelid(hotel.getZshotelid());
                    }
                    else {
                        price.setZshotelid(0l);
                    }
                    price.setJlkeyid(result.getJlkeyid());
                    price.setContractid(result.getHotelcd());
                    price.setHotelid(hotel.getId());
                    price.setContractver("");
                    price.setCur(result.getCurrency());
                    if (!"RMB".equals(price.getCur())) {
                        continue;
                    }
                    price.setProd(result.getRatetypeid());
                    price.setMaxday(99l);
                    //最少连住
                    try {
                        price.setMinday(Long.valueOf(result.getMinDay()));
                    }
                    catch (Exception e) {
                        price.setMinday(1l);
                    }
                    //提前天数
                    try {
                        price.setAdvancedday(Long.valueOf(result.getLeadTime()));
                    }
                    catch (Exception e) {
                        price.setAdvancedday(0l);
                    }
                    price.setTicket("0");
                    long roomtypeid = 0;
                    if (roomtypeids.containsKey(result.getJlroomtype())) {
                        roomtypeid = roomtypeids.get(result.getJlroomtype());
                    }
                    else {
                        List<Roomtype> rooms = Server
                                .getInstance()
                                .getHotelService()
                                .findAllRoomtype(
                                        "where c_hotelid=" + hotel.getId() + " and C_ROOMCODE='"
                                                + result.getJlroomtype() + "'", "", 1, 0);
                        if (rooms.size() == 1) {
                            roomtypeid = rooms.get(0).getId();
                            roomtypeids.put(result.getJlroomtype(), rooms.get(0).getId());
                        }
                    }
                    if (roomtypeid <= 0) {
                        continue;
                    }
                    price.setRoomtypeid(roomtypeid);
                    price.setType("");
                    price.setServ("");
                    String bf = result.getBreakfast();
                    if (ElongHotelInterfaceUtil.StringIsNull(bf)) {
                        bf = "";
                    }
                    price.setBreakfasttype(bf);
                    if (bf.equals("10")) {
                        price.setBf(0l);
                    }
                    else if (bf.equals("11") || bf.equals("12") || bf.equals("13")) {
                        price.setBf(1l);
                    }
                    else if (bf.equals("21") || bf.equals("22") || bf.equals("23")) {
                        price.setBf(2l);
                    }
                    else if (bf.equals("31") || bf.equals("32") || bf.equals("33")) {
                        price.setBf(3l);
                    }
                    else if (bf.equals("34")) {
                        price.setBf(6l);
                    }
                    else {
                        price.setBf(0l);
                    }
                    price.setStatedate(result.getStayDate());
                    String pricet = result.getPprice();
                    double saveprice = 0d;
                    try {
                        saveprice = Double.parseDouble(pricet);
                    }
                    catch (Exception e) {
                        saveprice = 0d;
                    }
                    if (saveprice <= 0) {
                        continue;
                    }
                    price.setPrice(saveprice);
                    price.setPriceoffer(saveprice);
                    price.setQunarprice(saveprice);
                    price.setUpdatetime(sdf.format(new Date(System.currentTimeMillis())));
                    price.setSourcetype("6");
                    price.setCityid(hotel.getCityid().toString());
                    price.setJltime(result.getJltime());
                    price.setRatetype(result.getRatetype());
                    int supplyid = -1;
                    try {
                        supplyid = Integer.valueOf(result.getSupplierid());
                    }
                    catch (NumberFormatException e) {
                    }
                    price.setSupplyid(supplyid);
                    String roomstatus = result.getAllot();
                    if ("12".equals(roomstatus)) {
                        price.setIsallot("Y");
                        price.setRoomstatus(0l);
                        price.setYuliuNum(99l);
                    }
                    else if ("16".equals(roomstatus)) {
                        price.setIsallot("C");
                        price.setRoomstatus(1l);
                        price.setYuliuNum(0l);
                    }
                    else {
                        try {
                            price.setYuliuNum(Long.parseLong(result.getFangliang()));
                        }
                        catch (Exception e) {
                            price.setYuliuNum(0l);
                        }
                        if (price.getYuliuNum() > 0) {
                            price.setIsallot("Y");
                            price.setRoomstatus(0l);
                        }
                        else {
                            price.setIsallot("N");
                            price.setRoomstatus(0l);
                            price.setYuliuNum(0l);
                        }
                    }
                    price.setAllotmenttype(result.getAllotmenttype());
                    //取消描述
                    price.setCanceldesc(result.getCanceldesc());
                    //类型：即订即保、提前多少天等
                    if (result.getVoidabletype() != null && !"".equals(result.getVoidabletype().trim())
                            && !"null".equals(result.getVoidabletype().trim().toLowerCase())) {
                        price.setCanceltype(Integer.parseInt(result.getVoidabletype().trim()));
                    }
                    //提前天数
                    if (result.getDayselect() != null && !"".equals(result.getDayselect().trim())
                            && !"null".equals(result.getDayselect().trim().toLowerCase())) {
                        price.setCanceladvance(Integer.parseInt(result.getDayselect().trim()));
                    }
                    //提前时间  多少小时
                    if (result.getTimeselect() != null && !"".equals(result.getTimeselect().trim())
                            && !"null".equals(result.getTimeselect().trim().toLowerCase())) {
                        price.setCanceltime(result.getTimeselect());
                    }
                    //不可修改、不可取消、两者
                    if (result.getNoeditorcancel() != null && !"".equals(result.getNoeditorcancel().trim())
                            && !"null".equals(result.getNoeditorcancel().trim().toLowerCase())) {
                        price.setNoeditcancel(Integer.parseInt(result.getNoeditorcancel().trim()));
                    }
                    //不可修改内容
                    if (result.getNoedit() != null && !"".equals(result.getNoedit().trim())
                            && !"null".equals(result.getNoedit().trim().toLowerCase())) {
                        price.setNoedittype(result.getNoedit().trim());
                    }
                    //担保金额类型
                    if (result.getGuaranteeamounttype() != null && !"".equals(result.getGuaranteeamounttype().trim())
                            && !"null".equals(result.getGuaranteeamounttype().trim().toLowerCase())) {
                        price.setGuamoneytype(Integer.parseInt(result.getGuaranteeamounttype().trim()));
                    }
                    System.out.println("添加一条价格记录：" + result.getHotelName() + ":" + result.getStayDate());
                    Server.getInstance().getHotelService().createHmhotelprice(price);

                }
            }
            if (infos.size() > 0) {
                Set<String> keys = infos.keySet();
                Iterator<String> keyite = keys.iterator();
                while (keyite.hasNext()) {
                    String key = keyite.next();
                    String sql = "delete from t_hmhotelprice where c_jlkeyid='" + key + "'";
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                }
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("更新一次用时" + (t2 - t1) / 1000 + "s");
        WriteLog.write("价格更新用时", (t2 - t1) / 1000 + "s");
    }
}