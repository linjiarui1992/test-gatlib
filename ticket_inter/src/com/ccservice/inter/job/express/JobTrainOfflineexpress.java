package com.ccservice.inter.job.express;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.inter.server.Server;

public class JobTrainOfflineexpress implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new Date());
		WriteLog.write("JobTrainOfflineexpress", "JobTrainOfflineexpress开始：" + date);
		try {
			String sql = "[TrainOrderOfflineExpressSelect]";
			List list = Server.getInstance().getSystemService()
					.findMapResultBySql(sql, null);
			WriteLog.write("JobTrainOfflineexpress", "订单量：" + list.size());
			for (int i = 0; i < list.size(); i++) {
				Map map = (Map)list.get(i);
				String orderId = map.get("Id").toString();
				String expressNum = map.get("ExpressNum").toString();
				new TrainExpressStatusThread(orderId, expressNum).start();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	// 设置调度任务及触发器，任务名及触发器名可用来停止任务或修改任务
	public static void startScheduler(String expr) throws Exception {
		System.out.println("开始JobTrainOfflineExpress_expr=" + expr);
		WriteLog.write("JobTrainOfflineexpress_expr", "expr=" + expr);
		JobDetail jobDetail = new JobDetail("JobTrainOfflineexpress",
				"JobTrainOfflineexpressGroup", JobTrainOfflineexpress.class);// 任务名，任务组名，任务执行类
		CronTrigger trigger = new CronTrigger("JobTrainOfflineexpress",
				"JobTrainOfflineexpressGroup", expr);// 触发器名，触发器组名
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.scheduleJob(jobDetail, trigger);
		scheduler.start();
	}

}
