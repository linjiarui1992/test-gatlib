package com.ccservice.offlineExpress;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.SendPostandGetUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.util.CommonUtil;
import com.ccservice.offlineExpress.uupt.UUptService;
import com.ccservice.tongcheng.service.TongChengOfflineLockOvertime;

public class UuptRoutecallbackServlet extends HttpServlet implements Servlet {
    private static final String LOGNAME = "线下票闪送订单的下单异步回调接口";

    private int r1 = new Random().nextInt(10000000);

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 编码
        request.setCharacterEncoding("utf-8");
        // 随机
        int random = CommonUtil.randomNum();
        String result = "failure";
        String data = request.getParameter("data");
        WriteLog.write("线下火车票_UU跑腿回调路由信息", "------回调信息------" + data);

        /**
         * -1 - 超时自动取消的相关回调的位置
        {
        "appid": "d70cd8e398784587a704278c3cc4905c",
        "nonce_str": "9891e3da60b0aa898866884fc09971ce",
        "sign": "FD7258798AC12FC8191D4454D3F1A151",
        "order_code": "U37908001711011437481439216",
        "state": "3",
        "state_text": "已抢单",
        "driver_name": "李跑男",
        "driver_jobnum": "ZZ303587",
        "driver_mobile": "13837151387",
        "timestamp": "1509518287",
        "return_code": "ok",
        "return_msg": "",
        "origin_id": "TC2017110113543432893424"
        }
        {
        "state": "4",
        "state_text": "已到达",
        }
        {
        "state": "5",
        "state_text": "已取件",
        }
        {
        "state": "6",
        "state_text": "已送达",
        }
        {
        "state": "10",
        "state_text": "完成收货",
        }
         * 
         */

        JSONObject dataJson = JSONObject.parseObject(data);
        String orderNumberAgain = dataJson.getString("origin_id");//二次订单ID
        String orderNumber = orderNumberAgain;//订单ID
        // 根据真是订单号订单号，查询订单信息
        String sql = "select orderNumber from uupt_orderprice where orderNumberAgain = '" + orderNumberAgain + "'";
        DataTable dataTable = new DataTable();
        try {
            dataTable = DBHelperOffline.GetDataTable(sql, null);
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionTCUtil.handleTCException(e);
            result = "failure";
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "新订单号查询失败");
        }
        // 取到数据
        if (dataTable.GetRow().size() > 0) {
            orderNumber = String.valueOf(dataTable.GetRow().get(0).GetColumn("orderNumber").GetValue());//订单ID
        }
        TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();
        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumber(orderNumber);
        }
        catch (Exception e) {
            ExceptionTCUtil.handleTCException(e);
            result = "failure";
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "回调失败：" + "订单信息获取错误");
        }

        //UU跑腿是否下单成功需要在数据库中的字段中体现出来 - 便于后续的提醒和处理

        if (trainOrderOffline == null) {
            result = "failure";
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "回调失败：" + "订单信息获取错误");
        }
        else {
            //修改闪送单的响应标志位
            Long orderId = trainOrderOffline.getId();
            trainOrderOfflineDao.updateTrainOrderOfflineisRapidSendCallbackById(orderId);

            //记录日志 - 使用系统的身份角色处理相关的信息
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "UU跑腿下单结果已反馈");

            String order_code = dataJson.getString("order_code").trim();//快递单号
            String driver_name = dataJson.getString("driver_name");//跑男姓名(跑男接单后)
            if (driver_name == null || "".equals(driver_name) || "null".equals(driver_name)) {
                driver_name = "";
            }
            String driver_jobnum = dataJson.getString("driver_jobnum");//跑男工号(跑男接单后)
            if (driver_jobnum == null || "".equals(driver_jobnum) || "null".equals(driver_jobnum)) {
                driver_jobnum = "";
            }
            String driver_mobile = dataJson.getString("driver_mobile");//跑男电话(跑男接单后)
            if (driver_mobile == null || "".equals(driver_mobile) || "null".equals(driver_mobile)) {
                driver_mobile = "";
            }
            String state = dataJson.getString("state");//当前状态1下单成功 3跑男抢单 4已到达 5已取件 6到达目的地 10收件人已收货 -1订单取消
            String state_text = dataJson.getString("state_text");//当前状态说明
            String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .format(new Date(Long.parseLong(dataJson.getString("timestamp")) * 1000));//状态更新时间
            String return_msg = dataJson.getString("return_msg");//返回信息，如非空，为错误原因，如签名失败、参数格式校验错误
            String return_code = dataJson.getString("return_code");//状态，ok/fail表示成功
            String sqlins = "INSERT INTO uupt_route (order_code, driver_name, driver_jobnum, driver_mobile, state, state_text, acceptTime, orderNumber, return_msg, return_code) VALUES "
                    + "('" + order_code + "', '" + driver_name + "', '" + driver_jobnum + "', '" + driver_mobile + "', "
                    + state + " , '" + state_text + "', '" + timestamp + "', '" + orderNumberAgain + "', '" + return_msg
                    + "', '" + return_code + "')";
            WriteLog.write("线下火车票_UU跑腿回调路由信息", "路由SQL：" + sqlins);
            int returnCode = DBHelperOffline.insertSql(sqlins);
            if (returnCode == 0) {
                result = "failure";
            }
            else {
                //正式环境目前不上线该方法
                //RouteCallBackUtil.flushExpressRoute("线下火车票_UU跑腿回调路由信息", random, order_code);
                //Integer lockedStatus = trainOrderOffline.getLockedStatus();//0表示未被锁定，1表示被锁定 - 2-锁单失败 3-锁单中 - 4-锁单超时 -【途牛】 状态5-闪送单的锁单超时 - 6-客服的闪送单的二次下单锁单操作

                //只有在跑男第一次回复已抢单的时候发起相关的采购交互请求 - 
                if ("3".equals(state)) {
                    /**
                     * 下单完成后，有人接单的话，此处会给出跑男抢单的状态
                     * 
                     * 修改闪送单的标志位为true - 发起同程的锁单请求
                     * 
                     * 发起后续的接口交互的锁单请求
                     */
                    Integer createUId = trainOrderOffline.getCreateUId();

                    //更新相关的闪送单的下单状态为成功 - 
                    trainOrderOfflineDao.updateTrainOrderOfflineisRapidSendSuccessById(orderId);

                    if (trainOrderOffline.getLockedStatus() == 5) {
                        //记录日志 - 使用系统的身份角色处理相关的信息
                        String errorMsg = "UU跑腿已接单成功，但系统已经超时自动取消了该单";

                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, errorMsg);

                        //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                        String flagStr = "TongChengOfflineLockRapidSendOvertime";
                        try {
                            JobDetail jobDetail = SchedulerUtil.getScheduler()
                                    .getJobDetail(flagStr + "Job" + trainOrderOffline.getId(), flagStr + "JobGroup");
                            if (jobDetail != null) {
                                SchedulerUtil.cancelCancelLockTicketScheduler(orderId, flagStr);

                                TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "同程闪送单的自动取消定时任务删除成功");
                            }
                        }
                        catch (Exception e) {
                            ExceptionTNUtil.handleTNException(e);
                        }

                        flagStr = "TrainTaoBaoOfflineLockRapidSendOvertime";
                        try {
                            JobDetail jobDetail = SchedulerUtil.getScheduler()
                                    .getJobDetail(flagStr + "Job" + trainOrderOffline.getId(), flagStr + "JobGroup");
                            if (jobDetail != null) {
                                SchedulerUtil.cancelCancelLockTicketScheduler(orderId, flagStr);

                                TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "淘宝闪送单的自动取消定时任务删除成功");
                            }
                        }
                        catch (Exception e) {
                            ExceptionTNUtil.handleTNException(e);
                        }
                    }
                    else {
                        //记录日志 - 使用系统的身份角色处理相关的信息
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "UU跑腿已接单，可以发起采购的相关锁单请求");

                        JSONObject resResult = new JSONObject();

                        if (createUId == 81) {//同程单的相关交互处理
                            //暂时还不存在锁单的情况 - 直接修改相关的状态即可

                            //更新锁单状态为锁单中 - 点击一次之后
                            trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderId, 1);//锁单成功

                            //还需要修改相关的锁单反馈的标志位为已反馈

                            //直接修改相关的锁的状态
                            trainOrderOfflineDao.updateTrainOrderOfflineisLockCallbackById(orderId);//0表示未被锁定，1表示被锁定

                            //更新锁单成功或者失败的时间 - 
                            try {
                                trainOrderOfflineDao.updateTrainOrderOfflineLockTimeById(orderId);
                            }
                            catch (Exception e2) {
                                ExceptionTCUtil.handleTCException(e2);
                            }

                            //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                            String flagStr = "TongChengOfflineLockRapidSendOvertime";
                            try {
                                JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail(
                                        flagStr + "Job" + trainOrderOffline.getId(), flagStr + "JobGroup");
                                if (jobDetail != null) {
                                    SchedulerUtil.cancelCancelLockTicketScheduler(orderId, flagStr);

                                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "同程闪送单的自动取消定时任务删除成功");
                                }
                            }
                            catch (Exception e) {
                                ExceptionTNUtil.handleTNException(e);
                            }

                            /**
                             * 如果想模拟成为同步反馈的结果，需要判定是否收到异步的锁单反馈 - 且不允许再次锁单 - 需要加上一个标志位
                             * 
                             * 同程的锁票的异步转同步的内部等待时间 - 以min为单位 - 目前设定为半小时
                             * 
                             */
                            String LockWait = PropertyUtil.getValue("TrainTongChengOfflineLockWait",
                                    "Train.GuestAccount.properties");

                            //毫秒数
                            Long overtime = new Date().getTime() + Integer.valueOf(LockWait) * 60 * 1000;

                            String lockWaitDateTime = TrainOrderOfflineUtil.getTimestrByTime(overtime);
                            //记录日志 - 使用系统的身份角色处理相关的信息
                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46,
                                    "锁票请求成功，等待同程异步反馈结果，目前等待超时的设置时间是:" + LockWait + "min，超时后自动解锁");

                            //同步入库成最晚出票时间 - 还是不能重复锁单的设计

                            try {
                                trainOrderOfflineDao.updateOrderTimeoutById(orderId, lockWaitDateTime);//2017-08-21 14:49:00
                            }
                            catch (Exception e1) {
                                ExceptionTCUtil.handleTCException(e1);
                            }

                            //此处另起JOB，完成5分钟之后的拒单操作

                            String year = lockWaitDateTime.substring(0, 4);
                            String month = lockWaitDateTime.substring(5, 7);
                            String day = lockWaitDateTime.substring(8, 10);
                            String hour = lockWaitDateTime.substring(11, 13);
                            String minute = lockWaitDateTime.substring(14, 16);
                            String second = lockWaitDateTime.substring(17, 19);

                            //"40 17 14 25 08 ? 2017"
                            String cronExpression = second + " " + minute + " " + hour + " " + day + " " + month + " ? "
                                    + year;

                            //System.out.println(cronExpression);

                            WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求启动定时任务-cronExpression" + cronExpression);

                            try {
                                JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail(
                                        "TongChengOfflineLockOvertime" + "Job" + trainOrderOffline.getId(),
                                        "TongChengOfflineLockOvertime" + "JobGroup");
                                if (jobDetail != null) {
                                    WriteLog.write(LOGNAME,
                                            r1 + ":同程线下票锁票请求定时任务已启动-jobDetail:" + jobDetail.getFullName());

                                    resResult.put("success", "true");
                                    resResult.put("msg", "锁票请求成功");

                                    WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求结果-resResult:" + resResult);
                                }
                                else {
                                    WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求启动定时任务-jobDetail:" + null);

                                    //创建一个定时任务，并在指定的时间点进行启动
                                    try {
                                        SchedulerUtil.startLockScheduler(orderId, 46, LockWait, cronExpression,
                                                "TongChengOfflineLockOvertime", TongChengOfflineLockOvertime.class);
                                    }
                                    catch (Exception e) {
                                        ExceptionTCUtil.handleTCException(e);
                                    }

                                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "锁票请求成功");

                                    resResult.put("success", "true");
                                    resResult.put("msg", "锁票请求成功");

                                    WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求结果-resResult:" + resResult);
                                }
                            }
                            catch (SchedulerException e1) {
                                resResult.put("success", "true");
                                resResult.put("msg", "锁票请求成功-定时任务出现异常");

                                WriteLog.write(LOGNAME, r1 + ":同程线下票锁票请求结果-resResult:" + resResult);
                            }
                        }
                        else if (createUId == 56) {//淘宝单的相关交互处理
                            //还需要修改相关的锁单反馈的标志位为已反馈

                            //直接修改相关的锁的状态
                            trainOrderOfflineDao.updateTrainOrderOfflineisLockCallbackById(orderId);//0表示未被锁定，1表示被锁定

                            //更新锁单成功或者失败的时间 - 
                            try {
                                trainOrderOfflineDao.updateTrainOrderOfflineLockTimeById(orderId);
                            }
                            catch (Exception e2) {
                                ExceptionTCUtil.handleTCException(e2);
                            }

                            //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                            String flagStr = "TrainTaoBaoOfflineLockRapidSendOvertime";
                            try {
                                JobDetail jobDetail = SchedulerUtil.getScheduler().getJobDetail(
                                        flagStr + "Job" + trainOrderOffline.getId(), flagStr + "JobGroup");
                                if (jobDetail != null) {
                                    SchedulerUtil.cancelCancelLockTicketScheduler(orderId, flagStr);

                                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "淘宝闪送单的自动取消定时任务删除成功");
                                }
                            }
                            catch (Exception e) {
                                ExceptionTNUtil.handleTNException(e);
                            }

                            //淘宝锁单请求的发起
                            String elongxml = PropertyUtil.getValue("rapidSendMailTaobaoRequestXml",
                                    "Train.GuestAccount.properties");
                            String urlsuodan = elongxml + "TaoBaoTrainOfflineConfirmServlet";
                            String paramjudan = "orderNum=" + trainOrderOffline.getOrderNumberOnline();

                            WriteLog.write(LOGNAME, r1 + "请求地址参数:" + urlsuodan + "?" + paramjudan);
                            String resultjudan = SendPostandGetUtil.submitPost(urlsuodan, paramjudan, "UTF-8")
                                    .toString();

                            WriteLog.write(LOGNAME, r1 + "订单Id:" + orderId + ";结果:" + resultjudan);

                            if (resultjudan != null && "true".equals(resultjudan)) {//锁单成功，记操作记录
                                TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "锁票请求成功");

                                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderId, 1);//锁单成功

                                resResult.put("success", "true");
                                resResult.put("msg", "锁票请求成功");

                                WriteLog.write(LOGNAME, r1 + ":淘宝闪送单锁票请求结果-resResult:" + resResult);
                            }
                            else {//锁单失败，记操作记录
                                trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(orderId, 2);//锁单失败

                                resResult.put("success", "false");
                                resResult.put("msg", "锁票请求失败");

                                WriteLog.write(LOGNAME, r1 + ":淘宝闪送单锁票请求结果-resResult:" + resResult);

                                TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "锁票请求失败");

                                Boolean isSecondRapidSend = TrainOrderOfflineUtil
                                        .getIsSecondCancelRapidSend(orderNumber);//判断闪送单是否是二次取消订单 - 

                                UUptService uuptService = new UUptService();
                                JSONObject responseJson = new JSONObject();

                                JSONObject dataRapidSend = new JSONObject();
                                dataRapidSend.put("orderId", orderId);

                                try {
                                    if (isSecondRapidSend) {//二次取消
                                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "发起闪送单独下单的二次取消请求");
                                        dataRapidSend.put("cancelReason", "线下票闪送订单出票失败发起二次取消");
                                        responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrderAgain",
                                                dataRapidSend, responseJson);
                                    }
                                    else {//一次取消
                                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "闪送单独下单的取消请求");
                                        dataRapidSend.put("cancelReason", "线下票闪送订单出票失败发起取消");
                                        responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrder",
                                                dataRapidSend, responseJson);
                                    }
                                }
                                catch (Exception e) {
                                    ExceptionTNUtil.handleTNException(e);
                                }
                                //记录请求信息日志
                                WriteLog.write(LOGNAME, r1 + ":闪送单独下单的取消反馈信息-responseJson" + responseJson);

                                /**
                                 * 
                                {"message":"成功","result":{"partnerOrderNumber":"U37908001710310949679202216","message":"订单发布成功","orderNumber":"","success":true},"success":true}
                                 * 
                                {"message":"请求发生异常","result":"","success":false}
                                 * 
                                 */
                                //Boolean responseB = responseJson.getBooleanValue("success");
                                Boolean responseB = false;
                                JSONObject responseJsonResult = new JSONObject();
                                if (responseJson.getBooleanValue("success")) {
                                    responseJsonResult = responseJson.getJSONObject("result");
                                    responseB = responseJsonResult.getBooleanValue("success");
                                }

                                if (responseB) {
                                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, "闪送单-淘宝锁单下单的取消成功");

                                    //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                                    String jobFlag = "TrainTaoBaoOfflineLockRapidSendOvertime";
                                    try {
                                        JobDetail jobDetail = SchedulerUtil.getScheduler()
                                                .getJobDetail(jobFlag + "Job" + orderId, jobFlag + "JobGroup");
                                        if (jobDetail != null) {
                                            SchedulerUtil.cancelCancelLockTicketScheduler(orderId, jobFlag);

                                            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46,
                                                    "闪送单-淘宝锁单下单的自动取消定时任务删除成功");
                                        }
                                    }
                                    catch (Exception e) {
                                        ExceptionTNUtil.handleTNException(e);
                                    }

                                    resResult.put("success", "true");
                                    resResult.put("msg", "闪送单-淘宝锁单下单的取消成功");
                                }
                                else {
                                    String errorMsg = responseJsonResult.getString("message");
                                    if (errorMsg == null || "".equals(errorMsg)) {
                                        errorMsg = responseJson.getString("message");
                                    }
                                    errorMsg = "闪送单-淘宝锁单下单的取消失败:" + errorMsg + "-请联系技术处理";
                                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, 46, errorMsg);

                                    resResult.put("success", "false");
                                    resResult.put("msg", errorMsg);
                                }

                                WriteLog.write(LOGNAME, r1 + ":闪送单-淘宝锁单下单的取消结果-resResult:" + resResult);
                            }
                        }

                    }
                }

                result = "success";
            }
        }
        WriteLog.write("线下火车票_UU跑腿回调路由信息", "------回调结果------" + result);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        response.getOutputStream().write(result.getBytes(Charset.forName("UTF-8")));
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        //闪送【送票到家】的测试订单的信息
        String data = "{\"appid\":\"ff65587a0dc8403fb7be03e220e30648\",\"nonce_str\":\"cf7ea79ca084dd6e38a5eee076b87cd3\",\"sign\":\"C0BD901ACD1417C8A5935569DE7BBE85\","
                + "\"order_code\":\"U37908001711211135619797216     \","
                + "\"state\":\"3\",\"state_text\":\"已抢单\",\"driver_name\":\"刘跑男\",\"driver_jobnum\":\"BJ135016\",\"driver_mobile\":\"13716519454\","
                + "\"timestamp\":\"" + System.currentTimeMillis() / 1000L
                + "\",\"return_code\":\"ok\",\"return_msg\":\"\"," + "\"origin_id\":\"T1711211125593203871\"}";

        //25min自动取消的回调测试
        //String data = "{\"appid\":\"d70cd8e398784587a704278c3cc4905c\",\"nonce_str\":\"7c20594cd88675de5e66805bc9e8ada9\",\"sign\":\"9E17A3CE731AFF6E7CB3425E63A028EB\",\"order_code\":\"U37908001711091152678043216     \",\"state\":\"-1\",\"state_text\":\"已取消\",\"driver_name\":null,\"driver_jobnum\":null,\"driver_mobile\":null,\"timestamp\":\"1510201041\",\"return_code\":\"ok\",\"return_msg\":\"\",\"origin_id\":\"TC2017110909582772696799\"}";

        System.out.println(data);

        //本地测试地址
        //String url = "http://localhost:8097/ticket_inter/UuptRoutecallbackServlet";
        //测试环境地址
        String url = "http://121.40.226.72:9007/ticket_inter/UuptRoutecallbackServlet";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/UuptRoutecallbackServlet";

        System.out.println(httpPostJsonUtil.doPostForm(url, data));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
