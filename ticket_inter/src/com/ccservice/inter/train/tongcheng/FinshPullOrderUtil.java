package com.ccservice.inter.train.tongcheng;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.inter.job.train.TrainInterfaceMethod;

public class FinshPullOrderUtil {

    public static boolean isTongChengOrder(Trainorder trainorder) {
        boolean isTongChengOrder = false;
        if (trainorder.getInterfacetype() == TrainInterfaceMethod.TONGCHENG && trainorder.getAgentid() == 47) {
            isTongChengOrder = true;
        }
        return isTongChengOrder;

    }
}
