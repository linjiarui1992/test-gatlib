/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainTicketOffline;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOrderOfflineDaoTest
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月16日 下午2:57:31 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOrderOfflineDaoTest {
    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();
    
    public void testAddTrainOrderOffline() throws Exception {
        //订单信息入库
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();

        trainOrderOffline.setOrderNumber("这就是个测试而已");
        
        //不能为空的字段的值
        
        trainOrderOffline.setAgentId(0);
        
        trainOrderOffline.setCreateUId(0);
        
        trainOrderOffline.setOrderPrice(0.0);
        trainOrderOffline.setOrderStatus(0);

        trainOrderOffline.setAgentProfit(0.0);

        trainOrderOffline.setSupplyPayWay(0);
        trainOrderOffline.setTicketCount(0);

        trainOrderOffline.setRefundReason(0);

        trainOrderOffline.setChuPiaoAgentid(0);

        trainOrderOffline.setLockedStatus(0);
        
        trainOrderOffline.setIsDelivery(1);
        
        /*trainOrderOffline.setCreateTime("");
        trainOrderOffline.setCreateUser("");
        trainOrderOffline.setContactUser("");
        trainOrderOffline.setContactTel("");
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setRefundReasonStr("");
        trainOrderOffline.setOrderTimeout("");
        trainOrderOffline.setChuPiaoTime("");
        trainOrderOffline.setRemark("");
        trainOrderOffline.setOrderNumberOnline("");
        trainOrderOffline.setPaystatus(0);
        trainOrderOffline.setPaperType(0);
        trainOrderOffline.setPaperBackup(0);
        trainOrderOffline.setPaperLowSeatCount(0);
        trainOrderOffline.setExtSeat("");
        trainOrderOffline.setOperateid(0);
        trainOrderOffline.setOperatetime("");
        trainOrderOffline.setRefusereason(0);
        trainOrderOffline.setRefusereasonstr("");
        trainOrderOffline.setExpressDeliver("");
        trainOrderOffline.setTelephone("");
        trainOrderOffline.setQuestionDraw(0);
        trainOrderOffline.setQuestionMail(0);
        trainOrderOffline.setIsDelivery(0);
        trainOrderOffline.setOfflineOrderType(0);
        trainOrderOffline.setNeedDeliveryTime("");
        trainOrderOffline.setQuestionOrder("");*/
        
        System.out.println(trainOrderOfflineDao.addTrainOrderOfflineTC(trainOrderOffline));
    }

    public void testFindTrainOrderOfflineById() throws Exception {
        System.out.println(trainOrderOfflineDao.findTrainOrderOfflineById(1939L));
    }
    
    public void testFindIsLockCallbackById() throws Exception {
        System.out.println(trainOrderOfflineDao.findIsLockCallbackById(1498L));
        System.out.println(trainOrderOfflineDao.findLockedStatusById(1498L));
        
        trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(1498L, 1);
        
        System.out.println(trainOrderOfflineDao.findIsLockCallbackById(1498L));
        System.out.println(trainOrderOfflineDao.findLockedStatusById(1498L));
    }

    public void testDelTrainOrderOfflineById() throws Exception {
        System.out.println(trainOrderOfflineDao.delTrainOrderOfflineById(1629L));
    }

    public void testFindTrainOrderOfflineByOrderNumberOnline() throws Exception {
        System.out.println(trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline("yglcw16081619084857a"));
    }

    public void testUpdateOrderTimeoutById() throws Exception {
        String OrderTimeout = "2017-08-21 14:49:00";//2017-08-21 14:49:00
        System.out.println(trainOrderOfflineDao.updateOrderTimeoutById(1525L, OrderTimeout));
    }

    public String getAllPaperTypestaobao(String PaperType, String PaperBackup, String PaperLowSeatCount) {
        String result = "无";
        String result1 = "";
        String result2 = "";
        if ("1".equals(PaperType)) {
            result1 = "靠窗票";
        }
        else if ("2".equals(PaperType)) {
            result1 = "连坐票";
        }
        else if ("3".equals(PaperType)) {
            result1 = "上铺票";
        }
        else if ("4".equals(PaperType)) {
            result1 = "中铺票";
        }
        else if ("5".equals(PaperType)) {
            result1 = "下铺票";
        }
        else if ("6".equals(PaperType)) {
            result1 = "同包厢";
        }
        else if ("7".equals(PaperType)) {
            result1 = "中上铺";
        }
        else if ("8".equals(PaperType)) {
            result1 = "一起";
        }
        else if ("9".equals(PaperType)) {
            result1 = "过道";
        }
        if ("0".equals(PaperBackup)) {
            result2 = "是否接受非" + result1 + ":不接受";
        }
        else if ("1".equals(PaperBackup)) {
            result2 = "是否接受非" + result1 + ":接受";
        }
        if ("0".equals(PaperType)) {
            result = "无";
        }
        else if ("0".equals(PaperLowSeatCount)) {
            result = "指定" + result1 + "，至少需要" + PaperLowSeatCount + "张" + result1 + "。";
        }
        else {
            result = "指定" + result1 + "，至少需要" + PaperLowSeatCount + "张" + result1 + "。     (无法满足定制服务时：" + result2 + ")";
        }
        return result;
    }

    public void handleTaobaoCustomizeProblem() throws Exception {
        String orderidStr = "1387027,1387025,1387023,1387021,1387020,1387019,1387018,1387017,1387015,1387014,1387012,1387011,1387008,1387005,1387004,1387002,1386999,1386998,1386996,1386995,1386994,1386990,1386989,1386986,1386985,1386984,1386983,1386981,1386980,1386979,1386975,1386974,1386971,1386970,1386969,1386968,1386967,1386966,1386965,1386964,1386963,1386958,1386956,1386955,1386954,1386952,1386951,1386950,1386947,1386946,1386945,1386939,1386938,1386934,1386933,1386928,1386926,1386925,1386922,1386920,1386917,1386915,1386914,1386913,1386912,1386911,1386907,1386906,1386904,1386903,1386900,1386898,1386897,1386894";
        
        String[] orderidArray = orderidStr.split(",");

        System.out.println("总共需要处理:"+orderidArray.length+"单");
        int flag = 1;
        for (String orderid:orderidArray) {
            System.out.println("正在处理第"+(flag++)+"单");
            
            Long orderidL = Long.valueOf(orderid);
            
            TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderidL);
            
            String PaperType = trainOrderOffline.getPaperType()+"";
            String PaperBackup = trainOrderOffline.getPaperBackup()+"";
            String PaperLowSeatCount = trainOrderOffline.getPaperLowSeatCount()+"";
            
            Integer TicketCount = trainOrderOffline.getTicketCount();
            
            //系统订单号-定制服务=实际出的票#备注信息
            
            StringBuilder sb = new StringBuilder("票的张数:"+TicketCount);
            List<TrainTicketOffline> trainTicketOfflineList = trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(orderidL);
            for (int i = 0; i < trainTicketOfflineList.size(); i++) {
                TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(i);
                sb.append(",原价:"+trainTicketOffline.getPrice()+",实际出票坐席:"+trainTicketOffline.getRealSeat()
                +",车厢:"+trainTicketOffline.getCoach()+",座位:"+trainTicketOffline.getSeatNo()+",价格:"+trainTicketOffline.getSealPrice());
            }

            String result = "系统订单号:"+trainOrderOffline.getOrderNumber()+"定制服务:"+getAllPaperTypestaobao(PaperType, PaperBackup, PaperLowSeatCount)
                +"实际出的票-"+sb.toString()+",备注信息-"+trainOrderOffline.getRemark();
            System.out.println(result);
            WriteLog.write("淘宝定制错误订单处理", result);
        }
    }

    public void expHandleTaobaoCustomizeProblem() throws Exception {
        OutputStream os=new FileOutputStream("d:\\淘宝定制错误订单处理.xls" ); 
        WritableWorkbook workbook = Workbook.createWorkbook(os);
        WritableSheet sheet = workbook.createSheet("淘宝定制错误订单处理", 0);

        int j = 0;
        Label label0 = new Label(j, 0, "系统订单号");
        sheet.addCell(label0);
        sheet.setColumnView(j++, 10); //设置列宽度
        Label label1 = new Label(j, 0, "定制服务");
        sheet.addCell(label1);
        sheet.setColumnView(j++, 10);
        Label label2 = new Label(j, 0, "实际出的票");
        sheet.addCell(label2);
        sheet.setColumnView(j++, 25);
        
        Label label3 = new Label(j, 0, "备注信息");
        sheet.addCell(label3);
        sheet.setColumnView(j++, 25);

        int i = 0;

        String orderidStr = "1387027,1387025,1387023,1387021,1387020,1387019,1387018,1387017,1387015,1387014,1387012,1387011,1387008,1387005,1387004,1387002,1386999,1386998,1386996,1386995,1386994,1386990,1386989,1386986,1386985,1386984,1386983,1386981,1386980,1386979,1386975,1386974,1386971,1386970,1386969,1386968,1386967,1386966,1386965,1386964,1386963,1386958,1386956,1386955,1386954,1386952,1386951,1386950,1386947,1386946,1386945,1386939,1386938,1386934,1386933,1386928,1386926,1386925,1386922,1386920,1386917,1386915,1386914,1386913,1386912,1386911,1386907,1386906,1386904,1386903,1386900,1386898,1386897,1386894";
        
        String[] orderidArray = orderidStr.split(",");

        System.out.println("总共需要处理:"+orderidArray.length+"单");
        int flag = 1;
        for (String orderid:orderidArray) {
            System.out.println("正在处理第"+(flag++)+"单");
            
            Long orderidL = Long.valueOf(orderid);
            
            TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderidL);
            
            String PaperType = trainOrderOffline.getPaperType()+"";
            String PaperBackup = trainOrderOffline.getPaperBackup()+"";
            String PaperLowSeatCount = trainOrderOffline.getPaperLowSeatCount()+"";
            
            Integer TicketCount = trainOrderOffline.getTicketCount();
            
            //系统订单号-定制服务=实际出的票#备注信息
            
            StringBuilder sb = new StringBuilder("票的张数:"+TicketCount);
            List<TrainTicketOffline> trainTicketOfflineList = trainTicketOfflineDao.findTrainTicketOfflineListByOrderId(orderidL);
            for (int m = 0; m < trainTicketOfflineList.size(); m++) {
                TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(m);
                sb.append(",原价:"+trainTicketOffline.getPrice()+",实际出票坐席:"+trainTicketOffline.getRealSeat()
                +",车厢:"+trainTicketOffline.getCoach()+",座位:"+trainTicketOffline.getSeatNo()+",价格:"+trainTicketOffline.getSealPrice());
            }

            String OrderNumber = trainOrderOffline.getOrderNumber();
            String PaperTypesTaoBao = getAllPaperTypestaobao(PaperType, PaperBackup, PaperLowSeatCount);
            String Remark = trainOrderOffline.getRemark();
            
            String result = "系统订单号:"+OrderNumber+"定制服务:"+PaperTypesTaoBao+"实际出的票-"+sb.toString()+",备注信息-"+Remark;
            
            System.out.println(result);
            WriteLog.write("淘宝定制错误订单处理", result);
            
            i += 1;
            j = 0;
            
            Label labe0 = new Label(j++, i, OrderNumber);//
            sheet.addCell(labe0);
            Label labe1 = new Label(j++, i, PaperTypesTaoBao);//
            sheet.addCell(labe1);
            Label labe2 = new Label(j++, i, sb.toString());//
            sheet.addCell(labe2);
            Label labe3 = new Label(j++, i, Remark);//
            sheet.addCell(labe3);
        }
        
        workbook.write();
        workbook.close();
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        TrainOrderOfflineDaoTest trainOrderOfflineDaoTest = new TrainOrderOfflineDaoTest();
        
        //trainOrderOfflineDaoTest.testAddTrainOrderOffline();
        trainOrderOfflineDaoTest.testFindTrainOrderOfflineById();
        //trainOrderOfflineDaoTest.testFindIsLockCallbackById();
        //trainOrderOfflineDaoTest.testDelTrainOrderOfflineById();
        
        //trainOrderOfflineDaoTest.testUpdateOrderTimeoutById();
        
        //trainOrderOfflineDaoTest.testFindTrainOrderOfflineByOrderNumberOnline();
        
        //trainOrderOfflineDaoTest.testTuNiuDesUtil();
        
        //trainOrderOfflineDaoTest.getCancelOrderParam();
        
        //trainOrderOfflineDaoTest.handleTaobaoCustomizeProblem();
        //trainOrderOfflineDaoTest.expHandleTaobaoCustomizeProblem();

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }
}
