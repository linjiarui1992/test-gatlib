package com.ccservice.b2b2c.policy.officialWebsite;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.airepay.upcztouser.webservice.OrderAndPaymentMultServiceStub;
import net.airepay.upcztouser.webservice.QueryPriceServiceStub;
import net.airepay.upcztouser.webservice.QueryTicketNoServiceStub;

import org.apache.axis2.AxisFault;

import com.airepay.yhb.security.Security;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.flightinfo.CarbinInfo;
import com.ccservice.b2b2c.base.flightinfo.FlightInfo;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.b2b2c.policy.util.PolicyUtil;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.qunar.util.ZipUtil;
import com.tenpay.util.MD5Util;

/**
 * 南航官网数据查询
 * 
 * @time 2015年6月2日 下午1:59:44
 * @author chendong
 */
public class OfficialWebsiteCz {
    static Security s = new Security();

    public static void main(String[] args) {
        Long l1 = System.currentTimeMillis();
        //        SeachFlight("PEK", "SHA", "", "2015-07-01", "1");
        //        createOrderAndPay();
        getTicketNoByOrderId("C1404240017256");
        System.out.println("耗时:" + (System.currentTimeMillis() - l1));
    }

    /**
     * 
     * 
     * @param OrderNo 航空公司订单号
     * 
     * @time 2015年6月4日 下午4:40:34
     * @author chendong
     */
    public static void getTicketNoByOrderId(String OrderNo) {
        try {
            QueryTicketNoServiceStub stub = new QueryTicketNoServiceStub();
            QueryTicketNoServiceStub.QueryTicketNo queryTicketNo = new QueryTicketNoServiceStub.QueryTicketNo();
            //            Account varchar 255 M 登录账户
            //                      String Account = "289486189@qq.com";//专区登录账户(银联协助用户开通)
            String Account = "zhaoxinbo1980929@163.com";//专区登录账户(银联协助用户开通)
            //            String Account = PropertyUtil.getValue("OfficialWebsiteCz_Account", "air.properties");//专区登录账户(银联协助用户开通) varchar 255 M 登录账户
            //            String OrderNo = "";// varchar 20 M 航空公司订单号，
            String ReqTime = TimeUtil.gettodaydate(4);// varchar 19 M 请求时间
            String Reserved01 = "";// varchar 100 M 保留域1
            String Reserved02 = "";// varchar 100 M 保留域2
            //            OrderNo varchar 20 M 航空公司订单号，
            //            ReqTime varchar 19 M 请求时间
            //            Reserved01 varchar 100 M 保留域1
            //            Reserved02 varchar 100 M 保留域2
            JSONObject jsonobject_in0 = new JSONObject();
            jsonobject_in0.put("Account", Account);//          varchar 10 M 航班号
            jsonobject_in0.put("OrderNo", OrderNo);// varchar 20 M 航空公司订单号，
            jsonobject_in0.put("ReqTime", ReqTime);// varchar 19 M 请求时间
            jsonobject_in0.put("Reserved01", Reserved01);// varchar 100 M 保留域1
            jsonobject_in0.put("Reserved02", Reserved02);// varchar 100 M 保留域2

            String sourceMsg = jsonobject_in0.toJSONString();//原文PlaneText
            System.out.println(sourceMsg);
            //            Security s = new Security();
            OfficialWebsiteCzSecurity s = new OfficialWebsiteCzSecurity();
            //            String certPath_cer = "D:\\UPCZ_EC.cer";
            String certPath = PropertyUtil.getValue("OfficialWebsiteCz_certPath_cer_path", "air.properties");
            //加密
            //            Boolean b = s.encryptMsg("要加密的明文", "证书公钥路径");
            Boolean b = s.encryptMsg(sourceMsg, certPath);
            if (b) {
                //获取加密密文
                String ms = s.getLastResultMsg();
                //                System.out.println("=================请求加密后=================");
                //                System.out.println(ms);
                queryTicketNo.setIn0(ms);
                String in1 = MD5Util.MD5Encode(sourceMsg, "UTF-8").toUpperCase();
                //                System.out.println("=================MD5加密后=================");
                //                System.out.println(in1);
                queryTicketNo.setIn1(in1);

                QueryTicketNoServiceStub.QueryTicketNoResponse response = stub.queryTicketNo(queryTicketNo);
                String outString = response.getOut();
                WriteLog.write("OfficialWebsiteCz_getTicketNoByOrderId", outString);
                JSONObject jsonobject_outString = JSONObject.parseObject(outString);
                String EncodeMsg = jsonobject_outString.getString("EncodeMsg");
                String SignMsg = jsonobject_outString.getString("SignMsg");
                //                System.out.println("=================EncodeMsg=================");
                //                System.out.println(EncodeMsg);
                //                System.out.println("=================SignMsg=================");
                //                System.out.println(SignMsg);
                String certPath2 = PropertyUtil.getValue("OfficialWebsiteCz_certPath2_pfx_path", "air.properties");
                String pwd = PropertyUtil.getValue("OfficialWebsiteCz_pwd", "air.properties");
                //                Boolean b = s.decryptMsg("要解密的密文", "私钥证书路径", "私钥保护PIN码");
                //                b = s.decryptMsg(EncodeMsg, certPath2, pwd);
                //                OfficialWebsiteCzSecurity s = new OfficialWebsiteCzSecurity();
                b = s.decryptMsg(EncodeMsg, certPath2, pwd);
                //获取解密明文
                if (b) {
                    ms = s.getLastResultMsg();
                    //                    System.out.println("=================解密后明文(未解压=================");
                    WriteLog.write("OfficialWebsiteCz_getTicketNoByOrderId",
                            "=================解密后明文(未解压=================");
                    WriteLog.write("OfficialWebsiteCz_getTicketNoByOrderId", ms);
                    //                    System.out.println(ms);
                    //                    resultData = ZipUtil.gunzip(ms);
                    //                    System.out.println("=================解密后明文(已压缩=================");
                    //                    System.out.println(resultData);
                }
                else {
                    System.out.println("解密失败");
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建订单并支付
     * 
     * @time 2015年6月3日 下午6:18:52
     * @author chendong
     */
    public static void createOrderAndPay() {
        String resultData = "-1";
        String FlightNo = "CZ6412";//            varchar 10 M 航班号
        String PriceType = "1";//          char 1 M 价格类型默认值1：官网运价
        String Cabin = "E";//              char 1 M 仓位
        String PreSetAuldtPrice = "";//   varchar 12 N 单个成人票面价，用于校验，可空
        String PreSetTotalPrice = "";//   varchar 1 M 订单支付总价，用于校验，可空
        String PassengerInfo = "陈栋^412823198909298017^^N";//      varchar 300 M 乘机人信息：姓名1^证件号1^^是否购买保险1~姓名2^证件号2^^是否购买保险2
        String Contactor = "陈栋";//          varchar 60 M 联系人姓名
        String ContactorPhone = "13522543333";//     varchar 30 M 联系电话
        String ContactorEmail = "249016428@qq.com";//     varchar 255 M 联系邮箱
        String ContactorTel = "4008803351";//       varchar 20 M 联系座机
        String SegType = "S";//            char 1 M 航程类型，默认值S，S 表示单程，目前只支持单程
        String DepartCity = "PEK";//             char 3 M 起飞城市三字码
        String ArriveCity = "SHA";//             char 3 M 抵达城市三字码
        String DepartDate = "20150617";//             varchar 10 M 起飞日期，例如：20140610
        String AdultNum = "1";//           char 1 M 成人数量
        String ChildNum = "0";//           char 1 M 默认0，暂时不支持儿童票
        String InfantNum = "0";//          char 1 M 默认0，暂时不支持婴儿
        String OtherMsg = "";//           varchar 100 M 备注信息
        String Reserved01 = "";//             varchar 100 M 保留域1
        String Reserved02 = "";//             varchar 100 M 保留域2
        //            String Account = "289486189@qq.com";//专区登录账户(银联协助用户开通)
        String Account = PropertyUtil.getValue("OfficialWebsiteCz_Account", "air.properties");//专区登录账户(银联协助用户开通) varchar 255 M 登录账户
        //            String Password = "tq64540699";//登录密码
        String Password = PropertyUtil.getValue("OfficialWebsiteCz_Account_Password", "air.properties");//登录密码 varchar 32 M 登录密码
        String DoPayAccount = "helloworld";//       varchar 32 M 易航宝支付账户号(银联提供给用户)
        String DoPayPassword = "helloworld";//      varchar 32 M 易航宝支付账户密码(银联提供给用户)
        String CardNo = "helloworld";//                 varchar 20 N 明珠卡号，可为空,暂时不需要改参数
        String PayType = "1";//            char 1 M 支付渠道，1:易航宝支付
        String PayMode = "0";//            char 2 M 账户类型，0:默认账户,1:现金账户,2:额度账户

        try {
            OrderAndPaymentMultServiceStub stub = new OrderAndPaymentMultServiceStub();
            OrderAndPaymentMultServiceStub.OrderAndPaymentMult orderAndPaymentMult = new OrderAndPaymentMultServiceStub.OrderAndPaymentMult();
            JSONObject jsonobject_in0 = new JSONObject();
            jsonobject_in0.put("FlightNo", FlightNo);//          varchar 10 M 航班号
            jsonobject_in0.put("PriceType", PriceType);//            char 1 M 价格类型默认值1：官网运价
            jsonobject_in0.put("Cabin", Cabin);//                char 1 M 仓位
            jsonobject_in0.put("PreSetAuldtPrice", PreSetAuldtPrice);//  varchar 12 N 单个成人票面价，用于校验，可空
            jsonobject_in0.put("PreSetTotalPrice", PreSetTotalPrice);//  varchar 1 M 订单支付总价，用于校验，可空
            jsonobject_in0.put("PassengerInfo", PassengerInfo);//        varchar 300 M 乘机人信息：姓名1^证件号1^^是否购买保险1~姓名2^证件号2^^是否购买保险2
            jsonobject_in0.put("Contactor", Contactor);//            varchar 60 M 联系人姓名
            jsonobject_in0.put("ContactorPhone", ContactorPhone);//      varchar 30 M 联系电话
            jsonobject_in0.put("ContactorEmail", ContactorEmail);//      varchar 255 M 联系邮箱
            jsonobject_in0.put("ContactorTel", ContactorTel);//      varchar 20 M 联系座机
            jsonobject_in0.put("SegType", SegType);//            char 1 M 航程类型，默认值S，S 表示单程，目前只支持单程
            jsonobject_in0.put("DepartCity", DepartCity);//          char 3 M 起飞城市三字码
            jsonobject_in0.put("ArriveCity", ArriveCity);//          char 3 M 抵达城市三字码
            jsonobject_in0.put("DepartDate", DepartDate);//          varchar 10 M 起飞日期，例如：20140610
            jsonobject_in0.put("AdultNum", AdultNum);//          char 1 M 成人数量
            jsonobject_in0.put("ChildNum", ChildNum);//          char 1 M 默认0，暂时不支持儿童票
            jsonobject_in0.put("InfantNum", InfantNum);//            char 1 M 默认0，暂时不支持婴儿
            jsonobject_in0.put("OtherMsg", OtherMsg);//          varchar 100 M 备注信息
            jsonobject_in0.put("Reserved01", Reserved01);//          varchar 100 M 保留域1
            jsonobject_in0.put("Reserved02", Reserved02);//          varchar 100 M 保留域2
            jsonobject_in0.put("Account", Account);//            varchar 255 M 登录账户
            jsonobject_in0.put("Password", Password);//          varchar 32 M 登录密码
            jsonobject_in0.put("DoPayAccount", DoPayAccount);//      varchar 32 M 易航宝支付账户号(银联提供给用户)
            jsonobject_in0.put("DoPayPassword", DoPayPassword);//        varchar 32 M 易航宝支付账户密码(银联提供给用户)
            jsonobject_in0.put("CardNo", CardNo);//              varchar 20 N 明珠卡号，可为空,暂时不需要改参数
            jsonobject_in0.put("PayType", PayType);//            char 1 M 支付渠道，1:易航宝支付
            jsonobject_in0.put("PayMode", PayMode);//            char 2 M 账户类型，0:默认账户,1:现金账户,2:额度账户

            String sourceMsg = jsonobject_in0.toJSONString();//原文PlaneText
            System.out.println(sourceMsg);
            //            Security s = new Security();
            OfficialWebsiteCzSecurity s = new OfficialWebsiteCzSecurity();
            //            String certPath_cer = "D:\\UPCZ_EC.cer";
            String certPath = PropertyUtil.getValue("OfficialWebsiteCz_certPath_cer_path", "air.properties");
            //加密
            //            Boolean b = s.encryptMsg("要加密的明文", "证书公钥路径");
            Boolean b = s.encryptMsg(sourceMsg, certPath);
            if (b) {
                //获取加密密文
                String ms = s.getLastResultMsg();
                System.out.println("=================请求加密后=================");
                System.out.println(ms);
                orderAndPaymentMult.setIn0(ms);
                String in1 = MD5Util.MD5Encode(sourceMsg, "UTF-8").toUpperCase();
                System.out.println("=================MD5加密后=================");
                System.out.println(in1);
                orderAndPaymentMult.setIn1(in1);

                OrderAndPaymentMultServiceStub.OrderAndPaymentMultResponse response = stub
                        .orderAndPaymentMult(orderAndPaymentMult);
                String outString = response.getOut();
                WriteLog.write("OfficialWebsiteCz_createOrderAndPay", outString);
                JSONObject jsonobject_outString = JSONObject.parseObject(outString);
                String EncodeMsg = jsonobject_outString.getString("EncodeMsg");
                String SignMsg = jsonobject_outString.getString("SignMsg");
                System.out.println("=================EncodeMsg=================");
                System.out.println(EncodeMsg);
                System.out.println("=================SignMsg=================");
                System.out.println(SignMsg);
                String certPath2 = PropertyUtil.getValue("OfficialWebsiteCz_certPath2_pfx_path", "air.properties");
                String pwd = PropertyUtil.getValue("OfficialWebsiteCz_pwd", "air.properties");
                //                Boolean b = s.decryptMsg("要解密的密文", "私钥证书路径", "私钥保护PIN码");
                //                b = s.decryptMsg(EncodeMsg, certPath2, pwd);
                //                OfficialWebsiteCzSecurity s = new OfficialWebsiteCzSecurity();
                b = s.decryptMsg(EncodeMsg, certPath2, pwd);
                //获取解密明文
                if (b) {
                    ms = s.getLastResultMsg();
                    System.out.println("=================解密后明文(未解压=================");
                    WriteLog.write("OfficialWebsiteCz_createOrderAndPay", "=================解密后明文(未解压=================");
                    WriteLog.write("OfficialWebsiteCz_createOrderAndPay", ms);
                    System.out.println(ms);
                    //                    resultData = ZipUtil.gunzip(ms);
                    //                    System.out.println("=================解密后明文(已压缩=================");
                    //                    System.out.println(resultData);
                }
                else {
                    System.out.println("解密失败");
                }
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 航班查询
     * 
     * @param scitycode PEK
     * @param ecitycode SHA
     * @param airlineCode 
     * @param depdate 2015-07-01
     * @param type
     * @return
     * @time 2015年6月3日 下午2:23:26
     * @author chendong
     */
    public static List SeachFlight(String scitycode, String ecitycode, String airlineCode, String depdate, String type) {
        List<FlightInfo> listFlightInfoAll = new ArrayList<FlightInfo>();
        WriteLog.write("OfficialWebsiteCz_SeachFlight", scitycode + ":" + ecitycode + ":" + airlineCode + ":" + depdate);
        //        String JSONString = "{\"FlightDetail\":{\"segments\":[{\"adultFuelTax\":\"0\",\"arrCity\":\"SHA\",\"childFuelTax\":\"0\",\"date \":\"20150617\",\"dateFlight\":{\"directFlight\":[{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"0840\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"J\",\"adultPrice\":\"3100\",\"brandtype\":\"J\",\"childPrice\":\"1550\",\"infantPrice\":\"310\",\"gbAdultPrice\":\"3100\",\"name\":\"J\",\"secondPrices\":null},{\"adultFareBasis\":\"CPRH5W\",\"adultPrice\":\"1860\",\"brandtype\":\"J\",\"childPrice\":\"1860\",\"infantPrice\":\"220\",\"gbAdultPrice\":\"2480\",\"name\":\"C\",\"secondPrices\":null},{\"adultFareBasis\":\"DPRK2\",\"adultPrice\":\"1620\",\"brandtype\":\"J\",\"childPrice\":\"1620\",\"infantPrice\":\"220\",\"gbAdultPrice\":\"0\",\"name\":\"D\",\"secondPrices\":null},{\"adultFareBasis\":\"IPRL\",\"adultPrice\":\"1240\",\"brandtype\":\"J\",\"childPrice\":\"1240\",\"infantPrice\":\"220\",\"gbAdultPrice\":\"0\",\"name\":\"I\",\"secondPrices\":null},{\"adultFareBasis\":\"W\",\"adultPrice\":\"1240\",\"brandtype\":\"W\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"W\",\"secondPrices\":null},{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"HDSAFW10\",\"adultPrice\":\"840\",\"brandtype\":\"SW\",\"childPrice\":\"840\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"930\",\"name\":\"H\",\"secondPrices\":null},{\"adultFareBasis\":\"UDSAFW10\",\"adultPrice\":\"780\",\"brandtype\":\"LX\",\"childPrice\":\"780\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"LDSAFW10\",\"adultPrice\":\"670\",\"brandtype\":\"LX\",\"childPrice\":\"670\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null},{\"adultFareBasis\":\"EDSAFW10\",\"adultPrice\":\"560\",\"brandtype\":\"LX\",\"childPrice\":\"560\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"620\",\"name\":\"E\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"0635\",\"depart\":\"PEK\",\"flightNo\":\"CZ6412\",\"plane\":\"321\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"0910\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"L\",\"adultPrice\":\"740\",\"brandtype\":\"LX\",\"childPrice\":\"740\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null},{\"adultFareBasis\":\"E\",\"adultPrice\":\"620\",\"brandtype\":\"LX\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"620\",\"name\":\"E\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"0700\",\"depart\":\"PEK\",\"flightNo\":\"CZ9307\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1010\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"L\",\"adultPrice\":\"740\",\"brandtype\":\"LX\",\"childPrice\":\"740\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"0800\",\"depart\":\"PEK\",\"flightNo\":\"CZ9271\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1045\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"F\",\"adultPrice\":\"3470\",\"brandtype\":\"F\",\"childPrice\":\"1740\",\"infantPrice\":\"350\",\"gbAdultPrice\":\"3470\",\"name\":\"F\",\"secondPrices\":null},{\"adultFareBasis\":\"PPRL\",\"adultPrice\":\"1240\",\"brandtype\":\"F\",\"childPrice\":\"1240\",\"infantPrice\":\"350\",\"gbAdultPrice\":\"2850\",\"name\":\"P\",\"secondPrices\":null},{\"adultFareBasis\":\"W\",\"adultPrice\":\"1240\",\"brandtype\":\"W\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"W\",\"secondPrices\":null},{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"0820\",\"depart\":\"PEK\",\"flightNo\":\"CZ3907\",\"plane\":\"330\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1110\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"0900\",\"depart\":\"PEK\",\"flightNo\":\"CZ9273\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1210\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1000\",\"depart\":\"PEK\",\"flightNo\":\"CZ9275\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1310\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1100\",\"depart\":\"PEK\",\"flightNo\":\"CZ9277\",\"plane\":\"763\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1340\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1130\",\"depart\":\"PEK\",\"flightNo\":\"CZ9311\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1420\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1200\",\"depart\":\"PEK\",\"flightNo\":\"CZ9279\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1510\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1300\",\"depart\":\"PEK\",\"flightNo\":\"CZ9281\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1545\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1335\",\"depart\":\"PEK\",\"flightNo\":\"CZ9313\",\"plane\":\"763\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1620\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1400\",\"depart\":\"PEK\",\"flightNo\":\"CZ9283\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1710\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1500\",\"depart\":\"PEK\",\"flightNo\":\"CZ9285\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1810\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1600\",\"depart\":\"PEK\",\"flightNo\":\"CZ9287\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"1915\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1700\",\"depart\":\"PEK\",\"flightNo\":\"CZ9289\",\"plane\":\"763\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2010\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1800\",\"depart\":\"PEK\",\"flightNo\":\"CZ9291\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2045\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"H\",\"adultPrice\":\"930\",\"brandtype\":\"SW\",\"childPrice\":\"930\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"930\",\"name\":\"H\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"1820\",\"depart\":\"PEK\",\"flightNo\":\"CZ5138\",\"plane\":\"738\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2045\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1830\",\"depart\":\"PEK\",\"flightNo\":\"CZ9315\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2110\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"1900\",\"depart\":\"PEK\",\"flightNo\":\"CZ9293\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2210\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":null,\"depDate\":\"20150617\",\"depTime\":\"2000\",\"depart\":\"PEK\",\"flightNo\":\"CZ9295\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2315\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"L\",\"adultPrice\":\"740\",\"brandtype\":\"LX\",\"childPrice\":\"740\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"2100\",\"depart\":\"PEK\",\"flightNo\":\"CZ9297\",\"plane\":\"333\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2340\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"L\",\"adultPrice\":\"740\",\"brandtype\":\"LX\",\"childPrice\":\"740\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"2130\",\"depart\":\"PEK\",\"flightNo\":\"CZ9317\",\"plane\":\"763\"},{\"airline\":\"CZ\",\"arrDate\":\"20150617\",\"arrTime\":\"2355\",\"arrive\":\"SHA\",\"tax\":\"50\",\"cabins\":[{\"adultFareBasis\":\"Y\",\"adultPrice\":\"1240\",\"brandtype\":\"SW\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1240\",\"name\":\"Y\",\"secondPrices\":null},{\"adultFareBasis\":\"B\",\"adultPrice\":\"1120\",\"brandtype\":\"SW\",\"childPrice\":\"1120\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"1120\",\"name\":\"B\",\"secondPrices\":null},{\"adultFareBasis\":\"M\",\"adultPrice\":\"990\",\"brandtype\":\"SW\",\"childPrice\":\"990\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"990\",\"name\":\"M\",\"secondPrices\":null},{\"adultFareBasis\":\"U\",\"adultPrice\":\"870\",\"brandtype\":\"LX\",\"childPrice\":\"870\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"870\",\"name\":\"U\",\"secondPrices\":null},{\"adultFareBasis\":\"L\",\"adultPrice\":\"740\",\"brandtype\":\"LX\",\"childPrice\":\"740\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"740\",\"name\":\"L\",\"secondPrices\":null},{\"adultFareBasis\":\"E\",\"adultPrice\":\"620\",\"brandtype\":\"LX\",\"childPrice\":\"620\",\"infantPrice\":\"120\",\"gbAdultPrice\":\"620\",\"name\":\"E\",\"secondPrices\":null}],\"depDate\":\"20150617\",\"depTime\":\"2200\",\"depart\":\"PEK\",\"flightNo\":\"CZ9319\",\"plane\":\"333\"}]},\"depCity\":\"PEK\",\"infantFuelTax\":\"0\"}]},\"ReqTime\":\"2015-06-02 17:58:33\",\"ResTime\":\"2015-06-02 17:58:35\",\"RespCode\":\"00\",\"Msg\":\"成功\",\"Reserved01\":\"\",\"Reserved02\":\"\"}";
        String JSONString = getdataJsonString(scitycode, ecitycode, depdate);
        WriteLog.write("OfficialWebsiteCz_SeachFlight", JSONString);
        JSONObject jsonobject = JSONObject.parseObject(JSONString);
        JSONArray jsonarray = jsonobject.getJSONObject("FlightDetail").getJSONArray("segments");
        for (int i = 0; i < jsonarray.size(); i++) {
            JSONObject jsonobject_segments = jsonarray.getJSONObject(i);
            String fuelFee = jsonobject_segments.getString("adultFuelTax");
            //            System.out.println(jsonobject_segments.toJSONString());
            JSONArray jsonarray_dateflight = jsonobject_segments.getJSONObject("dateFlight").getJSONArray(
                    "directFlight");
            for (int j = 0; j < jsonarray_dateflight.size(); j++) {//循环航班
                FlightInfo flighInfo = new FlightInfo();
                /*
                if (stopnum > 0) {
                    flighInfo.setIsStopInfo(stopnum + "");
                }
                String com = flightNo.substring(0, 2);
                // 出发时间
                */
                JSONObject jsonarray_dateflight_segments = jsonarray_dateflight.getJSONObject(j);
                //所有仓位信息
                JSONArray jsonarray_dateflight_segments_cabins = jsonarray_dateflight_segments.getJSONArray("cabins");
                if (jsonarray_dateflight_segments_cabins != null && jsonarray_dateflight_segments_cabins.size() > 0) {
                    //                    System.out.println(jsonarray_dateflight_segments.toJSONString());
                    String depTime = jsonarray_dateflight_segments.getString("depTime");
                    String arrive = jsonarray_dateflight_segments.getString("arrive");
                    String arrDate = jsonarray_dateflight_segments.getString("arrDate");
                    String flightNo = jsonarray_dateflight_segments.getString("flightNo");
                    String depDate = jsonarray_dateflight_segments.getString("depDate");
                    String plane = jsonarray_dateflight_segments.getString("plane");
                    String tax = jsonarray_dateflight_segments.getString("tax");
                    String depart = jsonarray_dateflight_segments.getString("depart");
                    String arrTime = jsonarray_dateflight_segments.getString("arrTime");
                    String airline = jsonarray_dateflight_segments.getString("airline");
                    flighInfo.setAirCompany(airline);// 航空公司二字码
                    flighInfo.setAirCompanyName(airline);// 航空公司名字
                    flighInfo.setAirline(flightNo);// 航班号
                    flighInfo.setAirplaneType(plane);// 机型
                    flighInfo.setAirportFee(Integer.parseInt(tax));// 基建
                    flighInfo.setFuelFee(Integer.parseInt(fuelFee));// 燃油费
                    flighInfo.setStartAirport(depart);// 起飞城市三字码
                    flighInfo.setEndAirport(arrive);// 到达城市三字码
                    flighInfo.setOffPointAT("");//出发航站楼
                    flighInfo.setBorderPointAT("");//到达航站楼
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmm");
                    Date startDate = null;
                    try {
                        startDate = dateFormat.parse(depDate + " " + depTime);
                    }
                    catch (ParseException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    flighInfo.setDepartTime(new Timestamp(startDate.getTime()));// 起飞时间
                    // 到达时间
                    Date arriveDate = null;
                    try {
                        arriveDate = dateFormat.parse(depDate + " " + arrTime);
                    }
                    catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    flighInfo.setArriveTime(new Timestamp(arriveDate.getTime()));// 降落时间

                    //                    System.out.println("=======================" + flightNo);
                    //                    System.out.println(depTime + ":" + arrive + ":" + arrDate + ":" + flightNo + ":" + depDate + ":"
                    //                            + plane + ":" + tax + ":" + depart + ":" + arrTime + ":" + airline);
                    float yprice = 0f;// Y仓位价格
                    // 定义仓位list
                    List<CarbinInfo> listCabinAll = new ArrayList<CarbinInfo>();
                    for (int k = 0; k < jsonarray_dateflight_segments_cabins.size(); k++) {
                        CarbinInfo cabin = new CarbinInfo();
                        JSONObject jsonarray_dateflight_segments_cabin = jsonarray_dateflight_segments_cabins
                                .getJSONObject(k);
                        String childPrice = jsonarray_dateflight_segments_cabin.getString("childPrice");//婴儿价
                        String gbAdultPrice = jsonarray_dateflight_segments_cabin.getString("gbAdultPrice");
                        String adultFareBasis = jsonarray_dateflight_segments_cabin.getString("adultFareBasis");//成人票价参考
                        String adultPrice = jsonarray_dateflight_segments_cabin.getString("adultPrice");//成人价
                        String name = jsonarray_dateflight_segments_cabin.getString("name");//婴儿价
                        String infantPrice = jsonarray_dateflight_segments_cabin.getString("infantPrice");//婴儿价
                        String brandtype = jsonarray_dateflight_segments_cabin.getString("brandtype");//仓位
                        if ("Y".equals(name)) {
                            yprice = Float.parseFloat(adultPrice);
                        }
                        cabin.setCabin(name);
                        cabin.setSeatNum("9");// 座位数
                        cabin.setDiscount(0.0f);
                        cabin.setPrice(Float.parseFloat(adultPrice));
                        String cabinTypeName = getcabinTypeName(brandtype);
                        //                        System.out.println("cabinTypeName======================================" + cabinTypeName);
                        cabin.setCabintypename(cabinTypeName);
                        //                        System.out.println(childPrice + ":" + gbAdultPrice + ":" + adultFareBasis + ":" + adultPrice
                        //                                + ":" + name + ":" + infantPrice + ":" + brandtype);//
                        listCabinAll.add(cabin);
                    }

                    flighInfo.setYPrice(yprice);// Y仓位价格
                    listCabinAll = PolicyUtil.reSetDiscount(listCabinAll, yprice);//计算仓位的折扣
                    // 从新设置折扣开始
                    for (int m = 0; m < listCabinAll.size(); m++) {
                        if (listCabinAll.get(m).getPrice() != 0 && yprice > 0) {
                            Float dis = listCabinAll.get(m).getPrice() / yprice;
                            dis = Float.parseFloat(PolicyUtil.formatMoney(dis));
                            // Float st=dis*100;
                            BigDecimal big = new BigDecimal(dis);
                            double f1 = big.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                            listCabinAll.get(m).setDiscount(Float.parseFloat(f1 * 100 + ""));
                            //                            System.out.println("新折扣===" + dis + ",折扣:" + listCabinAll.get(m).getDiscount());
                            if (dis <= 0.4f) {// 特价
                                listCabinAll.get(m).setSpecial(true);
                                //                                System.out.println("特价仓位==" + listCabinAll.get(m).getCabin() + ",价格=="
                                //                                        + listCabinAll.get(m).getPrice() + ",折扣=" + dis);
                            }
                            // System.out.println("新折扣=="+formatMoney_short(dis.toString()));
                        }
                    }
                    listCabinAll = PolicyUtil.sortListCabinAll(listCabinAll);//对仓位排序
                    // 处理最低价结束
                    flighInfo.setCarbins(listCabinAll);
                    int cabinIndex = 0;
                    CarbinInfo lowCabinInfo = new CarbinInfo();
                    for (int k = 0; k < listCabinAll.size(); k++) {
                        if (listCabinAll.get(k).getPrice() > 0) {
                            cabinIndex = k;
                            break;
                        }
                    }
                    if (listCabinAll.size() > 0 && listCabinAll.size() >= cabinIndex) {
                        CarbinInfo tempCabinInfo = (CarbinInfo) listCabinAll.get(cabinIndex);
                        if (tempCabinInfo.getCabin() != null) {
                            lowCabinInfo.setCabin(tempCabinInfo.getCabin());
                        }
                        if (tempCabinInfo.getRatevalue() != null) {
                            lowCabinInfo.setRatevalue(tempCabinInfo.getRatevalue());
                        }
                        if (tempCabinInfo.getCabinRemark() != null) {
                            lowCabinInfo.setCabinRemark(tempCabinInfo.getCabinRemark());
                        }
                        else {
                            lowCabinInfo.setCabinRemark("");
                        }
                        if (tempCabinInfo.getCabinRules() != null) {
                            lowCabinInfo.setCabinRules(tempCabinInfo.getCabinRules());
                        }
                        else {
                            lowCabinInfo.setCabinRules("");
                        }
                        if (tempCabinInfo.getDiscount() != null) {
                            lowCabinInfo.setDiscount(tempCabinInfo.getDiscount());
                        }
                        if (tempCabinInfo.getLevel() != null) {
                            lowCabinInfo.setLevel(tempCabinInfo.getLevel());
                        }
                        else {
                            lowCabinInfo.setLevel(1);
                        }
                        if (tempCabinInfo.getPrice() != null) {
                            lowCabinInfo.setPrice(tempCabinInfo.getPrice());
                        }
                        else {
                            lowCabinInfo.setPrice(0f);
                        }
                        if (tempCabinInfo.getSeatNum() != null) {
                            lowCabinInfo.setSeatNum(tempCabinInfo.getSeatNum());
                        }
                        else {
                            lowCabinInfo.setSeatNum("0");
                        }
                        if (tempCabinInfo.getCabintypename() != null) {
                            lowCabinInfo.setCabintypename(tempCabinInfo.getCabintypename());
                        }
                        else {
                            lowCabinInfo.setCabintypename("");
                        }
                        if (tempCabinInfo.isSpecial()) {
                            lowCabinInfo.setSpecial(true);
                        }
                        else {
                            lowCabinInfo.setSpecial(false);
                        }
                        flighInfo.setLowCarbin(lowCabinInfo);
                    }
                    else {
                        continue;
                    }
                    if (flighInfo != null) {
                        listFlightInfoAll.add(flighInfo);
                    }
                }
                else {
                    continue;
                }
            }
        }
        return listFlightInfoAll;
    }

    private static String getcabinTypeName(String brandtype) {
        String brandtype_String = "经济舱";
        if ("SW".equals(brandtype) || "W".equals(brandtype)) {
            brandtype_String = "商务舱";
        }
        if ("J".equals(brandtype)) {
            brandtype_String = "公务舱";
        }
        else if ("F".equals(brandtype)) {
            brandtype_String = "头等舱";
        }
        else if ("LX".equals(brandtype)) {
            brandtype_String = "经济舱";
        }
        return brandtype_String;
    }

    /**
     * 从接口处获取到数据（JSON） 返回
     * 
     * @param DepartCity
     * @param ArriveCity
     * @param DepartDate
     * @return
     * @time 2015年6月2日 下午6:08:48
     * @author chendong
     */
    private static String getdataJsonString(String DepartCity, String ArriveCity, String DepartDate) {
        String resultData = "-1";
        try {
            QueryPriceServiceStub stub = new QueryPriceServiceStub();//axis2生成的类
            QueryPriceServiceStub.Query query = new QueryPriceServiceStub.Query();//axis2生成的类
            String SegType = "S";//航程类型，默认值S，S 表示单程，目前只支持单程
            //            String DepartCity = "PEK";//PEK 起飞城市三字码
            //            String ArriveCity = "SHA";//抵达城市三字码
            //            String DepartDate = "20150617";//起飞日期，例如：20140610
            String AdultNum = "1";//成人数量
            String ChildNum = "0";//默认0，暂时不支持儿童票
            String InfantNum = "0";//默认0，暂时不支持婴儿
            String OtherMsg = "";//备注信息
            String Reserved01 = "";//保留域1
            String Reserved02 = "";//保留域2
            //            String Account = "289486189@qq.com";//专区登录账户(银联协助用户开通)
            String Account = PropertyUtil.getValue("OfficialWebsiteCz_Account", "air.properties");//专区登录账户(银联协助用户开通)
            //            String Password = "tq64540699";//登录密码
            String Password = PropertyUtil.getValue("OfficialWebsiteCz_Account_Password", "air.properties");//登录密码
            JSONObject jsonobject_in0 = new JSONObject();
            jsonobject_in0.put("SegType", SegType);
            jsonobject_in0.put("DepartCity", DepartCity);
            jsonobject_in0.put("ArriveCity", ArriveCity);
            jsonobject_in0.put("DepartDate", DepartDate);
            jsonobject_in0.put("AdultNum", AdultNum);
            jsonobject_in0.put("ChildNum", ChildNum);
            jsonobject_in0.put("InfantNum", InfantNum);
            jsonobject_in0.put("OtherMsg", OtherMsg);
            jsonobject_in0.put("Reserved01", Reserved01);
            jsonobject_in0.put("Reserved02", Reserved02);
            jsonobject_in0.put("Account", Account);
            jsonobject_in0.put("Password", Password);
            String sourceMsg = jsonobject_in0.toJSONString();//原文PlaneText
            System.out.println(sourceMsg);
            Security s = new Security();
            //            String certPath_cer = "D:\\UPCZ_EC.cer";
            String certPath = PropertyUtil.getValue("OfficialWebsiteCz_certPath_cer_path", "air.properties");
            //加密
            //            Boolean b = s.encryptMsg("要加密的明文", "证书公钥路径");
            Boolean b = s.encryptMsg(sourceMsg, certPath);
            if (b) {
                //获取加密密文
                String ms = s.getLastResultMsg();
                //                System.out.println("=================请求加密后=================");
                //                System.out.println(ms);
                query.setIn0(ms);
                String in1 = MD5Util.MD5Encode(sourceMsg, "utf-8").toUpperCase();
                //                System.out.println("=================MD5加密后=================");
                //                System.out.println(in1);
                query.setIn1(in1);
                QueryPriceServiceStub.QueryResponse response = stub.query(query);
                String outString = response.getOut();
                JSONObject jsonobject_outString = JSONObject.parseObject(outString);
                String EncodeMsg = jsonobject_outString.getString("EncodeMsg");
                String SignMsg = jsonobject_outString.getString("SignMsg");
                //                System.out.println("=================EncodeMsg=================");
                //                System.out.println(EncodeMsg);
                //                System.out.println("=================SignMsg=================");
                //                System.out.println(SignMsg);
                //                String certPath2 = "D:\\UPCZ_TEST.pfx";
                String certPath2 = PropertyUtil.getValue("OfficialWebsiteCz_certPath2_pfx_path", "air.properties");
                //                String pwd = "12345678";
                String pwd = PropertyUtil.getValue("OfficialWebsiteCz_pwd", "air.properties");
                //                Boolean b = s.decryptMsg("要解密的密文", "私钥证书路径", "私钥保护PIN码");
                b = s.decryptMsg(EncodeMsg, certPath2, pwd);
                //获取解密明文
                if (b) {
                    ms = s.getLastResultMsg();
                    //                    System.out.println("=================解密后明文(未解压=================");
                    //                    System.out.println(ms);
                    resultData = ZipUtil.gunzip(ms);
                    //                    System.out.println("=================解密后明文(已压缩=================");
                    //                    System.out.println(resultData);
                }
                else {
                    System.out.println("解密失败");
                }
            }
            else {
                System.out.println("加密失败");
            }
        }
        catch (AxisFault e) {
            e.printStackTrace();
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        return resultData;
    }
}
