/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tuniu.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.SignUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offline.util.TuNiuDesUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO -
 * 
 * 途牛线下票 - 途牛线下票取消请求接口
 * 
 *  取消的逻辑 - 
 *  
 *      锁单之前可以取消
 *  
 *      超时之后可以取消
 *          出票超时 - 订单自动取消 - 无需再次取消 - 且取消没有回调 - 半小时 或者 1个小时
 *          锁单超时 - 之后，一般只会出现拒单的操作 - 调用出票要提示出票的锁单超时，最好操作拒单 - 之后，可以选择取消 - 10分钟或者20分钟
 *  
 *      锁单之中不能取消 - 已锁单的状态
 * 
 * @author: 郑州-技术-郭伟强 E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25
 * @version: v 1.0
 * @since
 *
 */
public class TrainTuNiuOfflineCancelOrderServlet extends HttpServlet {
    private static final String LOGNAME = "途牛线下票取消请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求信息-reqBody-->" + reqBody);

        //幂等响应的初始化操作放在最开始接收到请求的地方

        //结果的返回
        JSONObject resResult = new JSONObject();

        PrintWriter out = response.getWriter();

        Boolean resultFlag = true;

        String reqBodyTemp = reqBody.substring(reqBody.indexOf("data") + 7, reqBody.length() - 2);

        //幂等的设计和实现
        String idempotentFlag = "TuNiuCancelOrder";

        //更换为数据库的持久化形式
        String idempotentLockKey = reqBodyTemp + idempotentFlag + "Lock";

        //String OrderLock = TrainOrderOfflineUtil.TnIdempotent.get(reqBodyTemp+idempotentFlag+"Lock");

        Integer PKID = 0;

        TrainOfflineIdempotent trainOfflineIdempotent = null;

        b: for (int i = 0; i < TrainOrderOfflineUtil.TNIDEMPOTENTRETRY; i++) {//幂等的后续处理-最多尝试三次 - 主要是跳出当前的并发处理逻辑-作为其它请求

            try {
                trainOfflineIdempotent = trainOfflineIdempotentDao
                        .findTrainOfflineIdempotentByLockKey(idempotentLockKey);
            }
            catch (Exception e1) {
                resResult = ExceptionTNUtil.handleTNException(e1);

                //记录请求信息日志
                WriteLog.write("途牛线下票取消请求幂等判定", r1 + ":途牛线下票取消请求幂等设计-幂等数据交互流程报错");

                out.print(resResult.toJSONString());
                out.flush();
                out.close();

                return;
            }

            if (trainOfflineIdempotent == null) {//锁定状态
                try {
                    PKID = Integer
                            .valueOf(trainOfflineIdempotentDao.addTrainOfflineIdempotent(idempotentLockKey, true));
                }
                catch (Exception e) {
                    resResult = ExceptionTNUtil.handleTNException(e);

                    //记录请求信息日志
                    WriteLog.write("途牛线下票取消请求幂等判定", r1 + ":途牛线下票取消请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //初始化之后进行赋值，方便后续使用
                try {
                    trainOfflineIdempotent = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                }
                catch (Exception e) {
                    resResult = ExceptionTNUtil.handleTNException(e);

                    //记录请求信息日志
                    WriteLog.write("途牛线下票取消请求幂等判定", r1 + ":途牛线下票取消请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //做二次健壮性判断
                Integer lockCount = 0;
                try {
                    lockCount = trainOfflineIdempotentDao.findCountByLockKey(idempotentLockKey);
                }
                catch (Exception e) {
                    ExceptionTNUtil.handleTNException(e);
                }
                if (lockCount > 1) {
                    //WriteLog.write("幂等判定流程测试", "进入重复请求处理流程-lockCount:"+lockCount);

                    //删除当前增加，并重新走b循环的流程
                    Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(PKID);
                    if (flagTemp < 1) {
                        //记录请求信息日志
                        WriteLog.write("途牛线下票取消请求幂等判定", "途牛线下票取消请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                    }
                    continue b;
                }

                break b;
            }
            else {
                PKID = trainOfflineIdempotent.getPKID();

                trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutReqNum(trainOfflineIdempotent);//其它次数的请求

                int loopNum = 1;

                a: while (true) {
                    String idempotentResultValue = trainOfflineIdempotent.getIdempotentResultValue();

                    //此处应做实时查询的动作之前，先进行一次判定
                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        try {
                            idempotentResultValue = trainOfflineIdempotentDao.findResultValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);

                            //记录请求信息日志
                            WriteLog.write("途牛线下票取消请求幂等判定", r1 + ":途牛线下票取消请求幂等设计-幂等数据交互流程报错");

                            continue a;
                        }
                    }

                    //等待相同的结果出现之后直接进行反馈

                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {
                        //需要再取出一次做是否已删除的尝试判定 - 
                        //防止循环对象的提前删除
                        Boolean idempotentLockValue = null;
                        try {
                            idempotentLockValue = trainOfflineIdempotentDao.findLockValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);

                            //记录请求信息日志
                            WriteLog.write("途牛线下票取消请求幂等判定", r1 + ":途牛线下票取消请求幂等设计-幂等数据交互流程报错");

                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();

                            return;
                        }

                        if (idempotentLockValue == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }
                            continue b;
                        }

                        try {
                            Thread.sleep(5 * 1000);
                        }
                        catch (InterruptedException e) {
                            ExceptionTNUtil.handleTNException(e);
                        }

                        loopNum++;

                        if (loopNum >= TrainOrderOfflineUtil.TNIDEMPOTENTRETRY) {
                            Boolean resFlag = false;
                            Integer errorCode = 231099;//success true用231000，false用231099

                            resResult = new JSONObject();

                            resResult.put("success", resFlag);
                            resResult.put("errorCode", errorCode);

                            WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求接口-报错之下引起的无限循环的卡断程序启动，接口反馈异常信息");

                            //resResult = JSONObject.parseObject(OrderHandleOverResultFlag);
                            resultFlag = false;

                            break b;
                        }

                        continue a;
                    }
                    else {
                        //幂等中的一次响应 - 该方法涉及到获取和存储，设计成同步的
                        //在这内里做了另外形式的判断
                        trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutResNum(trainOfflineIdempotent);//减少次数的响应

                        if (trainOfflineIdempotent == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }

                            continue b;
                        }

                        resResult = JSONObject.parseObject(idempotentResultValue);
                        resultFlag = false;

                        break b;
                    }
                }
            }

        }

        if (resultFlag) {
            resResult = new JSONObject();

            Boolean resFlag = false;
            Integer errorCode = 231099;//success true用231000，false用231099

            String partnerid = PropertyUtil.getValue("tuniu.account", "Train.GuestAccount.properties");
            String key = PropertyUtil.getValue("tuniu.desKey", "Train.GuestAccount.properties");

            JSONObject requestBody = JSONObject.parseObject(reqBody);
            /**
             * 传入类似空铁接口的JSON串格式的数据，对其直接进行解析
             */
            /*String account = request.getParameter("account");
            String sign = request.getParameter("sign");
            
            String timestamp = request.getParameter("timestamp");
            
            //与当期系统时间进行比对，比对时间戳？？？ - 时间可能有差值，不用比对
            
            String data = request.getParameter("data");*/

            String account = requestBody.getString("account");
            String sign = requestBody.getString("sign");

            String timestamp = requestBody.getString("timestamp");

            String data = requestBody.getString("data");

            //记录请求信息日志
            WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求信息-account-->" + account + "-sign-->" + sign + "-timestamp-->"
                    + timestamp + "-data:" + data);

            //身份校验 - 数据解密，签名校验
            //传的是什么，就用什么解密
            String checkSign = SignUtil.generateSign(account, data, timestamp, key);

            String dataStr = TuNiuDesUtil.decrypt(data);
            //后续再做转换处理
            JSONObject reqData = JSONObject.parseObject(dataStr);

            WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求-信息解密与校验-checkSign-->" + checkSign + "-reqData-->" + reqData);

            if (!account.equals(partnerid)) {//校验不通过的话，返回失败
                resResult.put("success", resFlag);
                resResult.put("errorCode", errorCode);
                resResult.put("msg", "不合法的请求-签名或者账户名错误");
            }
            else {
                if (sign.equals(checkSign)) {
                    //校验通过的话，可以存储数据
                    resResult = cancelTuNiuOffline(resResult, reqData, trainOfflineIdempotent);
                }
                else {//校验不通过的话，返回失败
                    resResult.put("success", resFlag);
                    resResult.put("errorCode", errorCode);
                    resResult.put("msg", "不合法的请求-签名或者账户名错误");
                }
            }

            WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求结果-resResult" + resResult);

        }

        out.print(resResult.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject cancelTuNiuOffline(JSONObject resResult, JSONObject reqData,
            TrainOfflineIdempotent trainOfflineIdempotent) {
        Boolean resFlag = false;
        Integer errorCode = 231099;//success true用231000，false用231099

        WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求-进入到方法-cancelTuNiuOffline");

        //重复订单校验
        String orderId = reqData.getString("orderId");//途牛订单号

        JSONObject dataResult = new JSONObject();
        dataResult.put("orderId", orderId);
        resResult.put("data", dataResult);

        if (orderId == null || orderId.equals("")) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单不存在，请联系查询传递的订单号");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        WriteLog.write(LOGNAME, r1 + ":途牛线下票取消请求-trainOrderOffline" + trainOrderOffline);

        if (trainOrderOffline == null) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单不存在，请联系查询传递的订单号");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        String logMsg = "";

        logMsg = "途牛发起取消请求";
        //记录日志
        TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), logMsg);

        /**
         * 
        
        *      锁单之前可以取消 - 订单状态 - 
        *  
        *      超时之后可以取消
        *          出票超时 - 订单自动取消 - 无需再次取消 - 且取消没有回调 - 半小时 或者 1个小时
        *          锁单超时 - 之后，一般只会出现拒单的操作 - 调用出票要提示出票的锁单超时，最好操作拒单 - 之后，可以选择取消 - 10分钟或者20分钟
        *  
        *      锁单之中不能取消 - 已锁单的状态
        * 
         * 
         */
        Integer OrderStatus = trainOrderOffline.getOrderStatus();//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】

        Integer lockedStatus = trainOrderOffline.getLockedStatus();//锁单状态 - 0表示未被锁定，1表示被锁定 - 2-锁票中 3-锁票超时

        //可以重复取消订单吗？ - 可以，回调成功 - 不用修改状态了
        if (OrderStatus == 3) {
            //记录日志
            logMsg = "订单已取消，请勿重复提交";
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), logMsg);

            resFlag = true;
            errorCode = 231000;//success true用231000，false用231099
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", logMsg);

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //业务逻辑中不允许有 2-0 2-2 2-3的组合

        if (OrderStatus == 2 && lockedStatus == 1) {//锁单成功，出票成功
            //记录日志
            logMsg = "订单在锁票期间已经出票成功，按照协议，订单无法取消";
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), logMsg);

            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", logMsg);

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //这种情况，已经回调出票了，不能够再次进行取消了
        if (OrderStatus == 2 && lockedStatus == 4) {//判断是否是锁票超时的状态 - 锁票超时，出票成功 - 代售点赔款 - 可以取消成功 - 这个逻辑是产品确认的 - 所以出票确认必须提示锁票超时
            //记录日志
            logMsg = "订单在锁票超时之后已回调出票，按照协议，订单无法取消";
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), logMsg);

            /*resFlag = true;
            errorCode = 231000;*///success true用231000，false用231099
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", logMsg);

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        if ((OrderStatus == 1 && lockedStatus == 1) || (OrderStatus == 1 && lockedStatus == 3)) {
            //锁单成功，等待出票 - 锁单中，等待出票
            //记录日志
            logMsg = "订单在锁票期间无法取消，正在等待出票";
            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), logMsg);

            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", logMsg);

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //1-0 1-2 1-4 - 是可以取消的逻辑

        int res = 0;
        Long id = trainOrderOffline.getId();
        try {
            //修改锁定状态 - 锁单中的单子-1，走不到这里的取消-3 - 锁单失败的单子-2 - 锁单超时-4 - 无需修改锁定状态
            //res = trainOrderOfflineDao.updateTrainOrderOfflinelockedStatusById(id, 0);//0表示未被锁定，1表示被锁定

            //修改订单状态
            res = trainOrderOfflineDao.calcelTrainOrderOfflineOrder(id);//订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理

            TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(trainOrderOffline.getId(), "订单取消成功");
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        if (res > 0) {
            resFlag = true;
            errorCode = 231000;//success true用231000，false用231099
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "取消成功");

            /*if (OrderStatus == 2 && lockedStatus == 4) {//判断是否是锁票超时的状态 - 锁票超时，出票成功 - 代售点赔款 - 可以取消成功 - 这个逻辑是产品确认的 - 所以出票确认必须提示锁票超时
                resResult.put("msg", "订单在锁票超时之后已出票，按照协议，订单取消成功");
            } else {
                resResult.put("msg", "取消成功");
            }*/
        }
        else {
            //抛出异常
        }

        //幂等的设计和实现
        TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        String data = "{\"orderId\":\"test17082940751109\"}";
        //String timestamp = "2017-08-1810:34:29";
        String timestamp = TrainOrderOfflineUtil.getTuNiuTimestamp();
        //String url = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineCancelOrder";
        String url = "http://121.40.226.72:9007/ticket_inter/TrainTuNiuOfflineCancelOrder";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/TrainTuNiuOfflineCancelOrder";

        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTuNiuOfflineCancelOrder";
        //String url = "http://ws.peisong.51kongtie.com/ticket_inter/TrainTuNiuOfflineCancelOrder";

        System.out.println(httpPostJsonUtil.getTuNiuRes(data, timestamp, url));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
