package com.ccservice.inter.job.zrate;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.inter.server.Server;

/**
 * 今日解析XML的工具类
 * 
 * @time 2014年12月1日 上午10:32:11
 * @author chendong
 */
public class AnalyticJinriXml extends DefaultHandler {

    List<Zrate> zrates = new ArrayList<Zrate>();

    Zrate zrate = null;

    String dburl = "";

    int i = 0;

    String valueT = "";

    String deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";

    private StringBuilder sb = new StringBuilder();

    @Override
    public void startDocument() throws SAXException {
        // TODO Auto-generated method stub
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        // TODO Auto-generated method stub
        super.endDocument();
        try {
            System.out.println("JINRI_base_del_count:"
                    + Server.getInstance().getAirService().excuteZrateBySql(this.deleteWhere));
            Server.getInstance().getAirService().createZrateList(this.zrates);
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        // TODO Auto-generated method stub
        super.startElement(uri, localName, qName, attributes);
        if (qName.equals("Table")) {
            this.zrate = new Zrate();
            this.zrate.setAgentid(6L);
            this.zrate.setCreateuser("JINRI");
            this.zrate.setModifyuser("JINRI");
            this.zrate.setZtype("1");
            this.zrate.setIsenable(1);
            this.zrate.setUsertype("1");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        // TODO Auto-generated method stub
        super.endElement(uri, localName, qName);
        try {
            if (qName.equals("RI")) {
                this.zrate.setOutid(this.valueT);
                sb.delete(0, sb.length());
            }
            else if (qName.equals("AI")) {
                this.zrate.setAgentcode(this.valueT);
                sb.delete(0, sb.length());
            }
            else if (qName.equals("SI")) {
                this.zrate.setDepartureport(this.valueT.toUpperCase());
                sb.delete(0, sb.length());
            }
            else if (qName.equals("ES")) {
                this.zrate.setArrivalport(this.valueT.toUpperCase());
                sb.delete(0, sb.length());
            }
            else if (qName.equals("NA")) {
                if (this.valueT.length() > 2) {
                    this.zrate.setWeeknum(this.valueT);
                    this.zrate.setAircompanycode(this.valueT.substring(0, 2));
                }
                else if (this.valueT.length() == 2) {
                    this.zrate.setAircompanycode(this.valueT);
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("YA")) {
                if (this.valueT.length() > 2) {
                    this.zrate.setFlightnumber(this.valueT);
                    this.zrate.setAircompanycode(this.valueT.substring(0, 2));
                }
                else if (this.valueT.length() == 2) {
                    this.zrate.setAircompanycode(this.valueT);
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("C")) {
                this.zrate.setCabincode(this.valueT);
                sb.delete(0, sb.length());
            }
            else if (qName.equals("V")) {
                if (this.valueT.equals("2"))
                    this.zrate.setVoyagetype("3");
                else if (this.valueT.equals("1"))
                    this.zrate.setVoyagetype("2");
                else
                    this.zrate.setVoyagetype("1");
                sb.delete(0, sb.length());
            }
            else if (qName.equals("U")) {
                if (!this.valueT.equals("2")) {
                    this.valueT.equals("1");
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("pd")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("D")) {
                this.zrate.setRatevalue(Float.valueOf(Float.parseFloat(this.valueT)));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RD")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("VD")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("SD")) {
                try {
                    //					value = getrealTime(value, sb.toString());
                    this.zrate.setBegindate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                            this.valueT.substring(0, 10)).getTime()));
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("ED")) {
                try {
                    //					value = getrealTime(value, sb.toString());
                    this.zrate.setEnddate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(
                            valueT.substring(0, 10)).getTime()));
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("WB")) {
                valueT = getrealTime(valueT, sb.toString());
                this.zrate
                        .setWorktime(this.valueT.substring(this.valueT.indexOf("T") + 1, this.valueT.indexOf("+") - 3));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("WE")) {
                //				valueT = getrealTime(value, sb.toString());
                this.zrate.setAfterworktime(this.valueT.substring(this.valueT.indexOf("T") + 1,
                        this.valueT.indexOf("+") - 3));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RM")) {
                this.zrate.setRemark(this.valueT);
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RW")) {
                if (this.valueT.equals("2")) {
                    this.zrate.setZtype("2");
                    this.zrate.setGeneral(Long.valueOf(2L));
                }
                else {
                    this.zrate.setGeneral(Long.valueOf(1L));
                    this.zrate.setZtype("1");
                }
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RT")) {
                if ((this.valueT.equals("0")) || (this.valueT.equals("2")))
                    this.zrate.setTickettype(Integer.valueOf(1));
                else
                    this.zrate.setTickettype(Integer.valueOf(2));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("WP")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("PT")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("XF")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("EC")) {
                this.zrate.setSpeed(this.valueT);
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RefundTimeBegin")) {
                this.zrate.setOnetofivewastetime(this.valueT.substring(this.valueT.indexOf("T") + 1,
                        this.valueT.indexOf("+") - 3));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("RefundTimeEnd")) {
                this.zrate.setOnetofivewastetime(zrate.getOnetofivewastetime() + "-"
                        + this.valueT.substring(this.valueT.indexOf("T") + 1, this.valueT.indexOf("+") - 3));
                sb.delete(0, sb.length());
            }
            else if (qName.equals("LastModifyTime")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("MD")) {
                sb.delete(0, sb.length());
            }
            else if (qName.equals("MDN")) {
                sb.delete(0, sb.length());
            }
        }
        catch (Exception localException1) {
        }
        if (qName.equals("Table")) {
            this.deleteWhere = (this.deleteWhere + " OR (C_OUTID='" + this.zrate.getOutid() + "' AND C_AGENTID=6) ");
            this.zrates.add(this.zrate);
            if (this.zrates.size() % 1000 == 0) {
                try {
                    System.out.println("JINRI_base_del_count:"
                            + Server.getInstance().getAirService().excuteZrateBySql(this.deleteWhere));
                    this.deleteWhere = "DELETE FROM T_ZRATE WHERE 1=2 ";
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (this.zrates.size() == 10000)
                try {
                    Server.getInstance().getAirService().createZrateList(this.zrates);
                    this.zrates.clear();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    public String getrealTime(String value, String valueTT) {
        if (value.length() == 25) {
        }
        else {
            if (valueTT.indexOf(" ") >= 0) {
                String[] valueTs = valueTT.split(" ");
                value = valueTs[(valueTs.length - 1)];
            }
        }
        return value;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        this.sb.append(ch, start, length);
        this.valueT = this.sb.toString().trim().replace("\n", "");
    }

    public static void main(String[] args) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse("E:\\000011-hthuayou-201401081827.xml", new AnalyticJinriXml());
        }
        catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public List<Zrate> getZrates() {
        return zrates;
    }

    public void setZrates(List<Zrate> zrates) {
        this.zrates = zrates;
    }

    public Zrate getZrate() {
        return zrate;
    }

    public void setZrate(Zrate zrate) {
        this.zrate = zrate;
    }

    public String getDburl() {
        return dburl;
    }

    public void setDburl(String dburl) {
        this.dburl = dburl;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public String getValueT() {
        return valueT;
    }

    public void setValueT(String valueT) {
        this.valueT = valueT;
    }

    public String getDeleteWhere() {
        return deleteWhere;
    }

    public void setDeleteWhere(String deleteWhere) {
        this.deleteWhere = deleteWhere;
    }

    public StringBuilder getSb() {
        return sb;
    }

    public void setSb(StringBuilder sb) {
        this.sb = sb;
    }

}
