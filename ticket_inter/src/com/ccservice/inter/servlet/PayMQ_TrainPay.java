package com.ccservice.inter.servlet;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

import com.ccservice.train.mqlistener.TrainorderPayMessageListener;

/**
 * http://192.168.0.5:8161/admin/
 * 
 */
@SuppressWarnings("serial")
public class PayMQ_TrainPay extends HttpServlet {

    private String mqaddress = "";// MQ地址

    private String mqusername = "";// MQ 用户名

    private String isstart = "";// 是否开启

    private int orderpaycount = 15;

    private String alipayurlset;

    @Override
    public void init() throws ServletException {
        this.mqaddress = this.getInitParameter("mqaddress");
        this.mqusername = this.getInitParameter("mqusername");
        this.isstart = this.getInitParameter("isstart");
        this.alipayurlset = this.getInitParameter("alipayurlset");
        this.orderpaycount = Integer.valueOf(this.getInitParameter("orderpaycount"));
        if ("1".equals(isstart)) {
            System.out.println("支付队列:开启,服务器：" + alipayurlset);
            payorderNotice();
        }
    }

    public void payorderNotice() {
        ConnectionFactory cf = new ActiveMQConnectionFactory(mqaddress);
        Connection conn = null;
        Session session = null;
        try {
            conn = cf.createConnection();
            Destination destination = new ActiveMQQueue(mqusername);
            for (int i = 0; i < orderpaycount; i++) {
                session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer consumer = session.createConsumer(destination);
                //consumer.setMessageListener(new TrainorderPayMessageListener(alipayurlset));
                consumer.setMessageListener(new TrainorderPayMessageListener(alipayurlset));
            }
            conn.start();
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
