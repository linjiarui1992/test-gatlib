/**
TongChengOfflineLockOvertime * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tongcheng.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainOrderOfflineRecordDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.HttpUtil;
import com.ccservice.offline.util.SchedulerUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票驳回请求接口 - 由CN平台发起
 * 
 * 

//票价是否需要核验？ - 在驳回中根据金额完成
//获取总票价的Double类型 - 做价格校验

 * 计算总票价 - 总票价之一 - InsurePrice不涉及折半优惠 - 可以放在外面直接计算 - InsurePrice * TicketCount
 * 放到外面算 - 也要判定其存在 的 业务逻辑 - 比较麻烦，不如直接放在内部加和计算
 * 
 * 总票价
 *  成人票的票价 + 儿童票的【半票价】 + 保险的票价 + 【邮寄的票价 - 暂无】
 *  
 *  TotalPrice - [需要订单提交的时候，即进行计算] - 来自车票中的价格的相关计算

//BigDecimal TotalInsurePrice = new BigDecimal(InsurePrice).multiply(new BigDecimal(TicketCount));
//TotalPrice = TotalPrice.add(TotalInsurePrice);

 * 
 * 
 * success true用231000，false用231099
 * 
 * 出票成功的话，需要取消掉驳回请求的自动取消的定时任务 - 只有等待出票状态的单子才会完成自动解锁
 * 
 * 更新快递时效信息
 * 
 * 新增相关的校验的判定
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTongChengOfflineOrderCallbackFailServletCN extends HttpServlet {
    private static final String LOGNAME = "同程线下票驳回请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainOrderOfflineRecordDao trainOrderOfflineRecordDao = new TrainOrderOfflineRecordDao();//记录操作日志，单独封装工具类

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求接口请求信息-reqBody-->" + reqBody);

        JSONObject requestBody = JSONObject.parseObject(reqBody);

        Long orderId = requestBody.getLong("orderId");
        Integer useridi = requestBody.getInteger("userid");

        TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, "发起驳回请求");

        String refundReason = requestBody.getString("refundReason");
        String refundreasonstr = requestBody.getString("refundreasonstr");//0-顺丰 - 1-EMS 2-宅急送

        if (refundreasonstr == null || refundreasonstr.equals("null") || refundreasonstr.equals("")) {
            refundreasonstr = TrainOrderOfflineUtil.getRefundreasonstrByRefundReason(refundReason);
        }

        //在此处就直接入库 - 方便二次出票回调

        //更新失败原因
        try {
            trainOrderOfflineDao.updateTrainOrderOfflineFailById(orderId, useridi, refundReason, refundreasonstr);
        }
        catch (Exception e) {
            ExceptionTCUtil.handleTCException(e);
        }

        JSONObject result = orderCallbackCN(orderId, useridi, refundReason, refundreasonstr);

        if (!result.getBooleanValue("success")) {
            TrainOrderOfflineUtil.CreateBusLogAdmin(orderId, useridi, result.getString("msg"));
        }

        WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求结果-result" + result);

        PrintWriter out = response.getWriter();

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject orderCallbackCN(Long orderIdl, Integer useridi, String refundReason, String refundreasonstr) {
        JSONObject resResult = new JSONObject();

        WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求-进入到方法-orderCallbackCN");

        if (orderIdl == null || orderIdl == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的订单号有误，请排查");
            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderIdl);
        }
        catch (Exception e) {
            return ExceptionTCUtil.handleTCException(e);
        }

        if (trainOrderOffline == null) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单不存在，请排查");
            return resResult;
        }

        if (trainOrderOffline.getOrderStatus() == 3) {
            resResult.put("success", "false");
            resResult.put("msg", "该订单已是拒单或取消的状态");
            return resResult;
        }

        //这些是回调出票失败的单子

        String orderId = trainOrderOffline.getOrderNumberOnline();

        Integer isRapidSend = trainOrderOffline.getIsRapidSend();
        if (isRapidSend == null) {
            isRapidSend = 0;
        }

        //UU跑腿的订单的取消的发起 - 在未锁单之前的拒单无需处理取消问题，只有锁单成功之后的出票失败的单子才会走相关的闪送取消订单的逻辑
        if (isRapidSend == 1 && trainOrderOffline.getLockedStatus() == 1) {
            JSONObject data = new JSONObject();

            Long OrderId = trainOrderOffline.getId();
            data.put("orderId", OrderId);

            TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "锁单成功之后,驳回接口发起闪送单的取消");

            Boolean isSecondRapidSend = TrainOrderOfflineUtil
                    .getIsSecondCancelRapidSend(trainOrderOffline.getOrderNumber());//判断闪送单是否是二次订单 - 

            UUptService uuptService = new UUptService();
            JSONObject responseJson = new JSONObject();

            try {
                if (isSecondRapidSend) {//二次取消
                    TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "发起闪送二次取消请求");
                    data.put("cancelReason", "线下票闪送订单出票失败发起二次取消");
                    responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrderAgain", data, responseJson);
                }
                else {//一次取消
                    TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "发起闪送取消请求");
                    data.put("cancelReason", "线下票闪送订单出票失败发起取消");
                    responseJson = uuptService.operate(LOGNAME, "uupt", r1, "cancelOrder", data, responseJson);
                }
            }
            catch (Exception e) {
                ExceptionTNUtil.handleTNException(e);
            }
            WriteLog.write(LOGNAME, r1 + ":" + LOGNAME + "-取消结果:responseJson-" + responseJson);

            //Boolean responseB = responseJson.getBooleanValue("success");
            //{"message":"成功","result":{"message":"订单编号无效","partnerOrderNumber":"","orderNumber":"TC20171107202322468891","success":false},"success":true}
            //{"message":"成功","result":{"message":"订单号已存在","success":false},"success":true}
            Boolean responseB = false;
            JSONObject responseJsonResult = new JSONObject();
            if (responseJson.getBooleanValue("success")) {
                responseJsonResult = responseJson.getJSONObject("result");
                responseB = responseJsonResult.getBooleanValue("success");
            }

            if (responseB) {
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送取消成功");

                //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                String flagStr = "TongChengOfflineLockRapidSendOvertime";
                try {
                    JobDetail jobDetail = SchedulerUtil.getScheduler()
                            .getJobDetail(flagStr + "Job" + trainOrderOffline.getId(), flagStr + "JobGroup");
                    if (jobDetail != null) {
                        SchedulerUtil.cancelCancelLockTicketScheduler(OrderId, flagStr);

                        TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送单的自动取消定时任务删除成功");
                    }
                }
                catch (Exception e) {
                    ExceptionTNUtil.handleTNException(e);
                }
            }
            else {
                String errorMsg = responseJsonResult.getString("message");
                if (errorMsg == null || "".equals(errorMsg)) {
                    errorMsg = responseJson.getString("message");
                }
                TrainOrderOfflineUtil.CreateBusLogAdmin(OrderId, useridi, "闪送取消失败:" + errorMsg + "-请联系技术处理");
            }
        }

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        JSONObject dataJson = new JSONObject();

        /**
         * 
        SELECT * from TrainTicketOffline where OrderId=" + orderId;
        String sql1 = "UPDATE TrainTicketOffline SET Coach='" + coach + "',SeatNo='" + seatno + "',sealPrice="
                        + priceString + ",ticketNo='" + ticketNo + "',realSeat='" + extString + "' WHERE TrainPid="
                        + TrainPid;
        
                WriteLogTNUtil.write("TN线下票驳回成功结果", "sql1:" + sql1); - 在上一步中已经完成了入库的操作
         * 
        做超时时间校验
        做价格校验
         * 
         */
        /*String ticketNo = "E000000";//出票成功之后回调的票号
        //String seatType = "软卧";//出票成功之后席别
        String seatNo = "14车厢，19座上铺";//14车厢，19座上铺
        
        //快递信息
        String deliveryCompanyName = "顺丰速运";//
        String trackingNumber = "666666666";//快递单号
        Double cost = 15.50;//*/

        dataJson.put("orderNo", orderId);

        //20171110-新增需求，无法满足定制的反馈，在备注字段中反馈具体的定制建议信息
        Integer failReasonId = 10;//其他 - 10
        if ("27".equals(refundReason)) {
            failReasonId = 9;
            dataJson.put("failRemark", refundreasonstr);
        }
        else {
            failReasonId = TrainOrderOfflineUtil.getTCfailReasonIdByRefundreasonstr(refundreasonstr);
            //没有匹配到原因的，failReasonId就填“其他”， 内容放到备注里
            if (failReasonId == 10) {
                dataJson.put("failRemark", refundreasonstr);
            }
        }
        dataJson.put("failReasonId", failReasonId);

        String data = dataJson.toJSONString();

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求反馈信息-dataJson" + data);

        String tongChengOfflineCallbackFailURL = PropertyUtil.getValue("TongChengOfflineCallbackFailURL",
                "Train.GuestAccount.properties");

        String res = "";

        for (int i = 0; i < TrainOrderOfflineUtil.TNRETRY; i++) {
            try {
                res = httpPostJsonUtil.doPost(tongChengOfflineCallbackFailURL, data);//{"success":true}
            }
            catch (Exception e1) {
                String content = "同程线下票驳回请求失败";
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);
                ExceptionTCUtil.handleTCException(e1);
            }

            WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求-res:" + res);

            //pickupNo  string  否   送票到站取票号（只有送票到站才有） - 但是对于我们来说，没什么用处，不需要了 - 信息是同程发的

            if (res != null && res.contains("true")) {//重试请求三次
                //{"isSuccess":true,"msgCode":109100,"msgInfo":"驳回成功","executeTime":"22054"}
                resResult.put("success", "true");

                //更新出票点的相关信息
                //修改订单状态和出票时间 - ChuPiaoTime - OrderStatus
                try {
                    trainOrderOfflineDao.updateTrainOrderOfflineSuccessById(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                //记录日志记录
                TrainOrderOfflineUtil.CreateBusLogAdminNoUserId(orderIdl, refundreasonstr);

                String content = "出票失败，回调成功";
                //TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(orderIdl, useridi, content, 2);//2 - 订单被拒绝
                TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                //更新票的状态
                //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 3, 1);

                //更新响应时间
                try {
                    trainOrderOfflineRecordDao
                            .updateTrainOrderOfflineRecordResponseTimeByOrderIdAndProviderAgentid(orderIdl, useridi);
                }
                catch (Exception e) {
                    return ExceptionTCUtil.handleTCException(e);
                }

                //判断正常的锁单流程中是否存在了有待处理的闪送单的定时取消任务 - 有的话，做清空动作
                String jobFlag = "TongChengOfflineLockOvertime";
                try {
                    JobDetail jobDetail = SchedulerUtil.getScheduler()
                            .getJobDetail(jobFlag + "Job" + trainOrderOffline.getId(), jobFlag + "JobGroup");

                    WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求-jobDetail:" + jobDetail);

                    if (jobDetail != null) {
                        SchedulerUtil.cancelCancelLockTicketScheduler(orderIdl, jobFlag);

                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "同程锁单的超时自动解锁定时任务删除成功");
                    }
                }
                catch (Exception e) {
                    ExceptionTCUtil.handleTCException(e);
                }

                resResult.put("msg", content);

                return resResult;
            }
            else {
                //{"isSuccess":false,"msgCode":109310,"msgInfo":"参数缺失","executeTime":"8"}

                if (res == null || res.equals("")) {
                    if (i == (TrainOrderOfflineUtil.TNRETRY - 1)) {
                        //记录日志
                        TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, "由于网络原因，驳回请求反馈失败，稍后重试或者联系技术处理");

                        resResult.put("success", "false");
                        resResult.put("msg", "由于网络原因，导致同程的驳回请求接口无法访问，请您暂且不要操作，稍后重试或者联系客服处理");//前面已经有了相关的对数据库的修改操作了

                        /**
                         * 
                        String updateorder = "UPDATE TrainOrderOffline SET OrderStatus=3,AgentId=2,operateid=" + agents
                        + ",operatetime='" + sdf.format(date) + "',refusereason=" + refundReason + ",refusereasonstr='"
                        + refundreasonstr + "' where Id =" + orderid;
                         * 
                         */

                        trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单

                        return resResult;
                    }
                    //记录操作记录
                    TrainOrderOfflineUtil.sleep(10 * 1000);
                    continue;
                }

                if (res.contains("参数缺失") || res.contains("订单状态有变化")) {
                    JSONObject resObject = JSONObject.parseObject(res);

                    //记录日志记录
                    String content = resObject.getString("msgInfo");
                    TrainOrderOfflineUtil.CreateBusLogAdmin(orderIdl, useridi, content);

                    //更新票的状态
                    //订单状态 - 1-等待出票 - 2-出票成功 - 3-已拒单【已取消】 - 其他值都是无效状态 - 可操作-是在页面上进行的判断处理
                    //questionDraw = "1"：出票采购问题/邮寄采购问题 questionDraw = "2" //出票成功已邮寄/出票成功待邮寄 - 采购问题订单
                    trainOrderOfflineDao.updateTrainOrderOfflineStatusQuestionById(orderIdl, 1, 1);//出票问题订单

                    resResult.put("success", resObject.getBoolean("isSuccess"));
                    resResult.put("msg", content);

                    //记录请求信息日志
                    WriteLog.write(LOGNAME, r1 + ":同程线下票出票回调请求反馈信息-resResult" + resResult);

                    return resResult;
                }

                resResult.put("success", "false");
                resResult.put("msg", "未知原因");

                if (res != null && !res.equals("")) {
                    JSONObject resJson = JSONObject.parseObject(res);
                    resResult.put("msg", resJson.getString("msgInfo"));
                }
                return resResult;
            }
        }

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":同程线下票驳回请求反馈信息-resResult" + resResult);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        Long OrderId = 1594L;
        Integer useridi = 382;
        String LockWait = "300";

        String result = "";

        //String tuniuOrderCallbackServlet = PropertyUtil.getValue("TuniuOrderCallbackServlet", "Train.GuestAccount.properties");

        String tuniuOrderCallbackServlet = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineOrderCallbackServletCN";

        /*String data = "?orderId="+OrderId+"&userid="+useridi+"&refundReason="+108
                        +"&refundreasonstr="+URLEncoder.encode("同程锁票异步回调在"+LockWait+"s内没有结果反馈，回调出票失败", "UTF-8")+"&orderSuccess="+false;*/

        /**
         * 
        {"refundReason":108,"userid":382,"orderSuccess":false,"refundreasonstr":"閫旂墰閿佺エ寮傛鍥炶皟鍦?00s鍐呮病鏈夌粨鏋滃弽棣堬紝鍥炶皟鍑虹エ澶辫触","orderId":1594}
         * 
         */
        JSONObject resObj = new JSONObject();
        resObj.put("orderId", OrderId);
        resObj.put("userid", useridi);
        resObj.put("refundReason", 108);
        resObj.put("refundreasonstr", "同程锁票异步回调在" + LockWait + "s内没有结果反馈，回调出票失败");
        resObj.put("orderSuccess", false);

        try {
            result = new HttpUtil().doPost(tuniuOrderCallbackServlet, resObj.toJSONString());
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        System.out.println(result);

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

    public void tongChengOrderCallBackFail(String orderidStr) throws Exception {
        String[] orderidArray = orderidStr.split(",");
        for (String orderid : orderidArray) {
            Long orderidL = Long.valueOf(orderid);
            TrainOrderOffline trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineById(orderidL);
            orderCallbackCN(orderidL, 46, String.valueOf(trainOrderOffline.getRefundReason()),
                    trainOrderOffline.getRefundReasonStr());
        }
    }
}
