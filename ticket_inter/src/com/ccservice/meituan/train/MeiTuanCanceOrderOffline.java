package com.ccservice.meituan.train;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.server.Server;
import com.tenpay.util.MD5Util;

public class MeiTuanCanceOrderOffline extends HttpServlet {
	
	public long startTime;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		startTime = System.currentTimeMillis();
		int code = 0;										//返回码
		String msg = "";									//返回描述
		Boolean success = false ;
		BufferedReader bf =null;
		String orderid = "";
		String errorMsg = "";
		//设置请求参数的编码
		try {
			request.setCharacterEncoding("UTF-8");
			ServletInputStream inputStream = request.getInputStream();			
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			bf = new BufferedReader(inputStreamReader);	//获取请求体中的内容
			StringBuffer sb = new StringBuffer("");  
			String temp;  
			while ((temp = bf.readLine()) != null) {  
				sb.append(temp);  
			}
			String jsonStr = sb.toString();
			JSONObject json = JSONObject.parseObject(jsonStr);
			orderid = json.get("orderid").toString();
			String biz_sign = json.get("biz_sign").toString();
			String partnerid = json.get("partnerid").toString();
			WriteLog.write("美团取消订单接口",startTime+"  OrderNumberOnline："+orderid+"，接收到取消订单的请求参数:"+jsonStr);	
			if (orderid!=null && !"".equals(orderid) && biz_sign!=null && !"".equals(biz_sign) && partnerid!=null && !"".equals(partnerid)) {
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String sql = "SELECT lockedStatus FROM TrainOrderOffline with(nolock) WHERE OrderNumberOnline='" + orderid+ "'";
				List list = getSystemServiceOldDB().findMapResultBySql(sql, null);
				String sqlkey = "SELECT keys from TrainOfflineAgentKey where partnerName='meituan'";
				List listkey = getSystemServiceOldDB().findMapResultBySql(sqlkey, null);
				WriteLog.write("美团取消订单接口",startTime+"  OrderNumberOnline："+orderid+"，订单的查询结果"+list.toString()+",获取的key值"+listkey.toString());			
				if (list.size()>0 && listkey.size()>0) {
					Map mapkey = (Map) listkey.get(0);
					String key = mapkey.get("keys").toString();
					String md5=MD5Util.MD5Encode(partnerid+key+orderid+MD5Util.MD5Encode(orderid+partnerid,"UTF-8"),"UTF-8");
					Map maplock = (Map) list.get(0);
					String lockedStatus = maplock.get("lockedStatus").toString();
					if (biz_sign.equals(md5) && "0".equals(lockedStatus)) {//校验通过
						String updatesql="UPDATE TrainOrderOffline SET AgentId=2,orderstatus=3 WHERE OrderNumberOnline='"+orderid+"'";
		            	Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql);
		            	//加操作记录
		            	String sqlorder="SELECT id from TrainOrderOffline where OrderNumberonline='"+orderid+"'";
		        		List listorder = Server.getInstance().getSystemService().findMapResultBySql(sqlorder, null);
		        		if(listorder.size()>0){
		        			Map maporder=(Map)listorder.get(0);
		        			String updatesql1 = "INSERT TrainOrderOfflineRecord(FKTrainOrderOfflineId,ProviderAgentid,DistributionTime,DealResult,RefundReasonStr) "
		    						+ " VALUES("
		    						+ maporder.get("id").toString()
		    						+ ",0,'"
		    						+ sdf.format(new Date())
		    						+ "',15,'" + "--------美团取消订单成功！--------')";
		        			WriteLog.write("美团取消订单接口", startTime+"  updatesql1:"+updatesql1);
		        			Server.getInstance().getSystemService().excuteAdvertisementBySql(updatesql1);
		        		}		            	
		        		code=100;
		            	success = true;
					}else {
						code=108;
		            	msg="错误的业务参数，信息验证未通过";
		            	success = false;
		            	errorMsg="错误的业务参数，信息验证未通过";
						}
					
				}else {
					code=108;
	            	msg="错误的业务参数，根据订单号未查到订单信息";
	            	success = false;
	            	errorMsg="错误的业务参数，根据订单号未查到订单信息";
					}				
			}else {
				code = 108;
				msg	= "错误的业务参数，参数不能为空";
				success = false;
			}	
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			WriteLog.write("美团取消订单接口", startTime+"  servlet异常，MTOrderNumber"+orderid);
		}finally{
			if (bf != null) {
				bf.close();
			}
		}		
		response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        JSONObject responsejson =new JSONObject();
        responsejson.put("code", code);
        responsejson.put("msg", msg);
        responsejson.put("success", success);;
        responsejson.put("errorMsg", errorMsg);
        WriteLog.write("美团取消订单接口", startTime+"  orderNumber="+orderid+";responsejson="+responsejson.toString());
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(responsejson.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }				
	}
	public ISystemService getSystemServiceOldDB() {
        String systemdburlString = PropertyUtil.getValue("offlineservice", "train.properties");
        HessianProxyFactory factory = new HessianProxyFactory();
        try {
            return (ISystemService) factory.create(ISystemService.class,
                    systemdburlString + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
