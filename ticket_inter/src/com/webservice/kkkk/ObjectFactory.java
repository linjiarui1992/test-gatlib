
package com.webservice.kkkk;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://tempuri.org/", "string");
    private final static QName _OrderDetailView_QNAME = new QName("http://tempuri.org/", "OrderDetailView");
    private final static QName _ArrayOfTgqStipulate_QNAME = new QName("http://tempuri.org/", "ArrayOfTgqStipulate");
    private final static QName _ArrayOfPolicyCoordination_QNAME = new QName("http://tempuri.org/", "ArrayOfPolicyCoordination");
    private final static QName _Boolean_QNAME = new QName("http://tempuri.org/", "boolean");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueryOrderList }
     * 
     */
    public QueryOrderList createQueryOrderList() {
        return new QueryOrderList();
    }

    /**
     * Create an instance of {@link GetPayMode }
     * 
     */
    public GetPayMode createGetPayMode() {
        return new GetPayMode();
    }

    /**
     * Create an instance of {@link SetCancelPNRResponse }
     * 
     */
    public SetCancelPNRResponse createSetCancelPNRResponse() {
        return new SetCancelPNRResponse();
    }

    /**
     * Create an instance of {@link CreateOrderResponse }
     * 
     */
    public CreateOrderResponse createCreateOrderResponse() {
        return new CreateOrderResponse();
    }

    /**
     * Create an instance of {@link ModifyCertification }
     * 
     */
    public ModifyCertification createModifyCertification() {
        return new ModifyCertification();
    }

    /**
     * Create an instance of {@link ApplyFormView }
     * 
     */
    public ApplyFormView createApplyFormView() {
        return new ApplyFormView();
    }

    /**
     * Create an instance of {@link RefreshCache }
     * 
     */
    public RefreshCache createRefreshCache() {
        return new RefreshCache();
    }

    /**
     * Create an instance of {@link QueryInsuranceList }
     * 
     */
    public QueryInsuranceList createQueryInsuranceList() {
        return new QueryInsuranceList();
    }

    /**
     * Create an instance of {@link CreateOrderByPNR }
     * 
     */
    public CreateOrderByPNR createCreateOrderByPNR() {
        return new CreateOrderByPNR();
    }

    /**
     * Create an instance of {@link TimeGetReplyPolicy }
     * 
     */
    public TimeGetReplyPolicy createTimeGetReplyPolicy() {
        return new TimeGetReplyPolicy();
    }

    /**
     * Create an instance of {@link GetInvalidPolicyResponse }
     * 
     */
    public GetInvalidPolicyResponse createGetInvalidPolicyResponse() {
        return new GetInvalidPolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicysRebateList }
     * 
     */
    public GetPolicysRebateList createGetPolicysRebateList() {
        return new GetPolicysRebateList();
    }

    /**
     * Create an instance of {@link CreateOrderByPNRInfoResponse }
     * 
     */
    public CreateOrderByPNRInfoResponse createCreateOrderByPNRInfoResponse() {
        return new CreateOrderByPNRInfoResponse();
    }

    /**
     * Create an instance of {@link QueryOriginalDataFlight }
     * 
     */
    public QueryOriginalDataFlight createQueryOriginalDataFlight() {
        return new QueryOriginalDataFlight();
    }

    /**
     * Create an instance of {@link QueryItineraryList }
     * 
     */
    public QueryItineraryList createQueryItineraryList() {
        return new QueryItineraryList();
    }

    /**
     * Create an instance of {@link QueryInsuranceDetailResponse }
     * 
     */
    public QueryInsuranceDetailResponse createQueryInsuranceDetailResponse() {
        return new QueryInsuranceDetailResponse();
    }

    /**
     * Create an instance of {@link QuerySpecialApplyFormDetailListResponse }
     * 
     */
    public QuerySpecialApplyFormDetailListResponse createQuerySpecialApplyFormDetailListResponse() {
        return new QuerySpecialApplyFormDetailListResponse();
    }

    /**
     * Create an instance of {@link QueryTeamApplyFormDetailList }
     * 
     */
    public QueryTeamApplyFormDetailList createQueryTeamApplyFormDetailList() {
        return new QueryTeamApplyFormDetailList();
    }

    /**
     * Create an instance of {@link QueryStopover }
     * 
     */
    public QueryStopover createQueryStopover() {
        return new QueryStopover();
    }

    /**
     * Create an instance of {@link GetSuspendPolicyResponse }
     * 
     */
    public GetSuspendPolicyResponse createGetSuspendPolicyResponse() {
        return new GetSuspendPolicyResponse();
    }

    /**
     * Create an instance of {@link GetPolicysRebateListResponse }
     * 
     */
    public GetPolicysRebateListResponse createGetPolicysRebateListResponse() {
        return new GetPolicysRebateListResponse();
    }

    /**
     * Create an instance of {@link BookClass }
     * 
     */
    public BookClass createBookClass() {
        return new BookClass();
    }

    /**
     * Create an instance of {@link IsCanPay }
     * 
     */
    public IsCanPay createIsCanPay() {
        return new IsCanPay();
    }

    /**
     * Create an instance of {@link QueryTeamApplyFormDetailListResponse }
     * 
     */
    public QueryTeamApplyFormDetailListResponse createQueryTeamApplyFormDetailListResponse() {
        return new QueryTeamApplyFormDetailListResponse();
    }

    /**
     * Create an instance of {@link ApplyHandleInfo }
     * 
     */
    public ApplyHandleInfo createApplyHandleInfo() {
        return new ApplyHandleInfo();
    }

    /**
     * Create an instance of {@link QuerySpecialclassPriceResponse }
     * 
     */
    public QuerySpecialclassPriceResponse createQuerySpecialclassPriceResponse() {
        return new QuerySpecialclassPriceResponse();
    }

    /**
     * Create an instance of {@link QueryPolicyCoordinationResponse }
     * 
     */
    public QueryPolicyCoordinationResponse createQueryPolicyCoordinationResponse() {
        return new QueryPolicyCoordinationResponse();
    }

    /**
     * Create an instance of {@link ApplyUpgradesResponse }
     * 
     */
    public ApplyUpgradesResponse createApplyUpgradesResponse() {
        return new ApplyUpgradesResponse();
    }

    /**
     * Create an instance of {@link OrderInfo }
     * 
     */
    public OrderInfo createOrderInfo() {
        return new OrderInfo();
    }

    /**
     * Create an instance of {@link PolicyCoordination }
     * 
     */
    public PolicyCoordination createPolicyCoordination() {
        return new PolicyCoordination();
    }

    /**
     * Create an instance of {@link QueryFlightAndMatchPolicyResponse }
     * 
     */
    public QueryFlightAndMatchPolicyResponse createQueryFlightAndMatchPolicyResponse() {
        return new QueryFlightAndMatchPolicyResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPassengerView }
     * 
     */
    public ArrayOfPassengerView createArrayOfPassengerView() {
        return new ArrayOfPassengerView();
    }

    /**
     * Create an instance of {@link QueryItineraryListResponse }
     * 
     */
    public QueryItineraryListResponse createQueryItineraryListResponse() {
        return new QueryItineraryListResponse();
    }

    /**
     * Create an instance of {@link SpecialPolicy }
     * 
     */
    public SpecialPolicy createSpecialPolicy() {
        return new SpecialPolicy();
    }

    /**
     * Create an instance of {@link KeyValuePairOfDecimalDecimal }
     * 
     */
    public KeyValuePairOfDecimalDecimal createKeyValuePairOfDecimalDecimal() {
        return new KeyValuePairOfDecimalDecimal();
    }

    /**
     * Create an instance of {@link ApplyPostponeTicketResponse }
     * 
     */
    public ApplyPostponeTicketResponse createApplyPostponeTicketResponse() {
        return new ApplyPostponeTicketResponse();
    }

    /**
     * Create an instance of {@link QueryTgqStipulateXMLResponse }
     * 
     */
    public QueryTgqStipulateXMLResponse createQueryTgqStipulateXMLResponse() {
        return new QueryTgqStipulateXMLResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSeat }
     * 
     */
    public ArrayOfSeat createArrayOfSeat() {
        return new ArrayOfSeat();
    }

    /**
     * Create an instance of {@link GetPNRInfoAnalyticResponse }
     * 
     */
    public GetPNRInfoAnalyticResponse createGetPNRInfoAnalyticResponse() {
        return new GetPNRInfoAnalyticResponse();
    }

    /**
     * Create an instance of {@link QueryOrderListResponse }
     * 
     */
    public QueryOrderListResponse createQueryOrderListResponse() {
        return new QueryOrderListResponse();
    }

    /**
     * Create an instance of {@link QueryFlightResponse }
     * 
     */
    public QueryFlightResponse createQueryFlightResponse() {
        return new QueryFlightResponse();
    }

    /**
     * Create an instance of {@link QueryInsuranceDetail }
     * 
     */
    public QueryInsuranceDetail createQueryInsuranceDetail() {
        return new QueryInsuranceDetail();
    }

    /**
     * Create an instance of {@link QueryTgqStipulateResponse }
     * 
     */
    public QueryTgqStipulateResponse createQueryTgqStipulateResponse() {
        return new QueryTgqStipulateResponse();
    }

    /**
     * Create an instance of {@link ModifyOSI }
     * 
     */
    public ModifyOSI createModifyOSI() {
        return new ModifyOSI();
    }

    /**
     * Create an instance of {@link MatchPolicyXML }
     * 
     */
    public MatchPolicyXML createMatchPolicyXML() {
        return new MatchPolicyXML();
    }

    /**
     * Create an instance of {@link QueryOrderLogResponse }
     * 
     */
    public QueryOrderLogResponse createQueryOrderLogResponse() {
        return new QueryOrderLogResponse();
    }

    /**
     * Create an instance of {@link VoyageView }
     * 
     */
    public VoyageView createVoyageView() {
        return new VoyageView();
    }

    /**
     * Create an instance of {@link TimeGetPolicyResponse }
     * 
     */
    public TimeGetPolicyResponse createTimeGetPolicyResponse() {
        return new TimeGetPolicyResponse();
    }

    /**
     * Create an instance of {@link GetSuspendPolicy }
     * 
     */
    public GetSuspendPolicy createGetSuspendPolicy() {
        return new GetSuspendPolicy();
    }

    /**
     * Create an instance of {@link PolicySupplierInfo }
     * 
     */
    public PolicySupplierInfo createPolicySupplierInfo() {
        return new PolicySupplierInfo();
    }

    /**
     * Create an instance of {@link Seat }
     * 
     */
    public Seat createSeat() {
        return new Seat();
    }

    /**
     * Create an instance of {@link CoordinationInfo }
     * 
     */
    public CoordinationInfo createCoordinationInfo() {
        return new CoordinationInfo();
    }

    /**
     * Create an instance of {@link NewTimeGetReplyPolicy }
     * 
     */
    public NewTimeGetReplyPolicy createNewTimeGetReplyPolicy() {
        return new NewTimeGetReplyPolicy();
    }

    /**
     * Create an instance of {@link GetPolicyRebate }
     * 
     */
    public GetPolicyRebate createGetPolicyRebate() {
        return new GetPolicyRebate();
    }

    /**
     * Create an instance of {@link NewIsCanPay }
     * 
     */
    public NewIsCanPay createNewIsCanPay() {
        return new NewIsCanPay();
    }

    /**
     * Create an instance of {@link ArrayOfApplyHandleInfo }
     * 
     */
    public ArrayOfApplyHandleInfo createArrayOfApplyHandleInfo() {
        return new ArrayOfApplyHandleInfo();
    }

    /**
     * Create an instance of {@link OrderAttachedInfo }
     * 
     */
    public OrderAttachedInfo createOrderAttachedInfo() {
        return new OrderAttachedInfo();
    }

    /**
     * Create an instance of {@link TimeGetPolicy }
     * 
     */
    public TimeGetPolicy createTimeGetPolicy() {
        return new TimeGetPolicy();
    }

    /**
     * Create an instance of {@link QueryTeamApplyFormListResponse }
     * 
     */
    public QueryTeamApplyFormListResponse createQueryTeamApplyFormListResponse() {
        return new QueryTeamApplyFormListResponse();
    }

    /**
     * Create an instance of {@link NewIsCanPayResponse }
     * 
     */
    public NewIsCanPayResponse createNewIsCanPayResponse() {
        return new NewIsCanPayResponse();
    }

    /**
     * Create an instance of {@link NewTimeGetReplyPolicyResponse }
     * 
     */
    public NewTimeGetReplyPolicyResponse createNewTimeGetReplyPolicyResponse() {
        return new NewTimeGetReplyPolicyResponse();
    }

    /**
     * Create an instance of {@link QueryOriginalDataFlightResponse }
     * 
     */
    public QueryOriginalDataFlightResponse createQueryOriginalDataFlightResponse() {
        return new QueryOriginalDataFlightResponse();
    }

    /**
     * Create an instance of {@link MatchPolicy }
     * 
     */
    public MatchPolicy createMatchPolicy() {
        return new MatchPolicy();
    }

    /**
     * Create an instance of {@link IsCanPayResponse }
     * 
     */
    public IsCanPayResponse createIsCanPayResponse() {
        return new IsCanPayResponse();
    }

    /**
     * Create an instance of {@link ApplyItemView }
     * 
     */
    public ApplyItemView createApplyItemView() {
        return new ApplyItemView();
    }

    /**
     * Create an instance of {@link NewTimeGetPolicy }
     * 
     */
    public NewTimeGetPolicy createNewTimeGetPolicy() {
        return new NewTimeGetPolicy();
    }

    /**
     * Create an instance of {@link CreateOrderByPNRResponse }
     * 
     */
    public CreateOrderByPNRResponse createCreateOrderByPNRResponse() {
        return new CreateOrderByPNRResponse();
    }

    /**
     * Create an instance of {@link GetPNRInfoAnalytic }
     * 
     */
    public GetPNRInfoAnalytic createGetPNRInfoAnalytic() {
        return new GetPNRInfoAnalytic();
    }

    /**
     * Create an instance of {@link QuerySpecialApplyFormList }
     * 
     */
    public QuerySpecialApplyFormList createQuerySpecialApplyFormList() {
        return new QuerySpecialApplyFormList();
    }

    /**
     * Create an instance of {@link TgqStipulate }
     * 
     */
    public TgqStipulate createTgqStipulate() {
        return new TgqStipulate();
    }

    /**
     * Create an instance of {@link ArrayOfApplyFormView }
     * 
     */
    public ArrayOfApplyFormView createArrayOfApplyFormView() {
        return new ArrayOfApplyFormView();
    }

    /**
     * Create an instance of {@link QueryTgqStipulateXML }
     * 
     */
    public QueryTgqStipulateXML createQueryTgqStipulateXML() {
        return new QueryTgqStipulateXML();
    }

    /**
     * Create an instance of {@link GetPolicyRebateResponse }
     * 
     */
    public GetPolicyRebateResponse createGetPolicyRebateResponse() {
        return new GetPolicyRebateResponse();
    }

    /**
     * Create an instance of {@link CreateOrder }
     * 
     */
    public CreateOrder createCreateOrder() {
        return new CreateOrder();
    }

    /**
     * Create an instance of {@link TimeGetReplyPolicyResponse }
     * 
     */
    public TimeGetReplyPolicyResponse createTimeGetReplyPolicyResponse() {
        return new TimeGetReplyPolicyResponse();
    }

    /**
     * Create an instance of {@link QuerySpecialclassPrice }
     * 
     */
    public QuerySpecialclassPrice createQuerySpecialclassPrice() {
        return new QuerySpecialclassPrice();
    }

    /**
     * Create an instance of {@link MatchPolicyResponse }
     * 
     */
    public MatchPolicyResponse createMatchPolicyResponse() {
        return new MatchPolicyResponse();
    }

    /**
     * Create an instance of {@link QueryInsuranceListResponse }
     * 
     */
    public QueryInsuranceListResponse createQueryInsuranceListResponse() {
        return new QueryInsuranceListResponse();
    }

    /**
     * Create an instance of {@link ApplyUpgrades }
     * 
     */
    public ApplyUpgrades createApplyUpgrades() {
        return new ApplyUpgrades();
    }

    /**
     * Create an instance of {@link PolicyExcludeAirline }
     * 
     */
    public PolicyExcludeAirline createPolicyExcludeAirline() {
        return new PolicyExcludeAirline();
    }

    /**
     * Create an instance of {@link PayOrder }
     * 
     */
    public PayOrder createPayOrder() {
        return new PayOrder();
    }

    /**
     * Create an instance of {@link NewGetInvalidPolicy }
     * 
     */
    public NewGetInvalidPolicy createNewGetInvalidPolicy() {
        return new NewGetInvalidPolicy();
    }

    /**
     * Create an instance of {@link ArrayOfPolicyExcludeAirline }
     * 
     */
    public ArrayOfPolicyExcludeAirline createArrayOfPolicyExcludeAirline() {
        return new ArrayOfPolicyExcludeAirline();
    }

    /**
     * Create an instance of {@link ArrayOfSpecialPolicy }
     * 
     */
    public ArrayOfSpecialPolicy createArrayOfSpecialPolicy() {
        return new ArrayOfSpecialPolicy();
    }

    /**
     * Create an instance of {@link QueryProviderInfoResponse }
     * 
     */
    public QueryProviderInfoResponse createQueryProviderInfoResponse() {
        return new QueryProviderInfoResponse();
    }

    /**
     * Create an instance of {@link QueryTeamApplyFormList }
     * 
     */
    public QueryTeamApplyFormList createQueryTeamApplyFormList() {
        return new QueryTeamApplyFormList();
    }

    /**
     * Create an instance of {@link QueryProviderInfo }
     * 
     */
    public QueryProviderInfo createQueryProviderInfo() {
        return new QueryProviderInfo();
    }

    /**
     * Create an instance of {@link MatchPolicyXMLResponse }
     * 
     */
    public MatchPolicyXMLResponse createMatchPolicyXMLResponse() {
        return new MatchPolicyXMLResponse();
    }

    /**
     * Create an instance of {@link ContactInfo }
     * 
     */
    public ContactInfo createContactInfo() {
        return new ContactInfo();
    }

    /**
     * Create an instance of {@link QuerySpecialApplyFormDetailList }
     * 
     */
    public QuerySpecialApplyFormDetailList createQuerySpecialApplyFormDetailList() {
        return new QuerySpecialApplyFormDetailList();
    }

    /**
     * Create an instance of {@link QueryFlight }
     * 
     */
    public QueryFlight createQueryFlight() {
        return new QueryFlight();
    }

    /**
     * Create an instance of {@link ArrayOfOrderAtionalView }
     * 
     */
    public ArrayOfOrderAtionalView createArrayOfOrderAtionalView() {
        return new ArrayOfOrderAtionalView();
    }

    /**
     * Create an instance of {@link ArrayOfFlight }
     * 
     */
    public ArrayOfFlight createArrayOfFlight() {
        return new ArrayOfFlight();
    }

    /**
     * Create an instance of {@link BookClassResponse }
     * 
     */
    public BookClassResponse createBookClassResponse() {
        return new BookClassResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCoordinationInfo }
     * 
     */
    public ArrayOfCoordinationInfo createArrayOfCoordinationInfo() {
        return new ArrayOfCoordinationInfo();
    }

    /**
     * Create an instance of {@link ArrayOfApplyItemView }
     * 
     */
    public ArrayOfApplyItemView createArrayOfApplyItemView() {
        return new ArrayOfApplyItemView();
    }

    /**
     * Create an instance of {@link RefreshCacheResponse }
     * 
     */
    public RefreshCacheResponse createRefreshCacheResponse() {
        return new RefreshCacheResponse();
    }

    /**
     * Create an instance of {@link CreateOrderByPNRInfo }
     * 
     */
    public CreateOrderByPNRInfo createCreateOrderByPNRInfo() {
        return new CreateOrderByPNRInfo();
    }

    /**
     * Create an instance of {@link ApplyPostponeTicket }
     * 
     */
    public ApplyPostponeTicket createApplyPostponeTicket() {
        return new ApplyPostponeTicket();
    }

    /**
     * Create an instance of {@link TicketView }
     * 
     */
    public TicketView createTicketView() {
        return new TicketView();
    }

    /**
     * Create an instance of {@link QueryItineraryDetail }
     * 
     */
    public QueryItineraryDetail createQueryItineraryDetail() {
        return new QueryItineraryDetail();
    }

    /**
     * Create an instance of {@link QueryCachedFlight }
     * 
     */
    public QueryCachedFlight createQueryCachedFlight() {
        return new QueryCachedFlight();
    }

    /**
     * Create an instance of {@link QueryItineraryApplyListResponse }
     * 
     */
    public QueryItineraryApplyListResponse createQueryItineraryApplyListResponse() {
        return new QueryItineraryApplyListResponse();
    }

    /**
     * Create an instance of {@link Flight }
     * 
     */
    public Flight createFlight() {
        return new Flight();
    }

    /**
     * Create an instance of {@link ApplyRefundTicket }
     * 
     */
    public ApplyRefundTicket createApplyRefundTicket() {
        return new ApplyRefundTicket();
    }

    /**
     * Create an instance of {@link QueryStopoverResponse }
     * 
     */
    public QueryStopoverResponse createQueryStopoverResponse() {
        return new QueryStopoverResponse();
    }

    /**
     * Create an instance of {@link CreateBackPriceApplyformResponse }
     * 
     */
    public CreateBackPriceApplyformResponse createCreateBackPriceApplyformResponse() {
        return new CreateBackPriceApplyformResponse();
    }

    /**
     * Create an instance of {@link GetPayModeResponse }
     * 
     */
    public GetPayModeResponse createGetPayModeResponse() {
        return new GetPayModeResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTgqStipulate }
     * 
     */
    public ArrayOfTgqStipulate createArrayOfTgqStipulate() {
        return new ArrayOfTgqStipulate();
    }

    /**
     * Create an instance of {@link ArrayOfTicketView }
     * 
     */
    public ArrayOfTicketView createArrayOfTicketView() {
        return new ArrayOfTicketView();
    }

    /**
     * Create an instance of {@link CreateBackPriceApplyform }
     * 
     */
    public CreateBackPriceApplyform createCreateBackPriceApplyform() {
        return new CreateBackPriceApplyform();
    }

    /**
     * Create an instance of {@link PassengerView }
     * 
     */
    public PassengerView createPassengerView() {
        return new PassengerView();
    }

    /**
     * Create an instance of {@link QuerySpecialApplyFormListResponse }
     * 
     */
    public QuerySpecialApplyFormListResponse createQuerySpecialApplyFormListResponse() {
        return new QuerySpecialApplyFormListResponse();
    }

    /**
     * Create an instance of {@link QueryInsureOrderList }
     * 
     */
    public QueryInsureOrderList createQueryInsureOrderList() {
        return new QueryInsureOrderList();
    }

    /**
     * Create an instance of {@link QueryOrder }
     * 
     */
    public QueryOrder createQueryOrder() {
        return new QueryOrder();
    }

    /**
     * Create an instance of {@link QueryItineraryDetailResponse }
     * 
     */
    public QueryItineraryDetailResponse createQueryItineraryDetailResponse() {
        return new QueryItineraryDetailResponse();
    }

    /**
     * Create an instance of {@link QueryOrderLog }
     * 
     */
    public QueryOrderLog createQueryOrderLog() {
        return new QueryOrderLog();
    }

    /**
     * Create an instance of {@link ArrayOfKeyValuePairOfDecimalDecimal }
     * 
     */
    public ArrayOfKeyValuePairOfDecimalDecimal createArrayOfKeyValuePairOfDecimalDecimal() {
        return new ArrayOfKeyValuePairOfDecimalDecimal();
    }

    /**
     * Create an instance of {@link ModifyOSIResponse }
     * 
     */
    public ModifyOSIResponse createModifyOSIResponse() {
        return new ModifyOSIResponse();
    }

    /**
     * Create an instance of {@link PayOrderResponse }
     * 
     */
    public PayOrderResponse createPayOrderResponse() {
        return new PayOrderResponse();
    }

    /**
     * Create an instance of {@link QueryItineraryApplyList }
     * 
     */
    public QueryItineraryApplyList createQueryItineraryApplyList() {
        return new QueryItineraryApplyList();
    }

    /**
     * Create an instance of {@link OrderDetailView }
     * 
     */
    public OrderDetailView createOrderDetailView() {
        return new OrderDetailView();
    }

    /**
     * Create an instance of {@link OrderAtionalView }
     * 
     */
    public OrderAtionalView createOrderAtionalView() {
        return new OrderAtionalView();
    }

    /**
     * Create an instance of {@link ApplyRefundTicketResponse }
     * 
     */
    public ApplyRefundTicketResponse createApplyRefundTicketResponse() {
        return new ApplyRefundTicketResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPolicyCoordination }
     * 
     */
    public ArrayOfPolicyCoordination createArrayOfPolicyCoordination() {
        return new ArrayOfPolicyCoordination();
    }

    /**
     * Create an instance of {@link QueryOrderResponse }
     * 
     */
    public QueryOrderResponse createQueryOrderResponse() {
        return new QueryOrderResponse();
    }

    /**
     * Create an instance of {@link NewTimeGetPolicyResponse }
     * 
     */
    public NewTimeGetPolicyResponse createNewTimeGetPolicyResponse() {
        return new NewTimeGetPolicyResponse();
    }

    /**
     * Create an instance of {@link ModifyCertificationResponse }
     * 
     */
    public ModifyCertificationResponse createModifyCertificationResponse() {
        return new ModifyCertificationResponse();
    }

    /**
     * Create an instance of {@link GetInvalidPolicy }
     * 
     */
    public GetInvalidPolicy createGetInvalidPolicy() {
        return new GetInvalidPolicy();
    }

    /**
     * Create an instance of {@link PurchaserInfo }
     * 
     */
    public PurchaserInfo createPurchaserInfo() {
        return new PurchaserInfo();
    }

    /**
     * Create an instance of {@link ArrayOfVoyageView }
     * 
     */
    public ArrayOfVoyageView createArrayOfVoyageView() {
        return new ArrayOfVoyageView();
    }

    /**
     * Create an instance of {@link NewGetInvalidPolicyResponse }
     * 
     */
    public NewGetInvalidPolicyResponse createNewGetInvalidPolicyResponse() {
        return new NewGetInvalidPolicyResponse();
    }

    /**
     * Create an instance of {@link QueryPolicyCoordination }
     * 
     */
    public QueryPolicyCoordination createQueryPolicyCoordination() {
        return new QueryPolicyCoordination();
    }

    /**
     * Create an instance of {@link InsuranceInfo }
     * 
     */
    public InsuranceInfo createInsuranceInfo() {
        return new InsuranceInfo();
    }

    /**
     * Create an instance of {@link GetPnrAndPriceResponse }
     * 
     */
    public GetPnrAndPriceResponse createGetPnrAndPriceResponse() {
        return new GetPnrAndPriceResponse();
    }

    /**
     * Create an instance of {@link GetPnrAndPrice }
     * 
     */
    public GetPnrAndPrice createGetPnrAndPrice() {
        return new GetPnrAndPrice();
    }

    /**
     * Create an instance of {@link QueryInsureOrderListResponse }
     * 
     */
    public QueryInsureOrderListResponse createQueryInsureOrderListResponse() {
        return new QueryInsureOrderListResponse();
    }

    /**
     * Create an instance of {@link QueryCachedFlightResponse }
     * 
     */
    public QueryCachedFlightResponse createQueryCachedFlightResponse() {
        return new QueryCachedFlightResponse();
    }

    /**
     * Create an instance of {@link QueryFlightAndMatchPolicy }
     * 
     */
    public QueryFlightAndMatchPolicy createQueryFlightAndMatchPolicy() {
        return new QueryFlightAndMatchPolicy();
    }

    /**
     * Create an instance of {@link QueryOrderXML }
     * 
     */
    public QueryOrderXML createQueryOrderXML() {
        return new QueryOrderXML();
    }

    /**
     * Create an instance of {@link SetCancelPNR }
     * 
     */
    public SetCancelPNR createSetCancelPNR() {
        return new SetCancelPNR();
    }

    /**
     * Create an instance of {@link QueryOrderXMLResponse }
     * 
     */
    public QueryOrderXMLResponse createQueryOrderXMLResponse() {
        return new QueryOrderXMLResponse();
    }

    /**
     * Create an instance of {@link QueryTgqStipulate }
     * 
     */
    public QueryTgqStipulate createQueryTgqStipulate() {
        return new QueryTgqStipulate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderDetailView }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "OrderDetailView")
    public JAXBElement<OrderDetailView> createOrderDetailView(OrderDetailView value) {
        return new JAXBElement<OrderDetailView>(_OrderDetailView_QNAME, OrderDetailView.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTgqStipulate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTgqStipulate")
    public JAXBElement<ArrayOfTgqStipulate> createArrayOfTgqStipulate(ArrayOfTgqStipulate value) {
        return new JAXBElement<ArrayOfTgqStipulate>(_ArrayOfTgqStipulate_QNAME, ArrayOfTgqStipulate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPolicyCoordination }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfPolicyCoordination")
    public JAXBElement<ArrayOfPolicyCoordination> createArrayOfPolicyCoordination(ArrayOfPolicyCoordination value) {
        return new JAXBElement<ArrayOfPolicyCoordination>(_ArrayOfPolicyCoordination_QNAME, ArrayOfPolicyCoordination.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

}
