package com.ccservice.test;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.db.DBHelper2;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.util.db.DBHelper;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.JobIndexAgain_RepUtil;

public class CallBackAccountAndPassengerDemoThread extends Thread {

    String LoginName;

    int CustomeruserId;

    long r1;

    public CallBackAccountAndPassengerDemoThread(String LoginName, int CustomeruserId, long r1) {
        this.LoginName = LoginName;
        this.CustomeruserId = CustomeruserId;
        this.r1 = r1;
    }

    @Override
    public void run() {
        try {
            checkCustomeruserEnble(CustomeruserId);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询线上帐号
     * 
     * @param loginNameId  帐号id
     * @throws Exception
     * @time 2016年1月7日 下午9:10:41
     * @author Administrator
     */
    public void checkCustomeruserEnble(int loginNameId) throws Exception {

        String sql = "select C_LOGINNAME,C_ISENABLE from T_CUSTOMERUSER WITH(NOLOCK) where ID=" + loginNameId;
        com.ccervice.util.db.DataTable result = DBHelper2.GetDataTable(sql);
        List<com.ccervice.util.db.DataRow> dataRows = result.GetRow();
        if (dataRows.size() > 0) {
            com.ccervice.util.db.DataRow dataRow = dataRows.get(0);
            String loginName = dataRow.GetColumnString("C_LOGINNAME");
            int isEnable = dataRow.GetColumnInt("C_ISENABLE");
            System.out.println(r1 + ":DB---->" + loginName + "==" + isEnable);
            callBacktongchengMethod(loginNameId, loginName, isEnable);
        }
        else {
            Customeruser customeruser = new Customeruser();
            customeruser.setId(loginNameId);
            customeruser.setIsenable(51);
            CallBackPassengerUtil.callBackTongcheng(customeruser, new ArrayList<Trainpassenger>(), 2);
            savaDBMethod(loginNameId, "无", 1, -404);
        }
    }

    /**
     * 判断帐号状态,回调接口
     * 
     * @param LoginnameId 帐号id
     * @param name  帐号名
     * @param isEnable 帐号状态
     * @throws Exception
     * @time 2015年12月22日 上午11:49:01
     * @author Administrator
     */
    public void callBacktongchengMethod(int LoginnameId, String name, int isEnable) throws Exception {
        int flag = 0;
        if (isEnable == 1 || isEnable == 3 || isEnable == 4 || isEnable == 10) {
            System.out.println(r1 + ":=====>loginName:" + this.LoginName);
            String repUrl = JobIndexAgain_RepUtil.getInstance().getRepUrl(false);
            String cookie = JobTrainUtil.getCookie(repUrl, toTrim(this.LoginName), "asd123456");
            String result = JobTrainUtil.initQueryUserInfo(cookie).toString();
            System.out.println(r1 + ":=====>result:" + result);
            JSONObject json = JSONObject.parseObject(result);
            if (result.indexOf("未登录") > -1) {
                flag = 0;
                savaDBMethod(LoginnameId, name, flag, isEnable);
            }
            else if (result.indexOf("未通过") > -1) {
                flag = 1;
                Customeruser customeruser = new Customeruser();
                customeruser.setId(LoginnameId);
                customeruser.setIsenable(isEnable);
                CallBackPassengerUtil.callBackTongcheng(customeruser, new ArrayList<Trainpassenger>(), 2);
                savaDBMethod(LoginnameId, name, flag, isEnable);

                System.out.println(r1 + ":@@@@@@@@@@@@@:" + LoginnameId + ":" + name + ":" + flag + ":" + isEnable);
            }
            else if (result.indexOf("其他") > -1) {
                WriteLog.write("CallBackAccountAndPassengerDemoByDb_callBacktongchengMethod", "repUrl:" + repUrl
                        + ":loginName:" + this.LoginName);
                flag = 3;
                savaDBMethod(LoginnameId, name, flag, isEnable);
            }
            else if (("已通过".equals(json.containsKey("shoujihaoma_heyanstatus") ? json.get("shoujihaoma_heyanstatus")
                    : "")) && ("已通过".equals(json.containsKey("heyanzhuangtai") ? json.get("heyanzhuangtai") : ""))) {
                flag = 2;
                savaDBMethod(LoginnameId, name, flag, isEnable);
            }
            else {
                flag = 0;
                savaDBMethod(LoginnameId, name, flag, isEnable);
            }

        }
        else {
            Customeruser customeruser = new Customeruser();
            customeruser.setId(LoginnameId);
            customeruser.setIsenable(isEnable);
            List<Trainpassenger> trainPassengers = new ArrayList<Trainpassenger>();
            Trainpassenger p1 = new Trainpassenger();
            p1.setName(name);
            //                p1.setIdnumber(IdCard);
            p1.setIdtype(1);
            Trainticket trainticket = new Trainticket();
            trainticket.setTickettype(1);
            List<Trainticket> traintickets = new ArrayList<Trainticket>();
            traintickets.add(trainticket);
            p1.setTraintickets(traintickets);
            //放入乘客
            //                trainPassengers.add(p1);
            int operationtypeid = 2;//操作类型 ID 1:新增，2:删除，3:修改
            CallBackPassengerUtil.callBackTongcheng(customeruser, trainPassengers, operationtypeid);
            flag = 1;

            //            savaDBMethod(LoginnameId, name, flag, isEnable);
        }
    }

    /**
     * 将数据DB.5进行修改
     * 
     * @param LoginnameId
     * @param name
     * @param flag 0:没处理  1:同步给同程   2:帐号可用,不用同步给同程 
     * @param count
     * @time 2015年12月22日 上午11:51:38
     * @author Administrator
     */
    public void savaDBMethod(int LoginnameId, String name, int flag, int isEnable) throws Exception {
        String sql2 = "UPDATE TongchengCustomeruser SET  LoginName='" + name + "',IsEnable=" + isEnable + ",falg="
                + flag + " WHERE CustomeruserId=" + LoginnameId;
        WriteLog.write("savaDBMethod", sql2);
        Boolean istrue = com.ccervice.util.db.DBHelper.executeSql(sql2);
        System.out.println(this.r1 + ":---->" + istrue);
    }

    public String toTrim(String loginName) {
        if (ElongHotelInterfaceUtil.StringIsNull(loginName)) {
            return "";
        }
        else {
            return loginName.trim();
        }
    }
}
