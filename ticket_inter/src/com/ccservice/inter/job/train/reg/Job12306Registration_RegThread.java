package com.ccservice.inter.job.train.reg;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.ccervice.util.RegistrationLong;
//import com.ccervice.util.RegistrationLong;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.Util.JobTrainUtil;
import com.ccservice.inter.job.train.account.Account12306Server;
import com.ccservice.inter.server.Server;
import com.ccservice.mobileCode.IMobileCode;
import com.ccservice.mobileCode.util.MobileCodeBeanUtil;
import com.ccservice.t12306.method.reg.CheckThisRepIsenable;
import com.ccservice.t12306.method.reg.Reg12306New;

/**
 * tc000153953@mlzg99.com 自动注册12306账号 开始自动注册12306账号 账号，密码，E-MAIL
 * 
 * @time 2014年12月14日 下午3:57:21
 * @author chendong
 * @author yinshubin
 */

public class Job12306Registration_RegThread extends Thread {
    String regInit = "https://kyfw.12306.cn/otn/regist/init";

    public static void main(String[] args) {
        /** x */
        // System.out.println(JobTrainUtil.check12306LoginName("liushuuhye"));
        //// Job12306Registration_RegThread job12306registration_regthread = new
        // Job12306Registration_RegThread("", "", "",
        // "");//线程注册
        // job12306registration_regthread.getPassenger();

    }

    /**
     * 
     * @param args
     * @time 2015年9月25日 下午4:00:01
     * @author chendong
     */

    String mobileCodeType;

    private int idx;

    // 一共使用多少个rep
    private int repTotalSize;

    private String rep_url;

    private String email_ip;

    private String email_host;// 形如 hangtian123.com.cn

    private List<String> reg_urls;

    // rep错误总次数
    private int codeTotalErrorCount = 0;

    String useProxy;

    String proxyHost;

    String proxyPort;

    String mobileNo;

    IMobileCode mobilecode;

    public Job12306Registration_RegThread(List<String> reg_urls, String useProxy, String proxyHost, String proxyPort) {
        super();
        this.reg_urls = reg_urls;
        this.repTotalSize = reg_urls.size();
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    public Job12306Registration_RegThread(String reg_rep_url, String useProxy, String proxyHost, String proxyPort) {
        super();
        this.rep_url = reg_rep_url;
        this.useProxy = useProxy;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.mobileCodeType = PropertyUtil.getValue("Job12306Registration_RegThread_mobileCodeType",
                "train.reg.properties");
        this.mobilecode = MobileCodeBeanUtil.initMobileCodeType(mobileCodeType);// 初始化手机验证码
                                                                                // 平台
    }

    public Job12306Registration_RegThread(int idx, String rep_url) {
        this.rep_url = rep_url;
        this.idx = idx;
    }

    @Override
    public void run() {
        Account12306Server.regdangqianCount++;
        // 注册哪个库的账号
        String Job12306Registration_serviceurl = PropertyUtil.getValue("Job12306Registration_serviceurl",
                "train.reg.properties");
        HessianProxyFactory factory = new HessianProxyFactory();
        try {
            isystemservice_serviceurl = (ISystemService) factory.create(ISystemService.class,
                    Job12306Registration_serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String regResult = "-10";
        try {
            regResult = execute_new();// 开始注册
            //            RegistrationLong registrationLong = new RegistrationLong();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        new RegistrationLong(PropertyUtil.getValue("vpsNo", "train.reg.properties")).ErrorPrint(regResult, "");
        System.out.println("========>>>最最终结果:" + regResult);
        WriteLog.write("12306Reg/", "Job12306Registration_RegThread", regResult);
        Account12306Server.regdangqianCount--;
        System.out.println("========>>>regdangqianCount:" + Account12306Server.regdangqianCount);
    }

    /**
     * 从rep获取到一个可用的手机号
     * 
     * @return
     * @time 2015年10月9日 下午12:25:23
     * @author chen
     */
    public String getMobileNo() {
        String mobileNo = mobilecode.GetMobilenum("", "", "");
        // String mobileNo = "17309651791";
        System.out.println("获取手机号结果:" + mobileNo + ":" + this.rep_url + "?" + "datatypeflag=-20&mobileCodeType="
                + this.mobileCodeType);
        return mobileNo;
    }

    ISystemService isystemservice_serviceurl;

    String loginname;//当前正在注册的账号的用户名

    /**
     * 新的注册账号的方法
     * 
     * @time 2015年5月7日 上午11:44:13
     * @author chendong
     * @param r1
     * @param email_host
     * @param email_ip
     * @param url
     */
    public String execute_new() {
        String reg_result = "-1";
        Long l2 = System.currentTimeMillis();
        Customeruser customeruser = getOneCustomeruserOninableIs12(isystemservice_serviceurl);
        System.out.println("获取(" + (System.currentTimeMillis() - l2) + ")到一个账号---id>" + customeruser.getId() + "->"
                + customeruser.getLoginname());
        if (customeruser.getId() > 0) {
            boolean isexist = JobTrainUtil.check12306LoginName(customeruser.getLoginname());
            if (isexist) {
                System.out.println("用户名已存在不往下走了,loginname;" + customeruser.getLoginname());
                String sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=14 WHERE ID=" + customeruser.getId();
                int recount = 0;
                try {
                    recount = isystemservice_serviceurl.excuteGiftBySql(sqlupdatecus);
                }
                catch (Exception e) {
                }
                WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread",
                        customeruser.getLoginname() + ":1:失败后修改数据库:用户名已经被注册:" + recount + ":" + sqlupdatecus);
                reg_result = "用户名已经被注册";
            }
            else {
                this.loginname = customeruser.getLoginname();
                String mobileNo_t = getMobileNo();
                if (mobileNo_t.length() == 11) {
                    this.mobileNo = mobileNo_t;
                    // 去注册
                    String result = toregistration(customeruser, 0, isystemservice_serviceurl);
                    WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", customeruser.getId()
                            + ":" + customeruser.getLoginname() + ":" + "----->结束了:耗时:"
                            + (System.currentTimeMillis() - l2) + ":" + result);
                    reg_result = result;
                }
                else {
                    // 获取手机号失败
                    GetMobileNo_t(customeruser.getLoginname(), customeruser.getId());
                    reg_result = "获取手机号失败";
                }
            }
        }
        else {
            System.out.println("获取注册账号失败===");
            reg_result = "获取注册账号失败";
        }
        return reg_result;
    }

    String randCode;

    String cookieString;

    /**
     * 
     * 
     * @param customeruser
     *            准备注册的人
     * @param email_ip
     * @param r1
     * @param url
     * @param jinlai_count
     * @param email_host
     * @param cookie
     *            12306Cookie，用于取新乘客重试注册
     * @time 2015年5月7日 下午12:26:18
     * @author chendong
     * @param isystemservice
     */
    private String toregistration(Customeruser customeruser, int jinlai_count, ISystemService isystemservice) {
        String proxyHost_proxyPortKey = proxyHost + ":" + proxyPort;
        String result = "";
        String isagain = "0";
        if (customeruser.getId() > 0) {
            // 多次
            int tempInt = 0;
            String mobileNo = null;
            tempInt++;
            result = registration(isagain, email_ip, customeruser, 0, isystemservice, tempInt);
            String[] results = result.split("[|]");
            if (results.length > 1 && results[1].length() == 11) {
                mobileNo = results[1];
            }
            WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", customeruser.getLoginname()
                    + ":registration:" + tempInt + ":次结果:" + result + ":this.repTotalSize:" + this.repTotalSize + ":"
                    + PropertyUtil.getValue("Job12306Registration_isEnableMailPath", "train.reg.properties")
                    + ":RegWlfmURLMapLastUseTime.size():" + Server.RegWlfmURLMapLastUseTime.size() + "==" + rep_url);
            WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", customeruser.getLoginname()
                    + ":registration总结果:" + result + "=codeTotalErrorCount=" + codeTotalErrorCount);
            //            System.out.println(this.loginname + ":rep结果:" + result);
            // 失败
            if (result.contains("短信验证后身份信息重复") || result.contains("您填写的身份信息重复") || result.contains("用户名已经被注册")) {
                if (result.contains("短信验证后身份信息重复") || result.contains("您填写的身份信息重复")) {
                    if (result.contains("短信验证后身份信息重复")) {
                        //                        this.randCode = getRandCode(result);
                        //                        this.cookieString = getCookieString(result);
                    }
                    customeruser.setMobile(mobileNo);// 重新把手机号拿到这里注册
                    // 拿一个新乘客
                    Trainpassenger trainpassenger = new Trainpassenger();
                    trainpassenger = getPassenger();
                    // 没有乘客可以注册了
                    if (trainpassenger.getId() == 0) {
                        System.out.println(System.currentTimeMillis() + ":没有乘客可以注册了");
                        //                        RegistrationLong.ErrorPrint("Job12306Registration_RegThread", "没有乘客可以注册了",
                        //                                new Timestamp(System.currentTimeMillis()), getAdsl());
                        result = "没有乘客可以注册了";
                    }
                    else {
                        for (int i = 0; i < 20; i++) {
                            if ((trainpassenger.getIdnumber().toLowerCase().contains("x"))) {
                                System.out.println("------->>>>>>>乘客证件号有x:"
                                        + trainpassenger.getIdnumber().toLowerCase());
                                trainpassenger = getPassenger();
                                continue;
                            }
                            else {
                                //                                System.out.println("------->>>>>>>乘客证件号没有x:" + trainpassenger.getIdnumber().toLowerCase());
                                break;
                            }
                        }
                        customeruser.setTicketnum((int) trainpassenger.getId());
                        String idnumber = trainpassenger.getIdnumber();// 证件号
                        String name = trainpassenger.getName();// 姓名
                        customeruser.setMembermobile(idnumber);
                        customeruser.setMembername(name);
                    }
                    // String[] results = result.split("#####");
                    // System.out.println(results[0]);
                    result = toregistration(customeruser, jinlai_count, isystemservice);// 重新去注册
                }
                else if (result.contains("用户名已经被注册")) {
                    String sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=14 WHERE ID=" + customeruser.getId();
                    int recount = 0;
                    try {
                        recount = isystemservice_serviceurl.excuteGiftBySql(sqlupdatecus);
                    }
                    catch (Exception e) {
                    }
                    //                    RegistrationLong.ErrorPrint("Job12306Registration_regFail",
                    //                            customeruser.getLoginname() + ":失败后修改数据库:用户名已经被注册:" + recount + ":" + sqlupdatecus,
                    //                            new Timestamp(System.currentTimeMillis()), getAdsl());
                    result = "用户名已经被注册";
                    WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread",
                            customeruser.getLoginname() + ":失败后修改数据库:用户名已经被注册:" + recount + ":" + sqlupdatecus);
                }
            }
            else if ("codeerror".equals(result)) {// 打码失败或者被封
                // 日志
                // 如果总rep数量的一半大于当前被封 暂停使用的rep数量 才进入循环注册的过程
                // 并且打码失败次数小于4次才循环换rep
                codeTotalErrorCount++;
                // Server.RegWlfmURLMapLastUseTime.put(rep_url,
                // System.currentTimeMillis());
                if ("1".equals(this.useProxy)) {// 如果用代理
                    // 把网络繁忙的次数加1
                    // Server.RegWlfmURLMapCount.put(this.proxyHost,
                    // Server.RegWlfmURLMapCount.get(this.proxyHost) + 1);
                    Server.ProxyIpList.remove(proxyHost_proxyPortKey);
                    WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "codeerror:"
                            + proxyHost_proxyPortKey);
                }
                else {// 如果不使用代理检测rep是否被封
                      // String resultString = JobTrainUtil
                      // .rep0Methods(/*this.rep_url,*/"", useProxy,
                      // proxyHost, proxyPort);
                    String resultString = CheckThisRepIsenable.checkThisRepIsenable(useProxy, proxyHost, proxyPort);
                    WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", useProxy + "="
                            + proxyHost + "=" + proxyPort + this.rep_url + "=>" + resultString);
                    if (resultString.contains("铁路客户服务中心")) {

                    }
                    else if (resultString.contains("网络繁忙")) {// 如果被封了这个不使用代理的rep就等2小时在用
                        Server.RegWlfmURLMapLastUseTime.put(this.rep_url, System.currentTimeMillis()
                                + (1000 * 60 * 60 * 2));
                    }
                }
            }
            else if (result.contains("您可继续使用获取验证码短信功能") || result.contains("您获取验证码短信次数过多")
                    || result.contains("系统忙，请稍后再试") || result.contains("网络繁忙")) {// 打码失败或者被封
                // codeTotalErrorCount++;
                Server.RegWlfmURLMapLastUseTime.put(rep_url, System.currentTimeMillis());
                if ("0".equals(this.useProxy)) {
                    JobTrainUtil.reConnectAdsl();// 断开adsl重新连接
                }
                else {// 如果用代理
                    Server.RegWlfmURLMapLastUseTime.put(this.proxyHost, System.currentTimeMillis());
                    // 把网络繁忙的次数加1
                    Server.RegWlfmURLMapCount.put(this.proxyHost, Server.RegWlfmURLMapCount.get(this.proxyHost) + 1);
                    Server.ProxyIpList.remove(proxyHost_proxyPortKey);
                    WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "wlfm:"
                            + proxyHost_proxyPortKey);

                }
                // 如果总rep数量的一半大于当前被封 暂停使用的rep数量 才进入循环注册的过程
                // 并且打码失败次数小于4次才循环换rep
            }
            // 注册或打码失败，还原状态
            else if ("注册失败".equals(result) || "系统忙，请稍后重试".equals(result)) {
                String sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=12 WHERE ID=" + customeruser.getId();
                int recount = 0;
                try {
                    recount = Server.getServer().getSystemService().excuteGiftBySql(sqlupdatecus);
                }
                catch (Exception e) {
                }
                WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread",
                        customeruser.getLoginname() + ":失败(" + result + ")后修改数据库:" + recount + ":" + sqlupdatecus);
            }
            else if (result.contains(email_host)) {
                if (result.contains("您填写的身份信息重复")) {// 失败
                }
                else if ("telnet登录失败".equals(result)) {// 失败
                    System.out.println("telnet登录失败");
                }
                else if ("打码校验失败".equals(result)) {// 失败
                    System.out.println("打码校验失败");
                }
                else {
                    Server.ProxyIpList.put(proxyHost_proxyPortKey, proxyHost_proxyPortKey);
                    result = "true";
                }
            }
        }
        else {
            System.out.println("没有可以注册的账号了");
            result = "没有可以注册的账号了";
        }
        return result;
    }

    /**
     * 
     * @return
     * @time 2015年10月15日 下午3:40:16
     * @author chendong
     */
    @SuppressWarnings("unused")
    private String getCookieString(String result) {
        String randCode = "";
        String[] results = result.split("[|]");
        if (results.length > 2) {
            randCode = results[2];
        }
        return randCode;
    }

    /**
     * 
     * @param result
     * @return
     * @time 2015年10月15日 下午1:16:11
     * @author chendong
     */
    public String getRandCode(String result) {
        String randCode = "";
        String[] results = result.split("[|]");
        if (results.length > 1) {
            randCode = results[0];
            String[] randCodes = randCode.split(",");
            if (randCodes.length > 1) {
                randCode = randCodes[1];
            }
        }
        return randCode;
    }

    List<Trainpassenger> trainpassengers = new ArrayList<Trainpassenger>();

    /**
     * 从存储过程中获取可以注册的人
     * 
     * 
     * @return
     * @time 2015年4月24日 下午9:24:46
     * @author chendong
     */
    private Trainpassenger getPassenger() {
        // //常旅客哪个库的常旅客
        String Job12306Registration_serviceurl = PropertyUtil.getValue("Job12306Registration_passenger_serviceurl",
                "train.reg.properties");
        HessianProxyFactory factory = new HessianProxyFactory();
        ISystemService isystemservice_passenger = null;
        try {
            isystemservice_passenger = (ISystemService) factory.create(ISystemService.class,
                    Job12306Registration_serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Trainpassenger trainpassenger = new Trainpassenger();
        if (trainpassengers.size() == 0) {
            List list = isystemservice_passenger.findMapResultByProcedure("sp_TrainPassenger_GetOnePassenger");
            if (list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Trainpassenger trainpass = new Trainpassenger();
                    Map map = (Map) list.get(i);
                    long id = Long.parseLong(objectisnull(map.get("id")));
                    String name = objectisnull(map.get("name"));
                    String idnumber = objectisnull(map.get("idnumber"));
                    trainpass.setId(id);
                    trainpass.setName(name.trim());
                    trainpass.setIdnumber(idnumber.trim());
                    trainpassengers.add(trainpass);
                }
            }
        }
        if (trainpassengers != null && trainpassengers.size() > 0) {
            trainpassenger = trainpassengers.remove(0);
        }
        return trainpassenger;
    }

    /**
     * 注册12306账号
     * 
     * @param logname
     *            12306登录名 最后一次用的
     * @param logpassword
     *            12306密码
     * @param id_no
     *            身份证号码
     * @param name
     *            姓名
     * @param email
     *            邮箱地址
     * @param isagain
     * @time 2014年10月20日 下午2:21:15
     * @author yinshubin
     * @param isystemservice
     * @param tempInt
     */
    public String registration(String isagain, String email_ip, Customeruser customeruser, int count,
            ISystemService isystemservice, int tempInt) {
        String email = customeruser.getMemberfax();
        String name = customeruser.getMembername().trim();
        String logname = customeruser.getLoginname();
        String logpassword = customeruser.getLogpassword();
        String id_no = customeruser.getMembermobile().trim();
        String mobile = customeruser.getMobile() == null ? "" : customeruser.getMobile();// 手机号
        String result = "-1";
        String mailname = email.split("@")[0];

        try {
            /*
             * RegistrationLong.ErrorPrint("Job12306Registration_RegThread",
             * logname + ";开始访问12306自动注册账号:" + "(" + name + ":" + id_no + "):" +
             * logname + ":" + email + ":" + rep_url, new
             * Timestamp(System.currentTimeMillis()), getAdsl());
             */
        }
        catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", logname + ";开始访问12306自动注册账号:"
                + "(" + name + ":" + id_no + "):" + logname + ":" + email + ":" + rep_url);
        if (tempInt == 1) {
            mobile = this.mobileNo;
        }
        /** 789 */
        result = sendregtorep(logname, logpassword, id_no, name, email, mobile, isagain);
        System.out.println(logname + ":rep结果:" + result);
        String[] results = result.split("[|]");
        if ("true".equals(results[0])) {
            String sinfo1 = "准备去激活:" + logname + ":" + result + ":" + email + ":" + email_ip + ":" + logname + ":"
                    + mailname;
            sinfo1 = "准备去激活:" + logname;
            System.out.println(sinfo1);
            WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "jihuo:" + sinfo1);
            //            try {
            // new Job12306Registration_JihuoThread(result, email, email_ip,
            // logname, customeruser, mailname,
            // isystemservice, useProxy, proxyHost, proxyPort).start();
            // int r1 = new Random().nextInt(100000);
            // 老激活
            // new
            // Job12306Registration_JihuoThread(isystemservice).tojihuo(result,
            // email, email_ip, logname, r1,
            // customeruser, mailname);
            // 如果注册成功把把哪台vps注册的编码发送到监控服务器便于监控哪个vps出问题了
            //                String vpsReg12306CountUrl = PropertyUtil.getValue("vpsReg12306CountUrl", "train.reg.properties");
            //                SendPostandGet.submitGet(vpsReg12306CountUrl);
            //            }
            //            catch (Exception e) {
            //            }
            // 新注册
            updateDb(results, customeruser, isystemservice);
        }
        else if (result.contains("您可继续使用获取验证码短信功能") || result.contains("您获取验证码短信次数过多") || result.contains("系统忙，请稍后再试")
                || result.contains("网络繁忙")) {
            JobTrainUtil.reConnectAdsl();// 断开adsl重新连接
            // #TODO 释放账号
            // this.mobileNo
        }
        else if (result.startsWith("no_data")) {
            this.mobilecode.AddIgnoreList("", this.mobileNo, "", "");

        }
        // 成功了就去激活
        else {
            /** x */
            WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", logname + ":err:result:"
                    + result);
        }
        return result;
    }

    /**
     * 
     * @time 2015年9月11日 下午7:54:31
     * @author chendong
     * @param results
     */
    private void updateDb(String[] results, Customeruser customeruser, ISystemService isystemservice) {
        String ticketnumsql = "update T_CUSTOMERUSER set C_ISENABLE=21,C_MEMBERFAX='" + customeruser.getMemberfax()
                + "',C_CREATETIME = '" + ElongHotelInterfaceUtil.getCurrentTime() + "',C_CARDTYPE='"
                + this.mobileCodeType + "',C_MEMBERMOBILE='" + customeruser.getMembermobile() + "',c_membername='"
                + customeruser.getMembername() + "'";
        if (results.length > 1) {
            String mobile = results[1];
            ticketnumsql += ",C_MOBILE='" + mobile + "' ";
        }
        ticketnumsql += " where ID=" + customeruser.getId();
        //// sqlupdatecus = "UPDATE T_CUSTOMERUSER SET
        //// C_ISENABLE=18,C_MEMBERFAX='" + email + "',C_LOGINNAME='"
        // + newlogname + "',C_TICKETNUM=" + customeruser.getTicketnum() +
        //// ",C_MEMBERMOBILE='"
        // + customeruser.getMembermobile() + "',c_membername='" +
        //// customeruser.getMembername()
        // + "',C_LINKOTHER='" + jihuourl + "', C_CREATETIME = '" +
        //// ElongHotelInterfaceUtil.getCurrentTime()
        // + "',C_MOBILE='" + customeruser.getMobile() + "' WHERE ID=" +
        //// customeruser.getId();//激活成功后把这个customeruser改成可用
        int count1 = 0;

        for (int i = 1; i < 11; i++) {
            try {
                count1 = isystemservice.excuteAdvertisementBySql(ticketnumsql);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
            if (count1 != 0) {
                System.out.println("第" + i + "次循环插入===>" + "结果" + ":" + count1);
                break;
            }
            else {
                try {
                    Thread.sleep(3000L);
                }
                catch (Exception e) {
                }
            }
        }
        WriteLog.write("Job12306Registration_RegThread_Over", "Job12306Registration_RegThread",
                "Job12306Registration_RegThread_Over" + String.valueOf(count1));
        System.out.println(count1 + ":" + ticketnumsql);
        WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread",
                "Job12306Registration_RegThread_updateDb:loginname:" + customeruser.getLoginname() + ":count1:"
                        + count1 + ":ticketnumsql:" + ticketnumsql);
    }

    /**
     * 
     * 发送注册账号的请求
     * 
     * @param logname
     *            12306登陆名
     * @param logpassword
     *            12306密码
     * @param id_no
     *            证件号
     * @param name_code
     *            姓名
     * @param email
     *            email
     * @param isagain
     *            是否第二次注册
     * @param url
     *            访问的rep地址
     * @return
     * @time 2015年4月23日 下午12:22:55
     * @author chendong
     */
    private String sendregtorep(String logname, String logpassword, String id_no, String name, String email,
            String mobile, String isagain) {
        // String name_code = getencode(name.trim());
        String name_code = name.trim();
        // String mobileCodeType=null;
        String userProxyInfo = null;
        // 开始时间
        long start = System.currentTimeMillis();
        // 请求参数
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("loginame", logname);
        jsonobject.put("logpassword", logpassword);
        jsonobject.put("name", name_code);
        jsonobject.put("idno", id_no);
        jsonobject.put("email", email);
        jsonobject.put("codenum", 0);
        String damarule = "1,1,1,1,1";
        try {
            damarule = PropertyUtil.getValue("reg_damarule", "train.reg.properties");
        }
        catch (Exception e) {
        }
        jsonobject.put("damarule", damarule);
        jsonobject.put("mobile", mobile);
        jsonobject.put("cookie", this.cookieString);
        jsonobject.put("cookieString", this.cookieString);
        jsonobject.put("randCode", this.randCode);
        // 发送注册请求
        StringBuffer param = new StringBuffer(jsonobject.toJSONString());
        // mobileCodeType =
        // PropertyUtil.getValue("Job12306Registration_RegThread_mobileCodeType","train.reg.properties");//1:优码,2:淘码,3:y码,4:壹码,5:爱码,6:卓码,7:要码

        if ("1".equals(this.useProxy)) {
            userProxyInfo = "&useProxy=1&proxyHost=" + this.proxyHost + "&proxyPort=" + this.proxyPort;
        }
        Reg12306New reg12306new = new Reg12306New(mobileCodeType);
        String resultt = reg12306new.mainReg12306New(jsonobject.toString());
        WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "resultt:0:" + resultt);
        WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "resultt:1:" + logname + ":url:"
                + this.rep_url + ":resultt:" + resultt + "reg12306new:" + mobileCodeType);
        /*
         * //REP int submitRepCount = 0; int submitRepMaxCount =
         * 3;//向rep请求的最大重试次数 try { submitRepMaxCount =
         * Integer.parseInt(PropertyUtil.getValue("submitRepMaxCount",
         * "train.reg.properties")); } catch (Exception e) { } String result =
         * ""; do { result = param.toString() + "" + this.rep_url;
         * //SendPostandGet.submitPost(this.rep_url, param.toString(),
         * "UTF-8").toString();
         * RegistrationLong.ErrorPrint("Job12306Registration_RegThread", logname
         * + ":" + submitRepCount + ":次:result:" + result, new
         * Timestamp(System.currentTimeMillis()), getAdsl());
         * System.out.println(logname + ":" + submitRepCount + ":次:result:" +
         * result); submitRepCount++; } while (("".equals(result) ||
         * result.contains("该手机号码已被注册") || result.contains("没有可用号码")) ||
         * result.contains("获取手机号失败") && submitRepCount < submitRepMaxCount);
         * //日志 RegistrationLong.ErrorPrint("Job12306Registration_RegThread",
         * logname + ":" + submitRepCount + ":次:result:" + result, new
         * Timestamp(System.currentTimeMillis()), getAdsl());
         * WriteLog.write("12306Reg/"+this.loginname,"Job12306Registration_RegThread", logname + ":" +
         * submitRepCount + ":次:result:" + result); //日志
         * RegistrationLong.ErrorPrint("Job12306Registration_RegThread", logname
         * + ":rep:耗时:" + (System.currentTimeMillis() - start), new
         * Timestamp(System.currentTimeMillis()), getAdsl());
         * WriteLog.write("12306Reg/"+this.loginname,"Job12306Registration_RegThread", logname + ":rep:耗时:"
         * + (System.currentTimeMillis() - start) + ":注册结果:" + result + ":" +
         * rep_url + ":logname|email:" + logname + "|" + email + ":" + id_no);
         */
        // 返回
        return resultt;
    }

    private String getencode(String name) {
        try {
            name = URLEncoder.encode(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return name;
    }

    /**
     * 获取到一个等待注册的账号去注册
     * 
     * @return
     * @time 2015年5月7日 下午12:35:05
     * @author chendong
     * @param isystemservice_serviceurl
     * @param isystemservice
     */
    private Customeruser getOneCustomeruserOninableIs12(ISystemService isystemservice_serviceurl) {
        Customeruser customeruser = new Customeruser();
        // 获取到一个等待注册的customeruser信息
        List list = isystemservice_serviceurl
                .findMapResultByProcedure("sp_CustomerUser_Job12306Registration_getOneCustomeruserOninableIs12");
        if (list.size() > 0) {
            Map map = (Map) list.get(0);
            long id = Long.parseLong(objectisnull(map.get("ID")));
            String C_LOGINNAME = objectisnull(map.get("C_LOGINNAME"));
            C_LOGINNAME = checkLoginNameAndUpdate(C_LOGINNAME, id);
            String C_LOGPASSWORD = objectisnull(map.get("C_LOGPASSWORD"));
            String C_MEMBERNAME = objectisnull(map.get("C_MEMBERNAME"));
            String C_MEMBERMOBILE = objectisnull(map.get("C_MEMBERMOBILE"));
            // String C_MOBILE = objectisnull(map.get("C_MOBILE"));
            String C_MEMBERFAX = objectisnull(map.get("C_MEMBERFAX"));
            // C_MOBILE = getRandomTel();
            customeruser.setId(id);
            customeruser.setLoginname(C_LOGINNAME.trim());
            customeruser.setLogpassword(C_LOGPASSWORD.trim());
            customeruser.setMembername(C_MEMBERNAME.trim());
            customeruser.setMembermobile(C_MEMBERMOBILE.trim());
            // customeruser.setMobile(C_MOBILE.trim());
            String memberfax = getMemberfax(C_MEMBERFAX.trim());
            customeruser.setMemberfax(memberfax.trim());// 邮箱地址
        }

        return customeruser;
    }

    private String checkLoginNameAndUpdate(String loginname, long id) {
        String loginuser = "a";
        int login_c = loginuser.charAt(0);
        if (login_c >= 48 && login_c <= 57) {
            System.out.println(login_c);
            loginname = "a" + loginname;
            try {
                String sql = "update T_CUSTOMERUSER set C_LOGINNAME='" + loginname + "' where id=" + id;
                int count = isystemservice_serviceurl.excuteGiftBySql(sql);
                WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread", "id:" + id + ":count:"
                        + count + ":sql:" + sql);
            }
            catch (Exception e) {
            }

        }
        return loginname;
    }

    /**
     * 获取一个转换后的域名
     * 
     * @param c_MEMBERFAX
     * @return
     * @time 2015年8月20日 下午9:25:33
     * @author chendong
     */
    private String getMemberfax(String c_memberfax) {
        // String isEnableMailPath = "tuuyoo.com";//可用的域名
        String isEnableMailPath = PropertyUtil
                .getValue("Job12306Registration_isEnableMailPath", "train.reg.properties");// 可用的域名
        this.email_host = isEnableMailPath;
        this.email_ip = PropertyUtil.getValue(isEnableMailPath, "train.reg.properties");
        String startStringemail = c_memberfax.split("@")[0];// email的开始字母、、例如:wsad00000038
        startStringemail = startStringemail + "@" + isEnableMailPath;
        return startStringemail;
    }

    /**
     * 返回手机号码
     */
    private static String[] telFirst = "134,135,136,137,138,139,150,151,152,157,158,159,130,131,132,155,156,133,153"
            .split(",");

    public static int getNum(int start, int end) {
        return (int) (Math.random() * (end - start + 1) + start);
    }

    private static String getRandomTel() {
        int index = getNum(0, telFirst.length - 1);
        String first = telFirst[index];
        String second = String.valueOf(getNum(1, 888) + 10000).substring(1);
        String thrid = String.valueOf(getNum(1, 9100) + 10000).substring(1);
        return first + second + thrid;
    }

    /**
     * 如果为空返回 "0"
     * 
     * @param object
     * @return
     * @time 2015年4月15日 下午1:57:54
     * @author chendong
     */
    public String objectisnull(Object object) {
        return object == null ? "0" : object.toString();
    }

    /**
     * 
     * @return
     * @time 2015年8月26日 下午12:48:03
     * @author chendong
     */
    @SuppressWarnings("unused")
    private void getReg_repUrl() {
        // String repUrlString = "-1";
        // 最多获取10次rep就不获取了
        for (int i = 0; i < this.repTotalSize; i++) {
            // 获取一次就+1
            Server.regIdx++;
            this.rep_url = reg_urls.get(Server.regIdx % this.repTotalSize);
            if (Server.RegWlfmURLMapLastUseTime.get(this.rep_url) == null) {
                break;
            }
            else {
                Long timeChaLong = System.currentTimeMillis() - Server.RegWlfmURLMapLastUseTime.get(this.rep_url);
                String Job12306Registration_repWaitTime = PropertyUtil.getValue("Job12306Registration_repWaitTime",
                        "train.reg.properties");
                Long Job12306Registration_repWaitTime_long = Long.parseLong(Job12306Registration_repWaitTime);
                // 如果时间大于300秒（五分钟）从map里移出返回rep
                if (timeChaLong > Job12306Registration_repWaitTime_long) {
                    Server.RegWlfmURLMapLastUseTime.remove(this.rep_url);
                    break;
                }
                else {// 否则就获取到下一个rep
                    continue;
                }
            }
        }
        // return repUrlString;
    }

    // 暂时没用
    //    public static void type(String nameOut) {
    //        RegistrationLong.Types(nameOut);
    //    }

    /**
     * 获取手机号失败还原状态
     * 
     */
    public void GetMobileNo_t(String Loginname, Long Id) {
        String sqlupdatecus = "UPDATE T_CUSTOMERUSER SET C_ISENABLE=12 WHERE ID=" + Id;
        int recount = 0;
        try {
            recount = isystemservice_serviceurl.excuteGiftBySql(sqlupdatecus);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(recount + "获取手机号失败还原状态:" + recount + ":" + sqlupdatecus);
        WriteLog.write("12306Reg/" + this.loginname, "Job12306Registration_RegThread",
                "Job12306Registration_regFail_20:" + Loginname + ":1:获取手机号失败还原状态:" + recount + ":" + sqlupdatecus);
    }

    /**
     * 获取adsl反回RegistrationLong类入库 yangtao 2015年11月3日 18:38:33
     */
    public String getAdsl() {
        String adsl = PropertyUtil.getValue("adsl", "train.reg.properties");
        //        RegistrationLong.setAdslNo(adsl);
        return adsl;
    }
}
