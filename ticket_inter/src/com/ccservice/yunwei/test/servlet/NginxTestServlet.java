/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.yunwei.test.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.dao.TrainOfflineTomAgentAddDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTCUtil;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.tongcheng.service.TrainTongChengOfflineAddOrderThread;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 同程线下票 - 同程线下票出票请求接口
 * 
 * 主要项目平台操作这些在cn_home里面
 * 
 * 
Method:HTTP-POST
Url:CreateOrder
DataType:json
 * 
 * isSuccess true用231000,false用231099
 * Exception
 * 同程区分了配送到站和配送票
 * 
你们传递的抢票单，里面，是不是也是会传很多这些定制信息？

抢票没有这些定制 内容的，
都是默认值

 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class NginxTestServlet extends HttpServlet {
    private static final String LOGNAME = "Nginx测试请求接口";

    private int r1 = new Random().nextInt(10000000);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //System.out.println(request.getContextPath());
        //System.out.println(request.getRemoteAddr());
        
        response.sendRedirect(request.getContextPath()+"/test/test.jsp");
    }
    
}
