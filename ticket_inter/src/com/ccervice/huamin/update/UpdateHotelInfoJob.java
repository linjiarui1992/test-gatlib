package com.ccervice.huamin.update;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 
 * @author wzc 更新华闽的酒店基本信息
 */
public class UpdateHotelInfoJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新华闽酒店基本信息");
		new HuaminHotelDb().updateHotelID();
		System.out.println("更新华闽酒店基本信息over");
	}

}
