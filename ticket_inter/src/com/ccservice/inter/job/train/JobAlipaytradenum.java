package com.ccservice.inter.job.train;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadAlipayTrade;
import com.ccservice.inter.server.Server;

/**
 * 
 * 定时更新支付中状态订单
 * @time 2014年12月26日 下午1:06:32
 * @author wzc
 */
public class JobAlipaytradenum extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        WriteLog.write("12306支付宝交易号", "运行中：" + System.currentTimeMillis());
        Jobupdatetrade();
    }

    /**
     * 更新银联支付交易号
     * 
     * @time 2014年12月13日 下午12:40:36
     * @author wzc
     */
    public String Jobupdatetrade() {
        String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
        String updatesql = " update T_TRAINORDER set C_ISQUESTIONORDER=2 where  C_SUPPLYACCOUNT not like '%/%' and  C_ORDERSTATUS=2 and C_STATE12306=5 ";
        Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
        String sql = " select TOP 30 ID from T_TRAINORDER where C_ORDERSTATUS=2 and   C_STATE12306=5 and C_ISQUESTIONORDER!=2 and C_SUPPLYACCOUNT like '%alipay%' AND  (C_SUPPLYTRADENO is null or C_SUPPLYTRADENO  LIKE 'W%') order by C_CREATETIME asc ";
        List listresult = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        int paycounttime = Integer.valueOf(getSysconfigString("paycount"));//支付次数
        String dangqianagentid = getSysconfigString("dangqianjiekouagentid");//当前agentid
        if (listresult != null && listresult.size() > 0) {
            String paythreadcount = getSysconfigString("Alipaytradenumtreadcount");
            ExecutorService pool = Executors.newFixedThreadPool(Integer.valueOf(paythreadcount).intValue());
            TrainSupplyMethod trainSupplyMethod = new TrainSupplyMethod();
            try {
                String idstr = "";
                for (int i = 0; i < listresult.size(); i++) {
                    Map map = (Map) listresult.get(i);
                    String id = map.get("ID").toString();
                    if (i == listresult.size() - 1) {
                        idstr += id;
                    }
                    else {
                        idstr += id + ",";
                    }
                }
                String updatestatesql = "update T_TRAINORDER set C_STATE12306=8 where ID in (" + idstr + ")";
                Server.getInstance().getSystemService().findMapResultBySql(updatestatesql, null);
                WriteLog.write("12306支付宝交易号", "支付审核中：" + updatestatesql);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
            for (int i = 0; i < listresult.size(); i++) {
                try {
                    Map map = (Map) listresult.get(i);
                    long orderid = Long.valueOf(map.get("ID").toString());
                    Thread thr = new MyThreadAlipayTrade(trainSupplyMethod, paycounttime, dangqianagentid,
                            tongchengcallbackurl, orderid);
                    pool.execute(thr);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pool.shutdown();
        }
        return "";
    }

    /**
     * 加工同程接口返回值，改成客服容易理解
     * @param callresult
     * @return
     * @time 2014年12月29日 下午6:57:52
     * @author fiend
     */
    public String changeCallResult(String callresult) {
        if (callresult.equals("")) {
            return "出票接口异常";
        }
        if (callresult.contains("orderid 有误")) {
            return "非接口订单，请客服核对";
        }
        return callresult;
    }

    public static void main(String[] args) {
        new JobAlipaytradenum().Jobupdatetrade();
    }
}
