package com.ccservice.inter.job.train.thread;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.service12306.bean.TrainOrderReturnBean;
import com.ccservice.b2b2c.base.customerpassenger.Customerpassenger;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.dnsmaintenance.Dnsmaintenance;
import com.ccservice.b2b2c.base.insuruser.Insuruser;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;

/**
 * 作者:邹远超
 * 日期:2014年9月2日
 */
public class MyThreadQunar extends Thread {

    private String passengers;

    private Trainorder trainorder;

    private long trainorderid;

    private int ordermax;

    private String QunarCallBack;

    private String merchantCode;

    private String repUrl;

    public MyThreadQunar(long trainorderid, int ordermax, String QunarCallBack, String merchantCode) {
        this.trainorderid = trainorderid;
        this.ordermax = ordermax;
        this.QunarCallBack = QunarCallBack;
        this.merchantCode = merchantCode;
    }

    public void run() {
        //随机REP地址
        this.repUrl = randomIp();
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(this.trainorderid);
        Customeruser customeruser = Server.getInstance().getTrain12306Service().getcustomeruser(trainorder);
        long customeruserid = customeruser.getId();
        WriteLog.write(
                "JobGenerateOrderQunar",
                "订单ID:" + trainorder.getId() + ":获取账号返回,当前user:" + customeruser.getLoginname() + ":"
                        + customeruser.getDescription());
        if (customeruser.getId() > 0 && customeruser.getIsenable() == 1) {
            WriteLog.write("JobGenerateOrderQunar",
                    "订单ID:" + trainorder.getId() + ":获取12306账号成功" + customeruser.getLoginname());
            Customeruser cuser = Server.getInstance().getMemberService().findCustomeruser(customeruserid);
            String loginname = cuser.getLoginname();
            String result = "";
            if (trainorder.getExtnumber() == null
                    || ("".equals(trainorder.getExtnumber()) || (trainorder.getExtnumber().indexOf(",") == 0))
                    && (trainorder.getIsquestionorder() == null || trainorder.getIsquestionorder() == 0)) {
                WriteLog.write("JobGenerateOrderQunar", "当前的订单:" + trainorder.getId());
                result = acquisitionParameters(this.repUrl, trainorder.getId(), cuser.getId(), cuser.getCardnunber(),
                        loginname);
                if (result.contains("请尽快支付")) {
                    WriteLog.write("JobGenerateOrderQunar", "下单成功,当前电子单号:" + result.replace("请尽快支付", ""));
                    returnQunar(trainorder, "true", cuser, true);
                }
                else if (result.contains("数据库同步失败") || result.contains("12306订单信息不全")) {//同步数据库失败或者是12306订单信息不全
                    if (result.contains("数据库同步失败")) {
                        upTrainorder("UPDATE T_TRAINORDER SET C_ISQUESTIONORDER=3 WHERE ID=" + this.trainorderid);
                    }
                    Trainorderrc rc = new Trainorderrc();
                    rc.setOrderid(trainorder.getId());
                    rc.setContent("下单成功," + result);
                    rc.setStatus(Trainticket.ISSUED);
                    rc.setCreateuser(cuser.getLoginname());
                    rc.setYwtype(1);
                    Server.getInstance().getTrainService().createTrainorderrc(rc);
                }
            }
        }
        else if (customeruser.getId() > 0 && customeruser.getIsenable() == 0) {
            WriteLog.write("JobGenerateOrderQunar", "订单ID:" + trainorder.getId() + ":身份验证失败");
            refuse(1, customeruser, "身份验证失败:" + customeruser.getDescription());
        }
        else {
            customeruser = Server.getInstance().getMemberService().findCustomeruser(62);
            WriteLog.write("JobGenerateOrderQunar", "订单ID:" + trainorder.getId() + "获取12306账号失败");
            refuse(1, customeruser, "获取12306账号失败");
        }

    }

    /**
     * 说明:判断无座 
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午8:32:55
     * @author yinshubin
     */
    public boolean ishaveseat(Trainorder trainorder) {
        boolean result = true;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if ("无座".equals(trainticket.getSeatno())) {
                    WriteLog.write("JobGenerateOrderQunar", "当前订单:" + trainorder.getId() + ":ishaveseat:包含无座");
                    result = false;
                    String extSeat = trainticket.getTcseatno();
                    if (extSeat != null && !"".equals(extSeat)) {
                        try {
                            JSONArray jsa = JSONArray.fromObject(extSeat);
                            for (int i = 0; i < jsa.size(); i++) {
                                JSONObject jso = jsa.getJSONObject(i);
                                if (jso.has("0")) {
                                    result = true;
                                    WriteLog.write("JobGenerateOrderQunar", "当前订单:" + trainorder.getId()
                                            + ":ishaveseat:包含无座并且接受无座");
                                    break;
                                }
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 订单票信息是否完整
     * @param trainorder
     * @return
     * @time 2015年1月5日 上午10:41:13
     * @author fiend
     */
    public boolean isallticketseat(Trainorder trainorder) {
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                if (trainticket.getSeatno() == null) {
                    WriteLog.write("JobGenerateOrderQunar", "当前订单:" + trainorder.getId() + ";ishaveseat:因坐席信息不全，无法直接出票");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 说明:通过12306账号比对未完成订单
     * @param customeruserId
     * @param pid
     * @param pname
     * @param pidtype
     * @return  boolean
     * @time 2014年8月30日 上午11:15:55
     * @author yinshubin
     */
    @SuppressWarnings("unchecked")
    public boolean isalignmentTrue(long customeruserId, String pid, String pname, int pidtype) {
        boolean result = false;
        String sql = "SELECT * FROM T_CUSTOMERPASSENGER a JOIN T_CUSTOMERCREDIT b ON a.ID=b.C_REFID WHERE a.C_CUSTOMERUSERID="
                + customeruserId
                + " AND a.C_USERNAME='"
                + pname
                + "' AND b.C_CREDITTYPEID="
                + pidtype
                + " AND b.C_CREDITNUMBER='" + pid + "' AND b.C_STAUS=1 AND a.C_STATE=1";
        WriteLog.write("JobGenerateOrderQunar", "当前的乘客是否在该12306中:SQL:" + sql);
        List<Customerpassenger> customerpassengerList = Server.getInstance().getMemberService()
                .findAllCustomerpassengerBySql(sql, -1, 0);
        WriteLog.write("JobGenerateOrderQunar", "当前的乘客是否在该12306中:" + customerpassengerList.size());
        if (1 == customerpassengerList.size()) {
            result = true;
        }
        return result;
    }

    /**
     * 
     * 说明:获取下单所用的参数
     * @param trainorderid
     * @param customeruserId
     * @param cookieString
     * @param loginname
     * @param isjointrip 是否是联程票
     * @return gotoInit();
     * @time 2014年8月30日 上午11:16:03
     * @author yinshubin
     */
    public String acquisitionParameters(String url, long trainorderid, long customeruserId, String cookieString,
            String loginname) {
        WriteLog.write("JobGenerateOrderQunar", "获取下单所用的参数:" + url + ":" + trainorderid + ":" + customeruserId + ":"
                + cookieString + ":" + loginname);
        String train_code = "";
        String date = "";
        String start_station_name = "";
        String end_station_name = "";
        String passengers = "";
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            String pname = trainpassenger.getName();
            String pidnumber = trainpassenger.getIdnumber();
            int pidtype = trainpassenger.getIdtype();
            Trainticket trainticket = trainpassenger.getTraintickets().get(0);
            String idtype = "1";
            if (1 == pidtype) {
                idtype = "1";
            }
            if (3 == pidtype) {
                idtype = "B";
            }
            if (4 == pidtype) {
                idtype = "C";
            }
            if (5 == pidtype) {
                idtype = "G";
            }
            String tickettype = trainticket.getTickettype() + "";
            if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                tickettype = "1";
            }
            else {
                if ("0".equals(tickettype)) {
                    tickettype = "2";
                }
            }
            String passenger = pname + "|" + idtype + "|" + pidnumber + "|" + tickettype + "|"
                    + trainticket.getSeattype() + "|"
                    + Float.valueOf(trainticket.getPayprice()).toString().replace(".", "");
            train_code = trainticket.getTrainno();
            date = changeDate(trainticket.getDeparttime());
            passengers += passenger + ",";
        }
        this.passengers = passengers.substring(0, passengers.length() - 1);
        WriteLog.write("JobGenerateOrderQunar", "获取下单所用的参数:" + trainorderid + "(:)" + cookieString + "(:)"
                + start_station_name + "(:)" + end_station_name + "(:)" + date + "(:)" + train_code + "(:)"
                + passengers + "(:)" + loginname);
        Customeruser user = Server.getInstance().getMemberService().findCustomeruser(customeruserId);
        int r1 = new Random().nextInt(10000);//获取一个随机数，用来匹配日志记录的这个流程
        return TCTrainOrdering(user, trainorder, r1, start_station_name, end_station_name);
    }

    /**
     * 原来是返回String暂时把结果放到Changesupplytradeno里
     * 如需要可以使用方法getChangesupplytradeno获取
     * @param trainorder
     * @param from_station 出发站三字码，可不传
     * @param to_station 到达站三字码，可不传
     * @param isjointrip 是否是联程票
     * @return
     *>> 格式如:[{"ticket_type":"票类型","price":票价,"zwcode":"座位编码","passenger_id_type_code":"证件类型","passenger_name":"乘客姓名","passenger_id_no":"证件号"}]
     * >> ticket_type:1:成人票，2:儿童票，3:学生票，4:残军票
     * >> price:float类型
     * >> zwcode:9:商务座，P:特等座，M:一等座，O:二等座，6:高级软卧，4:软卧，3:硬卧，2:软座，1:硬座，0:站票[无座]
     * >> passenger_id_type_code:1:二代身份证，C:港澳通行证，G:台湾通 行证，B:护照
     * @time 2014年12月14日 上午10:28:02
     * @author wzc
     */
    public String TCTrainOrdering(Customeruser user, Trainorder trainorder, int r1, String from_station,
            String to_station) {
        WriteLog.write("JobGenerateOrderQunar", r1 + ":s:TCTrainOrdering:" + user);
        String result = "下单接口访问错误";
        List<Trainpassenger> passengerlist = trainorder.getPassengers();
        int ticketmark = 0;
        String passengersstr = "";
        Trainticket ticket = null;
        if (passengerlist.size() > 0) {
            JSONArray array = new JSONArray();
            for (int i = 0; i < passengerlist.size(); i++) {
                Trainpassenger p = passengerlist.get(i);
                List<Trainticket> tickets = p.getTraintickets();
                if (tickets != null && tickets.size() > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("passenger_id_no", p.getIdnumber());
                    obj.put("passenger_name", p.getName());
                    obj.put("ticket_type", tickets.get(ticketmark).getTickettype() == 0 ? 2 : tickets.get(ticketmark)
                            .getTickettype());
                    obj.put("price", tickets.get(ticketmark).getPrice());
                    obj.put("zwcode", TrainSupplyMethod.getzwname(tickets.get(ticketmark).getSeattype()));
                    obj.put("passenger_id_type_code", TrainSupplyMethod.getIdtype12306(p.getIdtype()));
                    array.add(obj);
                }
            }
            passengersstr = array.toString();
            List<Trainticket> tickets = passengerlist.get(0).getTraintickets();
            if (tickets.size() > 0) {
                ticket = tickets.get(ticketmark);
            }
        }
        if (ticket != null && ticket.getDeparttime().length() >= 10) {
            long t1 = System.currentTimeMillis();
            result = orderRound(ticket, passengersstr, user, result, r1, t1, from_station, to_station, 1);
            long t2 = System.currentTimeMillis();
            WriteLog.write("JobGenerateOrderQunar", r1 + ":下单用时:" + (t2 - t1));
        }
        else {
            WriteLog.write("JobGenerateOrderQunar", r1 + ":乘客票有问题");
        }
        return result;
    }

    /**
     * 循环下单 
     * @param ticket
     * @param passengersstr
     * @param user
     * @param result
     * @param r1
     * @param t1
     * @param from_station
     * @param to_station
     * @param i
     * @return
     * @time 2015年1月3日 下午5:41:26
     * @author chendong
     */
    public String orderRound(Trainticket ticket, String passengersstr, Customeruser user, String result, int r1,
            long t1, String from_station, String to_station, int i) {
        String train_date = ticket.getDeparttime().substring(0, 10);
        TrainOrderReturnBean returnob = Server
                .getInstance()
                .getTrain12306Service()
                .create12306Order(trainorder.getId(), train_date, from_station, to_station, ticket.getDeparture(),
                        ticket.getArrival(), ticket.getTrainno(), passengersstr, user);
        String code = returnob.getCode();
        String jsonmsg = returnob.getJson();
        String msg = returnob.getMsg();
        result = msg;
        boolean success = returnob.getSuccess();
        WriteLog.write("JobGenerateOrderQunar", r1 + ":name:" + user.getLoginname() + ",pwd:" + user.getLogpassword()
                + ":e:TCTrainOrdering:" + code);
        WriteLog.write("JobGenerateOrderQunar", r1 + ":msgmsg:" + msg);
        if (success) {
            WriteLog.write("JobGenerateOrderQunar", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i + ":下单耗时:"
                    + (System.currentTimeMillis() - t1));

            for (int j = 0; j < 5; j++) {
                if (!isall(jsonmsg)) {
                    jsonmsg = getstr(user);
                }
                else {
                    break;
                }
            }
            if (isall(jsonmsg)) {
                return saveOrderInformation(jsonmsg, trainorder.getId(), user.getLoginname());
            }
            else {
                upTrainorder("UPDATE T_TRAINORDER SET C_SUPPLYACCOUNT='" + user.getLoginname()
                        + "',C_ISQUESTIONORDER=1 WHERE ID=" + this.trainorderid);
                return "12306订单信息不全";
            }
        }
        else if (msg.indexOf("账号尚未通过身份信息核验") > 0
                || (msg.indexOf("已订") < 0 && msg.indexOf("不可购票") < 0 /* && msg.indexOf("没有足够的票") < 0*/
                        && msg.indexOf("排队人数现已超过余票数") < 0 && msg.indexOf("本次购票行程冲突") < 0/* && msg.indexOf("已无余票") < 0*/
                        && msg.indexOf("价格发生变化") < 0/* && msg.indexOf("余票不足") < 0*/&& msg.indexOf("非法的席别") < 0
                        && msg.indexOf("您的身份信息未经核验") < 0 && msg.indexOf("当前提交订单用户过多") < 0)) {
            saveTrainorderdc(result, trainorder, user, i);
            if (i < this.ordermax) {
                if (msg.indexOf("订单未支付") > 0 || msg.indexOf("取消次数过多") > 0 || msg.indexOf("账号尚未通过身份信息核验") > 0
                        || msg.indexOf("第三方") > 0 || i % 3 == 0) {
                    if (msg.indexOf("取消次数过多") > 0) {//取消次数过多换账号
                        isenableTodaycustomeruser(user);
                    }
                    if (msg.indexOf("账号尚未通过身份信息核验") > 0) {//您的账号尚未通过身份信息核验
                        isenableforevercustomeruser(user, msg);
                    }
                    if (msg.indexOf("第三方") > 0 || i % 3 == 0) {//账号使用重复切换账号重新下单
                        freecustomeruser(user);
                    }
                    WriteLog.write("JobGenerateOrderQunar", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i
                            + "准备切换账号");
                    Customeruser customeruser = Server.getInstance().getTrain12306Service().getcustomeruser(trainorder);
                    if (customeruser.getId() > 0 && customeruser.getIsenable() == 1) {
                        WriteLog.write("JobGenerateOrderQunar", r1 + ":当前订单:" + trainorder.getId() + ":循环次数:" + i
                                + ":切换账号成功" + user.getLoginname() + "=========>" + customeruser.getLoginname());
                        user = customeruser;
                    }
                }
                try {
                    Thread.sleep(500L);
                }
                catch (InterruptedException e) {
                }
                return orderRound(ticket, passengersstr, user, result, r1, t1, from_station, to_station, i + 1);
            }
            else {
                refuse(1, user, result);
                freecustomeruser(user);
            }
        }
        else {
            refuse(1, user, result);
            freecustomeruser(user);
        }
        return result;
    }

    /**
     * 获取一个12306账号使用，获取后该账号变成使用中
     * 
     * 获取到一个customeruser 
     * 1、判断这个customeruser的id>0
     * 2、获取cookie   String cookieString = customeruser.getCardnunber();
     * 3、cust.getIsenable()==1 可用
     * 4、cust.getIsenable()==0 不可用，原因在cust.getDescription()里
     * @param p
     * @return
     * @time 2014年12月12日 下午7:46:25
     * @author chendong
     */
    @SuppressWarnings("unchecked")
    public Customeruser getcustomeruser_back(List<Trainpassenger> prainpassengerList) {
        Trainpassenger p = prainpassengerList.get(0);
        String sql = "SELECT b.* FROM T_CUSTOMERUSER b join T_CUSTOMERPASSENGER a on b.ID=a.C_CUSTOMERUSERID"
                + " join T_CUSTOMERCREDIT c on a.ID=c.C_REFID AND b.C_STATE=1 AND b.C_TYPE = 4 AND b.C_ENNAME='1'"
                + " AND b.C_ISENABLE=1 where a.C_USERNAME = \'" + p.getName() + "\' and c.C_CREDITNUMBER=\'"
                + p.getIdnumber() + "\'" + " and a.C_TYPE = 4 AND a.C_STATE=1";
        List<Customeruser> customerusers = Server.getInstance().getMemberService().findAllCustomeruserBySql(sql, -1, 0);
        Customeruser cust = new Customeruser();
        if (customerusers.size() > 0) {//
            WriteLog.write("JobGenerateOrderQunar", "下单前获取12306账号:" + customerusers.get(0).getLoginname());
            cust = customerusers.get(0);
        }
        else {
            cust = getcustomeruserbyEnname(true);
        }
        if (cust.getId() > 0) {
            cust = getc(cust, prainpassengerList);
        }
        return cust;
    }

    @SuppressWarnings("unchecked")
    public Customeruser getcustomeruserbyEnname(boolean isfirst) {
        int count = 200;
        Customeruser cust = new Customeruser();
        List<Customeruser> customeruser12306account = Server.getServer().getCustomeruser12306account();
        if (customeruser12306account == null) {
            Server.getServer().setCustomeruser12306account(new ArrayList<Customeruser>());
        }
        if (customeruser12306account.size() > 0) {
            cust = customeruser12306account.remove(0);
            Server.getServer().setCustomeruser12306account(customeruser12306account);
        }
        else {
            String sql = "SELECT * FROM T_CUSTOMERUSER WHERE C_STATE=1 AND C_TYPE = 4 AND C_ENNAME='1' "
                    + "AND C_ISENABLE=1 AND C_LOGINNUM>0 AND C_LOGINNUM<90 ORDER BY C_LOGINNUM ASC";
            customeruser12306account = Server.getInstance().getMemberService().findAllCustomeruserBySql(sql, count, 0);
            if (customeruser12306account.size() > 0) {
                cust = customeruser12306account.remove(0);
                Server.getServer().setCustomeruser12306account(customeruser12306account);
            }
        }
        try {
            if (cust.getId() > 0) {
                cust = Server.getInstance().getMemberService().findCustomeruser(cust.getId());
                WriteLog.write("JobGenerateOrderQunar", "ASD:" + cust.getEnname() + ":" + cust.getState());
                if ("1".equals(cust.getEnname()) && cust.getState() == 1) {
                    String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='0' where ID = " + cust.getId() + " ";
                    try {//拿到一个账号把这个账号修改为使用中的状态
                        Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (isfirst) {
                    cust = getcustomeruserbyEnname(true);
                }
            }
        }
        catch (Exception e) {
        }
        return cust;
    }

    /**
     * 绑定账号
     * @param cust
     * @param prainpassengerList
     * @return
     * @time 2014年12月31日 上午11:25:49
     * @author chendong
     */
    public Customeruser getc(Customeruser cust, List<Trainpassenger> prainpassengerList) {
        boolean isfreecustomeruser = true;
        for (int i = 0; i < prainpassengerList.size(); i++) {
            Trainpassenger trainpassenger = prainpassengerList.get(i);
            String shenfenzhengheyanjieguo = rep16Method(cust.getLoginname(), trainpassenger.getName(),
                    trainpassenger.getIdnumber(), TrainSupplyMethod.getIdtype12306(trainpassenger.getIdtype()),
                    cust.getCardnunber());
            WriteLog.write("JobGenerateOrderQunar_getc", cust.getLoginname() + ":身份验证:返回结果" + shenfenzhengheyanjieguo);
            if (shenfenzhengheyanjieguo.equals("false")) {
                shenfenzhengheyanjieguo = rep16Method(cust.getLoginname(), trainpassenger.getName(),
                        trainpassenger.getIdnumber(), TrainSupplyMethod.getIdtype12306(trainpassenger.getIdtype()),
                        cust.getCardnunber());
                WriteLog.write("JobGenerateOrderQunar", cust.getLoginname() + ":身份验证:返回结果2:" + shenfenzhengheyanjieguo);
            }
            if ("已通过".equals(shenfenzhengheyanjieguo) || "true".equals(shenfenzhengheyanjieguo)) {
                cust.setIsenable(1);
                isfreecustomeruser = false;
            }
            else if ("未登录".equals(shenfenzhengheyanjieguo)) {
                cust = verification12306(cust);
                shenfenzhengheyanjieguo = rep16Method(cust.getLoginname(), trainpassenger.getName(),
                        trainpassenger.getIdnumber(),
                        TrainSupplyMethod.getIdtype12306(trainpassenger.getIdtype()) + "", cust.getCardnunber());
                WriteLog.write("JobGenerateOrderQunar", cust.getLoginname() + "身份验证:返回结果:" + shenfenzhengheyanjieguo);
                if (shenfenzhengheyanjieguo.equals("false")) {
                    shenfenzhengheyanjieguo = rep16Method(cust.getLoginname(), trainpassenger.getName(),
                            trainpassenger.getIdnumber(), TrainSupplyMethod.getIdtype12306(trainpassenger.getIdtype()),
                            cust.getCardnunber());
                }
                if ("已通过".equals(shenfenzhengheyanjieguo) || "true".equals(shenfenzhengheyanjieguo)) {
                    cust.setIsenable(1);
                    isfreecustomeruser = false;
                }
                else if ("待核验".equals(shenfenzhengheyanjieguo) || shenfenzhengheyanjieguo.contains("证件号码输入有误")) {
                    try {
                        trainpassenger.setAduitstatus(1);
                        Server.getInstance().getTrainService().updateTrainpassenger(trainpassenger);
                        cust.setDescription("添加乘客" + trainpassenger.getName() + "未通过身份效验"
                                + trainpassenger.getIdnumber());
                    }
                    catch (Exception e) {
                    }
                    cust.setIsenable(0);
                    break;
                }
                else {
                    cust.setIsenable(0);
                    cust.setDescription(trainpassenger.getName() + ":未通过身份效验|"
                            + (cust.getDescription() == null ? "" : cust.getDescription()));
                    break;
                }
            }
            else if ("待核验".equals(shenfenzhengheyanjieguo) || shenfenzhengheyanjieguo.contains("证件号码输入有误")) {
                try {
                    trainpassenger.setAduitstatus(1);
                    Server.getInstance().getTrainService().updateTrainpassenger(trainpassenger);
                    cust.setDescription("添加乘客" + trainpassenger.getName() + "未通过身份效验" + trainpassenger.getIdnumber());
                }
                catch (Exception e) {
                }
                cust.setIsenable(0);
                break;
            }
            else {
                cust.setDescription(trainpassenger.getName() + ":未通过身份效验|"
                        + (cust.getDescription() == null ? "" : cust.getDescription()));
                cust.setIsenable(0);
                break;
            }
        }
        if (isfreecustomeruser) {
            WriteLog.write("JobGenerateOrderQunar_", cust.getLoginname() + ":isfreecustomeruser:" + isfreecustomeruser
                    + ":Description:" + cust.getDescription());
            freecustomeruser(cust);
        }
        return cust;
    }

    /**
     * 临时使用 
     * @param customeruser
     * @return
     * @time 2014年12月31日 上午11:21:31
     * @author fiend
     */
    public Customeruser verification12306(Customeruser customeruser) {
        String url = this.repUrl;
        int r1 = new Random().nextInt(10000);
        try {
            String logname = customeruser.getLoginname();
            String logpassword = customeruser.getLogpassword();
            customeruser.setCardtype(null);
            customeruser.setState(0);
            String par = "datatypeflag=12&logname=" + logname + "&logpassword=" + logpassword;
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号:" + logname + ":" + url
                    + "(:)" + par);
            String result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号:" + logname + ":"
                    + result);
            if (null != result && !"".equals(result) && result.contains("JSESSIONID")) {
                customeruser.setState(1);
                customeruser.setId(customeruser.getId());
                customeruser.setCardnunber(result);
                customeruser.setMemberemail(url);
                customeruser.setEnname("1");
                customeruser.setModifytime(new Timestamp(System.currentTimeMillis()));
                Server.getInstance().getMemberService().updateCustomeruserIgnoreNull(customeruser);
                WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号成功:" + logname);
            }
            else {
                WriteLog.write("Job12306index_MyThreadLogin", r1 + ":" + logname + ";正在自动登录12306账号失败:" + logname);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return customeruser;
    }

    /**
     * 身份证核验，绑定账号,新身份验证中间方法
     * 
     * @param logname 12306账号  
     * @param logpassword 12306密码
     * @param name 姓名
     * @param id_no 姓名
     * @param email 姓名
     * @return
     * @time 2014年12月19日 下午7:41:51
     * @author chendong
     */
    public String rep16Method(String passengerName, String id_no, String id_type, String cookieString) {
        String resultString = "";
        try {
            passengerName = URLEncoder.encode(passengerName, "utf-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String paramContent = "datatypeflag=16&name=" + passengerName + "&id_no=" + id_no + "&id_type=" + id_type
                + "&cookie=" + cookieString;
        resultString = SendPostandGet.submitPost(this.repUrl, paramContent, "utf-8").toString();
        return resultString;
    }

    /**
     * 占用一个账号后的解锁，
     * 支付完成等调用
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void freecustomeruser(Customeruser cust) {
        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='1' where C_ENNAME='0' AND C_LOGINNAME = '"
                + cust.getLoginname() + "' ";

        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", "freecustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 这个账号取消三次后今天不能使用了
     * 支付完成等调用
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void isenableTodaycustomeruser(Customeruser cust) {
        String sql = "UPDATE T_CUSTOMERUSER SET C_ENNAME='2' where C_LOGINNAME = '" + cust.getLoginname() + "' ";
        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", "isenableTodaycustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 这个账号由于不能使用了把 isenable 改为0表示禁用了
     * @param cust
     * @time 2014年12月23日 下午6:31:03
     * @author chendong
     */
    public void isenableforevercustomeruser(Customeruser cust, String description) {
        String sql = "UPDATE T_CUSTOMERUSER SET " + Customeruser.COL_isenable + "='0'," + Customeruser.COL_state
                + "='0'," + Customeruser.COL_description + "='" + description + "' WHERE C_LOGINNAME = '"
                + cust.getLoginname() + "' ";
        try {
            int count = Server.getInstance().getSystemService().excuteAdvertisementBySql(sql);
            WriteLog.write("Job_MyThread_user", "isenableforevercustomeruser:" + count + ":count:" + sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 时间转换 
     * @param date
     * @param format
     * @return
     * @time 2014年10月9日 下午6:45:34
     * @author yinshubin
     */
    public Timestamp formatStringToTime(String date, String format) {
        try {
            SimpleDateFormat simplefromat = new SimpleDateFormat(format);
            return new Timestamp(simplefromat.parse(date).getTime());

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param str
     * @return 是否为null或""
     */
    public boolean isNotNullOrEpt(String str) {
        if (str != null && str.trim().trim().length() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatchinaMMdd(Timestamp date) {
        return (new SimpleDateFormat("MM月dd日").format(date));
    }

    /**
     * 日期转换 
     * @param date
     * @return
     * @time 2014年10月9日 下午6:46:05
     * @author yinshubin
     */
    public String formatTimestampHHmm(Timestamp date) {
        try {
            return (new SimpleDateFormat("HH:mm").format(date));

        }
        catch (Exception e) {
            return "";
        }
    }

    /**
     * 说明:保存订单信息比对车次 
     * @param traincode
     * @param trainno
     * @return
     * @time 2014年10月6日 上午8:26:05
     * @author yinshubin
     */
    public boolean comparisonTraincode(String traincode, String trainno) {
        boolean result = false;
        if (trainno.contains("/")) {
            String[] trainnos = trainno.split("/");
            for (int i = 0; i < trainnos.length; i++) {
                result = traincode.equals(trainnos[i]);
                if (result) {
                    break;
                }
            }
        }
        else {
            result = traincode.equals(trainno);
        }
        return result;
    }

    /**
     * 说明:根据12306要求，将数据库中日期转变格式
     * @param date
     * @return date
     * @time 2014年8月30日 上午11:18:41
     * @author yinshubin
     */
    public String changeDate(String date) {
        try {
            Date date_result = new Date();
            DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
            date_result = df.parse(date);
            DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
            date = dfm.format(date_result);
        }
        catch (ParseException e1) {
            e1.printStackTrace();
        }
        return date;
    }

    /**
     * 说明:将自动下单成功的订单信息存入数据库
     * @param str
     * @param trainorderid
     * @param loginname
     * @param isjointrip
     * @return  存储结果，电子订单号
     * @time 2014年8月30日 上午11:18:59
     * @author yinshubin
     */
    public String saveOrderInformation(String str, long trainorderid, String loginname) {
        try {
            JSONObject jsono = JSONObject.fromObject(str);
            if (jsono.has("data")) {
                JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {//获取所有订单
                        JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.toString().contains("待支付")) {//拿到那个唯一的待支付订单
                            WriteLog.write("JobGenerateOrderQunar", "订单号:" + trainorderid + ":下单成功，获取待支付订单信息:"
                                    + jsonoorderDBList.toString());
                            String sequence_no = jsonoorderDBList.getString("sequence_no");
                            String order_date = jsonoorderDBList.getString("order_date");
                            Float ticket_total_price_page = Float.valueOf(jsonoorderDBList
                                    .getString("ticket_total_price_page"));
                            String start_time = jsonoorderDBList.getString("start_train_date_page");
                            String train_code = jsonoorderDBList.getString("train_code_page");
                            String from_station = jsonoorderDBList.getJSONArray("from_station_name_page").get(0)
                                    .toString();
                            String to_station = jsonoorderDBList.getJSONArray("to_station_name_page").get(0).toString();
                            String trainno = "";
                            String traintime = "";
                            String startstation = "";
                            String endstation = "";
                            trainno = trainorder.getPassengers().get(0).getTraintickets().get(0).getTrainno();
                            traintime = trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparttime();
                            startstation = trainorder.getPassengers().get(0).getTraintickets().get(0).getDeparture();
                            endstation = trainorder.getPassengers().get(0).getTraintickets().get(0).getArrival();
                            if (comparisonTraincode(train_code, trainno) && start_time.equals(traintime)
                                    && from_station.equals(startstation) && to_station.equals(endstation)) {
                                if (jsonoorderDBList.has("tickets")) {
                                    JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                                    updatepassengerfrom12306jsonbypassenger(trainorder.getPassengers(), tickets);
                                }
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                upTrainorder("UPDATE T_TRAINORDER SET C_CREATETIME='"
                                        + new Timestamp(format.parse(order_date).getTime())
                                        + "',C_EXTORDERCREATETIME='"
                                        + new Timestamp(format.parse(order_date).getTime()) + "',C_EXTNUMBER='"
                                        + sequence_no + "',C_SUPPLYACCOUNT='" + loginname + "',C_TOTALPRICE="
                                        + ticket_total_price_page + " WHERE ID=" + this.trainorderid);
                                Trainorderrc rc = new Trainorderrc();
                                rc.setOrderid(trainorder.getId());
                                rc.setContent("下单成功,电子单号: " + sequence_no);
                                rc.setYwtype(1);
                                rc.setCreateuser(loginname);
                                Server.getInstance().getTrainService().createTrainorderrc(rc);
                                WriteLog.write("JobGenerateOrderQunar", "订单号:" + trainorderid + ":下单成功，更改订单信息信息:"
                                        + trainorder.getId() + "(:)" + sequence_no);
                                return "请尽快支付" + sequence_no;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            WriteLog.write("JobGenerateOrderQunar", "订单号:" + trainorderid + ":下单成功，数据库同步失败:" + e);
        }
        WriteLog.write("JobGenerateOrderQunar", "订单号:" + trainorderid + ":下单成功，数据库同步失败");
        return "出票成功，数据库同步失败";
    }

    /**
     * 说明:自动下单失败的书写操作记录
     * @param result
     * @param loginname
     * @param trainorder
     * @param isjointrip  是否是联程票
     * @param customeruserId
     * @param orderno 第几次下单
     * @time 2014年8月30日 上午11:42:19
     * @author yinshubin
     */
    public void saveTrainorderdc(String result, Trainorder trainorder, Customeruser user, int orderno) {
        trainorder.setId(trainorder.getId());
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(user.getLoginname() + "第" + orderno + "次:下单失败:" + result);
        rc.setCreateuser(user.getLoginname());
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 说明:拒单公用方法
     * @param orderid
     * @param state
     * @param user
     * @param returnmsg  同程订单下单失败回调 具体原因
     * @time 2014年9月1日 下午2:45:18
     * @author yinshubin
     */
    public void refuse(int state, Customeruser user, String returnmsg) {
        WriteLog.write("JobGenerateOrderQunar", "订单ID:" + this.trainorderid + ";12306账号:" + user.getLoginname()
                + ":refuse:准备拒单:拒单原因:" + returnmsg);
        upTrainorder("UPDATE T_TRAINORDER SET C_ORDERSTATUS=" + Trainorder.CANCLED + ",C_REFUSEAFFIRM="
                + Trainorder.Refuseaffirm.NOAFFIRM + ",C_REFUNDREASON=" + state + " WHERE ID=" + this.trainorderid);
        Trainorderrc rc = new Trainorderrc();
        rc.setStatus(Trainticket.WAITISSUE);
        rc.setContent(returnmsg);
        rc.setCreateuser(user.getLoginname());
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
        returnQunar(trainorder, returnmsg, user, false);
    }

    /**
     * 说明:生成下单失败的解决办法
     * @param result
     * @param loginname
     * @param trainorder
     * @return
     * @time 2014年8月30日 上午11:42:56
     * @author yinshubin
     */
    public String saveSolution(String result, String loginname, Trainorder trainorder) {
        String solution = getSolution(result);
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(loginname + "下单失败:" + solution);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
        return "";
    }

    public static String getSolution(String result) {
        if ("下单成功，数据库同步失败".equals(result)) {
            return "请手动查询12306订单";
        }
        else if ("此车次无票".equals(result) || "已买过票了".equals(result)) {
            return "请打电话确认后拒单";
        }
        else {
            return "请手动下单";
        }
    }

    /**
     * 根据12306返回的tickets,获取到对应人的票的信息,并且把人放进去
     * 暂不用
     * @param trainpassenger
     * @param tickets
     * @param isjointrip
     * @time 2014年12月29日 下午5:49:51
     * @author chendong
     */
    private void updatepassengerfrom12306jsonbypassenger(List<Trainpassenger> trainpassengers, JSONArray tickets) {
        int k = 0;
        for (int i = 0; i < tickets.size(); i++) {
            JSONObject jsonoticket = tickets.getJSONObject(i);
            int ticket_type_code = jsonoticket.getInt("ticket_type_code");//乘客类型（票种）
            JSONObject jsonopassengerDTO = jsonoticket.getJSONObject("passengerDTO");
            String ticket_no = jsonoticket.getString("ticket_no");
            String coach_no = jsonoticket.getString("coach_no");
            String seat_name = jsonoticket.getString("seat_name");
            Float str_ticket_price_page = Float.valueOf(jsonoticket.getString("str_ticket_price_page"));
            String passenger_id_no = jsonopassengerDTO.getString("passenger_id_no");
            if (ticket_type_code == 1) {//成人
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);//获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if (trainticket.getTickettype() == 1 && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        //                        trainticket = Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        trainticket.setSeatno(seat_name);
                        trainticket.setPrice(str_ticket_price_page);
                        trainticket.setPayprice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        break;//找到这个人了就不往下走了,继续找
                    }
                }
            }
            else if (ticket_type_code == 2) {//儿童
                for (int j = 0; j < trainpassengers.size(); j++) {
                    Trainpassenger trainpassenger = trainpassengers.get(j);//获取到对象的票的信息
                    Trainticket trainticket = trainpassenger.getTraintickets().get(k);
                    if (trainticket.getTickettype() == 2 && trainticket.getSeatno() == null
                            && !"".equals(trainpassenger.getIdnumber())
                            && trainpassenger.getIdnumber().toUpperCase().equals(passenger_id_no)) {
                        //                        trainticket = Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                        trainticket.setTicketno(ticket_no);
                        trainticket.setTickettype(ticket_type_code);
                        trainticket.setCoach(coach_no);
                        trainticket.setSeatno(seat_name);
                        trainticket.setPrice(str_ticket_price_page);
                        trainticket.setPayprice(str_ticket_price_page);
                        Server.getInstance().getTrainService().updateTrainticket(trainticket);
                        break;//找到这个人了就不往下走了,继续找
                    }
                    else {
                        continue;//找不到这个人就继续走
                    }
                }
            }
        }
    }

    /**
     * 身份证核验，绑定账号,新身份验证中间方法
     * 
     * @param logname 12306账号  
     * @param logpassword 12306密码
     * @param name 姓名
     * @param id_no 姓名
     * @param email 姓名
     * @return
     * @time 2014年12月19日 下午7:41:51
     * @author chendong
     */
    public String rep16Method(String loginname12306, String passengerName, String id_no, String id_type,
            String cookieString) {
        String repUrl = this.repUrl;
        String resultString = "";
        WriteLog.write("JobGenerateOrderQunar", loginname12306 + ":rep16method:" + repUrl + ":" + loginname12306 + ":"
                + passengerName + ":" + id_no);
        try {
            passengerName = URLEncoder.encode(passengerName, "utf-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String paramContent = "datatypeflag=16&name=" + passengerName + "&id_no=" + id_no + "&id_type=" + id_type
                + "&cookie=" + cookieString;

        resultString = SendPostandGet.submitPost(repUrl, paramContent, "utf-8").toString();
        WriteLog.write("JobGenerateOrderQunar", loginname12306 + ":" + resultString + ":" + paramContent);
        return resultString;
    }

    /**
     * QUNAR回调占座结果
     * @param i
     * @param orderid
     * @param returnmsg 回调具体内容  占座成功传true
     * @return
     * @time 2014年12月12日 下午2:20:30
     * @author fiend
     */
    public String callBackQunarOrdered(long orderid, String returnmsg) {
        String result = "false";
        String url = this.QunarCallBack;
        returnmsg = returnMsgStr(returnmsg);
        try {
            returnmsg = URLEncoder.encode(returnmsg, "utf-8");
        }
        catch (Exception e) {
        }
        JSONObject jso = new JSONObject();
        jso.put("trainorderid", orderid);
        jso.put("method", "train_order_callback");
        jso.put("returnmsg", returnmsg);
        jso.put("merchantCode", this.merchantCode);
        try {
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 处理加工12306返回结果 
     * @param returnmsg
     * @return
     * @time 2015年1月4日 上午11:18:03
     * @author fiend
     */
    public String returnMsgStr(String returnmsg) {
        if (returnmsg.contains("获取12306账号失败") || returnmsg.contains("您的账号尚未通过身份信息核验")
                || returnmsg.contains("获取下单服务器失败")) {
            return "下单失败";
        }
        return returnmsg;
    }

    /**
     * QUNAR占座回调统一处理方案 
     * @param trainorder
     * @param returnmsg
     * @param user
     * @param isordered
     * @time 2015年1月4日 下午2:12:58
     * @author fiend
     */
    public void returnQunar(Trainorder trainorder, String returnmsg, Customeruser user, boolean isordered) {
        String isbudanstr = "占座";
        String str = isordered ? isbudanstr + "成功" : isbudanstr + "失败";
        WriteLog.write("JobGenerateOrderQunar", "订单ID:" + trainorder.getId() + ";12306账号:" + user.getLoginname()
                + ":refuse:准备调用QUNAR:" + isbudanstr + ":结果回调接口====>" + str);
        String callbackordered = "调:" + isbudanstr + ":接口失败";
        callbackordered = callBackQunarOrdered(trainorder.getId(), returnmsg);
        WriteLog.write("JobGenerateOrderQunar", "订单ID:" + trainorder.getId() + ";12306账号:" + user.getLoginname()
                + ":refuse:QUNAR" + isbudanstr + "结果回调接口返回====>" + callbackordered);
        if (callbackordered.contains("没有获取到基础价格")) {
            refuse(3, user, "qunar票价和12306不符");
        }
        else if (!"success".equalsIgnoreCase(callbackordered)) {
            qunarFalseRc(callbackordered, user.getLoginname(), trainorder, isordered, isbudanstr);
            if (isordered) {
                upTrainorder("UPDATE T_TRAINORDER SET C_STATE12306=" + Trainorder.ORDEREDWAITPAY
                        + ",C_ISQUESTIONORDER=" + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
            }
            else {
                upTrainorder("UPDATE T_TRAINORDER SET C_STATE12306=" + Trainorder.ORDERFALSE + ",C_ISQUESTIONORDER="
                        + Trainorder.CAIGOUQUESTION + " WHERE ID=" + this.trainorderid);
            }
        }
        else {
            try {
                if (returnmsg.contains("qunar票价和12306不符")) {
                    WriteLog.write("MyThreadQunar_cancel12306", trainorder.getId() + "------>" + user.getLoginname()
                            + "------>qunar票价和12306不符,开始取消12306订单");
                    String cookie = user.getCardnunber();
                    String url = randomIp();
                    String param = "datatypeflag=10&cookie=" + cookie + "&extnumber=" + trainorder.getExtnumber()
                            + "&trainorderid=" + trainorder.getId();
                    WriteLog.write("MyThreadQunar_cancel12306", trainorder.getId() + "------>取消12306订单" + url
                            + "------>" + param);
                    String result = SendPostandGet.submitPost(url, param, "UTF-8").toString();
                    WriteLog.write("MyThreadQunar_cancel12306", trainorder.getId() + "------>取消结果" + result);
                    if (result.contains("取消订单成功")) {
                        freecustomeruser(user);
                    }
                }
            }
            catch (Exception e) {
                WriteLog.write("MyThreadQunar_cancel12306_error", trainorder.getId() + "------>" + user.getLoginname()
                        + "------>取消订单异常");
            }
            qunarTrueRc(user.getLoginname(), trainorder, isordered, isbudanstr);
            if (isordered) {
                upTrainorder("UPDATE T_TRAINORDER SET C_STATE12306=" + Trainorder.ORDEREDWAITPAY
                        + ",C_ISQUESTIONORDER=" + Trainorder.NOQUESTION + " WHERE ID=" + this.trainorderid);
            }
            else {
                upTrainorder("UPDATE T_TRAINORDER SET C_STATE12306=" + Trainorder.ORDERFALSE + ",C_ISQUESTIONORDER="
                        + Trainorder.NOQUESTION + " WHERE ID=" + this.trainorderid);
            }
        }
    }

    /**
     * QUNAR回调失败后 生成操作记录
     * @param result
     * @param loginname
     * @param trainorder
     * @param isordered
     * @time 2015年1月4日 下午2:05:46
     * @author fiend
     */
    public void qunarFalseRc(String result, String loginname, Trainorder trainorder, boolean isordered,
            String isbudanstr) {
        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(str + "----回调失败:" + result);
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * QUNAR回调成功后 生成操作记录
     * @param loginname
     * @param trainorder
     * @param isordered
     * @time 2015年1月4日 下午2:05:46
     * @author fiend
     */
    public void qunarTrueRc(String loginname, Trainorder trainorder, boolean isordered, String isbudanstr) {
        String str = isordered ? "---" + isbudanstr + "成功" : "---" + isbudanstr + "失败";
        Trainorderrc rc = new Trainorderrc();
        rc.setContent(str + "----回调成功");
        rc.setCreateuser(loginname);
        rc.setOrderid(trainorder.getId());
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    /**
     * 12306返回信息是否齐全
     * @param str
     * @return
     */
    public boolean isall(String str) {
        JSONObject jsono = JSONObject.fromObject(str);
        if (jsono.has("data")) {
            JSONObject jsonodata = jsono.getJSONObject("data");
            if (jsonodata.has("orderDBList")) {
                JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                for (int j = 0; j < jsonaorderDBList.size(); j++) {// 获取所有订单
                    JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                    if (jsonoorderDBList.has("tickets")) {
                        JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                        if (tickets.size() == this.trainorder.getPassengers().size()
                                && str.contains(this.trainorder.getPassengers().get(0).getName())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * 重新获取未支付订单
     * @param customeruser
     * @return
     */
    public String getstr(Customeruser customeruser) {
        String url = randomIp();
        String par = "datatypeflag=18&cookie=" + customeruser.getCardnunber() + "&passengers=" + this.passengers
                + "&trainorderid=" + this.trainorder.getId();
        WriteLog.write("Train12306_nopayorder", "订单号：" + this.trainorder.getId() + ":获取未完成订单（问题订单使用） ，调取TrainInit："
                + url + "(:)" + par);
        String infodata = "";
        try {
            infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        WriteLog.write("Train12306_nopayorder", "订单号：" + this.trainorder.getId() + ":获取未完成订单（问题订单使用） ，调取TrainInit：返回"
                + infodata);
        return infodata;
    }

    /**
     *  随机调取下单接口，获取12306-cookie所在IP
     * @return cookie所在IP
     * @time 2014年12月16日 下午12:07:53
     * @author fiend
     */
    public String randomIp() {
        String canorderips = getSystemConfig("Reptile_traininit_strs");
        if (canorderips.equals("46") || canorderips.trim().equals("")) {
            return getSystemConfig("Reptile_traininit_url");
        }
        String[] iscanorderip = canorderips.split(",");
        int i = (int) (Math.random() * iscanorderip.length);
        return getSystemConfig("Reptile_traininit_url" + iscanorderip[i]);
    }

    @SuppressWarnings("unchecked")
    public String getSystemConfig(String name) {
        List<Sysconfig> configs = Server.getInstance().getSystemService()
                .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
        if (configs != null && configs.size() == 1) {
            Sysconfig config = configs.get(0);
            return config.getValue();
        }
        return "46";
    }

    /**
     * 修改订单
     * @param sql
     * @time 2015年1月25日 下午4:10:50
     * @author fiend
     */
    public void upTrainorder(String sql) {
        Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    }
}
