package com.ccservice.inter.job.train;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThreadQuery;

/**
 * @time 2014年8月30日 上午11:01:41
 * @author yinshubin
 * 订时任务，审核已出票，支付状态为支付完成并且订单创建时间在系统当前时间前45分钟以内的所有订单
 */
public class JobQueryMyOrder extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        //        queryMyOrderAll();
    }

    public void gotoQuery(long orderid, long ordersuccesstime, int queryno) {
        String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
        String Taobao_TrainCallBack = getSysconfigString("Taobao_TrainCallBack");//淘宝回调地址
        String freeType = getSysconfigString("freeType");//释放账号的类型 1:循环使用(默认类型),2:一天用一次,3:发车后才可以用
        ExecutorService pool = Executors.newFixedThreadPool(1);
        Long l1 = System.currentTimeMillis();
        Thread t1 = null;
        //        String url = randomIp();
        //        t1 = new MyThreadQuery(url, orderid, ordersuccesstime, tongchengcallbackurl, queryno, freeType,
        //                Taobao_TrainCallBack);
        //   t1 = new MyThreadQuery(url, orderid, ordersuccesstime, tongchengcallbackurl, queryno, freeType);
        //        String url = randomIp();
        // String url = getrepurl(2).getUrl();
        t1 = new MyThreadQuery(orderid, ordersuccesstime, tongchengcallbackurl, queryno, freeType, Taobao_TrainCallBack);
        pool.execute(t1);
        WriteLog.write("JobQueryMyOrder",
                orderid + ":审核次数===>" + queryno + ":本次审核订单结束用时===>" + (System.currentTimeMillis() - l1));
        pool.shutdown();
    }

    //    public void queryMyOrderAll() {
    //        String tongchengcallbackurl = getSysconfigString("tcTrainCallBack");//同程回调地址
    //        ExecutorService pool = Executors.newFixedThreadPool(100);
    //        Long l1 = System.currentTimeMillis();
    //        Thread t1 = null;
    //        String sql = " SELECT O.ID FROM T_TRAINORDER O JOIN  T_TRAINREFINFO TF ON O.ID = TF.C_ORDERID WHERE O.C_ORDERSTATUS=3 AND "
    //                + " ((O.C_PAYSUPPLYSTATUS =1 AND TF.C_PAYSUCCESSTIME <'"
    //                + getdateTimp(false)
    //                + "') OR (O.C_PAYSUPPLYSTATUS =0 AND TF.C_PAYSUCCESSTIME <'" + getdateTimp(true) + "'))";
    //
    //        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
    //        WriteLog.write("JobQueryMyOrder", l1 + ":待审核订单数:" + list.size());
    //        for (int i = 0; i < list.size(); i++) {
    //            Map map = (Map) list.get(i);
    //            long orderid = Long.valueOf(map.get("ID").toString());
    //            String url = randomIp();
    //            t1 = new MyThreadQuery(url, orderid, tongchengcallbackurl);
    //            pool.execute(t1);
    //        }
    //        WriteLog.write("JobQueryMyOrder", l1 + ":审核订单本次结束用时:" + (System.currentTimeMillis() - l1));
    //        pool.shutdown();
    //    }

    /**
     * 说明:获取当前时间的Timestamp
     * @return  当前时间
     * @time 2014年8月30日 上午10:59:27
     * @author yinshubin
     */
    public Timestamp getendtTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 当天0点时间Timestamp
     * @return
     * @time 2014年12月24日 上午9:57:59
     * @author fiend
     */
    public Timestamp getStartDayTime() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dangri = sdf.format(new Date()) + " 00:00:00";
            Timestamp t = new Timestamp(sdf1.parse(dangri).getTime());
            return t;
        }
        catch (ParseException e) {
        }
        return new Timestamp(new Date().getTime());
    }

    /**
     * 获取当前时间前5/10分钟的时间戳
     * @param ismin5   true 5分钟   ---false 10分钟
     * @return
     * @time 2015年1月2日 下午2:08:35
     * @author fiend
     */
    public String getdateTimp(boolean ismin5) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        Date afterDate = new Date(now.getTime() - 600000);
        if (ismin5) {
            afterDate = new Date(now.getTime() - 300000);
        }
        return sdf.format(afterDate);
    }

    //    public static void main(String[] args) {
    //        new JobQueryMyOrder().queryMyOrderAll();
    //    }
}
