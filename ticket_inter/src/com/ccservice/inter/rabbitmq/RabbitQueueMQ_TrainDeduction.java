package com.ccservice.inter.rabbitmq;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;
import com.ccservice.mq.consumer.ConsumerMaintenance;
import com.ccservice.mq.consumer.RabbitQueueConsumer;
import com.ccservice.train.qunar.TrainorderDeduction;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.MQPlatformTypeEnum;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.PublicConnectionInfos;
import com.ccservice.util.mq.util_mq.rabbitmq.bean.RabbitmqBean;

/**
 * 
 * @author zlx
 * @time 2016年11月18日 上午9:38:53
 * @Description TODO
 *  美团云MQ 扣款队列
 */
public class RabbitQueueMQ_TrainDeduction extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("Rabbit扣款队列----------开启");
        dudectionNtice();
    }

    private void dudectionNtice() {
        String kt110 = PropertyUtil.getValue("kt110", "rabbitMQ.properties");//空铁|同程 的【消费者TOMCAT编号】 
        int pingtaiType = Integer.parseInt(PropertyUtil.getValue("pingTaiType", "rabbitMQ.properties"));//平台类型 1=空铁 2=同程
        int consumerType = PublicConnectionInfos.getConsumerType(PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION);//消费者类型0代表测试 1-13代表自己的业务 1代表下单消费者
        WriteLog.write("消费者启动记录", this.getClass().getSimpleName() + ":kt110:" + kt110 + ":queueName:"
                + PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION + ":pingtaiType:" + pingtaiType + ":consumerType:"
                + consumerType);
        System.out.println("扣款队列【" + PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION + "】【consumerType:" + consumerType
                + "】【kt110:" + kt110 + "】RabbitMQ-----====平台=" + pingtaiType + "【空铁1,同程2】 ");
        //初始化参数结束
        //监控开始
        new ConsumerMaintenance(PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION,
                MQPlatformTypeEnum.getMQPlatformTypeEnumByType(pingtaiType), kt110) {

            @Override
            public void addCustomerCount(int count, RabbitmqBean rabbitmqBean) {
                // TODO Auto-generated method stub
                String pingtaiString = String.valueOf(pingtaiType);
                for (int i = 0; i < count; i++) {

                    new RabbitQueueConsumer(PublicConnectionInfos.RABBIT_QUEUEMQ_DEDUCTION, pingtaiString,
                            rabbitmqBean.getHost(), rabbitmqBean.getUsername(), rabbitmqBean.getPassword(),
                            "RabbitQueueMQ_TrainDeduction") {

                        @Override
                        public String execMethod(String notice) {
                            // TODO Auto-generated method stub
                            System.out.println("获取消息:" + notice);
                            WriteLog.write("RabbitQueueMQ_TrainDeduction", System.currentTimeMillis() + ":"
                                    + "orderNoticeResult:" + notice);
                            JSONObject jsonobject = JSONObject.parseObject(notice);
                            long orderid = jsonobject.containsKey("orderid") ? jsonobject.getLongValue("orderid") : 0;
                            int type = jsonobject.containsKey("type") ? jsonobject.getIntValue("type") : 0;
                            //处理方法
                            new TrainorderDeduction(orderid, type).tongchengDeduction();
                            WriteLog.write("RabbitQueueMQ_TrainDeduction", System.currentTimeMillis() + ":"
                                    + "orderid:" + orderid + "type:" + type);
                            return notice + "--->扣款结束";
                        }
                    }.start();

                }

            }
        }.start();
        //监控结束
    }

    private void dudectionNtice_back() {
        String queName = PropertyUtil.getValue("Rabbit_QueueMQ_Deduction", "rabbitMQ.properties");
        int type = 1;
        try {
            int threadNum = Integer.parseInt(getInitParameter("threadNum"));
            for (int i = 0; i < threadNum; i++) {
                new RabbitQueueConsumerTrainDeductionV3(queName, type).start();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
