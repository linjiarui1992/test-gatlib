package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class Yi9eBook {
	// 以下是19E接口需要参数
	private String agentcode;// 19E用户名
	private String safecode;// 19E安全码
	private String notifiedUrl;
	private String paymentReturnUrl;
	private String b2cCreatorCn;// webclient createuser;
	private String ispay = "0";// 代扣 1自动 0不自动
	private String staus = "0";// 接口是否启用 1,启用 0,禁用
	private String tuifeiurl;// 退费订单通知URL
	private String password;
	private String updatetime;
	private String lastid;

	public Yi9eBook() {
		List<Eaccount> listEaccount19e = new ArrayList<Eaccount>();
		Eaccount e = Server.getInstance().getSystemService().findEaccount(15);
		if (e != null && e.getName() != null) {
			listEaccount19e.add(e);
		}
		if (listEaccount19e.size() > 0) {
			agentcode = listEaccount19e.get(0).getUsername();
			safecode = listEaccount19e.get(0).getPwd();
			notifiedUrl = listEaccount19e.get(0).getNourl();
			paymentReturnUrl = listEaccount19e.get(0).getPayurl();
			ispay = listEaccount19e.get(0).getIspay();
			tuifeiurl = listEaccount19e.get(0).getUrl();
			password = listEaccount19e.get(0).getPassword();
			staus = listEaccount19e.get(0).getState();
		} else {
			staus = "0";
			System.out.println("NO-19E");
		}
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getLastid() {
		return lastid;
	}

	public void setLastid(String lastid) {
		this.lastid = lastid;
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}

	public String getSafecode() {
		return safecode;
	}

	public void setSafecode(String safecode) {
		this.safecode = safecode;
	}

	public String getNotifiedUrl() {
		return notifiedUrl;
	}

	public void setNotifiedUrl(String notifiedUrl) {
		this.notifiedUrl = notifiedUrl;
	}

	public String getPaymentReturnUrl() {
		return paymentReturnUrl;
	}

	public void setPaymentReturnUrl(String paymentReturnUrl) {
		this.paymentReturnUrl = paymentReturnUrl;
	}

	public String getB2cCreatorCn() {
		return b2cCreatorCn;
	}

	public void setB2cCreatorCn(String creatorCn) {
		b2cCreatorCn = creatorCn;
	}

	public String getIspay() {
		return ispay;
	}

	public void setIspay(String ispay) {
		this.ispay = ispay;
	}

	public String getStaus() {
		return staus;
	}

	public void setStaus(String staus) {
		this.staus = staus;
	}

	public String getTuifeiurl() {
		return tuifeiurl;
	}

	public void setTuifeiurl(String tuifeiurl) {
		this.tuifeiurl = tuifeiurl;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
