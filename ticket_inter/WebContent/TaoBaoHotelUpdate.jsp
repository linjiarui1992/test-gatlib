<%@page pageEncoding="utf-8"%>
<%@page contentType="text/html; charset=utf-8"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="com.ccservice.taobao.hotelupdate.SyncUpdateTaoBaoData"%>
<%
	//淘宝数据更新
	try{
	    //Request Data
	    BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = "";
        StringBuffer buf = new StringBuffer(1024);
        while ((line = br.readLine()) != null) {
        	buf.append(line);
        }
        String param = buf.toString();
	    //Class Operate
	    SyncUpdateTaoBaoData.sync(param);
	}catch (Exception e) {
	    String msg = e.getMessage();
        String ret = "出错了，错误信息为：" + (msg == null ? "空" : msg.trim());
        StackTraceElement stack = e.getStackTrace()[0];
        if (stack != null) {
            ret += "；异常类：" + stack.getFileName() + " ；方法： " + stack.getMethodName() + " ；行数： " + stack.getLineNumber();
        }
        System.out.println(ret);
	}
%>