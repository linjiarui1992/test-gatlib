package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;

import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class JinRiBook {

    // 以下是今日天下通接口需要参数
    private String JRURL = "";// 今日url

    private String JRUSER = "";// 今日账号

    private String JRUSER_xiaji = "";// 今日账号下级账号

    private String JRKey = "";// 今日账号下级账号

    private String staus = "0";// 接口是否启用 1,启用 0,禁用

    private String JRXiausername = "";

    private String JROFFICEID = "";

    public JinRiBook() {
        List<Eaccount> listEaccountJinri = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(1);
        if (e != null && e.getName() != null) {
            listEaccountJinri.add(e);
        }
        if (listEaccountJinri.size() > 0) {
            JRURL = listEaccountJinri.get(0).getUrl();
            JRUSER = listEaccountJinri.get(0).getUsername();
            JRUSER_xiaji = listEaccountJinri.get(0).getXiausername();
            JRKey = listEaccountJinri.get(0).getKeystr();
            JRXiausername = listEaccountJinri.get(0).getXiausername();
            JROFFICEID = listEaccountJinri.get(0).getEdesc();
            staus = listEaccountJinri.get(0).getState();
        }
        else {
            staus = "0";
            System.out.println("NO-jinri");
        }

    }

    public String getJRURL() {
        return JRURL;
    }

    public void setJRURL(String jrurl) {
        JRURL = jrurl;
    }

    public String getJRUSER() {
        return JRUSER;
    }

    public void setJRUSER(String jruser) {
        JRUSER = jruser;
    }

    public String getJRUSER_xiaji() {
        return JRUSER_xiaji;
    }

    public void setJRUSER_xiaji(String jruser_xiaji) {
        JRUSER_xiaji = jruser_xiaji;
    }

    public String getJRKey() {
        return JRKey;
    }

    public void setJRKey(String key) {
        JRKey = key;
    }

    public String getStaus() {
        return staus;
    }

    public void setStaus(String staus) {
        this.staus = staus;
    }

    public String getJRXiausername() {
        return JRXiausername;
    }

    public void setJRXiausername(String xiausername) {
        JRXiausername = xiausername;
    }

    public String getJROFFICEID() {
        return JROFFICEID;
    }

    public void setJROFFICEID(String jrofficeid) {
        JROFFICEID = jrofficeid;
    }

}
