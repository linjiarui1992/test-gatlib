package com.ccservice.t12306.method.reg;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import com.ccservice.inter.job.WriteLog;
import java.security.cert.X509Certificate;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * HTTPS POST/GET 
 * @time 2014年8月30日 下午3:01:10
 * @author yinshubin
 */
public class HttpsUtil {

    private static class TrustAnyTrustManager implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[] {};
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    /**
     * https post方式请求服务器
     * @param url
     * @param content
     * @param charset
     * @param cookiestring
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:58:26
     * @author yinshubin
     */
    public static String post(String url, String content, String charset, String cookiestring)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = new StringBuffer();
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
        URL console = new URL(url);
        URLConnection con = console.openConnection();
        con.addRequestProperty("JSESSIONID", cookiestring);
        con.addRequestProperty("Cookie", cookiestring);
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseMessage.toString();
    }

    public static String get(String url, String cookiestring, String charset) throws Exception {
        //SSL
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
        //RETURN
        StringBuffer res = new StringBuffer();
        InputStream in = null;
        HttpsURLConnection con = null;
        BufferedReader reader = null;
        try {
            con = (HttpsURLConnection) (new URL(url)).openConnection();
            con.setDoOutput(true);
            con.setUseCaches(false);
            if (cookiestring != null && !"".equals(cookiestring.trim())) {
                con.addRequestProperty("JSESSIONID", cookiestring);
                con.addRequestProperty("Cookie", cookiestring);
            }
            con.setSSLSocketFactory(sc.getSocketFactory());
            con.setHostnameVerifier(new TrustAnyHostnameVerifier());
            con.connect();
            in = con.getInputStream();
            //判断是否压缩
            String ContentEncoding = con.getHeaderField("Content-Encoding");
            if ("gzip".equalsIgnoreCase(ContentEncoding)) {
                reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(in), charset));
            }
            else {
                reader = new BufferedReader(new InputStreamReader(in, charset));
            }
            String lineTxt = null;
            while ((lineTxt = reader.readLine()) != null) {
                res.append(lineTxt);
            }
        }
        catch (Exception e) {
            res = new StringBuffer();
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            }
            catch (Exception e) {
            }
        }
        return res.toString();
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post12306(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            //            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            //            try {
            //                String cookiestring = getCookie(s.toString());
            //                WriteLog.write("train12306_order", "访问gateway.do获取的Set-Cookie:" + s);
            //                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            //            }
            //            catch (Exception e) {
            //            }
        }
        return responseMessage.toString();
    }

    /**
     * https 获取支付cookie
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post12306pay(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            try {
                String cookiestring = getCookie(s.toString());
                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            }
            catch (Exception e) {
            }
        }
        return responseMessage.toString();
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post12306cookie(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            //            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //            byte[] buffer = new byte[1024];
            //            int len = 0;
            //            while ((len = is.read(buffer)) != -1) {
            //                outStream.write(buffer, 0, len);
            //            }
            //            is.close();

            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            try {
                String cookiestring = getCookie(s.toString());
                WriteLog.write("train12306_order", "访问gateway.do获取的Set-Cookie:" + s);
                return responseMessage.toString() + "拿到新Cookie" + cookiestring;
            }
            catch (Exception e) {
            }
        }
        return responseMessage.toString();
    }

    /**
     * https post方式请求服务器。传入MAP
     * @param url
     * @param content
     * @param charset
     * @param requestproperty
     * @return
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws IOException
     * @time 2014年8月30日 下午2:57:46
     * @author yinshubin
     */
    public static String post2(String url, String content, String charset, Map<String, String> requestproperty)
            throws NoSuchAlgorithmException, KeyManagementException, IOException {
        StringBuffer responseMessage = null;
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

        URL console = new URL(url);
        URLConnection con = console.openConnection();
        //        con.addRequestProperty("JSESSIONID", cookiestring);
        //        con.addRequestProperty("Cookie", cookiestring);
        Iterator iter = requestproperty.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
            con.addRequestProperty(entry.getKey(), entry.getValue());
        }
        HttpsURLConnection conn = (HttpsURLConnection) con;
        conn.setSSLSocketFactory(sc.getSocketFactory());
        conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
        conn.setDoOutput(true);
        conn.connect();
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.write(content.getBytes(charset));
        // 刷新、关闭
        out.flush();
        out.close();
        InputStream is = conn.getInputStream();
        if (is != null) {
            //            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //            byte[] buffer = new byte[1024];
            //            int len = 0;
            //            while ((len = is.read(buffer)) != -1) {
            //                outStream.write(buffer, 0, len);
            //            }
            //            is.close();

            int charCount = -1;
            BufferedReader br = null;
            responseMessage = new StringBuffer();
            String ContentEncoding = conn.getHeaderField("Content-Encoding");
            if ("gzip".equals(ContentEncoding)) {
                br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
            }
            else {
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            }
            try {
                while ((charCount = br.read()) != -1) {
                    responseMessage.append((char) charCount);
                }
            }
            catch (Exception e) {
            }
        }
        return responseMessage.toString();
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:27
     * @author yinshubin
     */
    public static String Get(String strUrl) {
        URLConnection connection = null;
        BufferedReader reader = null;
        String str = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String lines;
            StringBuffer linebuff = new StringBuffer("");
            try {
                while ((lines = reader.readLine()) != null) {
                    linebuff.append(lines);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            str = linebuff.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:27
     * @author yinshubin
     */
    public static String Get_Cookie(String strUrl) {
        //        System.out.println(strUrl);
        URLConnection connection = null;
        BufferedReader reader = null;
        String str = null;
        String cookiestring = "";
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            cookiestring = getCookielogin(s.toString());
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String lines;
            StringBuffer linebuff = new StringBuffer("");
            try {
                while ((lines = reader.readLine()) != null) {
                    linebuff.append(lines);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            str = linebuff.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str + "cookiestring" + cookiestring;
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:27
     * @author yinshubin
     * @param proxy 
     * @param proxyPort 
     * @param proxyHost 
     */
    public static String Get_ProxyCookie(String strUrl, Proxy proxy) {
        //        System.out.println(strUrl);
        URLConnection connection = null;
        BufferedReader reader = null;
        String str = null;
        String cookiestring = "";
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
            URL url = new URL(strUrl);
            connection = url.openConnection(proxy);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            List<String> s = conn.getHeaderFields().get("Set-Cookie");
            cookiestring = getCookielogin(s.toString());
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String lines;
            StringBuffer linebuff = new StringBuffer("");
            try {
                while ((lines = reader.readLine()) != null) {
                    linebuff.append(lines);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            str = linebuff.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                reader.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str + "cookiestring" + cookiestring;
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:27
     * @author yinshubin
     */
    //    public static String Get12306(String strUrl, Map<String, String> requestproperty) {
    //        URLConnection connection = null;
    //        BufferedReader reader = null;
    //        String str = null;
    //        try {
    //            SSLContext sc = SSLContext.getInstance("SSL");
    //            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
    //
    //            URL url = new URL(strUrl);
    //            connection = url.openConnection();
    //            connection.setDoInput(true);
    //            connection.setDoOutput(false);
    //            Iterator iter = requestproperty.entrySet().iterator();
    //            while (iter.hasNext()) {
    //                Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
    //                connection.addRequestProperty(entry.getKey(), entry.getValue());
    //            }
    //            HttpsURLConnection conn = (HttpsURLConnection) connection;
    //            conn.setSSLSocketFactory(sc.getSocketFactory());
    //
    //            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
    //            conn.setDoOutput(true);
    //            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    //            String lines;
    //            StringBuffer linebuff = new StringBuffer("");
    //            try {
    //                while ((lines = reader.readLine()) != null) {
    //                    linebuff.append(lines);
    //                }
    //            }
    //            catch (Exception e) {
    //                e.printStackTrace();
    //            }
    //            str = linebuff.toString();
    //        }
    //        catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //        finally {
    //            try {
    //                reader.close();
    //            }
    //            catch (Exception e) {
    //                e.printStackTrace();
    //            }
    //        }
    //        return str;
    //    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交,变体
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:53
     * @author yinshubin
     */
    public static String Get12306(String strUrl, Map<String, String> requestproperty) {
        StringBuffer responseMessage = null;
        URLConnection connection = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            Iterator iter = requestproperty.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) iter.next();
                connection.addRequestProperty(entry.getKey(), entry.getValue());
            }
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            InputStream is = conn.getInputStream();
            if (is != null) {
                int charCount = -1;
                BufferedReader br = null;
                responseMessage = new StringBuffer();
                String ContentEncoding = conn.getHeaderField("Content-Encoding");
                if ("gzip".equals(ContentEncoding)) {
                    br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
                }
                else {
                    br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                }
                try {
                    while ((charCount = br.read()) != -1) {
                        responseMessage.append((char) charCount);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responseMessage.toString();
    }

    /**
     * java.net实现 HTTP或HTTPs GET方法提交,变体
     * @param strUrl
     * @return
     * @time 2014年8月30日 下午2:59:53
     * @author yinshubin
     */
    public static String Get1(String strUrl) {
        StringBuffer responseMessage = null;
        URLConnection connection = null;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());

            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            InputStream is = conn.getInputStream();
            if (is != null) {
                int charCount = -1;
                BufferedReader br = null;
                responseMessage = new StringBuffer();
                String ContentEncoding = conn.getHeaderField("Content-Encoding");
                if ("gzip".equals(ContentEncoding)) {
                    br = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), "UTF-8"));
                }
                else {
                    br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                }
                try {
                    while ((charCount = br.read()) != -1) {
                        responseMessage.append((char) charCount);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return responseMessage.toString();
    }

    /**
     * java.net实现 HTTP或HTTPs GET  
     * 指定下载路径和文件
     *        如果验证码大小小于1KB      返回-1
     * @param strUrl
     * @param dirPath
     * @param filePath
     * @param cookiestring
     * @return
     * @time 2014年8月30日 下午3:00:22
     * @author yinshubin
     */
    public static String Get_picture(String strUrl, String dirPath, String filePath, String cookiestring) {
        boolean isRegist = "1".equals(cookiestring);
        WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:参数:" + strUrl + "(:)" + dirPath + "(:)" + filePath + "(:)"
                + cookiestring);
        URLConnection connection = null;
        String Encryption = "";
        String Encryptionstr = "";
        String before = cookiestring;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
            if ("".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/login/init";
                String a = Get_Cookie(url_before);
                Encryption = a.split("cookiestring")[0];
                cookiestring = a.split("cookiestring")[1];
                try {
                    Encryptionstr = Train12306util.getEncryption(Encryption, Train12306util.LOGIN, cookiestring);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if ("1".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/regist/init";
                //                String a = Get(url_before);
                //                cookiestring = "";
                String a = Get_Cookie(url_before);
                cookiestring = a.split("cookiestring")[1];
            }

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.addRequestProperty("JSESSIONID", cookiestring);
            connection.addRequestProperty("Cookie", cookiestring);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.connect();
            InputStream in = conn.getInputStream();
            if (!DownloadPictureTest.savePicToDiskBool(in, dirPath, filePath, isRegist)) {
                return "-1";
            }
            if ("".equals(before) || "-1".equals(before) || "1".equals(before)) {
                List<String> s = conn.getHeaderFields().get("Set-Cookie");
                for (String str : s) {
                    cookiestring += "; " + getCookie(str.toString().replace("; Path=/", ""));
                }
                cookiestring += Encryptionstr;
                WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:获取验证码附带cookie:" + cookiestring);
                return cookiestring;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        }
        return cookiestring;
    }

    /**
     * java.net实现 HTTP或HTTPs GET  
     * 指定下载路径和文件
     *        如果验证码大小小于1KB      返回-1
     * @param strUrl
     * @param dirPath
     * @param filePath
     * @param cookiestring
     * @return
     * @time 2014年8月30日 下午3:00:22
     * @author yinshubin
     */
    public static String Get_pictureProxy(String strUrl, String dirPath, String filePath, String cookiestring,
            String proxyHost, int proxyPort) {
        // 使用java.net.Proxy类设置代理IP
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        boolean isRegist = "1".equals(cookiestring);
        WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:参数:" + strUrl + "(:)" + dirPath + "(:)" + filePath + "(:)"
                + cookiestring);
        URLConnection connection = null;
        String Encryption = "";
        String Encryptionstr = "";
        String before = cookiestring;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
            if ("".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/login/init";
                String a = Get_ProxyCookie(url_before, proxy);
                Encryption = a.split("cookiestring")[0];
                cookiestring = a.split("cookiestring")[1];
                try {
                    Encryptionstr = Train12306util.getEncryption(Encryption, Train12306util.LOGIN, cookiestring);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if ("1".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/regist/init";
                //                String a = Get(url_before);
                //                cookiestring = "";
                String a = Get_ProxyCookie(url_before, proxy);
                cookiestring = a.split("cookiestring")[1];
            }

            URL url = new URL(strUrl);
            connection = url.openConnection(proxy);
            connection.addRequestProperty("JSESSIONID", cookiestring);
            connection.addRequestProperty("Cookie", cookiestring);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.connect();
            InputStream in = conn.getInputStream();
            if (!DownloadPictureTest.savePicToDiskBool(in, dirPath, filePath, isRegist)) {
                return "-1";
            }
            if ("".equals(before) || "-1".equals(before) || "1".equals(before)) {
                List<String> s = conn.getHeaderFields().get("Set-Cookie");
                for (String str : s) {
                    cookiestring += "; " + getCookie(str.toString().replace("; Path=/", ""));
                }
                cookiestring += Encryptionstr;
                WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:获取验证码附带cookie:" + cookiestring);
                return cookiestring;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        }
        return cookiestring;
    }

    /**
     * java.net实现 HTTP或HTTPs GET  
     * 指定下载路径和文件
     *        如果验证码大小小于1KB      返回-1
     * @param strUrl
     * @param dirPath
     * @param filePath
     * @param cookiestring
     * @return
     * @time 2014年8月30日 下午3:00:22
     * @author yinshubin
     */
    public static String Get_picture_cxxd(String strUrl, String dirPath, String filePath, String cookiestring) {
        boolean isRegist = "1".equals(cookiestring);
        WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:参数:" + strUrl + "(:)" + dirPath + "(:)" + filePath + "(:)"
                + cookiestring);
        URLConnection connection = null;
        String Encryption = "";
        String before = cookiestring;
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { new TrustAnyTrustManager() }, new java.security.SecureRandom());
            if ("".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/login/init";
                String a = Get_Cookie(url_before);
                Encryption = a.split("cookiestring")[0];
                cookiestring = a.split("cookiestring")[1];
            }
            else if ("1".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/regist/init";
                //                String a = Get(url_before);
                //                cookiestring = "";
                String a = Get_Cookie(url_before);
                cookiestring = a.split("cookiestring")[1];
            }

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.addRequestProperty("JSESSIONID", cookiestring);
            connection.addRequestProperty("Cookie", cookiestring);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpsURLConnection conn = (HttpsURLConnection) connection;
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.connect();
            InputStream in = conn.getInputStream();
            DownloadPictureTest.savePicToDiskBool(in, dirPath, filePath, isRegist);
            if ("".equals(before) || "-1".equals(before) || "1".equals(before)) {
                List<String> s = conn.getHeaderFields().get("Set-Cookie");
                for (String str : s) {
                    cookiestring += "; " + getCookie(str.toString().replace("; Path=/", ""));
                }
                return cookiestring + "|" + dirPath + filePath;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        }
        return cookiestring;
    }

    /**
     * java.net实现 HTTP或HTTPs GET  
     * 指定下载路径和文件
     *        如果验证码大小小于1KB      返回-1
     * @param strUrl
     * @param dirPath
     * @param filePath
     * @param cookiestring
     * @return
     * @time 2014年8月30日 下午3:00:22
     * @author yinshubin
     */
    public static String Get_pictureHttp(String strUrl, String dirPath, String filePath, String cookiestring) {
        boolean isRegist = "1".equals(cookiestring);
        WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:参数:" + strUrl + "(:)" + dirPath + "(:)" + filePath + "(:)"
                + cookiestring);
        URLConnection connection = null;
        String Encryption = "";
        String Encryptionstr = "";
        String before = cookiestring;
        try {
            if ("".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/login/init";
                String a = Get_Cookie(url_before);
                Encryption = a.split("cookiestring")[0];
                cookiestring = a.split("cookiestring")[1];
                try {
                    Encryptionstr = Train12306util.getEncryption(Encryption, Train12306util.LOGIN, cookiestring);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if ("1".equals(cookiestring)) {
                String url_before = "https://kyfw.12306.cn/otn/regist/init";
                //                String a = Get(url_before);
                //                cookiestring = "";
                String a = Get_Cookie(url_before);
                cookiestring = a.split("cookiestring")[1];
            }

            URL url = new URL(strUrl);
            connection = url.openConnection();
            connection.addRequestProperty("JSESSIONID", cookiestring);
            connection.addRequestProperty("Cookie", cookiestring);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            HttpURLConnection conn = (HttpURLConnection) connection;
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.connect();
            InputStream in = conn.getInputStream();
            if (!DownloadPictureTest.savePicToDiskBool(in, dirPath, filePath, isRegist)) {
                return "-1";
            }
            if ("".equals(before) || "-1".equals(before) || "1".equals(before)) {
                List<String> s = conn.getHeaderFields().get("Set-Cookie");
                for (String str : s) {
                    cookiestring += "; " + getCookie(str.toString().replace("; Path=/", ""));
                }
                cookiestring += Encryptionstr;
                WriteLog.write("HttpsUtil_Get_picture", "HttpsUtil:获取验证码附带cookie:" + cookiestring);
                return cookiestring;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        }
        return cookiestring;
    }

    /**
     * 说明：拼接COOKIE
     * @param str
     * @return
     * @time 2014年8月30日 下午2:57:08
     * @author yinshubin
     */
    public static String getCookie(String str) {
        if (str.contains("BIGipServerotn")) {
            String[] strs = str.split("; path=/,");
            strs[0] = strs[0].substring(1, strs[0].length());
            if (strs.length > 1) {
                if (strs[1].contains("current_captcha_type")) {
                    String[] strs2 = strs[1].split("; Path=/,");
                    String[] strs3 = strs2[1].split(";");
                    str = strs3[0] + "; " + strs[0] + "; " + strs2[0];
                }
                else {
                    String[] strs2 = strs[1].split(";");
                    strs2[0] = strs2[0].substring(1, strs2[0].length());
                    str = strs2[0] + "; " + strs[0];
                }
            }
        }
        else if (str.contains("JSESSIONID")) {
            //            [JSESSIONID=9EF9F5BE7618AF2E207FD54C95CC3B9D; Path=/otn]
            String[] strs = str.split(";");
            str = strs[0].substring(1, strs[0].length());
        }
        return str;
    }

    /**
     * 说明：拼接COOKIE
     * @param str
     * @return
     * @time 2014年8月30日 下午2:57:08
     * @author yinshubin
     */
    public static String getCookielogin(String str) {
        //        System.out.println("str::::" + str);
        if (str.contains("BIGipServerotn")) {
            String[] strs = str.split(";");

            return strs[1].split(",")[1].trim() + "; " + strs[0].substring(1, strs[0].length());
            //            return str.split("Path=/otn")[0].trim() + " " + str.split("Path=/otn")[1].trim().split("; path=/")[0];
        }
        //        else if (str.contains("JSESSIONID")) {
        //            //            [JSESSIONID=9EF9F5BE7618AF2E207FD54C95CC3B9D; Path=/otn]
        //            String[] strs = str.split(";");
        //            str = strs[0].substring(1, strs[0].length());
        //        }
        return str;
    }

    /**
     * 
     * @return
     * @time 2014年12月18日 上午11:49:04
     * @author fiend
     */
    public static Map<String, String> Spelling12306MapGet(boolean accept, String referer, String cookie) {
        Map<String, String> header = new HashMap<String, String>();
        if (accept) {
            header.put("Accept", "application/json, text/javascript, */*; q=0.01");
        }
        else {
            header.put("Accept", "*/*");
        }
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        header.put("Cache-Control", "no-cache");
        header.put("Connection", "keep-alive");
        if (cookie != null && !cookie.trim().equals("")) {
            header.put("Cookie", cookie);
        }
        header.put("Host", "kyfw.12306.cn");
        header.put("If-Modified-Since", "0");
        if (referer != null && !referer.trim().equals("")) {
            header.put("Referer", referer);
        }
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:33.0) Gecko/20100101 Firefox/33.0");
        return header;
    }

    /**
     * @param accept 是否是"※/※"
    * @param contentlength 表单的长度
    * @param contenttype 是否使用表单
    * @param referer 来源
    * @param cookie cookie
    * @return
    * @time 2014年12月18日 上午11:49:04
    * @author fiend
    */
    public static Map<String, String> Spelling12306Map(boolean accept, String contentlength, boolean contenttype,
            String referer, String cookie) {
        Map<String, String> header = new HashMap<String, String>();
        if (accept) {
            header.put("Accept", "application/json, text/javascript, */*; q=0.01");
        }
        else {
            header.put("Accept", "*/*");
        }
        header.put("Accept-Encoding", "gzip, deflate");
        header.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        header.put("Cache-Control", "no-cache");
        header.put("Connection", "keep-alive");
        if (contentlength != null && !contentlength.trim().equals("")) {
            header.put("Content-Length", contentlength);
        }
        if (contenttype) {
            header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        }
        if (cookie != null && !cookie.trim().equals("")) {
            header.put("Cookie", cookie);
        }
        header.put("Host", "kyfw.12306.cn");
        header.put("If-Modified-Since", "0");
        if (referer != null && !referer.trim().equals("")) {
            header.put("Referer", referer);
        }
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko");
        header.put("X-Requested-With", "XMLHttpRequest");
        return header;
    }

    public static void main(String[] args) {
        //        String par = "1";
        //        String cookie = "2";
        //        Map<String, String> hmap = HttpsUtil.Spelling12306Map(false, null, false,
        //                "https://kyfw.12306.cn/otn/leftTicket/init", cookie);
        //        System.out.println(hmap.size());
        //        String coo = "JSESSIONID=AFA8156DA43E48E8F8CF6F11F1FA17A1; Path=/otn BIGipServerotn=735510794.38945.0000; path=/";
        //        System.out.println(
        //                coo.split("Path=/otn")[0].trim() + " " + coo.split("Path=/otn")[1].trim().split("path=/")[0].trim());
        Get_Cookie("https://kyfw.12306.cn/otn/regist/init");
    }
}
