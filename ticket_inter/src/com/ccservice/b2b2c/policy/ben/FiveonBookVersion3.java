package com.ccservice.b2b2c.policy.ben;

import java.util.ArrayList;
import java.util.List;
import com.ccservice.b2b2c.base.eaccount.Eaccount;
import com.ccservice.inter.server.Server;

public class FiveonBookVersion3 {
    // 以下是51book(v3.0)接口需要参数
    private String fiveoneUsernameV3;// 51book(v3.0)用户名

    private String fiveonePasswordV3;// 51book(v3.0)安全吗

    private String fiveoneStatusV3;// 51book(v3.0)账号状态 0 禁用 1 启用

    private String fiveoneLinknameV3;// 51book(v3.0) 联系人

    private String fiveoneLinkphoneV3;// 51book(v3.0) 联系人电话

    private String fiveoneTicketNotifiedUrlV3;// 51book(v3.0) 出票通知url

    private String fiveonePaymentReturnUrlV3;// 51book(v3.0) 支付完成后返回的url地址

    public FiveonBookVersion3() {
        List<Eaccount> listEaccount51bookV3 = new ArrayList<Eaccount>();
        Eaccount e = Server.getInstance().getSystemService().findEaccount(12);
        if (e != null && e.getName() != null) {
            listEaccount51bookV3.add(e);
        }
        if (listEaccount51bookV3.size() > 0) {
            fiveoneUsernameV3 = listEaccount51bookV3.get(0).getUsername();
            fiveonePasswordV3 = listEaccount51bookV3.get(0).getPassword();
            fiveoneStatusV3 = listEaccount51bookV3.get(0).getState();
            fiveoneLinknameV3 = listEaccount51bookV3.get(0).getCreateuser();
            fiveoneLinkphoneV3 = listEaccount51bookV3.get(0).getKeystr();
            fiveoneTicketNotifiedUrlV3 = listEaccount51bookV3.get(0).getNourl();
            fiveonePaymentReturnUrlV3 = listEaccount51bookV3.get(0).getPayurl();
        }
        else {
            fiveoneStatusV3 = "0";
            System.out.println("NO-51book");
        }
    }

    public String getFiveoneUsernameV3() {
        return fiveoneUsernameV3;
    }

    public void setFiveoneUsernameV3(String fiveoneUsernameV3) {
        this.fiveoneUsernameV3 = fiveoneUsernameV3;
    }

    public String getFiveonePasswordV3() {
        return fiveonePasswordV3;
    }

    public void setFiveonePasswordV3(String fiveonePasswordV3) {
        this.fiveonePasswordV3 = fiveonePasswordV3;
    }

    public String getFiveoneStatusV3() {
        return fiveoneStatusV3;
    }

    public void setFiveoneStatusV3(String fiveoneStatusV3) {
        this.fiveoneStatusV3 = fiveoneStatusV3;
    }

    public String getFiveoneLinknameV3() {
        return fiveoneLinknameV3;
    }

    public void setFiveoneLinknameV3(String fiveoneLinknameV3) {
        this.fiveoneLinknameV3 = fiveoneLinknameV3;
    }

    public String getFiveoneLinkphoneV3() {
        return fiveoneLinkphoneV3;
    }

    public void setFiveoneLinkphoneV3(String fiveoneLinkphoneV3) {
        this.fiveoneLinkphoneV3 = fiveoneLinkphoneV3;
    }

    public String getFiveoneTicketNotifiedUrlV3() {
        return fiveoneTicketNotifiedUrlV3;
    }

    public void setFiveoneTicketNotifiedUrlV3(String fiveoneTicketNotifiedUrlV3) {
        this.fiveoneTicketNotifiedUrlV3 = fiveoneTicketNotifiedUrlV3;
    }

    public String getFiveonePaymentReturnUrlV3() {
        return fiveonePaymentReturnUrlV3;
    }

    public void setFiveonePaymentReturnUrlV3(String fiveonePaymentReturnUrlV3) {
        this.fiveonePaymentReturnUrlV3 = fiveonePaymentReturnUrlV3;
    }
}
