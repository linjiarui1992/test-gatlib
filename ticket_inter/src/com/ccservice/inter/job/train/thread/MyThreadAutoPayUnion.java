package com.ccservice.inter.job.train.thread;

import java.util.List;

import net.sf.json.JSONObject;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.policy.TimeUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc
 * 银联自动支付处理类
 *
 */
public class MyThreadAutoPayUnion extends Thread {

    private Trainorder trainorder;

    private String i;

    private int t;//随机数据

    private String unionpayurl;

    private String tcTrainCallBack;

    public MyThreadAutoPayUnion(Trainorder trainorder, String i, int t, String unionpayurl, String tcTrainCallBack) {
        this.trainorder = trainorder;
        this.i = i;
        this.t = t;
        this.unionpayurl = unionpayurl;
        this.tcTrainCallBack = tcTrainCallBack;
    }

    @Override
    public void run() {
        WriteLog.write("12306银联自动支付用时", t + ":" + trainorder.getOrdernumber());
        autoUnionPay(trainorder, i);
        WriteLog.write("12306银联自动支付用时", t + ":" + trainorder.getOrdernumber());
    }

    public static void main(String[] args) {
        for (int i = 0; i < 50; i++) {
            JSONObject jso = new JSONObject();
            jso.accumulate("username", "trainone");
            jso.accumulate("sign", "FF1E07242CE2C86A");
            jso.accumulate("servid", "1");
            jso.accumulate("ordernum", "sdf" + i);
            jso.accumulate("postdata", "https://mapi.alipay.com/gateway.do");
            WriteLog.write("12306银联自动支付", "订单号：" + ("sdf" + i) + ",支付前支付账号：");
            String result = SendPostandGet.submitPost("http://223.4.155.3:8080/ccs_paymodule/unionpay", jso.toString(),
                    "UTF-8").toString();
            WriteLog.write("12306银联自动支付", ":请求数据：" + jso.toString() + ",返回结果：" + result);
        }
    }

    /**
     * 
     * @param trainorder
     * @return
     * @time 2014年12月10日 下午10:09:28
     * @author fiend
     */

    public boolean autoUnionPay(Trainorder trainorder, String i) {
        String url = this.unionpayurl;
        WriteLog.write("12306银联自动支付", "订单号：" + trainorder.getOrdernumber() + "对应支付银联地址：" + url);
        JSONObject jso = new JSONObject();
        jso.accumulate("username", "trainone");
        jso.accumulate("sign", "FF1E07242CE2C86A");
        jso.accumulate("servid", "1");
        jso.accumulate("ordernum", trainorder.getOrdernumber());
        try {
            jso.accumulate("postdata",
                    trainorder.getAutounionpayurl() == null ? "false" : trainorder.getAutounionpayurl());
            int paycount = 0;
            try {
                paycount = (int) trainorder.getTcprocedure();
                if (paycount > 0) {
                    jso.accumulate("RePay", "true");
                    WriteLog.write("12306银联自动支付", t + ":" + paycount + "次支付:" + jso.getString("RePay"));
                }
            }
            catch (Exception e1) {
            }
            WriteLog.write("12306银联自动支付", t + ":获取银联支付链接:" + jso.getString("postdata"));
        }
        catch (Exception e2) {
            WriteLog.write("12306银联自动支付", t + ":获取银联支付链接异常" + e2.getMessage());
            e2.printStackTrace();
        }
        try {
            String supplyaccount = trainorder.getSupplyaccount();
            WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",支付前支付账号：" + supplyaccount);
            String result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            WriteLog.write("12306银联自动支付", t + ":地址：" + url + ",请求数据：" + jso.toString() + ",返回结果：" + result);
            if ("".equals(result) || !result.contains("state")) {//没返回任何数据
                trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                trainorder.setPaysupplystatus(2);
                try {
                    trainRC(trainorder.getId(), "网络异常");
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                }
                catch (Exception e) {
                }
                return false;
            }
            JSONObject jsoresult = JSONObject.fromObject(result);
            if (jsoresult.getString("state").equals("1")) {
                try {
                    try {
                        WriteLog.write("12306银联自动支付",
                                t + ":订单号：" + trainorder.getOrdernumber() + "旧的支付账号" + trainorder.getSupplyaccount());
                        trainorder.setSupplyaccount(trainorder.getSupplyaccount().split("/")[0] + "/UNION" + i);
                        WriteLog.write("12306银联自动支付",
                                t + ":订单号：" + trainorder.getOrdernumber() + "新的支付账号" + trainorder.getSupplyaccount());
                    }
                    catch (Exception e) {
                        WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + "更改支付账号失败");
                        e.printStackTrace();
                    }
                    WriteLog.write("12306银联自动支付",
                            t + ":订单号：" + trainorder.getOrdernumber() + "银行账号:" + jsoresult.getString("bankcode"));
                    trainorder.setAutounionpayurlsecond(jsoresult.getString("bankcode"));//使用的银行卡号
                    trainorder.setState12306(Trainorder.ORDEREDPAYING);//12306订单支付中状态
                    //Server.getInstance().getTrainService().updateTrainorder(trainorder);
                    String now = TimeUtil.gettodaydatebyfrontandback(4, 0);
                    String sql = "update T_TRAINORDER set C_STATE12306=5,C_AUTOUNIONPAYURLSECOND='"
                            + trainorder.getAutounionpayurlsecond() + "',C_SUPPLYACCOUNT='"
                            + trainorder.getSupplyaccount() + "' where ID=" + trainorder.getId();
                    sql += ";delete from T_TRAINREFINFO where C_ORDERID=" + trainorder.getId()
                            + ";insert T_TRAINREFINFO(C_ORDERID,C_SUPPLYPAYTIME) values(" + trainorder.getId() + ",'"
                            + now + "')";
                    WriteLog.write("12306银联自动支付", t + ":更新sql：" + sql);
                    Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    WriteLog.write("12306银联自动支付", t + ":订单号：" + trainorder.getOrdernumber() + ",提交自动支付成功");
                    trainRC(trainorder.getId(), "银联自动支付已经接收支付信息,支付审核待确认");
                    return true;
                }
                catch (Exception e1) {
                    WriteLog.write("12306银联自动支付", t + ":1订单号：" + trainorder.getOrdernumber() + "保持银联交易号失败");
                    e1.printStackTrace();
                }
            }
            else {
                trainorder.setState12306(Trainorder.ORDEREDPAYFALSE);
                trainorder.setIsquestionorder(Trainorder.PAYINGQUESTION);
                trainorder.setPaysupplystatus(2);
                try {
                    Server.getInstance().getTrainService().updateTrainorder(trainorder);
                }
                catch (Exception e) {
                }
                return false;
            }
            WriteLog.write("12306银联自动支付", t + ":2订单号：" + trainorder.getOrdernumber() + "银联支付失败");
            return false;
        }
        catch (Exception e) {
            WriteLog.write("12306银联自动支付", t + ":3订单号：" + trainorder.getOrdernumber() + "银联支付失败");
            return false;
        }
    }

    /**
     * 添加日志记录
     * 
     * @param trainorderid
     * @time 2014年12月26日 下午1:45:14
     * @author wzc
     */
    public void trainRC(long trainorderid, String msg) {
        Trainorderrc rc = new Trainorderrc();
        rc.setOrderid(trainorderid);
        rc.setContent(msg);
        rc.setCreateuser("自动支付");
        rc.setYwtype(1);
        Server.getInstance().getTrainService().createTrainorderrc(rc);
    }

    private static final String paymethod = "train_pay_callback";

    private static final String refundmethod = "train_pay_callback";

    public boolean tongChengPayCallBack(Trainorder trainorder) {
        String result = "false";
        for (int i = 0; i < 5; i++) {
            String url = this.tcTrainCallBack;
            JSONObject jso = new JSONObject();
            jso.put("orderid", trainorder.getQunarOrdernumber());
            jso.put("transactionid", trainorder.getOrdernumber());
            jso.put("method", paymethod);
            result = SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
            if ("success".equals(result)) {
                return true;
            }
        }
        return false;
    }

    public boolean tongChengRefundCallBack(Trainorder trainorder) {
        String url = this.tcTrainCallBack;
        // "http://192.168.0.136:8080/cn_interface/tcTrainCallBack";//trainorder.getPassengers().get(0).getTraintickets().get(0).getInsureno();
        JSONObject jso = new JSONObject();
        jso.put("orderid", trainorder.getQunarOrdernumber());
        jso.put("transactionid", trainorder.getOrdernumber());
        jso.put("method", refundmethod);
        SendPostandGet.submitPost(url, jso.toString(), "UTF-8").toString();
        return false;
    }

}
