package com.ccservice.qunar.train;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.train.qunar.QunarOrderMethod;

public class QunarOrderByHandServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("content-type", "text/html;charset=UTF-8");
        PrintWriter out = null;
        String param = "";
        JSONObject result = new JSONObject();
        try {
            
        	out = response.getWriter();
        	param = request.getParameter("jsonStr");
        	if (param == null || "".equals(param)) {
				result.put("success", false);
				result.put("msg", "参数为空");
			} else {
				WriteLog.write("去哪儿_手工", "param="+param);
				JSONObject json = JSONObject.parseObject(param);
				Classification(json);
				result.put("success", true);
				result.put("msg", "");
			}
        	
		} catch (Exception e) {
			
		} finally {
			if (out != null) {
                out.print(result);
                out.flush();
                out.close();
            }
		}
	}
	
	public void Classification(JSONObject jsonObject) {
        if (jsonObject.containsKey("data")) {
            String data1 = jsonObject.getString("data");
            JSONArray data = JSONArray.parseArray(data1);
            String orderNos = allOrderNos(data);
            if (!"".equals(orderNos.trim())) {
                List<QunarOrderMethod> listqom = allOldOrders(orderNos);
                for (int i = 0; i < data.size(); i++) {
                    JSONObject info = data.getJSONObject(i);
                    String orderNo = info.getString("orderNo");
                    boolean isoldorder = false;
                    for (int j = 0; j < listqom.size(); j++) {
                        QunarOrderMethod qunarordermethod = (QunarOrderMethod) listqom.get(j);
                        if ((orderNo != null) && (orderNo.equals(qunarordermethod.getQunarordernumber()))) {
                        	WriteLog.write("去哪儿_手工", "isoldorder = true");
                            qunarordermethod.setOrderjson(info);
                            isoldorder = true;
                            break;
                        }
                    }
                    if (!isoldorder) {
                    	WriteLog.write("去哪儿_手工", "info="+info);
                        QunarOrderMethod qunarordermethod = new QunarOrderMethod();
                        qunarordermethod.setOrderjson(info);
                        exThread(qunarordermethod);
                    }
                }
            }
        }
    }
	
    public String allOrderNos(JSONArray data) {
        String orderNos = "";
        for (int a = 0; a < data.size(); a++) {
            JSONObject info = data.getJSONObject(a);
            String orderNo = info.getString("orderNo");
            orderNos = orderNos + "'" + orderNo + "'";
            if (a < data.size() - 1) {
                orderNos = orderNos + ",";
            }
        }
        return orderNos;
    }
    
    public List<QunarOrderMethod> allOldOrders(String orderNos) {
        List<QunarOrderMethod> listqom = new ArrayList();
        String sql_trainorder = "SELECT OrderNumberOnline,Id FROM TrainOrderOffline  WHERE OrderNumberOnline in (" +

        orderNos + ")";
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql_trainorder, null);
        for (int i = 0; i < list.size(); i++) {
            QunarOrderMethod qunarordermethod = new QunarOrderMethod();
            Map map = (Map) list.get(i);
            qunarordermethod.setQunarordernumber(map.get("OrderNumberOnline").toString());
            qunarordermethod.setId(Long.valueOf(map.get("Id").toString()).longValue());
            listqom.add(qunarordermethod);
        }
        return listqom;
    }
    
    public void exThread(QunarOrderMethod qunarordermethod) {
        ExecutorService pool = Executors.newFixedThreadPool(1);

        Thread t1 = null;

        t1 = new MyThreadQunarOrderOfflineByHand(qunarordermethod, 54);
        pool.execute(t1);

        pool.shutdown();
    }

}
