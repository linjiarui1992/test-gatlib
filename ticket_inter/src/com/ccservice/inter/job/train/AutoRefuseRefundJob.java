package com.ccservice.inter.job.train;

import java.util.Map;
import java.util.List;
import org.quartz.Job;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import java.util.concurrent.ExecutorService;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;

/**
 * JOB_自动拒绝超时退票，目前针对淘宝线上，超时时间为1小时
 * @author WH
 * @time 2016年12月12日 下午3:59:24
 * @version 1.0
 */

public class AutoRefuseRefundJob extends TrainSupplyMethod implements Job {

    //淘宝退票超时时间，分钟转毫秒
    private static final int TaoBaoRefundTimeOut = Integer.parseInt(PropertyUtil.getValue("TaoBaoRefundTimeOut",
            "train.properties")) * 60 * 1000;

    public void execute(JobExecutionContext context) throws JobExecutionException {
        getRefundTickets();
    }

    //查询退票
    @SuppressWarnings("rawtypes")
    private void getRefundTickets() {
        //申请时间
        String refundRequestTime = refundRequestTime();
        //退票查询
        String sql = "select p.C_ORDERID, t.ID, t.C_INTERFACETYPE, t.C_CHANGETYPE,"
                + " t.C_TTCDEPARTTIME, t.C_DEPARTTIME, t.C_INTERFACETICKETNO from T_TRAINTICKET t with(nolock)"
                + " left join T_TRAINPASSENGER p with(nolock) on p.ID = t.C_TRAINPID"
                + " where t.C_STATUS in (5, 6, 8) and t.C_REFUNDREQUESTTIME <= '" + refundRequestTime
                + "' and t.C_ISAPPLYTICKET = 1";
        //退票结果
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        //退票数量
        int refundSize = list.size();
        //PRINT
        System.out.println(ElongHotelInterfaceUtil.getCurrentTime() + "====开始拒绝接口线上退票====" + refundSize);
        //拒绝退票
        if (refundSize > 0) {
            //回调地址
            String trainorderNonRefundable = getSysconfigString("trainorderNonRefundable");
            //地址非空
            if (!ElongHotelInterfaceUtil.StringIsNull(trainorderNonRefundable)) {
                //线程池
                ExecutorService threadPool = Executors.newSingleThreadExecutor();
                //循环处理
                for (int i = 0; i < refundSize; i++) {
                    try {
                        //MAP
                        Map map = (Map) list.get(i);
                        //车票ID
                        long ticketId = getLongMapValue(map, "ID");
                        //订单ID
                        long orderId = getLongMapValue(map, "C_ORDERID");
                        //改签类型
                        int changeType = getIntMapValue(map, "C_CHANGETYPE");
                        //线上改签
                        boolean onlineChange = changeType == 1 || changeType == 2;
                        //接口类型
                        int interfaceType = getIntMapValue(map, "C_INTERFACETYPE");
                        //接口票号
                        String interfaceTicketNo = getStringMapValue(map, "C_INTERFACETICKETNO");
                        //发车时间
                        String departTime = map.get(onlineChange ? "C_TTCDEPARTTIME" : "C_DEPARTTIME").toString();
                        //淘宝接口
                        if ((interfaceType == 0 || interfaceType == 6)
                                && !ElongHotelInterfaceUtil.StringIsNull(interfaceTicketNo)) {
                            //线程处理
                            threadPool.execute(new AutoRefuseRefundThread(orderId, ticketId, interfaceType, departTime,
                                    trainorderNonRefundable, interfaceTicketNo));
                        }
                    }
                    catch (Exception e) {

                    }
                }
                //关闭
                threadPool.shutdown();
            }
        }
    }

    /**
     * 申请退票时间
     */
    private String refundRequestTime() {
        //当前时间 - 超时时间
        long time = System.currentTimeMillis() - TaoBaoRefundTimeOut;
        //格式化，返回结果
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(time));
    }

    @SuppressWarnings("rawtypes")
    private String getStringMapValue(Map map, String key) {
        return map.get(key) == null ? "" : map.get(key).toString();
    }

    @SuppressWarnings("rawtypes")
    private long getLongMapValue(Map map, String key) {
        return map.get(key) == null ? 0 : Long.parseLong(map.get(key).toString());
    }

    @SuppressWarnings("rawtypes")
    private int getIntMapValue(Map map, String key) {
        return map.get(key) == null ? 0 : Integer.parseInt(map.get(key).toString());
    }

}