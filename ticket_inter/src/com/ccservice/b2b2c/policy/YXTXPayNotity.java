package com.ccservice.b2b2c.policy;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 易行天下 支付成功同通知接口
 * 
 * @author 陈栋 2012年7月12日12:13:23
 * 
 */
public class YXTXPayNotity extends HttpServlet {

	private static final long serialVersionUID = 1L;
	WriteLog writeLog = new WriteLog();

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		writeLog.write("易行天下订单通知", request.getQueryString());
		PrintWriter out;
		try {
			out = response.getWriter();
			request.setCharacterEncoding("utf-8");
			String orderid = request.getParameter("orderid");
			String type = request.getParameter("type");
			String totalPrice = request.getParameter("totalPrice");
			double traemoney = 0f;
			if (totalPrice != null) {
				traemoney = Double.parseDouble(totalPrice);
			}
			String sql;
			if (type.equals("2") && orderid != null) {
				if (traemoney > 0) {
					sql = "UPDATE T_ORDERINFO SET C_EXTORDERPRICE=" + traemoney
							+ " , C_EXTORDERSTATUS=" + 2
							+ " WHERE C_EXTORDERID='" + orderid + "'";
				} else {
					sql = "UPDATE T_ORDERINFO SET C_EXTORDERSTATUS=" + 2
							+ " WHERE C_EXTORDERID='" + orderid + "'";
				}
				Server.getInstance().getSystemService().findMapResultBySql(sql,
						null);
				out.print("RECV_ORDID_" + orderid);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
