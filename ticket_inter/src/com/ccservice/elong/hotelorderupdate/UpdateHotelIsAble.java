package com.ccservice.elong.hotelorderupdate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotel.HotelsResult;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.inter.server.Server;

public class UpdateHotelIsAble {
	public static void main(String[] args) {
		String where = " where C_SOURCETYPE=1 and C_TYPE=1  and id <212493 order by ID  ";
		// int
		// count=(int)Server.getInstance().getHotelService().countHotelBySql("
		// select COUNT(id) from T_HOTEL where C_TYPE=1;");
		List<Hotel> hotels = Server.getInstance().getHotelService()
				.findAllHotel(where, "", -1, 0);
		System.out.println(hotels.size());
		Calendar sy = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(sy.getTime());
		sy.add(Calendar.DAY_OF_MONTH, 1);
		String tomorrow = sdf.format(sy.getTime());
		for (int i = 0; i < hotels.size(); i++) {
			//System.out.println("检测：" + hotels.get(i).getName() + "是否可用！！！"
			//		+ hotels.get(i).getHotelcode());
			HotelsResult hotelsResult = Server.getInstance()
					.getIELongHotelService().getPriceByHotelId(today, tomorrow,
							hotels.get(i).getHotelcode());
			List<Hotel> hotelTemp = hotelsResult.getListhotel();
			if (hotelTemp.size() > 0) {
				List<Roomtype> li = hotelTemp.get(0).getListroomtype();
				if (li.size() > 0) {
					hotels.get(i).setState(3);
					//System.out.println(hotels.get(i).getName() + "可用！！！"
						//	+ hotels.get(i).getHotelcode());
				} else {
					System.out.println(hotels.get(i).getName() + "不可用！！！"
							+ hotels.get(i).getHotelcode());
					hotels.get(i).setState(0);
				}
			} else {
				System.out.println(hotels.get(i).getName() + "不可用！！！"
						+ hotels.get(i).getHotelcode());
				hotels.get(i).setState(0);
			}
			Server.getInstance().getHotelService().updateHotelIgnoreNull(
					hotels.get(i));
		}

	}
}
