
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApplySubType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplySubType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Scrap"/>
 *     &lt;enumeration value="VoluntaryRefund"/>
 *     &lt;enumeration value="InvoluntaryRefund"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApplySubType")
@XmlEnum
public enum ApplySubType {

    @XmlEnumValue("Scrap")
    SCRAP("Scrap"),
    @XmlEnumValue("VoluntaryRefund")
    VOLUNTARY_REFUND("VoluntaryRefund"),
    @XmlEnumValue("InvoluntaryRefund")
    INVOLUNTARY_REFUND("InvoluntaryRefund");
    private final String value;

    ApplySubType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApplySubType fromValue(String v) {
        for (ApplySubType c: ApplySubType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
