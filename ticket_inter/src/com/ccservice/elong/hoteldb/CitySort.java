package com.ccservice.elong.hoteldb;

import java.util.List;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.city.City;

public class CitySort {

	@SuppressWarnings("unchecked")
	public static void citySort() {
		List<City> cities = Server
				.getInstance()
				.getHotelService()
				.findAllCityBySql(
						"SELECT * FROM T_CITY WHERE C_NAME IN('北京','上海','广州','深圳','杭州','苏州','宁波','南京','三亚','厦门','香港','武汉','成都','西安','昆明','青岛','大连','沈阳','天津')",
						-1, 0);
		String[] cityes = new String[] { "北京", "上海", "广州", "深圳", "杭州", "苏州",
				"宁波", "南京", "三亚", "厦门", "香港", "武汉", "成都", "西安", "昆明", "青岛",
				"大连", "沈阳", "天津" };
		int sort = 1;
		for (String string : cityes) {
			for (City city : cities) {
				if (city.getName().equals(string)) {
					city.setSort(sort++);
					Server.getInstance().getHotelService()
							.updateCityIgnoreNull(city);
				}
			}
		}
		System.out.println("城市顺序更新完成……");

	}

	public static void main(String[] args) {
		List<City> cities = Server
				.getInstance()
				.getHotelService()
				.findAllCityBySql(
						"SELECT * FROM T_CITY WHERE C_NAME IN('北京','上海','广州','深圳','杭州','苏州','宁波','南京','三亚','厦门','香港','武汉','成都','西安','昆明','青岛','大连','沈阳','天津')",
						-1, 0);
		String[] cityes = new String[] { "北京", "上海", "广州", "深圳", "杭州", "苏州",
				"宁波", "南京", "三亚", "厦门", "香港", "武汉", "成都", "西安", "昆明", "青岛",
				"大连", "沈阳", "天津" };
		int sort = 1;
		for (String string : cityes) {
			for (City city : cities) {
				if (city.getName().equals(string)) {
					city.setSort(sort++);
					Server.getInstance().getHotelService()
							.updateCityIgnoreNull(city);
				}
			}
		}
		System.out.println("城市顺序更新完成……");
	}
}
