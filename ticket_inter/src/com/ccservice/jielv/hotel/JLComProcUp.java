package com.ccservice.jielv.hotel;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.inter.server.Server;
/**
 * 
 * @author wzc
 * 比价实体工具类
 *
 */
public class JLComProcUp{
	/**
	 * 加载华闽数据
	 * @param hotel 酒店对象
	 * @param start 加载数据起始日期
	 * @param end   加载数据结束日期
	 * @param dayHMData 承载数据的每天数据
	 * @throws Exception
	 */

	public void loadUpdateRoomtype( String start, String end,Map<String, List<JLPriceResult>> dayJLData,long time,String citycode) throws Exception {
		List<JLPriceResult> result = Server.getInstance().getIJLHotelService().getUpdateHotelPrice("0",start, end,time,citycode);
		if (result.size()>0) {
			dayJLData.put(citycode, result);
		}
			
	}

	/**
	 * 讲优势数据写入数据库
	 * @param gooddata
	 */
	public void writeDataDB(List<HotelGoodData> gooddata) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		PropertyUtil pu=new PropertyUtil();
		for (int i = 0; i < gooddata.size(); i++) {
			
			HotelGoodData good = gooddata.get(i);
			String where = " where C_HOTELID ="+good.getHotelid();
			//上实时酒店用的
			//List<HotelGoodData> ht = Server.getInstance().getHotelService().findAllHotelGoodData(where, "", -1, 0);
			
			//if(ht.size()>0){
				
			
			good.setUpdatetime(sdf1
					.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			System.out.println(good);
			String wherex = "where C_HOTELID=" + good.getHotelid()
					+ " and C_ROOMTYPEID=" + good.getRoomtypeid()
					+ "  and C_DATENUM='" + good.getDatenum()
					+ "' and c_sorucetype=6";
			List<HotelGoodData> gd = Server.getInstance().getHotelService()
					.findAllHotelGoodData(wherex, "", -1, 0);
			good.getHotelid();
			if (good.getShijiprice() < 20) {
				good.setShijiprice(0l);
			}
			if (gd.size() > 0) {
				if(gd.get(0).getRoomstatus()==1){
					good.setRoomstatus(1l);
				}
				long nline = 0l;
				long sdate = 0l;
				long ndate = 0l;
				try {
					sdate = sdf.parse(gd.get(0).getUpdatetime()).getTime();
					ndate = sdf.parse(good.getUpdatetime()).getTime();
					nline = System.currentTimeMillis() - sdate;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				good.setOpenclose(Integer.parseInt(pu.getValue("openclose")));// 0是关房
				good.setSorucetype("6");
				good.setId(gd.get(0).getId());
				// 特殊处理
				if (good.getProfit() != null) {
					if (good.getProfit() > 0 && good.getProfit() < 20) {
						good.setRoomflag("2");
						// good.setRoomstatus(0l);//开房
						// good.setShijiprice(good.getBaseprice()+20);
					}
					if (good.getProfit() >= 20) {
						good.setRoomflag("1");
					}
				} else {
					System.out.println(good.getProfit());
				}
				
				// 更新时间大于20秒，说明是旧数据，直接删掉(单次循环中，同一酒店同一天，同一房型不可能20秒内就能更新一次，所以为旧数据)，
				// 20秒为估计时间
				
				if (nline > 50000) {
					int g = Server.getInstance().getHotelService()
							.updateHotelGoodDataIgnoreNull(good);
					if (g > 0) {
						System.out.println("更新成功………………");
					} else {
						System.out.println("更新失败........................");
					}
					// 小于20秒说明是一次更新内的数据，留最小早餐数
				} else {
					// 若一次内同酒店房型天库内早餐较少则不作处理，否则删除更新较少早餐价格
					if (gd.get(0).getBfcount() < good.getBfcount()) {

					} else {
						int g = Server.getInstance().getHotelService()
								.updateHotelGoodDataIgnoreNull(good);
						if (g > 0) {
							System.out.println("更新成功………………");
						} else {
							System.out.println("更新失败........................");
						}
					}

				}

			} else {
				try {
					System.out.println("新增");
					Server.getInstance().getHotelService().createHotelGoodData(
							good);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		//	}else{
		//		System.out.println("不是即时酒店");
		//	}
		}
	}
}
