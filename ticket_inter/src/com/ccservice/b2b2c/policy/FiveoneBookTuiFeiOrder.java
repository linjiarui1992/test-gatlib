package com.ccservice.b2b2c.policy;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.ccservice.b2b2c.base.orderinfo.Orderinfo;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * @author Administrator
 * 51book 退废订单状态通知接口
 *
 */
public class FiveoneBookTuiFeiOrder extends PolicyNotifySupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public void doGet(HttpServletRequest request,HttpServletResponse response){
		this.doPost(request, response);
	}
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response){
		Logger logger=Logger.getLogger(this.getClass().getName());
		logger.info("51Book退废通知请求：");
		String typestr=request.getParameter("type");
		if(typestr!=null){
			
			ISystemService service=Server.getInstance().getSystemService();
			int type=Integer.valueOf(typestr);
			String sequenceNo=request.getParameter("sequenceNo");//退废申请的新订单号
			String orderNo=request.getParameter("orderNo");//外部订单号(老)
			String where = "WHERE C_EXTORDERID='" + orderNo + "'";
			Orderinfo orderinfo = (Orderinfo) Server.getInstance()
					.getAirService().findAllOrderinfo(where, "", -1, 0).get(0);
			if(type==1){//退票成功				
				String venderRefundTime=request.getParameter("venderRefundTime");//退票时间
				String venderPayPrice=request.getParameter("venderPayPrice");//退票金额
				
				int state=8;
				if(orderinfo.getOrderstatus()==5){
					state=11;
				}
				String sql="UPDATE T_ORDERINFO SET C_EXTORDERSTATUS="+state+",C_EXTRETURNPRICE="+venderPayPrice+" WHERE C_EXTORDERID="+orderNo;
				service.findMapResultBySql(sql, null);
				try {
					super.log(orderinfo.getId(),  "供应退票通知：退款时间："+venderRefundTime+",退款金额："+venderRefundTime);
				} catch (SQLException e) {
					
				}
			    logger.info("51book退废票成功:sequenceNo:"+sequenceNo+",orderNo:"+orderNo+",手续费:"+venderPayPrice+",type:"+typestr);
				
			}
			
			if(type==2){//退票失败
				
				int state=9;
				if(orderinfo.getOrderstatus()==5){
					state=12;
				}
				String venderRefundTime=request.getParameter("venderRefundTime");//退票时间
				String venderRemark;
				try {
					venderRemark = new String(request.getParameter("venderRemark").getBytes("ISO-8859-1"),"UTF-8");
					logger.info("51book退废票失败:sequenceNo:"+sequenceNo+",orderNo:"+orderNo+",失败原因:"+venderRemark+",type:"+typestr);
					super.log(orderinfo.getId(), "供应退废票不通过。原因："+venderRemark);
				} catch (Exception e) {
					logger.info("记录日志出错：", e.fillInStackTrace());
				}
				
				
				
				String sql="UPDATE T_ORDERINFO SET C_EXTORDERSTATUS="+state+" WHERE C_EXTORDERID='"+orderNo+"'";
				service.findMapResultBySql(sql.toString(),null);
				
			}
			PrintWriter out;
			try {
				out = response.getWriter();
				out.print("S");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	

}
