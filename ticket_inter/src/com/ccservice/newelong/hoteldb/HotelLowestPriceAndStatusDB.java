package com.ccservice.newelong.hoteldb;

import java.util.Map;
import java.util.Date;
import java.util.List;
import org.quartz.Job;
import java.util.HashMap;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.RatePlan;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.hotel.HotelsResult;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.newelong.util.UpdateElDataUtil;

/**
 * 更新酒店最低价格和状态
 * 艺龙2.0接口
 * @author WH
 */
public class HotelLowestPriceAndStatusDB implements Job{
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		long start = System.currentTimeMillis();
		String sql = "where C_SOURCETYPE = 1 and C_HOTELCODE is not null";
		List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(sql, "", -1, 0);
		if(hotels!=null && hotels.size()>0){
			int total = 10;//一次请求10个艺龙酒店
			int remainder = hotels.size()%total;//不足total个的酒店数
			int count = 0;
			String elHotelIds = "";//艺龙酒店ID
			List<Long> localHotelIds = new ArrayList<Long>();//本地酒店ID
			Map<String,Long> idMap = new HashMap<String, Long>();//<艺龙酒店ID,本地酒店ID>
			Map<Long,Hotel> hotelMap = new HashMap<Long, Hotel>();//<本地酒店ID,本地酒店>
			for (int i = 0; i < hotels.size(); i++) {
				//本地酒店
				Hotel hotel = hotels.get(i);
				//本地酒店ID
				long localHotelId = hotel.getId();
				//艺龙酒店ID
				String elHotelId = hotel.getHotelcode();
				//赋值
				elHotelIds += elHotelId + ",";
				localHotelIds.add(localHotelId);
				idMap.put(elHotelId, localHotelId);
				hotelMap.put(localHotelId, hotel);
				//最多10个
				count++;
				if(count==total || (remainder>0 && i==hotels.size()-1)){
					elHotelIds = elHotelIds.substring(0,elHotelIds.length()-1);//去掉最后的逗号
					//请求艺龙
					getHotelPrice(elHotelIds, localHotelIds, idMap, hotelMap);
					//置空
					count = 0;
					elHotelIds = "";
					localHotelIds = new ArrayList<Long>();
					idMap = new HashMap<String, Long>();
					hotelMap = new HashMap<Long, Hotel>();
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("更新酒店最低价格和状态完成-----耗时-----" + DateSwitch.showTime(end - start));
	}
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		String sql = "where C_SOURCETYPE = 1 and C_HOTELCODE is not null and C_CITYID = 101";
		List<Hotel> hotels = Server.getInstance().getHotelService().findAllHotel(sql, "", -1, 0);
		if(hotels!=null && hotels.size()>0){
			int total = 10;//一次请求10个艺龙酒店
			int remainder = hotels.size()%total;//不足total个的酒店数
			int count = 0;
			String elHotelIds = "";//艺龙酒店ID
			List<Long> localHotelIds = new ArrayList<Long>();//本地酒店ID
			Map<String,Long> idMap = new HashMap<String, Long>();//<艺龙酒店ID,本地酒店ID>
			Map<Long,Hotel> hotelMap = new HashMap<Long, Hotel>();//<本地酒店ID,本地酒店>
			for (int i = 0; i < hotels.size(); i++) {
				//本地酒店
				Hotel hotel = hotels.get(i);
				//本地酒店ID
				long localHotelId = hotel.getId();
				//艺龙酒店ID
				String elHotelId = hotel.getHotelcode();
				//赋值
				elHotelIds += elHotelId + ",";
				localHotelIds.add(localHotelId);
				idMap.put(elHotelId, localHotelId);
				hotelMap.put(localHotelId, hotel);
				//最多10个
				count++;
				if(count==total || (remainder>0 && i==hotels.size()-1)){
					elHotelIds = elHotelIds.substring(0,elHotelIds.length()-1);//去掉最后的逗号
					//请求艺龙
					getHotelPrice(elHotelIds, localHotelIds, idMap, hotelMap);
					//置空
					count = 0;
					elHotelIds = "";
					localHotelIds = new ArrayList<Long>();
					idMap = new HashMap<String, Long>();
					hotelMap = new HashMap<Long, Hotel>();
				}
			}
		}
	}
	public static void getHotelPrice(String elHotelIds, List<Long> localHotelIds, Map<String,Long> idMap, Map<Long,Hotel> hotelMap){
		try {
			String checkInDate = UpdateElDataUtil.getCurrentDate();
			String checkOutDate = UpdateElDataUtil.getAddDate(checkInDate, 1);
			HotelsResult result = Server.getInstance().getIELongHotelService().getHotelPrice("", "", elHotelIds, checkInDate, checkOutDate);
			if("0".equals(result.getResultCode())){
				List<Hotel> elHotelList = result.getListhotel();
				if(elHotelList!=null && elHotelList.size()>0){
					boolean IsException = false;//是否发生异常
					List<Long> tempLocalHotelIds = new ArrayList<Long>();//在艺龙找到的酒店
					for(Hotel elHotel:elHotelList){
						try {
							if(!UpdateElDataUtil.StringIsNull(elHotel.getHotelcode())){
								//通过艺龙返回的ID查找本地酒店ID
								Long localId = idMap.get(elHotel.getHotelcode());
								tempLocalHotelIds.add(localId);
								//通过本地酒店ID查找本地酒店
								Hotel hotel = hotelMap.get(localId);
								//New一个更新酒店
								Hotel updateHotel = new Hotel();
								updateHotel.setId(hotel.getId());
								//是否要更新标志
								boolean needupdate = false;
								//最低价格
								if(elHotel.getLowestPrice()!=null && elHotel.getLowestPrice().doubleValue()>0){
									double elLowestPrice = elHotel.getLowestPrice().doubleValue();
									double localLowestPrice = hotel.getStartprice()==null ? 0 : hotel.getStartprice().doubleValue();
									if(elLowestPrice!=localLowestPrice){
										needupdate = true;
										updateHotel.setStartprice(elHotel.getLowestPrice().doubleValue());
									}
								}
								//判断是否存在可用RatePlan
								boolean haveRatePlan = false;
								//艺龙房型
								List<Roomtype> listRoomType = elHotel.getListroomtype();
								if(listRoomType!=null && listRoomType.size()>0){
									for(int i = 0 ; i < listRoomType.size() ; i++){
										Roomtype roomtype = listRoomType.get(i);
										List<RatePlan> listrateplan = roomtype.getListrateplan();
										if(listrateplan!=null && listrateplan.size()>0){
											haveRatePlan = true;
											break;
										}
									}
								}
								if(haveRatePlan){
									elHotel.setState(3);//酒店状态 是否可用 列名 表示当前酒店可用与否 0--表示暂时不可用 2--表示艺龙那边没有提供给我们房型 3--表示可用
									updateHotel.setStatedesc("该酒店可用");
								}else{
									elHotel.setState(2);
									updateHotel.setStatedesc("酒店可用,但是艺龙没有提供给我们房型!");
								}
								if(!elHotel.getState().equals(hotel.getState())){
									needupdate = true;
									updateHotel.setState(elHotel.getState());
								}
								if(needupdate){
									updateHotel.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
									Server.getInstance().getHotelService().updateHotelIgnoreNull(updateHotel);
									System.out.println("更新酒店-----" + hotel.getId() + "-----" + hotel.getName() + "-----" + hotel.getHotelcode());
								}else{
									System.out.println("匹配酒店-----" + hotel.getId() + "-----" + hotel.getName() + "-----" + hotel.getHotelcode());
								}
							}
						} catch (Exception e) {
							IsException = true;
						}
					}
					if(IsException){
						localHotelIds = new ArrayList<Long>();//出现异常，不禁用酒店
					}else{
						localHotelIds.removeAll(tempLocalHotelIds);
					}
				}
				if(localHotelIds!=null && localHotelIds.size()>0){
					String ids = "";
					for(Long id:localHotelIds){
						Hotel hotel = hotelMap.get(id);
						if(hotel.getState()==null || hotel.getState()!=0){//已禁用的不更新
							ids += id + ",";
						}
					}
					if(!UpdateElDataUtil.StringIsNull(ids)){
						ids = ids.substring(0,ids.length()-1);//去掉最后的逗号
						String lastupdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
						String sql = "update T_HOTEL " +
									 "set C_STATE = 0 , C_STATEDESC = '该酒店暂不可用' , C_LASTUPDATETIME = '" + lastupdatetime + "' " +
								     "where ID in ("+ids+")";
						Server.getInstance().getSystemService().findMapResultBySql(sql,null);
						System.out.println("-----通过价格接口未找到酒店，禁用酒店，ID分别为" + ids + "-----");
					}
				}
			}else{
				//请求参数异常
				if("H000998".equals(result.getResultCode()) && localHotelIds!=null && localHotelIds.size()>0){
					//禁用
					if(localHotelIds.size()==1){
						Hotel hotel = hotelMap.get(localHotelIds.get(0));
						if(hotel.getState()==null || hotel.getState()!=0){
							hotel.setState(0);
							hotel.setStatedesc("该酒店暂不可用");
							hotel.setLastupdatetime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
							Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
							System.out.println("更新酒店-----" + hotel.getId() + "-----" + hotel.getName() + "-----" + hotel.getHotelcode());
						}
					}
					//单个酒店重新请求
					else{
						for(Long id:localHotelIds){
							//重新赋值
							Hotel hotel = hotelMap.get(id);
							//localHotelIds
							List<Long> tempLocalHotelIds = new ArrayList<Long>();
							tempLocalHotelIds.add(id);
							//idMap
							Map<String,Long> tempIdMap = new HashMap<String, Long>();
							tempIdMap.put(hotel.getHotelcode(), id);
							//hotelMap
							Map<Long,Hotel> tempHotelMap = new HashMap<Long, Hotel>();
							tempHotelMap.put(id, hotel);
							//重新请求
							getHotelPrice(hotel.getHotelcode(), tempLocalHotelIds, tempIdMap, hotelMap);
						}
					}
				}
				//访问太频繁
				else if("A201010001".equals(result.getResultCode())){
					Thread.sleep(1000*5);
					//重新请求
					getHotelPrice(elHotelIds, localHotelIds, idMap, hotelMap);
				}
				else{
					throw new Exception(result.getResultCode() + "|"+ result.getResultMessage());
				}
			}
		} catch (Exception e) {
			System.out.println("发生异常~~~~~~~~~~~"+localHotelIds+"~~~~~~~~~~~"+e.getMessage());
		}
	}
}
