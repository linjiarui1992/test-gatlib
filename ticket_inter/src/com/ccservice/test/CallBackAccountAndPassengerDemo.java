package com.ccservice.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ccervice.util.db.DBHelper2;
import com.ccervice.util.db.DBHelperAccount;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.util.db.DBHelper;
import com.ccservice.inter.job.Util.CallBackPassengerUtil;

public class CallBackAccountAndPassengerDemo {

    static int Global_LoginnameId;

    static String loginName;

    static int isEnable;

    static int DB_LoginnameId;

    //    Map<String, Object> Accounts = new HashMap<String, Object>();

    int count = 1;

    public static void main(String[] args) {

        try {

            //            for (int i = 23; i < 41; i++) {
            //                for (int j = 1; j < 6; j++) {
            //                    String file = "C:/Users/Administrator/Desktop/同城数据反/身份数据(" + i + "_" + j + ")";
            //                    //                    System.out.println(file);
            //                }
            //            }

            //            String filew = "C:/Users/Administrator/Desktop/文档/有效账号数据.xlsx";
            String file = "D:/美团数据/供应商数据.xlsx";
            new CallBackAccountAndPassengerDemo().readExcelMsgMethod(file);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取Excel2007/2013的方法
     * 
     * @param file
     * @param flag
     * @throws Exception
     * @time 2016年1月7日 下午6:36:06
     * @author Administrator
     */
    public void readExcelMsgMethod(String file) throws Exception {
        InputStream is = new FileInputStream(file);
        XSSFWorkbook wb = new XSSFWorkbook(is);
        //获取第一张Sheet表
        XSSFSheet readSheet = wb.getSheetAt(0);
        //获取总行
        int rowCount = readSheet.getPhysicalNumberOfRows();
        //获取总列
        int columnCount = readSheet.getRow(0).getPhysicalNumberOfCells();
        int loginNameId = 0;
        for (int i = 0; i < rowCount; i++) {
            System.out.println(i + "------------------>");
            for (int j = 0; j < columnCount; j++) {

                if (i > 0) {
                    org.apache.poi.ss.usermodel.Cell cell = readSheet.getRow(i).getCell(j);
                    loginNameId = Integer.parseInt(cell.getStringCellValue().toString());
                    System.out.println(loginNameId);
                }
            }

            //            boolean isExist = false;
            //            List<com.ccervice.util.db.DataRow> dataRows = null;
            //
            //            String sql = "select C_LOGINNAME,C_ISENABLE from T_CUSTOMERUSER where ID=" + loginNameId;
            //            com.ccervice.util.db.DataTable result = DBHelperAccount.GetDataTable(sql);
            //            dataRows = result.GetRow();
            //            if (dataRows.size() > 0) {
            //                com.ccervice.util.db.DataRow dataRow = dataRows.get(0);
            //                loginName = dataRow.GetColumnString("C_LOGINNAME");
            //                isEnable = dataRow.GetColumnInt("C_ISENABLE");
            //                System.out.println("DB---->" + loginName + "==" + isEnable);
            //                callBacktongchengMethod(loginNameId, "小明", "123", isExist);
            //            }
            //            Thread.sleep(400L);
        }

    }

    /**
     * 读取Excel2003的方法
     * 
     * @param file
     * @throws Exception
     * @time 2016年1月7日 下午6:35:25
     * @author Administrator
     */
    public void readExcelMsgMethod2003(String file) throws Exception {

        Workbook readwb = Workbook.getWorkbook(new File(file + ".xls"));
        //获取第一张Sheet表   
        Sheet readsheet = readwb.getSheet(0);
        //获取Sheet表中所包含的总列数   
        int rsColumns = readsheet.getColumns();
        //获取Sheet表中所包含的总行数   
        int rsRows = readsheet.getRows();
        for (int i = 0; i < rsRows; i++) {
            System.out.println(i + "------------------>");
            String name = "";
            String IdCard = "";
            int LoginnameId = 0;
            if (i > 0) {
                for (int j = 0; j < rsColumns; j++) {
                    Cell cell = readsheet.getCell(j, i);
                    if (j == 1) {
                        name = cell.getContents();
                    }
                    else if (j == 2) {
                        IdCard = cell.getContents();
                    }
                    else if (j == 4) {
                        LoginnameId = Integer.parseInt(cell.getContents());
                    }
                }
                System.out.print(name + "--");
                System.out.print(IdCard + "--");
                System.out.println(LoginnameId);
                System.out.println(file);

                List<com.ccervice.util.db.DataRow> dataRows = null;
                boolean isExist = false;

                if (LoginnameId == Global_LoginnameId) {
                }
                else {
                    Global_LoginnameId = LoginnameId;
                    String sql = "select C_LOGINNAME,C_ISENABLE from T_CUSTOMERUSER where ID=" + LoginnameId;
                    com.ccervice.util.db.DataTable result = DBHelper2.GetDataTable(sql);
                    dataRows = result.GetRow();
                    if (dataRows.size() > 0) {
                        com.ccervice.util.db.DataRow dataRow = dataRows.get(0);
                        loginName = dataRow.GetColumnString("C_LOGINNAME");
                        isEnable = dataRow.GetColumnInt("C_ISENABLE");
                        System.out.println("DB---->" + loginName + "==" + isEnable);
                    }
                    else {
                        isExist = true;
                    }
                }
                //                callBacktongchengMethod(LoginnameId, name, IdCard, isExist);
            }
            Thread.sleep(400L);
        }
    }

    /**
     * 判断帐号状态,回调接口
     * 
     * @param LoginnameId
     * @param name
     * @param IdCard
     * @param isExist
     * @return  帐号状态标识
     * @throws Exception
     * @time 2015年12月22日 上午11:49:01
     * @author Administrator
     */
    public void callBacktongchengMethod(int LoginnameId, String name, String IdCard, boolean isExist) throws Exception {
        int flag = 0;
        if (!isExist) {
            if (isEnable == 1 || isEnable == 3 || isEnable == 4) {
                flag = 1;
                savaDBMethod(LoginnameId, name, IdCard, flag, count);
            }
            else {
                Customeruser customeruser = new Customeruser();
                customeruser.setLoginname(LoginnameId + "");
                customeruser.setIsenable(isEnable);
                List<Trainpassenger> trainPassengers = new ArrayList<Trainpassenger>();
                Trainpassenger p1 = new Trainpassenger();
                p1.setName(name);
                p1.setIdnumber(IdCard);
                p1.setIdtype(1);
                Trainticket trainticket = new Trainticket();
                trainticket.setTickettype(1);
                List<Trainticket> traintickets = new ArrayList<Trainticket>();
                traintickets.add(trainticket);
                p1.setTraintickets(traintickets);
                //放入乘客
                //                trainPassengers.add(p1);
                int operationtypeid = 2;//操作类型 ID 1:新增，2:删除，3:修改
                CallBackPassengerUtil.callBackTongcheng(customeruser, trainPassengers, operationtypeid);
                flag = 2;
                //
                //                Accounts.put("LoginnameId", LoginnameId);
                //                Accounts.put("name", IdCard);
                //                Accounts.put("flag", flag);
                //                Accounts.put("count", count);

                savaDBMethod(LoginnameId, name, IdCard, flag, count);
            }
        }
        else {
            flag = 3;
            isEnable = -1;
            loginName = "";
            savaDBMethod(LoginnameId, name, IdCard, flag, count);
        }

    }

    /**
     * 将数据存入DB.5
     * 
     * @param LoginnameId
     * @param name
     * @param IdCard
     * @param flag
     * @param count
     * @time 2015年12月22日 上午11:51:38
     * @author Administrator
     */
    public void savaDBMethod(int LoginnameId, String name, String IdCard, int flag, int isEnable) {
        String sql2 = "UPDATE TongchengCustomeruser SET LoginName='" + name + "',IsEnable=" + isEnable + ",falg="
                + flag + " WHERE CustomeruserId=" + LoginnameId;
        Boolean istrue = DBHelper.executeSql(sql2);
        System.out.println("---->" + istrue);
    }
}
