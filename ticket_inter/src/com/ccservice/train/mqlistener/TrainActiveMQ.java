package com.ccservice.train.mqlistener;

import javax.jms.JMSException;

import com.alibaba.fastjson.JSONObject;
import com.ccervice.util.ActiveMQUtil;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.train.mqlistener.Method.KongTieRabbtMQMethod;

public class TrainActiveMQ extends TrainSupplyMethod {
    private String url = "tcp://localhost:61616/";

    private String MQ_NAME = "";

    private int type;

    private long orderid;

    private long successtime;

    private long intotime;

    private int queryno;

    public TrainActiveMQ(long orderid) {
        this.orderid = orderid;
    }

    /**
     * 
     * @param orderid 订单号
     * @param type 类型
     */
    public TrainActiveMQ(long orderid, int type) {
        this.orderid = orderid;
        this.type = type;
    }

    /**
     * 死锁队列 
     * @time 2015年2月4日 下午3:25:34
     * @author fiend
     */
    public void sendDeadlockMQ() {
        url = getActiveMQUrl();
        MQ_NAME = getSysconfigString("DEADLOCK_NAME");
        try {
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("type", type);
            ActiveMQUtil.sendMessage(url, MQ_NAME, jsoseng.toString());
        }
        catch (JMSException e) {
        }
    }

    /**
     * 同程扣款队列 
     * @time 2015年2月4日 下午3:25:34
     * @author fiend
     */
    public void sendDeductionMQ() {
        url = getActiveMQUrl();
        MQ_NAME = "QueueMQ_TrainDeduction";
        try {
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid);
            jsoseng.put("type", type);
//            ActiveMQUtil.sendMessage(url, MQ_NAME, jsoseng.toString());
            new KongTieRabbtMQMethod().activeMQDeduction(jsoseng.toString());
        }
        catch (Exception e) {
        }
    }

    public TrainActiveMQ(int type, long orderid, long successtime, long intotime, int queryno) {
        this.type = type;
        this.orderid = orderid;
        this.successtime = successtime;
        this.intotime = intotime;
        this.queryno = queryno;
    }

    /**
     * 审核队列 
     * @time 2015年2月4日 下午3:25:44
     * @author fiend
     */
    public void sendqueryMQ() {
        url = getActiveMQUrl();
        MQ_NAME = getSysconfigString("QUEUE_NAME");
        try {
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("type", type);
            jsoseng.put("orderid", orderid);
            jsoseng.put("successtime", successtime);
            jsoseng.put("intotime", intotime);
            jsoseng.put("queryno", queryno);
            new KongTieRabbtMQMethod().activeMQquery(jsoseng.toString());
        }
        catch (Exception e) {
        }
    }

    /**
     * 清除缓存中补单id队列 
     * @time 2015年2月4日 下午3:25:44
     * @author fiend
     */
    public void sendBudanRemoveIdMQ() {
        url = getActiveMQUrl();
        MQ_NAME = getSysconfigString("BUDANREMOVEID_NAME");
        try {
            JSONObject jsoseng = new JSONObject();
            jsoseng.put("orderid", orderid + "");
            ActiveMQUtil.sendMessage(url, MQ_NAME, jsoseng.toString());
        }
        catch (Exception e) {
        }
    }

}
