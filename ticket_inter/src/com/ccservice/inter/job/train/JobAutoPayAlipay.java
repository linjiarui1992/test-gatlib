package com.ccservice.inter.job.train;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.inter.job.train.thread.MyThreadAutoPay;
import com.ccservice.inter.server.Server;

/**
 * 使用支付宝自动支付12306订单
 * @time 2014年11月6日 上午10:18:28
 * @author yinshubin
 */
public class JobAutoPayAlipay extends TrainSupplyMethod implements Job {

    //    private final static int[] PAY_NO = { 0, 1 };//机器人编号
    //
    //    private final static int[] PAY_NO_YEE = { 9000 };//机器人编号

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        exThread();
    }

    //    /**
    //     * 所有等待支付订单 
    //     * @return
    //     * @time 2014年11月6日 上午10:46:29
    //     * @author yinshubin
    //     */
    //    public void allWaitPay(Trainorder trainorder, int i, boolean isqunar) {
    //        int paysum = isqunar ? 6 : 3;
    //        try {
    //            Thread.sleep(isqunar ? i * 100 : (i - 9000) * 100);
    //        }
    //        catch (InterruptedException e) {
    //        }
    //        int j = 0;
    //        System.out.println("size:" + trainlist.size());
    //        for (int j2 = 0; j2 < trainlist.size(); j2++) {
    //            if (j < paysum && iscanpay()) {
    //                Integer before = isqunar ? Integer.valueOf(JobQunarOrder.getSystemConfig("alipaynumber")) : Integer
    //                        .valueOf(JobQunarOrder.getSystemConfig("alipaynumberyee"));
    //                int payno = isqunar ? i : i - 9000;
    //                if (before != null && before > 0) {
    //                    if (payno <= before) {
    //                        if ((int) (trainlist.get(j2).getId()) % before == payno) {
    //                            Trainorder trainorder = Server.getInstance().getTrainService()
    //                                    .findTrainorder(trainlist.get(j2).getId());
    //                            if (isqunar
    //                                    && trainorder.getAgentid() == Integer.valueOf(JobQunarOrder
    //                                            .getSystemConfig("qunar_agentid"))) {
    //                                if (trainorder.getPassengers().get(0).getTraintickets().size() == 1) {
    //                                    System.out.println("当前自动支付机器编号:" + i + "当前订单:" + trainlist.get(j2).getId());
    //                                    waitPayInformation(trainlist.get(j2).getId(), i);
    //                                    j++;
    //                                }
    //                            }
    //                            else if (!isqunar
    //                                    && trainorder.getAgentid() != Integer.valueOf(JobQunarOrder
    //                                            .getSystemConfig("qunar_agentid"))) {
    //                                System.out.println("当前自动支付机器编号:" + i + "当前订单:" + trainlist.get(j2).getId());
    //                                waitPayInformation(trainlist.get(j2).getId(), i);
    //                                j++;
    //                            }
    //                        }
    //                    }
    //                    else {
    //                        break;
    //                    }
    //                    if (before != (isqunar ? Integer.valueOf(JobQunarOrder.getSystemConfig("alipaynumber")) : Integer
    //                            .valueOf(JobQunarOrder.getSystemConfig("alipaynumberyee"))))
    //                        break;
    //                }
    //            }
    //            else {
    //                break;
    //            }
    //        }
    //        if (isqunar) {
    //            fresh(i);
    //        }
    //    }

    /**
     * 说明：开启线程
     * @time 2014年11月28日 上午10:57:01
     * @author fiend
     */
    public void exThread() {

        //        int alsum = Integer.valueOf(JobQunarOrder.getSystemConfig("alipaynumber"));
        //        int alsumyee = Integer.valueOf(JobQunarOrder.getSystemConfig("alipaynumberyee"));
        int alsum = Integer.valueOf(getSysconfigString("alipaynumber"));//有多少台计算机参与自动支付shu
        int alsumyee = Integer.valueOf(getSysconfigString("alipaynumberyee"));//有多少台计算机参与自动支付hy

        ExecutorService pool = Executors.newFixedThreadPool(10);
        Thread t1 = null;
        Trainform trainform = new Trainform();
        trainform.setOrderstatus(Trainorder.WAITISSUE);
        trainform.setState12306(Trainorder.ORDEREDWAITPAY);
        List<Trainorder> torderList = Server.getInstance().getTrainService().findAllTrainorder(trainform, null);
        for (int i = 0; i < alsum; i++) {
            if (getDataString("autostatus" + i, "0").equals("0")) {
                setDataString("autostatus" + i, "1");
                fresh(i);
                setDataString("autostatus" + i, "0");
            }
        }
        for (int j = 0; j < torderList.size(); j++) {
            Trainorder trainorder = torderList.get(j);
            if (trainorder.getIsquestionorder() == Trainorder.NOQUESTION) {
                int isjinru = 0;
                if (getSysconfigString("qunar").equals(trainorder.getAgentid() + "")) {
                    if (alsum == 0) {
                        isjinru = 1;
                    }
                    for (int i = 0; i < alsum; i++) {
                        //            if (JobQunarOrder.getSystemConfig("autostatus" + i) != null
                        //                    && JobQunarOrder.getSystemConfig("autostatus" + i).equals("0")) {
                        //            if (getSysconfigString("autostatus" + i) != null && getSysconfigString("autostatus" + i).equals("0")) {
                        if (getDataString("autostatus" + i, "0").equals("0")) {
                            //                        trainorder.setOrderstatus(Trainorder.ISSUED);
                            try {
                                //                    JobQunarOrder.changeSystemCofig(JobQunarOrder.getSystemConfigId("autostatus" + i), "1");
                                //                                        changeSystemCofig(getSystemConfigId("autostatus" + i), "1");
                                setDataString("autostatus" + i, "1");
                            }
                            catch (Exception e) {
                            }
                            trainorder = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
                            t1 = new MyThreadAutoPay(i, true, trainorder);
                            pool.execute(t1);
                            isjinru = 1;
                            break;
                        }
                    }
                }
                else {
                    if (alsumyee == 0) {
                        isjinru = 1;
                    }
                    for (int i = 0; i < alsumyee; i++) {
                        //                    trainorder.setOrderstatus(Trainorder.ISSUED);
                        //            if (JobQunarOrder.getSystemConfig("autostatus" + (9000 + i)) != null
                        //                    && JobQunarOrder.getSystemConfig("autostatus" + (9000 + i)).equals("0")) {
                        if (getSysconfigString("autostatus" + (9000 + i)) != null
                                && getSysconfigString("autostatus" + (9000 + i)).equals("0")) {
                            try {
                                //                    JobQunarOrder.changeSystemCofig(JobQunarOrder.getSystemConfigId("autostatus" + (9000 + i)), "1");
                                //                            changeSystemCofig(getSystemConfigId("autostatus" + (9000 + i)), "1");
                                setDataString("autostatus" + 9000 + i, "1");
                            }
                            catch (Exception e) {
                            }
                            trainorder = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
                            t1 = new MyThreadAutoPay(9000 + i, false, trainorder);
                            pool.execute(t1);
                            isjinru = 1;
                            break;
                        }
                    }
                }
                if (isjinru == 0) {
                    j--;
                }
            }
        }
        pool.shutdown();
    }
}
