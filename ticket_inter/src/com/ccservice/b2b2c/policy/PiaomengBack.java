package com.ccservice.b2b2c.policy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.zrate.Zrate;

/**
 * 
 * @author
 * 
 */
public class PiaomengBack extends SupplyMethod {

    /**
     * 根据航班获取政策信息
     * 
     * @param scity
     * @param ecity
     * @param sdate
     * @param flightnumber
     * @param cabin
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Zrate> getZrateByFlightNumber(String scity, String ecity, String sdate, String flightnumber,
            String cabin, String url) {
        List<Zrate> zrates = new ArrayList<Zrate>();
        List list = new ArrayList();
        String mDateTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        String strSP = "[dbo].[sp_GetZrateByFlight] " + "@chufajichang = N'" + scity + "'," + "@daodajichang = N'"
                + ecity + "'," + "@chufariqi = N'" + sdate + "'," + "@dangqianshijian= N'" + mDateTime + "',"
                + "@hangkonggongsi= N'" + flightnumber.substring(0, 2) + "'," + "@cangwei= N'" + cabin + "',"
                + "@hangbanhao= N'" + flightnumber + "'," + "@ismulity=1,@isgaofan=1";
        try {
            url = url + "/cn_service/service/";
            HessianProxyFactory factory = new HessianProxyFactory();
            ISystemService servier = (ISystemService) factory.create(ISystemService.class,
                    url + ISystemService.class.getSimpleName());
            list = servier.findMapResultByProcedure(strSP);
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    Zrate zrate = new Zrate();
                    Map zrateMap = (Map) list.get(i);
                    if (zrateMap.get("id") != null) {
                        zrate.setId(Integer.valueOf(zrateMap.get("id").toString()));
                    }
                    if (zrateMap.get("afterworktime") != null) {
                        zrate.setAfterworktime(zrateMap.get("afterworktime").toString());
                    }
                    if (zrateMap.get("ratevalue") != null) {
                        zrate.setRatevalue(Float.valueOf(zrateMap.get("ratevalue").toString()));
                    }
                    if (zrateMap.get("remark") != null) {
                        zrate.setRemark(zrateMap.get("remark").toString());
                    }
                    if (zrateMap.get("speed") != null) {
                        zrate.setSpeed(zrateMap.get("speed").toString());
                    }
                    if (zrateMap.get("onetofivewastetime") != null) {
                        zrate.setOnetofivewastetime(zrateMap.get("onetofivewastetime").toString());
                    }
                    if (zrateMap.get("worktime") != null) {
                        zrate.setWorktime(zrateMap.get("worktime").toString());
                    }
                    if (zrateMap.get("outid") != null) {
                        zrate.setOutid(zrateMap.get("outid").toString());
                    }
                    if (zrateMap.get("general") != null) {
                        zrate.setGeneral(Long.valueOf(zrateMap.get("general").toString()));
                    }
                    if (zrateMap.get("agentid") != null) {
                        zrate.setAgentid(Long.valueOf(zrateMap.get("agentid").toString()));
                    }
                    zrates.add(zrate);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return zrates;
    }
}
