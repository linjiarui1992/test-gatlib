package com.ccservice.offlineExpress;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offlineExpress.ems.EmsService;
import com.ccservice.offlineExpress.util.CommonUtil;
import com.ccservice.offlineExpress.uupt.UUptService;

public class OfflineExpressServlet extends HttpServlet implements Servlet {

    private static final long serialVersionUID = -6094836118456072680L;

    // 日志名称
    private static final String logName = "线下火车票_ticket_inter请求快递公司内部交互接口";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        // 编码
        req.setCharacterEncoding("utf-8");
        res.setCharacterEncoding("utf-8");
        res.setContentType("application/json;charset=utf-8");
        // 随机
        int random = CommonUtil.randomNum();
        // 结果
        JSONObject responseJson = new JSONObject();
        // 异常捕捉
        try {
            // 请求快递公司
            String expressName = req.getParameter("expressName");
            // 请求方法
            String method = req.getParameter("method");
            // 请求参数
            String data = req.getParameter("data");
            // 日志
            WriteLog.write(logName,
                    random + "expressName：" + expressName + "----method：" + method + "----data：" + data);
            // 解析
            JSONObject dataJson = JSONObject.parseObject(data);
            // uu跑腿
            if ("uupt".equals(expressName)) {
                responseJson = new UUptService().operate(logName, expressName, random, method, dataJson, responseJson);
                // 日志
                WriteLog.write(logName, random + "接口最终返回结果responseJson：" + responseJson);
            }
            // EMS
            if ("ems".equals(expressName)) {
                responseJson = new EmsService().operate(logName, expressName, random, method, dataJson, responseJson);
            }
        }
        catch (Exception e) {
            // 记录异常日志
            ExceptionUtil.writelogByException(logName + "异常日志", e, String.valueOf(random));
        }
        finally {
            try {
                // 输出
                res.getWriter().write(responseJson.toString());
                // 日志
                WriteLog.write(logName, random + "---responseJson---" + responseJson);
            }
            catch (Exception e) {

            }
        }
    }
}
