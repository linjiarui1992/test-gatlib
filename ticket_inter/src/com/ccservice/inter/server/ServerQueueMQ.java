/**
 * 
 */
package com.ccservice.inter.server;

import java.util.ArrayList;
import java.util.List;

import javax.jms.MessageConsumer;
import javax.jms.Session;

/**
 * 放消费者监听的全局 工具类
 * @time 2015年10月16日 下午11:06:39
 * @author chendong
 */
public class ServerQueueMQ {

    public static List<Session> TrainCreateOrderSessionList = new ArrayList<Session>();

    public static List<MessageConsumer> MessageListenerChendongTestList = new ArrayList<MessageConsumer>();

    public static List<MessageConsumer> TrainCreateOrderMessageConsumerList = new ArrayList<MessageConsumer>();

    /**
     * 核验手机号的消费者的List
     */
    public static List<MessageConsumer> MqCheckTrainAccountMobileNoList = new ArrayList<MessageConsumer>();

    /**
     * 下单消费者的List
     */
    public static List<MessageConsumer> MqTrainCreateOrderList = new ArrayList<MessageConsumer>();
    /**
     * 下单消费者排队
     */
    public static List<MessageConsumer> MqTrainCreateOrderListpaidui = new ArrayList<MessageConsumer>();

}
