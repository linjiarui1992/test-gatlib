
package com.liantuo.webservice.version2_0.createpolicyorderbypnrtxt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="param1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payerAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="settlePrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="tradeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentInfo", propOrder = {
    "param1",
    "payerAccount",
    "paymentUrl",
    "settlePrice",
    "tradeNo"
})
public class PaymentInfo {

    @XmlElementRef(name = "param1", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> param1;
    @XmlElementRef(name = "payerAccount", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> payerAccount;
    @XmlElementRef(name = "paymentUrl", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> paymentUrl;
    @XmlElementRef(name = "settlePrice", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<Double> settlePrice;
    @XmlElementRef(name = "tradeNo", namespace = "http://createpolicyorderbypnrtxt.version2_0.webservice.liantuo.com", type = JAXBElement.class)
    protected JAXBElement<String> tradeNo;

    /**
     * Gets the value of the param1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParam1() {
        return param1;
    }

    /**
     * Sets the value of the param1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParam1(JAXBElement<String> value) {
        this.param1 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the payerAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPayerAccount() {
        return payerAccount;
    }

    /**
     * Sets the value of the payerAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPayerAccount(JAXBElement<String> value) {
        this.payerAccount = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the paymentUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPaymentUrl() {
        return paymentUrl;
    }

    /**
     * Sets the value of the paymentUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPaymentUrl(JAXBElement<String> value) {
        this.paymentUrl = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the settlePrice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSettlePrice() {
        return settlePrice;
    }

    /**
     * Sets the value of the settlePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSettlePrice(JAXBElement<Double> value) {
        this.settlePrice = ((JAXBElement<Double> ) value);
    }

    /**
     * Gets the value of the tradeNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTradeNo() {
        return tradeNo;
    }

    /**
     * Sets the value of the tradeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTradeNo(JAXBElement<String> value) {
        this.tradeNo = ((JAXBElement<String> ) value);
    }

}
