package com.ccservice.elong.hoteldb;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.XPath;

import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.inter.server.Server;

public class HotelDBElementHandler implements ElementHandler {

	@Override
	public void onEnd(ElementPath path) {
		// 获取当前节点
		Element row = path.getCurrent();
		// 对节点进行操作
		String tree = path.getPath().substring(26);
		if (tree.equals("HotelInfoForIndex")) {
			anayHotel(row);
		}
		// 处理当前节点后,将其从dom树种剪除
		row.detach();
	}

	@Override
	public void onStart(ElementPath path) {
	}

	@SuppressWarnings("unchecked")
	public void anayHotel(Element root){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<Element> list = root.elements();
		Hotel hotel = new Hotel();
		String isOk = "";
		String hotelCode = "";
		String modifytime = "";//此酒店最近一次修改时间
		for (Element e : list) {
			// 酒店是否可用  0表示可用，1表示酒店暂时关闭、 2表示酒店已删除
			if (e.getQName().getName().equals("Isreserve")) {
				isOk = e.getText()==null?"":e.getText().trim();
				continue;
			}
			// 酒店中文名
			if (e.getQName().getName().equals("Hotel_name")) {
				hotel.setName(e.getText());
				continue;
			}
			//酒店英文名
			if (e.getQName().getName().equals("Hotel_name_en")) {
				hotel.setEnname(e.getText());
				continue;
			}
			// 酒店id（酒店在艺龙系统中的唯一标识HotelID）
			if (e.getQName().getName().equals("Hotel_id")) {
				if(e.getText()!=null && !"".equals(e.getText().trim())){
					hotelCode = e.getText();
				}else{
					//艺龙酒店编码为空，酒店置为暂不可用，理论上不可能存在
					isOk = "1";
				}
				continue;
			}
			//修改时间
			if("Modifytime".equals(e.getQName().getName())){
				modifytime = e.getText()==null?"":e.getText().trim();
				continue;
			}
		}
		if(hotel.getEnname()==null || "".equals(hotel.getEnname().trim())){
			hotel.setEnname("");
		} 
		hotel.setSourcetype(1l);//表示来自于艺龙
		hotel.setHotelcode(hotelCode);
		if ("0".equals(isOk)) {
			hotel.setState(3);//酒店状态 是否可用 列名 表示当前酒店可用与否 0--表示暂时不可用 2--表示艺龙那边没有提供给我们房型 3--表示可用
			hotel.setStatedesc("该酒店可用");
		}else{
			hotel.setState(0);
			hotel.setStatedesc("该酒店暂不可用");
		}
		hotel.setElModifytime(modifytime);
		hotel.setLastupdatetime(sdf.format(new Date(System.currentTimeMillis())));
		List<Hotel> hotelList = Server.getInstance().getHotelService().findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotelCode + "'", "", -1, 0);
		String localModifytime = "";
		if (hotelList!=null && hotelList.size() > 0) {
			Hotel localHotel = hotelList.get(0);
			hotel.setId(localHotel.getId());
			//艺龙可用，本地不可用  --> 状态由状态、最低价格程序更新
			if(hotel.getState()==3 && localHotel.getState()!=null && localHotel.getState()!=3){
				hotel.setState(localHotel.getState());
			}
			localModifytime = localHotel.getElModifytime()==null?"":localHotel.getElModifytime().trim();
			if(localHotel.getEnname()==null || "".equals(localHotel.getEnname().trim())){
				localHotel.setEnname("");
			}
			if(!localModifytime.equals(modifytime) || !hotel.getName().equals(localHotel.getName()) ||
					!hotel.getEnname().equals(localHotel.getEnname()) || !hotel.getState().equals(localHotel.getState()) || 
						!hotel.getStatedesc().equals(localHotel.getStatedesc())){
				System.out.println("酒店更新……");
				Server.getInstance().getHotelService().updateHotelIgnoreNull(hotel);
			}
		} else {
			if(hotel.getState()==3){
				try {
					System.out.println("增加了一个新酒店……");
					hotel = Server.getInstance().getHotelService().createHotel(hotel);
				} catch (SQLException e) {
					isOk = "1";
				}
			}
		}
		//酒店可用且艺龙更近修改时间与库中不一致
		//不用"0".equals(isOk)是为了更新房型，融合数据用
		if (hotel!=null && hotel.getId()>0 && hotel.getHotelcode()!=null && !"".equals(hotel.getHotelcode().trim())) {//"0".equals(isOk)
			if("".equals(modifytime) || "".equals(localModifytime) || !modifytime.equals(localModifytime)){
				anay(hotel.getHotelcode(), hotel.getId());
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static void anay(String hotelCode, long hotelId) {
		String urltemp = "http://114-svc.elong.com/xml/v1.2/perhotelcn/" + hotelCode + ".xml";
		try {
			URL url = new URL(urltemp);
			URLConnection con = url.openConnection();
			//设置连接主机超时(单位:毫秒)
			con.setConnectTimeout(120000);
			//设置从主机读取数据超时(单位:毫秒)
			con.setReadTimeout(120000);
			//是否想url输出
			con.setDoOutput(true);
			//设定传送的内容类型是可序列化的对象
			con.setRequestProperty("Content-type", "application/x-java-serialized-object");
			String sCurrentLine;
			StringBuilder sTotalString=new StringBuilder();
			sCurrentLine = "";
			InputStream in = con.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
			while ((sCurrentLine = bf.readLine()) != null) {
				sTotalString.append(sCurrentLine);
			}
			Map map = new HashMap();
			map.put("q1:", "http://api.elong.com/staticInfo");
			org.dom4j.Document document = DocumentHelper.parseText(sTotalString.toString());
			XPath path = document.createXPath("HotelDetail");
			path.setNamespaceURIs(map);
			List nodelist = path.selectNodes(document);
			HotelDB.parseHotelXML(nodelist, hotelId);
			bf.close();
			in.close();
		} catch (Exception e) {
			
		}
	}
}
