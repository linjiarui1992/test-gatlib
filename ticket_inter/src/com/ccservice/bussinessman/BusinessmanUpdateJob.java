package com.ccservice.bussinessman;

import java.text.SimpleDateFormat;
import java.util.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.hmhotelprice.Hmhotelprice;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.roomtype.Roomtype;


/**
 * 生意人接口
 * @author WH
 */

public class BusinessmanUpdateJob {
	/**
	 * 更新生意人城市、区域
	 */
	public void updateCityAndRegion(){
		try {
			Server.getInstance().getIBussHotelService().getCityAndRegion();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 更新生意人酒店、房型、价格
	 */
	public void updateHotelInfo(){
		try {
			Server.getInstance().getIBussHotelService().getHotelRoomPrice();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 更新酒店价格
	 */
	@SuppressWarnings("unchecked")
	public void updateHotelPrice(){
		//本地生意人酒店
		List<Hotel> hotels = 
			Server.getInstance().getHotelService().
				findAllHotel("where C_SOURCETYPE=5 and C_CITYID is not null and C_HOTELCODE is not null and (C_PUSH != 2 or C_PUSH is null)",
							 "order by c_cityid ", -1, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeformat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(hotels!=null && hotels.size()>0){
			int count = hotels.size();
			for(Hotel h:hotels){
				System.out.println("更新生意人酒店==="+h.getName()+"===生意酒店CODE==="+h.getHotelcode()+"==="+(count--));
				try {
					Long hotelid = h.getId();//本地酒店ID
					Date currentStartDate = new Date();
					Date currentEndDate = CatchNext31Day(currentStartDate);
					String startDate = sdf.format(currentStartDate);
					String endDate = sdf.format(currentEndDate);
					//请求生意人
					String jsonresult = Server.getInstance().getIBussHotelService().getSingleHotel(h.getHotelcode(),"",startDate,endDate);
					//解析
					if(jsonresult!=null && !"".equals(jsonresult.trim()) &&
							!"{\"Table\":[{}],\"Table1\":[{}],\"Table2\":[{}],\"Table3\":[{}],\"Table4\":[{}]}".equals(jsonresult)){
						JSONObject obj = JSONObject.fromObject(jsonresult.replaceAll("\n", "").replaceAll("\\[\\{\\{", "\\[\\{").replaceAll("\\}\\}\\]", "\\}\\]"));
						//房型信息
						Map<String,Long> roommap = new HashMap<String,Long>();//<生意人房型编码,本地房型编码>，用于价格获取本地房型编码
						JSONArray Table1s = obj.getJSONArray("Table1");
						for (int i = 0; i < Table1s.size(); i++) {
							JSONObject Table1 = Table1s.getJSONObject(i);
							Roomtype room = new Roomtype();
							//生意人房型编码
							room.setRoomcode(Table1.getInt("DHI03Id")+"");
							room.setHotelid(hotelid);
							String roomname = Table1.getString("DHI11Name");
							int paytype = Table1.getInt("DHI08DOP02Id");//支付方式 : 1 转账 / 2 现付 / 3 全额预付
							int usertype = Table1.getInt("DHI10DUP02Id");//客户等级 ：1 散客 / 2同行
							if(roomname==null || "".equals(roomname.trim()) || paytype != 1 || usertype!=2){
								continue;
							}
							room.setName(roomname);
							room.setAreadesc(Table1.getString("DHI03Dimension"));//面积描述
							room.setRoomdesc(Table1.getString("DHI03Info"));//房型描述
							String DHI03DHP05Id = "BUS_" + Table1.getInt("DHI03DHP05Id");//增加生意人标识，床型ID
							String DHP05Name = Table1.getString("DHP05Name");//床型名称
							if(!"BUS_".equals(DHI03DHP05Id)){
								Bedtype bed = new Bedtype();
								bed.setType(DHI03DHP05Id);
								bed.setTypename(DHP05Name);
								List<Bedtype> localbeds = 
									Server.getInstance().getHotelService().
										findAllBedtype("where C_TYPE='"+DHI03DHP05Id+"'", "", -1, 0);
								if(localbeds==null||localbeds.size()==0){
									if(DHP05Name!=null && !"".equals(DHP05Name.trim())){
										bed = Server.getInstance().getHotelService().createBedtype(bed); 
										System.out.println("[录入生意人酒店相关的房型床型数据]向C_BEDTYPE表添加一条数据,添加成功,ID:"+ bed.getId());
									}else{
										continue;
									}
								}else{
									Bedtype localbed = localbeds.get(0);
									bed.setId(localbed.getId());
									if(localbed.getTypename()==null || !localbed.getTypename().equals(DHP05Name)){
										Server.getInstance().getHotelService().updateBedtypeIgnoreNull(bed);
										System.out.println("[录入生意人酒店相关的房型床型数据]更改C_BEDTYPE表一条数据,更改成功");
									}else{
										System.out.println("[录入生意人酒店相关的房型床型数据]匹配C_BEDTYPE表一条数据,匹配成功,ID:"+ bed.getId());
									}
								}
								room.setBed(Integer.parseInt(bed.getId()+""));
							}
							room.setState(1);//房型状态 - 1:可用
							room.setLayer(Table1.getString("DHI03Storey"));//楼层
							String bf = Table1.getString("DHI10BreakfastDesc");//早餐
							if(bf==null || "80001".equals(bf)){
								room.setBreakfast(0);//无早
							}else if("81000".equals(bf) || "81001".equals(bf) || "81101".equals(bf) || "81201".equals(bf) || "81311".equals(bf)){
								room.setBreakfast(1);//单早
							}else if("82001".equals(bf) || "82101".equals(bf) || "82201".equals(bf) || "82311".equals(bf)){
								room.setBreakfast(2);//双早
							}else if("83001".equals(bf)){
								room.setBreakfast(3);//三早
							}else if("84001".equals(bf)){
								room.setBreakfast(4);//四早
							}else if("86001".equals(bf)){
								room.setBreakfast(6);//六早
							}else{
								room.setBreakfast(0);//无早
							}
							String wideband = Table1.getString("DHI03IsOnLine");//宽带
							room.setWideband(0);
							if(wideband!=null && "1".equals(wideband)){
								String DHI03OnLineRemark = Table1.getString("DHI03OnLineRemark");
								if("宽带免费".equals(DHI03OnLineRemark) || "免费有线".equals(DHI03OnLineRemark)){
									room.setWideband(1);
									room.setWidedesc("免费");
								}
								if("收费有线".equals(DHI03OnLineRemark)){
									room.setWideband(2);
									room.setWidedesc("收费");
								}
							}
							room.setLanguage(0);
							room.setLastupdatetime(timeformat.format(new Date()));
							List<Roomtype> roomtypeList = 
								Server.getInstance().getHotelService().findAllRoomtype(
									"where C_ROOMCODE='" + room.getRoomcode()+ "' and  C_BED='"+ room.getBed()+ "' and C_HOTELID="+ room.getHotelid(), "", -1, 0);
							if (roomtypeList != null && roomtypeList.size() > 0) {
								room.setId(roomtypeList.get(0).getId());
								Server.getInstance().getHotelService().updateRoomtypeIgnoreNull(room);
								System.out.println("[录入生意人酒店相关的房型床型数据]更改C_ROOMTYPE表一条数据,更改成功");
							}else{
								room = Server.getInstance().getHotelService().createRoomtype(room);
								System.out.println("[录入生意人酒店相关的房型床型数据]向C_ROOMTYPE表添加一条数据,添加成功,ID:" + room.getId());
							}
							roommap.put(room.getRoomcode(), room.getId());
						}
						
						//-----------------------------------华丽的分隔线------------------------------------//
						
						//价格信息
						boolean deleteflag = false;
						JSONArray Table2s = obj.getJSONArray("Table2");
						for (int i = 0; i < Table2s.size(); i++) {
							JSONObject Table2 = Table2s.getJSONObject(i);
							Long roomid = roommap.get(Table2.getInt("DHI03Id")+"");
							int paytype = Table2.getInt("DHI08DOP02Id");//支付方式 : 1 转账 / 2 现付 / 3 全额预付
							int DHI10DUP02Id = Table2.getInt("DHI10DUP02Id");//客户等级 1 散客 / 2 同行
							if(paytype==1 && DHI10DUP02Id==2){
								//删除价格
								if(!deleteflag){
									Server.getInstance().getSystemService().findMapResultBySql(
											" delete from t_hmhotelprice where c_hotelid=" + h.getId() +
											" and C_STATEDATE<'"+endDate+"' and C_STATEDATE>='"+startDate+"'",null); 
									deleteflag = true;
								}
								if(roomid!=null && roomid>0){
									Hmhotelprice price = new Hmhotelprice();
									price.setHotelid(hotelid);
									price.setRoomtypeid(roomid);
									//提前预订天数
									String DHP09Days = Table2.getString("DHP09Days");
									if(DHP09Days==null||"".equals(DHP09Days.trim())){
										price.setAdvancedday(0l);
									}else{
										price.setAdvancedday(Long.parseLong(DHP09Days));
									}
									//早餐
									String bf = Table2.getString("DHI10BreakfastDesc");
									if(bf==null || "80001".equals(bf)){
										price.setBf(0l);//无早
									}else if("81000".equals(bf) || "81001".equals(bf) || "81101".equals(bf) || "81201".equals(bf) || "81311".equals(bf)){
										price.setBf(1l);//单早
									}else if("82001".equals(bf) || "82101".equals(bf) || "82201".equals(bf) || "82311".equals(bf)){
										price.setBf(2l);//双早
									}else if("83001".equals(bf)){
										price.setBf(3l);//三早
									}else if("84001".equals(bf)){
										price.setBf(4l);//四早
									}else if("86001".equals(bf)){
										price.setBf(6l);//六早
									}else{
										price.setBf(0l);//无早
									}
									price.setStatedate(sdf.format(sdf.parse(Table2.getString("DHI09Date"))));//日期
									price.setPrice(Table2.getDouble("DHI10Pay"));//价格
									if(price.getPrice().doubleValue()<=12){//生意人12块以下不卖，接口数据没作处理
										continue;
									}
									String DHP08Amount = Table2.getString("DHP08Amount");//最低预定量
									if(DHP08Amount==null || "".equals(DHP08Amount.trim())){
										price.setMinday(1l);//最少停留晚数
									}else{
										price.setMinday(Long.parseLong(DHP08Amount));//最少停留晚数
									}
									price.setPriceoffer(price.getPrice()+20);
									price.setAbleornot(1);
									price.setUpdatetime(timeformat.format(new Date()));
									String DHI18SurplusInventory = Table2.getString("DHI18SurplusInventory");// 房型剩余库存
									if(DHI18SurplusInventory!=null && !"".equals(DHI18SurplusInventory.trim())){
										price.setYuliuNum(Long.parseLong(DHI18SurplusInventory));
									}else{
										price.setYuliuNum(0l);
									}
									//房态
									String DHI18RoomState = Table2.getString("DHI18RoomState");
									if(DHI18RoomState==null || "".equals(DHI18RoomState.trim()) || "3".equals(DHI18RoomState)){
										price.setRoomstatus(0l);//申请
										price.setYuliuNum(0l);
										price.setIsallot("N");
									}else if("1".equals(DHI18RoomState)){
										price.setRoomstatus(0l);//即时确认
										price.setIsallot("Y");
									}else if("4".equals(DHI18RoomState)){
										price.setRoomstatus(0l);//紧张
										price.setIsallot("N");
									}else{
										price.setRoomstatus(1l);//满房
										price.setIsallot("C");
									}
									price.setCityid(h.getCityid()+"");
									price.setSourcetype("5");//生意人
									String DHI10DHP07Id = Table2.getString("DHI10DHP07Id");//促销等级
									String DHP07Name = Table2.getString("DHP07Name");//促销等级名称
									if(DHI10DHP07Id==null || "".equals(DHI10DHP07Id.trim()) ||
											DHP07Name==null || "".equals(DHP07Name.trim()) ||
												"1".equals(DHI10DHP07Id.trim())){
										price.setProd("1");//无
										price.setRatetype("");//价格类型名称
									}else{
										price.setProd(DHI10DHP07Id.trim());//4：限时房；5：特价房；6：限量房
										price.setRatetype(DHP07Name);//价格类型名称
									}
									price.setContractid("");
									price.setContractver("");
									price.setCur("RMB");
									price.setTicket("0");
									//取消规则
									price.setCanceldesc("");
									String DHI10DHP11Id = Table2.getString("DHI10DHP11Id");
									if(DHI10DHP11Id!=null && !"".equals(DHI10DHP11Id.trim())){
										String canceldesc = "";
										JSONArray Table4s = obj.getJSONArray("Table4");
										for (int j = 0; j < Table4s.size(); j++) {
											JSONObject Table4 = Table4s.getJSONObject(j);
											
											int DHP12RoomId = Table4.getInt("DHP12RoomId");
											int DHP12DHP11Id = Table4.getInt("DHP12DHP11Id");
											String DHP11Date = sdf.format(sdf.parse(Table4.getString("DHP11Date")));
											
											if(Table2.getInt("DHI03Id")==DHP12RoomId && 
													DHP12DHP11Id==Integer.parseInt(DHI10DHP11Id) && 
														price.getStatedate().equals(DHP11Date)){
												int DHP12DayCount = Table4.getInt("DHP12DayCount") - 1;//提前取消天数
												int DHP12Fee = Table4.getInt("DHP12Fee");//扣款类型
												if(DHP12DayCount<0){
													canceldesc += "取消收取";
												}else if(DHP12DayCount==0){
													canceldesc += "入住当天取消收取";
												}else{
													canceldesc += "入住前"+DHP12DayCount+"天取消收取";
												}
												if(DHP12Fee==1){
													canceldesc += "全额房费；";
												}else if(DHP12Fee==2){
													canceldesc += "首晚房费；";
												}else if(DHP12Fee==3){
													canceldesc += "最后一晚房费；";
												}else if(DHP12Fee==4){
													canceldesc += "最高一晚房费；";
												}else if(DHP12Fee==5){//扣款类型为“无”
													canceldesc += "全额房费；";
												}else if(DHP12Fee==6){
													canceldesc += "50%全额房费；";
												}else{
													canceldesc += "全额房费；";
												}
											}
										}
										if(canceldesc.endsWith("；")){//入住当天取消扣除首晚房费；
											canceldesc = canceldesc.substring(0, canceldesc.length()-1);
										}
										price.setCanceldesc(canceldesc);
									}
									price = Server.getInstance().getHotelService().createHmhotelprice(price);
									System.out.println("[录入生意人酒店相关的价格]向T_HMHOTELPRICE表添加一条数据,添加成功,ID:" + price.getId());
								}
							}
						}	
					}
				} catch (Exception e) {
					if(!"JSONObject[\"DHI03Id\"] is not a number.".equals(e.getMessage())){
						System.out.println("更新生意人酒店价格异常，异常信息为："+e.getMessage());
					}
				}
			}
		}
	}
	
	/**
	 * 获取当前日期31天后的日期
	 */
	private Date CatchNext31Day(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		Date newdate = cal.getTime();
		return newdate;
	}

}
