package com.cm.client;
import javax.xml.namespace.QName;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

public class AxisUtil {
	public static void main(String[] args) {
		
//		String xmlStr="<?xml version=\"1.0\" encoding=\"UTF-8\" ?><OrderRequest><Authentication><ServiceName>order.addOrder</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2015-07-22 15:29:22</TimeStamp><MessageIdentity>4cad761817839dda5d9ec5f691563662</MessageIdentity></Authentication><TrainOrderService><OrderNumber>2015072215292295733</OrderNumber><ChannelName>360_search</ChannelName><OrderTicketType>0</OrderTicketType><Order><OrderTime>2015-07-22 15:29:22</OrderTime><OrderMedia>pc</OrderMedia><TicketOffsetFee>0</TicketOffsetFee><Insurance>N</Insurance><Invoice>N</Invoice><PrivateCustomization>1</PrivateCustomization><TicketInfo><TicketItem><FromStationName>上海</FromStationName><ToStationName>汉口</ToStationName><TicketTime>2015-07-27 22:15</TicketTime><TrainNumber>K1152</TrainNumber><TicketPrice>122</TicketPrice><TicketCount>2</TicketCount><AuditTicketCount>2</AuditTicketCount><ChildTicketCount>0</ChildTicketCount><SeatName>硬座</SeatName><AcceptSeat>必须连号，否则无票，出错会造成赔款!</AcceptSeat><passport>张志梦,身份证,42128119871020656X,成人票,1987-10-20,0|卢柳灿,身份证,352229198206214533,成人票,1982-06-21,0</passport><OrderPrice>274</OrderPrice></TicketItem></TicketInfo></Order><User><UserID>54224353</UserID><UserName>张志梦</UserName><UserTel>--</UserTel><userLoginName>13564818486</userLoginName><UserMobile>13564818486</UserMobile></User><DeliverInfo><AreaID>14</AreaID><address>鹤望路365弄65号501室</address><OrderDeliverTypeID>440</OrderDeliverTypeID><DeliverPrice>20</DeliverPrice><WeekendDeliver>N</WeekendDeliver></DeliverInfo></TrainOrderService></OrderRequest>";
		String xmlStr="<?xml version=\"1.0\" encoding=\"utf-8\"?><OrderRequest xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><Authentication><ServiceName>order.pay</ServiceName><PartnerName>elong</PartnerName><TimeStamp>2015-07-28 16:12:09</TimeStamp><MessageIdentity>b62587ac52ebc8c8cf0ce3d467b4db0a</MessageIdentity></Authentication><TrainOrderService><OrderNumber>2015072215292295733</OrderNumber><PayedPrice>25.50</PayedPrice><PayTime>16:12:09</PayTime><PayType>yuejie</PayType><tradeNumber>JingyanT20150728160001</tradeNumber></TrainOrderService></OrderRequest>";
		String url="http://127.0.0.1:9010/ticket_inter/services/HelloWorld";
		String method="saySorry";
		AxisUtil.sendService(xmlStr,url,method);
	}
	public static String sendService(String xmlStr,String url,String method){
		String xml=null;
		try {
			
			RPCServiceClient serviceClient = new RPCServiceClient();
			Options options = serviceClient.getOptions();
			EndpointReference targetEPR = new EndpointReference(url);
			options.setTo(targetEPR);
			// 在创建QName对象时，QName类的构造方法的第一个参数表示WSDL文件的命名空间名，也就是<wsdl:definitions>元素的targetNamespace属性值
			 QName opAddEntry = new QName("http://service.cm.com",method);
			 // 参数，如果有多个，继续往后面增加即可，不用指定参数的名称
			 Object[] opAddEntryArgs = new Object[] {xmlStr};
			 // 返回参数类型，这个和axis1有点区别
			 // invokeBlocking方法有三个参数，其中第一个参数的类型是QName对象，表示要调用的方法名；
			 // 第二个参数表示要调用的WebService方法的参数值，参数类型为Object[]；
			 // 第三个参数表示WebService方法的返回值类型的Class对象，参数类型为Class[]。
			 // 当方法没有参数时，invokeBlocking方法的第二个参数值不能是null，而要使用new Object[]{}
			 // 如果被调用的WebService方法没有返回值，应使用RPCServiceClient类的invokeRobust方法，
			 // 该方法只有两个参数，它们的含义与invokeBlocking方法的前两个参数的含义相同
			 Class[] classes = new Class[] { String.class };
			 xml=(String)serviceClient.invokeBlocking(opAddEntry,opAddEntryArgs, classes)[0];
			 System.out.println(xml); 


		} catch (Exception e) {
			e.printStackTrace();
			long end = System.currentTimeMillis();
		}
		return xml;
	}
}