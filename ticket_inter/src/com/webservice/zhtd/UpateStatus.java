
package com.webservice.zhtd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FDIID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Clientname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientPwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datatype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fdiid",
    "clientname",
    "clientPwd",
    "datatype"
})
@XmlRootElement(name = "UpateStatus")
public class UpateStatus {

    @XmlElement(name = "FDIID")
    protected int fdiid;
    @XmlElement(name = "Clientname")
    protected String clientname;
    @XmlElement(name = "ClientPwd")
    protected String clientPwd;
    protected String datatype;

    /**
     * Gets the value of the fdiid property.
     * 
     */
    public int getFDIID() {
        return fdiid;
    }

    /**
     * Sets the value of the fdiid property.
     * 
     */
    public void setFDIID(int value) {
        this.fdiid = value;
    }

    /**
     * Gets the value of the clientname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientname() {
        return clientname;
    }

    /**
     * Sets the value of the clientname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientname(String value) {
        this.clientname = value;
    }

    /**
     * Gets the value of the clientPwd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientPwd() {
        return clientPwd;
    }

    /**
     * Sets the value of the clientPwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientPwd(String value) {
        this.clientPwd = value;
    }

    /**
     * Gets the value of the datatype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * Sets the value of the datatype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatatype(String value) {
        this.datatype = value;
    }

}
