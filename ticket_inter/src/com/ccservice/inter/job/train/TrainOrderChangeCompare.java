package com.ccservice.inter.job.train;

import net.sf.json.JSONObject;

import java.util.Comparator;

import com.ccservice.b2b2c.base.train.Trainorder;

public class TrainOrderChangeCompare implements Comparator<Trainorder> {

    // 对象的排序方式[升、降]   
    public static boolean sortASC = true;

    // 对象的排序属性   
    public static boolean sortByordertime = false;

    public static boolean sortBychangeordertime = false;

    public int compare(Trainorder o1, Trainorder o2) {
        int result = 0;
        // TODO Auto-generated method stub
        if (sortASC) {
            if (sortBychangeordertime) {
                String ordertime1 = JSONObject.fromObject(o1.getAutounionpayurl()).getString("time");
                String ordertime2 = JSONObject.fromObject(o2.getAutounionpayurl()).getString("time");
                result = ordertime1.compareTo(ordertime2);
            }
            else if (sortByordertime) {
            }
        }
        else {
            if (sortBychangeordertime) {
                String ordertime1 = JSONObject.fromObject(o1.getAutounionpayurl()).getString("time");
                String ordertime2 = JSONObject.fromObject(o2.getAutounionpayurl()).getString("time");
                result = -ordertime1.compareTo(ordertime2);
            }
            else if (sortByordertime) {
            }
        }
        return result;
    }
}
