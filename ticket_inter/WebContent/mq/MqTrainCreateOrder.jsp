<%@page import="com.alibaba.fastjson.JSONObject"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%
    String brokerURL = "failover:(tcp://192.168.0.40:61616/)";
    String name = "QueueMQ_trainorder_waitorder_orderid?consumer.prefetchSize=1";
    String type = request.getParameter("type");
    int MqTrainCreateOrderListSize = 0;
    try {
        if ("1".equals(type)) {
            com.ccservice.inter.servlet.QueueMQ_TrainCreateOrder.initMessageConsumer(1, brokerURL, name);
        }
        else if ("0".equals(type)) {
            com.ccservice.inter.servlet.QueueMQ_TrainCreateOrder.closeMessageConsumer(1);
        }
        MqTrainCreateOrderListSize = com.ccservice.inter.server.ServerQueueMQ.MqTrainCreateOrderList.size();
    }
    catch (Exception e) {
        e.printStackTrace();
    }
%>
<% 
JSONObject json=new JSONObject();
json.put("type",type);
json.put("brokerURL",brokerURL);
json.put("MqTrainCreateOrderListSize", MqTrainCreateOrderListSize);
%>
<%=json%>

