package com.ccservice.compareprice;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ComPareHotelGoodDataJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		long time1 = System.currentTimeMillis();
		try {
			new ComPareHotelGoodData().loadData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		long time2 = System.currentTimeMillis();
		System.out.println("遍历一遍用时：" + (time2 - time1) / 1000 / 60 + "分钟");
	}

}
