/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.domain;

/**
 * @className: com.ccservice.tuniu.train.domain.TrainOfflineMatchAgent
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月17日 上午9:29:33 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineMatchAgent {
    private Integer PkId;//
    private Integer createUid;//采购商ID
    private Integer agentId;//代售点ID
    private String provinces;//代售点所在省份
    private Integer status;//2-默认取值 1-随机取值
    private String note;//备注信息
    public Integer getPkId() {
        return PkId;
    }
    public void setPkId(Integer pkId) {
        PkId = pkId;
    }
    public Integer getCreateUid() {
        return createUid;
    }
    public void setCreateUid(Integer createUid) {
        this.createUid = createUid;
    }
    public Integer getAgentId() {
        return agentId;
    }
    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }
    public String getProvinces() {
        return provinces;
    }
    public void setProvinces(String provinces) {
        this.provinces = provinces;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    @Override
    public String toString() {
        return "TrainOfflineMatchAgent [PkId=" + PkId + ", createUid=" + createUid + ", agentId=" + agentId
                + ", provinces=" + provinces + ", status=" + status + ", note=" + note + "]";
    }
    
}
