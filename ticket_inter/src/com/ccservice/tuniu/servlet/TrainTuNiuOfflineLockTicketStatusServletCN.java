package com.ccservice.tuniu.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.util.ExceptionTNUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineLockOrderCallbackServlet
 * @description: TODO - 途牛的待出票的票的订单号锁单时间和锁单状态队列请求接口 - 
 * 
 * cn_home平台 请求该接口
 * 
 * 将异步的方式转换为同步的结果反馈
 * 
 * 向cn_home平台 进行 模拟的 - 同步反馈
 * 
 * 
 * 途牛的待出票的票的订单号锁单时间和锁单状态队列
 * 
 * 参照幂等，融入全局队列缓存
 * 
 * 二级队列，根据代售点进行区分
 * 
 * 20171031
 * 该类并入 - 闪送单的锁单的提醒的友情提示
 * 
 * @author: 郑州-技术-陈亚峰  E-mail:gwq20521@163.com
 * @createTime: 2017年8月18日 下午1:57:36 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTuNiuOfflineLockTicketStatusServletCN extends HttpServlet {
    private static final String LOGNAME = "途牛的待出票的票的订单号锁单时间和锁单状态队列请求接口-CN发起";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String userid = request.getParameter("userid");//当前系统的登录用户的ID - 用于记录操作日志
        Integer useridi = Integer.valueOf(userid);

        //记录请求信息日志
        //WriteLog.write(LOGNAME, r1 + ":途牛的待出票的票的订单号锁单时间和锁单状态队列请求信息-userid-->" + useridi);

        JSONObject result = lockTicketStatusCN(useridi);

        //WriteLog.write(LOGNAME, r1 + ":途牛的待出票的票的订单号锁单时间和锁单状态队列请求结果-result" + result);

        out.print(result.toJSONString());
        out.flush();
        out.close();
    }

    private JSONObject lockTicketStatusCN(Integer useridi) {
        //获取途牛的相关的订单的出票状态和锁票状态的信息
        JSONObject resResult = new JSONObject();

        /*resResult.put("success", "false");
        resResult.put("msg", "不存在相关标识订单信息");*/

        //WriteLog.write(LOGNAME, r1 + ":途牛的待出票的票的订单号锁单时间和锁单状态队列请求-进入到方法-lockTicketStatusCN:useridi-->"+useridi);

        if (useridi == null || useridi == 0L) {
            resResult.put("success", "false");
            resResult.put("msg", "传入的用户的ID不正确");
            return resResult;
        }

        //锁单的结果的提示集
        List<TrainOrderOffline> trainOrderOfflineList = new ArrayList<TrainOrderOffline>();

        try {
            trainOrderOfflineList = trainOrderOfflineDao.findTrainOrderOfflineLockTicketByAgentId(useridi);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }

        //途牛的锁单的结果的提示集
        /*List<TrainOrderOffline> trainOrderOfflineListTN = new ArrayList<TrainOrderOffline>();
        try {
            trainOrderOfflineListTN = trainOrderOfflineDao.findTrainOrderOfflineLockTicketTNByAgentId(useridi);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }
        
        //同程的锁单的结果的提示集
        List<TrainOrderOffline> trainOrderOfflineListTC = new ArrayList<TrainOrderOffline>();
        try {
            trainOrderOfflineListTC = trainOrderOfflineDao.findTrainOrderOfflineLockTicketTCByAgentId(useridi);
        }
        catch (Exception e) {
            return ExceptionTNUtil.handleTNException(e);
        }
        
        trainOrderOfflineList.addAll(trainOrderOfflineListTN);
        trainOrderOfflineList.addAll(trainOrderOfflineListTC);*/

        if (trainOrderOfflineList == null || trainOrderOfflineList.size() <= 0) {
            resResult.put("success", "false");
            resResult.put("msg", "不存在相关订单信息");
            return resResult;
        }

        JSONArray resArray = new JSONArray();

        for (int i = 0; i < trainOrderOfflineList.size(); i++) {
            JSONObject resObject = new JSONObject();

            TrainOrderOffline trainOrderOffline = trainOrderOfflineList.get(i);
            String orderId = trainOrderOffline.getOrderNumber();
            resObject.put("orderId", orderId);

            Integer lockedStatus = trainOrderOffline.getLockedStatus();//0表示未被锁定，1表示被锁定

            //首先判定是否是闪送单 - 闪送单是闪送反馈和锁单反馈的二合一版本 - 否则是单独的版本
            Integer isRapidSend = trainOrderOffline.getIsRapidSend();//20171023-新增需求 - 是否是闪送订单  0: 普通订单,1: 闪送订单 - 默认值是 0 //通过快递类型进行判定 - 目前只有邮寄票会是闪送订单

            if (isRapidSend == null) {
                isRapidSend = 0;
            }

            if (isRapidSend == 0) {//普通订单的锁单提醒 - 途牛
                if (trainOrderOffline.getCreateUId() == 80) {//只有途牛才会走入该提醒逻辑
                    if (trainOrderOffline.getIsLockCallback() == null) {
                        trainOrderOffline.setIsLockCallback(false);
                    }

                    if (trainOrderOffline.getIsLockCallback()) {//已反馈
                        resObject.put("success", "true");
                        if (lockedStatus == 1) {
                            //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                            resObject.put("msg", "锁单成功，请尽快出票");
                        }
                        else if (lockedStatus == 2) {
                            //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                            resObject.put("msg", "锁单失败，请选择拒单");
                        }
                        else if (lockedStatus == 4) {
                            //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                            resObject.put("msg", "锁单已超时，请选择拒单");
                        }
                    }
                    else {//未反馈
                        resObject.put("success", "false");
                        resObject.put("msg", "锁单结果未反馈，超时系统会自动拒单");
                    }
                }
            }
            else {//闪送订单的提醒 - 目前只有同程会走入此逻辑

                if (trainOrderOffline.getIsRapidSendCallback() == null) {
                    trainOrderOffline.setIsRapidSendCallback(false);
                }

                if (trainOrderOffline.getIsRapidSendCallback()) {//先判断闪送是否有反馈

                    if (trainOrderOffline.getIsRapidSendSuccess() == null) {
                        trainOrderOffline.setIsRapidSendSuccess(false);
                    }

                    if (trainOrderOffline.getIsRapidSendSuccess()) {

                        if (trainOrderOffline.getIsLockCallback() == null) {
                            trainOrderOffline.setIsLockCallback(false);
                        }

                        if (trainOrderOffline.getIsLockCallback()) {//再判定锁单是否有反馈
                            resObject.put("success", "true");
                            if (lockedStatus == 1) {
                                //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                                resObject.put("msg", "锁单成功，请尽快出票");
                            }
                            else if (lockedStatus == 2) {
                                //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                                resObject.put("msg", "锁单失败，请选择拒单");
                            }
                            else if (lockedStatus == 4) {
                                //resObject.put("suodanTime", TrainOrderOfflineUtil.getTuNiuReqTimestamp());
                                resObject.put("msg", "锁单已超时，请选择拒单");
                            }
                        }
                        else {//未反馈
                            resObject.put("success", "false");
                            resObject.put("msg", "闪送已接单，等待锁单结果");
                        }
                    }
                    else {
                        resObject.put("success", "false");
                        resObject.put("msg", "闪送接单失败，请联系客服处理");
                    }
                }
                else {
                    if (lockedStatus == 5) {
                        resObject.put("success", "false");
                        resObject.put("msg", "闪送接单超时，请联系客服处理");
                    }
                    else if (lockedStatus == 6) {
                        resObject.put("success", "false");
                        resObject.put("msg", "客服正在二次闪送下单锁单操作");
                    }
                    else {
                        resObject.put("success", "false");
                        resObject.put("msg", "闪送接单结果未反馈");
                    }
                }
            }

            resArray.add(resObject);
            resResult.put("resArray", resArray);
        }

        //WriteLog.write(LOGNAME, r1 + ":途牛的待出票的票的订单号锁单时间和锁单状态队列请求-进入到方法-lockTicketStatusCN:resResult-->"+resResult);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        /*HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();
        
        String param2 = "?userid=" + 382;
        
        String tuniuLockTicketStatusUrl = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineLockTicketStatusServletCN";
        
        String resLTS = "";//
        try {
            resLTS = new HttpUtil().doGet(tuniuLockTicketStatusUrl, param2);
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        
        System.out.println(resLTS);*/

        TrainTuNiuOfflineLockTicketStatusServletCN trainTuNiuOfflineLockTicketStatusServletCN = new TrainTuNiuOfflineLockTicketStatusServletCN();
        System.out.println(trainTuNiuOfflineLockTicketStatusServletCN.lockTicketStatusCN(382));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
