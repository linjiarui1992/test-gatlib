package com.ccservice.elong.inter;

import java.rmi.RemoteException;

import com.ccservice.elong.base.NorthBoundAPIServiceStub;
import com.ccservice.elong.base.NorthBoundAPIServiceStub.GetHotelOrderDetailByIdResponse;
import com.ccservice.elong.base.NorthBoundAPIServiceStub.GetHotelOrderDetailByOrderIdRequest;

/**
 * 根据OrderID获取订单
 * 
 * @author 师卫林
 * 
 */
public class ELongGetHotelOrderDetailByOrderId {
	public static void main(String[] args) throws RemoteException {
		// 订单ID
		int HotelOrderId = 100087683;
		getDetail(HotelOrderId);
	}

	public static void getDetail(int HotelOrderId) throws RemoteException {
		NorthBoundAPIServiceStub stub = new NorthBoundAPIServiceStub();
		NorthBoundAPIServiceStub.GetHotelOrderDetailById detailById = new NorthBoundAPIServiceStub.GetHotelOrderDetailById();
		GetHotelOrderDetailByIdResponse byOrderIdResponse = new GetHotelOrderDetailByIdResponse();
		GetHotelOrderDetailByOrderIdRequest byOrderIdRequest = new GetHotelOrderDetailByOrderIdRequest();

		byOrderIdRequest.setHotelOrderID(HotelOrderId);
		byOrderIdRequest.setRequestHead(ElongRequestHead.getRequestHead(""));

		detailById.setGetHotelOrderDetailByIdRequest(byOrderIdRequest);

		byOrderIdResponse = stub.getHotelOrderDetailById(detailById);
//		System.out.println("结果代码:"+byOrderIdResponse.getGetHotelOrderDetailByIdResult()
//				.getResponseHead().getResultCode());
//		System.out.println("结果信息:"+byOrderIdResponse.getGetHotelOrderDetailByIdResult()
//				.getResponseHead().getResultMessage());
		NorthBoundAPIServiceStub.HotelOrderForGetOrderByID hotelOrderForGetOrderByID = byOrderIdResponse
				.getGetHotelOrderDetailByIdResult().getHotelOrder();
		// 订单ID
		System.out.println("订单ID:"+hotelOrderForGetOrderByID.getHotelOrderId());
		// 订单状态
		System.out.println("订单状态:"+hotelOrderForGetOrderByID.getOrderStatusCode());
		// 货币代码
		System.out.println("货币代码:"+hotelOrderForGetOrderByID.getCurrencyCode());
		// 总价
		System.out.println("总价:"+hotelOrderForGetOrderByID.getOrderTotalPrice());
		NorthBoundAPIServiceStub.RoomForGetOrderByID[] roomForGetOrderByID = hotelOrderForGetOrderByID
				.getRoomGroups().getRoom();
		if (roomForGetOrderByID != null && roomForGetOrderByID.length > 0) {
			for (int i = 0; i < roomForGetOrderByID.length; i++) {
				NorthBoundAPIServiceStub.RoomForGetOrderByID roomForGetOrderByID2 = roomForGetOrderByID[i];
				// 酒店ID
				System.out.println("酒店ID:"+roomForGetOrderByID2.getHotelId());
				// 房型ID
				System.out.println("房型ID:"+roomForGetOrderByID2.getRoomTypeId());
				// RatePlanID
				System.out.println("RatePlanID:"+roomForGetOrderByID2.getRatePlanID());
//				// 价格代码code
//				System.out.println(roomForGetOrderByID2.getRatePlanCode());
				// 入住时间
				System.out.println("入住时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getCheckInDate()));
				// 离店时间
				System.out.println("离店时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getCheckOutDate()));
				// ELONG卡号
				System.out.println("ELONG卡号:"+roomForGetOrderByID2.getElongCardNo());
				// 宾客类型代码
				System.out.println("宾客类型代码:"+roomForGetOrderByID2.getGuestTypeCode());
				// 房间数量
				System.out.println("房间数量:"+roomForGetOrderByID2.getRoomAmount());
				// 入住人数
				System.out.println("入住人数:"+roomForGetOrderByID2.getGuestAmount());
				// 支付方式
				System.out.println("支付方式:"+roomForGetOrderByID2.getPaymentTypeCode());
				// 最早到达时间
				System.out.println("最早到达时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getArrivalEarlyTime()));
				// 最晚到达时间
				System.out.println("最晚到达时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getArrivalLateTime()));
				// 货币代码
				System.out.println("货币代码:"+roomForGetOrderByID2.getCurrencyCode());
				// 总价
				System.out.println("总价:"+roomForGetOrderByID2.getTotalPrice());
				// 订单确认方式
				System.out.println("订单确认方式:"+roomForGetOrderByID2.getConfirmTypeCode());
				// 订单确认语言
				System.out.println("订单确认语言:"+roomForGetOrderByID2
						.getConfirmLanguageCode());
//				// 给酒店备注
//				System.out.println(roomForGetOrderByID2.getNoteToHotel());
//				// 给艺龙备注
//				System.out.println(roomForGetOrderByID2.getNoteToElong());
				// 最晚取消时间
				System.out.println("最晚取消时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getCancelDeadline()));
				NorthBoundAPIServiceStub.ContacterForGetOrderByID[] contacterForGetOrderByIDs = roomForGetOrderByID2
						.getContacters().getContacter();
				if (contacterForGetOrderByIDs != null
						&& contacterForGetOrderByIDs.length > 0) {
					for (int j = 0; j < contacterForGetOrderByIDs.length; j++) {
						NorthBoundAPIServiceStub.ContacterForGetOrderByID contacterForGetOrderByID = contacterForGetOrderByIDs[j];
						// 姓名
						System.out.println("姓名:"+contacterForGetOrderByID.getName());
//						// 姓名代码
//						System.out.println(contacterForGetOrderByID
//								.getGenderCode());
//						// Email
//						System.out.println(contacterForGetOrderByID.getEmail());
//						// 手机
//						System.out
//								.println(contacterForGetOrderByID.getMobile());
//						// 证件类别代码
//						System.out.println(contacterForGetOrderByID
//								.getIdTypeCode());
//						// 证件号码
//						System.out.println(contacterForGetOrderByID
//								.getIdNumber());
//						NorthBoundAPIServiceStub.PhoneForGetOrderByID phoneForGetOrderByID = contacterForGetOrderByID
//								.getPhone();
//						// 国际区号
//						System.out.println(phoneForGetOrderByID.getInterCode());
//						// 国内区号
//						System.out.println(phoneForGetOrderByID.getAreaCode());
//						// 电话号码
//						System.out.println(phoneForGetOrderByID.getNmber());
//						// 分机号
//						System.out.println(phoneForGetOrderByID.getExt());
//						NorthBoundAPIServiceStub.FaxForGetOrderByID faxForGetOrderByID = contacterForGetOrderByID
//								.getFax();
//						// 国际区号
//						System.out.println(faxForGetOrderByID.getInterCode());
//						// 国内区号
//						System.out.println(faxForGetOrderByID.getAreaCode());
//						// 电话号码
//						System.out.println(faxForGetOrderByID.getNmber());
//						// 分机号
//						System.out.println(faxForGetOrderByID.getExt());
						System.out.println();
					}
				}
//				NorthBoundAPIServiceStub.CreditCardForGetOrderByID cardForGetOrderByID = roomForGetOrderByID2
//						.getCreditCard();
//				// 信用卡号
//				System.out.println(cardForGetOrderByID.getNumber());
//				// 校验码
//				System.out.println(cardForGetOrderByID.getVeryfyCode());
//				// 有效期年
//				System.out
//						.println("有效期年:" + cardForGetOrderByID.getValidYear());
//				// 有效期月
//				System.out.println("有效期月:"
//						+ cardForGetOrderByID.getValidMonth());
//				// 持卡人姓名
//				System.out.println(cardForGetOrderByID.getCardHolderName());
//				// 证件类别代码
//				System.out.println(cardForGetOrderByID.getIdTypeCode());
//				// 证件号码
//				System.out.println(cardForGetOrderByID.getIdNumber());
				NorthBoundAPIServiceStub.GuestForGetOrderByID[] guestForGetOrderByIDs = roomForGetOrderByID2
						.getGuests().getGuest();
				if (guestForGetOrderByIDs != null
						&& guestForGetOrderByIDs.length > 0) {
					for (int j = 0; j < guestForGetOrderByIDs.length; j++) {
						NorthBoundAPIServiceStub.GuestForGetOrderByID guestForGetOrderByID = guestForGetOrderByIDs[j];
						// 姓名
						System.out.println("姓名:"+guestForGetOrderByID.getName());
//						// 性别代码
//						System.out
//								.println(guestForGetOrderByID.getGenderCode());
//						// Email
//						System.out.println(guestForGetOrderByID.getEmail());
//						// 手机
//						System.out.println(guestForGetOrderByID.getMobile());
//						// 证件类别
//						System.out
//								.println(guestForGetOrderByID.getIdTypeCode());
//						// 证件号码
//						System.out.println(guestForGetOrderByID.getIdNumber());
//						NorthBoundAPIServiceStub.PhoneForGetOrderByID phoneForGetOrderByID = guestForGetOrderByID
//								.getPhone();
//						// 国际区号
//						System.out.println(phoneForGetOrderByID.getInterCode());
//						// 国内区号
//						System.out.println(phoneForGetOrderByID.getAreaCode());
//						// 电话号码
//						System.out.println(phoneForGetOrderByID.getNmber());
//						// 分机号
//						System.out.println(phoneForGetOrderByID.getExt());
//						NorthBoundAPIServiceStub.FaxForGetOrderByID faxForGetOrderByID = guestForGetOrderByID
//								.getFax();
//						// 国际区号
//						System.out.println(faxForGetOrderByID.getInterCode());
//						// 国内区号
//						System.out.println(faxForGetOrderByID.getAreaCode());
//						// 电话号码
//						System.out.println(faxForGetOrderByID.getNmber());
//						// 分机号
//						System.out.println(faxForGetOrderByID.getExt());
						System.out.println();
					}
				}
				NorthBoundAPIServiceStub.RatesForGetOrderByID ratesForGetOrderByID = roomForGetOrderByID2
						.getRates();
				// 总价
				System.out.println("总价:"+ratesForGetOrderByID.getTotalPrice());
				// 货币代码
				System.out.println("货币代码:"+ratesForGetOrderByID.getCurrencyCode());
				NorthBoundAPIServiceStub.RateForGetOrderByID[] rateForGetOrderByIDs = ratesForGetOrderByID
						.getRates().getRate();
				if (rateForGetOrderByIDs != null
						&& rateForGetOrderByIDs.length > 0) {
					for (int j = 0; j < rateForGetOrderByIDs.length; j++) {
						NorthBoundAPIServiceStub.RateForGetOrderByID rateForGetOrderByID = rateForGetOrderByIDs[j];
						// 对应日期
						System.out.println("对应日期:"
								+ DateSwitch
										.SwitchString(rateForGetOrderByID
												.getDate()));
						// 货币代码
						System.out.println("货币代码:"+rateForGetOrderByID
								.getCurrencyCode());
						// 门市价
						System.out.println("门市价:"+rateForGetOrderByID.getRetailRate());
						// 会员价格
						System.out.println("会员价格:"+rateForGetOrderByID.getMemberRate());
						// 加床价格
						System.out.println("加床价格:"+rateForGetOrderByID.getAddBedRate());
						System.out.println();
					}
				}
//				NorthBoundAPIServiceStub.GaranteeRuleForGetOrderByID[] garanteeRules = roomForGetOrderByID2
//						.getGaranteeRules().getGaranteeRule();
//				if (garanteeRules != null && garanteeRules.length > 0) {
//					for (int m = 0; m < garanteeRules.length; m++) {
//						NorthBoundAPIServiceStub.GaranteeRuleForGetOrderByID garanteeRule = garanteeRules[m];
//						// 担保规则类型
//						System.out.println(garanteeRule
//								.getGaranteeRulesTypeCode());
//						System.out.println(garanteeRule.getDescription());
//						System.out.println(garanteeRule.getRuleValues());
//					}
//				}
//				NorthBoundAPIServiceStub.BookingRuleForGetOrderByID[] bookingRuless = roomForGetOrderByID2
//						.getBookingRules().getBookingRule();
//				if (bookingRuless != null && bookingRuless.length > 0) {
//					for (int m = 0; m < bookingRuless.length; m++) {
//						NorthBoundAPIServiceStub.BookingRuleForGetOrderByID bookingRule = bookingRuless[m];
//						// 预定规则类型代码
//						System.out
//								.println(bookingRule.getBookingRuleTypeCode());
//						System.out.println(bookingRule.getRuleValues());
//						System.out.println(bookingRule.getDescription());
//					}
//				}
//				NorthBoundAPIServiceStub.AddValueForGetOrderByID[] addVaules = roomForGetOrderByID2
//						.getAddValues().getAddValue();
//				if (addVaules != null && addVaules.length > 0) {
//					for (int m = 0; m < addVaules.length; m++) {
//						NorthBoundAPIServiceStub.AddValueForGetOrderByID addVaule = addVaules[m];
//						// 增值服务ID
//						System.out.println(addVaule.getAddValueID());
//						// 业务代码
//						System.out.println(addVaule.getBusinessCode());
//						// 是否包含在房费中
//						System.out.println(addVaule.getIsInclude());
//						// 包含的分数
//						System.out.println(addVaule.getIncludedCount());
//						// 货币代码
//						System.out.println(addVaule.getCurrencyCode());
//						// 单价默认选项
//						System.out.println(addVaule.getPriceDefaultOption());
//						// 单价
//						System.out.println(addVaule.getPriceNumber());
//						// 是否单加
//						System.out.println(addVaule.getIsAdd());
//						// 单加单价默认选项
//						System.out.println(addVaule.getSinglePriceOption());
//						// 单加价格
//						System.out.println(addVaule.getSinglePrice());
//						// 附加服务描述
//						System.out.println(addVaule.getDescription());
//					}
//				}
//				// 增值服务信息描述
//				System.out.println(roomForGetOrderByID2
//						.getAddValuesDescription());
				// 订单生成时间
				System.out.println("订单生成时间:"
						+ DateSwitch.SwitchString(roomForGetOrderByID2
								.getBookingTime()));
			}
			System.out.println();
		}

	}
}
