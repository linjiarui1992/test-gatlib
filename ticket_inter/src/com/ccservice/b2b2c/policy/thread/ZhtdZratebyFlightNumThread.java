package com.ccservice.b2b2c.policy.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.ccservice.b2b2c.base.zrate.Zrate;
import com.ccservice.b2b2c.policy.ZongHengTianDiMethod;

/**
 * 
 * @author 贾建磊
 * 
 */
public class ZhtdZratebyFlightNumThread implements Callable<List<Zrate>> {

	String scity;
	String ecity;
	String sdate;
	String flightnumber;
	String cabin;

	public ZhtdZratebyFlightNumThread() {
	}

	public ZhtdZratebyFlightNumThread(String scity, String ecity, String sdate, String flightnumber, String cabin) {
		this.scity = scity;
		this.ecity = ecity;
		this.sdate = sdate;
		this.flightnumber = flightnumber;
		this.cabin = cabin;
	}

	@Override
	public List<Zrate> call() throws Exception {
		List<Zrate> zhtdZrates = new ArrayList<Zrate>();
		try {
			zhtdZrates = ZongHengTianDiMethod.GetSinglePolicyListV2(sdate, scity, ecity, flightnumber.substring(0, 2),cabin,flightnumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return zhtdZrates;
	}
}
