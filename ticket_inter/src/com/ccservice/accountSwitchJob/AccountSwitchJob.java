package com.ccservice.accountSwitchJob;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


public class AccountSwitchJob implements Job{

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		new AccountSwitchLogic().work();
	}


}
