package com.ccservice.jielv.hotelupdate;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 持续更新酒店房型job-3
 */
public class UpdatejlhrJob3  implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("开始更新-捷旅-持续更新酒店房型job...");
		JLUpdateJob.updatejlhrJob();
		System.out.println("结束更新--捷旅-持续更新酒店房型job...");
	}

}
