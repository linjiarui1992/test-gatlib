package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
/**
 * 
 * @author wzc
 * 华闽酒店比价程序
 *
 */
public class ComPriceDBJob implements Job{

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if(Utils.getFlag()){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				System.out.println("开始更新："+sdf.format(new Date(System.currentTimeMillis())));
				new ComPriceDB().getHotelDatas();
				System.out.println("更新结束："+sdf.format(new Date(System.currentTimeMillis())));
			}else{
				System.out.println("当前时间不能更新：23:50--01:10更新没数据");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
