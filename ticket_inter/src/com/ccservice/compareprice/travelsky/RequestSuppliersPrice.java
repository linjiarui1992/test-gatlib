package com.ccservice.compareprice.travelsky;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.ccservice.inter.server.Server;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.compareprice.HotelData;
import com.ccservice.compareprice.PropertyUtil;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.hmhotelprice.JLPriceResult;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.atom.hotel.ElongHotelInterfaceUtil;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceDetail;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXQueryHotelRequest;
import com.ccservice.b2b2c.atom.service.travelskyhotel.bean.HXHotelPriceResponse;

/**
 * 酒店供应商房型价格
 */

public class RequestSuppliersPrice {

    private static String separate = "@.@";//与SuppliersComparePriceWithQunarJob需一致

    private static String travelSkyRoomStatus = PropertyUtil.getValue("travelSkyRoomStatus").trim();

    private static String qunarPriceUrl = PropertyUtil.getValue("qunarPriceUrl").trim();//去哪儿价格地址

    //加载中航信数据
    public static Map<String, HXHotelPriceResponse> loadHX(Hotel hotel, String startDate, String endDate)
            throws Exception {
        //REQ
        HXQueryHotelRequest req = new HXQueryHotelRequest();
        req.setHotelCode(hotel.getHotelcode().trim());
        req.setHotelId(hotel.getId());
        req.setCheckInDate(startDate);
        req.setCheckOutDate(endDate);
        //POST
        List<HXHotelPriceResponse> list = Server.getInstance().getTravelskyHotelService().travelskySingleHotels(req);
        //Map，用于取最低价格
        Map<String, HXHotelPriceResponse> lowestMap = new HashMap<String, HXHotelPriceResponse>();//key：RoomCode+BF
        //中航信有房的状态，onRequest:可申请;avail:即时确认;noavail:不可用
        String[] hxstatus = travelSkyRoomStatus.split(",");
        List<String> haveRoomList = new ArrayList<String>();
        for (String status : hxstatus) {
            haveRoomList.add(status.trim());
        }
        for (HXHotelPriceResponse res : list) {
            //宾客类型(内宾D; 外宾F; A或空则表示通用价)、房型为其他
            if ("F".equals(res.getGuestTypeIndicator()) || res.getBedType() == -1) {
                continue;
            }
            //判断是否全部满房
            int count = 0;
            Map<String, HXHotelPriceDetail> rates = res.getRates();
            for (String date : rates.keySet()) {
                HXHotelPriceDetail detail = rates.get(date);
                if (haveRoomList.contains(detail.getRoomStatus())) {
                    count++;
                    break;
                }
            }
            //全满房
            if (count == 0) {
                continue;
            }
            double totalPrice = res.getTotalAmountPrice();//总价
            String roomTypeName = res.getRoomTypeName();//房型名称
            if (roomTypeName.endsWith("房")) {
                roomTypeName = roomTypeName.substring(0, roomTypeName.length() - 1) + "间";
            }
            int freeMeal = rates.get(startDate).getFreeMeal();//早餐
            String key = roomTypeName + separate + freeMeal;//同房型、同早餐
            if (lowestMap.containsKey(key)) {
                double oldTotalPrice = lowestMap.get(key).getTotalAmountPrice();
                if (totalPrice < oldTotalPrice) {
                    lowestMap.put(key, res);
                }
            }
            else {
                lowestMap.put(key, res);
            }
        }
        return lowestMap;
    }

    /**
     * 中航信数据转换为本地
     */
    public static HotelGoodData hxDataToLocal(HXHotelPriceResponse res, HXHotelPriceDetail detail, String date) {
        Hotel local = res.getLocalHotel();
        //NEW
        HotelGoodData hd = new HotelGoodData();
        hd.setHotelid(local.getId());
        hd.setHotelname(local.getName());
        hd.setRoomtypeid(res.getLocalRoomId());
        hd.setRoomtypename(res.getRoomTypeName());
        hd.setBedtypeid(Long.valueOf(res.getBedType()));
        hd.setBaseprice(Double.valueOf(detail.getAmountPrice()).longValue());
        hd.setProdid(res.getVendorCode());
        hd.setRatetype(res.getRatePlanName());
        hd.setRatetypeid(res.getRatePlanCode());
        hd.setJlroomtypeid(res.getRoomTypeCode());
        hd.setDatenum(date);
        String uniqueId = hd.getHotelid() + separate + hd.getRoomtypeid() + separate + hd.getRatetypeid() + separate
                + res.getVendorCode() + separate + hd.getDatenum();
        try {
            hd.setJlkeyid(ElongHotelInterfaceUtil.MD5(uniqueId));//用于确定唯一，航信酒店编码+房型编码+价格计划编码+供应商+日期确定唯一
        }
        catch (Exception e) {
        }
        hd.setMinday(Long.valueOf(res.getMinDay()));
        hd.setBeforeday(Long.valueOf(res.getBeforeDay()));
        hd.setYuliunum(Long.valueOf(detail.getQuantity()));
        //中航信有房的状态，onRequest:可申请;avail:即时确认;noavail:不可用
        String[] hxstatus = travelSkyRoomStatus.split(",");
        List<String> haveRoomList = new ArrayList<String>();
        for (String status : hxstatus) {
            haveRoomList.add(status.trim());
        }
        hd.setJlf(res.getBedName());
        hd.setJlft(detail.getRoomStatus());
        //去哪儿房态(0：开房  ；1：关房)
        hd.setRoomstatus(haveRoomList.contains(detail.getRoomStatus()) ? 0l : 1l);
        hd.setBfcount(Long.valueOf(detail.getFreeMeal()));
        hd.setWeb(res.getInternet());
        hd.setSorucetype("12");
        hd.setCityid(String.valueOf(local.getCityid()));
        return hd;
    }

    //加载深捷旅数据
    @SuppressWarnings("unchecked")
    public static Map<String, List<HotelGoodData>> loadJL(Hotel hotel, String startDate, String endDate) {
        //Return
        Map<String, List<HotelGoodData>> map = new HashMap<String, List<HotelGoodData>>();
        //Check
        Map<String, Boolean> repeatMap = new HashMap<String, Boolean>();//重复天数，有问题数据
        Map<String, String> checkRepeatMap = new HashMap<String, String>();
        //本地房型
        String where = "where C_ROOMCODE != '' and C_HOTELID  = " + hotel.getId();
        List<Roomtype> localRoomList = Server.getInstance().getHotelService().findAllRoomtype(where, "", -1, 0);
        if (localRoomList == null || localRoomList.size() == 0) {
            return map;
        }
        //请求深捷旅
        List<JLPriceResult> jlResults = Server.getInstance().getIJLHotelService()
                .getHotelPriceByRoom(hotel.getHotelcode().trim(), "", "", startDate, endDate);
        if (jlResults == null || jlResults.size() == 0) {
            return map;
        }
        Map<String, Roomtype> localRoomMap = new HashMap<String, Roomtype>();
        for (Roomtype r : localRoomList) {
            localRoomMap.put(r.getRoomcode().trim(), r);
        }
        for (JLPriceResult jl : jlResults) {
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getJlroomtype())
                    || "null".equalsIgnoreCase(jl.getJlroomtype().trim())
                    || ElongHotelInterfaceUtil.StringIsNull(jl.getRatetypeid())
                    || "null".equalsIgnoreCase(jl.getRatetypeid().trim())
                    || ElongHotelInterfaceUtil.StringIsNull(jl.getStayDate())
                    || "null".equalsIgnoreCase(jl.getStayDate().trim())
                    || ElongHotelInterfaceUtil.StringIsNull(jl.getPprice())
                    || "null".equalsIgnoreCase(jl.getPprice().trim()) || "外宾".equals(jl.getRatetype())) {
                continue;
            }
            String jlRoomId = jl.getJlroomtype().trim();
            Roomtype local = localRoomMap.get(jlRoomId);
            if (local == null) {
                continue;
            }
            //日期有重复或同一套餐，多个价格
            String roomAndProd = jlRoomId + "@" + jl.getRatetypeid().trim();
            String roomAndProdAndDate = roomAndProd + "@" + jl.getStayDate().trim();
            String repeatDate = checkRepeatMap.get(roomAndProdAndDate);
            if (!ElongHotelInterfaceUtil.StringIsNull(repeatDate)) {
                repeatMap.put(roomAndProd, true);
                continue;
            }
            else {
                checkRepeatMap.put(roomAndProdAndDate, jl.getStayDate().trim());
            }
            HotelGoodData hd = new HotelGoodData();
            hd.setHotelid(hotel.getId());
            hd.setHotelname(hotel.getName());
            hd.setRoomtypeid(local.getId());
            hd.setRoomtypename(jl.getRoomtype().trim());
            //去哪儿房型 0：大床； 1：双床；2：大/双床；3：三床；5：单人床
            hd.setBedtypeid(jlBedToQunar(local.getBed()));
            try {
                hd.setBaseprice((long) Double.parseDouble(jl.getPprice()));
            }
            catch (Exception e) {
                hd.setBaseprice(0l);
            }
            if (hd.getBaseprice().longValue() <= 0) {
                continue;
            }
            hd.setJlroomtypeid(jlRoomId);
            hd.setJlf(jl.getFangliang());
            hd.setJlft(jl.getRoomstate());
            hd.setJlkeyid(jl.getJlkeyid().trim());
            hd.setJltime(jl.getJltime());
            hd.setAllotmenttype(jl.getAllotmenttype());
            hd.setRatetype(jl.getRatetype());
            hd.setRatetypeid(jl.getRatetypeid().trim());
            hd.setDatenum(jl.getStayDate());
            //最少连住
            try {
                hd.setMinday(Long.parseLong(jl.getMinDay()));
            }
            catch (Exception e) {
                hd.setMinday(1l);
            }
            //提前天数
            try {
                hd.setBeforeday(Long.parseLong(jl.getLeadTime()));
            }
            catch (Exception e) {
                hd.setBeforeday(0l);
            }
            //判断即时确认
            boolean isNowFlag = false;
            //房态 12:Open 13:良好 14:紧张 15:不可超 16:满房
            if (ElongHotelInterfaceUtil.StringIsNull(jl.getAllot()) || "null".equalsIgnoreCase(jl.getAllot().trim())) {
                jl.setAllot("0");//用于判断房量
            }
            String jlRoomStatus = jl.getAllot().trim();
            //Open
            if ("12".equals(jlRoomStatus)) {
                isNowFlag = true;
            }
            if ("16".equals(jlRoomStatus)) {
                hd.setYuliunum(0l);
            }
            else {
                try {
                    hd.setYuliunum(Long.parseLong(jl.getFangliang()));
                }
                catch (Exception e) {
                    hd.setYuliunum(0l);
                }
            }
            if (hd.getYuliunum().longValue() > 0) {
                isNowFlag = true;
            }
            //0：开房  ；1：关房
            hd.setRoomstatus(isNowFlag ? 0l : 1l);
            //捷旅房态
            hd.setJlft(jlRoomStatus);
            hd.setContractid(hotel.getHotelcode2());
            //深捷旅早餐转去哪儿早餐
            hd.setBfcount(jlBfToQunar(jl.getBreakfast()));
            //深捷旅宽带转去哪儿宽带
            hd.setWeb(jlWebToQunar(jl.getNetfee()));
            hd.setSorucetype("6");
            hd.setCityid(String.valueOf(hotel.getCityid()));
            //多套餐，用ID区分
            List<HotelGoodData> tempList = map.get(roomAndProd);
            if (tempList == null || tempList.size() == 0) {
                tempList = new ArrayList<HotelGoodData>();
            }
            tempList.add(hd);
            map.put(roomAndProd, tempList);
        }
        if (repeatMap.size() > 0 && map.size() > 0) {
            for (String key : repeatMap.keySet()) {
                if (map.containsKey(key)) {
                    map.remove(key);
                }
            }
        }
        return map;
    }

    //加载去哪儿数据
    public static Map<String, List<HotelData>> loadQN(Hotel hotel, String startDate, String endDate) throws Exception {
        //URL
        String url = qunarPriceUrl + "?payType=2&reqType=ComparePirce&QunarHotelId=" + hotel.getQunarId()
                + "&startDate=" + startDate + "&endDate=" + endDate;
        //POST
        String str = SendPostandGet.submitPost(url, "", "utf-8").toString();
        //ANALY
        Map<String, List<HotelData>> roomDatas = new HashMap<String, List<HotelData>>();
        if (!ElongHotelInterfaceUtil.StringIsNull(str)) {
            JSONArray json = JSONArray.fromObject(str);
            for (int i = 0; i < json.size(); i++) {
                JSONObject obj = json.getJSONObject(i);
                //0:现付 1:预付、易订行
                if (obj.getInt("paytype") != 1 || "易订行".equals(obj.getString("agentname"))) {
                    continue;
                }
                //new
                HotelData hoteldata = new HotelData();
                hoteldata.setSealprice((int) obj.getDouble("rPrice"));
                hoteldata.setAgentNo(obj.getString("agentid"));
                hoteldata.setAgentNmae(obj.getString("agentname"));
                hoteldata.setRoomnameBF(obj.getString("bfstr"));
                hoteldata.setRoomstatus(obj.getInt("roomstatus"));
                hoteldata.setType(obj.getInt("paytype"));
                hoteldata.setAgentRoom(obj.getString("agentRoom"));
                String roomname = obj.getString("roomname").trim();
                hoteldata.setRoomname(roomname);
                List<HotelData> hoteldatas = new ArrayList<HotelData>();
                if (roomDatas.containsKey(roomname)) {
                    hoteldatas = roomDatas.get(roomname);
                }
                hoteldatas.add(hoteldata);
                roomDatas.put(roomname, hoteldatas);
            }
        }
        return roomDatas;
    }

    /**
     * 判断是否更新
     * @param source 酒店来源  6：深捷旅；12：中航信比价；-12：中航信更新
     */
    public static boolean NeedUpdate(HotelGoodData old, HotelGoodData data, int source) {
        //中航信
        if (source == 12 || source == -12) {
            //去哪儿卖价变化
            if (source == 12 && old.getShijiprice().longValue() != data.getShijiprice().longValue()) {
                return true;
            }
            //供应价格变化
            if (source == -12 && old.getBaseprice().doubleValue() != data.getBaseprice().doubleValue()) {
                return true;
            }
            //房态变化
            if (old.getRoomstatus().longValue() != data.getRoomstatus().longValue()) {
                return true;
            }
            //早餐变化
            if (old.getBfcount().longValue() != data.getBfcount().longValue()) {
                return true;
            }
            //宽带变化
            if (old.getWeb().intValue() != data.getWeb().intValue()) {
                return true;
            }
            //提前天数
            if (old.getBeforeday().intValue() != data.getBeforeday().intValue()) {
                return true;
            }
            //连住天数
            if (old.getMinday().intValue() != data.getMinday().intValue()) {
                return true;
            }
        }
        else if (source == 6) {
            //深捷旅最后更新时间不同或价格发生变化
            if (!old.getJltime().equals(data.getJltime())
                    || old.getShijiprice().longValue() != data.getShijiprice().longValue()) {
                return true;
            }
        }
        return false;
    }

    //深捷旅床型转去哪儿床型 0：大床； 1：双床；2：大/双床；5：单人床
    private static long jlBedToQunar(Integer bedid) {
        long bed = 2;//大/双床
        if (bedid == null || bedid == 0) {
            return bed;
        }
        else if (bedid == 1) {
            bed = 5;//单人床
        }
        else if (bedid == 2) {
            bed = 0;//大床
        }
        else if (bedid == 3) {
            bed = 1;//双床
        }
        return bed;
    }

    //深捷旅早餐转去哪儿早餐
    private static long jlBfToQunar(String bf) {
        String ret = "0";//不含早
        if (ElongHotelInterfaceUtil.StringIsNull(bf) || "null".equalsIgnoreCase(bf.trim())) {
            ret = "0";//不含早
        }
        else if (bf.equals("11") || bf.equals("12") || bf.equals("13") || bf.equals("1")) {
            ret = "1";//单早
        }
        else if (bf.equals("21") || bf.equals("22") || bf.equals("23") || bf.equals("2")) {
            ret = "2";//双早
        }
        else if (bf.equals("31") || bf.equals("32") || bf.equals("33") || bf.equals("3")) {
            ret = "3";//三早
        }
        else if (bf.equals("34") || bf.equals("6")) {
            ret = "-1";//含早
        }
        return Long.parseLong(ret);
    }

    //深捷旅宽带转去哪儿宽带
    private static int jlWebToQunar(String net) {
        int web = 0;
        if (ElongHotelInterfaceUtil.StringIsNull(net) || "null".equalsIgnoreCase(net.trim())) {
            web = 0;//无
        }
        else if (net.trim().equals("0")) {
            web = 2;//免费
        }
        else if (Integer.parseInt(net.trim()) > 0) {
            web = 3;//收费
        }
        return web;
    }
}
