package com.ccservice.train.station;

import java.util.HashMap;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.inter.job.HttpsUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 12306车站更新
 * @author wzc
 * @version 创建时间：2015年7月8日 下午2:04:21
 */
public class StationUpdateJob implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        UpdateStationName();
    }

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        UpdateStationName();
    }

    /**
     * 更新job
     * url  https://kyfw.12306.cn/otn/resources/js/framework/station_name.js
     */
    public static void UpdateStationName() {
        String url = "https://kyfw.12306.cn/otn/resources/js/framework/station_name.js";
        String strresult = HttpsUtil.Get12306(url, new HashMap<String, String>());
        WriteLog.write("12306车站更新", "12306获取数据：" + strresult);
        if (strresult != null && strresult.length() > 0 && strresult.contains("'")) {
            String stationname = strresult.substring(strresult.indexOf("'") + 1, strresult.lastIndexOf("'"));
            String[] station = stationname.split("@");//站数组
            for (int i = 0; i < station.length; i++) {
                if ("".equals(station[i])) {
                    continue;
                }
                System.out.println(station[i]);
                //@bjb|北京北|VAP|beijingbei|bjb|0
                String[] stationonly = station[i].split("[|]");
                if (stationonly.length == 6) {
                    String jp = stationonly[0];//简拼
                    String name = stationonly[1];//站名
                    String code = stationonly[2];//代码
                    String qp = stationonly[3];//全拼
                    String jp1 = stationonly[4];//简拼1
                    String index = stationonly[5];//索引
                    updateDBStation(jp, name, code, qp, jp1, index);
                }
            }
        }
        else {
            WriteLog.write("12306车站更新", "获取数据异常：" + strresult);
        }
    }

    /**
     * 更新数据库数据
     */
    public static void updateDBStation(String jp, String name, String code, String qp, String jp1, String index) {
        try {
            String sql = "insert into StationName(Jp, SName, Qp, Scode, Jp1, Sindex) select '" + jp + "','" + name
                    + "','" + qp + "','" + code + "','" + jp1 + "','" + index
                    + "' from StationName with(nolock) where Scode='" + code + "'  having count(1)<=0;";
            Server.getInstance().getSystemService().excuteGiftBySql(sql);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
