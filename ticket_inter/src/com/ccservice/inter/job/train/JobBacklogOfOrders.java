package com.ccservice.inter.job.train;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainorderrc;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.ben.Trainform;
import com.ccservice.b2b2c.policy.QunarTrainMethod;
import com.ccservice.inter.job.train.JobQunarOrder;
import com.ccservice.inter.job.train.thread.MyThread;
import com.ccservice.inter.job.train.thread.MyThreadBacalog;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;

/**
 * 
 * 定时更新排队中的订单出票状态
 * @time 2014年12月18日 下午6:06:42
 * @author yinshubin
 */
public class JobBacklogOfOrders extends TrainSupplyMethod implements Job {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub
        exThread();
    }

    /**
     * 开启线程
     * @time 2014年12月8日 下午6:34:04
     * @author fiend
     */
    public static void exThread() {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        Thread t1 = null;
        Trainform trainform = new Trainform();
        //        trainform.setOrderstatus(Trainorder.ISQUEUING);
        List<Trainorder> torderList = Server.getInstance().getTrainService().findAllTrainorder(trainform, null);
        for (Trainorder trainorder : torderList) {
            t1 = new MyThreadBacalog(trainorder.getId());
            pool.execute(t1);
        }
        pool.shutdown();
    }

    /**
     *  获取订单的未完成订单 
     * @param trainorderid
     * @time 2014年12月8日 下午6:28:22
     * @author fiend
     */
    public void backlogOfOrders(long trainorderid) {
        if (backlogOfOrder(trainorderid).equals("出票完成")) {
            Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
            //            trainorder.setOrderstatus(Trainorder.ISSUEDWAITPAY);
            Server.getInstance().getTrainService().updateTrainorder(trainorder);
        }
    }

    /**
     *  查询订单的未完成订单 
     * @param trainorderid
     * @return
     * @time 2014年12月8日 下午6:27:48
     * @author fiend
     */
    public String backlogOfOrder(long trainorderid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        Customeruser customeruser = new Customeruser();
        String result = "false";
        String cookieString = "";
        String passengers = "";
        String infodata = "";
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            String pname = trainpassenger.getName();
            String pidnumber = trainpassenger.getIdnumber();
            int pidtype = trainpassenger.getIdtype();
            String sql = "select b.id from T_CUSTOMERUSER b"
                    + " join T_CUSTOMERPASSENGER a on a.C_CUSTOMERUSERID=b.ID join T_CUSTOMERCREDIT c on a.ID=c.C_REFID where a.C_USERNAME = \'"
                    + pname + "\' and c.C_CREDITNUMBER=\'" + pidnumber + "\'" + " and c.C_CREDITTYPEID= " + pidtype
                    + " AND a.C_TYPE=4";
            List<Customeruser> customeruserList = Server.getInstance().getMemberService()
                    .findAllCustomeruserBySql(sql, -1, 0);
            if (customeruserList.size() == 0) {
                result = "请确认乘客有归属12306账号";
                WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，失败：" + result);
                break;
            }
            customeruser = customeruserList.get(0);
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，调取TrainInit："
                        + trainticket.getId());
                if (null != customeruser) {
                    customeruser = Server.getInstance().getMemberService().findCustomeruser(customeruser.getId());
                    WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，调取TrainInit："
                            + customeruser.getId());
                    if (1 == customeruser.getState()) {
                        if ("".equals(cookieString) || cookieString.equals(customeruser.getCardnunber())) {
                            cookieString = customeruser.getCardnunber();
                            String idtype = "";
                            if (1 == pidtype) {
                                idtype = "1";
                            }
                            if (3 == pidtype) {
                                idtype = "B";
                            }
                            if (4 == pidtype) {
                                idtype = "C";
                            }
                            if (5 == pidtype) {
                                idtype = "G";
                            }
                            String tickettype = trainticket.getTickettype() + "";
                            if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                                tickettype = "1";
                            }
                            else {
                                if ("0".equals(tickettype)) {
                                    tickettype = "2";
                                }
                            }
                            String passenger = pname + "|" + idtype + "|" + pidnumber + "|" + tickettype + "|"
                                    + trainticket.getSeattype() + "|"
                                    + Float.valueOf(trainticket.getPayprice()).toString().replace(".", "");
                            passengers += passenger + ",";
                        }
                        else {
                            result = "乘客不在同一个12306账号下面";
                            break;
                        }
                    }
                    else {
                        result = "12306账号未登录";
                        break;
                    }
                }
                else {
                    result = "该乘客无绑定12306账号";
                    break;
                }
            }
            if (!"false".equals(result)) {
                break;
            }
        }
        if ("false".equals(result)) {
            WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，调取TrainInit：测试");
            passengers.substring(0, passengers.length() - 1);
            try {
                passengers = URLEncoder.encode(passengers, "UTF-8");
                cookieString = URLEncoder.encode(cookieString, "UTF-8");
            }
            catch (UnsupportedEncodingException e) {
                WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":ajaxOrderPicture：encode：" + e);
            }
            //            String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
            String url = customeruser.getMemberemail();
            String par = "datatypeflag=18&cookie=" + cookieString + "&passengers=" + passengers + "&trainorderid="
                    + trainorderid;
            try {
                WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，调取TrainInit：" + url
                        + "(:)" + par);
                infodata = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":获取未完成订单（问题订单使用） ，调取TrainInit：返回"
                        + infodata);
                if (!"false".equals(infodata)) {
                    if (infodata.contains("orderDBList")) {
                        WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":提交生成订单：保存订单信息：");
                        result = saveOrderInformation(infodata, trainorderid, customeruser.getLoginname());
                        WriteLog.write("Train12306_nopayorder", "订单号：" + trainorderid + ":提交生成订单：保存订单信息：返回结果");
                    }
                    else if (infodata.contains("orderCacheDTO")) {
                        result = "已买过票了";
                    }
                    else {
                        result = infodata;
                        if (result.contains("该账号取消次数过多")) {
                        }
                    }
                }
            }
            catch (Exception e) {
                WriteLog.write("Train12306_order_exception", "订单号：" + trainorderid + ":nopayorder：获取未完成订单（问题订单使用） ："
                        + e);
            }
        }
        if ("出票成功，数据库同步失败".equals(result)) {
            result = "12306待支付订单不是当前订单";
        }
        if (result.contains("出票成功，请尽快支付")) {
            long quer_id = Long.valueOf(getSysconfigString("qunar_agentid"));
            if (quer_id == trainorder.getAgentid()) {
                if (JobGenerateOrder.ishaveseat(trainorder)
                        && (trainorder.getIsjointtrip() == null || trainorder.getIsjointtrip() == 0)) {
                    trainIssue(trainorder, customeruser.getLoginname());
                    result = "出票完成";
                }
            }
            else {
                if (JobGenerateOrder.ishaveseat(trainorder)) {
                    //                                TODO 易订行订单出票
                    yeeIssue(trainorder.getId(), customeruser.getId());
                    result = "出票完成";
                }
            }
        }
        return result;
    }

    /**
     * 说明：将自动下单成功的订单信息存入数据库
     * @param str
     * @param trainorderid
     * @param loginname
     * @return
     * @time 2014年8月30日 下午3:08:48
     * @author yinshubin
     */
    public static String saveOrderInformation(String str, long trainorderid, String loginname) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        try {
            JSONObject jsono = JSONObject.fromObject(str);
            if (jsono.has("data")) {
                JSONObject jsonodata = jsono.getJSONObject("data");
                if (jsonodata.has("orderDBList")) {
                    JSONArray jsonaorderDBList = jsonodata.getJSONArray("orderDBList");
                    for (int j = 0; j < jsonaorderDBList.size(); j++) {//获取所有订单
                        JSONObject jsonoorderDBList = jsonaorderDBList.getJSONObject(j);
                        if (jsonoorderDBList.toString().contains("待支付")) {//拿到那个唯一的待支付订单
                            String start_time = jsonoorderDBList.getString("start_train_date_page");
                            String train_code = jsonoorderDBList.getString("train_code_page");
                            String trainno = trainorder.getPassengers().get(0).getTraintickets().get(0).getTrainno();
                            WriteLog.write("Train12306_order", "订单号：" + trainorderid + ":下单成功，获取待支付订单信息:"
                                    + jsonoorderDBList.toString());
                            String sequence_no = jsonoorderDBList.getString("sequence_no");
                            if (start_time.equals(trainorder.getPassengers().get(0).getTraintickets().get(0)
                                    .getDeparttime())
                                    && comparisonTraincode(train_code, trainno)) {
                                if (jsonoorderDBList.has("tickets")) {
                                    JSONArray tickets = jsonoorderDBList.getJSONArray("tickets");
                                    List<Trainpassenger> trainpassengers = trainorder.getPassengers();
                                    for (Trainpassenger trainpassenger : trainpassengers) {
                                        for (int i = 0; i < tickets.size(); i++) {
                                            JSONObject jsonoticket = tickets.getJSONObject(i);
                                            if (jsonoticket.has("passengerDTO")) {
                                                JSONObject jsonopassengerDTO = jsonoticket
                                                        .getJSONObject("passengerDTO");
                                                String passenger_name = jsonopassengerDTO.getString("passenger_name");
                                                String id_cype_code = jsonopassengerDTO
                                                        .getString("passenger_id_type_code");
                                                int ticket_type_code = jsonoticket.getInt("ticket_type_code");
                                                int passenger_id_type_code = 1;
                                                if ("C".equals(id_cype_code)) {
                                                    passenger_id_type_code = 4;
                                                }
                                                if ("B".equals(id_cype_code)) {
                                                    passenger_id_type_code = 3;
                                                }
                                                if ("G".equals(id_cype_code)) {
                                                    passenger_id_type_code = 5;
                                                }
                                                String passenger_id_no = jsonopassengerDTO.getString("passenger_id_no");
                                                if (!"".equals(trainpassenger.getName())
                                                        && !"".equals(trainpassenger.getIdnumber())
                                                        && trainpassenger.getName().equals(passenger_name)
                                                        && trainpassenger.getIdnumber().toUpperCase()
                                                                .equals(passenger_id_no)
                                                        && trainpassenger.getIdtype() == passenger_id_type_code) {
                                                    for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                                                        int ickettype = trainticket.getTickettype();
                                                        if (null != trainorder.getQunarOrdernumber()
                                                                && !"".equals(trainorder.getQunarOrdernumber())
                                                                && 0 == ickettype) {
                                                            ickettype = 2;
                                                        }
                                                        else {
                                                            ickettype = 1;
                                                        }
                                                        //                                                Trainticket trainticketsave=Server.getInstance().getTrainService().findTrainticket(trainticket.getId());
                                                        if (ickettype == ticket_type_code) {
                                                            trainticket.setCoach(jsonoticket.getString("coach_no"));
                                                            trainticket.setSeatno(jsonoticket.getString("seat_name"));
                                                            trainticket.setPrice(Float.valueOf(jsonoticket
                                                                    .getString("str_ticket_price_page")));
                                                            Server.getInstance().getTrainService()
                                                                    .updateTrainticket(trainticket);
                                                            WriteLog.write(
                                                                    "Train12306_order",
                                                                    "订单号："
                                                                            + trainorderid
                                                                            + ":下单成功，更改票信息:"
                                                                            + trainticket.getId()
                                                                            + "(:)"
                                                                            + jsonoticket.getString("coach_no")
                                                                            + "(:)"
                                                                            + jsonoticket.getString("seat_name")
                                                                            + "(:)"
                                                                            + Float.valueOf(jsonoticket
                                                                                    .getString("str_ticket_price_page"))
                                                                            + "(:)"
                                                                            + jsonoticket.getInt("ticket_type_code"));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                trainorder.setExtnumber(sequence_no.trim());
                                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                                Trainorderrc rc = new Trainorderrc();
                                rc.setOrderid(trainorder.getId());
                                rc.setContent("下单成功,电子单号: " + sequence_no);
                                rc.setYwtype(1);
                                rc.setCreateuser(loginname);
                                Server.getInstance().getTrainService().createTrainorderrc(rc);
                                WriteLog.write("Train12306_order", "订单号：" + trainorderid + ":下单成功，更改订单信息信息:"
                                        + trainorder.getId() + "(:)" + sequence_no);
                                ajaxorderpayment1(trainorderid);
                                return "出票成功，请尽快支付,电子单号:" + sequence_no;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            WriteLog.write("Train12306_order_exception", "订单号：" + trainorderid + ":下单成功，数据库同步失败:" + e);
        }
        WriteLog.write("Train12306_orde r", "订单号：" + trainorderid + ":下单成功，数据库同步失败");
        return "出票成功，数据库同步失败";
    }

    /**
     * 说明：保存订单信息比对车次 
     * @param traincode
     * @param trainno
     * @return
     * @time 2014年10月6日 上午8:26:05
     * @author yinshubin
     */
    public static boolean comparisonTraincode(String traincode, String trainno) {
        boolean result = false;
        if (trainno.contains("/")) {
            String[] trainnos = trainno.split("/");
            for (int i = 0; i < trainnos.length; i++) {
                result = traincode.equals(trainnos[i]);
                if (result) {
                    break;
                }
            }
        }
        else {
            result = traincode.equals(trainno);
        }
        return result;
    }

    /**
     * 说明：获取支付订单链接
     * @time 2014年8月30日 下午3:08:37
     * @author yinshubin
     */
    public static void ajaxorderpayment1(long trainorderid) {
        String result = "false";
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        if (null != trainorder.getChangesupplytradeno() && !"".equals(trainorder.getChangesupplytradeno())) {
            result = trainorder.getChangesupplytradeno();
        }
        else {
            String extnumber = trainorder.getExtnumber();
            String cookieString = "";
            Customeruser customeruser = null;
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                String pname = trainpassenger.getName();
                String pidnumber = trainpassenger.getIdnumber();
                int pidtype = trainpassenger.getIdtype();
                String sql = "select b.id from T_CUSTOMERUSER b"
                        + " join T_CUSTOMERPASSENGER a on a.C_CUSTOMERUSERID=b.ID join T_CUSTOMERCREDIT c on a.ID=c.C_REFID where a.C_USERNAME = \'"
                        + pname + "\' and c.C_CREDITNUMBER=\'" + pidnumber + "\'" + " and c.C_CREDITTYPEID= " + pidtype
                        + " AND a.C_TYPE=4";
                List<Customeruser> customeruserList = Server.getInstance().getMemberService()
                        .findAllCustomeruserBySql(sql, -1, 0);
                if (customeruserList.size() > 0) {
                    customeruser = customeruserList.get(0);
                    customeruser = Server.getInstance().getMemberService().findCustomeruser(customeruser.getId());
                    if (1 == customeruser.getState()) {
                        if ("".equals(cookieString) || cookieString.equals(customeruser.getCardnunber())) {
                            cookieString = customeruser.getCardnunber();
                        }
                    }
                    else {
                        result = "12306账号未登录";
                        break;
                    }
                }
            }
            if ("false".equals(result)) {
                try {
                    cookieString = URLEncoder.encode(cookieString, "UTF-8");
                    //                    String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
                    String url = customeruser.getMemberemail();
                    String par = "datatypeflag=9&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                            + trainorderid;
                    result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
                    if (result.indexOf("gateway.do") >= 0 && null != customeruser) {
                        float supplyprice = Float.valueOf(result.split("ord_amt=")[1].split("&ord_cur")[0]);
                        trainorder.setSupplyprice(supplyprice);
                        trainorder.setSupplypayway(7);
                        if (null == trainorder.getQunarOrdernumber() || "".equals(trainorder.getQunarOrdernumber())) {
                            trainorder.setSupplyaccount(customeruser.getLoginname() + "/hy");
                        }
                        else {
                            trainorder.setSupplyaccount(customeruser.getLoginname() + "/shu");
                        }
                        trainorder.setSupplytradeno(result.split("ord_id_ext=")[1].split("&ord_name")[0]);
                        trainorder.setChangesupplytradeno(result);
                        Server.getInstance().getTrainService().updateTrainorder(trainorder);
                        orderpaymentunionpayurl(url, cookieString, extnumber, trainorderid);
                    }
                }
                catch (Exception e) {
                    WriteLog.write("Train12306_order_exception", "订单号：" + trainorderid + ":ajaxorderpayment:" + e);
                }
            }
        }
    }

    /**
     * 获取银联支付链接 
     * @param cookieString
     * @param extnumber
     * @param trainorderid
     * @time 2014年12月6日 下午3:52:46
     * @author fiend
     */
    //    public static void orderpaymentunionpayurl(String cookieString, String extnumber, long trainorderid) {
    //        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
    //        String result = "";
    //        try {
    //            cookieString = URLEncoder.encode(cookieString, "UTF-8");
    //            String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
    //            String par = "datatypeflag=29&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
    //                    + trainorder.getId();
    //            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
    //            if (result.indexOf("Pay.action") >= 0) {
    //                Trainorder train = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
    //                train.setId(trainorderid);
    //                train.setAutounionpayurl(result);
    //                Server.getInstance().getTrainService().updateTrainorder(train);
    //            }
    //        }
    //        catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //    }
    /**
     * 获取银联支付链接 
     * @param cookieString
     * @param extnumber
     * @param trainorderid
     * @time 2014年12月6日 下午3:52:46
     * @author fiend
     */
    public static void orderpaymentunionpayurl(String url, String cookieString, String extnumber, long trainorderid) {
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(trainorderid);
        String result = "";
        try {
            cookieString = URLEncoder.encode(cookieString, "UTF-8");
            //            String url = JobQunarOrder.getSystemConfig("Reptile_traininit_url");
            String par = "datatypeflag=29&cookie=" + cookieString + "&extnumber=" + extnumber + "&trainorderid="
                    + trainorder.getId();
            result = SendPostandGet.submitPost(url, par, "UTF-8").toString();
            if (result.indexOf("Pay.action") >= 0) {
                Trainorder train = Server.getInstance().getTrainService().findTrainorder(trainorder.getId());
                train.setId(trainorderid);
                train.setAutounionpayurl(result);
                Server.getInstance().getTrainService().updateTrainorder(train);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 说明：拒单公用方法
     * @param orderid
     * @param state
     * @param loginname
     * @param customeruserId
     * @time 2014年9月1日 下午2:45:18
     * @author yinshubin
     */
    public void refuse(long orderid, int state, String loginname, long customeruserId) {
        WriteLog.write("JobGenerateOrder", "订单ID：" + orderid + ";12306账号：" + loginname + "；refuse：准备拒单：拒单原因："
                + MyThread.refusereason(state));
        long quer_id = Long.valueOf(getSysconfigString("qunar_agentid"));
        Trainorder trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        trainorder.setState12306(Trainorder.ORDERFALSE);
        if (trainorder.getAgentid() != quer_id) {
            trainorder.setOrderstatus(Trainorder.REFUSENOREFUND);
        }
        trainorder.setRefuseaffirm(Trainorder.Refuseaffirm.NOAFFIRM);
        trainorder.setRefundreason(state);
        Server.getInstance().getTrainService().updateTrainorder(trainorder);
        if (trainorder.getAgentid() != quer_id) {
            Trainorderrc rcrefund = new Trainorderrc();
            rcrefund.setStatus(Trainticket.NONISSUEDABLE);
            rcrefund.setContent("拒单-无法出票:" + MyThread.refusereason(state));
            rcrefund.setCreateuser(loginname);
            rcrefund.setOrderid(trainorder.getId());
            rcrefund.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rcrefund);
            refuserefund(trainorder.getId(), customeruserId);
            //TODO 拒单后退款
        }
        else {
            for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
                JobQunarOrder.changecustomeruser(trainpassenger.getName(), trainpassenger.getIdnumber(), "1");
            }
            Trainorderrc rc = new Trainorderrc();
            WriteLog.write("JobGenerateOrder", "订单ID：" + trainorder.getId() + ";12306账号：" + loginname
                    + "；refuse：准备调用拒单拒单接口");
            if (isrefuse(trainorder.getId(), 0)) {
                WriteLog.write("JobGenerateOrder", "订单ID：" + trainorder.getId() + ";12306账号：" + loginname
                        + "；refuse：拒单成功");
                trainorder.setOrderstatus(Trainorder.REFUSED);
                Server.getInstance().getTrainService().refusequnarTrain(trainorder, Trainticket.NONISSUEDABLE);
                rc.setStatus(Trainticket.NONISSUEDABLE);
                rc.setContent("拒单-无法出票:" + MyThread.refusereason(state));
                rc.setCreateuser(loginname);
            }
            else {
                WriteLog.write("JobGenerateOrder", "订单ID：" + trainorder.getId() + ";12306账号：" + loginname
                        + "；refuse：拒单失败");
                trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
                rc.setStatus(Trainticket.WAITISSUE);
                rc.setContent("拒单-失败");
            }
            rc.setCreateuser(loginname);
            rc.setOrderid(trainorder.getId());
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
    }

    /**
     * 易订行退票退款  
     * @param trainorderid
     * @param customeruserid
     * @time 2014年10月24日 上午10:38:04
     * @author yinshubin
     */
    public void refuserefund(long trainorderid, long customeruserid) {
        Customeruser customeruser = Server.getInstance().getMemberService().findCustomeruser(customeruserid);
        Server.getInstance().getTrainService()
                .refuseTrain(trainorderid, customeruser, getSysconfigString("cninterfaceurl"));
    }

    /**
     * 说明：调取出票、拒单接口 
     * @param orderid
     * @param state
     * @return
     * @time 2014年9月1日 下午2:45:39
     * @author yinshubin
     */
    public static boolean isrefuse(long orderid, int state) {
        boolean result = false;
        result = QunarTrainMethod.trainIssueOrRefuse(orderid, state);
        return result;
    }

    /**
     * 说明：出票 
     * @time 2014年9月1日 下午5:45:25
     * @author yinshubin
     */
    public void trainIssue(Trainorder trainorder, String loginname) {
        float differenceprice = getdirrerenceprice(trainorder);
        if (differenceprice < 0) {
            refuse(trainorder.getId(), 3, loginname, 0);
        }
        else {
            WriteLog.write("JobGenerateOrder", "订单ID：" + trainorder.getId() + ";12306账号：" + loginname
                    + "；trainIssue：调取出票接口");
            boolean msg = isrefuse(trainorder.getId(), 3);
            WriteLog.write("JobGenerateOrder", "订单ID：" + trainorder.getId() + ";12306账号：" + loginname
                    + "；trainIssue：调取出票接口返回结果：" + msg);
            boolean isresult = false;
            trainorder.setState12306(Trainorder.ORDEREDWAITPAY);
            if (msg) {
                //                trainorder.setOrderstatus(Trainorder.ISSUED);
                //trainorder.setOperateuid(0L);//出票完成自动解锁
                //trainorder.setControlname("");
                trainorder.setIsquestionorder(Trainorder.NOQUESTION);
                Server.getInstance().getTrainService().refusequnarTrain(trainorder, Trainticket.ISSUED);
                isresult = true;
            }
            Trainorderrc rc = new Trainorderrc();
            rc.setOrderid(trainorder.getId());
            if (isresult) {
                rc.setContent("出票完成。退还差价:" + differenceprice + "元");
            }
            else {
                rc.setContent("出票失败");
                trainorder.setIsquestionorder(Trainorder.CAIGOUQUESTION);
                Server.getInstance().getTrainService().updateTrainorder(trainorder);
            }
            rc.setStatus(Trainticket.ISSUED);
            rc.setCreateuser(loginname);
            rc.setYwtype(1);
            Server.getInstance().getTrainService().createTrainorderrc(rc);
        }
    }

    /**
     * 说明：得到订单差价 
     * @param trainorder
     * @return
     * @time 2014年9月1日 下午3:29:14
     * @author yinshubin
     */
    public float getdirrerenceprice(Trainorder trainorder) {
        float differenceprice = 0f;
        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            for (Trainticket trainticket : trainpassenger.getTraintickets()) {
                differenceprice += trainticket.getPayprice() - trainticket.getPrice();
            }
        }
        return differenceprice;
    }
}
