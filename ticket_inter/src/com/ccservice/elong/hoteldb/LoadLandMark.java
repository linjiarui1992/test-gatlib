package com.ccservice.elong.hoteldb;

import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.compareprice.HttpClient;
import com.ccservice.inter.server.Server;

public class LoadLandMark {
	public static void main(String[] args) {
		List<Landmark> landmarks = Server
				.getInstance()
				.getHotelService()
				.findAllLandmark("where c_cityid=101 ",
						"order by id asc", -1, 0);
		for (int i = 0; i < landmarks.size(); i++) {
			String[] ary=getLanLng(landmarks.get(i).getName());
			//北京航空航天大学体育馆 （奥运场馆）
			if(ary[0]!=null&&!ary[0].equals("")){
				landmarks.get(i).setLat(Double.valueOf(ary[0]));
			}
			if(ary[1]!=null&&!ary[1].equals("")){
				landmarks.get(i).setLng(Double.valueOf(ary[1]));
			}
			if(ary[0]!=null&&ary[1]!=null&&!ary[1].equals("")&&!ary[0].equals("")){
				Server.getInstance().getHotelService().updateLandmarkIgnoreNull(landmarks.get(i));
			}
		}
	}

	public static String[] getLanLng(String address){
		String[] ary={null,null};
		try {
			String url = "http://api.map.baidu.com/geocoder?address=";
			String str = HttpClient.httpget(url + address, "utf-8");
			if(str!=null&&!"null".equals(str)){
				SAXBuilder reader = new SAXBuilder();
				Document doc;
				doc = reader.build(new StringReader(str.trim()));
				Element root = doc.getRootElement();
				Element result = root.getChild("result");
				Element location = result.getChild("location");
				String lat = location.getChildText("lat");// 纬度
				String lng = location.getChildText("lng");// 经度
				String level=location.getChildText("level");//类型
				ary[0]=lat;
				ary[1]=lng;
				System.out.println("地标：" + address + ",纬度：" + lat + ",经度:" + lng+",类型："+level);
			}
		} catch (Exception e) {
		}
		return ary;
	}
}
