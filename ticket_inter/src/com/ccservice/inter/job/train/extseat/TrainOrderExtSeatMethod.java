package com.ccservice.inter.job.train.extseat;

public class TrainOrderExtSeatMethod {
    private String seat;

    private float price;

    private boolean created;

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

}
