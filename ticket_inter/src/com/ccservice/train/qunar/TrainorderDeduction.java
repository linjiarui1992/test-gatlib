package com.ccservice.train.qunar;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ccservice.b2b2c.base.rebaterecord.Rebaterecord;
import com.ccservice.b2b2c.base.sysconfig.Sysconfig;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.ben.Payresult;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.TrainSupplyMethod;
import com.ccservice.inter.server.Server;
import com.ccservice.inter.train.TrainWithholding;
import com.ccservice.inter.train.TrainWithholeResult;
import com.ccservice.qunar.util.ExceptionUtil;

public class TrainorderDeduction extends TrainSupplyMethod {
    private Trainorder trainorder;

    private long orderid = 73418572;

    //网页端改支付宝获取新连接  开始
    public static Map<String, TrainDeductionEntry> serviceMap = new HashMap<String, TrainDeductionEntry>();

    private boolean iscandeduction;

    //2后扣款模式 1预付模式
    private int type = 1;

    public TrainorderDeduction(long orderid, int type) {
        this.orderid = orderid;
        this.type = type;
    }

    public void tongchengDeduction() {
        this.trainorder = Server.getInstance().getTrainService().findTrainorder(orderid);
        if (trainorder.getInterfacetype() != null && trainorder.getInterfacetype() == 4) {//出票前代扣
            upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 1 + " WHERE ID=" + this.orderid);//代扣成功
        }
        else {
            if (1 == type) {
                xuniDeduction();
            }
            if (2 == type) {
                afterDeduction();//后扣款模式支付扣除支付宝里的钱
            }
        }
    }

    /**
     * 后扣款模式支付
     * @return
     * @time 2015年1月27日 上午10:55:20
     * @author fiend
     */
    public void afterDeduction() {
        iscanDeduction();
        if (this.iscandeduction) {
            WriteLog.write("TrainorderDeduction", orderid + "");
            WriteLog.write("TrainorderDeduction", orderid + ":符合后扣款模式------>" + orderid);
            Map map = getagentbytrainorderid(orderid);
            String agentid = getagentidbymap(map, "C_AGENTID");
            String ordernumber = getagentidbymap(map, "C_ORDERNUMBER");
            String username = getcustomeruserKEYandloginname(agentid, 1);
            String key = getcustomeruserKEYandloginname(agentid, 2);
            if (!"-1".equals(agentid) && !"-1".equals(ordernumber) && !"-1".equals(username) && !"-1".equals(key)) {
                TrainWithholeResult trainWithholeResult = new TrainWithholding().withholding(username, key,
                        ordernumber);
                WriteLog.write("TrainorderDeduction", orderid + ":" + trainWithholeResult.toString());
                if ("S".equalsIgnoreCase(trainWithholeResult.getStatuscode())) {
                    String trainnumber = trainWithholeResult.getRemark();
                    //扣款成功
                    WriteLog.write("TrainorderDeduction", orderid + ":代扣成功------>" + orderid);
                    Server.getInstance().getSystemService()
                            .findMapResultBySql("UPDATE T_TRAINORDER SET C_PAYMETHOD=1,C_TRADENO='" + trainnumber
                                    + "',C_ISPLACEING=" + 1 + " WHERE ID =" + orderid, null);
                    writeRC(orderid, "代扣成功", "代扣接口", 3, 1);
                }
                else if ("F".equalsIgnoreCase(trainWithholeResult.getStatuscode())) {
                    //扣款失败
                    WriteLog.write("TrainorderDeduction", orderid + ":代扣失败------>" + orderid);
                    Server.getInstance().getSystemService().findMapResultBySql(
                            "UPDATE T_TRAINORDER SET C_ISPLACEING=" + 4 + " WHERE ID =" + orderid, null);
                    writeRC(orderid, "代扣失败1", "代扣接口", 3, 1);
                }
                else {
                    //异常 需要客服后台操作 检查代扣是否成功  失败重新扣款
                    //扣款异常
                    WriteLog.write("TrainorderDeduction", orderid + ":代扣异常------>" + orderid);
                    Server.getInstance().getSystemService().findMapResultBySql(
                            "UPDATE T_TRAINORDER SET C_ISPLACEING=" + 6 + " WHERE ID =" + orderid, null);
                    writeRC(orderid, "代扣失败2", "代扣接口", 3, 1);
                }
            }
            else {
                WriteLog.write("TrainorderDeduction", orderid + ":获取信息失败:" + agentid + ":" + username + ":" + key);
            }
        }
        else {
            writeRC(this.trainorder.getId(), "拒绝扣款-已扣款或扣款异常", "系统自动扣款", 3, 1);
        }
    }

    private String getagentidbymap(Map map, String key) {
        String value = "-1";
        if (map.get(key) != null) {
            value = map.get(key).toString();
        }
        return value;
    }

    private Map getagentbytrainorderid(long orderid2) {
        Long agentid = 0L;
        String sql = "select C_AGENTID,C_ORDERNUMBER from T_trainorder with(nolock) where id=" + orderid2;
        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
        Map map = new HashMap();
        if (list.size() > 0) {
            map = (Map) list.get(0);
        }
        return map;
    }

    /**
     * 
     * @param agentid
     * @param type 1 获取username 2 获取key
     * @return
     * @time 2015年1月14日 下午7:01:07
     * @author chendong
     */
    private String getcustomeruserKEYandloginname(String agentid, int type) {
        String db_value = "-1";//dateHashMap
        String key_agent_username = "loginname_username_" + agentid;
        String key_agent_key = "loginname_workphone_" + agentid;
        String key = key_agent_username;
        if (type == 2) {
            key = key_agent_key;
        }
        if (Server.getInstance().getDateHashMap().get(key) == null) {
            String ziduan = "C_LOGINNAME";
            if (type == 2) {
                ziduan = "C_WORKPHONE";
            }
            try {
                String sql = "select " + ziduan + " from T_customeruser with(nolock) where C_AGENTID=" + agentid;
                List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                if (list.size() > 0) {
                    Map map = (Map) list.get(0);
                    if (map.get(ziduan) != null) {
                        db_value = map.get(ziduan).toString();
                        Server.getInstance().getDateHashMap().put(key, db_value);
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            db_value = Server.getInstance().getDateHashMap().get(key);
        }
        return db_value;
    }

    /**
     * 后扣款现扣模式是否可以扣款 
     * @time 2015年2月12日 下午10:30:04
     * @author fiend
     */
    @SuppressWarnings({ "rawtypes" })
    public void iscanDeductionAfter() {
        try {
            String sql = "select COUNT(id) countid from T_REBATERECORD with(nolock) where c_tradeno is not null and C_ORDERID="
                    + this.orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                int countid = Integer.valueOf(map.get("countid").toString());
                if (countid == 0) {
                    this.iscandeduction = true;
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.iscandeduction = false;
    }

    public static void main(String[] args) {
        new TrainorderDeduction(73418572, 1).tongchengDeduction();
    }

    //TODO============================================================虚拟扣款开始======================================================================
    /**
     * 同程虚拟支付
     * @return
     * @time 2015年1月27日 上午10:55:20
     * @author fiend
     */
    public void xuniDeduction() {
        long t1 = System.currentTimeMillis();
        //订单金额非法时，无需进主扣款流程
        if (this.trainorder.getOrderprice() <= 0) {
            writeRC(this.trainorder.getId(), "扣款失败【订单金额非法】", "系统自动扣款", 3, 1);
            upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 4 + " WHERE ID=" + this.orderid);
            return;
        }
        //手续费计算Redis，无需进主扣款流程
        double kouprice = 0;
        try {
            boolean result = isNeedPayUrl(false);
            if (result) {
                kouprice = getShouXuFei(this.trainorder.getAgentid(), trainorder);
            }
            else {
                kouprice = this.trainorder.getOrderprice() + getShouXuFei(this.trainorder.getAgentid(), trainorder);
            }
        }
        catch (Exception e1) {
            ExceptionUtil.writelogByException("TrainorderDeduction_xuniDeduction_Exception", e1,
                    "" + this.trainorder.getId());
            writeRC(this.trainorder.getId(), "扣款失败【判断是否是客人支付_异常】", "系统自动扣款", 3, 1);
            upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 4 + " WHERE ID=" + this.orderid);
            return;
        }

        //主扣款流程
        long t2 = System.currentTimeMillis();
        Payresult payresult = vmonyPay(this.trainorder.getId(), this.trainorder.getOrdernumber(), kouprice, 3,
                this.trainorder.getQunarOrdernumber(), this.trainorder.getAgentid());
        long t3 = System.currentTimeMillis();
        if (payresult.isPaysuccess()) {
            writeRC(this.trainorder.getId(), "扣款成功:" + this.trainorder.getOrderprice(), "系统自动扣款", 3, 1);
            upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 1 + ",c_paymethod=4 WHERE ID=" + this.orderid);
        }
        else {
            if (!payresult.getResultmessage().contains("已扣款")) {
                writeRC(this.trainorder.getId(), "扣款失败【" + payresult.getResultmessage() + "】", "系统自动扣款", 3, 1);
                upTrainorder("UPDATE T_TRAINORDER SET C_ISPLACEING=" + 4 + " WHERE ID=" + this.orderid);
            }
        }
        long t4 = System.currentTimeMillis();
        WriteLog.write("t虚拟耗时new", this.trainorder.getId() + ":整体耗时:[" + (t4 - t1) + "]单步:耗时=1[" + (t2 - t1) + "]2:["
                + (t3 - t2) + "]3：[" + (t4 - t3) + "]");
    }

    /**
     * 判断是否客人支付类型
     * @return
     */
    public boolean isNeedPayUrl(boolean isChange) {
        boolean result = false;
        String sql = "sp_TomasPayUrlInfo_selectByOrderId @OrderId=" + this.orderid + ", @ChangeFlag= "
                + (isChange ? 1 : 0);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
        if (list != null && list.size() > 0) {
            result = true;
        }
        return result;
    }

    /**
     * 获取订单手续费
     * @param agentid
     * @return
     */
    public double getShouXuFei(long agentid, Trainorder order) {
        double sumshouxufei = 0.0d;
        try {
            double shouxufei = 0.0d;//代购手续费
            double tuoGuanShouxufei = 0.0d;//托管帐号手续费
            int ticketcount = 0;
            String sql = "select ISNULL(C_SHOUXUFEI,0) C_SHOUXUFEI,ISNULL(C_SHOUXUFEI1,0) C_SHOUXUFEI1  from T_INTERFACEACCOUNT with(nolock) where C_AGENTID="
                    + agentid;
            String sqlcount = "select ISNULL(C_TICKETCOUNT,0) C_TICKETCOUNT from T_TRAINORDER with(nolock) where ID="
                    + order.getId();
            //redis中取值
            try {
                long currentTime = System.currentTimeMillis();
                String key = "TrainDeduction_" + agentid;
                if (serviceMap.containsKey(key)) {
                    TrainDeductionEntry entry = serviceMap.get(key);
                    String servicePayValue = "";
                    if (entry.getTimeBefore() == 0 || (currentTime - entry.getTimeBefore()) > 5 * 60 * 1000) {
                        //从数据库中取值
                        List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                        if (list.size() > 0) {
                            Map map = (Map) list.get(0);
                            shouxufei = Double.valueOf(map.get("C_SHOUXUFEI").toString());
                            tuoGuanShouxufei = Double.valueOf(map.get("C_SHOUXUFEI1").toString());
                            servicePayValue = shouxufei + "," + tuoGuanShouxufei;
                        }
                        entry.setTimeBefore(System.currentTimeMillis());
                        entry.setTempValue(servicePayValue);
                        serviceMap.put(key, entry);
                    }
                    else {
                        String[] fee = entry.getTempValue().split(",");
                        if (fee.length == 2) {
                            shouxufei = Double.valueOf(fee[0]).doubleValue();
                            tuoGuanShouxufei = Double.valueOf(fee[1]).doubleValue();
                            System.out.println("使用缓存:" + shouxufei + ":" + tuoGuanShouxufei);
                        }
                    }
                }
                else {
                    List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
                    String servicePayValue = "";
                    if (list.size() > 0) {
                        Map map = (Map) list.get(0);
                        shouxufei = Double.valueOf(map.get("C_SHOUXUFEI").toString());
                        tuoGuanShouxufei = Double.valueOf(map.get("C_SHOUXUFEI1").toString());
                        servicePayValue = shouxufei + "," + tuoGuanShouxufei;
                    }
                    TrainDeductionEntry entry = new TrainDeductionEntry();
                    entry.setTimeBefore(System.currentTimeMillis());
                    entry.setTempValue(servicePayValue);
                    serviceMap.put(key, entry);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (shouxufei > 0 || tuoGuanShouxufei > 0) {
                List sqlcountlist = Server.getInstance().getSystemService().findMapResultBySql(sqlcount, null);
                if (sqlcountlist.size() > 0) {
                    Map map = (Map) sqlcountlist.get(0);
                    ticketcount = Integer.valueOf(map.get("C_TICKETCOUNT").toString());
                }
                System.out.println("当前订单id:" + order.getId() + ",类型：" + order.getOrdertype());
                if (order.getOrdertype() == 3 || order.getOrdertype() == 4) {//托管帐号
                    sumshouxufei = tuoGuanShouxufei * ticketcount;
                }
                else {
                    sumshouxufei = shouxufei * ticketcount;
                }
                try {
                    String updatesql = "update T_TRAINORDER set C_COMMISSION=" + sumshouxufei + " WHERE ID="
                            + order.getId();
                    Server.getInstance().getSystemService().findMapResultBySql(updatesql, null);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
        }
        return sumshouxufei;
    }

    /**
     * 是否可以扣款 
     * @time 2015年2月12日 下午10:30:04
     * @author fiend
     */
    @SuppressWarnings({ "rawtypes" })
    public void iscanDeduction() {
        try {
            String sql = "select COUNT(id) countid from T_REBATERECORD with(nolock) where C_REBATETYPE=2 and C_YEWUTYPE=3 and C_ORDERID="
                    + this.orderid;
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            if (list.size() > 0) {
                Map map = (Map) list.get(0);
                int countid = Integer.valueOf(map.get("countid").toString());
                if (countid == 0) {
                    this.iscandeduction = true;
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        this.iscandeduction = false;
    }

    /**
     * 
     * 扣除虚拟账号里面的钱
     * 
     * @param orderid 订单ID
     * @param ordernumber 订单号
     * @param orderprice 订单支付金额
     * @param ywtype 业务类型 3：火车票
     * @param refordernum  同城关联订单号
     */
    public Payresult vmonyPay(long orderid, String ordernumber, double orderprice, int ywtype, String refordernum,
            long agentid) {
        WriteLog.write("tc虚拟账户支付new",
                "订单号ID：" + orderid + ":订单号：" + ordernumber + ":订单价格：" + orderprice + ":业务类型：" + ywtype);
        Payresult payresult = new Payresult();
        payresult.setPaysuccess(true);
        payresult.setResultmessage("支付成功");
        String msg = "订单";
        if (orderprice == 0) {
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败,支付金额不能等于0！");
            WriteLog.write("tc虚拟账户支付new",
                    "订单号ID：" + orderid + ",订单号：" + ordernumber + "," + payresult.getResultmessage());
            return payresult;
        }
        String memo = msg + "支付扣除" + orderprice + "元";
        try {
            String sql = "TrainorderDeduction " + new BigDecimal(orderid) + ", '" + ordernumber + "', "
                    + new BigDecimal(ywtype) + ", " + new BigDecimal(agentid) + ", "
                    + new BigDecimal(String.valueOf(0D - orderprice)) + ", " + new BigDecimal(1) + ", "
                    + new BigDecimal(Rebaterecord.PINGTAIXIAOFEI) + ", '" + memo + "', " + new BigDecimal(0) + ", '"
                    + (refordernum != null ? refordernum : "") + "', " + new BigDecimal(10) + ", " + new BigDecimal(1);
            WriteLog.write("tc虚拟账户支付new", "事务sql：" + sql);
            List list = Server.getInstance().getSystemService().findMapResultByProcedure(sql);
            if (null != list && list.size() > 0) {
                Map map = (Map) list.get(0);
                String result = map.get("result").toString();
                String error = map.get("message").toString();
                if (!"success".equals(result)) {// 失败
                    payresult.setPaysuccess(false);
                    payresult.setResultmessage(msg + " " + error);
                    WriteLog.write("tc虚拟账户支付new", "订单号ID：" + orderid + ":订单号：" + ordernumber);
                }
                else if ("success".equals(result)) { // 成功
                    payresult.setPaysuccess(true);
                    System.out.println("扣款成功" + result);
                    WriteLog.write("tc虚拟账户支付new", "订单号ID：" + orderid + ":订单号：" + ordernumber);
                }
            }
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + orderid + ":订单号：" + ordernumber + ":支付成功");
        }
        catch (Exception e) {
            e.printStackTrace();
            payresult.setPaysuccess(false);
            payresult.setResultmessage(msg + "支付失败！");
            WriteLog.write("tc虚拟账户支付new", "订单号ID：" + orderid + ":订单号：" + ordernumber + "," + e.getMessage());
        }

        return payresult;
    }

    /**
     * 获取系统配置属性 实时
     */
    @SuppressWarnings("unchecked")
    public static String getSystemConfigMap(String name) {
        String value = "-1";
        if (Server.getInstance().getDateHashMap().containsKey(name)) {
            value = Server.getInstance().getDateHashMap().get(name);
        }
        else {
            List<Sysconfig> configs = Server.getInstance().getSystemService()
                    .findAllSysconfig("where c_name='" + name + "'", "", -1, 0);
            if (configs != null && configs.size() == 1) {
                Sysconfig config = configs.get(0);
                value = config.getValue();
                Server.getInstance().getDateHashMap().put(name, value);
            }
        }
        return value;
    }

    /**
     * 获取当前时间
     */
    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }
}
