
package com.yeexing.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pnr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plcid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ibePrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="out_orderid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="disc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extReward" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "pnr",
    "plcid",
    "ibePrice",
    "outOrderid",
    "disc",
    "extReward",
    "sign"
})
@XmlRootElement(name = "PnrBook")
public class PnrBook {

    @XmlElement(required = true, nillable = true)
    protected String userName;
    @XmlElement(required = true, nillable = true)
    protected String pnr;
    @XmlElement(required = true, nillable = true)
    protected String plcid;
    @XmlElement(required = true, nillable = true)
    protected String ibePrice;
    @XmlElement(name = "out_orderid", required = true, nillable = true)
    protected String outOrderid;
    @XmlElement(required = true, nillable = true)
    protected String disc;
    @XmlElement(required = true, nillable = true)
    protected String extReward;
    @XmlElement(required = true, nillable = true)
    protected String sign;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the pnr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnr() {
        return pnr;
    }

    /**
     * Sets the value of the pnr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnr(String value) {
        this.pnr = value;
    }

    /**
     * Gets the value of the plcid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlcid() {
        return plcid;
    }

    /**
     * Sets the value of the plcid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlcid(String value) {
        this.plcid = value;
    }

    /**
     * Gets the value of the ibePrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIbePrice() {
        return ibePrice;
    }

    /**
     * Sets the value of the ibePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbePrice(String value) {
        this.ibePrice = value;
    }

    /**
     * Gets the value of the outOrderid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutOrderid() {
        return outOrderid;
    }

    /**
     * Sets the value of the outOrderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutOrderid(String value) {
        this.outOrderid = value;
    }

    /**
     * Gets the value of the disc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisc() {
        return disc;
    }

    /**
     * Sets the value of the disc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisc(String value) {
        this.disc = value;
    }

    /**
     * Gets the value of the extReward property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtReward() {
        return extReward;
    }

    /**
     * Sets the value of the extReward property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtReward(String value) {
        this.extReward = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

}
