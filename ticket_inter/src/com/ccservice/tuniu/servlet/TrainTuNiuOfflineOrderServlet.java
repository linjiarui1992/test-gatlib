/**
 * 版权所有, 空铁无忧
 * Author: 火车票 H5-微信端 项目开发组
 * copyright: 2017
 */
package com.ccservice.tuniu.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.Util.PropertyUtil;
import com.ccservice.Util.file.WriteLog;
import com.ccservice.b2b2c.util.ExceptionUtil;
import com.ccservice.offline.dao.MailAddressDao;
import com.ccservice.offline.dao.TrainOfflineAgentKeyDao;
import com.ccservice.offline.dao.TrainOfflineIdempotentDao;
import com.ccservice.offline.dao.TrainOrderOfflineDao;
import com.ccservice.offline.dao.TrainPassengerOfflineDao;
import com.ccservice.offline.dao.TrainTicketOfflineDao;
import com.ccservice.offline.domain.MailAddress;
import com.ccservice.offline.domain.TrainOfflineAgentKey;
import com.ccservice.offline.domain.TrainOfflineIdempotent;
import com.ccservice.offline.domain.TrainOrderOffline;
import com.ccservice.offline.domain.TrainPassengerOffline;
import com.ccservice.offline.domain.TrainTicketOffline;
import com.ccservice.offline.util.ExceptionTNUtil;
import com.ccservice.offline.util.HttpPostJsonUtil;
import com.ccservice.offline.util.NewDelieveUtils;
import com.ccservice.offline.util.SignUtil;
import com.ccservice.offline.util.TrainOrderOfflineUtil;
import com.ccservice.offline.util.TuNiuDesUtil;

/**
 * @className: com.ccservice.tuniu.train.servlet.TrainTuNiuOfflineOrderServlet
 * @description: TODO - 
 * 
 * 途牛线下票 - 途牛线下票出票请求接口
 * 
 * 需要入库，入库之后会在平台上展示，不需要建库。用以前的
 * 参考以前的淘宝，去哪儿，艺龙的下单
 * 
 * 主要项目平台操作这些在cn_home里面
 * 
 * 线下出票接口，途牛请求供应商，此接口中的票要么都成功，要么都失败，不存在部分的情况。
 * 
 * 
Method：HTTP-POST
Url：待定
DataType：json
 * CreateBusLogAdminNoUserId
 * success true用231000，false用231099
 * 
 * 接口不是可以提交10个人么
 * 
接口不是可以提交10个人么
一个成人最多带4个儿童。
@北京-技术-郭征举 
接口不是可以提交10个人么
郑州-技术-郭伟强(2662136489)  9:57:08
那这个你最好跟他们那边沟通限制一下
北京-产品-郅鹏(1376922541)  9:57:58
3、只在回填的时候价格校验啊。
北京-产品-郅鹏(1376922541)  9:59:41
@郑州-技术-郭伟强 一个订单最多提交5个人一个乘客带4个儿童。
 * 
 * 途牛并未区分配送到站和配送票
 * 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月15日 上午9:34:25 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainTuNiuOfflineOrderServlet extends HttpServlet {
    private static final String LOGNAME = "途牛线下票出票请求接口";

    private int r1 = new Random().nextInt(10000000);

    private TrainOrderOfflineDao trainOrderOfflineDao = new TrainOrderOfflineDao();

    private TrainOfflineAgentKeyDao trainOfflineAgentKeyDao = new TrainOfflineAgentKeyDao();

    private TrainTicketOfflineDao trainTicketOfflineDao = new TrainTicketOfflineDao();

    private TrainPassengerOfflineDao trainPassengerOfflineDao = new TrainPassengerOfflineDao();

    private MailAddressDao mailAddressDao = new MailAddressDao();

    private TrainOfflineIdempotentDao trainOfflineIdempotentDao = new TrainOfflineIdempotentDao();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        String line = null;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        //将资料解码  
        String reqBody = sb.toString();
        //System.out.println(reqBody);

        //记录请求信息日志
        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求信息-reqBody-->" + reqBody);

        //幂等响应的初始化操作放在最开始接收到请求的地方

        //结果的返回
        JSONObject resResult = new JSONObject();

        PrintWriter out = response.getWriter();

        Boolean resultFlag = true;

        String reqBodyTemp = reqBody.substring(reqBody.indexOf("data") + 7, reqBody.length() - 2);

        //幂等的设计和实现
        String idempotentFlag = "TuNiuOrder";

        //更换为数据库的持久化形式
        String idempotentLockKey = reqBodyTemp + idempotentFlag + "Lock";

        //String OrderLock = TrainOrderOfflineUtil.TnIdempotent.get(reqBodyTemp+idempotentFlag+"Lock");

        Integer PKID = 0;

        TrainOfflineIdempotent trainOfflineIdempotent = null;

        b: for (int i = 0; i < TrainOrderOfflineUtil.TNIDEMPOTENTRETRY; i++) {//幂等的后续处理-最多尝试三次 - 主要是跳出当前的并发处理逻辑-作为其它请求

            //判定是否是二次请求
            //Boolean OrderLock = false;
            try {
                //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(reqBodyTemp+idempotentFlag+"Lock");

                //减少访问次数，便于下次的复用和处理
                //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(idempotentLockKey);
                trainOfflineIdempotent = trainOfflineIdempotentDao
                        .findTrainOfflineIdempotentByLockKey(idempotentLockKey);
            }
            catch (Exception e1) {
                resResult = ExceptionTNUtil.handleTNException(e1);

                //记录请求信息日志
                WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");

                out.print(resResult.toJSONString());
                out.flush();
                out.close();

                return;
            }

            if (trainOfflineIdempotent == null) {//锁定状态
                //幂等中的请求初始化数据 - 
                //TrainOrderOfflineUtil.initIdempotent(reqBodyTemp, idempotentFlag);
                //进行最初的初始化
                try {
                    //trainOfflineIdempotentDao.addTrainOfflineIdempotent(reqBodyTemp+idempotentFlag+"Lock", true);
                    PKID = Integer
                            .valueOf(trainOfflineIdempotentDao.addTrainOfflineIdempotent(idempotentLockKey, true));
                }
                catch (Exception e) {
                    resResult = ExceptionTNUtil.handleTNException(e);

                    //记录请求信息日志
                    WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //初始化之后进行赋值，方便后续使用
                try {
                    trainOfflineIdempotent = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                }
                catch (Exception e) {
                    resResult = ExceptionTNUtil.handleTNException(e);

                    //记录请求信息日志
                    WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");

                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();

                    return;
                }

                //记录请求信息日志
                //WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-初始化请求一次");

                //一次请求完毕之后，也需要清除幂等的效果

                //做二次健壮性判断
                Integer lockCount = 0;
                try {
                    lockCount = trainOfflineIdempotentDao.findCountByLockKey(idempotentLockKey);
                }
                catch (Exception e) {
                    ExceptionTNUtil.handleTNException(e);
                }
                if (lockCount > 1) {
                    //WriteLog.write("幂等判定流程测试", "进入重复请求处理流程-lockCount:"+lockCount);

                    //删除当前增加，并重新走b循环的流程
                    Integer flagTemp = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(PKID);
                    if (flagTemp < 1) {
                        //记录请求信息日志
                        WriteLog.write("途牛线下票出票请求幂等判定", "途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                    }
                    continue b;
                }

                break b;
            }
            else {
                PKID = trainOfflineIdempotent.getPKID();

                //幂等中的一次请求 - 该方法涉及到获取和存储，设计成同步的
                //内里更新成为数据库的请求方式 - 上述已经完成了初始化的操作
                //String OrderReqNum = TrainOrderOfflineUtil.idempotentGetPut(reqBodyTemp, idempotentFlag+"ReqNum");
                //trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPut(trainOfflineIdempotent, 1);//其它次数的请求

                //执行此操作前需要进行二次判定 - 判定是否已经做了清空操作

                /*TrainOfflineIdempotent trainOfflineIdempotentTemp = null;
                try {
                    trainOfflineIdempotentTemp = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                }
                catch (Exception e1) {
                    resResult = ExceptionTNUtil.handleTNException(e1);
                
                    //记录请求信息日志
                    WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");
                
                    out.print(resResult.toJSONString());
                    out.flush();
                    out.close();
                    
                    return;
                }
                
                if (trainOfflineIdempotentTemp == null) {//锁定状态
                    try {
                        Thread.sleep(3*1000);
                    }
                    catch (InterruptedException e) {
                        ExceptionTNUtil.handleTNException(e);
                    }
                    continue b;
                }*/

                trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutReqNum(trainOfflineIdempotent);//其它次数的请求

                //WriteLog.write("幂等判定流程测试", r1 + ":PKID-"+PKID+",idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum());

                //记录请求信息日志
                //WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-新增请求:"+OrderReqNum);

                int loopNum = 1;

                a: while (true) {
                    //获取响应的结果
                    //String OrderHandleOverResultFlag = TrainOrderOfflineUtil.TnIdempotent.get(reqBodyTemp+idempotentFlag+"HandleOverResultFlag");
                    String idempotentResultValue = trainOfflineIdempotent.getIdempotentResultValue();

                    //此处应做实时查询的动作之前，先进行一次判定
                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {//先尝试本地取
                        try {
                            idempotentResultValue = trainOfflineIdempotentDao.findResultValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);

                            //记录请求信息日志
                            WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");

                            continue a;
                        }
                    }

                    //等待相同的结果出现之后直接进行反馈

                    if (idempotentResultValue == null || "".equals(idempotentResultValue)) {//二次判定 - 数据库的取出结果

                        //需要再取出一次做是否已删除的尝试判定 - 

                        /*TrainOfflineIdempotent trainOfflineIdempotentTemp = null;
                        try {
                            //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(reqBodyTemp+idempotentFlag+"Lock");
                            
                            //减少访问次数，便于下次的复用和处理
                            //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(idempotentLockKey);
                            trainOfflineIdempotentTemp = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);
                        
                            //记录请求信息日志
                            WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");
                        
                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();
                            
                            return;
                        }
                        
                        if (trainOfflineIdempotentTemp == null) {//锁定状态
                            try {
                                Thread.sleep(3*1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }
                            continue b;
                        }*/

                        //防止循环对象的提前删除
                        Boolean idempotentLockValue = null;
                        try {
                            idempotentLockValue = trainOfflineIdempotentDao.findLockValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);

                            //记录请求信息日志
                            WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");

                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();

                            return;
                        }

                        if (idempotentLockValue == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }
                            continue b;
                        }

                        try {
                            Thread.sleep(3 * 1000);
                        }
                        catch (InterruptedException e) {
                            ExceptionTNUtil.handleTNException(e);
                        }

                        loopNum++;

                        //记录请求信息日志
                        //WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-等待反馈中:"+TrainOrderOfflineUtil.getNowDateStr());

                        //一分钟之后自动结束响应，避免报错之下引起的无限循环对程序本身造成困扰 - 在此处的可重复次数为 - 13次

                        if (loopNum >= TrainOrderOfflineUtil.TNIDEMPOTENTRETRY) {
                            Boolean resFlag = false;
                            Integer errorCode = 231099;//success true用231000，false用231099

                            resResult = new JSONObject();

                            resResult.put("success", resFlag);
                            resResult.put("errorCode", errorCode);

                            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求接口-报错之下引起的无限循环的卡断程序启动，接口反馈异常信息");

                            //resResult = JSONObject.parseObject(OrderHandleOverResultFlag);//多余的不该出现的代码 - 并未反馈正常的结果信息
                            resultFlag = false;

                            break b;
                        }

                        continue a;
                    }
                    else {
                        //幂等中的一次响应 - 该方法涉及到获取和存储，设计成同步的
                        //String OrderResNum = TrainOrderOfflineUtil.idempotentGetPut(reqBodyTemp, idempotentFlag+"ResNum");

                        //响应的添加和比对删除的动作也必须设置成同步的 - 
                        //trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPut(trainOfflineIdempotent, 2);//减少次数的响应

                        /*Boolean idempotentLockValue = null;
                        try {
                            //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(reqBodyTemp+idempotentFlag+"Lock");
                            
                            //减少访问次数，便于下次的复用和处理
                            //OrderLock = trainOfflineIdempotentDao.findLockValueByLockKey(idempotentLockKey);
                            //trainOfflineIdempotentTemp = trainOfflineIdempotentDao.findTrainOfflineIdempotentByPKID(PKID);
                            idempotentLockValue = trainOfflineIdempotentDao.findLockValueByPKID(PKID);
                        }
                        catch (Exception e1) {
                            resResult = ExceptionTNUtil.handleTNException(e1);
                        
                            //记录请求信息日志
                            WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-幂等数据交互流程报错");
                        
                            out.print(resResult.toJSONString());
                            out.flush();
                            out.close();
                            
                            return;
                        }
                        
                        if (idempotentLockValue == null) {//锁定状态
                            try {
                                Thread.sleep(3*1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }
                        
                            //记录请求信息日志
                            WriteLog.write("幂等判定流程测试", r1 + ":PKID-"+PKID+"-被删除后进入了响应动作");
                        
                            continue b;
                        }*/

                        //在这内里做了另外形式的判断
                        trainOfflineIdempotent = TrainOrderOfflineUtil.idempotentGetPutResNum(trainOfflineIdempotent);//减少次数的响应

                        if (trainOfflineIdempotent == null) {//锁定状态
                            try {
                                Thread.sleep(3 * 1000);
                            }
                            catch (InterruptedException e) {
                                ExceptionTNUtil.handleTNException(e);
                            }

                            //记录请求信息日志
                            //WriteLog.write("幂等判定流程测试", r1 + ":PKID-"+PKID+"-被删除后进入了响应动作");

                            continue b;
                        }

                        //记录请求信息日志
                        //WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-获取到结果之后进行一次反馈:"+OrderResNum);

                        //此处的时间应该已经发生了变化，需要再取一次最新的做判定
                        //OrderReqNum = TrainOrderOfflineUtil.TnIdempotent.get(reqBodyTemp+idempotentFlag+"ReqNum");

                        //此处的
                        /*Integer idempotentReqNum = trainOfflineIdempotent.getIdempotentReqNum();//请求次数
                        Integer idempotentResNum = trainOfflineIdempotent.getIdempotentResNum();//响应次数
                        
                        WriteLog.write("幂等判定流程测试", "PKID-"+trainOfflineIdempotent.getPKID()+",idempotentReqNum="+trainOfflineIdempotent.getIdempotentReqNum()+",idempotentResNum="+idempotentResNum);
                        
                        if (idempotentReqNum.equals(idempotentResNum)) {//请求和响应次数一致，做删除动作
                            //TrainOrderOfflineUtil.removeData(reqBodyTemp, idempotentFlag);// - 删除数据库的记录 - 
                            Integer flag = trainOfflineIdempotentDao.delTrainOfflineIdempotentByPKID(trainOfflineIdempotent.getPKID());
                            if (flag<1) {
                                //记录请求信息日志
                                WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作---数据库清空失败");
                            }
                        
                            //记录请求信息日志
                            //WriteLog.write("途牛线下票出票请求幂等判定", r1 + ":途牛线下票出票请求幂等设计-请求和反馈一致之后进行了清空操作:"+idempotentResNum);
                        
                            WriteLog.write("幂等判定流程测试", r1 + ":idempotentReqNum-"+idempotentReqNum+",idempotentResNum="+idempotentResNum+",进行了清空操作-PKID-"+trainOfflineIdempotent.getPKID());
                        
                        }*/

                        resResult = JSONObject.parseObject(idempotentResultValue);
                        resultFlag = false;

                        break b;
                    }
                }
            }

        }

        //内部逻辑抛异常引起的幂等的请求接口的挂掉，最好单独抛线程处理 - 增强接口的健壮性

        if (resultFlag) {
            resResult = new JSONObject();

            Boolean resFlag = false;
            Integer errorCode = 231099;//success true用231000，false用231099

            String partnerid = PropertyUtil.getValue("tuniu.account", "Train.GuestAccount.properties");
            String key = PropertyUtil.getValue("tuniu.desKey", "Train.GuestAccount.properties");

            JSONObject requestBody = JSONObject.parseObject(reqBody);
            /**
             * 传入类似空铁接口的JSON串格式的数据，对其直接进行解析
             */
            /*String account = request.getParameter("account");
            String sign = request.getParameter("sign");
            
            String timestamp = request.getParameter("timestamp");
            
            //与当期系统时间进行比对，比对时间戳？？？ - 时间可能有差值，不用比对
            
            String data = request.getParameter("data");*/

            String account = requestBody.getString("account");
            String sign = requestBody.getString("sign");

            String timestamp = requestBody.getString("timestamp");

            String data = requestBody.getString("data");

            //记录请求信息日志
            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求信息-account-->" + account + "-sign-->" + sign + "-timestamp-->"
                    + timestamp + "-data:" + data);

            //身份校验 - 数据解密，签名校验
            //传的是什么，就用什么解密
            String checkSign = SignUtil.generateSign(account, data, timestamp, key);

            String dataStr = TuNiuDesUtil.decrypt(data);
            //后续再做转换处理
            JSONObject reqData = JSONObject.parseObject(dataStr);

            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-信息解密与校验-checkSign-->" + checkSign + "-reqData-->" + reqData);

            if (!account.equals(partnerid)) {//校验不通过的话，返回失败
                resResult.put("success", resFlag);
                resResult.put("errorCode", errorCode);
                resResult.put("msg", "不合法的请求-签名或者账户名错误");
            }
            else {
                if (sign.equals(checkSign)) {
                    //校验通过的话，可以存储数据

                    /*try {
                        Thread.sleep(10*1000);//幂等测试
                    }
                    catch (InterruptedException e) {
                        ExceptionTNUtil.handleTNException(e);
                    }*/

                    resResult = addTuNiuOffline(resResult, reqData, trainOfflineIdempotent);
                }
                else {//校验不通过的话，返回失败
                    resResult.put("success", resFlag);
                    resResult.put("errorCode", errorCode);
                    resResult.put("msg", "不合法的请求-签名或者账户名错误");
                }
            }

            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求结果-resResult" + resResult);

        }

        out.print(resResult.toJSONString());
        out.flush();
        out.close();
    }

    //private JSONObject addTuNiuOffline(JSONObject resResult, JSONObject reqData, String reqBody, String idempotentFlag) {
    private JSONObject addTuNiuOffline(JSONObject resResult, JSONObject reqData,
            TrainOfflineIdempotent trainOfflineIdempotent) {
        Boolean resFlag = false;
        Integer errorCode = 231099;//success true用231000，false用231099

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-进入到方法-addTuNiuOffline");

        //重复订单校验
        String orderId = reqData.getString("orderId");//途牛订单号

        JSONObject dataResult = new JSONObject();
        dataResult.put("orderId", orderId);
        resResult.put("data", dataResult);

        if (orderId == null || orderId.equals("")) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单不存在，请联系查询传递的订单号");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        TrainOrderOffline trainOrderOffline = null;
        try {
            trainOrderOffline = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        WriteLog.write(LOGNAME,
                r1 + ":途牛线下票出票请求-进入到方法-orderId-->" + orderId + ",trainOrderOffline-->" + trainOrderOffline);

        if (trainOrderOffline != null) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "该订单已存在，请勿重复提交");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //判定和存储callBackUrl
        String callBackUrl = reqData.getString("callBackUrl");//回调地址

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-callBackUrl-->" + callBackUrl);

        //存储到配置信息的表中 - trainTuNiuOfflineOrderCallBackUrl
        if (callBackUrl != null && !callBackUrl.equals("")) {
            TrainOfflineAgentKey trainOfflineAgentKey = null;
            try {
                trainOfflineAgentKey = trainOfflineAgentKeyDao
                        .findTrainOfflineAgentKeyByPartnerName("trainTuNiuOfflineOrderCallBackUrl");
            }
            catch (Exception e1) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e1);
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }

            if (trainOfflineAgentKey == null) {//进行添加
                TrainOfflineAgentKey trainOfflineAgentKeyTemp = new TrainOfflineAgentKey();
                trainOfflineAgentKeyTemp.setPartnerName("trainTuNiuOfflineOrderCallBackUrl");
                trainOfflineAgentKeyTemp.setKeys(callBackUrl);
                try {
                    trainOfflineAgentKeyDao.addTrainOfflineAgentKey(trainOfflineAgentKeyTemp);
                }
                catch (Exception e) {
                    //幂等的设计和实现
                    JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
                    TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                    return jsonTemp;
                }
            }

            if (trainOfflineAgentKey != null && !trainOfflineAgentKey.getKeys().equals(callBackUrl)) {//进行修改
                trainOfflineAgentKeyDao.updateTrainOfflineAgentKeysByPkId(trainOfflineAgentKey.getPkId(), callBackUrl);
            }
        }

        //创建火车票线下订单TrainOrderOffline
        //处理相关的数据进行入库操作
        trainOrderOffline = new TrainOrderOffline();

        //生成自己系统平台的订单号
        String OrderNumber = "TN" + TrainOrderOfflineUtil.timeMinID();
        trainOrderOffline.setOrderNumber(OrderNumber);
        trainOrderOffline.setOrderNumberOnline(orderId);

        //不能为空的字段的值 - 分单逻辑

        trainOrderOffline.setOrderStatus(1);//订单状态

        //trainOrderOffline.setSupplyPayWay(0);
        //trainOrderOffline.setRefundReason(0);
        //trainOrderOffline.setChuPiaoAgentid(0);
        trainOrderOffline.setLockedStatus(0);

        String cheCi = reqData.getString("cheCi");//车次 - 

        String fromStationCode = reqData.getString("fromStationCode");//出发站简码 - 【无用字段，无需处理】

        String fromStationName = reqData.getString("fromStationName");//出发站名称

        String toStationCode = reqData.getString("toStationCode");//到达站简码 - 【无用字段，无需处理】

        String toStationName = reqData.getString("toStationName");//到达站名称

        String trainDate = reqData.getString("trainDate");//乘车日期 - 2015-08-09
        String departTime = reqData.getString("departTime");//出发时间 - 07:02

        String arriveTime = reqData.getString("arriveTime");//到达时间 - 05:57

        //获取包含乘客信息和保单信息的 passengers - JSONArray - 至少有一个
        JSONArray passengers = reqData.getJSONArray("passengers");//乘客信息

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-passengers-->" + passengers);

        List<TrainTicketOffline> trainTicketOfflineList = new ArrayList<TrainTicketOffline>();
        List<TrainPassengerOffline> trainPassengerOfflineList = new ArrayList<TrainPassengerOffline>();

        Double orderPrice = 0.0D;

        int ticketCount = passengers.size();

        /*if (ticketCount > 5) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "线下票请求出票接口，一个订单最多只能处理五张票");
        
            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(reqBody, idempotentFlag, resResult.toJSONString(), r1);
            
            return resResult;
        }*/

        trainOrderOffline.setTicketCount(ticketCount);

        //判断儿童票的张数和成人票的张数
        int adultNum = 0;
        int kidNum = 0;

        Boolean isHaveStu = false;
        Boolean isHaveArmy = false;

        for (int i = 0; i < ticketCount; i++) {
            JSONObject passenger = passengers.getJSONObject(i);

            //创建火车票线下订单TrainPassengerOffline
            //处理相关的数据进行入库操作
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();

            Integer passengerId = passenger.getInteger("passengerId");//乘客的顺序号
            trainPassengerOffline.setPassengerId(String.valueOf(passengerId));

            String ticketNo = passenger.getString("ticketNo");//票号（此票在本订单中的唯一标识，订票成功后才有值） - 【回调出票的地方会完成信息的回填 - 非必须】

            String passengerName = passenger.getString("passengerName");//乘客姓名
            trainPassengerOffline.setName(passengerName);

            String passportNo = passenger.getString("passportNo");//乘客证件号码
            trainPassengerOffline.setIdNumber(passportNo);

            //证件类型ID - 与名称对应关系: - 1:二代身份证，2:一代身份证，C:港澳通行证，G:台湾通行证，B:护照更多参见证件编码对应表
            String passportTypeId = passenger.getString("passportTypeId");
            if (passportTypeId.equals("2") || passportTypeId.equals("H")) {//不符合要求的证件类型
                resResult.put("success", resFlag);
                resResult.put("errorCode", errorCode);
                resResult.put("msg", "不符合要求的证件类型，请查询");

                //幂等的设计和实现
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

                return resResult;
            }

            //乘客证件类型 - 1:二代身份证，2:一代身份证，C:港澳通行证，G:台湾通行证，B:护照 ,H:外国人居留证 - 途牛
            trainPassengerOffline.setIdType(TrainOrderOfflineUtil.getTuNiuIdType(passportTypeId));
            trainPassengerOfflineList.add(trainPassengerOffline);

            String passportTypeName = passenger.getString("passportTypeName");//证件类型名称 - 【无用字段，无需处理 - 后期在程序内部做了处理和显示】

            //创建火车票线下订单TrainOrderOffline
            //处理相关的数据进行入库操作
            TrainTicketOffline trainTicketOffline = new TrainTicketOffline();

            //来自外围数据
            trainTicketOffline.setTrainNo(cheCi);
            trainTicketOffline.setDeparture(fromStationName);
            trainTicketOffline.setArrival(toStationName);

            trainTicketOffline.setDepartTime(trainDate + " " + departTime + ":00.000");// - 2016-06-28 18:10:00.000
            //trainTicketOffline.setDepartTime(trainDate);// - 2016-06-28 18:10:00.000

            trainTicketOffline.setStartTime(departTime);// - 2016-06-28 18:10:00.000
            trainTicketOffline.setArrivalTime(arriveTime);
            trainTicketOffline.setCostTime("0");

            //票种ID。 - 与票种名称对应关系： - 1:成人票，2:儿童票，3:学生票，4:残军票
            String piaoType = passenger.getString("piaoType");
            trainTicketOffline.setTicketType(Integer.valueOf(piaoType));

            if (piaoType.endsWith("1")) {
                adultNum += 1;
            }
            else if (piaoType.endsWith("2")) {
                kidNum += 1;
            }
            else if (piaoType.endsWith("3")) {
                isHaveStu = true;
            }
            else if (piaoType.endsWith("4")) {
                isHaveArmy = true;
            }

            String piaoTypeName = passenger.getString("piaoTypeName");//票种名称 - 【无用字段，无需处理 - 后期在程序内部做了处理和显示】

            /**
             * 座位编码 - 
             * 与座位名称对应关系： - 
             * 9:商务座，P:特等座，M:一等座，O:二等座，6:高级软卧， - 
             * 4:软卧，3:硬卧，2:软座，1:硬座更多参见座位编码对应表 - 
             * 
             * 注意：当最低的一种座位，
             * 无票时，购买选择该座位种类，
             * 买下的就是无座(也就说买无座的席别编码就是该车次的最低席别的编码)，
             * 
             * 另外，当最低席别的票卖完了的时候才可以卖无座的票。
             */
            String zwCode = passenger.getString("zwCode");// - 【无用字段，无需处理 - 对应的座位的编码 - 后期在程序内部做了处理和显示】

            String zwName = passenger.getString("zwName");//座位名称 - 【非必须】
            trainTicketOffline.setSeatType(zwName);

            String cxin = passenger.getString("cxin");//几车厢几座 - 【非必须】

            String price = passenger.getString("price");//票价
            trainTicketOffline.setPrice(Double.valueOf(price));

            trainTicketOfflineList.add(trainTicketOffline);

            orderPrice += Double.valueOf(price);

            String provinceCode = passenger.getString("provinceCode");//省份编号 - 【非必须】

            String schoolCode = passenger.getString("schoolCode");//学校代号 - 【非必须】
            String schoolName = passenger.getString("schoolName");//学校名称 - 【非必须】
            String studentNo = passenger.getString("studentNo");//学号 - 【非必须】
            String schoolSystem = passenger.getString("schoolSystem");//学制 - 【非必须】
            String enterYear = passenger.getString("enterYear");//入学年份 - 【非必须】

            String preferenceFromStationName = passenger.getString("preferenceFromStationName");//优惠区间起始地名称 - 【非必须】
            String preferenceFromStationCode = passenger.getString("preferenceFromStationCode");//优惠区间起始地代号 - 【非必须】
            String preferenceToStationName = passenger.getString("preferenceToStationName");//优惠区间到达地名称 - 【非必须】
            String preferenceToStationCode = passenger.getString("preferenceToStationCode");//优惠区间到达地代号 - 【非必须】

        }

        if (isHaveStu) {//如果包含学生票，不允许
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "线下票请求出票接口，不支持学生票，需要学生证原件才能购买");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        if (isHaveArmy) {//如果包含残军票，不允许
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "线下票请求出票接口，不支持的售票类型-残军票");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //判断儿童票的张数和成人票的张数 - 不能单独乘车
        if (kidNum > 0 && kidNum == ticketCount) {
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "线下票请求出票接口，儿童不能单独乘车");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //通过票的信息进行单独的订单的金额的总的计算 - 
        trainOrderOffline.setOrderPrice(orderPrice);

        JSONObject deliveryInfo = reqData.getJSONObject("deliveryInfo");//配送信息

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-deliveryInfo-->" + deliveryInfo);

        //创建火车票线下订单MailAddress
        //处理相关的数据进行入库操作
        MailAddress mailAddress = new MailAddress();

        String recipientName = deliveryInfo.getString("recipientName");//收件人
        mailAddress.setMAILNAME(recipientName);

        String deliveryAddress = deliveryInfo.getString("deliveryAddress");//收件地址
        mailAddress.setADDRESS(deliveryAddress);

        String zipCode = deliveryInfo.getString("zipCode");//邮编
        mailAddress.setPOSTCODE(zipCode);

        String recipientPhone = deliveryInfo.getString("recipientPhone");//收件人联系电话
        mailAddress.setMAILTEL(recipientPhone);

        //通过快递信息设计分单逻辑 - 分配代售点

        //途牛线下订单分流 ---线下票分流--途牛----66 - 北京北京海淀#西三旗街道永泰东里x号楼x单元x室
        String createUid = "80";
        String createUser = "途牛";
        String agentidtemp = "";

        //春节期间的快递的拦截 - 今年的春节的时间
        String star = "2017-01-26 00:00:00";//开始时间
        String end = "2017-02-03 00:00:00";//结束时间
        SimpleDateFormat localTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date sdate = localTime.parse(star);
            Date edate = localTime.parse(end);
            long time = System.currentTimeMillis();
            if (time >= sdate.getTime() && time < edate.getTime()) {
                if (deliveryAddress.contains("县") || deliveryAddress.contains("镇") || deliveryAddress.contains("乡")) {
                    agentidtemp = "414";//分到了测试账号
                    WriteLog.write("春节途牛部分地区拦截", r1 + ":addTuNiuOffline:" + "线下火车票分单log记录:地址:" + deliveryAddress
                            + "----->agentId:" + agentidtemp);
                }
            }
            else {
                agentidtemp = TrainOrderOfflineUtil.distribution(deliveryAddress, Integer.valueOf(createUid));
            }
        }
        catch (Exception e) {
            ExceptionUtil.writelogByException("途牛从数据库中获取分单的代售点失败，或者，途牛春节顺丰无法邮寄的地区拦截异常", e);

            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }
        WriteLog.write("TrainTuNiuOfflineOrderServlet",
                r1 + ":addTuNiuOffline:" + "线下火车票分单log记录:地址:" + deliveryAddress + "----->agentId:" + agentidtemp);

        trainOrderOffline.setCreateUId(Integer.valueOf(createUid));
        trainOrderOffline.setCreateUser(createUser);

        //对于逻辑中可能存在的转换异常抛出的情况需要做相关的处理 - 并解除幂等的等待限制

        int agentid = 0;
        if (agentidtemp.contains("途牛的默认出票点的信息不存在")) {//
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "途牛的默认出票点的信息不存在");

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }
        else {
            agentid = Integer.valueOf(agentidtemp);
            trainOrderOffline.setAgentId(agentid);//分配逻辑中分配的代售点的相关的信息
        }

        //待补充 - 暂时设定为0
        trainOrderOffline.setAgentProfit(0.0);

        /**
         * 非必须字段 - 【非必须】
         * 
         * 个人定制信息，可以指定几个上下铺等信息。如果指定了订单信息，并不接受其他情况，当出不了指定的铺位或坐席时，会出票失败处理。
         * 
         * 定制信息 - 录入线下订单表的部分 - 待补充
         * 
         */
        String extSeat = "无";
        trainOrderOffline.setExtSeat(extSeat);

        //这个需要做非空判断，因为是非必传字段
        JSONObject personalTailor = reqData.getJSONObject("personalTailor");

        String TradeNo = "无";

        StringBuilder TradeNoSb = new StringBuilder("");

        Boolean hasSeat = reqData.getBoolean("hasSeat");//是否出无座票 - true:不出无座票 - false:允许出无座票
        if (hasSeat) {
            //trainOrderOffline.setPaperType(6);
            TradeNoSb.append("不允许出无座票");
        }
        else {
            //trainOrderOffline.setPaperType(7);
            TradeNoSb.append("允许出无座票");
        }

        trainOrderOffline.setPaperType(0);

        int PaperBackup = 1;//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
        int paperLowSeatCount = 0;//至少接受下铺/靠窗/连坐数量

        if (personalTailor != null) {
            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-personalTailor-->" + personalTailor);

            /**
             * 定制信息的逻辑待补充
             */
            Boolean acceptOthers = personalTailor.getBoolean("acceptOthers");//是否接受其他（定制以外）坐席

            String info = personalTailor.getString("info");//座位信息 - A-C-D - 【非必须】
            Boolean isConnecting = personalTailor.getBoolean("isConnecting");//是否连座 - 【非必须】
            Integer connectingNum = personalTailor.getInteger("connectingNum");//连座数目 - 【非必须】
            Integer nextWindowNumber = personalTailor.getInteger("nextWindowNumber");//靠窗数目

            Boolean isSameRoom = personalTailor.getBoolean("isSameRoom");//是否同一个房间 - 【非必须】
            Integer topNum = personalTailor.getInteger("topNum");//上铺数目 - 【非必须】
            Integer middleNum = personalTailor.getInteger("middleNum");//中铺数目 - 【非必须】
            Integer lowerNum = personalTailor.getInteger("lowerNum");//下铺数目 - 【非必须】

            if (info != null && !info.equals("")) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("指定座位：" + info);
                }
                else {
                    TradeNoSb.append("，指定座位：" + info);
                }
            }

            if (isConnecting) {
                PaperBackup = 1;
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("要求连座");
                }
                else {
                    TradeNoSb.append("，要求连座");
                }
            }
            else {
                /*if (TradeNoSb!=null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("不要求连座");
                } else {
                    TradeNoSb.append("，不要求连座");
                }*/
            }

            if (connectingNum > 0) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("指定连座票，至少需要" + connectingNum + "张连座票");
                }
                else {
                    TradeNoSb.append("，指定连座票，至少需要" + connectingNum + "张连座票");
                }
            }

            if (nextWindowNumber > 0) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("指定靠窗票，至少需要" + nextWindowNumber + "张靠窗票");
                }
                else {
                    TradeNoSb.append("，指定靠窗票，至少需要" + nextWindowNumber + "张靠窗票");
                }
            }

            if (isSameRoom) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    //TradeNoSb.append("铺位席别要求在同一个房间");
                    TradeNoSb.append("指定同包厢");
                }
                else {
                    TradeNoSb.append("，指定同包厢");
                }
            }
            else {
                /*if (TradeNoSb!=null && TradeNoSb.toString().equals("")) {
                    TradeNoSb.append("铺位席别不要求在同一个房间");
                } else {
                    TradeNoSb.append("，铺位席别不要求在同一个房间");
                }*/
            }

            if (topNum > 0) {
                if (isSameRoom) {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("至少需要" + topNum + "张上铺票");
                    }
                    else {
                        TradeNoSb.append("，至少需要" + topNum + "张上铺票");
                    }
                }
                else {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("指定上铺票，至少需要" + topNum + "张上铺票");
                    }
                    else {
                        TradeNoSb.append("，指定上铺票，至少需要" + topNum + "张上铺票");
                    }
                }
            }

            if (middleNum > 0) {
                if (isSameRoom) {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("至少需要" + middleNum + "张中铺票");
                    }
                    else {
                        TradeNoSb.append("，至少需要" + middleNum + "张中铺票");
                    }
                }
                else {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("指定中铺票，至少需要" + middleNum + "张中铺票");
                    }
                    else {
                        TradeNoSb.append("，指定中铺票，至少需要" + middleNum + "张中铺票");
                    }
                }
            }

            if (lowerNum > 0) {
                PaperBackup = 0;
                paperLowSeatCount = lowerNum;
                if (isSameRoom) {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("至少需要" + lowerNum + "张下铺票");
                    }
                    else {
                        TradeNoSb.append("，至少需要" + lowerNum + "张下铺票");
                    }
                }
                else {
                    if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                        TradeNoSb.append("指定下铺票，至少需要" + lowerNum + "张下铺票");
                    }
                    else {
                        TradeNoSb.append("，指定下铺票，至少需要" + lowerNum + "张下铺票");
                    }
                }
            }

            if (acceptOthers) {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    //TradeNoSb.append("接受其他（定制以外）席别");
                    TradeNoSb.append("(无法满足定制服务时：是否接受其它席别：接受)");
                }
                else {
                    TradeNoSb.append("。(无法满足定制服务时：是否接受其它席别：接受)");
                }
            }
            else {
                if (TradeNoSb != null && TradeNoSb.toString().equals("")) {
                    //TradeNoSb.append("不接受其他（定制以外）席别");
                    TradeNoSb.append("(无法满足定制服务时：是否接受其它席别：不接受)");
                }
                else {
                    TradeNoSb.append("。(无法满足定制服务时：是否接受其它席别：不接受)");
                }
            }

            /*if (TradeNoSb != null && !TradeNo.equals("")) {
                trainOrderOffline.setTradeNo(TradeNoSb.toString());
            } else {
                trainOrderOffline.setTradeNo("无");
            }*/
        }

        TradeNo = TradeNoSb.toString();

        trainOrderOffline.setTradeNo(TradeNo);

        //这两个即使没有，也必须传入
        trainOrderOffline.setPaperBackup(PaperBackup);
        trainOrderOffline.setPaperLowSeatCount(paperLowSeatCount);

        //其它多余无用信息的补充
        trainOrderOffline.setContactUser(recipientName);
        trainOrderOffline.setContactTel(recipientPhone);

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-trainOrderOffline-->" + trainOrderOffline);

        TrainOrderOffline trainOrderOfflineTemp = null;
        try {
            trainOrderOfflineTemp = trainOrderOfflineDao.findTrainOrderOfflineByOrderNumberOnline(orderId);
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        if (trainOrderOfflineTemp != null) {
            //插入数据之前的幂等的二次判断逻辑
            resFlag = true;
            errorCode = 231000;//success true用231000，false用231099
            resResult.put("success", resFlag);
            resResult.put("errorCode", errorCode);
            resResult.put("msg", "请求已经接收");

            //此处另开线程处理出票请求超时的逻辑

            //幂等的设计和实现
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

            return resResult;
        }

        //插入线下订单表 - 
        Long offlineOrderId = 0L;
        try {
            offlineOrderId = Long.valueOf(trainOrderOfflineDao.addTrainOrderOffline(trainOrderOffline));
        }
        catch (Exception e) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        //更新快递时效信息
        String delieveStr = "暂无快递信息!";

        //利用顺丰的接口，获取快递时效信息 - 46 - 系统
        try {
            //        	delieveStr = TrainOrderOfflineUtil.getDelieveStr(offlineOrderId, agentid, deliveryAddress);//        	
            //            delieveStr = DelieveUtils.getDelieveStr(String.valueOf(agentid), deliveryAddress);//
            NewDelieveUtils util = new NewDelieveUtils();
            delieveStr = util.getDelieveStr("0", String.valueOf(agentid), deliveryAddress, 80);
        }
        catch (Exception e) {
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        //更新快递时效信息到数据库中
        trainOrderOfflineDao.updateTrainOrderOfflineexpressDeliverById(offlineOrderId, delieveStr);

        //插入邮寄信息表
        mailAddress.setORDERID(offlineOrderId.intValue());

        /**
         * 获取到省份信息之后，进行插入 - 但是后期无用，又不能为null - 可以出传入空字符串
         * 
         * ExpressNum - MAILNAME - MAILTEL - 【PROVINCENAME】 - 【CITYNAME】 - 【REGIONNAME】 - ADDRESS - ExpressAgent
         * 
         * 【PROVINCENAME】/【CITYNAME】/【REGIONNAME】 - ""
         */

        mailAddress.setPROVINCENAME("");
        mailAddress.setCITYNAME("");
        mailAddress.setREGIONNAME("");

        WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-mailAddress-->" + mailAddress);

        try {
            mailAddressDao.addMailAddress(mailAddress);
        }
        catch (Exception e1) {
            //幂等的设计和实现
            JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e1);
            TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
            return jsonTemp;
        }

        //插入乘客表 - 插入火车票表
        for (int i = 0; i < trainPassengerOfflineList.size(); i++) {
            //插入乘客表
            TrainPassengerOffline trainPassengerOffline = trainPassengerOfflineList.get(i);
            trainPassengerOffline.setOrderId(offlineOrderId);//关联系统平台的 - Id
            Long offlinePassengerId = 0L;
            try {
                offlinePassengerId = Long
                        .valueOf(trainPassengerOfflineDao.addTrainPassengerOffline(trainPassengerOffline));
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }

            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-trainPassengerOffline-->" + trainPassengerOffline);

            //插入火车票表
            TrainTicketOffline trainTicketOffline = trainTicketOfflineList.get(i);
            trainTicketOffline.setTrainPid(offlinePassengerId);
            trainTicketOffline.setOrderId(offlineOrderId);
            try {
                trainTicketOfflineDao.addTrainTicketOffline(trainTicketOffline);
            }
            catch (Exception e) {
                //幂等的设计和实现
                JSONObject jsonTemp = ExceptionTNUtil.handleTNException(e);
                TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, jsonTemp.toJSONString(), r1);
                return jsonTemp;
            }

            WriteLog.write(LOGNAME, r1 + ":途牛线下票出票请求-trainTicketOffline-->" + trainTicketOffline);

        }

        //记录日志
        //TrainOrderOfflineUtil.CreateBusLogAdminNoUserIdOtherDealResult(offlineOrderId, "订单接收成功", 0);//0 - 订单已发放 - 刚接受订单
        TrainOrderOfflineUtil.CreateBusLogAdminOtherDealResult(offlineOrderId, agentid, "订单接收成功", 0);//0 - 订单已发放 - 刚接受订单

        //上述获取和插入的全部都是对 TrainTicket 和 TrainInsure 的 集合 的相关操作 - 

        //订单信息入库

        //TicketCount - [需要订单提交的时候，即进行计算] - 来自集合的长度

        //记录请求信息日志

        //不同的支付方式，涉及不同的处理方式

        /**
         * 
        {
        "success": true,
        "errorCode": 231000,
        "msg": "请求已经接收",
        "data": {
        "orderId": "tn17072220433404"
        }
        }
         * 
         */
        resFlag = true;
        errorCode = 231000;//success true用231000，false用231099
        resResult.put("success", resFlag);
        resResult.put("errorCode", errorCode);
        resResult.put("msg", "请求已经接收");

        //此处另开线程处理出票请求超时的逻辑

        //幂等的设计和实现
        TrainOrderOfflineUtil.resOneAndAddResult(trainOfflineIdempotent, resResult.toJSONString(), r1);

        return resResult;
    }

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();//获取开始时间

        HttpPostJsonUtil httpPostJsonUtil = new HttpPostJsonUtil();

        //String data = "{\"cheCi\":\"G1956\",\"toStationCode\":\"TNV\",\"fromStationCode\":\"NKH\",\"deliveryInfo\":{\"recipientPhone\":\"18516101447\",\"recipientName\":\"余学文\",\"isJdFeedBack\":false,\"zipCode\":\"\",\"isJdDelivery\":false,\"deliveryAddress\":\"上海市#静安区恒丰路太阳CITY6楼\"},\"departTime\":\"13:40\",\"callBackUrl\":\"http://218.94.82.118:9181/aln/common/delivery/confirmFeedback\",\"passengers\":[{\"passportNo\":\"220124195707030423\",\"procedureFee\":\"4\",\"passengerId\":138307446,\"piaoTypeName\":\"成人票\",\"zwCode\":\"O\",\"ticketNo\":\"\",\"price\":\"562.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"赵桂荣\",\"piaoType\":\"1\",\"zwName\":\"二等座\"}],\"toStationName\":\"太原南\",\"arriveTime\":\"21:35\",\"trainDate\":\"2017-09-11\",\"hasSeat\":true,\"fromStationName\":\"南京南\",\"personalTailor\":{\"lowerNum\":0,\"isSameRoom\":false,\"memo\":\"\",\"middleNum\":0,\"acceptOthers\":false,\"topNum\":0,\"nextWindowNumber\":0,\"connectingNum\":0,\"isConnecting\":false,\"info\":\"F\"},\"orderId\":\"test17081987854221\"}";

        //定制信息为null - 先测试异常的统一处理
        //String data = "{\"orderId\":\"test17082414155149\",\"cheCi\":\"6044\",\"fromStationCode\":\"CBF\",\"fromStationName\":\"长治北\",\"toStationCode\":\"UTP\",\"toStationName\":\"潞城\",\"departTime\":\"07:55\",\"arriveTime\":\"08:17\",\"trainDate\":\"2017-09-12\",\"hasSeat\":true,\"callBackUrl\":\"http://218.94.82.118:9181/aln/common/delivery/confirmFeedback\",\"deliveryInfo\":{\"recipientName\":\"测测2\",\"deliveryAddress\":\"陕西西安碑林区XXX路XXX号\",\"zipCode\":\"457342\",\"recipientPhone\":\"13109596313\",\"isJdDelivery\":false,\"isJdFeedBack\":false,\"trackingNumber\":null,\"provinceName\":null,\"cityName\":null,\"areaName\":null},\"passengers\":[{\"passengerId\":266737,\"ticketNo\":null,\"passengerName\":\"平台测试单1\",\"passportNo\":\"330382199606167111\",\"passportTypeId\":\"1\",\"passportTypeName\":\"二代身份证\",\"piaoType\":\"1\",\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"zwName\":\"硬座\",\"cxin\":\"\",\"price\":\"8.00\",\"procedureFee\":null,\"provinceCode\":null,\"schoolCode\":null,\"schoolName\":null,\"studentNo\":null,\"schoolSystem\":null,\"enterYear\":null,\"preferenceFromStationName\":null,\"preferenceFromStationCode\":null,\"preferenceToStationName\":null,\"preferenceToStationCode\":null,\"memo\":null}],\"personalTailor\":null}";

        //String data = "{\"orderId\":\"test17082940751044\",\"cheCi\":\"6044\",\"toStationCode\":\"UTP\",\"fromStationCode\":\"CBF\",\"deliveryInfo\":{\"recipientPhone\":\"13109596313\",\"recipientName\":\"平台测试单\",\"isJdFeedBack\":false,\"zipCode\":\"457342\",\"isJdDelivery\":false,\"deliveryAddress\":\"陕西西安碑林区XXX路XXX号\"},\"departTime\":\"07:55\",\"callBackUrl\":\"http://218.94.82.118:9181/aln/common/delivery/confirmFeedback\",\"passengers\":[{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单1\",\"piaoType\":\"1\",\"cxin\":\"\",\"zwName\":\"硬座\"},{\"passportNo\":\"330382199606167112\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单2\",\"piaoType\":\"2\",\"cxin\":\"\",\"zwName\":\"硬座\"},{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单3\",\"piaoType\":\"4\",\"cxin\":\"\",\"zwName\":\"硬座\"}],\"toStationName\":\"潞城\",\"arriveTime\":\"08:17\",\"trainDate\":\"2017-09-15\",\"hasSeat\":false,\"fromStationName\":\"长治北\",\"personalTailor\":{\"lowerNum\":2,\"isSameRoom\":false,\"middleNum\":0,\"acceptOthers\":true,\"topNum\":0,\"nextWindowNumber\":0,\"connectingNum\":0,\"isConnecting\":false,\"info\":\"\"}}";

        //String data = "{\"orderId\":\"test17082940751052\",\"cheCi\":\"6044\",\"toStationCode\":\"UTP\",\"fromStationCode\":\"CBF\",\"deliveryInfo\":{\"recipientPhone\":\"13109596313\",\"recipientName\":\"平台测试单\",\"isJdFeedBack\":false,\"zipCode\":\"457342\",\"isJdDelivery\":false,\"deliveryAddress\":\"陕西西安碑林区XXX路XXX号\"},\"departTime\":\"07:55\",\"callBackUrl\":\"http://218.94.82.118:9181/aln/common/delivery/confirmFeedback\",\"passengers\":[{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单1\",\"piaoType\":\"1\",\"cxin\":\"\",\"zwName\":\"硬卧\"},{\"passportNo\":\"330382199606167112\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单2\",\"piaoType\":\"2\",\"cxin\":\"\",\"zwName\":\"硬卧\"},{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单3\",\"piaoType\":\"4\",\"cxin\":\"\",\"zwName\":\"硬座\"}],\"toStationName\":\"潞城\",\"arriveTime\":\"08:17\",\"trainDate\":\"2017-09-15\",\"hasSeat\":true,\"fromStationName\":\"长治北\",\"personalTailor\":{\"lowerNum\":2,\"isSameRoom\":false,\"middleNum\":0,\"acceptOthers\":false,\"topNum\":0,\"nextWindowNumber\":0,\"connectingNum\":0,\"isConnecting\":false,\"info\":\"\"}}";

        String data = "{\"orderId\":\"test17082940752226\",\"cheCi\":\"G1353\",\"toStationCode\":\"UTP\",\"fromStationCode\":\"CBF\",\"deliveryInfo\":{\"recipientPhone\":\"13109596313\",\"recipientName\":\"平台测试单-多乘客测试\",\"isJdFeedBack\":false,\"zipCode\":\"457342\",\"isJdDelivery\":false,"
                + "\"deliveryAddress\":\"陕西省西安市碑林区#XXX路XXX号\"},\"departTime\":\"07:55\",\"callBackUrl\":\"http://218.94.82.118:9180/aln/common/delivery/confirmFeedback\",\"passengers\":["
                + "{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单1\",\"piaoType\":\"1\",\"cxin\":\"\",\"zwName\":\"二等座\"}"
                + ",{\"passportNo\":\"330382199606167112\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单2\",\"piaoType\":\"2\",\"cxin\":\"\",\"zwName\":\"二等座\"}"
                + ",{\"passportNo\":\"330382199606167111\",\"passengerId\":266737,\"piaoTypeName\":\"成人票\",\"zwCode\":\"1\",\"price\":\"8.00\",\"passportTypeName\":\"二代身份证\",\"passportTypeId\":\"1\",\"passengerName\":\"平台测试单3\",\"piaoType\":\"2\",\"cxin\":\"\",\"zwName\":\"二等座\"}"
                + "],"
                + "\"toStationName\":\"潞城\",\"arriveTime\":\"08:17\",\"trainDate\":\"2017-09-15\",\"hasSeat\":true,\"fromStationName\":\"长治北\","

                //本地定制测试
                //+ "\"personalTailor\":{\"lowerNum\":1,\"isSameRoom\":true,\"middleNum\":1,\"acceptOthers\":false,\"topNum\":0,\"nextWindowNumber\":0,\"connectingNum\":0,\"isConnecting\":false,\"info\":\"\"}}";

                //途牛定制1
                //+ "\"personalTailor\":{\"info\": \"D-F\",\"isConnecting\": false,\"isSameRoom\": false,\"acceptOthers\": false,\"connectingNum\": 0,\"topNum\": 0,\"middleNum\": 0,\"lowerNum\": 0,\"nextWindowNumber\": 0,\"memo\": \"\"}}";

                //途牛定制2
                //+ "\"personalTailor\":{\"info\":\"\",\"isConnecting\":false,\"isSameRoom\":false,\"acceptOthers\":false,\"connectingNum\":0,\"topNum\":0,\"middleNum\":0,\"lowerNum\":1,\"nextWindowNumber\":0,\"memo\":\"\"}}";

                //途牛定制3
                + "\"personalTailor\":{\"info\":\"F\",\"isConnecting\":false,\"isSameRoom\":false,\"acceptOthers\":false,\"connectingNum\":0,\"topNum\":0,\"middleNum\":0,\"lowerNum\":0,\"nextWindowNumber\":0,\"memo\":\"\"}}";

        System.out.println(data);

        //String timestamp = "2017-08-1810:34:29";
        String timestamp = TrainOrderOfflineUtil.getTuNiuTimestamp();
        //本地测试地址
        //String url = "http://localhost:8097/ticket_inter/TrainTuNiuOfflineOrder";
        //测试环境地址
        String url = "http://121.40.226.72:9007/ticket_inter/TrainTuNiuOfflineOrder";
        //String url = "http://peisong.test.51kongtie.com/ticket_inter/TrainTuNiuOfflineOrder";

        //正式环境地址
        //String url = "http://121.40.241.126:9010/ticket_inter/TrainTuNiuOfflineOrder";
        //        String url = "http://ws.peisong.51kongtie.com/ticket_inter/TrainTuNiuOfflineOrder";

        System.out.println(httpPostJsonUtil.getTuNiuRes(data, timestamp, url));

        long endTime = System.nanoTime();//获取结束时间
        long runTimeNS = endTime - startTime;
        System.out.println("程序运行时间： " + runTimeNS + "ns");//毫微秒 - 纳秒 - 
        int runTimeS = (int) (runTimeNS / 1000000000);
        System.out.println("程序运行时间： " + runTimeS + "s");//秒 - 
        System.out.println("程序运行时间： " + runTimeS / 60 + "min：" + runTimeS % 60 + "s");//分
    }

}
