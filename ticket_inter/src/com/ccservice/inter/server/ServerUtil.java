package com.ccservice.inter.server;

import java.net.MalformedURLException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.b2b2c.base.service.ITrainService;

public class ServerUtil {
    static HessianProxyFactory factory = new HessianProxyFactory();

    /**
     * 
     * @time 2015年9月30日 下午3:28:40
     * @author chendong
     * @return 
     */
    public static ISystemService getISystemService(String serviceurl) {
        //        String serviceurl = "";
        //        String LogintocheckHeyan_serviceurl = PropertyUtil.getValue("serviceurl", "train.checkMobile.properties");
        //        serviceurl = LogintocheckHeyan_serviceurl;
        ISystemService isystemservice = null;
        try {
            isystemservice = (ISystemService) factory.create(ISystemService.class,
                    serviceurl + ISystemService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return isystemservice;
    }

    public static ITrainService getTrainService(String serviceurl) {
        try {
            return (ITrainService) factory
                    .create(ITrainService.class, serviceurl + ITrainService.class.getSimpleName());
        }
        catch (MalformedURLException e) {
            System.out.println("****调用TrainService出现异常：");
            e.printStackTrace();
            return null;
        }
    }
}
