
package com.yeexing.webservice.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="orderid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalPrice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="payPlatform" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pay_notify_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="out_notify_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sign" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="isAutoCancel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cancel_notify_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userName",
    "orderid",
    "totalPrice",
    "payPlatform",
    "payNotifyUrl",
    "outNotifyUrl",
    "sign",
    "isAutoCancel",
    "cancelNotifyUrl"
})
@XmlRootElement(name = "AutoOut")
public class AutoOut {

    @XmlElement(required = true, nillable = true)
    protected String userName;
    @XmlElement(required = true, nillable = true)
    protected String orderid;
    @XmlElement(required = true, nillable = true)
    protected String totalPrice;
    @XmlElement(required = true, nillable = true)
    protected String payPlatform;
    @XmlElement(name = "pay_notify_url", required = true, nillable = true)
    protected String payNotifyUrl;
    @XmlElement(name = "out_notify_url", required = true, nillable = true)
    protected String outNotifyUrl;
    @XmlElement(required = true, nillable = true)
    protected String sign;
    @XmlElement(required = true, nillable = true)
    protected String isAutoCancel;
    @XmlElement(name = "cancel_notify_url", required = true, nillable = true)
    protected String cancelNotifyUrl;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the orderid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderid() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderid(String value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the totalPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the value of the totalPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPrice(String value) {
        this.totalPrice = value;
    }

    /**
     * Gets the value of the payPlatform property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayPlatform() {
        return payPlatform;
    }

    /**
     * Sets the value of the payPlatform property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayPlatform(String value) {
        this.payPlatform = value;
    }

    /**
     * Gets the value of the payNotifyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayNotifyUrl() {
        return payNotifyUrl;
    }

    /**
     * Sets the value of the payNotifyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayNotifyUrl(String value) {
        this.payNotifyUrl = value;
    }

    /**
     * Gets the value of the outNotifyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutNotifyUrl() {
        return outNotifyUrl;
    }

    /**
     * Sets the value of the outNotifyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutNotifyUrl(String value) {
        this.outNotifyUrl = value;
    }

    /**
     * Gets the value of the sign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets the value of the sign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

    /**
     * Gets the value of the isAutoCancel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAutoCancel() {
        return isAutoCancel;
    }

    /**
     * Sets the value of the isAutoCancel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAutoCancel(String value) {
        this.isAutoCancel = value;
    }

    /**
     * Gets the value of the cancelNotifyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancelNotifyUrl() {
        return cancelNotifyUrl;
    }

    /**
     * Sets the value of the cancelNotifyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancelNotifyUrl(String value) {
        this.cancelNotifyUrl = value;
    }

}
