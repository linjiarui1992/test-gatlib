package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 壹码 http://www.yzm1.com/
 * @time 2015年8月20日 下午1:00:33
 * @author chendong
 */
public class YiMaMobileCode implements IMobileCode {
    public static void main(String[] args) {
        String pid = "5995";//12306项目id
        String uid = "";//12306
        String author_uid = "cd1989929";//开发者用户名
        String author_pwd = "cd1989929";//开发者用户名
        String token = "";
        YiMaMobileCode yimamobilecode = getInstance(pid);
        String mobileNo = yimamobilecode.GetMobilenum(uid, pid, token);//获取手机号 13531961860|51a75506c9801c88
        System.out.println("mobileNo:" + mobileNo);
        String getVcodeAndReleaseMobile = yimamobilecode.getVcodeAndReleaseMobile(author_uid, mobileNo, pid, token,
                author_uid);
        String mobiles = "";
        String addIgnoreList = yimamobilecode.AddIgnoreList(author_uid, mobileNo, pid, token);
        //        System.out.println(getVcodeAndReleaseMobile);

    }

    /**
     * 
     * @param pid 5995(12306项目项目编号)
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static YiMaMobileCode getInstance(String pid) {
        //        String pid = "5995";//12306项目项目编号
        String uid = "";//12306
        String author_uid = PropertyUtil.getValue("YiMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("YiMaauthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        YiMaMobileCode yiMaMobileCode = new YiMaMobileCode(pid, uid, author_uid, author_pwd);
        return yiMaMobileCode;
    }

    public YiMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        this.token = LoginIn(author_uid, author_pwd);//登录;
    }

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    //平台登陆
    final static String LOGININURL = "http://www.yzm1.com/api/do.php";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://www.yzm1.com/api/do.php";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://www.yzm1.com/api/do.php";

    //拉黑号码
    final static String ADDIGNORELISTURL = "http://www.yzm1.com/api/do.php";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.yzm1.com/api/do.php";

    final static String CANCELRECV = "http://www.yzm1.com/api/do.php";

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String, java.lang.String)
     */
    @Override
    public String LoginIn(String uid, String pwd) {//&uid=用户名&pwd=密码
        System.out.println("帐号:" + uid);
        String paramContent = "action=loginIn&name=" + this.author_uid + "&password=" + this.author_pwd;
        String result = SendPostandGet.submitPost(LOGININURL, paramContent, "utf-8").toString();// .Post(API.LoginIn, String.Format("uid={0}&pwd={1}", uid, pwd));
        WriteLog.write("YiMaMobileCode_LoginIn", paramContent + ":LoginIn:" + result);
        String[] results = result.split("[|]");
        if ("1".equals(results[0])) {
            result = results[1];
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        String paramContent = "action=getPhone&sid=" + this.pid + "&token=" + this.token;
        String result = SendPostandGet.submitPost(GETMOBILENUMURL, paramContent, "utf-8").toString();
        WriteLog.write("YiMaMobileCode_GetMobilenum",
                this.author_uid + ":->" + paramContent + ":GetMobilenum:" + result);
        String[] results = result.split("[|]");
        String showString = "";
        if ("1".equals(results[0])) {
            result = results[1];
        }
        else {
            System.out.println(showString);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    // 获取短信验证码并释放号码
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobile, String pid, String token, String author_uid) {
        String paramContent = "action=getMessage&sid=" + this.pid + "&token=" + this.token + "&phone=" + mobile
                + "&author=cd1989929";
        //        &uid=用户&token=登录时返回的令牌&pid=项目ID&mobile=获取到的手机号码
        String result = SendPostandGet.submitPost(GETVCODEANDRELEASEMOBILEURL, paramContent, "utf-8").toString();
        WriteLog.write("YiMaMobileCode_getVcodeAndReleaseMobile", paramContent + ":getVcodeAndReleaseMobile:" + result);
        if (result.contains("还没有接收到短信")) {
            result = "no_data";
        }
        else {
        }
        return result;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        String AddIgnoreListResult = "-1";
        if (this.uid == null || this.token == null) {
            AddIgnoreListResult = "用户名或密码不能为空";
        }
        else {
            String url = ADDIGNORELISTURL + "?action=addBlacklist&token=" + this.token + "&phone=" + mobiles + "&sid="
                    + this.pid;
            String result = SendPostandGet.submitGet(url, "utf-8");
            try {
                if (result.contains("操作成功")) {
                    AddIgnoreListResult = "1";
                }
                else {
                    AddIgnoreListResult = result;
                }
            }
            catch (Exception e) {
            }
            WriteLog.write("YiMaMobileCode_AddIgnoreList", url + ":AddIgnoreList:" + result);
        }
        return AddIgnoreListResult;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        String CancelSMSRecvResult = "-1";
        String url = CANCELRECV + "?action=cancelRecv&sid=" + this.pid + "&phone=" + mobile + "&token=" + this.token;
        String result = SendPostandGet.submitGet(url, "utf-8");
        try {
            if (result.contains("操作成功")) {
                CancelSMSRecvResult = "1";
            }
            else {
                CancelSMSRecvResult = result;
            }
        }
        catch (Exception e) {
        }
        WriteLog.write("YiMaMobileCode_CancelSMSRecv", url + ":AddIgnoreList:" + result);
        return CancelSMSRecvResult;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
