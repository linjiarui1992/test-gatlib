package com.ccservice.elong.hoteldb;

import java.util.List;

import java.io.StringReader;
import java.net.URLEncoder;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.ccservice.b2b2c.atom.server.Server;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.compareprice.HttpClient;

/**
 * 艺龙地标经纬度 
 * @author WH
 */
public class GoogleLandLatAndLng {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		//地标
		List<Landmark> marks = Server.getInstance().getHotelService().findAllLandmark("where (C_LNG is null or C_LAT is null) and C_CITYID is not null and C_NAME is not null", "order by C_CITYID", -1, 0);
		for(Landmark m:marks){
			try {
				//城市
				City city = Server.getInstance().getHotelService().findCity(m.getCityid());
				String cityname = city.getName();
				//请求
				String url = "http://maps.googleapis.com/maps/api/geocode/xml?address="+URLEncoder.encode(m.getName().trim(),"utf-8")+"&sensor=false&language=zh-CN&region=.%E4%B8%AD%E5%9B%BD";
				//System.out.println("请求谷歌地址："+url);
				String strReturn = HttpClient.httpget(url, "utf-8");
				if(strReturn!=null && strReturn.contains(m.getName().trim()) && strReturn.contains(cityname)){
					SAXBuilder builder = new SAXBuilder();
					Document doc = builder.build(new StringReader(strReturn.trim()));
					Element root = doc.getRootElement();
					String status = root.getChildText("status");
					if("OK".equals(status)){
						List<Element> results = root.getChildren("result");
						label:for(int i = 0 ; i < results.size() ; i++){
							Element result = results.get(i);
							List<Element> address_components = result.getChildren("address_component");
							if(address_components!=null && address_components.size()>1){
								Element first = address_components.get(0);
								if(m.getName().equals(first.getChildText("long_name")) || m.getName().equals(first.getChildText("long_name")+"站")){
									for(int j = 1 ; j < address_components.size() ; j++){
										Element address_component = address_components.get(j);
										String name = address_component.getChildText("long_name");
										List<Element> types = address_component.getChildren("type");
										for(Element type:types){
											if("locality".equals(type.getText())){
												if(name.equals(cityname) || name.equals(cityname + "市")){
													Element geometry = result.getChild("geometry");
													Element location = geometry.getChild("location");
													String lat = location.getChildText("lat");
													String lng = location.getChildText("lng");
													if(lat!=null && !"".equals(lat.trim()) && lng!=null && !"".equals(lng.trim()) && Double.parseDouble(lat)>0 && Double.parseDouble(lng)>0){
														System.out.println("更新："+m.getId()+"---"+m.getName());
														m.setLat(Double.parseDouble(lat));
														m.setLng(Double.parseDouble(lng));
														Server.getInstance().getHotelService().updateLandmarkIgnoreNull(m);
													}
													break label;
												}
											}
										}
									}
								}
							}
						}
					}
				}else if(strReturn!=null && strReturn.contains("<status>OVER_QUERY_LIMIT</status>")){
					System.out.println("已超出Google Map API规定的地址解析请求次数的最高值，程序终止。");
					break;
				}
			} catch (Exception e) {
				System.out.println("抓取谷歌经纬度出现异常---"+m.getId()+"---"+m.getName()+"---"+e.getMessage());
			}
		}
		long end = System.currentTimeMillis();
		System.out.println("消耗的时间为："+(end-start)/1000+"秒");
	}
}
