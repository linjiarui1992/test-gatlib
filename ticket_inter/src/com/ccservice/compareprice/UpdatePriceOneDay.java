package com.ccservice.compareprice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ccservice.b2b2c.base.bedtype.Bedtype;
import com.ccservice.b2b2c.base.hmhotelprice.PriceResult;
import com.ccservice.b2b2c.base.hmhotelprice.ResultProduct;
import com.ccservice.b2b2c.base.hmhotelprice.ResultRoom;
import com.ccservice.b2b2c.base.hmhotelprice.ResultStay;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelgooddata.HotelGoodData;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.util.Util;
import com.ccservice.inter.server.Server;

/**
 * 
 * @author wzc 插入固定的利润问题
 * 
 */
public class UpdatePriceOneDay extends Util{
	private ComProc proc = null;// 比价工具对象
	private Map<Long, PriceResult> dayHMData = new HashMap<Long, PriceResult>();
	
	private long profittrue=23l;
	/**
	 * 构造器，实例化比价对象
	 */
	public UpdatePriceOneDay() {
		proc = new ComProc();
	}
	
	public void test() throws Exception{
		profittrue=Long.valueOf(PropertyUtil.getValue("profit"));
		System.out.println("当前利润点为："+profittrue);
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String sql="delete from t_hotelgooddata where c_datenum<'"+sdf.format(cal.getTime())+"'";
		Server.getInstance().getSystemService().findMapResultBySql(sql, null);
		System.out.println("删除垃圾数据……");
		
		Calendar start = GregorianCalendar.getInstance();
		start.add(Calendar.DAY_OF_MONTH, 30);
		Calendar end = GregorianCalendar.getInstance();
		end.add(Calendar.DAY_OF_MONTH,31);
		long time1=System.currentTimeMillis();
			String where = " where C_NAME not like '%锦江之星%'  and c_star in (3,4,5)  and c_state=3  and c_sourcetype=3 and c_push=1";
				List<Hotel> hotels=Server.getInstance().getHotelService().findAllHotel(where, "order by id asc", -1, 0);
				int k=hotels.size();
				for (int j = 0; j < hotels.size(); j++) {
					System.out.println("酒店数量："+k--);
					long time3=System.currentTimeMillis();
					System.out.println("酒店名称："+hotels.get(j).getName());
					work(hotels.get(j), start.getTime(), end.getTime(), dayHMData);
					long time4=System.currentTimeMillis();
					System.out.println("用时："+(time4-time3)/1000+"s");
				}
		long time2=System.currentTimeMillis();
		System.out.println("用时："+(time2-time1)/1000/60);
	}
	/**
	 * 固定利润业务代码实现
	 * @param hotel
	 * @param start
	 * @param end
	 * @param dayHMData
	 * @param profit
	 */
	public void work(Hotel hotel, Date start, Date end,
			Map<Long, PriceResult> dayHMData) {
		List<HotelGoodData> HotelGoodDatas= new ArrayList<HotelGoodData>();
		try {
			proc.loadroomtype(hotel, start, end, dayHMData);
			if (dayHMData != null) {
				PriceResult result = dayHMData.get(hotel.getId());// 华闽加载的数据结果
				if (result != null) {
					String contractid=result.getContract();//合同id
					String contractver=result.getVer();//合同版本
					String hotelname = result.getHotelname();//酒店名称
					List<ResultProduct> product = result.getProduct();
					if(product!=null&&product.size()>0){
						for (ResultProduct resultProduct : product) {
							String countryarea = resultProduct.getNationname();// 试用区域
							String prod=resultProduct.getProd();//产品id
							if (countryarea.contains("不适用于中国")
									|| countryarea.contains("非中国大陆市场")
									|| countryarea.equals("香港及台湾市场")
									|| countryarea.equals("香港市场")
									|| countryarea.equals("只限香港身份证")) {
								System.out.println("不使用区域过滤……");
								continue;
							}
							int beforeday = resultProduct.getAdvance();// 提前几天
							int minday=0;
							if(resultProduct.getMin()>0){
								 minday = resultProduct.getMin();// 最少多少天
							}
							List<ResultRoom> roomstemp = resultProduct.getRooms();//获取的房型信息集合
							if(roomstemp!=null&&roomstemp.size()>0){
								for (ResultRoom resultRoom : roomstemp) {
									Bedtype bedtype = null;
									String type = resultRoom.getType();
									if ("S".equals(type)) {
										continue;
									}
									List<Bedtype> bedtypes = Server.getInstance().getHotelService().findAllBedtype("where c_type='"+ type+ "'", "",-1, 0);
									if (bedtypes.size() > 0) {
										bedtype = bedtypes.get(0);
									}else{
										Qtype.getType(type);
										bedtypes = Server.getInstance().getHotelService().findAllBedtype("where c_type='"+ type+ "'", "",-1, 0);
										bedtype = bedtypes.get(0);
									}
									String cat = resultRoom.getCat();//获取华闽的房型代码
									Roomtype roomtype = null;
									String where = "where c_hotelid="
											+ hotel.getId() + " and c_roomcode='"
											+ cat + "' and c_bed!=58 and c_bed in(select id from t_bedtype where c_type='"+type+"')";
									List<Roomtype> roomtemps = Server.getInstance().getHotelService().findAllRoomtype(where," order by id ", -1, 0);
									if (roomtemps.size() > 0) {
										roomtype = roomtemps.get(0);
									}
									String serv = resultRoom.getServ();
									String bf = resultRoom.getBf();
									String deadline = resultRoom.getDeadline();
									List<ResultStay> stays = resultRoom.getStays();
									if(stays!=null&&stays.size()>0&&roomtype!=null){
										label:for (ResultStay resultStay : stays) {
											HotelGoodData good = new HotelGoodData();
											good.setHotelid(hotel.getId());
											good.setCityid(hotel.getCityid().toString());
											good.setRoomtypeid(roomtype.getId());
											good.setRoomtypename(roomtype.getName());
											good.setHotelname(hotelname);
											good.setAllot(resultStay.getAllot());
											good.setBedtypeid(bedtype.getId());
											good.setContractid(contractid);
											good.setContractver(contractver);
											good.setBfcount(Long.parseLong(bf));
											good.setProdid(prod);
											good.setMinday((long)minday);
											if(roomtype.getQunarname()!=null){
												good.setQunarName(roomtype.getQunarname());
											}
											if(bedtype!=null){
												String bedtypename=bedtype.getTypename();
												good.setRoomtypename(good.getRoomtypename()+"-"+bedtypename);
											}
											good.setBeforeday((long)beforeday);
											Double baseprice=Double.parseDouble(resultStay.getPrice());
											good.setProfit(profittrue);
											/**
											 * 注意
											 */
											good.setShijiprice(baseprice.longValue()+profittrue);
											good.setAgentname("");
											String status=resultStay.getIsallot();
											if(status.equals("Y")){
												good.setYuliunum(0l);//暂时写为0 以后稳定了在为1（及时确认）
											}else{
												good.setYuliunum(0l);
											}
											good.setDatenum(resultStay.getStaydate());
											good.setBaseprice(Long.parseLong(resultStay.getPrice()));
											good.setSealprice(baseprice.longValue());
											if(bf.equals("0")){
												good.setBfcount(0l);
											}else if (bf.equals("1")) {
												good.setBfcount(0l);
											}else if (bf.equals("2")) {
												good.setBfcount(2l);
											} else if (bf.equals("3")){
												good.setBfcount(3l);
											}else if (bf.equals("4")){
												good.setBfcount(4l);
											}
											if (resultStay.getIsallot().equals("C")) {
												good.setRoomflag("0");
												good.setRoomstatus(1l);
												HotelGoodDatas.add(good);
												continue label;
											}
											good.setRoomflag("1");
											good.setRoomstatus(0l);
											HotelGoodDatas.add(good);
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		writeDataDB(HotelGoodDatas);
	}
	/**
	 * 写入数据写入数据库
	 * 
	 * @param gooddata
	 */
	public void writeDataDB(List<HotelGoodData> gooddata){
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Set<Long> hotelids=new HashSet<Long>();
		int version=0;
		for (int i = 0; i < gooddata.size() ; i++) {
			HotelGoodData good=gooddata.get(i);
			if(!hotelids.contains(good.getHotelid())){
				hotelids.add(good.getHotelid());
				List versionids=Server.getInstance().getSystemService().findMapResultBySql("select MAX(C_HIDECLOSE) as C_HIDECLOSE from T_HOTELGOODDATA where C_HOTELID="+good.getHotelid(), null);
				if(versionids!=null&&versionids.size()>0){
					if(((Map)versionids.get(0)).get("C_HIDECLOSE")!=null){
						version=Integer.parseInt(((Map)versionids.get(0)).get("C_HIDECLOSE").toString());
					}
				}
			}
			good.setUpdatetime(sdf1.format(new Date(System.currentTimeMillis())));
			good.setId(0l);
			good.setOpenclose(1);
			good.setSorucetype("3");
			// 特殊处理
			if(good.getProfit()<20){
				if(good.getRoomflag()!=null&&!good.getRoomflag().equals("0")){
					good.setRoomstatus(0l);//开房
				}else{
					good.setRoomflag("2");
				}
				good.setShijiprice(good.getBaseprice()+20);
			}
			if(good.getProfit()>=20){
				good.setRoomflag("1");
			}
			System.out.println(good);
			String str="[dbo].[sp_inserthmgooddata] @hotelid = "+
			good.getHotelid()+",@hotelname = N'"+
			good.getHotelname()+"',@roomtypeid = "+
			good.getRoomtypeid()+",@roomtypename = N'"+
			good.getRoomtypename()+"',@shijiprice = "+
			good.getShijiprice()+",@baseprice = "+
			good.getBaseprice()+",	@sealprice = "+
			good.getSealprice()+",@profit = "+
			good.getProfit()+",@roomstatus = "+
			good.getRoomstatus()+",@yuliunum = "+
			good.getYuliunum()+",@datenum = N'"+
			good.getDatenum()+"',@minday = "+
			good.getMinday()+",@openclose = "+
			good.getOpenclose()+",@beforeday = "+
			good.getBeforeday()+",@contractid = N'"+
			good.getContractid()+"',@contractver = N'"+
			good.getContractver()+"',@prodid = N'"+
			good.getProdid()+"',@bfcount = "+
			good.getBfcount()+",@sorucetype = N'"+
			good.getSorucetype()+"',@agentname = N'"+
			good.getAgentname()+"',@updatetime = N'"+
			good.getUpdatetime()+"',@roomflag=N'"+
			good.getRoomflag()+"',@cityid=N'"+
			good.getCityid()+"',@bedtypeid="+
			good.getBedtypeid()+",@version="+
			version+",@allot="+
			good.getAllot()+",@qunarname=N'"+
			good.getQunarName()+"'";
			synchronized (this) {
				List result = Server.getInstance().getSystemService().findMapResultByProcedure(str);
			}
		}
	}
	public ComProc getProc() {
		return proc;
	}

	public void setProc(ComProc proc) {
		this.proc = proc;
	}
	public Map<Long, PriceResult> getDayHMData() {
		return dayHMData;
	}
	public void setDayHMData(Map<Long, PriceResult> dayHMData) {
		this.dayHMData = dayHMData;
	}
}