
package com.webservice.kkkk;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderAttachedInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderAttachedInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PolicyOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PolicyActual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CoordinateOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CoordinateActual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="VipOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="VipActual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProviderLazyOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ProviderLazyActual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PurchaserLazyOriginal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PurchaserLazyActual" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Propotion" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderAttachedInfo", propOrder = {
    "policyOriginal",
    "policyActual",
    "coordinateOriginal",
    "coordinateActual",
    "vipOriginal",
    "vipActual",
    "providerLazyOriginal",
    "providerLazyActual",
    "purchaserLazyOriginal",
    "purchaserLazyActual",
    "propotion"
})
public class OrderAttachedInfo {

    @XmlElement(name = "PolicyOriginal", required = true)
    protected BigDecimal policyOriginal;
    @XmlElement(name = "PolicyActual", required = true)
    protected BigDecimal policyActual;
    @XmlElement(name = "CoordinateOriginal", required = true)
    protected BigDecimal coordinateOriginal;
    @XmlElement(name = "CoordinateActual", required = true)
    protected BigDecimal coordinateActual;
    @XmlElement(name = "VipOriginal", required = true)
    protected BigDecimal vipOriginal;
    @XmlElement(name = "VipActual", required = true)
    protected BigDecimal vipActual;
    @XmlElement(name = "ProviderLazyOriginal", required = true)
    protected BigDecimal providerLazyOriginal;
    @XmlElement(name = "ProviderLazyActual", required = true)
    protected BigDecimal providerLazyActual;
    @XmlElement(name = "PurchaserLazyOriginal", required = true)
    protected BigDecimal purchaserLazyOriginal;
    @XmlElement(name = "PurchaserLazyActual", required = true)
    protected BigDecimal purchaserLazyActual;
    @XmlElement(name = "Propotion", required = true)
    protected BigDecimal propotion;

    /**
     * Gets the value of the policyOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyOriginal() {
        return policyOriginal;
    }

    /**
     * Sets the value of the policyOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyOriginal(BigDecimal value) {
        this.policyOriginal = value;
    }

    /**
     * Gets the value of the policyActual property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPolicyActual() {
        return policyActual;
    }

    /**
     * Sets the value of the policyActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPolicyActual(BigDecimal value) {
        this.policyActual = value;
    }

    /**
     * Gets the value of the coordinateOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoordinateOriginal() {
        return coordinateOriginal;
    }

    /**
     * Sets the value of the coordinateOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoordinateOriginal(BigDecimal value) {
        this.coordinateOriginal = value;
    }

    /**
     * Gets the value of the coordinateActual property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoordinateActual() {
        return coordinateActual;
    }

    /**
     * Sets the value of the coordinateActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoordinateActual(BigDecimal value) {
        this.coordinateActual = value;
    }

    /**
     * Gets the value of the vipOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVipOriginal() {
        return vipOriginal;
    }

    /**
     * Sets the value of the vipOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVipOriginal(BigDecimal value) {
        this.vipOriginal = value;
    }

    /**
     * Gets the value of the vipActual property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVipActual() {
        return vipActual;
    }

    /**
     * Sets the value of the vipActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVipActual(BigDecimal value) {
        this.vipActual = value;
    }

    /**
     * Gets the value of the providerLazyOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProviderLazyOriginal() {
        return providerLazyOriginal;
    }

    /**
     * Sets the value of the providerLazyOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProviderLazyOriginal(BigDecimal value) {
        this.providerLazyOriginal = value;
    }

    /**
     * Gets the value of the providerLazyActual property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProviderLazyActual() {
        return providerLazyActual;
    }

    /**
     * Sets the value of the providerLazyActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProviderLazyActual(BigDecimal value) {
        this.providerLazyActual = value;
    }

    /**
     * Gets the value of the purchaserLazyOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaserLazyOriginal() {
        return purchaserLazyOriginal;
    }

    /**
     * Sets the value of the purchaserLazyOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaserLazyOriginal(BigDecimal value) {
        this.purchaserLazyOriginal = value;
    }

    /**
     * Gets the value of the purchaserLazyActual property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPurchaserLazyActual() {
        return purchaserLazyActual;
    }

    /**
     * Sets the value of the purchaserLazyActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPurchaserLazyActual(BigDecimal value) {
        this.purchaserLazyActual = value;
    }

    /**
     * Gets the value of the propotion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPropotion() {
        return propotion;
    }

    /**
     * Sets the value of the propotion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPropotion(BigDecimal value) {
        this.propotion = value;
    }

}
