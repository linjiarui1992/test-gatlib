/**
 * 版权所有, 空铁无忧
 * Author: ZZ-汽车票业务研发组荣誉出品
 * copyright: 2017
 */
package com.ccservice.offline.dao;

import com.ccervice.util.db.DBHelperOffline;
import com.ccervice.util.db.DataTable;

/**
 * @className: com.ccservice.tuniu.train.dao.TrainOfflineExpressCodeDao
 * @description: TODO - 
 * @author: 郑州-技术-郭伟强   E-mail:gwq20521@163.com
 * @createTime: 2017年8月28日 下午4:48:32 
 * @version: v 1.0
 * @since 
 *
 */
public class TrainOfflineExpressCodeDao {

    //通过存储过程获取乘客地址的citycode - sp_TrainOfflineExpress_getCode
    public String getCityCodeByAddress(String address) throws Exception {
        /*String sql = "SELECT TOP 1 * FROM (SELECT *,CHARINDEX(City,'"+address+"',0) AS CityCharIndex FROM TrainOfflineExpressCode WITH(NOLOCK) "
                + "WHERE '"+address+"' LIKE ProvinceSimple+'%') AS t WHERE CityCharIndex<>0 ORDER BY CityCharIndex ASC";*/
        String sql = "SELECT TOP 1 PKId,Province,ProvinceSimple,City,CityCode FROM (SELECT *,CHARINDEX(City,'"+address+"',0) AS CityCharIndex FROM TrainOfflineExpressCode1 "
                + "WITH(NOLOCK) WHERE '"+address+"' LIKE ProvinceSimple+'%') AS t WHERE CityCharIndex<>0 ORDER BY CityCharIndex ASC";
        
        DataTable dataTable = DBHelperOffline.GetDataTable(sql, null);
        if (dataTable.GetRow().size() == 0) {
            return null;
        }
        return String.valueOf(dataTable.GetRow().get(0).GetColumn("CityCode").GetValue());
    }

}
