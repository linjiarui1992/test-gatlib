package com.ccservice.elong.analyxml;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.city.City;
import com.ccservice.b2b2c.base.hotel.Hotel;
import com.ccservice.b2b2c.base.hotelimage.Hotelimage;
import com.ccservice.b2b2c.base.hotellandmark.Hotellandmark;
import com.ccservice.b2b2c.base.landmark.Landmark;
import com.ccservice.b2b2c.base.province.Province;
import com.ccservice.b2b2c.base.region.Region;
import com.ccservice.b2b2c.base.roomtype.Roomtype;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.b2b2c.base.service.ISystemService;
import com.ccservice.elong.inter.DateSwitch;

public class NewHotel {
	public static String url = "http://localhost:8080/cn_service/service/";
	public static HessianProxyFactory factory = new HessianProxyFactory();
	public static IHotelService servier;
	public static ISystemService systemservier;
	static {
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url + IHotelService.class.getSimpleName());
			systemservier = (ISystemService) factory.create(ISystemService.class, url + ISystemService.class.getSimpleName());
		} catch (Exception e) {
		}
	}
	public static String findCity = "SELECT ID,C_ELONGCITYID FROM T_CITY ORDER BY C_ELONGCITYID DESC";
	public static String findProvince = "SELECT ID,C_CODE FROM T_PROVINCE";
	public static String findRegion = "SELECT ID,C_REGIONID FROM T_REGION";
	public static String findChainInfo = "SELECT ID,C_BRANDID FROM T_CHAININFO";
	public static String findLandmark = "SELECT ID,C_ELONGMARKID,C_CITYID FROM T_LANDMARK";

	@SuppressWarnings( { "unchecked", "unchecked" })
	public static void parseXML(List list) throws Exception {
		List<City> cityList = systemservier.findMapResultBySql(findCity, null);
		List<Province> provinceList = systemservier.findMapResultBySql(findProvince, null);
		List<Region> regionList = systemservier.findMapResultBySql(findRegion, null);
		List<Chaininfo> chaininfoList = systemservier.findMapResultBySql(findChainInfo, null);
		List<Landmark> landmarkList = systemservier.findMapResultBySql(findLandmark, null);
		long startTime = System.currentTimeMillis();
		System.out.println("一次循环开始时间:" + startTime);
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Hotel hotel = new Hotel();
			City city = new City();
			Province province = new Province();
			Region region = new Region();
			Element element = (Element) it.next();
			// 酒店id
			if (!element.elementText("id").equals("")) {
				System.out.println("酒店ID:" + element.elementText("id"));
				hotel.setHotelcode(element.elementText("id"));
			}
			// 酒店名称
			if (!element.elementText("name") .equals("")) {
				hotel.setName(element.elementText("name"));
			}
			// 酒店地址
			if (!element.elementText("address") .equals("")) {
				hotel.setAddress(element.elementText("address"));
			}
			// 来源类型 艺龙1
			hotel.setSourcetype(1l);
			// 酒店所在地邮编
			if (!element.elementText("zip") .equals("")) {
				hotel.setPostcode(element.elementText("zip"));
			}
			// 酒店房间总数
			if (!element.elementText("roomNumber") .equals("")) {
				hotel.setRooms(Integer.valueOf(element.elementText("roomNumber")));
			}
			// 酒店特殊信息提示
			if (!element.elementText("availPolicy") .equals("")) {
				hotel.setAvailPolicy(element.elementText("availPolicy"));
			}
			// 酒店所在位置的纬度
			if (!element.elementText("lat") .equals("")) {
				hotel.setLat(Double.valueOf(element.elementText("lat")));
			}
			// 酒店所在位置的经度
			if (!element.elementText("lon") .equals("")) {
				hotel.setLng(Double.valueOf(element.elementText("lon")));
			}
			// 酒店所在国家
			if (!element.elementText("country") .equals("")) {
				hotel.setCountryid(168l);
			}
			// 酒店所在省份
			if (!element.elementText("province") .equals("")) {
				System.out.println("省份:" + element.elementText("province"));
				province.setCode(element.elementText("province"));
				System.out.println("Code:" + province.getCode());
				for (int i = 0; i < provinceList.size(); i++) {
					Map map = (Map) provinceList.get(i);
					if (province.getCode().equals(map.get("C_CODE"))) {
						hotel.setProvinceid(Long.valueOf(map.get("ID").toString()));
						System.out.println("provinceid  ok  !!");
					}
				}
			}
			// 酒店所在城市
			System.out.println("city:::::" + element.elementText("city"));
			if (!element.elementText("city") .equals("")) {
				city.setElongcityid(element.elementText("city"));
				for (int i = 0; i < cityList.size(); i++) {
					Map map = (Map) cityList.get(i);
					if (city.getElongcityid() == map.get("C_ELONGCITYID").toString()) {
						System.out.println("CITY表中的ID:" + Long.valueOf(map.get("ID").toString()));
						hotel.setCityid(Long.parseLong(map.get("ID").toString()));
						System.out.println("酒店ID;" + hotel.getCityid());
					}
				}
			}
			// 酒店所在商业区 region2
			if (!element.elementText("businessZone") .equals("")) {
				region.setRegionid(element.elementText("businessZone"));
				for (int i = 0; i < regionList.size(); i++) {
					Map map = (Map) regionList.get(i);
					if (region.getRegionid().equals(map.get("C_REGIONID").toString())) {
						hotel.setRegionid2(Long.parseLong(map.get("ID").toString()));
						// System.out.println("你好,北京");
					}
				}
			}
			// 酒店所在行政区 region1
			if (!element.elementText("district") .equals("")) {
				region.setRegionid(element.elementText("district"));
				for (int i = 0; i < regionList.size(); i++) {
					Map map = (Map) regionList.get(i);
					if (region.getRegionid().equals(map.get("C_REGIONID").toString())) {
						hotel.setRegionid1(Long.parseLong(map.get("ID").toString()));
						// System.out.println("Nihao,beijing");
					}
				}
			}
			// 酒店介绍信息
			if (!element.elementText("introEditor") .equals("")) {
				hotel.setDescription(element.elementText("introEditor").trim());
			}
			// 可支持的信用卡
			if (!element.elementText("ccAccepted") .equals("")) {
				hotel.setCarttype(element.elementText("ccAccepted"));
			}
			if (!element.elementText("airportPickUpService") .equals("")) {
				List image = element.elements("airportPickUpService");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					hotel.setAirportservice("免费接机开始日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartDate")))
							+ "免费接机结束日期:" + DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndDate"))) + "接机每天开始时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("StartTime"))) + "接机每天结束时间:"
							+ DateSwitch.SwitchString(DateSwitch.SwitchCalendar(ele.elementText("EndTime"))));
				}
			}
			// 酒店服务设施信息
			if (!element.elementText("roomAmenities") .equals("")) {
				List image = element.elements("roomAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店服务设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setServiceitem(ele.elementText("Overview").trim());
					}
					// 服务设施列表
					if (!ele.elementText("AmenitySimpleList") .equals("")) {
						System.out.println("酒店服务设施列表:"+ele.elementText("AmenitySimpleList"));
						hotel.setRoomAmenities(ele.elementText("AmenitySimpleList").trim());
					}
				}  
			}
			// 休闲服务设施信息
			if (!element.elementText("recreationAmenities") .equals("")) {
				List image = element.elements("recreationAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 房间服务设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setPlayitem(ele.elementText("AmenitySimpleList").trim());
					}
				}
			}
			// 会议服务设施信息
			if (!element.elementText("conferenceAmenities") .equals("")) {
				List image = element.elements("conferenceAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店会议室设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setMeetingitem(ele.elementText("Overview").trim());
					}
				}
			}
			// 餐饮服务设施信息
			if (!element.elementText("diningAmenities") .equals("")) {
				List image = element.elements("diningAmenities");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店餐厅设施概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setFootitem(ele.elementText("Overview").trim());
					}
				}
			}
			// 周边交通信息
			if (!element.elementText("trafficAndAroundInformations") .equals("")) {

				List image = element.elements("trafficAndAroundInformations");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 周边交通信息概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setTrafficinfo(ele.elementText("Overview").trim());
					}
				}
			}
			// 周边餐饮信息
			if (!element.elementText("surroundingRestaurants") .equals("")) {
				List image = element.elements("surroundingRestaurants");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 周边餐厅名字
					if (!ele.elementText("Name") .equals("")) {
						hotel.setNearhotel(ele.elementText("Name"));
					}
				}
			}
			// 周边景致信息
			if (!element.elementText("surroundingAttractions") .equals("")) {
				List image = element.elements("surroundingAttractions");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 周边餐厅名字
					if (!ele.elementText("Name") .equals("")) {
						hotel.setNearhotel(ele.elementText("Name"));
					}
				}
			}
			// 周边购物信息
			if (!element.elementText("surroundingShops") .equals("")) {
				List image = element.elements("surroundingShops");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 商业区名称
					if (!ele.elementText("Name") .equals("")) {
						hotel.setNearhotel(ele.elementText("Name"));
					}
				}
			}
			// 酒店特色信息
			if (!element.elementText("featureInfo") .equals("")) {

				List image = element.elements("featureInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					// 酒店特色信息概述
					if (!ele.elementText("Overview") .equals("")) {
						hotel.setSellpoint(ele.elementText("DrivingGuide").trim() + "<br/>" + ele.elementText("PropertyOtherHightlights").trim()
								+ "<br/>" + ele.elementText("PropertyAmenitiesHightlights").trim() + "<br/>"
								+ ele.elementText("LocationHighlights").trim() + "<br/>" + ele.elementText("Overview").trim());
					}
				}
			}
			// 酒店电话(前台)
			if (!element.elementText("Phone") .equals("")) {
				hotel.setMarkettell(element.elementText("Phone"));
			}
			// 酒店传真(前台)
			if (!element.elementText("Fax") .equals("")) {
				hotel.setFax1(element.elementText("Fax"));
			}
			// 酒店开业日期
			if (!element.elementText("OpeningDate") .equals("")) {
				String OpeningDate = element.elementText("OpeningDate");
				java.sql.Date date = DateSwitch.SwitchSqlDate(DateSwitch.SwitchCalendar(OpeningDate));
				hotel.setOpendate(date);
			}
			// 酒店装修日期
			if (!element.elementText("RenovationDate") .equals("")) {
				hotel.setRepaildate(element.elementText("RenovationDate"));
			}
			// 酒店挂牌星级 0-无星级；1-一星级；2-二星级；3-三星级；4-四星级；5-五星级
			if (!element.elementText("star") .equals("")) {
				hotel.setStar(Integer.valueOf(element.elementText("star")));
			}
			// 酒店所属连锁品牌ID
			if (!element.elementText("brandID") .equals("")) {
				String brandId = element.elementText("brandID");
				for (int i = 0; i < chaininfoList.size(); i++) {
					Map map = (Map) chaininfoList.get(i);
					if (brandId.equals(map.get("C_BRANDID").toString())) {
						hotel.setChaininfoid(Long.parseLong(map.get("ID").toString()));
					}
				}
			}
			hotel.setType(1);
			hotel.setLanguage(0);
			List<Hotel> hotelList = servier.findAllHotel("where " + Hotel.COL_hotelcode + "='" + hotel.getHotelcode() + "'", "", -1, 0);
			if (hotelList.size() > 0) {
				hotel.setId(hotelList.get(0).getId());
				servier.updateHotelIgnoreNull(hotel);
			} else {
				hotel = servier.createHotel(hotel);
			}
			System.out.println();
			// 酒店附近标志物
			if (!element.elementText("Landmarks") .equals("")) {
				List image = element.elements("Landmarks");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (!ele.elementText("HotelLandMark") .equals("")) {
						List list2 = ele.elements("HotelLandMark");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Hotellandmark hotellandmark = new Hotellandmark();
							Element element2 = (Element) iterator.next();
							// 酒店ID
							if (!element2.elementText("HotelID") .equals("")) {
								hotellandmark.setHotelid(hotel.getId());
							}
							// 酒店附近标志物ID
							if (!element2.elementText("LandMarkID") .equals("")) {
								String landmarkID = element2.elementText("LandMarkID");
								for (int i = 0; i < landmarkList.size(); i++) {
									Map map = (Map) landmarkList.get(i);
									if (landmarkID.equals(map.get("C_ELONGMARKID").toString())
											&& hotel.getCityid() == Long.parseLong(map.get("C_CITYID").toString())) {
										hotellandmark.setLandmarkid(Long.parseLong(map.get("ID").toString()));
									}
								}
							}
							hotellandmark.setLanguage(0);
							hotellandmark.setHotelid(hotel.getId());
							System.out.println("酒店id~!~~~~~:" + hotel.getId());
							List<Hotellandmark> hotelandmarkList = servier.findAllHotellandmark(" where " + Hotellandmark.COL_landmarkid + "="
									+ hotellandmark.getLandmarkid(), "", -1, 0);
							if (hotelandmarkList.size() > 0) {
								hotellandmark.setId(hotelandmarkList.get(0).getId());
								servier.updateHotellandmarkIgnoreNull(hotellandmark);
							} else {
								hotellandmark = servier.createHotellandmark(hotellandmark);
							}
						}
					}
				}
			}
			// 房间类型信息
			if (!element.elementText("roomInfo") .equals("")) {
				List image = element.elements("roomInfo");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					Element ele = (Element) Image.next();
					if (!ele.elementText("room") .equals("")) {
						List list2 = ele.elements("room");
						for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
							Roomtype roomtype = new Roomtype();
							Element element2 = (Element) iterator.next();
							// 房型ID
							if (!element2.elementText("roomTypeId") .equals("")) {
								roomtype.setRoomcode(element2.elementText("roomTypeId"));
							}
							// 房型名称
							if (!element2.elementText("roomName") .equals("")) {
								roomtype.setName(element2.elementText("roomName"));
							}
							// 房间面积
							if (!element2.elementText("area") .equals("")) {
								roomtype.setAreadesc(element2.elementText("area"));
							}
							// 房间所在楼层
							if (!element2.elementText("floor") .equals("")) {
								roomtype.setLayer(element2.elementText("floor"));
							}
							// 是否有带宽 0表示无宽带，1 表示免费 2表示收费
							if (!element2.elementText("hasBroadnet") .equals("")) {
								roomtype.setWideband(Integer.valueOf(element2.elementText("hasBroadnet")));
							}
							// 宽待是否收费 0 表示免费 1 表示收费
							if (!element2.elementText("broadnetFee") .equals("")) {
								if (element2.elementText("broadnetFee").equals("1")) {
									roomtype.setWideband(2);
								}
							}
							// 备注
							if (!element2.elementText("note") .equals("")) {
								roomtype.setNote(element2.elementText("note").trim());
							}
							// 房间描述
							if (!element2.elementText("bedDescription") .equals("")) {
								roomtype.setRoomdesc(element2.elementText("bedDescription").trim());
							}
							// 床型描述信息 1 单人床 2 大床 3 双床 4 大或双 5 其他
							if (!element2.elementText("bedType") .equals("")) {
								if (element2.elementText("bedType").contains("单人床")) {
									roomtype.setBed(1);
								} else if (element2.elementText("bedType").contains("大床")) {
									roomtype.setBed(2);
								} else if (element2.elementText("bedType").contains("双床")) {
									roomtype.setBed(3);
								} else if (element2.elementText("bedType").contains("大或双")) {
									roomtype.setBed(4);
								} else {
									roomtype.setBed(5);
								}
							}
							roomtype.setLanguage(0);
							roomtype.setHotelid(hotel.getId());
							List<Roomtype> roomTypeList = servier.findAllRoomtype("where " + Roomtype.COL_name + "='" + roomtype.getName() + "'"
									+ " and " + Roomtype.COL_hotelid + "='" + roomtype.getHotelid() + "'", "", -1, 0);
							if (roomTypeList.size() > 0) {
								roomtype.setId(roomTypeList.get(0).getId());
								servier.updateRoomtypeIgnoreNull(roomtype);
							} else {
								roomtype = servier.createRoomtype(roomtype);
							}
						}
					}
				}
				System.out.println("roomType........ok!!!");
			}
			// 酒店描述
			if (!element.elementText("images") .equals("")) {
				List image = element.elements("images");
				for (Iterator Image = image.iterator(); Image.hasNext();) {
					System.out.println();
					Element ele = (Element) Image.next();
					List image2 = ele.elements("image");
					for (Iterator Image2 = image2.iterator(); Image2.hasNext();) {
						Hotelimage hotelImage = new Hotelimage();
						Element ele2 = (Element) Image2.next();
						// 图片URL地址
						if (!ele2.elementText("imgUrl") .equals("")) {
							hotelImage.setPath(ele2.elementText("imgUrl"));
						}
						// 图片类型 各值表示如下：0-展示图；1-餐厅；2-休闲室；3-会议室
						// ；4-服务；5-酒店外观
						// ；6-大堂/接待台；7-酒店介绍；8-房型；9-背景图；10-其他
						if (!ele2.elementText("imgType") .equals("")) {
							hotelImage.setType(Integer.valueOf(ele2.elementText("imgType")));
						}
						// 图片标题
						if (!ele2.elementText("title") .equals("")) {
							hotelImage.setDescription(ele2.elementText("title"));
						}
						hotelImage.setHotelid(hotel.getId());
						hotelImage.setLanguage(0);
						List<Hotelimage> hotelImageList = servier.findAllHotelimage("where " + Hotelimage.COL_path + "='" + hotelImage.getPath()
								+ "'", "", -1, 0);
						if (hotelImageList.size() > 0) {
							hotelImage.setId(hotelImageList.get(0).getId());
							servier.updateHotelimageIgnoreNull(hotelImage);
						} else {
							hotelImage = servier.createHotelimage(hotelImage);
						}
					}
				}
				System.out.println("hotelImage......ok!!!!");
			}
			System.out.println("ok!!!");
		}
		long endTime = System.currentTimeMillis();
		System.out.println("一次循环结束时间:" + endTime);
		System.out.println("插入一条数据需要:" + DateSwitch.showTime(endTime - startTime));
	}
}
