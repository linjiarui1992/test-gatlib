package com.ccservice.mobileCode;

import com.ccservice.b2b2c.atom.component.SendPostandGet;
import com.ccservice.b2b2c.atom.component.WriteLog;
import com.ccservice.elong.inter.PropertyUtil;

/**
 * 宝码 http://user.bmyzm.com/Home/Main#
 * @time 2015年8月20日 下午1:00:33
 * @author chendong
 */
public class BaoMaMobileCode implements IMobileCode {
    public static void main(String[] args) {
        String pid = "6";//12306项目id
        String uid = "";//12306
        BaoMaMobileCode baomamobilecode = getInstance(pid);
        //        String mobile = baomamobilecode.GetMobilenum("", "", "");
        String mobile = "13149342858";
        System.out.println(mobile);
        mobile = baomamobilecode.getVcodeAndReleaseMobile("", mobile, "", "", "");
        System.out.println(mobile);
    }

    /**
     * 
     * 
     * @return
     * @time 2015年9月21日 上午10:12:26
     * @author chendong
     */
    public static BaoMaMobileCode getInstance(String pid) {
        //        String pid = "3305";//12306项目项目编号
        String uid = "";//12306 
        String author_uid = PropertyUtil.getValue("BaoMaAuthor_uid", "MobileCode.properties");//"cd1989929";//开发者用户名
        String author_pwd = PropertyUtil.getValue("BaoMaMaauthor_pwd", "MobileCode.properties");//"cd1989929";//开发者用户名
        BaoMaMobileCode baoMaMobileCode = new BaoMaMobileCode(pid, uid, author_uid, author_pwd);
        return baoMaMobileCode;
    }

    public BaoMaMobileCode(String pid, String uid, String author_uid, String author_pwd) {
        super();
        this.pid = pid;
        this.uid = uid;
        this.author_uid = author_uid;
        this.author_pwd = author_pwd;
        //        this.token = LoginIn(author_uid, author_pwd);//登录;
        this.token = PropertyUtil.getValue("BaoMaMaauthor_token", "MobileCode.properties");//"cd1989929";//开发者用户名
    }

    String pid;

    String uid;

    String author_uid;

    String author_pwd;

    String token;

    //平台登陆
    final static String LOGININURL = "http://api.bmyzm.com/api/apiuser/login";

    //获取手机号码
    final static String GETMOBILENUMURL = "http://api.bmyzm.com/api/apiuser/getphone";

    //获取验证码并释放
    final static String GETVCODEANDRELEASEMOBILEURL = "http://api.bmyzm.com/api/apiuser/getcode";

    //拉黑号码
    final static String ADDIGNORELISTURL = "http://api.bmyzm.com/api/apiuser/addblack";

    //释放所有号码
    final static String RELEASEALLURL = "http://api.bmyzm.com/api/apiuser/release";

    //获取该用户所使用的号码和项目
    final static String GETRECVINGINFO = "http://www.yzm1.com/api/do.php";

    final static String CANCELRECV = "http://www.yzm1.com/api/do.php";

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#LoginIn(java.lang.String, java.lang.String)
     */
    //登录
    @Override
    public String LoginIn(String uid, String pwd) {//&uid=用户名&pwd=密码
        if (this.author_uid == null || this.author_pwd == null) {
            return "用户名或密码为空";
        }
        String url = LOGININURL + "?uid=" + this.author_uid + "&pasw=" + this.author_pwd;
        String result = SendPostandGet.submitGet(url, "utf-8");
        WriteLog.write("BaoMaMobileCode_LoginIn", url + ":LoginIn:" + result);
        String[] results = result.split("[|]");
        if ("1".equals(results[0])) {
            result = results[1];
        }
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetMobilenum(java.lang.String, java.lang.String, java.lang.String)
     */
    //获取手机号码
    @Override
    public String GetMobilenum(String uid, String pid, String token) {
        if (token == null || pid == null) {
            return "传参有误";
        }
        String url = GETMOBILENUMURL + "?getphone&token=" + this.token + "&pid=" + this.pid;
        String result = SendPostandGet.submitGet(url, "utf-8");
        WriteLog.write("BaoMobileCode_GetMobilenum", url + ":GetMobilenum:" + result);
        String[] results = result.split("[|]");
        if ("1".equals(results[0])) {
            result = results[1];
        }
        //        System.out.println("Server.yimaMobileCount.GetMobilenum:" + Server.yimaMobileCount);
        return result;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetVcodeAndHoldMobilenum(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetVcodeAndHoldMobilenum(String uid, String mobile, String next_pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    //获取验证码并不在使用本号码
    @Override
    public String getVcodeAndReleaseMobile(String uid, String mobiles, String pid, String token, String author_uid) {
        String url = GETVCODEANDRELEASEMOBILEURL + "?token=" + this.token + "&vct=" + mobiles;
        String result = SendPostandGet.submitGet(url, "utf-8").toString();
        WriteLog.write("BaoMobileCode_getVcodeAndReleaseMobile", url + ":getVcodeAndReleaseMobile:" + result);
        if (result.contains("无此记录")) {
            result = "no_data";
        }
        else {
        }
        return result;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#AddIgnoreList(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    //添加黑名单
    @Override
    public String AddIgnoreList(String uid, String mobiles, String pid, String token) {
        String AddIgnoreListResult = "-1";
        if (this.uid == null || this.token == null) {
            AddIgnoreListResult = "用户名或密码不能为空";
        }
        else {
            String url = ADDIGNORELISTURL + "?token=" + this.token + "&pid=" + pid + "&phone=" + mobiles;
            String result = SendPostandGet.submitGet(url, "utf-8");
            try {
                if (result.contains("操作成功")) {
                    AddIgnoreListResult = "1";
                }
                else {
                    AddIgnoreListResult = result;
                }
            }
            catch (Exception e) {
            }
            WriteLog.write("YiMaMobileCode_GetMobilenum", url + ":AddIgnoreList:" + result);
        }
        return AddIgnoreListResult;

    }

    //释放所有号码
    public String ReleaseAll() {
        return "";
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#GetRecvingInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String GetRecvingInfo(String uid, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecvAll(java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecvAll(String uid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#CancelSMSRecv(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String CancelSMSRecv(String uid, String mobile, String token) {
        String CancelSMSRecvResult = "-1";
        String url = CANCELRECV + "?action=cancelRecv&sid=" + this.pid + "&phone=" + mobile + "&token=" + this.token;
        String result = SendPostandGet.submitGet(url, "utf-8");
        try {
            if (result.contains("操作成功")) {
                CancelSMSRecvResult = "1";
            }
            else {
                CancelSMSRecvResult = result;
            }
        }
        catch (Exception e) {
        }
        WriteLog.write("YiMaMobileCode_CancelSMSRecv", url + ":AddIgnoreList:" + result);
        return CancelSMSRecvResult;

    }

    /* (non-Javadoc)
     * @see com.ccservice.mobileCode.IMobileCode#send(java.lang.String, java.lang.String)
     */
    @Override
    public String send(String mobile, String content) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String ReleaseMobile(String uid, String mobiles, String pid, String token) {
        // TODO Auto-generated method stub
        return null;
    }

}
