package com.ccservice.qunar.train;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ccservice.b2b2c.base.mailaddress.MailAddress;
import com.ccservice.b2b2c.base.train.Trainorder;
import com.ccservice.b2b2c.base.train.Trainticket;
import com.ccservice.b2b2c.base.trainOrderOffline.TrainOrderOffline;
import com.ccservice.b2b2c.base.trainTicketOffline.TrainTicketOffline;
import com.ccservice.b2b2c.base.trainpassenger.Trainpassenger;
import com.ccservice.b2b2c.base.trainpassengerOffline.TrainPassengerOffline;
import com.ccservice.b2b2c.util.BaiDuMapApi;
import com.ccservice.b2b2c.util.DBoperationUtil;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.server.Server;
import com.ccservice.qunar.util.ExceptionUtil;

public class TaobaoAddOrder {

		
	private static DBoperationUtil dt = new DBoperationUtil();
	
	public static void main(String[] args) {
//		String text="{\"mailing\":false,\"service_price\":1000,\"relation_name\":\"美珠\",\"transport_price\":2000,\"transport_phone\":\"13810065191\",\"transport_name\":\"艾蜜莉\",\"transport_address\":\"北京市北京市多多岛纳普福特火车站\",\"total_price\":14700,\"order_status\":1,\"paper_type\":4,\"is_success\":true,\"request_id\":\"5vs4szv1lmx1\",\"paper_backup\":0,\"address\":\"no address\",\"main_order_id\":194108660841092,\"paper_low_seat_count\":1,\"telephone\":\"13810065191\",\"tickets\":{\"to_agent_ticket_info\":[{\"birthday\":\"1982-11-22\",\"train_num\":\"G383\",\"tag\":1,\"certificate_num\":\"12323232\",\"passenger_type\":0,\"to_station\":\"天津\",\"insurance_price\":0,\"certificate_type\":\"1\",\"insurance_unit_price\":0,\"ticket_price\":5850,\"seat\":14,\"from_time\":\"2016-02-23 07:30:00\",\"sub_order_id\":\"185330163\",\"from_station\":\"北京南\",\"passenger_name\":\"张三\",\"to_time\":\"2016-02-23 08:16:00\"},{\"birthday\":\"1977-11-26\",\"train_num\":\"G383\",\"tag\":1,\"certificate_num\":\"123456789\",\"passenger_type\":0,\"to_station\":\"天津\",\"insurance_price\":0,\"certificate_type\":\"1\",\"insurance_unit_price\":0,\"ticket_price\":5850,\"seat\":14,\"from_time\":\"2016-02-23 07:30:00\",\"sub_order_id\":\"185330164\",\"from_station\":\"北京南\",\"passenger_name\":\"李四\",\"to_time\":\"2016-02-23 08:16:00\"}]},\"latest_issue_time\":\"2016-02-18 19:39:59\",\"order_type\":2}";
//		JSONObject info=JSONObject.parseObject(text);
//		getorder(info);
		System.out.println(getAllPaperTypes("5","0","1"));
	}
	public static  String getAllPaperTypes(String PaperType,String PaperBackup,String PaperLowSeatCount){
        String result="无";
        String result1="";
        String result2="";
        if("1".equals(PaperType)){
            result1="团体票";
        }else if("2".equals(PaperType)){
            result1="下铺票";
        }else if("3".equals(PaperType)){
            result1="靠窗票";
        }else if("4".equals(PaperType)){
            result1="连坐票";
        }
        if("0".equals(PaperBackup)){
            result2="是否接受非"+result1+":否,至少需要"+PaperLowSeatCount+"张"+result1;
        }else if("1".equals(PaperBackup)){
            result2="是否接受非"+result1+":是,至少需要"+PaperLowSeatCount+"张"+result1;
        }
        if("0".equals(PaperType)){
            result="无";
        }else{
            result="指定"+result1+","+result2+".";
        }
        return result;
    }
	public static void getorder(JSONObject info) {
        String ispaper = info.getString("is_success");// 是否成功.
        String order_status=info.getString("order_status");//1-已付款，2-关闭，3-成功
        String order_type=info.getString("order_type");//2：线下邮寄票
//        System.out.println(ispaper);
        if("true".equals(ispaper) && "1".equals(order_status) && "2".equals(order_type)){
        	String latest_issue_time=info.getString("latest_issue_time");//最晚出票时间------
        	String main_order_id=info.getString("main_order_id");//主订单id
        	String paperBackup=info.getString("paper_backup");//当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
        	String paperLowSeatCount=info.getString("paper_low_seat_count");//至少接受下铺/靠窗/连坐数量
        	String paperType=info.getString("paper_type");//纸质票类型: 1 靠窗,2 连坐,3 上铺,4中铺,5 下铺
        	float total_price=info.getFloat("total_price");
        	Trainorder trainorder = new Trainorder();// 创建订单
        	trainorder.setTaobaosendid("无");//备选席别
        	trainorder.setQunarOrdernumber(main_order_id);//淘宝订单号
        	String tickets1=info.getString("tickets");
        	JSONObject info1=JSONObject.parseObject(tickets1);
        	JSONArray tickets = info1.getJSONArray("to_agent_ticket_info");
        	List<Trainpassenger> trainplist = new ArrayList<Trainpassenger>();
        	for (int i = 0; i < tickets.size(); i++) {
        		List<Trainticket> traintickets = new ArrayList<Trainticket>();
        		JSONObject infotickets = tickets.getJSONObject(i);
        		String birthday = infotickets.getString("birthday");//乘客生日
        		String certNoValue = infotickets.getString("certificate_num");//乘车人证件号码
        		String certificate_type = infotickets.getString("certificate_type");//证件类型，0:身份证 1:护照 4:港澳通行证 5:台湾通行证
        		String dptStationValue = infotickets.getString("from_station");//出发站
        		String trainStartTimeValue = infotickets.getString("from_time");//出发时间
        		String insurance_price = infotickets.getString("insurance_price");//保险价格，精确到分，例如10元，输入1000
        		String insurance_unit_price = infotickets.getString("insurance_unit_price");//保险的单一价格
        		String nameValue = infotickets.getString("passenger_name");//乘客姓名
//        		String passenger_type = infotickets.getString("passenger_type");//0:成人 1:儿童 2：学生
        		int passenger_type;
        		String seat = infotickets.getString("seat");//坐席
        		String seatValue_Key=getSeat(Integer.parseInt(seat));
        		String sub_order_id = infotickets.getString("sub_order_id");//淘宝火车票子订单id
        		String tag = infotickets.getString("tag");//1:单程票
//        		double seatValue_Value = infotickets.getString("ticket_price");//单张票价(不包含保险价格),例如100元,输出为10000,精确到分
        		float seatValue_Value = infotickets.getFloat("ticket_price");
        		String arrStationValue = infotickets.getString("to_station");//到达站
        		String trainEndTimeValue = infotickets.getString("to_time");//到站时间
        		
        		String trainNoValue = infotickets.getString("train_num");//车次
        		
        		Date date_trainEndTime = new Date();
                DateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm");
                try {
                    date_trainEndTime = df.parse(trainEndTimeValue);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                
                Date date_trainStartTime = new Date();
                try {
                    date_trainStartTime = df.parse(trainStartTimeValue);
                }
                catch (ParseException e) {
                    e.printStackTrace();
                }
                long endMilliSeconds = date_trainEndTime.getTime();
                long startMilliSeconds = date_trainStartTime.getTime();
                int costtime = (int) ((endMilliSeconds - startMilliSeconds) / 1000);
                String costTime = String.valueOf(costtime / (60 * 60)) + ":" + String.valueOf((costtime % (60 * 60)) / 60);// 历时
        		
        		if ("1".equals(certificate_type)) {
        			certificate_type = "3";//护照
                }else if ("0".equals(certificate_type)) {
                	certificate_type = "1";//身份证
                }
//                else if ("4".equals(certificate_type)) {
//                	certificate_type = "4";//港澳通行证
//                }
//                else if ("5".equals(certificate_type)) {
//                	certificate_type = "5";//台湾通行证
//                }
        		if (infotickets.containsKey("passenger_type")) {// 票种类型，0:成人 1:儿童 2：学生
        			passenger_type = Integer.parseInt(infotickets.getString("passenger_type"));
        			if(passenger_type==0){
        				passenger_type=1;
        			}else if(passenger_type==1){
        				passenger_type=0;
        			}
                }
                else {
                	passenger_type = 1;
                }
        		Trainticket trainticket = new Trainticket();
                Trainpassenger trainpassenger = new Trainpassenger();
                trainticket.setArrival(arrStationValue);// 存入终点站
                trainticket.setDeparture(dptStationValue);// 存入始发站
                trainticket.setStatus(Trainticket.WAITISSUE);
                // 无法存入剩余票
                trainticket.setSeq(0);// 存入订单类型
                trainticket.setTickettype(Integer.parseInt(certificate_type));// 存入票种类型
                trainticket.setPayprice(seatValue_Value/100);
                trainticket.setPrice(seatValue_Value/100);// 应付火车票价格
                trainticket.setArrivaltime(trainEndTimeValue);// 火车到达时间
                trainticket.setTrainno(trainNoValue); // 火车编号
                trainticket.setDeparttime(trainStartTimeValue);// 火车出发时间
                trainticket.setIsapplyticket(1);
                trainticket.setRefundfailreason(0);
                trainticket.setCosttime(costTime);// 历时
                //                Map<String, String> seatmap = extSeat(seatValue_Key, extSeatValue);
                //                seatValue_Key = seatmap.get("seatCode");
                //                extSeatValue = seatmap.get("extSeat");
                trainticket.setSeattype(seatValue_Key);// 车票坐席
                trainticket.setInsurenum(0);
                trainticket.setInsurprice(0f);// 采购支付价
                trainticket.setInsurorigprice(0f);// 保险原价
                traintickets.add(trainticket);
                trainpassenger.setIdnumber(certNoValue);// 存入乘车人证件号码
                trainpassenger.setIdtype(passenger_type);// 存入乘车人证件种类
                trainpassenger.setName(nameValue);// 存入乘车人姓名
                trainpassenger.setBirthday(birthday);
                trainpassenger.setChangeid(0);
                trainpassenger.setAduitstatus(1);
                trainpassenger.setTraintickets(traintickets);
                trainplist.add(trainpassenger);
        	}
        	
       
//            //纸质票类型(0普通,1团体,2下铺,3靠窗,4连坐)
            trainorder.setPaymethod(Integer.parseInt(paperType));
//            //当下铺/靠窗/连坐无票时,是否支持非下铺/非靠窗/非连坐(0不接受,1接受)
            trainorder.setSupplypayway(Integer.parseInt(paperBackup));
//            //至少接受下铺/靠窗/连坐数量
            trainorder.setRefuseaffirm(Integer.parseInt(paperLowSeatCount));
            trainorder.setContactuser(info.getString("transport_name"));
            trainorder.setContacttel(info.getString("transport_phone"));
            trainorder.setTicketcount(trainplist.size());
            trainorder.setCreateuid(56l);
            trainorder.setInsureadreess(info.getString("transport_address"));
//
            trainorder.setOrderprice(total_price/100);
            trainorder.setIsjointtrip(0);
            trainorder.setAgentid(0);
            trainorder.setState12306(Trainorder.WAITORDER);
            trainorder.setPassengers(trainplist);
            trainorder.setOrderstatus(Trainorder.WAITISSUE);
            trainorder.setAgentprofit(0f);
            trainorder.setCommission(0f);
            trainorder.setTcprocedure(0f);
            trainorder.setInterfacetype(2);
            trainorder.setCreateuser("淘宝");
            WriteLog.write("qunartrainorder", trainorder.getQunarOrdernumber() + "(:)" + trainorder.getId());
            trainorderofflineadd(trainorder);
        }
    }
	
	public static void trainorderofflineadd(Trainorder trainorder) {
        // 创建火车票线下订单TrainOrderOffline
        TrainOrderOffline trainOrderOffline = new TrainOrderOffline();
        // 有合适的出票点，分配
        //        String addresstemp = "河北省,石家庄市,长安区,就业服务大厦";
        String addresstemp = trainorder.getInsureadreess();
        if (addresstemp.contains("省") && addresstemp.contains("市")) {
            String province = addresstemp.substring(0, addresstemp.indexOf("省"));
            String city = addresstemp.substring(addresstemp.indexOf("省") + 1, addresstemp.indexOf("市"));
            String region = addresstemp.substring(addresstemp.indexOf("市") + 1);
            addresstemp = province + "," + city + "," + region;
        }
        // 通过订单邮寄地址匹配出票点
        BaiDuMapApi bd = new BaiDuMapApi();
        //        String agentidtemp = bd.getAgentidByInsuraddress(getAllListByAdd(addresstemp, "-1"), addresstemp);
//        String agentidtemp = distribution1(addresstemp);
        String agentidtemp = "378";
        WriteLog.write("线下火车票分单log记录", "地址:"+addresstemp+"----->agentId:"+agentidtemp);
        trainOrderOffline.setAgentId(Long.valueOf(agentidtemp));//ok
        trainOrderOffline.setPaystatus(1);
        trainOrderOffline.setTradeNo("");
        trainOrderOffline.setCreateUId(trainorder.getCreateuid());//ok
        trainOrderOffline.setCreateUser(trainorder.getCreateuser());//ok
        trainOrderOffline.setContactUser(trainorder.getContactuser());//ok
        trainOrderOffline.setContactTel(trainorder.getContacttel());//ok
        trainOrderOffline.setOrderPrice(trainorder.getOrderprice());//ok
        trainOrderOffline.setAgentProfit(trainorder.getAgentprofit());//ok
        trainOrderOffline.setOrdernumberonline(trainorder.getQunarOrdernumber());//ok
        trainOrderOffline.setTicketCount(trainorder.getTicketcount());//ok
        trainOrderOffline.setPaperType(trainorder.getPaymethod());
        trainOrderOffline.setPaperBackup(trainorder.getSupplypayway());
        trainOrderOffline.setPaperLowSeatCount(trainorder.getRefuseaffirm());
        trainOrderOffline.setExtSeat(trainorder.getTaobaosendid());

        String sp_TrainOrderOffline_insert = dt.getCreateTrainorderOfficeProcedureSql(trainOrderOffline);
        WriteLog.write("TrainOrderOffline_insert_保存订单存储过程", sp_TrainOrderOffline_insert);
        List list = Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainOrderOffline_insert);
        Map map = (Map) list.get(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderid = map.get("id").toString();
        // [TrainOrderOfflineRecord]表插入数据
        String procedureRecord = "sp_TrainOrderOfflineRecord_insert @FKTrainOrderOfflineId=" + orderid
                + ",@ProviderAgentid=" + agentidtemp + ",@DistributionTime='" + sdf.format(new Date())
                + "',@ResponseTime='',@DealResult=0,@RefundReason=0,@RefundReasonStr=''";
        WriteLog.write("sp_TrainOrderOfflineRecord_insert_记录表存储过程", procedureRecord);
        Server.getInstance().getSystemService().findMapResultByProcedure(procedureRecord);
        // 火车票线下订单邮寄信息
        MailAddress address = new MailAddress();
        address.setMailName(trainorder.getContactuser());
        address.setMailTel(trainorder.getContacttel());
        address.setPrintState(0);
        // String insureaddress=trainorder.getInsureadreess();
        //        String insureaddress = "郑宇凤^^^河北^^^石家庄市^^^长安区^^^河北省石家庄市长安区裕华东路99号就业服务大厦^^^050000^^^18819411570^^^";
        String insureaddress = addresstemp;
        String[] splitadd = insureaddress.split(",");
        address.setPostcode("100000");
        address.setAddress(trainorder.getInsureadreess());
        //        address.setProvinceName(splitadd[0]);
        //        address.setCityName(splitadd[1]);
        //        address.setRegionName(splitadd[2]);
        //        address.setTownName("桥东区政府");
        address.setOrderid(Integer.parseInt(orderid));
        address.setCreatetime(trainorder.getCreatetime());
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        address.setCreatetime(timestamp);
        String sp_MailAddress_insert = dt.getMailAddressProcedureSql(address);
        WriteLog.write("TrainOrderOfflineMailAddress_insert_保存邮寄地址存储过程", sp_MailAddress_insert);
        Server.getInstance().getSystemService().findMapResultByProcedure(sp_MailAddress_insert);

        for (Trainpassenger trainpassenger : trainorder.getPassengers()) {
            // 火车票乘客线下TrainPassengerOffline
            TrainPassengerOffline trainPassengerOffline = new TrainPassengerOffline();
            trainPassengerOffline.setorderid(Long.parseLong(orderid));
            trainPassengerOffline.setname(trainpassenger.getName());
            trainPassengerOffline.setidtype(trainpassenger.getIdtype());
            trainPassengerOffline.setidnumber(trainpassenger.getIdnumber());
            trainPassengerOffline.setbirthday(trainpassenger.getBirthday());
            String sp_TrainPassengerOffline_insert = dt
                    .getCreateTrainpassengerOfficeProcedureSql(trainPassengerOffline);
            WriteLog.write("TrainOrderOfflinePassengerOffline_insert_保存乘客存储过程", sp_TrainPassengerOffline_insert);
            List listP = Server.getInstance().getSystemService()
                    .findMapResultByProcedure(sp_TrainPassengerOffline_insert);
            Map map2 = (Map) listP.get(0);
            String trainPid = map2.get("id").toString();
            for (Trainticket ticket : trainpassenger.getTraintickets()) {
                // 线下火车票TrainTicketOffline
                TrainTicketOffline trainTicketOffline = new TrainTicketOffline();
                trainTicketOffline.setTrainPid(Long.parseLong(trainPid));
                trainTicketOffline.setOrderid(Long.parseLong(orderid));
                trainTicketOffline.setDepartTime(Timestamp.valueOf(ticket.getDeparttime()));
                trainTicketOffline.setDeparture(ticket.getDeparture());
                trainTicketOffline.setArrival(ticket.getArrival());
                trainTicketOffline.setTrainno(ticket.getTrainno());
                trainTicketOffline.setTicketType(ticket.getTickettype());
                trainTicketOffline.setSeatType(ticket.getSeattype());
                trainTicketOffline.setPrice(ticket.getPrice());
                trainTicketOffline.setCostTime(ticket.getCosttime());
                trainTicketOffline.setStartTime(ticket.getDeparttime().substring(11, 16));
                trainTicketOffline.setArrivalTime(ticket.getArrivaltime().substring(11, 16));
                String sp_TrainTicketOffline_insert = dt.getCreateTrainticketOfficeProcedureSql(trainTicketOffline);
                WriteLog.write("TrainOrderOfflineTicketOffline_insert_保存车票存储过程", sp_TrainTicketOffline_insert);
                Server.getInstance().getSystemService().findMapResultByProcedure(sp_TrainTicketOffline_insert);
            }
        }
    }
	//1：硬座；2：硬卧上；3：硬卧中；4：硬卧下；5:软座；6:软卧上;7:软卧中;8:软卧下;9：商务座；10:观光座;11:一等包座;12:特等座;13:一等座;14:二等座;15:高级软卧上;16:高级软卧上;
    public static String getSeat(int i_seat) {
        String str = "";
        switch (i_seat) {
        case 1:
            str = "硬座";
            break;
        case 2:
            str = "硬卧上";
            break;
        case 3:
            str = "硬卧中";
            break;
        case 4:
            str = "硬卧下";
            break;
        case 5:
            str = "软座";
            break;
        case 6:
            str = "软卧上";
            break;
        case 7:
            str = "软卧中";
            break;
        case 8:
            str = "软卧下";
            break;
        case 9:
            str = "商务座";
            break;
        case 10:
            str = "观光座";
            break;
        case 11:
            str = "一等包座";
            break;
        case 12:
            str = "特等座";
            break;
        case 13:
            str = "一等座";
            break;
        case 14:
        	str = "二等座";
        	break;
        case 15:
        	str = "高级软卧上";
        	break;
        case 16:
        	str = "高级软卧下";
        	break;	
        default:
            break;
        }
        return str;
    }
}
