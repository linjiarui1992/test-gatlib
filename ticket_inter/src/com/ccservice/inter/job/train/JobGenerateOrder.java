package com.ccservice.inter.job.train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ccservice.b2b2c.base.customeruser.Customeruser;
import com.ccservice.inter.job.WriteLog;
import com.ccservice.inter.job.train.thread.MyThread;
import com.ccservice.inter.server.Server;

/**
 *  自动下单并行方法
 *   
 * @time 2014年8月30日 上午11:15:27
 * @author yinshubin
 */
public class JobGenerateOrder extends TrainSupplyMethod implements Job {
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        if (getNowTime()) {
            exThread();
        }
    }

    /**
     * 开启新下单线程
     * @time 2015年1月19日 下午2:03:32
     * @author fiend
     */
    public void exThreadJSP(long orderid) {
        if (orderid > 0) {
            Long l1 = System.currentTimeMillis();
            //当前系统  ID  0为qunar 1为同程
            long dangqianjiekouagentid = Long.valueOf(getSysconfigString("dangqianjiekouagentid"));
            //            long dangqianjiekouagentid = 1;
            //配置线程值大小
            Integer orderingpoolsize = Integer.valueOf(getSysconfigString("orderingpoolsize"));
            if (orderingpoolsize < 0) {
                orderingpoolsize = 50;
            }
            String sql = "UPDATE T_TRAINORDER SET C_STATE12306=2 WHERE ID =" + orderid;
            Server.getInstance().getSystemService().excuteGiftBySql(sql);
            long gaikuhaoshi = System.currentTimeMillis() - l1;
            //cninterface地址
            String cninterfaceurl = getSysconfigString("cninterfaceurl");
            //去哪儿agentid
            long qunar_agentid = Long.valueOf(getSysconfigString("qunar_agentid"));
            //同程agentid
            long tongcheng_agentid = Long.valueOf(getSysconfigString("tongcheng_agentid"));
            //同程回调地址
            String tcTrainCallBack = getSysconfigString("tcTrainCallBack");
            //配置循环下单次数
            Integer ordermax = Integer.valueOf(getSysconfigString("ordermax"));
            //qunar代付接口地址
            String qunarPayurl = getSysconfigString("qunarPayurl");
            if (ordermax < 0) {
                ordermax = 4;
            }
            // 创建一个可重用固定线程数的线程池
            ExecutorService pool = Executors.newFixedThreadPool(orderingpoolsize);
            // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
            Thread t1 = null;
            //随机REP地址
            String repUrl = RepServerUtil.getRepServer(new Customeruser(), false).getUrl();
            t1 = new MyThread(orderid, true, repUrl, cninterfaceurl, qunar_agentid, dangqianjiekouagentid, ordermax,
                    tongcheng_agentid, tcTrainCallBack, qunarPayurl);
            pool.execute(t1);
            // 关闭线程池
            pool.shutdown();
            WriteLog.write("JobGenerateOrder_exThreadJSP",
                    l1 + ":exThreadJSP:本批次结束时间:用时:" + (System.currentTimeMillis() - l1) + ":改库耗时:" + gaikuhaoshi);
        }
    }

    /**
     * 开启新下单线程
     * @time 2015年1月19日 下午2:03:32
     * @author fiend
     */
    public void exThread() {
        Long l1 = System.currentTimeMillis();
        WriteLog.write("JobGenerateOrder", l1 + ":本批次开始时间");
        //当前系统ID  0为qunar 1为同程
        long dangqianjiekouagentid = Long.valueOf(getSysconfigString("dangqianjiekouagentid"));
        //        long dangqianjiekouagentid = 1;
        //配置线程值大小
        Integer orderingpoolsize = Integer.valueOf(getSysconfigString("orderingpoolsize"));
        if (orderingpoolsize < 0) {
            orderingpoolsize = 50;
        }

        //1'获取待出票订单
        String sql = "";
        if (dangqianjiekouagentid == 0) {//qunar和B2B 只处理非联程票
            sql = "SELECT TOP "
                    + orderingpoolsize
                    + " ID FROM T_TRAINORDER WHERE C_ORDERSTATUS =2 AND C_STATE12306=1 AND C_ISQUESTIONORDER=0 AND C_ISJOINTTRIP =0 ORDER BY ID ASC";
        }
        else if (dangqianjiekouagentid == 1) {//同程
            sql = "SELECT TOP "
                    + orderingpoolsize
                    + " ID FROM T_TRAINORDER WHERE C_ORDERSTATUS IN (1,2) AND C_STATE12306=1 AND C_ISQUESTIONORDER=0 ORDER BY ID ASC";
        }
        if (!"".equals(sql)) {
            List list = Server.getInstance().getSystemService().findMapResultBySql(sql, null);
            WriteLog.write("JobGenerateOrder", l1 + ":本次处理订单数:" + list.size() + ":线程池数:" + orderingpoolsize);
            String orderids = "";
            for (int i = 0; i < list.size(); i++) {
                Map map = (Map) list.get(i);
                orderids += map.get("ID").toString() + ",";
            }
            if (!"".equals(orderids.trim())) {

                orderids = orderids.substring(0, orderids.length() - 1);
                sql = "UPDATE T_TRAINORDER SET C_STATE12306=2 WHERE  C_ISQUESTIONORDER=0 AND ID IN (" + orderids + ")";
                int ordersum = Server.getInstance().getSystemService().excuteGiftBySql(sql);
                WriteLog.write("JobGenerateOrder", l1 + ":修改下单中结束时间:" + (System.currentTimeMillis() - l1) + ":操作订单数量:"
                        + ordersum + ":orderids:" + orderids);
            }
            //cninterface地址
            String cninterfaceurl = getSysconfigString("cninterfaceurl");
            //去哪儿agentid
            long qunar_agentid = Long.valueOf(getSysconfigString("qunar_agentid"));
            //同程agentid
            long tongcheng_agentid = Long.valueOf(getSysconfigString("tongcheng_agentid"));
            //同程回调地址
            String tcTrainCallBack = getSysconfigString("tcTrainCallBack");
            //配置循环下单次数
            Integer ordermax = Integer.valueOf(getSysconfigString("ordermax"));
            //qunar代付接口地址
            String qunarPayurl = getSysconfigString("qunarPayurl");
            if (ordermax < 0) {
                ordermax = 4;
            }
            // 创建一个可重用固定线程数的线程池
            ExecutorService pool = Executors.newFixedThreadPool(orderingpoolsize);
            // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口
            Thread t1 = null;
            for (int i = 0; i < list.size(); i++) {
                Long l2 = System.currentTimeMillis();
                Map map = (Map) list.get(i);
                long trainorderid = Long.valueOf(map.get("ID").toString());
                String repUrl = RepServerUtil.getRepServer(new Customeruser(), false).getUrl();
                t1 = new MyThread(trainorderid, true, repUrl, cninterfaceurl, qunar_agentid, dangqianjiekouagentid,
                        ordermax, tongcheng_agentid, tcTrainCallBack, qunarPayurl);
                pool.execute(t1);
                WriteLog.write("JobGenerateOrder",
                        l1 + ":订单ID:" + trainorderid + ":进入下单队列:用时:" + (System.currentTimeMillis() - l2));
            }
            // 关闭线程池
            pool.shutdown();
        }
        WriteLog.write("JobGenerateOrder", l1 + ":本批次结束时间:用时:" + (System.currentTimeMillis() - l1));

    }

    /**
     * 说明:接口可调用时间:早7晚11
     * @param date
     * @return
     * @time 2014年8月30日 下午4:23:20
     * @author yinshubin
     */
    public boolean getNowTime() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        try {
            Date dateBefor = df.parse("07:02:00");
            Date dateAfter = df.parse("23:00:00");
            Date time = df.parse(df.format(date));
            if (time.after(dateBefor) && time.before(dateAfter)) {
                return true;
            }
        }
        catch (ParseException e) {
        }
        return false;//现在24小时,以后有需要再改为FALSE
    }

}
