
package com.webservice.kkkk;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompanyOemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CompanyOemType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PlatNormal"/>
 *     &lt;enumeration value="OemAdmin"/>
 *     &lt;enumeration value="OemLower"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CompanyOemType")
@XmlEnum
public enum CompanyOemType {

    @XmlEnumValue("PlatNormal")
    PLAT_NORMAL("PlatNormal"),
    @XmlEnumValue("OemAdmin")
    OEM_ADMIN("OemAdmin"),
    @XmlEnumValue("OemLower")
    OEM_LOWER("OemLower");
    private final String value;

    CompanyOemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CompanyOemType fromValue(String v) {
        for (CompanyOemType c: CompanyOemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
