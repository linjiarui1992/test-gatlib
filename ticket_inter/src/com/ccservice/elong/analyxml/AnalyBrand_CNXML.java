package com.ccservice.elong.analyxml;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.caucho.hessian.client.HessianProxyFactory;
import com.ccservice.b2b2c.base.chaininfo.Chaininfo;
import com.ccservice.b2b2c.base.service.IHotelService;
import com.ccservice.elong.inter.DateSwitch;
import com.ccservice.elong.inter.DownFile;

public class AnalyBrand_CNXML {
	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		System.out.println("开始执行时间:" + new Date());
		DownFile.download("http://114-svc.elong.com/xml/brand_cn.xml",
		"D:\\酒店数据\\brand_cn.xml");
		readXML("D:\\酒店数据\\brand_cn.xml");
		long end = System.currentTimeMillis();
		System.out.println("执行结束时间:" + new Date());
		System.out.println("耗时:" + DateSwitch.showTime(end - start));
	}

	@SuppressWarnings("unchecked")
	public static void readXML(String filename) {
		// 解析器
		SAXReader reader = new SAXReader();
		// 指定XML文件
		File file = new File(filename);
		try {
			Document doc = reader.read(file);
			Element rootElement = doc.getRootElement();
			System.out.println(rootElement.getName());
			// 获取所有brand的元素集合
			List list = rootElement.elements("brand");
			//parseXML(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 解析所有brand的元素集合
	 * 
	 * @param list
	 * @throws FileNotFoundException
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	private static void parseXML(List list) throws Exception {
		// TODO Auto-generated method stub
		// PrintStream ps=new PrintStream(new
		// FileOutputStream("F:\\航天华有\\酒店文档\\艺龙酒店API接口说明文档_V1.1.6(1)\\Brand_cn.txt"));
		String url = "http://localhost:8080/cn_service/service/";

		HessianProxyFactory factory = new HessianProxyFactory();
		IHotelService servier = null;
		try {
			servier = (IHotelService) factory.create(IHotelService.class, url
					+ IHotelService.class.getSimpleName());
		} catch (Exception e) {
			// TODO: handle exception
		}
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Chaininfo chaininfo=new Chaininfo();
			Element element = (Element) it.next();
			// 连锁酒店品牌中文简称
			if (!element.elementText("brandName") .equals("")) {
				System.out.println("brandName:"
						+ element.elementText("brandName"));
				chaininfo.setName(element.elementText("brandName"));
			}
			// 连锁酒店品牌中文全称
			if (!element.elementText("brandNameLong") .equals("")) {
				System.out.println("brandNameLong:"
						+ element.elementText("brandNameLong"));
			}
			// 连锁品牌ID 
			if (!element.elementText("brandID") .equals("")) {
				System.out.println("brandID:" + element.elementText("brandID"));
				chaininfo.setBindid(element.elementText("brandID"));
			}
			// 连锁品牌所属的酒店集团ID
			if (!element.elementText("groupId") .equals("")) {
				System.out.println("groupId:" + element.elementText("groupId"));
			}
			// 连锁品牌名称首字母
			if (!element.elementText("brandFirstletter") .equals("")) {
				chaininfo.setShortname(element.elementText("brandFirstletter"));
			}
			// 连锁品牌网站的URL
			if (!element.elementText("brandURL") .equals("")) {
				System.out.println("brandURL:"
						+ element.elementText("brandURL"));
			}
			// 连锁品牌图片地址的URL
			if (!element.elementText("picURL") .equals("")) {
				System.out.println("picURL:" + element.elementText("picURL"));
				chaininfo.setImagepic(element.elementText("picURL"));
			}
			// 连锁品牌包含酒店数量
			if (!element.elementText("hotelCount") .equals("")) {
				System.out.println("hotelCount:"
						+ element.elementText("hotelCount"));
				chaininfo.setTotal(element.elementText("hotelCount"));
			}
			// 最后的修改时间
			if (!element.elementText("lastChangetime") .equals("")) {
				System.out.println("lastChangetime:"
						+ element.elementText("lastChangetime"));
			}
			if (!element.elementText("brandPinYin") .equals("")) {
				chaininfo.setFullname(element.elementText("brandPinYin"));
			}
			System.out.println();
			List<Chaininfo> chainList = servier.findAllChaininfo("where "+Chaininfo.COL_bindid+"='"+chaininfo.getBindid()+"'", " ", -1, 0);
			if(chainList.size()>0){
				chaininfo.setId(chainList.get(0).getId());
				servier.updateChaininfoIgnoreNull(chaininfo);
			}else{
				chaininfo=servier.createChaininfo(chaininfo);
			}
		}
	}
}
